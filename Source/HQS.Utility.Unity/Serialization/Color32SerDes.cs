﻿namespace HQS.Utility.Unity.Serialization;

public class Color32SerDes : CompositeSerDes<byte, byte, byte, byte, Color32>
{
	public Color32SerDes(ISerDes<byte> serDes)
		: base(serDes, serDes, serDes, serDes)
	{
	}

	public Color32SerDes(ISerDes<byte> colorSerDes, ISerDes<byte> alphaSerDes)
		: base(colorSerDes, colorSerDes, colorSerDes, alphaSerDes)
	{
	}

	public Color32SerDes(
		ISerDes<byte> redSerDes, ISerDes<byte> greenSerDes, ISerDes<byte> blueSerDes, ISerDes<byte> alphaSerDes)
		: base(redSerDes, greenSerDes, blueSerDes, alphaSerDes)
	{
	}

	public static ISerDes<Color32> IncludeAlpha { get; } = new Color32SerDes(SerDes.OfByte);

	public static ISerDes<Color32> AlphaIs0 { get; } =
		new Color32SerDes(SerDes.OfByte, new ConstantValueSerDes<byte>(0));

	public static ISerDes<Color32> AlphaIs255 { get; } =
		new Color32SerDes(SerDes.OfByte, new ConstantValueSerDes<byte>(255));

	/// <inheritdoc />
	protected override Color32 ComposeValue(byte r, byte g, byte b, byte a) => new Color32(r, g, b, a);

	/// <inheritdoc />
	protected override void DecomposeValue(Color32 value, out byte r, out byte g, out byte b, out byte a)
	{
		r = value.r;
		g = value.g;
		b = value.b;
		a = value.a;
	}
}
