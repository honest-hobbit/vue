﻿namespace HQS.Utility.Unity.Serialization;

public class Vector4SerDes : CompositeSerDes<float, float, float, float, Vector4>
{
	public Vector4SerDes(ISerDes<float> serDes)
		: base(serDes, serDes, serDes, serDes)
	{
	}

	public Vector4SerDes(ISerDes<float> xSerDes, ISerDes<float> ySerDes, ISerDes<float> zSerDes, ISerDes<float> wSerDes)
		: base(xSerDes, ySerDes, zSerDes, wSerDes)
	{
	}

	public static ISerDes<Vector4> Instance { get; } = new Vector4SerDes(SerDes.OfFloat);

	/// <inheritdoc />
	protected override Vector4 ComposeValue(float x, float y, float z, float w) => new Vector4(x, y, z, w);

	/// <inheritdoc />
	protected override void DecomposeValue(Vector4 value, out float x, out float y, out float z, out float w)
	{
		x = value.x;
		y = value.y;
		z = value.z;
		w = value.w;
	}
}
