﻿namespace HQS.Utility.Unity.Serialization;

public static class VectorSerDes
{
	public static ISerDes<Vector2> For2D => Vector2SerDes.Instance;

	public static ISerDes<Vector3> For3D => Vector3SerDes.Instance;

	public static ISerDes<Vector4> For4D => Vector4SerDes.Instance;
}
