﻿namespace HQS.Utility.Unity.Meshing;

public class MultiMeshBuilder : IReadOnlyList<IMeshDefinition>
{
	private readonly List<Pin<MeshBuilder>> builders = new List<Pin<MeshBuilder>>();

	private readonly Func<Pin<MeshBuilder>> factory;

	public MultiMeshBuilder(OptionalMeshParts parts)
		: this(() => Pin.WithoutPool(new MeshBuilder(parts)))
	{
	}

	public MultiMeshBuilder(OptionalMeshParts parts, int vertexCapacity)
		: this(() => Pin.WithoutPool(new MeshBuilder(parts, vertexCapacity)))
	{
		Debug.Assert(vertexCapacity >= 0);
		Debug.Assert(vertexCapacity <= MeshConstants.MaxVertices);
	}

	public MultiMeshBuilder(Func<Pin<MeshBuilder>> factory)
	{
		Debug.Assert(factory != null);

		this.factory = factory;
	}

	public MultiMeshBuilder(IPinPool<MeshBuilder> pool)
	{
		Debug.Assert(pool != null);

		this.factory = pool.Rent;
	}

	/// <inheritdoc />
	public int Count => this.builders.Count;

	/// <inheritdoc />
	public IMeshDefinition this[int index]
	{
		get
		{
			IReadOnlyListContracts.Indexer(this, index);

			return this.builders[index].Value;
		}
	}

	public void AddMany(IReadOnlyList<IMeshDefinition> meshes)
	{
		Debug.Assert(meshes.AllAndSelfNotNull());

		int max = meshes.Count;
		for (int i = 0; i < max; i++)
		{
			this.Add(meshes[i]);
		}
	}

	public void Add(IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);

		if (mesh.IsEmpty())
		{
			return;
		}

		int max = this.builders.Count;
		for (int i = 0; i < max; i++)
		{
			if (this.builders[i].Value.TryAdd(mesh))
			{
				return;
			}
		}

		var newBuilder = this.factory();
		newBuilder.Value.SetTo(mesh);
		this.builders.Add(newBuilder);
	}

	public void Clear() => this.builders.ForEach(x => x.Value.Clear());

	public void ReleaseBuilders()
	{
		this.builders.ForEach(x =>
		{
			x.Value.Clear();
			x.Dispose();
		});

		this.builders.Clear();
	}

	/// <inheritdoc />
	public IEnumerator<IMeshDefinition> GetEnumerator() => this.builders.Select(x => (IMeshDefinition)x.Value).GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
