﻿namespace HQS.Utility.Unity.Meshing;

// Quad vertices are laid out as such;
// A   B
//   E
// D   C
[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like class.")]
[SuppressMessage("StyleCop", "SA1401", Justification = "Mutable tuple-like class.")]
public sealed class SplitQuadBuilder : IMeshPrimitive
{
	public QuadEdges Edges = QuadEdges.None;

	public Vertex A;

	public Vertex B;

	public Vertex C;

	public Vertex D;

	public Vertex E;

	/// <inheritdoc />
	public OptionalMeshParts Parts { get; set; } = OptionalMeshParts.None;

	public void SetPositions(Vector3 position)
	{
		this.A.Position = position;
		this.B.Position = position;
		this.C.Position = position;
		this.D.Position = position;
		this.E.Position = position;
	}

	public void SetNormals(Vector3 normal)
	{
		this.A.Normal = normal;
		this.B.Normal = normal;
		this.C.Normal = normal;
		this.D.Normal = normal;
		this.E.Normal = normal;
	}

	public void SetTangents(Vector4 tangent)
	{
		this.A.Tangent = tangent;
		this.B.Tangent = tangent;
		this.C.Tangent = tangent;
		this.D.Tangent = tangent;
		this.E.Tangent = tangent;
	}

	public void SetColors(Color32 color)
	{
		this.A.Color = color;
		this.B.Color = color;
		this.C.Color = color;
		this.D.Color = color;
		this.E.Color = color;
	}

	public void SetUVs(Vector2 uv)
	{
		this.A.UV = uv;
		this.B.UV = uv;
		this.C.UV = uv;
		this.D.UV = uv;
		this.E.UV = uv;
	}

	public void CalculateNormals()
	{
		Vector3 normal = default;
		if (this.Edges.Has(QuadEdges.AB))
		{
			normal += Vector3Utility.CalculateNormal(this.A.Position, this.B.Position, this.E.Position);
		}

		if (this.Edges.Has(QuadEdges.BC))
		{
			normal += Vector3Utility.CalculateNormal(this.B.Position, this.C.Position, this.E.Position);
		}

		if (this.Edges.Has(QuadEdges.CD))
		{
			normal += Vector3Utility.CalculateNormal(this.C.Position, this.D.Position, this.E.Position);
		}

		if (this.Edges.Has(QuadEdges.DA))
		{
			normal += Vector3Utility.CalculateNormal(this.D.Position, this.A.Position, this.E.Position);
		}

		this.SetNormals(normal.normalized);
	}

	/// <inheritdoc />
	bool IMeshPrimitive.TryAddTo(IMeshBuilder builder)
	{
		IMeshPrimitiveContracts.TryAddTo(this, builder);

		if (this.Edges == QuadEdges.None)
		{
			return true;
		}

		bool hasA = false;
		bool hasB = false;
		bool hasC = false;
		bool hasD = false;

		if (this.Edges.Has(QuadEdges.AB))
		{
			hasA = true;
			hasB = true;
		}

		if (this.Edges.Has(QuadEdges.BC))
		{
			hasB = true;
			hasC = true;
		}

		if (this.Edges.Has(QuadEdges.CD))
		{
			hasC = true;
			hasD = true;
		}

		if (this.Edges.Has(QuadEdges.DA))
		{
			hasD = true;
			hasA = true;
		}

		int offset = 0;
		int a = 0;
		int b = 0;
		int c = 0;
		int d = 0;
		if (hasA) { a = offset++; }
		if (hasB) { b = offset++; }
		if (hasC) { c = offset++; }
		if (hasD) { d = offset++; }
		int e = offset;

		if (builder.Vertices.Count + offset + 1 > MeshConstants.MaxVertices)
		{
			return false;
		}

		// Quad vertices are laid out as such;
		// A   B
		//   E
		// D   C
		int index = builder.Vertices.Count;

		if (this.Edges.Has(QuadEdges.AB))
		{
			// triangle ABE
			builder.Triangles.Add(index + a);
			builder.Triangles.Add(index + b);
			builder.Triangles.Add(index + e);
		}

		if (this.Edges.Has(QuadEdges.BC))
		{
			// triangle BCE
			builder.Triangles.Add(index + b);
			builder.Triangles.Add(index + c);
			builder.Triangles.Add(index + e);
		}

		if (this.Edges.Has(QuadEdges.CD))
		{
			// triangle CDE
			builder.Triangles.Add(index + c);
			builder.Triangles.Add(index + d);
			builder.Triangles.Add(index + e);
		}

		if (this.Edges.Has(QuadEdges.DA))
		{
			// triangle DAE
			builder.Triangles.Add(index + d);
			builder.Triangles.Add(index + a);
			builder.Triangles.Add(index + e);
		}

		// add vertices (positions)
		if (hasA) { builder.Vertices.Add(this.A.Position); }
		if (hasB) { builder.Vertices.Add(this.B.Position); }
		if (hasC) { builder.Vertices.Add(this.C.Position); }
		if (hasD) { builder.Vertices.Add(this.D.Position); }
		builder.Vertices.Add(this.E.Position);

		// add normals
		if (this.Parts.Has(OptionalMeshParts.Normals))
		{
			if (hasA) { builder.Normals.Add(this.A.Normal); }
			if (hasB) { builder.Normals.Add(this.B.Normal); }
			if (hasC) { builder.Normals.Add(this.C.Normal); }
			if (hasD) { builder.Normals.Add(this.D.Normal); }
			builder.Normals.Add(this.E.Normal);
		}

		// add tangents
		if (this.Parts.Has(OptionalMeshParts.Tangents))
		{
			if (hasA) { builder.Tangents.Add(this.A.Tangent); }
			if (hasB) { builder.Tangents.Add(this.B.Tangent); }
			if (hasC) { builder.Tangents.Add(this.C.Tangent); }
			if (hasD) { builder.Tangents.Add(this.D.Tangent); }
			builder.Tangents.Add(this.E.Tangent);
		}

		// add colors
		if (this.Parts.Has(OptionalMeshParts.Colors))
		{
			if (hasA) { builder.Colors.Add(this.A.Color); }
			if (hasB) { builder.Colors.Add(this.B.Color); }
			if (hasC) { builder.Colors.Add(this.C.Color); }
			if (hasD) { builder.Colors.Add(this.D.Color); }
			builder.Colors.Add(this.E.Color);
		}

		// add UVs
		if (this.Parts.Has(OptionalMeshParts.UV))
		{
			if (hasA) { builder.UV.Add(this.A.UV); }
			if (hasB) { builder.UV.Add(this.B.UV); }
			if (hasC) { builder.UV.Add(this.C.UV); }
			if (hasD) { builder.UV.Add(this.D.UV); }
			builder.UV.Add(this.E.UV);
		}

		return true;
	}
}
