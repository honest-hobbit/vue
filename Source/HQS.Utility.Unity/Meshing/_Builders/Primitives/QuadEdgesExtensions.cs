﻿namespace HQS.Utility.Unity.Meshing;

public static class QuadEdgesExtensions
{
	public static bool Has(this QuadEdges current, QuadEdges flag) => (current & flag) == flag;

	public static bool HasAny(this QuadEdges current, QuadEdges flags) => (current & flags) != 0;

	public static QuadEdges Add(this QuadEdges current, QuadEdges flags) => current | flags;

	public static QuadEdges Remove(this QuadEdges current, QuadEdges flags) => current & ~flags;

	public static QuadEdges Set(this QuadEdges current, QuadEdges flags, bool value) =>
		value ? current.Add(flags) : current.Remove(flags);
}
