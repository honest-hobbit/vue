﻿namespace HQS.Utility.Unity.Meshing;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
public record struct Vertex
{
	public Vector3 Position;

	public Vector3 Normal;

	public Vector4 Tangent;

	public Color32 Color;

	public Vector2 UV;
}
