﻿namespace HQS.Utility.Unity.Meshing;

[Flags]
public enum QuadEdges : byte
{
	None = 0,

	AB = 1,

	BC = 1 << 1,

	CD = 1 << 2,

	DA = 1 << 3,

	All = AB | BC | CD | DA,
}
