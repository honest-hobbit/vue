﻿namespace HQS.Utility.Unity.Meshing;

// Quad vertices are laid out as such;
// A  B
// D  C
[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like class.")]
[SuppressMessage("StyleCop", "SA1401", Justification = "Mutable tuple-like class.")]
public sealed class QuadBuilder : IMeshPrimitive
{
	public QuadDiagonal Diagonal = QuadDiagonal.AC;

	public Vertex A;

	public Vertex B;

	public Vertex C;

	public Vertex D;

	/// <inheritdoc />
	public OptionalMeshParts Parts { get; set; } = OptionalMeshParts.None;

	public void SetPositions(Vector3 position)
	{
		this.A.Position = position;
		this.B.Position = position;
		this.C.Position = position;
		this.D.Position = position;
	}

	public void SetNormals(Vector3 normal)
	{
		this.A.Normal = normal;
		this.B.Normal = normal;
		this.C.Normal = normal;
		this.D.Normal = normal;
	}

	public void SetTangents(Vector4 tangent)
	{
		this.A.Tangent = tangent;
		this.B.Tangent = tangent;
		this.C.Tangent = tangent;
		this.D.Tangent = tangent;
	}

	public void SetColors(Color32 color)
	{
		this.A.Color = color;
		this.B.Color = color;
		this.C.Color = color;
		this.D.Color = color;
	}

	public void SetUVs(Vector2 uv)
	{
		this.A.UV = uv;
		this.B.UV = uv;
		this.C.UV = uv;
		this.D.UV = uv;
	}

	public void CalculateNormals() => this.SetNormals(this.Diagonal switch
	{
		QuadDiagonal.AC =>
		(Vector3Utility.CalculateNormal(this.A.Position, this.B.Position, this.C.Position) +
		Vector3Utility.CalculateNormal(this.A.Position, this.C.Position, this.D.Position)).normalized,

		QuadDiagonal.BD =>
		(Vector3Utility.CalculateNormal(this.B.Position, this.D.Position, this.A.Position) +
		Vector3Utility.CalculateNormal(this.B.Position, this.C.Position, this.D.Position)).normalized,

		_ => throw InvalidEnumArgument.CreateException(nameof(this.Diagonal), this.Diagonal),
	});

	/// <inheritdoc />
	bool IMeshPrimitive.TryAddTo(IMeshBuilder builder)
	{
		IMeshPrimitiveContracts.TryAddTo(this, builder);

		if (builder.Vertices.Count + 4 > MeshConstants.MaxVertices)
		{
			return false;
		}

		// Quad vertices are laid out as such;
		// A  B
		// D  C
		int index = builder.Vertices.Count;
		switch (this.Diagonal)
		{
			case QuadDiagonal.AC:
				// triangle ABC
				builder.Triangles.Add(index);
				builder.Triangles.Add(index + 1);
				builder.Triangles.Add(index + 2);

				// triangle ACD
				builder.Triangles.Add(index);
				builder.Triangles.Add(index + 2);
				builder.Triangles.Add(index + 3);
				break;

			case QuadDiagonal.BD:
				// triangle ABD (or BDA)
				builder.Triangles.Add(index);
				builder.Triangles.Add(index + 1);
				builder.Triangles.Add(index + 3);

				// triangle BCD
				builder.Triangles.Add(index + 1);
				builder.Triangles.Add(index + 2);
				builder.Triangles.Add(index + 3);
				break;

			default: throw InvalidEnumArgument.CreateException(nameof(this.Diagonal), this.Diagonal);
		}

		builder.Vertices.Add(this.A.Position);
		builder.Vertices.Add(this.B.Position);
		builder.Vertices.Add(this.C.Position);
		builder.Vertices.Add(this.D.Position);

		if (this.Parts.Has(OptionalMeshParts.Normals))
		{
			builder.Normals.Add(this.A.Normal);
			builder.Normals.Add(this.B.Normal);
			builder.Normals.Add(this.C.Normal);
			builder.Normals.Add(this.D.Normal);
		}

		if (this.Parts.Has(OptionalMeshParts.Tangents))
		{
			builder.Tangents.Add(this.A.Tangent);
			builder.Tangents.Add(this.B.Tangent);
			builder.Tangents.Add(this.C.Tangent);
			builder.Tangents.Add(this.D.Tangent);
		}

		if (this.Parts.Has(OptionalMeshParts.Colors))
		{
			builder.Colors.Add(this.A.Color);
			builder.Colors.Add(this.B.Color);
			builder.Colors.Add(this.C.Color);
			builder.Colors.Add(this.D.Color);
		}

		if (this.Parts.Has(OptionalMeshParts.UV))
		{
			builder.UV.Add(this.A.UV);
			builder.UV.Add(this.B.UV);
			builder.UV.Add(this.C.UV);
			builder.UV.Add(this.D.UV);
		}

		return true;
	}
}
