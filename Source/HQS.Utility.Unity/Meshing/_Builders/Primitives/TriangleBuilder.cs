﻿namespace HQS.Utility.Unity.Meshing;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like class.")]
[SuppressMessage("StyleCop", "SA1401", Justification = "Mutable tuple-like class.")]
public sealed class TriangleBuilder : IMeshPrimitive
{
	public Vertex A;

	public Vertex B;

	public Vertex C;

	/// <inheritdoc />
	public OptionalMeshParts Parts { get; set; } = OptionalMeshParts.None;

	public void SetPositions(Vector3 position)
	{
		this.A.Position = position;
		this.B.Position = position;
		this.C.Position = position;
	}

	public void SetNormals(Vector3 normal)
	{
		this.A.Normal = normal;
		this.B.Normal = normal;
		this.C.Normal = normal;
	}

	public void SetTangents(Vector4 tangent)
	{
		this.A.Tangent = tangent;
		this.B.Tangent = tangent;
		this.C.Tangent = tangent;
	}

	public void SetColors(Color32 color)
	{
		this.A.Color = color;
		this.B.Color = color;
		this.C.Color = color;
	}

	public void SetUVs(Vector2 uv)
	{
		this.A.UV = uv;
		this.B.UV = uv;
		this.C.UV = uv;
	}

	public void CalculateNormals() => this.SetNormals(
		Vector3Utility.CalculateNormal(this.A.Position, this.B.Position, this.C.Position));

	/// <inheritdoc />
	bool IMeshPrimitive.TryAddTo(IMeshBuilder builder)
	{
		IMeshPrimitiveContracts.TryAddTo(this, builder);

		if (builder.Vertices.Count + 3 > MeshConstants.MaxVertices)
		{
			return false;
		}

		int index = builder.Vertices.Count;
		builder.Triangles.Add(index);
		builder.Triangles.Add(index + 1);
		builder.Triangles.Add(index + 2);

		builder.Vertices.Add(this.A.Position);
		builder.Vertices.Add(this.B.Position);
		builder.Vertices.Add(this.C.Position);

		if (this.Parts.Has(OptionalMeshParts.Normals))
		{
			builder.Normals.Add(this.A.Normal);
			builder.Normals.Add(this.B.Normal);
			builder.Normals.Add(this.C.Normal);
		}

		if (this.Parts.Has(OptionalMeshParts.Tangents))
		{
			builder.Tangents.Add(this.A.Tangent);
			builder.Tangents.Add(this.B.Tangent);
			builder.Tangents.Add(this.C.Tangent);
		}

		if (this.Parts.Has(OptionalMeshParts.Colors))
		{
			builder.Colors.Add(this.A.Color);
			builder.Colors.Add(this.B.Color);
			builder.Colors.Add(this.C.Color);
		}

		if (this.Parts.Has(OptionalMeshParts.UV))
		{
			builder.UV.Add(this.A.UV);
			builder.UV.Add(this.B.UV);
			builder.UV.Add(this.C.UV);
		}

		return true;
	}
}
