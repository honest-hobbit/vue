﻿namespace HQS.Utility.Unity.Meshing;

public class MeshBuilder : AbstractMeshBuilder
{
	public MeshBuilder(OptionalMeshParts parts)
		: base(parts)
	{
	}

	public MeshBuilder(OptionalMeshParts parts, int initialVertexCapacity)
		: base(parts, initialVertexCapacity)
	{
	}

	public MeshBuilder(
		OptionalMeshParts parts, int initialTriangleCapacity, int initialVertexCapacity)
		: base(parts, initialTriangleCapacity, initialVertexCapacity)
	{
	}

	public void SetTriangles(IEnumerable<int> triangles)
	{
		Debug.Assert(triangles != null);

		this.Builder.Triangles.Clear();
		this.Builder.Triangles.AddMany(triangles);
	}

	public void SetVertices(IEnumerable<Vector3> vertices)
	{
		Debug.Assert(vertices != null);
		Debug.Assert(vertices.CountExtended() <= MeshConstants.MaxVertices);

		this.Builder.Vertices.Clear();
		this.Builder.Vertices.AddManyMaxCapacity(vertices, MeshConstants.MaxVertices);
	}

	public void SetNormals(IEnumerable<Vector3> normals)
	{
		Debug.Assert(this.Parts.Has(OptionalMeshParts.Normals));
		Debug.Assert(normals != null);
		Debug.Assert(normals.CountExtended() <= MeshConstants.MaxVertices);

		this.Builder.Normals.Clear();
		this.Builder.Normals.AddManyMaxCapacity(normals, MeshConstants.MaxVertices);
	}

	public void SetTangents(IEnumerable<Vector4> tangents)
	{
		Debug.Assert(this.Parts.Has(OptionalMeshParts.Tangents));
		Debug.Assert(tangents != null);
		Debug.Assert(tangents.CountExtended() <= MeshConstants.MaxVertices);

		this.Builder.Tangents.Clear();
		this.Builder.Tangents.AddManyMaxCapacity(tangents, MeshConstants.MaxVertices);
	}

	public void SetColors(IEnumerable<Color32> colors)
	{
		Debug.Assert(this.Parts.Has(OptionalMeshParts.Colors));
		Debug.Assert(colors != null);
		Debug.Assert(colors.CountExtended() <= MeshConstants.MaxVertices);

		this.Builder.Colors.Clear();
		this.Builder.Colors.AddManyMaxCapacity(colors, MeshConstants.MaxVertices);
	}

	public void SetUV(IEnumerable<Vector2> uv)
	{
		Debug.Assert(this.Parts.Has(OptionalMeshParts.UV));
		Debug.Assert(uv != null);
		Debug.Assert(uv.CountExtended() <= MeshConstants.MaxVertices);

		this.Builder.UV.Clear();
		this.Builder.UV.AddManyMaxCapacity(uv, MeshConstants.MaxVertices);
	}

	public void SetTo(Mesh mesh)
	{
		Debug.Assert(mesh != null);

		this.Clear();
		mesh.GetTriangles(this.Builder.Triangles, 0);

		this.Builder.Vertices.EnsureCapacity(mesh.vertexCount);
		mesh.GetVertices(this.Builder.Vertices);

		if (this.Parts.Has(OptionalMeshParts.Normals))
		{
			this.Builder.Normals.EnsureCapacity(mesh.vertexCount);
			mesh.GetNormals(this.Builder.Normals);
		}

		if (this.Parts.Has(OptionalMeshParts.Tangents))
		{
			this.Builder.Tangents.EnsureCapacity(mesh.vertexCount);
			mesh.GetTangents(this.Builder.Tangents);
		}

		if (this.Parts.Has(OptionalMeshParts.Colors))
		{
			this.Builder.Colors.EnsureCapacity(mesh.vertexCount);
			mesh.GetColors(this.Builder.Colors);
		}

		if (this.Parts.Has(OptionalMeshParts.UV))
		{
			this.Builder.UV.EnsureCapacity(mesh.vertexCount);
			mesh.GetUVs(0, this.Builder.UV);
		}
	}

	public void SetTo(IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);
		Debug.Assert(mesh.Parts == this.Parts);

		this.Clear();
		if (!this.TryAdd(mesh))
		{
			throw new ArgumentException("Mesh is larger than the capacity of this builder.", nameof(mesh));
		}
	}

	public bool TryAdd(IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);
		Debug.Assert(mesh.Parts == this.Parts);

		if (mesh.IsEmpty())
		{
			return true;
		}

		if (this.Vertices.Count + mesh.Vertices.Count > MeshConstants.MaxVertices)
		{
			return false;
		}

		var previousVertexCount = this.Vertices.Count;
		this.Builder.Triangles.AddMany(mesh.Triangles.Select(x => x + previousVertexCount));
		this.Builder.Vertices.AddManyMaxCapacity(mesh.Vertices, MeshConstants.MaxVertices);
		this.Builder.Normals.AddManyMaxCapacity(mesh.Normals, MeshConstants.MaxVertices);
		this.Builder.Tangents.AddManyMaxCapacity(mesh.Tangents, MeshConstants.MaxVertices);
		this.Builder.Colors.AddManyMaxCapacity(mesh.Colors, MeshConstants.MaxVertices);
		this.Builder.UV.AddManyMaxCapacity(mesh.UV, MeshConstants.MaxVertices);
		return true;
	}

	public bool TryAdd(IMeshPrimitive primitive)
	{
		Debug.Assert(primitive != null);
		Debug.Assert(primitive.Parts == this.Parts);

		return primitive.TryAddTo(this.Builder);
	}

	public void Add(IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);
		Debug.Assert(mesh.Parts == this.Parts);

		if (!this.TryAdd(mesh))
		{
			this.ThrowVerticesCountException();
		}
	}

	public void Add(IMeshPrimitive primitive)
	{
		Debug.Assert(primitive != null);
		Debug.Assert(primitive.Parts == this.Parts);

		if (!this.TryAdd(primitive))
		{
			this.ThrowVerticesCountException();
		}
	}

	private void ThrowVerticesCountException() => throw new InvalidOperationException(
		$"Unable to add to {nameof(MeshBuilder)}. Total number of vertices would be greater than {MeshConstants.MaxVertices}.");
}
