﻿namespace HQS.Utility.Unity.Meshing;

public abstract class AbstractMeshBuilder : IMeshDefinition
{
	private readonly List<int> triangles;

	private readonly List<Vector3> vertices;

	private readonly List<Vector3> normals;

	private readonly List<Vector4> tangents;

	private readonly List<Color32> colors;

	private readonly List<Vector2> uv;

	public AbstractMeshBuilder(OptionalMeshParts parts)
		: this(parts, MeshConstants.MaxVertices)
	{
	}

	public AbstractMeshBuilder(OptionalMeshParts parts, int initialVertexCapacity)
		: this(parts, initialVertexCapacity, initialVertexCapacity)
	{
	}

	public AbstractMeshBuilder(
		OptionalMeshParts parts, int initialTriangleCapacity, int initialVertexCapacity)
	{
		Debug.Assert(initialTriangleCapacity >= 0);
		Debug.Assert(initialVertexCapacity >= 0);
		Debug.Assert(initialVertexCapacity <= MeshConstants.MaxVertices);

		this.Parts = parts;

		this.triangles = new List<int>(initialTriangleCapacity);
		this.vertices = new List<Vector3>(initialVertexCapacity);

		if (parts.Has(OptionalMeshParts.Normals))
		{
			this.normals = new List<Vector3>(initialVertexCapacity);
		}

		if (parts.Has(OptionalMeshParts.Tangents))
		{
			this.tangents = new List<Vector4>(initialVertexCapacity);
		}

		if (parts.Has(OptionalMeshParts.Colors))
		{
			this.colors = new List<Color32>(initialVertexCapacity);
		}

		if (parts.Has(OptionalMeshParts.UV))
		{
			this.uv = new List<Vector2>(initialVertexCapacity);
		}

		// null will be set to empty read only list
		this.Triangles = this.triangles.AsReadOnlyList();
		this.Vertices = this.vertices.AsReadOnlyList();
		this.Normals = this.normals.AsReadOnlyList();
		this.Tangents = this.tangents.AsReadOnlyList();
		this.Colors = this.colors.AsReadOnlyList();
		this.UV = this.uv.AsReadOnlyList();

		this.Builder = new MeshBuilder(this);
	}

	public IMeshBuilder Builder { get; }

	/// <inheritdoc />
	public OptionalMeshParts Parts { get; }

	/// <inheritdoc />
	public IReadOnlyList<int> Triangles { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector3> Vertices { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector3> Normals { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector4> Tangents { get; }

	/// <inheritdoc />
	public IReadOnlyList<Color32> Colors { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector2> UV { get; }

	/// <inheritdoc />
	public Mesh BuildMesh(Mesh mesh, bool calculateBounds)
	{
		IMeshDefinitionContracts.BuildMesh(mesh);

		// vertices has to be assigned first
		mesh.SetVertices(this.vertices);

		// optional components
		if (this.normals != null)
		{
			Debug.Assert(this.normals.Count == this.vertices.Count);
			mesh.SetNormals(this.normals);
		}

		if (this.tangents != null)
		{
			Debug.Assert(this.tangents.Count == this.tangents.Count);
			mesh.SetTangents(this.tangents);
		}

		if (this.colors != null)
		{
			Debug.Assert(this.colors.Count == this.colors.Count);
			mesh.SetColors(this.colors);
		}

		if (this.uv != null)
		{
			Debug.Assert(this.uv.Count == this.uv.Count);
			mesh.SetUVs(0, this.uv);
		}

		// triangles must be set last to avoid index out of bounds exceptions
		mesh.SetTriangles(this.triangles, 0, calculateBounds);
		return mesh;
	}

	public virtual void Clear()
	{
		this.triangles.Clear();
		this.vertices.Clear();
		this.normals?.Clear();
		this.tangents?.Clear();
		this.colors?.Clear();
		this.uv?.Clear();
	}

	/// <inheritdoc />
	public override string ToString() => this.ToDebugString();

	private class MeshBuilder : IMeshBuilder
	{
		private readonly AbstractMeshBuilder parent;

		public MeshBuilder(AbstractMeshBuilder parent)
		{
			Debug.Assert(parent != null);

			this.parent = parent;
		}

		/// <inheritdoc />
		public OptionalMeshParts Parts => this.parent.Parts;

		/// <inheritdoc />
		public List<int> Triangles => this.parent.triangles;

		/// <inheritdoc />
		public List<Vector3> Vertices => this.parent.vertices;

		/// <inheritdoc />
		public List<Vector3> Normals => this.parent.normals;

		/// <inheritdoc />
		public List<Vector4> Tangents => this.parent.tangents;

		/// <inheritdoc />
		public List<Color32> Colors => this.parent.colors;

		/// <inheritdoc />
		public List<Vector2> UV => this.parent.uv;
	}
}
