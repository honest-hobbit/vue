﻿namespace HQS.Utility.Unity.Meshing.Serialization;

public static class OptionalMeshPartSerDes
{
	public static ISerDes<OptionalMeshParts> Instance { get; } = ConverterSerDes.Create(x => (byte)x, x => (OptionalMeshParts)x, SerDes.OfByte);
}
