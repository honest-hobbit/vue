﻿namespace HQS.Utility.Unity.Meshing.Serialization;

// TODO should this include serializing the Parts field?
public class MeshDefinitionSerDes : ISerDes<IMeshDefinition>
{
	private static readonly ISerDes<int> TriangleCountSerializer = SerDes.OfInt;

	private static readonly ISerDes<int> VertexIndexSerializer = SerializeInt.AsUShort;

	private readonly ISerDes<Vector3> vertexSerializer;

	private readonly ISerDes<Vector3> normalSerializer;

	private readonly ISerDes<Vector4> tangentSerializer;

	private readonly ISerDes<Color32> colorSerializer;

	private readonly ISerDes<Vector2> uvSerializer;

	public MeshDefinitionSerDes(
		ISerDes<Vector3> vertexSerializer,
		ISerDes<Vector3> normalSerializer = null,
		ISerDes<Vector4> tangentSerializer = null,
		ISerDes<Color32> colorSerializer = null,
		ISerDes<Vector2> uvSerializer = null)
	{
		Debug.Assert(vertexSerializer != null);

		this.vertexSerializer = vertexSerializer;
		this.normalSerializer = normalSerializer;
		this.tangentSerializer = tangentSerializer;
		this.colorSerializer = colorSerializer;
		this.uvSerializer = uvSerializer;

		var parts = OptionalMeshParts.None;
		parts |= normalSerializer != null ? OptionalMeshParts.Normals : OptionalMeshParts.None;
		parts |= tangentSerializer != null ? OptionalMeshParts.Tangents : OptionalMeshParts.None;
		parts |= colorSerializer != null ? OptionalMeshParts.Colors : OptionalMeshParts.None;
		parts |= uvSerializer != null ? OptionalMeshParts.UV : OptionalMeshParts.None;
		this.RequiredParts = parts;
	}

	public OptionalMeshParts RequiredParts { get; }

	/// <inheritdoc />
	public void Serialize(IMeshDefinition mesh, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(mesh, writeByte);
		Debug.Assert(mesh.IsEmpty() || mesh.Parts == this.RequiredParts);

		TriangleCountSerializer.Serialize(mesh.Triangles.Count, writeByte);
		VertexIndexSerializer.Serialize(mesh.Vertices.Count, writeByte);

		VertexIndexSerializer.SerializeMany(mesh.Triangles, writeByte);
		this.vertexSerializer.SerializeMany(mesh.Vertices, writeByte);

		this.normalSerializer?.SerializeMany(mesh.Normals, writeByte);
		this.tangentSerializer?.SerializeMany(mesh.Tangents, writeByte);
		this.colorSerializer?.SerializeMany(mesh.Colors, writeByte);
		this.uvSerializer?.SerializeMany(mesh.UV, writeByte);
	}

	/// <inheritdoc />
	public IMeshDefinition Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		int triangleCount = TriangleCountSerializer.Deserialize(readByte);
		int verticesCount = VertexIndexSerializer.Deserialize(readByte);

		var triangles = VertexIndexSerializer.DeserializeMany(triangleCount, readByte);
		var vertices = this.vertexSerializer.DeserializeMany(verticesCount, readByte);

		var normals = this.normalSerializer?.DeserializeMany(verticesCount, readByte);
		var tangents = this.tangentSerializer?.DeserializeMany(verticesCount, readByte);
		var colors = this.colorSerializer?.DeserializeMany(verticesCount, readByte);
		var uv = this.uvSerializer?.DeserializeMany(verticesCount, readByte);

		return new MeshDefinition(triangles, vertices, normals, tangents, colors, uv);
	}
}
