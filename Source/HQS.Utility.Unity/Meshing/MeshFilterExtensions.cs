﻿namespace HQS.Utility.Unity.Meshing;

public static class MeshFilterExtensions
{
	public static Mesh GetOrAddSharedMesh(this MeshFilter meshFilter)
	{
		Ensure.That(meshFilter, nameof(meshFilter)).IsNotNull();

		var mesh = meshFilter.sharedMesh;
		if (mesh == null)
		{
			mesh = new Mesh();
			meshFilter.sharedMesh = mesh;
		}

		return mesh;
	}
}
