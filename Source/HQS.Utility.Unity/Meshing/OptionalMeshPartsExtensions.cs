﻿namespace HQS.Utility.Unity.Meshing;

public static class OptionalMeshPartsExtensions
{
	/// <summary>
	/// Determines whether the current flags value contains the specified flag or flags (all of them).
	/// This implementation is faster than the Enum.HasFlag method because it avoids boxing of values.
	/// </summary>
	/// <param name="current">The current value to check for containing flags.</param>
	/// <param name="flag">The flag or combined flags to check for.</param>
	/// <returns>True if the current value contains the flag(s); otherwise false.</returns>
	public static bool Has(this OptionalMeshParts current, OptionalMeshParts flag) => (current & flag) == flag;

	public static bool HasAny(this OptionalMeshParts current, OptionalMeshParts flags) => (current & flags) != 0;

	public static OptionalMeshParts Add(this OptionalMeshParts current, OptionalMeshParts flags) => current | flags;

	public static OptionalMeshParts Remove(this OptionalMeshParts current, OptionalMeshParts flags) => current & ~flags;

	public static OptionalMeshParts Set(this OptionalMeshParts current, OptionalMeshParts flags, bool value) =>
		value ? current.Add(flags) : current.Remove(flags);
}
