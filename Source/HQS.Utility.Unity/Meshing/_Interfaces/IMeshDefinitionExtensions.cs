﻿namespace HQS.Utility.Unity.Meshing;

public static class IMeshDefinitionExtensions
{
	public static string ToDebugString(this IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);

		return $"Indices: {mesh.Triangles.Count} Vertices: {mesh.Vertices.Count} Parts: {mesh.Parts}";
	}

	public static IMeshDefinition DeepClone(this IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);

		return new MeshDefinition(
			mesh.Triangles.ToArrayExtended(),
			mesh.Vertices.ToArrayExtended(),
			mesh.Normals.CloneOptional(),
			mesh.Tangents.CloneOptional(),
			mesh.Colors.CloneOptional(),
			mesh.UV.CloneOptional());
	}

	public static Mesh BuildMesh(this IMeshDefinition definition, Mesh mesh)
	{
		Debug.Assert(definition != null);
		Debug.Assert(mesh != null);

		return definition.BuildMesh(mesh, calculateBounds: true);
	}

	public static Mesh BuildMesh(this IMeshDefinition definition, bool calculateBounds)
	{
		Debug.Assert(definition != null);

		return definition.BuildMesh(new Mesh(), calculateBounds);
	}

	public static Mesh BuildMesh(this IMeshDefinition definition)
	{
		Debug.Assert(definition != null);

		return definition.BuildMesh(new Mesh(), calculateBounds: true);
	}

	public static bool IsEmpty(this IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);

		return mesh.Triangles.Count == 0;
	}

	public static bool IsValid(this IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);

		return mesh.Triangles.Count.IsDivisibleBy(3)
			&& mesh.Vertices.Count <= MeshConstants.MaxVertices
			&& mesh.IsOptionalComponentValid(mesh.Normals)
			&& mesh.IsOptionalComponentValid(mesh.Tangents)
			&& mesh.IsOptionalComponentValid(mesh.Colors)
			&& mesh.IsOptionalComponentValid(mesh.UV)
			&& mesh.Parts == mesh.GetOptionalParts();
	}

	internal static OptionalMeshParts GetOptionalParts(this IMeshDefinition mesh)
	{
		Debug.Assert(mesh != null);

		OptionalMeshParts parts = OptionalMeshParts.None;
		if (mesh.Normals.Count > 0)
		{
			parts |= OptionalMeshParts.Normals;
		}

		if (mesh.Tangents.Count > 0)
		{
			parts |= OptionalMeshParts.Tangents;
		}

		if (mesh.Colors.Count > 0)
		{
			parts |= OptionalMeshParts.Colors;
		}

		if (mesh.UV.Count > 0)
		{
			parts |= OptionalMeshParts.UV;
		}

		return parts;
	}

	private static bool IsOptionalComponentValid<T>(this IMeshDefinition mesh, IReadOnlyList<T> component)
	{
		Debug.Assert(mesh != null);
		Debug.Assert(component != null);

		return component.Count == 0 || component.Count == mesh.Vertices.Count;
	}

	private static T[] CloneOptional<T>(this IReadOnlyList<T> component)
	{
		Debug.Assert(component != null);

		return component.Count == 0 ? null : component.ToArrayExtended();
	}
}
