﻿namespace HQS.Utility.Unity.Meshing;

public static class IMeshPrimitiveContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void TryAddTo(IMeshPrimitive instance, IMeshBuilder builder)
	{
		Debug.Assert(instance != null);
		Debug.Assert(builder != null);
		Debug.Assert(builder.Parts == instance.Parts);
	}
}
