﻿namespace HQS.Utility.Unity.Meshing;

public static class IMeshBuilderExtensions
{
	// percentage of 0 is no resize, percentage of 1 is 100%
	// negative percentage shrinks, positive percentage expands
	public static void Resize(this IMeshBuilder builder, float percentage)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		var vertices = builder.Vertices;
		if (vertices.Count <= 1 || percentage == 0)
		{
			return;
		}

		var center = vertices[0];
		for (int i = 1; i < vertices.Count; i++)
		{
			center += vertices[i];
		}

		center /= vertices.Count;
		for (int i = 0; i < vertices.Count; i++)
		{
			var distance = Vector3.Distance(vertices[i], center) * -percentage;
			vertices[i] = Vector3Utility.MoveTowards(vertices[i], center, distance);
		}
	}
}
