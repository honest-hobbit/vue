﻿namespace HQS.Utility.Unity.Meshing;

public static class IMeshDefinitionContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void BuildMesh(Mesh mesh)
	{
		Debug.Assert(mesh != null);
	}
}
