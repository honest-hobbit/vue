﻿namespace HQS.Utility.Unity.Meshing;

// optional parts not in use will have the List be an empty collection (not null)
public interface IMeshDefinition
{
	OptionalMeshParts Parts { get; }

	IReadOnlyList<int> Triangles { get; }

	IReadOnlyList<Vector3> Vertices { get; }

	IReadOnlyList<Vector3> Normals { get; }

	IReadOnlyList<Vector4> Tangents { get; }

	IReadOnlyList<Color32> Colors { get; }

	IReadOnlyList<Vector2> UV { get; }

	// only call this from Unity's main thread
	Mesh BuildMesh(Mesh mesh, bool calculateBounds);
}
