﻿namespace HQS.Utility.Unity.Meshing;

// optional parts not in use will have the List be null
public interface IMeshBuilder
{
	OptionalMeshParts Parts { get; }

	List<int> Triangles { get; }

	List<Vector3> Vertices { get; }

	List<Vector3> Normals { get; }

	List<Vector4> Tangents { get; }

	List<Color32> Colors { get; }

	List<Vector2> UV { get; }
}
