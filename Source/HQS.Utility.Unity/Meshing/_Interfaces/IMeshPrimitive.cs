﻿namespace HQS.Utility.Unity.Meshing;

public interface IMeshPrimitive
{
	OptionalMeshParts Parts { get; }

	bool TryAddTo(IMeshBuilder builder);
}
