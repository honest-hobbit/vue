﻿namespace HQS.Utility.Unity.Meshing;

public class MeshDefinition : IMeshDefinition
{
	private readonly int[] triangles;

	private readonly Vector3[] vertices;

	private readonly Vector3[] normals;

	private readonly Vector4[] tangents;

	private readonly Color32[] colors;

	private readonly Vector2[] uv;

	public MeshDefinition(
		int[] triangles,
		Vector3[] vertices,
		Vector3[] normals = null,
		Vector4[] tangents = null,
		Color32[] colors = null,
		Vector2[] uv = null)
	{
		Debug.Assert(triangles != null);
		Debug.Assert(vertices != null);
		Debug.Assert(vertices.Length <= MeshConstants.MaxVertices);
		Debug.Assert(normals == null || normals.Length == vertices.Length);
		Debug.Assert(tangents == null || tangents.Length == vertices.Length);
		Debug.Assert(colors == null || colors.Length == vertices.Length);
		Debug.Assert(uv == null || uv.Length == vertices.Length);

		this.triangles = triangles;
		this.vertices = vertices;
		this.normals = normals;
		this.tangents = tangents;
		this.colors = colors;
		this.uv = uv;

		this.Triangles = triangles.AsReadOnlyList();
		this.Vertices = vertices.AsReadOnlyList();
		this.Normals = normals.AsReadOnlyList();
		this.Tangents = tangents.AsReadOnlyList();
		this.Colors = colors.AsReadOnlyList();
		this.UV = uv.AsReadOnlyList();

		this.Parts = this.GetOptionalParts();

		Debug.Assert(this.IsValid());
	}

	public static IMeshDefinition Empty { get; } =
		new MeshDefinition(Array.Empty<int>(), Array.Empty<Vector3>());

	/// <inheritdoc />
	public OptionalMeshParts Parts { get; }

	/// <inheritdoc />
	public IReadOnlyList<int> Triangles { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector3> Vertices { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector3> Normals { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector4> Tangents { get; }

	/// <inheritdoc />
	public IReadOnlyList<Color32> Colors { get; }

	/// <inheritdoc />
	public IReadOnlyList<Vector2> UV { get; }

	/// <inheritdoc />
	public Mesh BuildMesh(Mesh mesh, bool calculateBounds)
	{
		IMeshDefinitionContracts.BuildMesh(mesh);

		// vertices has to be assigned first
		mesh.vertices = this.vertices;

		// optional components
		if (this.normals != null)
		{
			mesh.normals = this.normals;
		}

		if (this.tangents != null)
		{
			mesh.tangents = this.tangents;
		}

		if (this.colors != null)
		{
			mesh.colors32 = this.colors;
		}

		if (this.uv != null)
		{
			mesh.uv = this.uv;
		}

		// triangles must be set last to avoid index out of bounds exceptions
		mesh.SetTriangles(this.triangles, 0, calculateBounds);
		return mesh;
	}

	/// <inheritdoc />
	public override string ToString() => this.ToDebugString();
}
