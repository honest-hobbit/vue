﻿namespace HQS.Utility.Unity.EditorAddons;

public enum ExpandableBackground
{
	None,

	HelpBox,

	Darken,

	Lighten,
}
