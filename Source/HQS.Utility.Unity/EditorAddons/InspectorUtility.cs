﻿#if UNITY_EDITOR

namespace HQS.Utility.Unity.EditorAddons;

public static class InspectorUtility
{
	private static readonly GUIStyle Style = new GUIStyle()
	{
		fontStyle = FontStyle.Bold,
		normal = new GUIStyleState()
		{
			textColor = EditorGUIUtility.isProSkin ? new Color(0.7f, 0.7f, 0.7f) : Color.black,
		},
	};

	public static void LabelHeader(string text, params GUILayoutOption[] options) =>
		EditorGUILayout.LabelField(text, Style, options);

	public static void SelectableTextField(string text) => EditorGUILayout.SelectableLabel(
		text, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));

	public static void SelectableTextFieldWithLabel(string label, string text, float labelScale = 1)
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(label, GUILayout.Width(EditorGUIUtility.labelWidth * labelScale));
		SelectableTextField(text);
		EditorGUILayout.EndHorizontal();
	}

	public static string TextFieldWithLabel(string label, string text, float labelScale = 1)
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(label, GUILayout.Width(EditorGUIUtility.labelWidth * labelScale));
		var result = GUILayout.TextField(text);
		EditorGUILayout.EndHorizontal();
		return result;
	}

	public static bool ToggleWithLabel(string label, bool value, float labelScale = 1)
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(label, GUILayout.Width(EditorGUIUtility.labelWidth * labelScale));
		var result = GUILayout.Toggle(value, string.Empty);
		EditorGUILayout.EndHorizontal();
		return result;
	}
}

#endif
