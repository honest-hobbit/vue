﻿// https://forum.unity.com/threads/editor-tool-better-scriptableobject-inspector-editing.484393/
// latest version taken from Fydar, Feb 2, 2018 and/or laurentlavigne, Mar 22, 2018 (they're identical)

#if UNITY_EDITOR

namespace HQS.Utility.Unity.EditorAddons;

/// <summary>
/// Draws the property field for any field marked with ExpandableAttribute.
/// </summary>
[CustomPropertyDrawer(typeof(ExpandableAttribute), true)]
public class ExpandableAttributeDrawer : PropertyDrawer
{
	/// <summary>
	/// The spacing on the inside of the background rect.
	/// </summary>
	private const float InnerSpacing = 6.0f;

	/// <summary>
	/// The spacing on the outside of the background rect.
	/// </summary>
	private const float OuterSpacing = 4.0f;

	/// <summary>
	/// The colour that is used to darken the background.
	/// </summary>
	private static readonly Color DarkenColor = new Color(0.0f, 0.0f, 0.0f, 0.2f);

	/// <summary>
	/// The colour that is used to lighten the background.
	/// </summary>
	private static readonly Color LightenColor = new Color(1.0f, 1.0f, 1.0f, 0.2f);

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		float totalHeight = EditorGUIUtility.singleLineHeight;

		if (property.objectReferenceValue == null)
		{
			return totalHeight;
		}

		if (!property.isExpanded)
		{
			return totalHeight;
		}

		using var targetObject = new SerializedObject(property.objectReferenceValue);
		////if (targetObject == null)
		////{
		////	return totalHeight;
		////}

		var field = targetObject.GetIterator();

		field.NextVisible(true);

		////if (this.GetAttribute().ShowScriptField)
		////{
		////	totalHeight += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		////}

		while (field.NextVisible(false))
		{
			totalHeight += EditorGUI.GetPropertyHeight(field, true) + EditorGUIUtility.standardVerticalSpacing;
		}

		totalHeight += InnerSpacing * 2;
		totalHeight += OuterSpacing * 2;

		return totalHeight;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var fieldRect = new Rect(position)
		{
			height = EditorGUIUtility.singleLineHeight,
		};

		EditorGUI.PropertyField(fieldRect, property, label, true);

		if (property.objectReferenceValue == null)
		{
			return;
		}

		property.isExpanded = EditorGUI.Foldout(fieldRect, property.isExpanded, GUIContent.none, true);

		if (!property.isExpanded)
		{
			return;
		}

		using var targetObject = new SerializedObject(property.objectReferenceValue);
		////if (targetObject == null)
		////{
		////	return;
		////}

		var propertyRects = new List<Rect>();
		var marchingRect = new Rect(fieldRect);

		var bodyRect = new Rect(fieldRect);
		bodyRect.xMin += EditorGUI.indentLevel * 14;
		bodyRect.yMin += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing + OuterSpacing;

		var field = targetObject.GetIterator();
		field.NextVisible(true);

		marchingRect.y += InnerSpacing + OuterSpacing;

		////if (this.GetAttribute().ShowScriptField)
		////{
		////	propertyRects.Add(marchingRect);
		////	marchingRect.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		////}

		while (field.NextVisible(false))
		{
			marchingRect.y += marchingRect.height + EditorGUIUtility.standardVerticalSpacing;
			marchingRect.height = EditorGUI.GetPropertyHeight(field, true);
			propertyRects.Add(marchingRect);
		}

		marchingRect.y += InnerSpacing;

		bodyRect.yMax = marchingRect.yMax;
		this.DrawBackground(bodyRect);

		////EditorGUI.indentLevel++;

		int index = 0;
		field = targetObject.GetIterator();
		field.NextVisible(true);

		////if (this.GetAttribute().ShowScriptField)
		////{
		////	// Show the disabled script field
		////	EditorGUI.BeginDisabledGroup(true);
		////	EditorGUI.PropertyField(propertyRects[index], field, true);
		////	EditorGUI.EndDisabledGroup();
		////	index++;
		////}

		// Replacement for "editor.OnInspectorGUI ();" so we have more control on how we draw the editor
		while (field.NextVisible(false))
		{
			try
			{
				EditorGUI.PropertyField(propertyRects[index], field, true);
			}
			catch (StackOverflowException)
			{
				field.objectReferenceValue = null;
				UnityEngine.Debug.LogError(
					"Detected self-nesting cauisng a StackOverflowException, avoid using the same object iside a nested structure.");
			}

			index++;
		}

		targetObject.ApplyModifiedProperties();
	}

	/// <summary>
	/// Draws the Background.
	/// </summary>
	/// <param name="rect">The Rect where the background is drawn.</param>
	private void DrawBackground(Rect rect)
	{
		switch (this.GetAttribute().BackgroundStyle)
		{
			case ExpandableBackground.HelpBox:
				EditorGUI.HelpBox(rect, string.Empty, MessageType.None);
				break;

			case ExpandableBackground.Darken:
				EditorGUI.DrawRect(rect, DarkenColor);
				break;

			case ExpandableBackground.Lighten:
				EditorGUI.DrawRect(rect, LightenColor);
				break;
		}
	}

	private ExpandableAttribute GetAttribute() => (ExpandableAttribute)this.attribute;
}

#endif
