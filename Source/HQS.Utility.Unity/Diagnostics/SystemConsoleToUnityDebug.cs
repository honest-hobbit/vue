﻿namespace HQS.Utility.Unity.Diagnostics;

// based on; https://www.jacksondunstan.com/articles/2986
public static class SystemConsoleToUnityDebug
{
	public static void Redirect()
	{
		Console.SetOut(new UnityTextWriter());
	}

	private class UnityTextWriter : TextWriter
	{
		private StringBuilder buffer = new StringBuilder();

		public override Encoding Encoding => Encoding.Default;

		public override void Flush()
		{
			UnityEngine.Debug.Log(this.buffer.ToString());
			this.buffer.Length = 0;
		}

		public override void Write(string value)
		{
			this.buffer.Append(value);
			if (value != null)
			{
				var len = value.Length;
				if (len > 0)
				{
					var lastChar = value[len - 1];
					if (lastChar == '\n')
					{
						this.Flush();
					}
				}
			}
		}

		public override void Write(char value)
		{
			this.buffer.Append(value);
			if (value == '\n')
			{
				this.Flush();
			}
		}

		public override void Write(char[] value, int index, int count)
		{
			this.Write(new string(value, index, count));
		}
	}
}
