﻿namespace HQS.Utility.Unity.Diagnostics;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class FrameCounterComponent : MonoBehaviour
{
	private readonly System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

	[SerializeField]
	private int targetFramerate = 60;

	[SerializeField]
	private KeyCode resetStatsKey = KeyCode.None;

	[SerializeField]
	private KeyCode displayStatsKey = KeyCode.None;

	private long frameCount;

	public void OnValidate()
	{
		this.targetFramerate.ClampLower(0);
	}

	public void Start()
	{
		this.timer.Start();
	}

	public void Update()
	{
		if (Input.GetKeyDown(this.resetStatsKey))
		{
			this.frameCount = 0;
			this.timer.Restart();
		}

		this.frameCount++;

		if (Input.GetKeyDown(this.displayStatsKey))
		{
			long target = (long)Math.Round(this.timer.Elapsed.TotalSeconds * this.targetFramerate);
			long missing = target - this.frameCount;
			float percent = (float)this.frameCount / target;
			UnityEngine.Debug.Log(
				$"Frames: {this.frameCount:n0}   Target: {target:n0}   Missing: {missing:n0}   %: {percent:P2}");
		}
	}
}
