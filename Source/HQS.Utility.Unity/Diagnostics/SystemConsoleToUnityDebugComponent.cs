﻿namespace HQS.Utility.Unity.Diagnostics;

[ExecuteInEditMode]
public class SystemConsoleToUnityDebugComponent : MonoBehaviour
{
	private void Awake()
	{
		SystemConsoleToUnityDebug.Redirect();
	}
}
