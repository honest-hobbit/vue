﻿namespace HQS.Utility.Unity.Diagnostics;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class HudFpsComponent : MonoBehaviour
{
	// Attach this to any object to make a frames/second indicator.
	//
	// It calculates frames/second over each updateInterval,
	// so the display does not keep changing wildly.
	//
	// It is also fairly accurate at very low FPS counts (<10).
	// We do this not by simply counting frames per interval, but
	// by accumulating FPS for each frame. This way we end up with
	// corstartRect overall FPS even if the interval renders something like
	// 5.5 frames.
	[SerializeField]
	private Rect startRect = new Rect(10, 10, 75, 50); // The rect the window is initially displayed at.

	[SerializeField]
	private bool updateColor = true; // Do you want the color to change if the FPS gets low

	[SerializeField]
	private bool allowDrag = true; // Do you want to allow the dragging of the FPS window

	[SerializeField]
	private float frequency = 0.5F; // The update frequency of the fps

	[SerializeField]
	private int decimals = 1; // How many decimal do you want to display

	private float accum = 0f; // FPS accumulated over the interval

	private int frames = 0; // Frames drawn over the interval

	private Color color = Color.white; // The color of the GUI, depending of the FPS ( R < 10, Y < 30, G >= 30 )

	private string sFPS = string.Empty; // The fps formatted into a string.

	private GUIStyle style; // The style the text will be displayed at, based en defaultSkin.label.

	public void Start()
	{
		this.StartCoroutine(this.FPS());
	}

	public void Update()
	{
		this.accum += Time.timeScale / Time.deltaTime;
		this.frames++;
	}

	public void OnGUI()
	{
		// Copy the default label skin, change the color and the alignement
		if (this.style == null)
		{
			this.style = new GUIStyle(GUI.skin.label);
			this.style.normal.textColor = Color.white;
			this.style.alignment = TextAnchor.MiddleCenter;
		}

		GUI.color = this.updateColor ? this.color : Color.white;
		this.startRect = GUI.Window(0, this.startRect, this.DoMyWindow, string.Empty);
	}

	private IEnumerator FPS()
	{
		// Infinite loop executed every "frenquency" secondes.
		while (true)
		{
			// Update the FPS
			float fps = this.accum / this.frames;
			this.sFPS = fps.ToString("f" + Mathf.Clamp(this.decimals, 0, 10));

			// Update the color
			this.color = (fps >= 30) ? Color.green : ((fps > 10) ? Color.red : Color.yellow);

			this.accum = 0.0F;
			this.frames = 0;

			yield return new WaitForSeconds(this.frequency);
		}
	}

	private void DoMyWindow(int windowID)
	{
		GUI.Label(new Rect(0, 0, this.startRect.width, this.startRect.height), this.sFPS + " FPS", this.style);
		if (this.allowDrag)
		{
			GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));
		}
	}
}
