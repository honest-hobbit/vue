﻿namespace HQS.Utility.Unity.Diagnostics;

public static class UnityCompilationSymbol
{
	public const string Assertions = "UNITY_ASSERTIONS";

	public const string Editor = "UNITY_EDITOR";
}
