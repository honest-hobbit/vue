﻿namespace HQS.Utility.Unity.Diagnostics;

[ExecuteInEditMode]
public class DebugAssertExceptionComponent : MonoBehaviour
{
	private void Awake()
	{
		DebugAssertExceptionTraceListener.AddListener();
	}
}
