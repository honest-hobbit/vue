﻿namespace HQS.Utility.Unity.Drawing;

public class Pixels2D : IPixels2D<Color>
{
	public Pixels2D()
	{
	}

	public Pixels2D(PixelIndexer dimensions)
	{
		this.SetDimensions(dimensions);
	}

	/// <inheritdoc />
	public Color[] Pixels { get; private set; }

	/// <inheritdoc />
	public PixelIndexer Dimensions { get; private set; }

	/// <inheritdoc />
	public void SetPixelsArray(Color[] array, PixelIndexer dimensions)
	{
		IPixels2DContracts.SetPixelsArray(array, dimensions);

		this.Pixels = array;
		this.Dimensions = dimensions;
	}

	/// <inheritdoc />
	public void ApplyTo(Texture2D texture, bool updateMipmaps = true)
	{
		IPixels2DContracts.ApplyTo(texture);

		var textureDimensions = new PixelIndexer(texture);
		if (this.Dimensions != textureDimensions)
		{
			texture.Reinitialize(this.Dimensions.Width, this.Dimensions.Height);
		}

		texture.SetPixels(this.Pixels);
		texture.Apply(updateMipmaps);
	}

	/// <inheritdoc />
	public Color GetPixel(Int2 index) => this.Pixels[this.Dimensions[index.X, index.Y]];

	/// <inheritdoc />
	public Color GetPixel(int x, int y) => this.Pixels[this.Dimensions[x, y]];

	/// <inheritdoc />
	public void SetPixel(Int2 index, Color color) => this.Pixels[this.Dimensions[index.X, index.Y]] = color;

	/// <inheritdoc />
	public void SetPixel(int x, int y, Color color) => this.Pixels[this.Dimensions[x, y]] = color;

	/// <inheritdoc />
	public void SetAllPixelsTo(Color color) => this.Pixels.SetAllTo(color);

	/// <inheritdoc />
	public void DrawRectangleFilled(Int2 origin, Int2 dimensions, Color color) =>
		this.Pixels.DrawRectangleFilled(this.Dimensions, origin.X, origin.Y, dimensions.X, dimensions.Y, color);

	/// <inheritdoc />
	public void DrawRectangleFilled(int x, int y, int width, int height, Color color) =>
		this.Pixels.DrawRectangleFilled(this.Dimensions, x, y, width, height, color);

	/// <inheritdoc />
	public void DrawRectangleOutline(Int2 origin, Int2 dimensions, Color color, int thickness = 1) =>
		this.Pixels.DrawRectangleOutline(this.Dimensions, origin.X, origin.Y, dimensions.X, dimensions.Y, color, thickness);

	/// <inheritdoc />
	public void DrawRectangleOutline(int x, int y, int width, int height, Color color, int thickness = 1) =>
		this.Pixels.DrawRectangleOutline(this.Dimensions, x, y, width, height, color, thickness);

	/// <inheritdoc />
	public void DrawRectangle(Int2 origin, Int2 dimensions, Color fill, Color outline, int thickness = 1) =>
		this.Pixels.DrawRectangle(this.Dimensions, origin.X, origin.Y, dimensions.X, dimensions.Y, fill, outline, thickness);

	/// <inheritdoc />
	public void DrawRectangle(int x, int y, int width, int height, Color fill, Color outline, int thickness = 1) =>
		this.Pixels.DrawRectangle(this.Dimensions, x, y, width, height, fill, outline, thickness);
}
