﻿namespace HQS.Utility.Unity.Drawing;

public static class Texture2DExtensions
{
	public static void SetAllPixelsTo(this Texture2D texture, Color color)
	{
		Debug.Assert(texture != null);

		texture.DrawRectangleFilled(0, 0, texture.width, texture.height, color);
	}

	public static void DrawRectangleFilled(
		this Texture2D texture, Int2 origin, Int2 dimensions, Color color) =>
		texture.DrawRectangleFilled(origin.X, origin.Y, dimensions.X, dimensions.Y, color);

	public static void DrawRectangleFilled(
		this Texture2D texture, int x, int y, int width, int height, Color color)
	{
		Debug.Assert(texture != null);
		Debug.Assert(x >= 0);
		Debug.Assert(y >= 0);
		Debug.Assert(x + width - 1 < texture.width);
		Debug.Assert(y + height - 1 < texture.height);

		int xMax = x + width;
		int yMax = y + height;
		if (height >= width)
		{
			for (int iX = x; iX < xMax; iX++)
			{
				for (int iY = y; iY < yMax; iY++)
				{
					texture.SetPixel(iX, iY, color);
				}
			}
		}
		else
		{
			for (int iY = y; iY < yMax; iY++)
			{
				for (int iX = x; iX < xMax; iX++)
				{
					texture.SetPixel(iX, iY, color);
				}
			}
		}
	}

	public static void DrawRectangleOutline(
		this Texture2D texture, Int2 origin, Int2 dimensions, Color color, int thickness = 1) =>
		texture.DrawRectangleOutline(origin.X, origin.Y, dimensions.X, dimensions.Y, color, thickness);

	public static void DrawRectangleOutline(
		this Texture2D texture, int x, int y, int width, int height, Color color, int thickness = 1) =>
		texture.DrawRectangle(x, y, width, height, color, thickness, Color.black, false);

	public static void DrawRectangle(
		this Texture2D texture, Int2 origin, Int2 dimensions, Color fill, Color outline, int thickness = 1) =>
		texture.DrawRectangle(origin.X, origin.Y, dimensions.X, dimensions.Y, fill, outline, thickness);

	public static void DrawRectangle(
		this Texture2D texture, int x, int y, int width, int height, Color fill, Color outline, int thickness = 1) =>
		texture.DrawRectangle(x, y, width, height, outline, thickness, fill, true);

	private static void DrawRectangle(
		this Texture2D texture, int x, int y, int width, int height, Color outline, int thickness, Color fill, bool doFill)
	{
		Debug.Assert(texture != null);
		Debug.Assert(x >= 0);
		Debug.Assert(y >= 0);
		Debug.Assert(x + width - 1 < texture.width);
		Debug.Assert(y + height - 1 < texture.height);
		Debug.Assert(thickness >= 0);

		int doubleThick = thickness * 2;
		if (thickness > 0)
		{
			if (doubleThick >= width || doubleThick >= height)
			{
				texture.DrawRectangleFilled(x, y, width, height, outline);
				return;
			}

			texture.DrawRectangleFilled(x, y, width, thickness, outline);
			texture.DrawRectangleFilled(x, y + height - thickness, width, thickness, outline);

			int sideY = y + thickness;
			int sidesHeight = height - doubleThick;
			texture.DrawRectangleFilled(x, sideY, thickness, sidesHeight, outline);
			texture.DrawRectangleFilled(x + width - thickness, sideY, thickness, sidesHeight, outline);
		}

		if (doFill)
		{
			texture.DrawRectangleFilled(x + thickness, y + thickness, width - doubleThick, height - doubleThick, fill);
		}
	}
}
