﻿namespace HQS.Utility.Unity.Drawing;

public readonly record struct PixelIndexer
{
	public PixelIndexer(Texture2D texture)
	{
		Debug.Assert(texture != null);

		this.Width = texture.width;
		this.Height = texture.height;
	}

	public PixelIndexer(int width, int height)
	{
		Debug.Assert(width >= 0);
		Debug.Assert(height >= 0);

		this.Width = width;
		this.Height = height;
	}

	public int Width { get; }

	public int Height { get; }

	public long ArrayLength => (long)this.Width * this.Height;

	public int this[int x, int y]
	{
		get
		{
			Debug.Assert(x >= 0 && x < this.Width);
			Debug.Assert(y >= 0 && y < this.Height);

			return x + (y * this.Width);
		}
	}

	/// <inheritdoc />
	public override string ToString() => $"[{this.Width}, {this.Height}]";
}
