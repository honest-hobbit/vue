﻿namespace HQS.Utility.Unity.Drawing;

public class Texture2DPixels : Pixels2D, ITexture2DPixels<Color>
{
	public Texture2DPixels(Texture2D texture)
	{
		Debug.Assert(texture != null);
		Debug.Assert(texture.width > 0);
		Debug.Assert(texture.height > 0);

		this.Texture = texture;
		this.GetPixelsFromTexture();
	}

	/// <inheritdoc />
	public Texture2D Texture { get; }

	/// <inheritdoc />
	public void CheckResized()
	{
		var textureDimensions = new PixelIndexer(this.Texture);
		if (this.Dimensions == textureDimensions)
		{
			return;
		}

		long length = textureDimensions.ArrayLength;
		var array = (this.Pixels?.LongLength == length) ? this.Pixels : this.Texture.GetPixels();

		this.SetPixelsArray(array, textureDimensions);
	}

	/// <inheritdoc />
	public void GetPixelsFromTexture()
	{
		this.SetPixelsArray(this.Texture.GetPixels(), new PixelIndexer(this.Texture));
	}

	/// <inheritdoc />
	public void ApplyPixelsToTexture()
	{
		this.ApplyTo(this.Texture, updateMipmaps: false);
	}
}
