﻿namespace HQS.Utility.Unity.Drawing;

public interface ITexture2DPixels<T> : IPixels2D<T>
{
	Texture2D Texture { get; }

	void CheckResized();

	void GetPixelsFromTexture();

	void ApplyPixelsToTexture();
}
