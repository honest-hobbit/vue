﻿namespace HQS.Utility.Unity.Drawing;

internal static class PixelsArrayUtility
{
	public static void DrawRectangleFilled<T>(
		this T[] pixels, PixelIndexer indexer, int x, int y, int width, int height, T color)
	{
		Debug.Assert(pixels != null);
		Debug.Assert(x >= 0);
		Debug.Assert(y >= 0);
		Debug.Assert(x + width - 1 < indexer.Width);
		Debug.Assert(y + height - 1 < indexer.Height);

		int xMax = x + width;
		int yMax = y + height;
		if (height >= width)
		{
			for (int iX = x; iX < xMax; iX++)
			{
				for (int iY = y; iY < yMax; iY++)
				{
					pixels[indexer[iX, iY]] = color;
				}
			}
		}
		else
		{
			for (int iY = y; iY < yMax; iY++)
			{
				for (int iX = x; iX < xMax; iX++)
				{
					pixels[indexer[iX, iY]] = color;
				}
			}
		}
	}

	public static void DrawRectangleOutline<T>(
		this T[] pixels, PixelIndexer indexer, int x, int y, int width, int height, T outline, int thickness)
	{
		Debug.Assert(pixels != null);
		Debug.Assert(x >= 0);
		Debug.Assert(y >= 0);
		Debug.Assert(x + width - 1 < indexer.Width);
		Debug.Assert(y + height - 1 < indexer.Height);
		Debug.Assert(thickness >= 0);

		if (thickness == 0)
		{
			return;
		}

		int doubleThick = thickness * 2;
		if (doubleThick >= width || doubleThick >= height)
		{
			pixels.DrawRectangleFilled(indexer, x, y, width, height, outline);
			return;
		}

		pixels.DrawRectangleFilled(indexer, x, y, width, thickness, outline);
		pixels.DrawRectangleFilled(indexer, x, y + height - thickness, width, thickness, outline);

		int sideY = y + thickness;
		int sidesHeight = height - doubleThick;
		pixels.DrawRectangleFilled(indexer, x, sideY, thickness, sidesHeight, outline);
		pixels.DrawRectangleFilled(indexer, x + width - thickness, sideY, thickness, sidesHeight, outline);
	}

	public static void DrawRectangle<T>(
		this T[] pixels, PixelIndexer indexer, int x, int y, int width, int height, T fill, T outline, int thickness)
	{
		if (thickness > 0)
		{
			pixels.DrawRectangleOutline(indexer, x, y, width, height, outline, thickness);
			int doubleThick = thickness * 2;
			pixels.DrawRectangleFilled(indexer, x + thickness, y + thickness, width - doubleThick, height - doubleThick, fill);
		}
		else
		{
			pixels.DrawRectangleFilled(indexer, x, y, width, height, fill);
		}
	}
}
