﻿namespace HQS.Utility.Unity.Drawing;

public static class IPixels2DExtensions
{
	public static void SetPixelsArray<T>(this IPixels2D<T> pixels, T[] array, int width, int height)
	{
		Debug.Assert(pixels != null);

		pixels.SetPixelsArray(array, new PixelIndexer(width, height));
	}

	public static void SetDimensions<T>(this IPixels2D<T> pixels, PixelIndexer dimensions)
	{
		Debug.Assert(pixels != null);

		long length = dimensions.ArrayLength;
		var array = (pixels.Pixels?.LongLength == length) ? pixels.Pixels : new T[length];

		pixels.SetPixelsArray(array, dimensions);
	}

	public static void SetDimensions<T>(this IPixels2D<T> pixels, int width, int height)
	{
		Debug.Assert(pixels != null);

		pixels.SetDimensions(new PixelIndexer(width, height));
	}
}
