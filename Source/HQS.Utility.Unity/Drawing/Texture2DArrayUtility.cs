﻿namespace HQS.Utility.Unity.Drawing;

public static class Texture2DArrayUtility
{
	public static Texture2DArray CreateTextureArray(params Texture2D[] textures) =>
		CreateTextureArray((IReadOnlyList<Texture2D>)textures);

	public static Texture2DArray CreateTextureArray(IReadOnlyList<Texture2D> textures)
	{
		Ensure.That(textures, nameof(textures)).IsNotNull();
		Ensure.That(textures.Count, $"{nameof(textures)}.Count").IsGte(1);

		var texture = textures[0];
		int width = texture.width;
		int height = texture.height;
		int max = textures.Count;

		for (int i = 1; i < max; i++)
		{
			texture = textures[i];
			Ensure.That(
				texture.width,
				$"{nameof(textures)}[{i}].{nameof(Texture2D.width)}",
				opt => opt.WithMessage($"All textures must have the same {nameof(Texture2D.width)}.")).Is(width);
			Ensure.That(
				texture.height,
				$"{nameof(textures)}[{i}].{nameof(Texture2D.height)}",
				opt => opt.WithMessage($"All textures must have the same {nameof(Texture2D.height)}.")).Is(height);
		}

		var textureArray = new Texture2DArray(
			width, height, max, TextureFormat.RGBA32, true, false)
		{
			filterMode = FilterMode.Bilinear,
			wrapMode = TextureWrapMode.Repeat,
		};

		for (int i = 0; i < max; i++)
		{
			textureArray.SetPixels32(textures[i].GetPixels32(0), i, 0);
		}

		textureArray.Apply();
		return textureArray;
	}
}
