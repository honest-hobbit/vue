﻿namespace HQS.Utility.Unity.Drawing;

public static class IPixels2DContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void SetPixelsArray<T>(T[] array, PixelIndexer dimensions)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.LongLength == dimensions.ArrayLength);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void ApplyTo(Texture2D texture)
	{
		Debug.Assert(texture != null);
	}
}
