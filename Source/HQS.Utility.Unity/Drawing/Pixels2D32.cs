﻿namespace HQS.Utility.Unity.Drawing;

public class Pixels2D32 : IPixels2D<Color32>
{
	public Pixels2D32()
	{
	}

	public Pixels2D32(PixelIndexer dimensions)
	{
		this.SetDimensions(dimensions);
	}

	/// <inheritdoc />
	public Color32[] Pixels { get; private set; }

	/// <inheritdoc />
	public PixelIndexer Dimensions { get; private set; }

	/// <inheritdoc />
	public void SetPixelsArray(Color32[] array, PixelIndexer dimensions)
	{
		IPixels2DContracts.SetPixelsArray(array, dimensions);

		this.Pixels = array;
		this.Dimensions = dimensions;
	}

	/// <inheritdoc />
	public void ApplyTo(Texture2D texture, bool updateMipmaps = true)
	{
		IPixels2DContracts.ApplyTo(texture);

		var textureDimensions = new PixelIndexer(texture);
		if (this.Dimensions != textureDimensions)
		{
			texture.Reinitialize(this.Dimensions.Width, this.Dimensions.Height);
		}

		texture.SetPixels32(this.Pixels);
		texture.Apply(updateMipmaps);
	}

	/// <inheritdoc />
	public Color32 GetPixel(Int2 index) => this.Pixels[this.Dimensions[index.X, index.Y]];

	/// <inheritdoc />
	public Color32 GetPixel(int x, int y) => this.Pixels[this.Dimensions[x, y]];

	/// <inheritdoc />
	public void SetPixel(Int2 index, Color32 color) => this.Pixels[this.Dimensions[index.X, index.Y]] = color;

	/// <inheritdoc />
	public void SetPixel(int x, int y, Color32 color) => this.Pixels[this.Dimensions[x, y]] = color;

	/// <inheritdoc />
	public void SetAllPixelsTo(Color32 color) => this.Pixels.SetAllTo(color);

	/// <inheritdoc />
	public void DrawRectangleFilled(Int2 origin, Int2 dimensions, Color32 color) =>
		this.Pixels.DrawRectangleFilled(this.Dimensions, origin.X, origin.Y, dimensions.X, dimensions.Y, color);

	/// <inheritdoc />
	public void DrawRectangleFilled(int x, int y, int width, int height, Color32 color) =>
		this.Pixels.DrawRectangleFilled(this.Dimensions, x, y, width, height, color);

	/// <inheritdoc />
	public void DrawRectangleOutline(Int2 origin, Int2 dimensions, Color32 color, int thickness = 1) =>
		this.Pixels.DrawRectangleOutline(this.Dimensions, origin.X, origin.Y, dimensions.X, dimensions.Y, color, thickness);

	/// <inheritdoc />
	public void DrawRectangleOutline(int x, int y, int width, int height, Color32 color, int thickness = 1) =>
		this.Pixels.DrawRectangleOutline(this.Dimensions, x, y, width, height, color, thickness);

	/// <inheritdoc />
	public void DrawRectangle(Int2 origin, Int2 dimensions, Color32 fill, Color32 outline, int thickness = 1) =>
		this.Pixels.DrawRectangle(this.Dimensions, origin.X, origin.Y, dimensions.X, dimensions.Y, fill, outline, thickness);

	/// <inheritdoc />
	public void DrawRectangle(int x, int y, int width, int height, Color32 fill, Color32 outline, int thickness = 1) =>
		this.Pixels.DrawRectangle(this.Dimensions, x, y, width, height, fill, outline, thickness);
}
