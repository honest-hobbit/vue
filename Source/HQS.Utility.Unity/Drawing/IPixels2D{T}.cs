﻿namespace HQS.Utility.Unity.Drawing;

public interface IPixels2D<T>
{
	T[] Pixels { get; }

	PixelIndexer Dimensions { get; }

	void SetPixelsArray(T[] array, PixelIndexer dimensions);

	void ApplyTo(Texture2D texture, bool updateMipmaps = true);

	T GetPixel(Int2 index);

	T GetPixel(int x, int y);

	void SetPixel(Int2 index, T color);

	void SetPixel(int x, int y, T color);

	void SetAllPixelsTo(T color);

	void DrawRectangleFilled(Int2 origin, Int2 dimensions, T color);

	void DrawRectangleFilled(int x, int y, int width, int height, T color);

	void DrawRectangleOutline(Int2 origin, Int2 dimensions, T color, int thickness = 1);

	void DrawRectangleOutline(int x, int y, int width, int height, T color, int thickness = 1);

	void DrawRectangle(Int2 origin, Int2 dimensions, T fill, T outline, int thickness = 1);

	void DrawRectangle(int x, int y, int width, int height, T fill, T outline, int thickness = 1);
}
