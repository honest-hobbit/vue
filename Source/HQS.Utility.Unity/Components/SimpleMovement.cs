﻿namespace HQS.Utility.Unity.Components;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class SimpleMovement : MonoBehaviour
{
	[SerializeField]
	private GameObject cameraObject = null;

	[SerializeField]
	private float moveSpeed = 10;

	[SerializeField]
	private KeyCode up = KeyCode.R;

	[SerializeField]
	private KeyCode down = KeyCode.F;

	private new Transform transform;

	public void Start()
	{
		this.transform = this.GetComponent<Transform>();
	}

	public void Update()
	{
		var offset = this.GetInputDirection();

		if (Input.GetKey(this.up))
		{
			offset += this.transform.up * this.moveSpeed;
		}
		else if (Input.GetKey(this.down))
		{
			offset -= this.transform.up * this.moveSpeed;
		}

		this.transform.position += offset * Time.deltaTime;
	}

	private Vector3 GetInputDirection()
	{
		// get axis derived from camera but aligned with floor plane
		var forward = this.cameraObject.transform.TransformDirection(Vector3.forward);
		forward.y = 0;
		forward = forward.normalized;

		var direction = Vector3.zero;
		if (Input.GetButton("Vertical"))
		{
			direction += Input.GetAxis("Vertical") * forward;
		}

		if (Input.GetButton("Horizontal"))
		{
			direction += Input.GetAxis("Horizontal") * new Vector3(forward.z, 0, -forward.x);
		}

		return direction.normalized * this.moveSpeed;
	}
}
