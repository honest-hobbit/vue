﻿namespace HQS.Utility.Unity.Components;

public static class Singleton
{
	public static T Of<T>()
		where T : Component
	{
		return SingletonComponent.Instance.Of<T>();
	}

	public static T Of<T>(Func<T, bool> where)
		where T : Component
	{
		return SingletonComponent.Instance.Of(where);
	}

	public static T Of<T>(Func<T, bool> where, Action<T> initialize)
		where T : Component
	{
		return SingletonComponent.Instance.Of(where, initialize);
	}
}
