﻿namespace HQS.Utility.Unity.Components;

public class ExceptionReporter : MonoBehaviour
{
	static ExceptionReporter()
	{
		AppDomain.CurrentDomain.UnhandledException += (sender, args) => UnityEngine.Debug.LogError(args.ExceptionObject);
	}
}
