﻿namespace HQS.Utility.Unity.Components;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class TagComponent : MonoBehaviour
{
	public string[] Tags;

	public void AddTag(string tag)
	{
		Ensure.That(tag, nameof(tag)).IsNotNullOrWhiteSpace();

		if (this.Tags == null)
		{
			this.Tags = new string[] { tag };
		}
		else
		{
			var temp = new string[this.Tags.Length + 1];
			Array.Copy(this.Tags, temp, this.Tags.Length);
			temp[^1] = tag;
			this.Tags = temp;
		}
	}
}
