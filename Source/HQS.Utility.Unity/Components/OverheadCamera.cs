﻿namespace HQS.Utility.Unity.Components;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class OverheadCamera : MonoBehaviour
{
	public enum Projection
	{
		Overhead,
		SideScrolling,
		Both,
	}

	[SerializeField]
	private string virtualButtonToggleProjection = "Toggle Camera Projection";

	[SerializeField]
	private string virtualAxisRotateCamera = "Rotate Camera";

	[SerializeField]
	private string virtualAxisZoomCamera = "Mouse ScrollWheel";

	[SerializeField]
	private GameObject follow = null;

	[SerializeField]
	private bool initiallyOverhead = true;

	[SerializeField]
	private Projection projection = Projection.Both;

	[SerializeField]
	private float cameraDistance = 20;

	[SerializeField]
	private float initialZoomDistance = 10;

	[SerializeField]
	private float minZoomDistance = 1;

	[SerializeField]
	private float maxZoomDistance = 100;

	[SerializeField]
	private float zoomAmount = 1;

	[SerializeField]
	private float zoomEasingSpeed = 1;

	[Range(0, 360)]
	[SerializeField]
	private float initialRotation = 0;

	[Range(0, 360)]
	[SerializeField]
	private float rotateAmount = 45;

	[SerializeField]
	private float rotateEasingSpeed = 1;

	[SerializeField]
	private float toggleVerticalSpeed = 1;

	private new Camera camera;

	private bool overhead;

	private float height;

	private float targetZoom;

	private float rotation;

	private float targetRotation;

	public void OnValidate()
	{
		if (this.projection == Projection.Overhead)
		{
			this.initiallyOverhead = true;
		}

		if (this.projection == Projection.SideScrolling)
		{
			this.initiallyOverhead = false;
		}

		if (this.cameraDistance <= 0)
		{
			this.cameraDistance = 1;
		}

		if (this.minZoomDistance <= 0)
		{
			this.minZoomDistance = 1;
		}

		if (this.maxZoomDistance < this.minZoomDistance)
		{
			this.maxZoomDistance = this.minZoomDistance;
		}

		if (this.zoomAmount <= 0)
		{
			this.zoomAmount = 1;
		}

		if (this.zoomEasingSpeed <= 0)
		{
			this.zoomEasingSpeed = 1;
		}

		if (this.rotateEasingSpeed <= 0)
		{
			this.rotateEasingSpeed = 1;
		}

		if (this.toggleVerticalSpeed <= 0)
		{
			this.toggleVerticalSpeed = 1;
		}
	}

	public void Start()
	{
		this.targetRotation = this.initialRotation;
		this.overhead = this.initiallyOverhead;
		this.height = this.GetTargetHeight();

		this.initialZoomDistance = this.initialZoomDistance.Clamp(this.minZoomDistance, this.maxZoomDistance);
		this.targetZoom = this.initialZoomDistance;

		this.camera = this.GetComponent<Camera>();
		if (this.camera.orthographic)
		{
			this.camera.orthographicSize = this.initialZoomDistance;
		}
		else
		{
			this.cameraDistance = this.targetZoom;
		}
	}

	public void LateUpdate()
	{
		if (this.projection == Projection.Both && !this.virtualButtonToggleProjection.IsNullOrEmpty() && Input.GetButtonDown(this.virtualButtonToggleProjection))
		{
			this.overhead = !this.overhead;
		}

		if (!this.virtualAxisRotateCamera.IsNullOrEmpty() && Input.GetButtonDown(this.virtualAxisRotateCamera))
		{
			this.targetRotation += this.rotateAmount * Input.GetAxis(this.virtualAxisRotateCamera);
		}

		if (this.zoomAmount > 0 && !this.virtualAxisZoomCamera.IsNullOrEmpty())
		{
			this.targetZoom += -Input.GetAxis(this.virtualAxisZoomCamera) * this.zoomAmount;
			this.targetZoom = this.targetZoom.Clamp(this.minZoomDistance, this.maxZoomDistance);

			if (this.camera.orthographic)
			{
				this.camera.orthographicSize = Mathf.Lerp(this.camera.orthographicSize, this.targetZoom, Time.deltaTime * this.zoomEasingSpeed);
			}
			else
			{
				this.cameraDistance = Mathf.Lerp(this.cameraDistance, this.targetZoom, Time.deltaTime * this.zoomEasingSpeed);
			}
		}

		this.height = Mathf.LerpAngle(this.height, this.GetTargetHeight(), Time.deltaTime * this.toggleVerticalSpeed);
		this.rotation = Mathf.LerpAngle(this.rotation, this.targetRotation, Time.deltaTime * this.rotateEasingSpeed);

		var radians = this.rotation * Mathf.Deg2Rad;
		var cameraDirection = new Vector3(Mathf.Cos(radians), this.height, Mathf.Sin(radians));

		this.transform.position = this.follow.transform.position + (cameraDirection * this.cameraDistance);
		this.transform.LookAt(this.follow.transform);
	}

	private float GetTargetHeight() => this.overhead ? 1 : 0;
}
