﻿namespace HQS.Utility.Unity.Components;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class ToggleObjects : MonoBehaviour
{
	[SerializeField]
	private KeyCode toggleKey = KeyCode.None;

	[SerializeField]
	private List<GameObject> objects = null;

	[SerializeField]
	private List<Behaviour> components = null;

	public void Update()
	{
		if (Input.GetKeyDown(this.toggleKey) && this.objects != null)
		{
			foreach (var gameObject in this.objects)
			{
				gameObject.ToggleActive();
			}

			foreach (var component in this.components)
			{
				component.ToggleEnabled();
			}
		}
	}
}
