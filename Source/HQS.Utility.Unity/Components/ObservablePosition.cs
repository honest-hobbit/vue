﻿namespace HQS.Utility.Unity.Components;

[SuppressMessage("Design", "CA1001", Justification = "OnDestroy handles disposal.")]
public class ObservablePosition : MonoBehaviour
{
	private readonly ReplaySubject<Vector3> positionChanged = new ReplaySubject<Vector3>(1);

	public ObservablePosition()
	{
		this.PositionChanged = this.positionChanged.DistinctUntilChanged(Vector3Equality.Comparer);
	}

	public IObservable<Vector3> PositionChanged { get; }

	public virtual void Update() => this.positionChanged.OnNext(this.transform.position);

	protected virtual void OnDestroy()
	{
		this.positionChanged.OnCompleted();
		this.positionChanged.Dispose();
	}
}
