﻿namespace HQS.Utility.Unity.Components;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
[RequireComponent(typeof(CharacterController))]
public class OverheadController : MonoBehaviour
{
	[SerializeField]
	private GameObject cameraObject = null;

	[SerializeField]
	private float speed = 6;

	[SerializeField]
	private float sprintMultiplier = 1.5f;

	[SerializeField]
	private float sprintJumpMultiplier = 1.5f;

	[SerializeField]
	private float jumpSpeed = 8;

	[SerializeField]
	private float midAirControl = 1;

	[SerializeField]
	private float gravity = 20;

	[SerializeField]
	private float maxSpeed = 100;

	private CharacterController controller;

	private float verticalSpeed = 0;

	private Vector3 fallMovement = Vector3.zero;

	private float maxFallSpeed = 0;

	public void Start()
	{
		this.controller = this.GetComponent<CharacterController>();

		if (Physics.Raycast(this.transform.position, Vector3.down, out var hit))
		{
			this.controller.Move(hit.point - this.transform.position);
		}
	}

	public void Update()
	{
		var moveBy = this.GetInputDirection();
		if (moveBy != Vector3.zero)
		{
			this.transform.rotation = Quaternion.LookRotation(moveBy);
		}

		if (this.controller.isGrounded)
		{
			if (Input.GetButton("Sprint"))
			{
				moveBy *= this.sprintMultiplier;
			}

			this.fallMovement = moveBy;
			this.maxFallSpeed = moveBy.magnitude.ClampLower(this.speed * this.midAirControl);

			this.verticalSpeed = 0;
			if (Input.GetButtonDown("Jump"))
			{
				var jumpForce = this.jumpSpeed;
				if (Input.GetButton("Sprint"))
				{
					jumpForce *= this.sprintJumpMultiplier;
				}

				this.verticalSpeed = jumpForce;
			}
		}
		else
		{
			moveBy = Vector3.ClampMagnitude((moveBy * this.midAirControl) + this.fallMovement, this.maxFallSpeed);
		}

		this.verticalSpeed -= this.gravity * Time.deltaTime;
		this.verticalSpeed = this.verticalSpeed.Clamp(-this.maxSpeed, this.maxSpeed);
		moveBy += new Vector3(0, this.verticalSpeed, 0);

		moveBy = Vector3.ClampMagnitude(moveBy, this.maxSpeed);
		this.controller.Move(moveBy * Time.deltaTime);
	}

	private Vector3 GetInputDirection()
	{
		// get axis derived from camera but aligned with floor plane
		var forward = this.cameraObject.transform.TransformDirection(Vector3.forward);
		forward.y = 0;
		forward = forward.normalized;

		var direction = Vector3.zero;
		if (Input.GetButton("Vertical"))
		{
			direction += Input.GetAxis("Vertical") * forward;
		}

		if (Input.GetButton("Horizontal"))
		{
			direction += Input.GetAxis("Horizontal") * new Vector3(forward.z, 0, -forward.x);
		}

		return direction.normalized * this.speed;
	}
}
