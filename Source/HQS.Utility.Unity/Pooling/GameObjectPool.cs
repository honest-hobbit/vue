﻿namespace HQS.Utility.Unity.Pooling;

public static class GameObjectPool
{
	public static IPool<GameObject> Create(
		GameObject prefab = null,
		Action<GameObject> prefabCloned = null,
		PoolOptions<GameObject> options = default) =>
		new Pool<GameObject>(CreateFactory(prefab, prefabCloned), CreateOptions(options));

	public static IPool<T> Create<T>(
		T prefab = null,
		Action<T> prefabCloned = null,
		PoolOptions<T> options = default)
		where T : Component =>
		new Pool<T>(CreateFactory(prefab, prefabCloned), CreateOptions(options));

	public static class ThreadSafe
	{
		public static IPool<GameObject> Create(
			GameObject prefab = null,
			Action<GameObject> prefabCloned = null,
			PoolOptions<GameObject> options = default) =>
			new ThreadSafePool<GameObject>(CreateFactory(prefab, prefabCloned), CreateOptions(options));

		public static IPool<T> Create<T>(
			T prefab = null,
			Action<T> prefabCloned = null,
			PoolOptions<T> options = default)
			where T : Component =>
			new ThreadSafePool<T>(CreateFactory(prefab, prefabCloned), CreateOptions(options));

		public static class Pins
		{
			public static IPinPool<GameObject> Create(
				GameObject prefab = null,
				Action<GameObject> prefabCloned = null,
				PoolOptions<GameObject> options = default) =>
				new PinPool<GameObject>(CreateFactory(prefab, prefabCloned), CreateOptions(options));

			public static IPinPool<T> Create<T>(
				T prefab = null,
				Action<T> prefabCloned = null,
				PoolOptions<T> options = default)
				where T : Component =>
				new PinPool<T>(CreateFactory(prefab, prefabCloned), CreateOptions(options));
		}
	}

	[MethodImpl(MethodImplOptions.NoInlining)]
	private static Func<GameObject> CreateFactory(GameObject prefab, Action<GameObject> prefabCloned)
	{
		prefab ??= new GameObject();
		Deactivate(prefab);

		return () =>
		{
			var clone = UnityEngine.Object.Instantiate(prefab);
			Deactivate(clone);
			prefabCloned?.Invoke(clone);
			return clone;
		};
	}

	[MethodImpl(MethodImplOptions.NoInlining)]
	private static Func<T> CreateFactory<T>(T prefab, Action<T> prefabCloned)
		where T : Component
	{
		prefab ??= new GameObject().AddComponent<T>();
		Deactivate(prefab.gameObject);

		return () =>
		{
			var clone = UnityEngine.Object.Instantiate(prefab);
			Deactivate(clone.gameObject);
			prefabCloned?.Invoke(clone);
			return clone;
		};
	}

	private static PoolOptions<GameObject> CreateOptions(PoolOptions<GameObject> options) =>
		new PoolOptions<GameObject>()
		{
			Initialize = x =>
			{
				Initialize(x);
				options.Initialize?.Invoke(x);
			},
			Deinitialize = x =>
			{
				options.Deinitialize?.Invoke(x);
				Deinitialize(x);
			},
			Release = options.Release ?? ((GameObject x) => x.DestroySelf()),
			BoundedCapacity = options.BoundedCapacity,
			DuplicateComparer = options.DuplicateComparer ?? EqualityComparer.ByIdentity<GameObject>(),
		};

	private static PoolOptions<T> CreateOptions<T>(PoolOptions<T> options)
		where T : Component
	{
		options = PoolOptions.WithInterfaces(options);
		return new PoolOptions<T>()
		{
			Initialize = x =>
			{
				Initialize(x.gameObject);
				options.Initialize?.Invoke(x);
			},
			Deinitialize = x =>
			{
				options.Deinitialize?.Invoke(x);
				Deinitialize(x.gameObject);
			},
			Release = options.Release ?? ((T x) => x.gameObject.DestroySelf()),
			BoundedCapacity = options.BoundedCapacity,
			DuplicateComparer = options.DuplicateComparer ?? EqualityComparer.ByIdentity<T>(),
		};
	}

	private static void Deactivate(GameObject prefab)
	{
		Debug.Assert(prefab != null);

		prefab.SetActive(false);
		prefab.hideFlags = HideFlags.HideInHierarchy;
	}

	private static void Initialize(GameObject x)
	{
		Debug.Assert(x != null);

		x.hideFlags = HideFlags.None;
	}

	private static void Deinitialize(GameObject x)
	{
		Debug.Assert(x != null);

		x.SetActive(false);
		x.transform.SetParent(null, false);
		x.transform.position = Vector3.zero;
		x.transform.localScale = Vector3.one;
		x.transform.rotation = Quaternion.identity;
		x.hideFlags = HideFlags.HideInHierarchy;
	}
}
