﻿namespace HQS.Utility.Unity;

public class Vector3Equality : EqualityComparer<Vector3>
{
	private Vector3Equality()
	{
	}

	public static EqualityComparer<Vector3> Comparer { get; } = new Vector3Equality();

	/// <inheritdoc />
	public override bool Equals(Vector3 a, Vector3 b) => a.x == b.x && a.y == b.y && a.z == b.z;

	/// <inheritdoc />
	public override int GetHashCode(Vector3 obj) => obj.GetHashCode();
}
