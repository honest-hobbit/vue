﻿#if UNITY_EDITOR

namespace HQS.Utility.Unity;

// original source: https://stackoverflow.com/questions/54748945/unity-custom-drawing-of-a-struct-in-the-inspector
[CustomPropertyDrawer(typeof(Int4))]
public class Int4Drawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUIUtility.singleLineHeight * (EditorGUIUtility.wideMode ? 1 : 2);
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		// Find the SerializedProperties by name
		var x = property.FindPropertyRelative(nameof(Int4.X));
		var y = property.FindPropertyRelative(nameof(Int4.Y));
		var z = property.FindPropertyRelative(nameof(Int4.Z));
		var w = property.FindPropertyRelative(nameof(Int4.W));

		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		// TODO There is no Vector4IntField so Vector4Field is the closest convenient option
		// but EditorGUILayout.IntField could be used instead
		var result = EditorGUI.Vector4Field(position, label, new Vector4(x.intValue, y.intValue, z.intValue, w.intValue));
		x.intValue = (int)result.x;
		y.intValue = (int)result.y;
		z.intValue = (int)result.z;
		w.intValue = (int)result.w;

		EditorGUI.EndProperty();
	}
}

#endif
