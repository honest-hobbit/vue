﻿#if UNITY_EDITOR

namespace HQS.Utility.Unity;

// original source: https://stackoverflow.com/questions/54748945/unity-custom-drawing-of-a-struct-in-the-inspector
[CustomPropertyDrawer(typeof(Int2))]
public class Int2Drawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUIUtility.singleLineHeight * (EditorGUIUtility.wideMode ? 1 : 2);
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		// Find the SerializedProperties by name
		var x = property.FindPropertyRelative(nameof(Int2.X));
		var y = property.FindPropertyRelative(nameof(Int2.Y));

		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		var result = EditorGUI.Vector2IntField(position, label, new Vector2Int(x.intValue, y.intValue));
		x.intValue = result.x;
		y.intValue = result.y;

		EditorGUI.EndProperty();
	}
}

#endif
