﻿namespace HQS.Utility.Unity;

public static class VectorExtensions
{
	public static Vector2 Abs(this Vector2 vec) => new Vector2(vec.x.Abs(), vec.y.Abs());

	public static Vector3 Abs(this Vector3 vec) => new Vector3(vec.x.Abs(), vec.y.Abs(), vec.z.Abs());

	public static Vector4 Abs(this Vector4 vec) => new Vector4(vec.x.Abs(), vec.y.Abs(), vec.z.Abs(), vec.w.Abs());

	public static Vector2 ToUnityVector(this Int2 index) => new Vector2(index.X, index.Y);

	public static Vector3 ToUnityVector(this Int3 index) => new Vector3(index.X, index.Y, index.Z);

	public static Vector4 ToUnityVector(this Int4 index) => new Vector4(index.X, index.Y, index.Z, index.W);

	public static Vector2 ToUnityVector2(this float value) => new Vector2(value, value);

	public static Vector3 ToUnityVector3(this float value) => new Vector3(value, value, value);

	public static Vector4 ToUnityVector4(this float value) => new Vector4(value, value, value, value);
}
