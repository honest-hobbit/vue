﻿namespace HQS.Utility.Unity;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
public record struct ColorHSV
{
	public float H;

	public float S;

	public float V;

	public float A;

	public ColorHSV(float h, float s, float v)
		: this(h, s, v, 1f)
	{
	}

	public ColorHSV(float h, float s, float v, float a)
	{
		this.H = h;
		this.S = s;
		this.V = v;
		this.A = a;
	}

	public static implicit operator ColorHSV(Color color) => color.ToHSV();

	public static implicit operator ColorHSV(Color32 color) => color.ToHSV();

	public static implicit operator Color(ColorHSV color) => color.ToRGB();

	public static implicit operator Color32(ColorHSV color) => color.ToRGB32();

	public Color32 ToRGB32()
	{
		var color = Color.HSVToRGB(this.H, this.S, this.V, false);
		color.a = this.A;
		return color;
	}

	public Color ToRGB()
	{
		var color = Color.HSVToRGB(this.H, this.S, this.V, false);
		color.a = this.A;
		return color;
	}

	public Color ToRGB(bool hdr)
	{
		var color = Color.HSVToRGB(this.H, this.S, this.V, hdr);
		color.a = this.A;
		return color;
	}

	public ColorHSV Clamp() => new ColorHSV(
		this.H.Clamp(0, 1), this.S.Clamp(0, 1), this.V.Clamp(0, 1), this.A.Clamp(0, 1));

	public ColorHSV AdjustHue(float hue)
	{
		var result = this.Clamp();
		result.H = Adjust(result.H, hue);
		return result;
	}

	public ColorHSV AdjustSaturation(float saturation)
	{
		var result = this.Clamp();
		result.S = Adjust(result.S, saturation);
		return result;
	}

	public ColorHSV AdjustValue(float value)
	{
		var result = this.Clamp();
		result.V = Adjust(result.V, value);
		return result;
	}

	public ColorHSV AdjustAlpha(float alpha)
	{
		var result = this.Clamp();
		result.A = Adjust(result.A, alpha);
		return result;
	}

	public ColorHSV AdjustShade(float amount) => this.AdjustShade(amount, amount);

	public ColorHSV AdjustShade(float saturation, float value)
	{
		var result = this.Clamp();
		result.S = Adjust(result.S, saturation);
		result.V = Adjust(result.V, value);
		return result;
	}

	public ColorHSV AdjustTint(float hue = 0, float saturation = 0, float value = 0)
	{
		var result = this.Clamp();
		result.H = Adjust(result.H, hue);
		result.S = Adjust(result.S, saturation);
		result.V = Adjust(result.V, value);
		return result;
	}

	public ColorHSV AdjustColor(float hue = 0, float saturation = 0, float value = 0, float alpha = 0)
	{
		var result = this.Clamp();
		result.H = Adjust(result.H, hue);
		result.S = Adjust(result.S, saturation);
		result.V = Adjust(result.V, value);
		result.A = Adjust(result.A, alpha);
		return result;
	}

	/// <inheritdoc />
	public override string ToString() => $"HSVA[{this.H:F3}, {this.S:F3}, {this.V:F3}, {this.A:F3}]";

	private static float Adjust(float value, float adjustment)
	{
		Debug.Assert(value >= 0 && value <= 1);
		Debug.Assert(adjustment >= -1 && adjustment <= 1);

		if (adjustment > 0)
		{
			return (value + ((1 - value) * adjustment)).ClampUpper(1);
		}
		else if (adjustment < 0)
		{
			return (value + (value * adjustment)).ClampLower(0);
		}
		else
		{
			return value;
		}
	}
}
