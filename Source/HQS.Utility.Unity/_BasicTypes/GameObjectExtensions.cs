﻿namespace HQS.Utility.Unity;

public static class GameObjectExtensions
{
	public static T GetOrAddComponent<T>(this GameObject gameObject)
		where T : Component
	{
		Debug.Assert(gameObject != null);

		var component = gameObject.GetComponent<T>();
		if (component == null)
		{
			component = gameObject.AddComponent<T>();
		}

		return component;
	}

	public static void DestroyComponent<T>(this GameObject gameObject)
		where T : Component
	{
		Debug.Assert(gameObject != null);

		var component = gameObject.GetComponent<T>();
		if (component != null)
		{
			UnityEngine.Object.Destroy(component);
		}
	}

	public static void DestroySelf(this GameObject gameObject)
	{
		Debug.Assert(gameObject != null);

		UnityEngine.Object.Destroy(gameObject);
	}

	public static void DestroySelf<T>(this T component)
		where T : Component
	{
		Debug.Assert(component != null);

		UnityEngine.Object.Destroy(component);
	}

	public static void ToggleActive(this GameObject gameObject)
	{
		Debug.Assert(gameObject != null);

		gameObject.SetActive(!gameObject.activeSelf);
	}
}
