﻿namespace HQS.Utility.Unity;

public static class MouseButton
{
	public const int Primary = 0;

	public const int Secondary = 1;

	public const int Tertiary = 2;

	public const int Left = Primary;

	public const int Right = Secondary;

	public const int Middle = Tertiary;
}
