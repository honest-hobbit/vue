﻿namespace HQS.Utility.Unity;

public class Vector2Equality : EqualityComparer<Vector2>
{
	private Vector2Equality()
	{
	}

	public static EqualityComparer<Vector2> Comparer { get; } = new Vector2Equality();

	/// <inheritdoc />
	public override bool Equals(Vector2 a, Vector2 b) => a.x == b.x && a.y == b.y;

	/// <inheritdoc />
	public override int GetHashCode(Vector2 obj) => obj.GetHashCode();
}
