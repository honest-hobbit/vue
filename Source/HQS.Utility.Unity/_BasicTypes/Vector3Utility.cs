﻿namespace HQS.Utility.Unity;

public static class Vector3Utility
{
	public static Vector3 CalculateNormal(Vector3 a, Vector3 b, Vector3 c) =>
		Vector3.Cross(b - a, c - a).normalized;

	public static Vector3 MoveTowards(Vector3 point, Vector3 target, float distance)
	{
		var vector = target - point;
		vector.Normalize();
		return point + (vector * distance);
	}
}
