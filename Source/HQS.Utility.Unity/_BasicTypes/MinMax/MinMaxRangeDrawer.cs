﻿#if UNITY_EDITOR

namespace HQS.Utility.Unity;

[CustomPropertyDrawer(typeof(MinMaxFloat))]
[CustomPropertyDrawer(typeof(MinMaxInt))]
[CustomPropertyDrawer(typeof(MinMaxRangeAttribute))]
public class MinMaxRangeDrawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		float result = base.GetPropertyHeight(property, label);
		if (!IsMinMaxRange(property))
		{
			return result;
		}

		if (property.serializedObject.isEditingMultipleObjects)
		{
			// TODO I don't know if this is the proper way to handle not editing multiple objects
			return 0;
		}

		if (this.attribute as MinMaxRangeAttribute != null)
		{
			result += 16;
		}

		return result;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		if (!IsMinMaxRange(property))
		{
			label = EditorGUI.BeginProperty(position, label, property);
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
			EditorGUI.LabelField(position, $"Use MinMaxRange with {nameof(MinMaxFloat)}.");
			return;
		}

		if (property.serializedObject.isEditingMultipleObjects)
		{
			// TODO I don't know if this is the proper way to handle not editing multiple objects
			return;
		}

		var minmax = this.attribute as MinMaxRangeAttribute;
		if (minmax != null)
		{
			position.height -= 16f;
		}

		label = EditorGUI.BeginProperty(position, label, property);
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
		var left = new Rect(position.x, position.y, (position.width / 2) - 11f, position.height);
		var right = new Rect(position.x + position.width - left.width, position.y, left.width, position.height);
		var mid = new Rect(left.xMax, position.y, 22, position.height);

		if (property.type == nameof(MinMaxFloat))
		{
			// min max float
			var minProperty = property.FindPropertyRelative(nameof(MinMaxFloat.Min));
			var maxProperty = property.FindPropertyRelative(nameof(MinMaxFloat.Max));
			var min = minProperty.floatValue;
			var max = maxProperty.floatValue;
			min = Mathf.Clamp(EditorGUI.FloatField(left, min), minmax?.Min ?? float.MinValue, max);
			EditorGUI.LabelField(mid, " to ");
			max = Mathf.Clamp(EditorGUI.FloatField(right, max), min, minmax?.Max ?? float.MaxValue);

			if (minmax != null)
			{
				position.y += 16f;
				EditorGUI.MinMaxSlider(position, GUIContent.none, ref min, ref max, minmax.Min, minmax.Max);
			}

			minProperty.floatValue = min;
			maxProperty.floatValue = max;
		}
		else
		{
			// min max int
			var minProperty = property.FindPropertyRelative(nameof(MinMaxInt.Min));
			var maxProperty = property.FindPropertyRelative(nameof(MinMaxInt.Max));
			var min = minProperty.intValue;
			var max = maxProperty.intValue;
			min = Mathf.Clamp(EditorGUI.IntField(left, min), (int?)minmax?.Min ?? int.MinValue, max);
			EditorGUI.LabelField(mid, " to ");
			max = Mathf.Clamp(EditorGUI.IntField(right, max), min, (int?)minmax?.Max ?? int.MaxValue);

			float minFloat = min;
			float maxFloat = max;
			if (minmax != null)
			{
				position.y += 16f;
				EditorGUI.MinMaxSlider(position, GUIContent.none, ref minFloat, ref maxFloat, minmax.Min, minmax.Max);
			}

			minProperty.intValue = Mathf.RoundToInt(minFloat);
			maxProperty.intValue = Mathf.RoundToInt(maxFloat);
		}

		EditorGUI.EndProperty();
	}

	private static bool IsMinMaxRange(SerializedProperty property) =>
		property.type == nameof(MinMaxFloat) || property.type == nameof(MinMaxInt);
}

#endif
