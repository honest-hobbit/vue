﻿namespace HQS.Utility.Unity;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
[Serializable]
public record struct MinMaxInt : IMinMax<MinMaxInt, int>
{
	public int Min;

	public int Max;

	public MinMaxInt(int min, int max)
	{
		this.Min = min;
		this.Max = max;
	}

	/// <inheritdoc />
	int IMinMax<MinMaxInt, int>.Min
	{
		get => this.Min;
		set => this.Min = value;
	}

	/// <inheritdoc />
	int IMinMax<MinMaxInt, int>.Max
	{
		get => this.Max;
		set => this.Max = value;
	}

	/// <inheritdoc />
	public int Range => this.Max - this.Min;

	public static MinMaxInt Zero => new MinMaxInt(0, 0);

	public static MinMaxInt Ordered(int x, int y) => x <= y ? new MinMaxInt(x, y) : new MinMaxInt(y, x);

	/// <inheritdoc />
	public MinMaxInt Order() => this.Min <= this.Max ? this : new MinMaxInt(this.Max, this.Min);

	/// <inheritdoc />
	public int GetRandomValue()
	{
		var range = this.Order();
		return UnityEngine.Random.Range(range.Min, range.Max + 1);
	}

	/// <inheritdoc />
	public int GetRandomValue(System.Random random)
	{
		Debug.Assert(random != null);

		var range = this.Order();
		return random.Next(range.Min, range.Max + 1);
	}

	/// <inheritdoc />
	public int Clamp(int value)
	{
		var range = this.Order();
		return Mathf.Clamp(value, range.Min, range.Max);
	}

	/// <inheritdoc />
	public bool Contains(int value)
	{
		var range = this.Order();
		return value >= range.Min && value <= range.Max;
	}

	/// <inheritdoc />
	public MinMaxInt Encompass(int value)
	{
		var range = this.Order();
		if (value > range.Max)
		{
			return new MinMaxInt(range.Min, value);
		}
		else if (value < range.Min)
		{
			return new MinMaxInt(value, range.Max);
		}
		else
		{
			return this;
		}
	}

	/// <inheritdoc />
	public override string ToString() => $"[{this.Min}, {this.Max}]";
}
