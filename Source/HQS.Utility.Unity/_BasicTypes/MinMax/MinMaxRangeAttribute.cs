﻿namespace HQS.Utility.Unity;

[AttributeUsage(AttributeTargets.Field)]
public class MinMaxRangeAttribute : PropertyAttribute
{
	public MinMaxRangeAttribute(float min, float max)
	{
		this.Min = min;
		this.Max = max;
	}

	public float Min { get; }

	public float Max { get; }
}
