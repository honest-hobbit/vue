﻿namespace HQS.Utility.Unity;

public interface IMinMax<TSelf, TValue> : IEquatable<TSelf>
	where TValue : IComparable<TValue>
{
	TValue Min { get; set; }

	TValue Max { get; set; }

	TValue Range { get; }

	TSelf Order();

	TValue GetRandomValue();

	TValue GetRandomValue(System.Random random);

	TValue Clamp(TValue value);

	bool Contains(TValue value);

	TSelf Encompass(TValue value);
}
