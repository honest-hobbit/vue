﻿namespace HQS.Utility.Unity;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
[Serializable]
public record struct MinMaxFloat : IMinMax<MinMaxFloat, float>
{
	public float Min;

	public float Max;

	public MinMaxFloat(float min, float max)
	{
		this.Min = min;
		this.Max = max;
	}

	/// <inheritdoc />
	float IMinMax<MinMaxFloat, float>.Min
	{
		get => this.Min;
		set => this.Min = value;
	}

	/// <inheritdoc />
	float IMinMax<MinMaxFloat, float>.Max
	{
		get => this.Max;
		set => this.Max = value;
	}

	/// <inheritdoc />
	public float Range => this.Max - this.Min;

	public static MinMaxFloat Zero => new MinMaxFloat(0, 0);

	public static MinMaxFloat Ordered(float x, float y) => x <= y ? new MinMaxFloat(x, y) : new MinMaxFloat(y, x);

	/// <inheritdoc />
	public MinMaxFloat Order() => this.Min <= this.Max ? this : new MinMaxFloat(this.Max, this.Min);

	/// <inheritdoc />
	public float GetRandomValue()
	{
		var range = this.Order();
		return UnityEngine.Random.Range(range.Min, range.Max);
	}

	/// <inheritdoc />
	public float GetRandomValue(System.Random random)
	{
		Debug.Assert(random != null);

		var range = this.Order();
		return (float)(range.Min + (random.NextDouble() * (range.Max - range.Min)));
	}

	/// <inheritdoc />
	public float Clamp(float value)
	{
		var range = this.Order();
		return Mathf.Clamp(value, range.Min, range.Max);
	}

	/// <inheritdoc />
	public bool Contains(float value)
	{
		var range = this.Order();
		return value >= range.Min && value <= range.Max;
	}

	/// <inheritdoc />
	public MinMaxFloat Encompass(float value)
	{
		var range = this.Order();
		if (value > range.Max)
		{
			return new MinMaxFloat(range.Min, value);
		}
		else if (value < range.Min)
		{
			return new MinMaxFloat(value, range.Max);
		}
		else
		{
			return this;
		}
	}

	/// <inheritdoc />
	public override string ToString() => $"[{this.Min}, {this.Max}]";
}
