﻿namespace HQS.Utility.Unity;

public static class ColorExtensions
{
	public static bool IsOpaque(this Color32 color) => color.a == byte.MaxValue;

	public static bool IsInvisible(this Color32 color) => color.a == byte.MinValue;

	public static bool IsVisible(this Color32 color) => color.a != byte.MinValue;

	public static bool IsTransparent(this Color32 color) => color.a != byte.MaxValue;

	public static bool IsSemiTransparent(this Color32 color) => color.a != byte.MaxValue && color.a != byte.MinValue;

	public static bool IsEqualTo(this Color32 color, Color32 other) =>
		color.r == other.r && color.g == other.g && color.b == other.b && color.a == other.a;

	public static bool IsRGBEqualTo(this Color32 color, Color32 other) =>
		color.r == other.r && color.g == other.g && color.b == other.b;

	public static ColorHSV ToHSV(this Color32 color) => ToHSV((Color)color);

	public static ColorHSV ToHSV(this Color color)
	{
		Color.RGBToHSV(color, out float h, out float s, out float v);
		return new ColorHSV(h, s, v, color.a);
	}
}
