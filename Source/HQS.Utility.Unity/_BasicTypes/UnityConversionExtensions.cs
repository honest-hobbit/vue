﻿namespace HQS.Utility.Unity;

public static class UnityConversionExtensions
{
	public static System.Numerics.Vector2 ToSystem(this Vector2 value) => new System.Numerics.Vector2(value.x, value.y);

	public static System.Numerics.Vector3 ToSystem(this Vector3 value) => new System.Numerics.Vector3(value.x, value.y, value.z);

	public static System.Numerics.Vector4 ToSystem(this Vector4 value) => new System.Numerics.Vector4(value.x, value.y, value.z, value.w);

	public static Vector2 ToUnity(this System.Numerics.Vector2 value) => new Vector2(value.X, value.Y);

	public static Vector3 ToUnity(this System.Numerics.Vector3 value) => new Vector3(value.X, value.Y, value.Z);

	public static Vector4 ToUnity(this System.Numerics.Vector4 value) => new Vector4(value.X, value.Y, value.Z, value.W);

	public static ColorRGB ToHQS(this Color32 value) => new ColorRGB(value.r, value.g, value.b, value.a);

	public static Color32 ToUnity(this ColorRGB value) => new Color32(value.R, value.G, value.B, value.A);
}
