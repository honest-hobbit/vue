﻿namespace HQS.Utility.Unity;

public static class BehaviorExtensions
{
	public static void ToggleEnabled(this Behaviour behavior)
	{
		Debug.Assert(behavior != null);

		behavior.enabled = !behavior.enabled;
	}
}
