﻿namespace HQS.Utility.Unity.Concurrency;

public static class UnityThreadPool
{
	public static bool QueueUserWorkItem(Action callBack) =>
		QueueUserWorkItem(callBack, e => UnityEngine.Debug.LogError(e));

	public static bool QueueUserWorkItem(Action callBack, Action<Exception> onError)
	{
		Debug.Assert(callBack != null);
		Debug.Assert(onError != null);

		return ThreadPool.QueueUserWorkItem(
			callbackState =>
			{
				try
				{
					callBack();
				}
				catch (Exception e)
				{
					onError(e);
				}
			});
	}

	public static bool QueueUserWorkItem(WaitCallback callBack) =>
		QueueUserWorkItem(callBack, e => UnityEngine.Debug.LogError(e));

	public static bool QueueUserWorkItem(WaitCallback callBack, Action<Exception> onError) =>
		QueueUserWorkItem(callBack, null, onError);

	public static bool QueueUserWorkItem(WaitCallback callBack, object state) =>
		QueueUserWorkItem(callBack, state, e => UnityEngine.Debug.LogError(e));

	public static bool QueueUserWorkItem(WaitCallback callBack, object state, Action<Exception> onError)
	{
		Debug.Assert(callBack != null);
		Debug.Assert(onError != null);

		return ThreadPool.QueueUserWorkItem(
			callbackState =>
			{
				try
				{
					callBack(callbackState);
				}
				catch (Exception e)
				{
					onError(e);
				}
			}, state);
	}
}
