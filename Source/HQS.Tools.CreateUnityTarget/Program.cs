﻿using System.Reflection;
using System.Xml.Linq;
using HQS.Tools.Utility;

//System.Diagnostics.Debugger.Launch();

try
{
	var unityTargetFilePath = @"Misc\UnityAssemblies.target";

	var executableDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!;
	var repositoryDirectory = Path.GetFullPath(Path.Combine(executableDirectory, @"..\..\"));

	var unityProvidedAssemblyFilePaths =
		UnityProject.GetAssemblyReferences(repositoryDirectory)
		.Where(path =>
			path.Contains(UnityProject.UnityProvidedAssemblies.UnityEngineDirectory) ||
			path.Contains(UnityProject.ScriptAssembliesDirectory))
		.Select(path => path.Replace(repositoryDirectory, "$(RepoRoot)"))
		.ToArray();

	var document = new XDocument();
	var projectNode = new XElement("Project");
	var itemGroup = new XElement("ItemGroup");
	projectNode.Add(itemGroup);
	document.Add(projectNode);

	foreach (var assemblyPath in unityProvidedAssemblyFilePaths)
	{
		var referenceNode = new XElement("Reference");
		referenceNode.SetAttributeValue("Include", Path.GetFileNameWithoutExtension(assemblyPath));

		var hintPathNode = new XElement("HintPath");
		hintPathNode.SetValue(assemblyPath);

		referenceNode.Add(hintPathNode);
		itemGroup.Add(referenceNode);
	}

	var unityTargetFileFullPath = Path.Combine(repositoryDirectory, unityTargetFilePath);
	File.WriteAllText(unityTargetFileFullPath, document.ToString());

	Console.WriteLine("Unity Assemblies target file created successfully:");
	Console.WriteLine();
	Console.WriteLine(unityTargetFileFullPath);
}
catch (Exception error)
{
	Console.WriteLine("Exception while creating Unity Assemblies target file:");
	Console.WriteLine();
	Console.WriteLine(error);
}

Console.WriteLine();
Console.WriteLine("Press Enter to close.");
Console.ReadLine();
