﻿namespace HQS.VUE.OldEngine;

public class AdjacencyCube<T> : IAdjacencyView<T>
{
	private readonly T[] values = new T[27];

	public AdjacencyCube()
	{
	}

	public AdjacencyCube(T value)
	{
		this.SetAllTo(value);
	}

	/// <inheritdoc />
	public int Length => this.values.Length;

	public T this[int index]
	{
		get => this.values[index];
		set => this.values[index] = value;
	}

	public T this[int x, int y, int z]
	{
		get => this.values[Adjacency.ToIndex(x, y, z)];
		set => this.values[Adjacency.ToIndex(x, y, z)] = value;
	}

	public T this[Index3D index]
	{
		get => this.values[Adjacency.ToIndex(index.X, index.Y, index.Z)];
		set => this.values[Adjacency.ToIndex(index.X, index.Y, index.Z)] = value;
	}

	/// <inheritdoc />
	T IAdjacencyView<T>.this[int index] => this[index];

	/// <inheritdoc />
	T IAdjacencyView<T>.this[int x, int y, int z] => this[x, y, z];

	/// <inheritdoc />
	T IAdjacencyView<T>.this[Index3D index] => this[index];

	public void SetAllTo(T value)
	{
		for (int index = 0; index < this.values.Length; index++)
		{
			this.values[index] = value;
		}
	}

	public void SetCrossTo(T value)
	{
		this.values[Adjacency.Center] = value;
		this.values[Adjacency.NegX] = value;
		this.values[Adjacency.PosX] = value;
		this.values[Adjacency.NegY] = value;
		this.values[Adjacency.PosY] = value;
		this.values[Adjacency.NegZ] = value;
		this.values[Adjacency.PosZ] = value;
	}

	public void ClearAll() => this.SetAllTo(default);

	public void ClearCross() => this.SetCrossTo(default);

	// assign over NegX after calling this
	public void ShiftCrossTowardsNegativeX()
	{
		this[Adjacency.PosX] = this[Adjacency.Center];
		this[Adjacency.Center] = this[Adjacency.NegX];
	}

	// assign over PosX after calling this
	public void ShiftCrossTowardsPositiveX()
	{
		this[Adjacency.NegX] = this[Adjacency.Center];
		this[Adjacency.Center] = this[Adjacency.PosX];
	}

	// assign over NegY after calling this
	public void ShiftCrossTowardsNegativeY()
	{
		this[Adjacency.PosY] = this[Adjacency.Center];
		this[Adjacency.Center] = this[Adjacency.NegY];
	}

	// assign over PosY after calling this
	public void ShiftCrossTowardsPositiveY()
	{
		this[Adjacency.NegY] = this[Adjacency.Center];
		this[Adjacency.Center] = this[Adjacency.PosY];
	}

	// assign over NegZ after calling this
	public void ShiftCrossTowardsNegativeZ()
	{
		this[Adjacency.PosZ] = this[Adjacency.Center];
		this[Adjacency.Center] = this[Adjacency.NegZ];
	}

	// assign over PosZ after calling this
	public void ShiftCrossTowardsPositiveZ()
	{
		this[Adjacency.NegZ] = this[Adjacency.Center];
		this[Adjacency.Center] = this[Adjacency.PosZ];
	}
}
