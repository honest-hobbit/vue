﻿namespace HQS.VUE.OldEngine;

public static class Adjacency
{
	static Adjacency()
	{
		Center = ZZZ;
		NegX = NZZ;
		PosX = PZZ;
		NegY = ZNZ;
		PosY = ZPZ;
		NegZ = ZZN;
		PosZ = ZZP;
		Neighbors = All.Where(i => i.AsIndex != Index3D.Zero).ToArray().AsReadOnlyList();
	}

	public static IReadOnlyList<Pair> Neighbors { get; }

	public static IReadOnlyList<Pair> All { get; } = Factory.CreateArray(27, i => new Pair(i)).AsReadOnlyList();

	public static int Center { get; }

	public static int NegX { get; }

	public static int PosX { get; }

	public static int NegY { get; }

	public static int PosY { get; }

	public static int NegZ { get; }

	public static int PosZ { get; }

	public static int NNN { get; } = ToIndex(-1, -1, -1);

	public static int ZNN { get; } = ToIndex(0, -1, -1);

	public static int PNN { get; } = ToIndex(1, -1, -1);

	public static int NZN { get; } = ToIndex(-1, 0, -1);

	public static int ZZN { get; } = ToIndex(0, 0, -1);

	public static int PZN { get; } = ToIndex(1, 0, -1);

	public static int NPN { get; } = ToIndex(-1, 1, -1);

	public static int ZPN { get; } = ToIndex(0, 1, -1);

	public static int PPN { get; } = ToIndex(1, 1, -1);

	public static int NNZ { get; } = ToIndex(-1, -1, 0);

	public static int ZNZ { get; } = ToIndex(0, -1, 0);

	public static int PNZ { get; } = ToIndex(1, -1, 0);

	public static int NZZ { get; } = ToIndex(-1, 0, 0);

	public static int ZZZ { get; } = ToIndex(0, 0, 0);

	public static int PZZ { get; } = ToIndex(1, 0, 0);

	public static int NPZ { get; } = ToIndex(-1, 1, 0);

	public static int ZPZ { get; } = ToIndex(0, 1, 0);

	public static int PPZ { get; } = ToIndex(1, 1, 0);

	public static int NNP { get; } = ToIndex(-1, -1, 1);

	public static int ZNP { get; } = ToIndex(0, -1, 1);

	public static int PNP { get; } = ToIndex(1, -1, 1);

	public static int NZP { get; } = ToIndex(-1, 0, 1);

	public static int ZZP { get; } = ToIndex(0, 0, 1);

	public static int PZP { get; } = ToIndex(1, 0, 1);

	public static int NPP { get; } = ToIndex(-1, 1, 1);

	public static int ZPP { get; } = ToIndex(0, 1, 1);

	public static int PPP { get; } = ToIndex(1, 1, 1);

	public static int ToIndex(Index3D index) => ToIndex(index.X, index.Y, index.Z);

	public static int ToIndex(int x, int y, int z)
	{
		Debug.Assert(x >= -1 && x <= 1);
		Debug.Assert(y >= -1 && y <= 1);
		Debug.Assert(z >= -1 && z <= 1);

		int index = 0;
		if (z == 0)
		{
			index += 9;
		}
		else if (z == 1)
		{
			index += 18;
		}

		if (y == 0)
		{
			index += 3;
		}
		else if (y == 1)
		{
			index += 6;
		}

		return index + x + 1;
	}

	public static Index3D ToIndex(int index) => All[index].AsIndex;

	private static Index3D GetIndex(int index)
	{
		Debug.Assert(index >= 0 && index <= 26);

		int x;
		int y;
		int z;

		if (index >= 18)
		{
			z = 1;
			index -= 18;
		}
		else if (index >= 9)
		{
			z = 0;
			index -= 9;
		}
		else
		{
			z = -1;
		}

		if (index >= 6)
		{
			y = 1;
			index -= 6;
		}
		else if (index >= 3)
		{
			y = 0;
			index -= 3;
		}
		else
		{
			y = -1;
		}

		x = index - 1;
		return new Index3D(x, y, z);
	}

	public struct Pair
	{
		public Pair(Index3D index)
		{
			this.AsIndex = index;
			this.AsInt = ToIndex(index);
		}

		public Pair(int index)
		{
			this.AsIndex = GetIndex(index);
			this.AsInt = index;
		}

		public Index3D AsIndex { get; }

		public int AsInt { get; }
	}
}
