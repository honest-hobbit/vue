﻿namespace HQS.VUE.OldEngine;

public static class VolumeSelection
{
	public enum Mode
	{
		None,
		All,
		Center,
		Bottom,
		Top,
		Left,
		Right,
		Front,
		Back,
	}

	public enum Axis
	{
		None = Mode.None,
		All = Mode.All,
		Center = Mode.Center,
		NegativeX = Mode.Left,
		PositiveX = Mode.Right,
		NegativeY = Mode.Bottom,
		PositiveY = Mode.Top,
		NegativeZ = Mode.Back,
		PositiveZ = Mode.Front,
	}

	public static IEnumerable<Index3D> Range(Index3D dimensions, Axis mode, int width) => Range(dimensions, (Mode)mode, width);

	public static IEnumerable<Index3D> Range(Index3D dimensions, Mode mode, int width)
	{
		Debug.Assert(dimensions.X >= 0);
		Debug.Assert(dimensions.Y >= 0);
		Debug.Assert(dimensions.Z >= 0);
		Debug.Assert(Enumeration.IsDefined(mode));
		Debug.Assert(width >= 0);

		if (mode == Mode.All)
		{
			return Index3D.Zero.Range(dimensions);
		}

		if (mode == Mode.None || dimensions.X == 0 || dimensions.Y == 0 || dimensions.Z == 0 || width == 0)
		{
			return Enumerable.Empty<Index3D>();
		}

		return mode switch
		{
			Mode.Center => Center(),
			Mode.Bottom => Bottom(),
			Mode.Top => Top(),
			Mode.Left => Left(),
			Mode.Right => Right(),
			Mode.Front => Front(),
			Mode.Back => Back(),
			_ => throw InvalidEnumArgument.CreateException(nameof(mode), mode),
		};

		IEnumerable<Index3D> Center()
		{
			var lower = Index3D.Zero.Midpoint(dimensions) - new Index3D(width / 2);
			lower = new Index3D(lower.X.ClampLower(0), lower.Y.ClampLower(0), lower.Z.ClampLower(0));
			dimensions = new Index3D(
				width.ClampUpper(dimensions.X), width.ClampUpper(dimensions.Y), width.ClampUpper(dimensions.Z));

			foreach (var index in lower.Range(dimensions))
			{
				yield return index;
			}
		}

		IEnumerable<Index3D> Bottom()
		{
			for (int iX = 0; iX < dimensions.X; iX++)
			{
				for (int iZ = 0; iZ < dimensions.Z; iZ++)
				{
					for (int iY = 0; iY < width.ClampUpper(dimensions.Y); iY++)
					{
						yield return new Index3D(iX, iY, iZ);
					}
				}
			}
		}

		IEnumerable<Index3D> Top()
		{
			for (int iX = 0; iX < dimensions.X; iX++)
			{
				for (int iZ = 0; iZ < dimensions.Z; iZ++)
				{
					for (int iY = dimensions.Y - 1; iY >= (dimensions.Y - width).ClampLower(0); iY--)
					{
						yield return new Index3D(iX, iY, iZ);
					}
				}
			}
		}

		IEnumerable<Index3D> Left()
		{
			for (int iY = 0; iY < dimensions.Y; iY++)
			{
				for (int iZ = 0; iZ < dimensions.Z; iZ++)
				{
					for (int iX = 0; iX < width.ClampUpper(dimensions.X); iX++)
					{
						yield return new Index3D(iX, iY, iZ);
					}
				}
			}
		}

		IEnumerable<Index3D> Right()
		{
			for (int iY = 0; iY < dimensions.Y; iY++)
			{
				for (int iZ = 0; iZ < dimensions.Z; iZ++)
				{
					for (int iX = dimensions.X - 1; iX >= (dimensions.X - width).ClampLower(0); iX--)
					{
						yield return new Index3D(iX, iY, iZ);
					}
				}
			}
		}

		IEnumerable<Index3D> Front()
		{
			for (int iX = 0; iX < dimensions.X; iX++)
			{
				for (int iY = 0; iY < dimensions.Y; iY++)
				{
					for (int iZ = 0; iZ < width.ClampUpper(dimensions.Z); iZ++)
					{
						yield return new Index3D(iX, iY, iZ);
					}
				}
			}
		}

		IEnumerable<Index3D> Back()
		{
			for (int iX = 0; iX < dimensions.X; iX++)
			{
				for (int iY = 0; iY < dimensions.Y; iY++)
				{
					for (int iZ = dimensions.Z - 1; iZ >= (dimensions.Z - width).ClampLower(0); iZ--)
					{
						yield return new Index3D(iX, iY, iZ);
					}
				}
			}
		}
	}
}
