﻿namespace HQS.VUE.OldEngine;

public class SquareArray<T> : ISquareArrayView<T>, IIndexable<Index2D, T>
{
	public SquareArray(int sizeExponent)
	{
		Debug.Assert(sizeExponent >= 0);

		this.SizeExponent = sizeExponent;
		this.Array = new T[BinaryArray.GetSquareLength(sizeExponent)];
	}

	/// <inheritdoc />
	public Index2D Dimensions => new Index2D(this.SideLength);

	/// <inheritdoc />
	public Index2D LowerBounds => Index2D.Zero;

	/// <inheritdoc />
	public Index2D UpperBounds => new Index2D(this.SideLength - 1);

	/// <inheritdoc />
	public int SizeExponent { get; }

	/// <inheritdoc />
	public int SideLength => BinaryArray.GetSideLength(this.SizeExponent);

	/// <inheritdoc />
	public int Length => this.Array.Length;

	public T[] Array { get; }

	/// <inheritdoc />
	public T this[int index]
	{
		get
		{
			ISquareArrayViewContracts.Indexer(this, index);
			return this.Array[index];
		}

		set
		{
			ISquareArrayViewContracts.Indexer(this, index);
			this.Array[index] = value;
		}
	}

	/// <inheritdoc />
	public T this[int x, int y]
	{
		get => this.Array[this.ToArrayIndex(x, y)];
		set => this.Array[this.ToArrayIndex(x, y)] = value;
	}

	/// <inheritdoc />
	public T this[Index2D index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);
			return this.Array[this.ToArrayIndex(index)];
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);
			this.Array[this.ToArrayIndex(index)] = value;
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index2D, T>> GetEnumerator()
	{
		foreach (var index in this.LowerBounds.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private int ToArrayIndex(Index2D index) => index.X + (index.Y << this.SizeExponent);

	private int ToArrayIndex(int x, int y)
	{
		ISquareArrayViewContracts.Indexer(this, x, y);
		return x + (y << this.SizeExponent);
	}
}
