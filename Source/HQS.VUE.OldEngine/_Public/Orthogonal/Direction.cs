﻿namespace HQS.VUE.OldEngine;

public enum Direction : byte
{
	Left = 0,
	NegativeX = 0,

	Right = 1,
	PositiveX = 1,

	Down = 2,
	NegativeY = 2,

	Up = 3,
	PositiveY = 3,

	Back = 4,
	NegativeZ = 4,

	Forward = 5,
	PositiveZ = 5,
}
