﻿namespace HQS.VUE.OldEngine;

public static class Orthogonal
{
	public static IReadOnlyList<OrthoAxis> Orientations { get; } = Enumeration.Values<OrthoAxis>();

	public static Direction GetDirection(this OrthoAxis value) => OrthogonalOrientation.From(value).Direction;

	public static Rotation GetRotation(this OrthoAxis value) => OrthogonalOrientation.From(value).Rotation;

	public static bool GetIsFlipped(this OrthoAxis value) => OrthogonalOrientation.From(value).IsFlipped;

	public static OrthogonalOrientation GetOrientation(OrthoAxis axis) => OrthogonalOrientation.From(axis);

	public static OrthogonalOrientation GetOrientation(Direction direction, Rotation rotation, bool isFlipped) =>
		OrthogonalOrientation.From(direction, rotation, isFlipped);

	public static OrthoAxis GetOrthoAxis(Direction direction, Rotation rotation, bool isFlipped) =>
		OrthogonalOrientation.From(direction, rotation, isFlipped).Orientation;
}
