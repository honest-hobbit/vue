﻿namespace HQS.VUE.OldEngine;

public static class RotationExtensions
{
	public static Rotation Add(this Rotation value, Rotation add)
	{
		Debug.Assert(Enumeration.IsDefined(value));
		Debug.Assert(Enumeration.IsDefined(add));

		int result = (int)value + (int)add;
		if (result >= 4)
		{
			result -= 4;
		}

		return (Rotation)result;
	}

	public static Rotation Rotate(this Rotation value, bool clockwise = true) => clockwise ? value.Next() : value.Previous();

	public static Rotation Next(this Rotation value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		int result = (int)value + 1;
		if (result >= 4)
		{
			result -= 4;
		}

		return (Rotation)result;
	}

	public static Rotation Previous(this Rotation value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		int result = (int)value - 1;
		if (result < 0)
		{
			result += 4;
		}

		return (Rotation)result;
	}

	public static Rotation Reverse(this Rotation value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		int result = (int)value + 2;
		if (result >= 4)
		{
			result -= 4;
		}

		return (Rotation)result;
	}
}
