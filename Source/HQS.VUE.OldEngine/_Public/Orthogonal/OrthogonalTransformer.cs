﻿namespace HQS.VUE.OldEngine;

public class OrthogonalTransformer
{
	private static readonly Func<Vector3, Vector3>[] Transforms = CreateArray();

	private OrthogonalOrientation orientation;

	private Func<Vector3, Vector3> transform;

	public OrthogonalTransformer()
	{
		this.orientation = OrthogonalOrientation.From(OrthoAxis.PosY000Norm_24);
		this.transform = Transforms[(int)OrthoAxis.PosY000Norm_24];
	}

	public OrthoAxis Orientation
	{
		get => this.orientation.Orientation;
		set
		{
			Debug.Assert(Enumeration.IsDefined(value));

			if (this.orientation.Orientation != value)
			{
				this.orientation = OrthogonalOrientation.From(value);
				this.transform = Transforms[(int)value];
			}
		}
	}

	public Direction Direction => this.orientation.Direction;

	public Rotation Rotation => this.orientation.Rotation;

	public bool IsFlipped => this.orientation.IsFlipped;

	public Vector3 Transform(Vector3 vector) => this.transform(vector);

	public Func<Vector3, Vector3> GetTransform() => this.transform;

	public void Reset() => this.Orientation = OrthoAxis.PosY000Norm_24;

	private static Func<Vector3, Vector3>[] CreateArray()
	{
		var result = new Func<Vector3, Vector3>[Orthogonal.Orientations.Count];

		result[(int)OrthoAxis.NegX000Norm_00] = vec => new Vector3(-vec.z, -vec.x, vec.y);
		result[(int)OrthoAxis.NegX000Flip_01] = vec => new Vector3(vec.z, -vec.x, vec.y);
		result[(int)OrthoAxis.NegX090Norm_02] = vec => new Vector3(vec.y, -vec.x, vec.z);
		result[(int)OrthoAxis.NegX090Flip_03] = vec => new Vector3(-vec.y, -vec.x, vec.z);
		result[(int)OrthoAxis.NegX180Norm_04] = vec => new Vector3(vec.z, -vec.x, -vec.y);
		result[(int)OrthoAxis.NegX180Flip_05] = vec => new Vector3(-vec.z, -vec.x, -vec.y);
		result[(int)OrthoAxis.NegX270Norm_06] = vec => new Vector3(-vec.y, -vec.x, -vec.z);
		result[(int)OrthoAxis.NegX270Flip_07] = vec => new Vector3(vec.y, -vec.x, -vec.z);

		result[(int)OrthoAxis.PosX000Norm_08] = vec => new Vector3(vec.z, vec.x, vec.y);
		result[(int)OrthoAxis.PosX000Flip_09] = vec => new Vector3(-vec.z, vec.x, vec.y);
		result[(int)OrthoAxis.PosX090Norm_10] = vec => new Vector3(vec.y, vec.x, -vec.z);
		result[(int)OrthoAxis.PosX090Flip_11] = vec => new Vector3(-vec.y, vec.x, -vec.z);
		result[(int)OrthoAxis.PosX180Norm_12] = vec => new Vector3(-vec.z, vec.x, -vec.y);
		result[(int)OrthoAxis.PosX180Flip_13] = vec => new Vector3(vec.z, vec.x, -vec.y);
		result[(int)OrthoAxis.PosX270Norm_14] = vec => new Vector3(-vec.y, vec.x, vec.z);
		result[(int)OrthoAxis.PosX270Flip_15] = vec => new Vector3(vec.y, vec.x, vec.z);

		result[(int)OrthoAxis.NegY000Norm_16] = vec => new Vector3(vec.x, -vec.y, -vec.z);
		result[(int)OrthoAxis.NegY000Flip_17] = vec => new Vector3(-vec.x, -vec.y, -vec.z);
		result[(int)OrthoAxis.NegY090Norm_18] = vec => new Vector3(-vec.z, -vec.y, -vec.x);
		result[(int)OrthoAxis.NegY090Flip_19] = vec => new Vector3(vec.z, -vec.y, -vec.x);
		result[(int)OrthoAxis.NegY180Norm_20] = vec => new Vector3(-vec.x, -vec.y, vec.z);
		result[(int)OrthoAxis.NegY180Flip_21] = vec => new Vector3(vec.x, -vec.y, vec.z);
		result[(int)OrthoAxis.NegY270Norm_22] = vec => new Vector3(vec.z, -vec.y, vec.x);
		result[(int)OrthoAxis.NegY270Flip_23] = vec => new Vector3(-vec.z, -vec.y, vec.x);

		result[(int)OrthoAxis.PosY000Norm_24] = vec => vec;
		result[(int)OrthoAxis.PosY000Flip_25] = vec => new Vector3(-vec.x, vec.y, vec.z);
		result[(int)OrthoAxis.PosY090Norm_26] = vec => new Vector3(vec.z, vec.y, -vec.x);
		result[(int)OrthoAxis.PosY090Flip_27] = vec => new Vector3(-vec.z, vec.y, -vec.x);
		result[(int)OrthoAxis.PosY180Norm_28] = vec => new Vector3(-vec.x, vec.y, -vec.z);
		result[(int)OrthoAxis.PosY180Flip_29] = vec => new Vector3(vec.x, vec.y, -vec.z);
		result[(int)OrthoAxis.PosY270Norm_30] = vec => new Vector3(-vec.z, vec.y, vec.x);
		result[(int)OrthoAxis.PosY270Flip_31] = vec => new Vector3(vec.z, vec.y, vec.x);

		result[(int)OrthoAxis.NegZ000Norm_32] = vec => new Vector3(vec.x, -vec.z, vec.y);
		result[(int)OrthoAxis.NegZ000Flip_33] = vec => new Vector3(-vec.x, -vec.z, vec.y);
		result[(int)OrthoAxis.NegZ090Norm_34] = vec => new Vector3(vec.y, -vec.z, -vec.x);
		result[(int)OrthoAxis.NegZ090Flip_35] = vec => new Vector3(-vec.y, -vec.z, -vec.x);
		result[(int)OrthoAxis.NegZ180Norm_36] = vec => new Vector3(-vec.x, -vec.z, -vec.y);
		result[(int)OrthoAxis.NegZ180Flip_37] = vec => new Vector3(vec.x, -vec.z, -vec.y);
		result[(int)OrthoAxis.NegZ270Norm_38] = vec => new Vector3(-vec.y, -vec.z, vec.x);
		result[(int)OrthoAxis.NegZ270Flip_39] = vec => new Vector3(vec.y, -vec.z, vec.x);

		result[(int)OrthoAxis.PosZ000Norm_40] = vec => new Vector3(-vec.x, vec.z, vec.y);
		result[(int)OrthoAxis.PosZ000Flip_41] = vec => new Vector3(vec.x, vec.z, vec.y);
		result[(int)OrthoAxis.PosZ090Norm_42] = vec => new Vector3(vec.y, vec.z, vec.x);
		result[(int)OrthoAxis.PosZ090Flip_43] = vec => new Vector3(-vec.y, vec.z, vec.x);
		result[(int)OrthoAxis.PosZ180Norm_44] = vec => new Vector3(vec.x, vec.z, -vec.y);
		result[(int)OrthoAxis.PosZ180Flip_45] = vec => new Vector3(-vec.x, vec.z, -vec.y);
		result[(int)OrthoAxis.PosZ270Norm_46] = vec => new Vector3(-vec.y, vec.z, -vec.x);
		result[(int)OrthoAxis.PosZ270Flip_47] = vec => new Vector3(vec.y, vec.z, -vec.x);

		return result;
	}
}
