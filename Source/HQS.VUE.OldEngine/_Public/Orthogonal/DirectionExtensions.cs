﻿namespace HQS.VUE.OldEngine;

public static class DirectionExtensions
{
	private const float MaxThreshold = .99f;

	private const float MinThreshold = 1f - MaxThreshold;

	public static Direction Next(this Direction value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		int result = (int)value + 1;
		if (result >= 6)
		{
			result -= 6;
		}

		return (Direction)result;
	}

	public static Direction Previous(this Direction value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		int result = (int)value - 1;
		if (result < 0)
		{
			result += 6;
		}

		return (Direction)result;
	}

	public static Direction Reverse(this Direction value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		return value switch
		{
			Direction.PositiveX => Direction.NegativeX,
			Direction.NegativeX => Direction.PositiveX,
			Direction.PositiveY => Direction.NegativeY,
			Direction.NegativeY => Direction.PositiveY,
			Direction.PositiveZ => Direction.NegativeZ,
			Direction.NegativeZ => Direction.PositiveZ,
			_ => throw InvalidEnumArgument.CreateException(nameof(value), value),
		};
	}

	public static Direction ToDirection(this Vector3 vector)
	{
		if (vector.x < -MaxThreshold)
		{
			Debug.Assert(vector.y.Abs() < MinThreshold);
			Debug.Assert(vector.z.Abs() < MinThreshold);
			return Direction.NegativeX;
		}

		if (vector.x > MaxThreshold)
		{
			Debug.Assert(vector.y.Abs() < MinThreshold);
			Debug.Assert(vector.z.Abs() < MinThreshold);
			return Direction.PositiveX;
		}

		if (vector.y < -MaxThreshold)
		{
			Debug.Assert(vector.x.Abs() < MinThreshold);
			Debug.Assert(vector.z.Abs() < MinThreshold);
			return Direction.NegativeY;
		}

		if (vector.y > MaxThreshold)
		{
			Debug.Assert(vector.x.Abs() < MinThreshold);
			Debug.Assert(vector.z.Abs() < MinThreshold);
			return Direction.PositiveY;
		}

		if (vector.z < -MaxThreshold)
		{
			Debug.Assert(vector.x.Abs() < MinThreshold);
			Debug.Assert(vector.y.Abs() < MinThreshold);
			return Direction.NegativeZ;
		}

		if (vector.z > MaxThreshold)
		{
			Debug.Assert(vector.x.Abs() < MinThreshold);
			Debug.Assert(vector.y.Abs() < MinThreshold);
			return Direction.PositiveZ;
		}

		throw new ArgumentException("Must be orthogonal.", nameof(vector));
	}

	public static Vector3 ToVector(this Direction direction)
	{
		Debug.Assert(Enumeration.IsDefined(direction));

		return direction switch
		{
			Direction.Left => Vector3.left,
			Direction.Right => Vector3.right,
			Direction.Down => Vector3.down,
			Direction.Up => Vector3.up,
			Direction.Back => Vector3.back,
			Direction.Forward => Vector3.forward,
			_ => throw InvalidEnumArgument.CreateException(nameof(direction), direction),
		};
	}
}
