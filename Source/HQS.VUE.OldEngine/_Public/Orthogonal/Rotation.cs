﻿namespace HQS.VUE.OldEngine;

public enum Rotation : byte
{
	Clockwise0 = 0,

	Clockwise90 = 1,

	Clockwise180 = 2,

	Clockwise270 = 3,

	Counter0 = Clockwise0,

	Counter90 = Clockwise270,

	Counter180 = Clockwise180,

	Counter270 = Clockwise90,
}
