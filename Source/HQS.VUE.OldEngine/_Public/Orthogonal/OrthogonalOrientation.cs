﻿namespace HQS.VUE.OldEngine;

public struct OrthogonalOrientation : IEquatable<OrthogonalOrientation>
{
	private static readonly OrthogonalOrientation[] Conversion = OrientationArray.Create(
		x => x.Orientation,
		new OrthogonalOrientation(OrthoAxis.NegX000Norm_00, Direction.NegativeX, Rotation.Clockwise0, false),
		new OrthogonalOrientation(OrthoAxis.NegX000Flip_01, Direction.NegativeX, Rotation.Clockwise0, true),
		new OrthogonalOrientation(OrthoAxis.NegX090Norm_02, Direction.NegativeX, Rotation.Clockwise90, false),
		new OrthogonalOrientation(OrthoAxis.NegX090Flip_03, Direction.NegativeX, Rotation.Clockwise90, true),
		new OrthogonalOrientation(OrthoAxis.NegX180Norm_04, Direction.NegativeX, Rotation.Clockwise180, false),
		new OrthogonalOrientation(OrthoAxis.NegX180Flip_05, Direction.NegativeX, Rotation.Clockwise180, true),
		new OrthogonalOrientation(OrthoAxis.NegX270Norm_06, Direction.NegativeX, Rotation.Clockwise270, false),
		new OrthogonalOrientation(OrthoAxis.NegX270Flip_07, Direction.NegativeX, Rotation.Clockwise270, true),
		new OrthogonalOrientation(OrthoAxis.PosX000Norm_08, Direction.PositiveX, Rotation.Clockwise0, false),
		new OrthogonalOrientation(OrthoAxis.PosX000Flip_09, Direction.PositiveX, Rotation.Clockwise0, true),
		new OrthogonalOrientation(OrthoAxis.PosX090Norm_10, Direction.PositiveX, Rotation.Clockwise90, false),
		new OrthogonalOrientation(OrthoAxis.PosX090Flip_11, Direction.PositiveX, Rotation.Clockwise90, true),
		new OrthogonalOrientation(OrthoAxis.PosX180Norm_12, Direction.PositiveX, Rotation.Clockwise180, false),
		new OrthogonalOrientation(OrthoAxis.PosX180Flip_13, Direction.PositiveX, Rotation.Clockwise180, true),
		new OrthogonalOrientation(OrthoAxis.PosX270Norm_14, Direction.PositiveX, Rotation.Clockwise270, false),
		new OrthogonalOrientation(OrthoAxis.PosX270Flip_15, Direction.PositiveX, Rotation.Clockwise270, true),
		new OrthogonalOrientation(OrthoAxis.NegY000Norm_16, Direction.NegativeY, Rotation.Clockwise0, false),
		new OrthogonalOrientation(OrthoAxis.NegY000Flip_17, Direction.NegativeY, Rotation.Clockwise0, true),
		new OrthogonalOrientation(OrthoAxis.NegY090Norm_18, Direction.NegativeY, Rotation.Clockwise90, false),
		new OrthogonalOrientation(OrthoAxis.NegY090Flip_19, Direction.NegativeY, Rotation.Clockwise90, true),
		new OrthogonalOrientation(OrthoAxis.NegY180Norm_20, Direction.NegativeY, Rotation.Clockwise180, false),
		new OrthogonalOrientation(OrthoAxis.NegY180Flip_21, Direction.NegativeY, Rotation.Clockwise180, true),
		new OrthogonalOrientation(OrthoAxis.NegY270Norm_22, Direction.NegativeY, Rotation.Clockwise270, false),
		new OrthogonalOrientation(OrthoAxis.NegY270Flip_23, Direction.NegativeY, Rotation.Clockwise270, true),
		new OrthogonalOrientation(OrthoAxis.PosY000Norm_24, Direction.PositiveY, Rotation.Clockwise0, false),
		new OrthogonalOrientation(OrthoAxis.PosY000Flip_25, Direction.PositiveY, Rotation.Clockwise0, true),
		new OrthogonalOrientation(OrthoAxis.PosY090Norm_26, Direction.PositiveY, Rotation.Clockwise90, false),
		new OrthogonalOrientation(OrthoAxis.PosY090Flip_27, Direction.PositiveY, Rotation.Clockwise90, true),
		new OrthogonalOrientation(OrthoAxis.PosY180Norm_28, Direction.PositiveY, Rotation.Clockwise180, false),
		new OrthogonalOrientation(OrthoAxis.PosY180Flip_29, Direction.PositiveY, Rotation.Clockwise180, true),
		new OrthogonalOrientation(OrthoAxis.PosY270Norm_30, Direction.PositiveY, Rotation.Clockwise270, false),
		new OrthogonalOrientation(OrthoAxis.PosY270Flip_31, Direction.PositiveY, Rotation.Clockwise270, true),
		new OrthogonalOrientation(OrthoAxis.NegZ000Norm_32, Direction.NegativeZ, Rotation.Clockwise0, false),
		new OrthogonalOrientation(OrthoAxis.NegZ000Flip_33, Direction.NegativeZ, Rotation.Clockwise0, true),
		new OrthogonalOrientation(OrthoAxis.NegZ090Norm_34, Direction.NegativeZ, Rotation.Clockwise90, false),
		new OrthogonalOrientation(OrthoAxis.NegZ090Flip_35, Direction.NegativeZ, Rotation.Clockwise90, true),
		new OrthogonalOrientation(OrthoAxis.NegZ180Norm_36, Direction.NegativeZ, Rotation.Clockwise180, false),
		new OrthogonalOrientation(OrthoAxis.NegZ180Flip_37, Direction.NegativeZ, Rotation.Clockwise180, true),
		new OrthogonalOrientation(OrthoAxis.NegZ270Norm_38, Direction.NegativeZ, Rotation.Clockwise270, false),
		new OrthogonalOrientation(OrthoAxis.NegZ270Flip_39, Direction.NegativeZ, Rotation.Clockwise270, true),
		new OrthogonalOrientation(OrthoAxis.PosZ000Norm_40, Direction.PositiveZ, Rotation.Clockwise0, false),
		new OrthogonalOrientation(OrthoAxis.PosZ000Flip_41, Direction.PositiveZ, Rotation.Clockwise0, true),
		new OrthogonalOrientation(OrthoAxis.PosZ090Norm_42, Direction.PositiveZ, Rotation.Clockwise90, false),
		new OrthogonalOrientation(OrthoAxis.PosZ090Flip_43, Direction.PositiveZ, Rotation.Clockwise90, true),
		new OrthogonalOrientation(OrthoAxis.PosZ180Norm_44, Direction.PositiveZ, Rotation.Clockwise180, false),
		new OrthogonalOrientation(OrthoAxis.PosZ180Flip_45, Direction.PositiveZ, Rotation.Clockwise180, true),
		new OrthogonalOrientation(OrthoAxis.PosZ270Norm_46, Direction.PositiveZ, Rotation.Clockwise270, false),
		new OrthogonalOrientation(OrthoAxis.PosZ270Flip_47, Direction.PositiveZ, Rotation.Clockwise270, true));

	private OrthogonalOrientation(OrthoAxis orientation, Direction direction, Rotation rotation, bool isFlipped)
	{
		Debug.Assert(Enumeration.IsDefined(orientation));
		Debug.Assert(Enumeration.IsDefined(direction));
		Debug.Assert(Enumeration.IsDefined(rotation));

		this.Orientation = orientation;
		this.Direction = direction;
		this.Rotation = rotation;
		this.IsFlipped = isFlipped;
	}

	public OrthoAxis Orientation { get; }

	public Direction Direction { get; }

	public Rotation Rotation { get; }

	public bool IsFlipped { get; }

	public static OrthogonalOrientation From(OrthoAxis axis)
	{
		Debug.Assert(Enumeration.IsDefined(axis));

		return Conversion[(int)axis];
	}

	public static OrthogonalOrientation From(Direction direction, Rotation rotation, bool isFlipped)
	{
		Debug.Assert(Enumeration.IsDefined(direction));
		Debug.Assert(Enumeration.IsDefined(rotation));

		// << 3 is equivalent to multiplying by 8
		// << 1 is equivalent to multiplying by 2
		int index = ((int)direction << 3) + ((int)rotation << 1);
		if (isFlipped)
		{
			index++;
		}

		return Conversion[index];
	}

	public static bool operator ==(OrthogonalOrientation lhs, OrthogonalOrientation rhs) => lhs.Equals(rhs);

	public static bool operator !=(OrthogonalOrientation lhs, OrthogonalOrientation rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(OrthogonalOrientation other) => this.Orientation == other.Orientation;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.Orientation.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.Orientation.ToString();
}
