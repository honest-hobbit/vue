﻿using static HQS.VUE.OldEngine.OrthoAxis;

namespace HQS.VUE.OldEngine;

public static class OrthoAxisExtensions
{
	private static readonly OrthoAxis[] XMirrored = new OrthoAxis[]
	{
			NegX000Flip_01, NegX000Norm_00, NegX090Flip_03, NegX090Norm_02,
			NegX180Flip_05, NegX180Norm_04, NegX270Flip_07, NegX270Norm_06,

			PosX000Flip_09, PosX000Norm_08, PosX090Flip_11, PosX090Norm_10,
			PosX180Flip_13, PosX180Norm_12, PosX270Flip_15, PosX270Norm_14,

			NegY000Flip_17, NegY000Norm_16, NegY090Flip_19, NegY090Norm_18,
			NegY180Flip_21, NegY180Norm_20, NegY270Flip_23, NegY270Norm_22,

			PosY000Flip_25, PosY000Norm_24, PosY090Flip_27, PosY090Norm_26,
			PosY180Flip_29, PosY180Norm_28, PosY270Flip_31, PosY270Norm_30,

			NegZ000Flip_33, NegZ000Norm_32, NegZ090Flip_35, NegZ090Norm_34,
			NegZ180Flip_37, NegZ180Norm_36, NegZ270Flip_39, NegZ270Norm_38,

			PosZ000Flip_41, PosZ000Norm_40, PosZ090Flip_43, PosZ090Norm_42,
			PosZ180Flip_45, PosZ180Norm_44, PosZ270Flip_47, PosZ270Norm_46,
	};

	private static readonly OrthoAxis[] YMirrored = new OrthoAxis[]
	{
			PosX000Flip_09, PosX000Norm_08, PosX270Flip_15, PosX270Norm_14,
			PosX180Flip_13, PosX180Norm_12, PosX090Flip_11, PosX090Norm_10,

			NegX000Flip_01, NegX000Norm_00, NegX270Flip_07, NegX270Norm_06,
			NegX180Flip_05, NegX180Norm_04, NegX090Flip_03, NegX090Norm_02,

			PosY180Flip_29, PosY180Norm_28, PosY090Flip_27, PosY090Norm_26,
			PosY000Flip_25, PosY000Norm_24, PosY270Flip_31, PosY270Norm_30,

			NegY180Flip_21, NegY180Norm_20, NegY090Flip_19, NegY090Norm_18,
			NegY000Flip_17, NegY000Norm_16, NegY270Flip_23, NegY270Norm_22,

			PosZ000Flip_41, PosZ000Norm_40, PosZ270Flip_47, PosZ270Norm_46,
			PosZ180Flip_45, PosZ180Norm_44, PosZ090Flip_43, PosZ090Norm_42,

			NegZ000Flip_33, NegZ000Norm_32, NegZ270Flip_39, NegZ270Norm_38,
			NegZ180Flip_37, NegZ180Norm_36, NegZ090Flip_35, NegZ090Norm_34,
	};

	private static readonly OrthoAxis[] ZMirrored = new OrthoAxis[]
	{
			NegX180Flip_05, NegX180Norm_04, NegX270Flip_07, NegX270Norm_06,
			NegX000Flip_01, NegX000Norm_00, NegX090Flip_03, NegX090Norm_02,

			PosX180Flip_13, PosX180Norm_12, PosX270Flip_15, PosX270Norm_14,
			PosX000Flip_09, PosX000Norm_08, PosX090Flip_11, PosX090Norm_10,

			NegY180Flip_21, NegY180Norm_20, NegY270Flip_23, NegY270Norm_22,
			NegY000Flip_17, NegY000Norm_16, NegY090Flip_19, NegY090Norm_18,

			PosY180Flip_29, PosY180Norm_28, PosY270Flip_31, PosY270Norm_30,
			PosY000Flip_25, PosY000Norm_24, PosY090Flip_27, PosY090Norm_26,

			NegZ180Flip_37, NegZ180Norm_36, NegZ270Flip_39, NegZ270Norm_38,
			NegZ000Flip_33, NegZ000Norm_32, NegZ090Flip_35, NegZ090Norm_34,

			PosZ180Flip_45, PosZ180Norm_44, PosZ270Flip_47, PosZ270Norm_46,
			PosZ000Flip_41, PosZ000Norm_40, PosZ090Flip_43, PosZ090Norm_42,
	};

	private static readonly OrthoAxis[] XRotated = new OrthoAxis[]
	{
			NegX000Norm_00, NegY090Norm_18, PosX180Norm_12, PosY270Norm_30,
			NegX000Flip_01, NegY090Flip_19, PosX180Flip_13, PosY270Flip_31,
			NegX090Norm_02, NegZ090Norm_34, PosX090Norm_10, PosZ090Norm_42,
			NegX090Flip_03, NegZ090Flip_35, PosX090Flip_11, PosZ090Flip_43,
			NegX180Norm_04, PosY090Norm_26, PosX000Norm_08, NegY270Norm_22,
			NegX180Flip_05, PosY090Flip_27, PosX000Flip_09, NegY270Flip_23,
			NegX270Norm_06, PosZ270Norm_46, PosX270Norm_14, NegZ270Norm_38,
			NegX270Flip_07, PosZ270Flip_47, PosX270Flip_15, NegZ270Flip_39,

			PosX000Norm_08, NegY270Norm_22, NegX180Norm_04, PosY090Norm_26,
			PosX000Flip_09, NegY270Flip_23, NegX180Flip_05, PosY090Flip_27,
			PosX090Norm_10, PosZ090Norm_42, NegX090Norm_02, NegZ090Norm_34,
			PosX090Flip_11, PosZ090Flip_43, NegX090Flip_03, NegZ090Flip_35,
			PosX180Norm_12, PosY270Norm_30, NegX000Norm_00, NegY090Norm_18,
			PosX180Flip_13, PosY270Flip_31, NegX000Flip_01, NegY090Flip_19,
			PosX270Norm_14, NegZ270Norm_38, NegX270Norm_06, PosZ270Norm_46,
			PosX270Flip_15, NegZ270Flip_39, NegX270Flip_07, PosZ270Flip_47,

			NegY000Norm_16, PosZ180Norm_44, PosY000Norm_24, NegZ000Norm_32,
			NegY000Flip_17, PosZ180Flip_45, PosY000Flip_25, NegZ000Flip_33,
			NegY090Norm_18, PosX180Norm_12, PosY270Norm_30, NegX000Norm_00,
			NegY090Flip_19, PosX180Flip_13, PosY270Flip_31, NegX000Flip_01,
			NegY180Norm_20, NegZ180Norm_36, PosY180Norm_28, PosZ000Norm_40,
			NegY180Flip_21, NegZ180Flip_37, PosY180Flip_29, PosZ000Flip_41,
			NegY270Norm_22, NegX180Norm_04, PosY090Norm_26, PosX000Norm_08,
			NegY270Flip_23, NegX180Flip_05, PosY090Flip_27, PosX000Flip_09,

			PosY000Norm_24, NegZ000Norm_32, NegY000Norm_16, PosZ180Norm_44,
			PosY000Flip_25, NegZ000Flip_33, NegY000Flip_17, PosZ180Flip_45,
			PosY090Norm_26, PosX000Norm_08, NegY270Norm_22, NegX180Norm_04,
			PosY090Flip_27, PosX000Flip_09, NegY270Flip_23, NegX180Flip_05,
			PosY180Norm_28, PosZ000Norm_40, NegY180Norm_20, NegZ180Norm_36,
			PosY180Flip_29, PosZ000Flip_41, NegY180Flip_21, NegZ180Flip_37,
			PosY270Norm_30, NegX000Norm_00, NegY090Norm_18, PosX180Norm_12,
			PosY270Flip_31, NegX000Flip_01, NegY090Flip_19, PosX180Flip_13,

			NegZ000Norm_32, NegY000Norm_16, PosZ180Norm_44, PosY000Norm_24,
			NegZ000Flip_33, NegY000Flip_17, PosZ180Flip_45, PosY000Flip_25,
			NegZ090Norm_34, PosX090Norm_10, PosZ090Norm_42, NegX090Norm_02,
			NegZ090Flip_35, PosX090Flip_11, PosZ090Flip_43, NegX090Flip_03,
			NegZ180Norm_36, PosY180Norm_28, PosZ000Norm_40, NegY180Norm_20,
			NegZ180Flip_37, PosY180Flip_29, PosZ000Flip_41, NegY180Flip_21,
			NegZ270Norm_38, NegX270Norm_06, PosZ270Norm_46, PosX270Norm_14,
			NegZ270Flip_39, NegX270Flip_07, PosZ270Flip_47, PosX270Flip_15,

			PosZ000Norm_40, NegY180Norm_20, NegZ180Norm_36, PosY180Norm_28,
			PosZ000Flip_41, NegY180Flip_21, NegZ180Flip_37, PosY180Flip_29,
			PosZ090Norm_42, NegX090Norm_02, NegZ090Norm_34, PosX090Norm_10,
			PosZ090Flip_43, NegX090Flip_03, NegZ090Flip_35, PosX090Flip_11,
			PosZ180Norm_44, PosY000Norm_24, NegZ000Norm_32, NegY000Norm_16,
			PosZ180Flip_45, PosY000Flip_25, NegZ000Flip_33, NegY000Flip_17,
			PosZ270Norm_46, PosX270Norm_14, NegZ270Norm_38, NegX270Norm_06,
			PosZ270Flip_47, PosX270Flip_15, NegZ270Flip_39, NegX270Flip_07,
	};

	private static readonly OrthoAxis[] YRotated = new OrthoAxis[]
	{
			NegX000Norm_00, NegX090Norm_02, NegX180Norm_04, NegX270Norm_06,
			NegX000Flip_01, NegX270Flip_07, NegX180Flip_05, NegX090Flip_03,
			NegX090Norm_02, NegX180Norm_04, NegX270Norm_06, NegX000Norm_00,
			NegX090Flip_03, NegX000Flip_01, NegX270Flip_07, NegX180Flip_05,
			NegX180Norm_04, NegX270Norm_06, NegX000Norm_00, NegX090Norm_02,
			NegX180Flip_05, NegX090Flip_03, NegX000Flip_01, NegX270Flip_07,
			NegX270Norm_06, NegX000Norm_00, NegX090Norm_02, NegX180Norm_04,
			NegX270Flip_07, NegX180Flip_05, NegX090Flip_03, NegX000Flip_01,

			PosX000Norm_08, PosX090Norm_10, PosX180Norm_12, PosX270Norm_14,
			PosX000Flip_09, PosX270Flip_15, PosX180Flip_13, PosX090Flip_11,
			PosX090Norm_10, PosX180Norm_12, PosX270Norm_14, PosX000Norm_08,
			PosX090Flip_11, PosX000Flip_09, PosX270Flip_15, PosX180Flip_13,
			PosX180Norm_12, PosX270Norm_14, PosX000Norm_08, PosX090Norm_10,
			PosX180Flip_13, PosX090Flip_11, PosX000Flip_09, PosX270Flip_15,
			PosX270Norm_14, PosX000Norm_08, PosX090Norm_10, PosX180Norm_12,
			PosX270Flip_15, PosX180Flip_13, PosX090Flip_11, PosX000Flip_09,

			NegY000Norm_16, NegY090Norm_18, NegY180Norm_20, NegY270Norm_22,
			NegY000Flip_17, NegY270Flip_23, NegY180Flip_21, NegY090Flip_19,
			NegY090Norm_18, NegY180Norm_20, NegY270Norm_22, NegY000Norm_16,
			NegY090Flip_19, NegY000Flip_17, NegY270Flip_23, NegY180Flip_21,
			NegY180Norm_20, NegY270Norm_22, NegY000Norm_16, NegY090Norm_18,
			NegY180Flip_21, NegY090Flip_19, NegY000Flip_17, NegY270Flip_23,
			NegY270Norm_22, NegY000Norm_16, NegY090Norm_18, NegY180Norm_20,
			NegY270Flip_23, NegY180Flip_21, NegY090Flip_19, NegY000Flip_17,

			PosY000Norm_24, PosY090Norm_26, PosY180Norm_28, PosY270Norm_30,
			PosY000Flip_25, PosY270Flip_31, PosY180Flip_29, PosY090Flip_27,
			PosY090Norm_26, PosY180Norm_28, PosY270Norm_30, PosY000Norm_24,
			PosY090Flip_27, PosY000Flip_25, PosY270Flip_31, PosY180Flip_29,
			PosY180Norm_28, PosY270Norm_30, PosY000Norm_24, PosY090Norm_26,
			PosY180Flip_29, PosY090Flip_27, PosY000Flip_25, PosY270Flip_31,
			PosY270Norm_30, PosY000Norm_24, PosY090Norm_26, PosY180Norm_28,
			PosY270Flip_31, PosY180Flip_29, PosY090Flip_27, PosY000Flip_25,

			NegZ000Norm_32, NegZ090Norm_34, NegZ180Norm_36, NegZ270Norm_38,
			NegZ000Flip_33, NegZ270Flip_39, NegZ180Flip_37, NegZ090Flip_35,
			NegZ090Norm_34, NegZ180Norm_36, NegZ270Norm_38, NegZ000Norm_32,
			NegZ090Flip_35, NegZ000Flip_33, NegZ270Flip_39, NegZ180Flip_37,
			NegZ180Norm_36, NegZ270Norm_38, NegZ000Norm_32, NegZ090Norm_34,
			NegZ180Flip_37, NegZ090Flip_35, NegZ000Flip_33, NegZ270Flip_39,
			NegZ270Norm_38, NegZ000Norm_32, NegZ090Norm_34, NegZ180Norm_36,
			NegZ270Flip_39, NegZ180Flip_37, NegZ090Flip_35, NegZ000Flip_33,

			PosZ000Norm_40, PosZ090Norm_42, PosZ180Norm_44, PosZ270Norm_46,
			PosZ000Flip_41, PosZ270Flip_47, PosZ180Flip_45, PosZ090Flip_43,
			PosZ090Norm_42, PosZ180Norm_44, PosZ270Norm_46, PosZ000Norm_40,
			PosZ090Flip_43, PosZ000Flip_41, PosZ270Flip_47, PosZ180Flip_45,
			PosZ180Norm_44, PosZ270Norm_46, PosZ000Norm_40, PosZ090Norm_42,
			PosZ180Flip_45, PosZ090Flip_43, PosZ000Flip_41, PosZ270Flip_47,
			PosZ270Norm_46, PosZ000Norm_40, PosZ090Norm_42, PosZ180Norm_44,
			PosZ270Flip_47, PosZ180Flip_45, PosZ090Flip_43, PosZ000Flip_41,
	};

	private static readonly OrthoAxis[] ZRotated = new OrthoAxis[]
	{
			NegX000Norm_00, NegZ000Norm_32, PosX000Norm_08, PosZ000Norm_40,
			NegX000Flip_01, PosZ000Flip_41, PosX000Flip_09, NegZ000Flip_33,
			NegX090Norm_02, PosY000Norm_24, PosX270Norm_14, NegY180Norm_20,
			NegX090Flip_03, NegY180Flip_21, PosX270Flip_15, PosY000Flip_25,
			NegX180Norm_04, PosZ180Norm_44, PosX180Norm_12, NegZ180Norm_36,
			NegX180Flip_05, NegZ180Flip_37, PosX180Flip_13, PosZ180Flip_45,
			NegX270Norm_06, NegY000Norm_16, PosX090Norm_10, PosY180Norm_28,
			NegX270Flip_07, PosY180Flip_29, PosX090Flip_11, NegY000Flip_17,

			PosX000Norm_08, PosZ000Norm_40, NegX000Norm_00, NegZ000Norm_32,
			PosX000Flip_09, NegZ000Flip_33, NegX000Flip_01, PosZ000Flip_41,
			PosX090Norm_10, PosY180Norm_28, NegX270Norm_06, NegY000Norm_16,
			PosX090Flip_11, NegY000Flip_17, NegX270Flip_07, PosY180Flip_29,
			PosX180Norm_12, NegZ180Norm_36, NegX180Norm_04, PosZ180Norm_44,
			PosX180Flip_13, PosZ180Flip_45, NegX180Flip_05, NegZ180Flip_37,
			PosX270Norm_14, NegY180Norm_20, NegX090Norm_02, PosY000Norm_24,
			PosX270Flip_15, PosY000Flip_25, NegX090Flip_03, NegY180Flip_21,

			NegY000Norm_16, PosX090Norm_10, PosY180Norm_28, NegX270Norm_06,
			NegY000Flip_17, NegX270Flip_07, PosY180Flip_29, PosX090Flip_11,
			NegY090Norm_18, NegZ090Norm_34, PosY090Norm_26, PosZ270Norm_46,
			NegY090Flip_19, PosZ270Flip_47, PosY090Flip_27, NegZ090Flip_35,
			NegY180Norm_20, NegX090Norm_02, PosY000Norm_24, PosX270Norm_14,
			NegY180Flip_21, PosX270Flip_15, PosY000Flip_25, NegX090Flip_03,
			NegY270Norm_22, PosZ090Norm_42, PosY270Norm_30, NegZ270Norm_38,
			NegY270Flip_23, NegZ270Flip_39, PosY270Flip_31, PosZ090Flip_43,

			PosY000Norm_24, PosX270Norm_14, NegY180Norm_20, NegX090Norm_02,
			PosY000Flip_25, NegX090Flip_03, NegY180Flip_21, PosX270Flip_15,
			PosY090Norm_26, PosZ270Norm_46, NegY090Norm_18, NegZ090Norm_34,
			PosY090Flip_27, NegZ090Flip_35, NegY090Flip_19, PosZ270Flip_47,
			PosY180Norm_28, NegX270Norm_06, NegY000Norm_16, PosX090Norm_10,
			PosY180Flip_29, PosX090Flip_11, NegY000Flip_17, NegX270Flip_07,
			PosY270Norm_30, NegZ270Norm_38, NegY270Norm_22, PosZ090Norm_42,
			PosY270Flip_31, PosZ090Flip_43, NegY270Flip_23, NegZ270Flip_39,

			NegZ000Norm_32, PosX000Norm_08, PosZ000Norm_40, NegX000Norm_00,
			NegZ000Flip_33, NegX000Flip_01, PosZ000Flip_41, PosX000Flip_09,
			NegZ090Norm_34, PosY090Norm_26, PosZ270Norm_46, NegY090Norm_18,
			NegZ090Flip_35, NegY090Flip_19, PosZ270Flip_47, PosY090Flip_27,
			NegZ180Norm_36, NegX180Norm_04, PosZ180Norm_44, PosX180Norm_12,
			NegZ180Flip_37, PosX180Flip_13, PosZ180Flip_45, NegX180Flip_05,
			NegZ270Norm_38, NegY270Norm_22, PosZ090Norm_42, PosY270Norm_30,
			NegZ270Flip_39, PosY270Flip_31, PosZ090Flip_43, NegY270Flip_23,

			PosZ000Norm_40, NegX000Norm_00, NegZ000Norm_32, PosX000Norm_08,
			PosZ000Flip_41, PosX000Flip_09, NegZ000Flip_33, NegX000Flip_01,
			PosZ090Norm_42, PosY270Norm_30, NegZ270Norm_38, NegY270Norm_22,
			PosZ090Flip_43, NegY270Flip_23, NegZ270Flip_39, PosY270Flip_31,
			PosZ180Norm_44, PosX180Norm_12, NegZ180Norm_36, NegX180Norm_04,
			PosZ180Flip_45, NegX180Flip_05, NegZ180Flip_37, PosX180Flip_13,
			PosZ270Norm_46, NegY090Norm_18, NegZ090Norm_34, PosY090Norm_26,
			PosZ270Flip_47, PosY090Flip_27, NegZ090Flip_35, NegY090Flip_19,
	};

	public static OrthoAxis MirrorX(this OrthoAxis value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		return XMirrored[(int)value];
	}

	public static OrthoAxis MirrorY(this OrthoAxis value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		return YMirrored[(int)value];
	}

	public static OrthoAxis MirrorZ(this OrthoAxis value)
	{
		Debug.Assert(Enumeration.IsDefined(value));

		return ZMirrored[(int)value];
	}

	public static OrthoAxis RotateX(this OrthoAxis value, Rotation amount = Rotation.Clockwise90)
	{
		Debug.Assert(Enumeration.IsDefined(value));
		Debug.Assert(Enumeration.IsDefined(amount));

		// << 2 is equivalent to multiplying by 4
		return XRotated[((int)value << 2) + (int)amount];
	}

	public static OrthoAxis RotateY(this OrthoAxis value, Rotation amount = Rotation.Clockwise90)
	{
		Debug.Assert(Enumeration.IsDefined(value));
		Debug.Assert(Enumeration.IsDefined(amount));

		// << 2 is equivalent to multiplying by 4
		return YRotated[((int)value << 2) + (int)amount];
	}

	public static OrthoAxis RotateZ(this OrthoAxis value, Rotation amount = Rotation.Clockwise90)
	{
		Debug.Assert(Enumeration.IsDefined(value));
		Debug.Assert(Enumeration.IsDefined(amount));

		// << 2 is equivalent to multiplying by 4
		return ZRotated[((int)value << 2) + (int)amount];
	}
}
