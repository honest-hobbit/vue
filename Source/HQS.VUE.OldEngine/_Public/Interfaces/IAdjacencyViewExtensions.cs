﻿namespace HQS.VUE.OldEngine;

public static class IAdjacencyViewExtensions
{
	public static bool AllNotNull<T>(this IAdjacencyView<T> view)
		where T : class
	{
		Debug.Assert(view != null);

		for (int index = 0; index < view.Length; index++)
		{
			if (view[index] == null)
			{
				return false;
			}
		}

		return true;
	}

	public static bool AllAssigned<T>(this IAdjacencyView<T> view)
		where T : struct, IEquatable<T>
	{
		Debug.Assert(view != null);

		for (int index = 0; index < view.Length; index++)
		{
			if (view[index].Equals(default))
			{
				return false;
			}
		}

		return true;
	}

	public static bool IsCrossNotNull<T>(this IAdjacencyView<T> view)
		where T : class
	{
		Debug.Assert(view != null);

		return view[Adjacency.Center] != null
			&& view[Adjacency.NegX] != null && view[Adjacency.PosX] != null
			&& view[Adjacency.NegY] != null && view[Adjacency.PosY] != null
			&& view[Adjacency.NegZ] != null && view[Adjacency.PosZ] != null;
	}

	public static bool IsCrossAssigned<T>(this IAdjacencyView<T> view)
		where T : struct, IEquatable<T>
	{
		Debug.Assert(view != null);

		return !view[Adjacency.Center].Equals(default)
			&& !view[Adjacency.NegX].Equals(default) && !view[Adjacency.PosX].Equals(default)
			&& !view[Adjacency.NegY].Equals(default) && !view[Adjacency.PosY].Equals(default)
			&& !view[Adjacency.NegZ].Equals(default) && !view[Adjacency.PosZ].Equals(default);
	}
}
