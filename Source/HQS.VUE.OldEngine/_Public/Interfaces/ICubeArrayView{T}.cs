﻿namespace HQS.VUE.OldEngine;

public interface ICubeArrayView<T> : IIndexableView<Index3D, T>
{
	int SizeExponent { get; }

	int SideLength { get; }

	int Length { get; }

	T this[int index] { get; }

	T this[int x, int y, int z] { get; }
}
