﻿namespace HQS.VUE.OldEngine;

public interface ISquareArrayView<T> : IIndexableView<Index2D, T>
{
	int SizeExponent { get; }

	int SideLength { get; }

	int Length { get; }

	T this[int index] { get; }

	T this[int x, int y] { get; }
}
