﻿namespace HQS.VUE.OldEngine;

public static class ISquareArrayViewContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Indexer<T>(ISquareArrayView<T> instance, int index)
	{
		Debug.Assert(instance != null);
		Debug.Assert(index >= 0);
		Debug.Assert(index < instance.Length);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Indexer<T>(ISquareArrayView<T> instance, int x, int y)
	{
		Debug.Assert(instance != null);
		Debug.Assert(x >= 0);
		Debug.Assert(x < instance.SideLength);
		Debug.Assert(y >= 0);
		Debug.Assert(y < instance.SideLength);
	}
}
