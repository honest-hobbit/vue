﻿namespace HQS.VUE.OldEngine;

public static class ICubeArrayViewContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Indexer<T>(ICubeArrayView<T> instance, int index)
	{
		Debug.Assert(instance != null);
		Debug.Assert(index >= 0);
		Debug.Assert(index < instance.Length);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Indexer<T>(ICubeArrayView<T> instance, int x, int y, int z)
	{
		Debug.Assert(instance != null);
		Debug.Assert(x >= 0);
		Debug.Assert(x < instance.SideLength);
		Debug.Assert(y >= 0);
		Debug.Assert(y < instance.SideLength);
		Debug.Assert(z >= 0);
		Debug.Assert(z < instance.SideLength);
	}
}
