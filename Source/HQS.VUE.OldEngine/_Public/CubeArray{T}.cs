﻿namespace HQS.VUE.OldEngine;

public class CubeArray<T> : ICubeArrayView<T>, IIndexable<Index3D, T>
{
	public CubeArray(int sizeExponent)
	{
		Debug.Assert(sizeExponent >= 0);

		this.SizeExponent = sizeExponent;
		this.Array = new T[BinaryArray.GetCubeLength(sizeExponent)];
	}

	/// <inheritdoc />
	public Index3D Dimensions => new Index3D(this.SideLength);

	/// <inheritdoc />
	public Index3D LowerBounds => Index3D.Zero;

	/// <inheritdoc />
	public Index3D UpperBounds => new Index3D(this.SideLength - 1);

	/// <inheritdoc />
	public int SizeExponent { get; }

	/// <inheritdoc />
	public int SideLength => BinaryArray.GetSideLength(this.SizeExponent);

	/// <inheritdoc />
	public int Length => this.Array.Length;

	public T[] Array { get; }

	/// <inheritdoc />
	public T this[int index]
	{
		get
		{
			ICubeArrayViewContracts.Indexer(this, index);
			return this.Array[index];
		}

		set
		{
			ICubeArrayViewContracts.Indexer(this, index);
			this.Array[index] = value;
		}
	}

	/// <inheritdoc />
	public T this[int x, int y, int z]
	{
		get
		{
			ICubeArrayViewContracts.Indexer(this, x, y, z);
			return this.Array[CubeArray.ToArrayIndex(this.SizeExponent, x, y, z)];
		}

		set
		{
			ICubeArrayViewContracts.Indexer(this, x, y, z);
			this.Array[CubeArray.ToArrayIndex(this.SizeExponent, x, y, z)] = value;
		}
	}

	/// <inheritdoc />
	public T this[Index3D index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);
			return this.Array[CubeArray.ToArrayIndex(this.SizeExponent, index)];
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);
			this.Array[CubeArray.ToArrayIndex(this.SizeExponent, index)] = value;
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index3D, T>> GetEnumerator()
	{
		foreach (var index in this.LowerBounds.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
