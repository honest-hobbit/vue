﻿namespace HQS.VUE.OldEngine;

internal static class CubeArrayProjection
{
	private static readonly ICubeArrayProjection[] Instances = OrientationArray.Create<ICubeArrayProjection>(
		x => x.Orientation,
		new ProjectionNegX000Norm_00(),
		new ProjectionNegX000Flip_01(),
		new ProjectionNegX090Norm_02(),
		new ProjectionNegX090Flip_03(),
		new ProjectionNegX180Norm_04(),
		new ProjectionNegX180Flip_05(),
		new ProjectionNegX270Norm_06(),
		new ProjectionNegX270Flip_07(),
		new ProjectionPosX000Norm_08(),
		new ProjectionPosX000Flip_09(),
		new ProjectionPosX090Norm_10(),
		new ProjectionPosX090Flip_11(),
		new ProjectionPosX180Norm_12(),
		new ProjectionPosX180Flip_13(),
		new ProjectionPosX270Norm_14(),
		new ProjectionPosX270Flip_15(),
		new ProjectionNegY000Norm_16(),
		new ProjectionNegY000Flip_17(),
		new ProjectionNegY090Norm_18(),
		new ProjectionNegY090Flip_19(),
		new ProjectionNegY180Norm_20(),
		new ProjectionNegY180Flip_21(),
		new ProjectionNegY270Norm_22(),
		new ProjectionNegY270Flip_23(),
		new ProjectionPosY000Norm_24(),
		new ProjectionPosY000Flip_25(),
		new ProjectionPosY090Norm_26(),
		new ProjectionPosY090Flip_27(),
		new ProjectionPosY180Norm_28(),
		new ProjectionPosY180Flip_29(),
		new ProjectionPosY270Norm_30(),
		new ProjectionPosY270Flip_31(),
		new ProjectionNegZ000Norm_32(),
		new ProjectionNegZ000Flip_33(),
		new ProjectionNegZ090Norm_34(),
		new ProjectionNegZ090Flip_35(),
		new ProjectionNegZ180Norm_36(),
		new ProjectionNegZ180Flip_37(),
		new ProjectionNegZ270Norm_38(),
		new ProjectionNegZ270Flip_39(),
		new ProjectionPosZ000Norm_40(),
		new ProjectionPosZ000Flip_41(),
		new ProjectionPosZ090Norm_42(),
		new ProjectionPosZ090Flip_43(),
		new ProjectionPosZ180Norm_44(),
		new ProjectionPosZ180Flip_45(),
		new ProjectionPosZ270Norm_46(),
		new ProjectionPosZ270Flip_47());

	public static ICubeArrayProjection Neutral => Get(OrthoAxis.PosY000Norm_24);

	public static ICubeArrayProjection Get(OrthoAxis orientation)
	{
		Debug.Assert(Enumeration.IsDefined(orientation));

		return Instances[(int)orientation];
	}

	#region Private Classes

	private abstract class AbstractProjection : ICubeArrayProjection
	{
		/// <inheritdoc />
		public abstract OrthoAxis Orientation { get; }

		/// <inheritdoc />
		public T Get<T>(ICubeArrayView<T> array, Index3D index)
		{
			Debug.Assert(array != null);

			return this.Get(array, index.X, index.Y, index.Z);
		}

		/// <inheritdoc />
		public abstract T Get<T>(ICubeArrayView<T> array, int x, int y, int z);

		/// <inheritdoc />
		public IEnumerable<IndexValuePair<Index3D, T>> GetEnumerable<T>(ICubeArrayView<T> array)
		{
			Debug.Assert(array != null);

			foreach (var index in array.LowerBounds.Range(array.Dimensions))
			{
				yield return IndexValuePair.New(index, this.Get(array, index.X, index.Y, index.Z));
			}
		}

		/// <inheritdoc />
		public override string ToString() => this.Orientation.ToString();
	}

	private class ProjectionNegX090Norm_02 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX090Norm_02;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, x, z];
		}
	}

	// flip z
	private class ProjectionNegX270Flip_07 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX270Flip_07;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, x, max - z];
		}
	}

	private class ProjectionNegX180Norm_04 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX180Norm_04;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, max - z, x];
		}
	}

	// flip z
	private class ProjectionNegX180Flip_05 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX180Flip_05;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, max - z, max - x];
		}
	}

	private class ProjectionNegX270Norm_06 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX270Norm_06;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, max - x, max - z];
		}
	}

	// flip z
	private class ProjectionNegX090Flip_03 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX090Flip_03;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, max - x, z];
		}
	}

	private class ProjectionNegX000Norm_00 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX000Norm_00;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, z, max - x];
		}
	}

	// flip z
	private class ProjectionNegX000Flip_01 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegX000Flip_01;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - y, z, x];
		}
	}

	private class ProjectionPosX270Norm_14 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX270Norm_14;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[y, max - x, z];
		}
	}

	// flip z
	private class ProjectionPosX090Flip_11 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX090Flip_11;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[y, max - x, max - z];
		}
	}

	private class ProjectionPosX000Norm_08 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX000Norm_08;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			return array[y, z, x];
		}
	}

	// flip z
	private class ProjectionPosX000Flip_09 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX000Flip_09;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[y, z, max - x];
		}
	}

	private class ProjectionPosX090Norm_10 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX090Norm_10;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[y, x, max - z];
		}
	}

	// flip z
	private class ProjectionPosX270Flip_15 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX270Flip_15;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			return array[y, x, z];
		}
	}

	private class ProjectionPosX180Norm_12 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX180Norm_12;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[y, max - z, max - x];
		}
	}

	// flip z
	private class ProjectionPosX180Flip_13 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosX180Flip_13;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[y, max - z, x];
		}
	}

	private class ProjectionNegY000Norm_16 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY000Norm_16;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[x, max - y, max - z];
		}
	}

	// flip z
	private class ProjectionNegY180Flip_21 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY180Flip_21;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[x, max - y, z];
		}
	}

	private class ProjectionNegY090Norm_18 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY090Norm_18;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, max - y, max - x];
		}
	}

	// flip z
	private class ProjectionNegY090Flip_19 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY090Flip_19;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, max - y, x];
		}
	}

	private class ProjectionNegY180Norm_20 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY180Norm_20;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, max - y, z];
		}
	}

	// flip z
	private class ProjectionNegY000Flip_17 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY000Flip_17;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, max - y, max - z];
		}
	}

	private class ProjectionNegY270Norm_22 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY270Norm_22;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[z, max - y, x];
		}
	}

	// flip z
	private class ProjectionNegY270Flip_23 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegY270Flip_23;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[z, max - y, max - x];
		}
	}

	private class ProjectionPosY000Norm_24 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY000Norm_24;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			return array[x, y, z];
		}
	}

	// flip z
	private class ProjectionPosY180Flip_29 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY180Flip_29;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[x, y, max - z];
		}
	}

	private class ProjectionPosY090Norm_26 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY090Norm_26;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, y, x];
		}
	}

	// flip z
	private class ProjectionPosY090Flip_27 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY090Flip_27;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, y, max - x];
		}
	}

	private class ProjectionPosY180Norm_28 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY180Norm_28;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, y, max - z];
		}
	}

	// flip z
	private class ProjectionPosY000Flip_25 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY000Flip_25;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, y, z];
		}
	}

	private class ProjectionPosY270Norm_30 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY270Norm_30;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[z, y, max - x];
		}
	}

	// flip z
	private class ProjectionPosY270Flip_31 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosY270Flip_31;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			return array[z, y, x];
		}
	}

	private class ProjectionNegZ000Norm_32 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ000Norm_32;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[x, z, max - y];
		}
	}

	// flip x
	private class ProjectionNegZ000Flip_33 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ000Flip_33;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, z, max - y];
		}
	}

	private class ProjectionNegZ090Norm_34 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ090Norm_34;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, x, max - y];
		}
	}

	// flip x
	private class ProjectionNegZ270Flip_39 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ270Flip_39;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[z, x, max - y];
		}
	}

	private class ProjectionNegZ180Norm_36 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ180Norm_36;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, max - z, max - y];
		}
	}

	// flip x
	private class ProjectionNegZ180Flip_37 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ180Flip_37;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[x, max - z, max - y];
		}
	}

	private class ProjectionNegZ270Norm_38 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ270Norm_38;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[z, max - x, max - y];
		}
	}

	// flip x
	private class ProjectionNegZ090Flip_35 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.NegZ090Flip_35;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, max - x, max - y];
		}
	}

	private class ProjectionPosZ180Norm_44 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ180Norm_44;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[x, max - z, y];
		}
	}

	// flip x
	private class ProjectionPosZ180Flip_45 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ180Flip_45;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, max - z, y];
		}
	}

	private class ProjectionPosZ270Norm_46 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ270Norm_46;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, max - x, y];
		}
	}

	// flip x
	private class ProjectionPosZ090Flip_43 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ090Flip_43;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[z, max - x, y];
		}
	}

	private class ProjectionPosZ000Norm_40 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ000Norm_40;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - x, z, y];
		}
	}

	// flip x
	private class ProjectionPosZ000Flip_41 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ000Flip_41;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			return array[x, z, y];
		}
	}

	private class ProjectionPosZ090Norm_42 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ090Norm_42;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			return array[z, x, y];
		}
	}

	// flip x
	private class ProjectionPosZ270Flip_47 : AbstractProjection
	{
		/// <inheritdoc />
		public override OrthoAxis Orientation => OrthoAxis.PosZ270Flip_47;

		/// <inheritdoc />
		public override T Get<T>(ICubeArrayView<T> array, int x, int y, int z)
		{
			Debug.Assert(array != null);

			int max = array.SideLength - 1;
			return array[max - z, x, y];
		}
	}

	#endregion
}
