﻿namespace HQS.VUE.OldEngine;

internal class ConstantCubeArrayView<T> : ICubeArrayView<T>
{
	public ConstantCubeArrayView(int sizeExponent, T value)
	{
		Debug.Assert(sizeExponent >= 0);

		this.SizeExponent = sizeExponent;
		this.Value = value;
	}

	public T Value { get; }

	/// <inheritdoc />
	public Index3D Dimensions => new Index3D(this.SideLength);

	/// <inheritdoc />
	public Index3D LowerBounds => Index3D.Zero;

	/// <inheritdoc />
	public Index3D UpperBounds => new Index3D(this.SideLength - 1);

	/// <inheritdoc />
	public int SizeExponent { get; }

	/// <inheritdoc />
	public int SideLength => BinaryArray.GetSideLength(this.SizeExponent);

	/// <inheritdoc />
	public int Length
	{
		get
		{
			int sideLength = this.SideLength;
			return sideLength * sideLength * sideLength;
		}
	}

	/// <inheritdoc />
	public T this[int index]
	{
		get
		{
			ICubeArrayViewContracts.Indexer(this, index);
			return this.Value;
		}
	}

	/// <inheritdoc />
	public T this[int x, int y, int z]
	{
		get
		{
			ICubeArrayViewContracts.Indexer(this, x, y, z);
			return this.Value;
		}
	}

	/// <inheritdoc />
	public T this[Index3D index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);
			return this.Value;
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index3D, T>> GetEnumerator()
	{
		foreach (var index in this.LowerBounds.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this.Value);
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
