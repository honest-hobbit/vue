﻿namespace HQS.VUE.OldEngine;

internal static class BinaryArray
{
	public static int GetSideLength(int sizeExponent)
	{
		Debug.Assert(sizeExponent >= 0);

		return MathUtility.PowerOf2(sizeExponent);
	}

	public static int GetSquareLength(int sizeExponent)
	{
		Debug.Assert(sizeExponent >= 0);

		return MathUtility.IntegerPower(GetSideLength(sizeExponent), Index2D.Zero.Rank);
	}

	public static int GetCubeLength(int sizeExponent)
	{
		Debug.Assert(sizeExponent >= 0);

		return MathUtility.IntegerPower(GetSideLength(sizeExponent), Index3D.Zero.Rank);
	}
}
