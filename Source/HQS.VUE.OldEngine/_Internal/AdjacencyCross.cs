﻿namespace HQS.VUE.OldEngine;

internal static class AdjacencyCross
{
	public const int Center = 0;

	public const int NegX = 1;

	public const int PosX = 2;

	public const int NegY = 3;

	public const int PosY = 4;

	public const int NegZ = 5;

	public const int PosZ = 6;
}
