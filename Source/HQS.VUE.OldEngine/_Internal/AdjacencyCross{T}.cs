﻿namespace HQS.VUE.OldEngine;

internal class AdjacencyCross<T> : IReadOnlyList<T>
{
	public AdjacencyCross()
	{
	}

	public AdjacencyCross(T value)
	{
		this.SetAllTo(value);
	}

	/// <inheritdoc />
	public T this[int index]
	{
		get
		{
			IReadOnlyListContracts.Indexer(this, index);

			return index switch
			{
				AdjacencyCross.Center => this.Center,
				AdjacencyCross.NegX => this.NegX,
				AdjacencyCross.PosX => this.PosX,
				AdjacencyCross.NegY => this.NegY,
				AdjacencyCross.PosY => this.PosY,
				AdjacencyCross.NegZ => this.NegZ,
				AdjacencyCross.PosZ => this.PosZ,
				_ => throw new IndexOutOfRangeException(),
			};
		}

		set
		{
			IReadOnlyListContracts.Indexer(this, index);

			switch (index)
			{
				case AdjacencyCross.Center: this.Center = value; break;
				case AdjacencyCross.NegX: this.NegX = value; break;
				case AdjacencyCross.PosX: this.PosX = value; break;
				case AdjacencyCross.NegY: this.NegY = value; break;
				case AdjacencyCross.PosY: this.PosY = value; break;
				case AdjacencyCross.NegZ: this.NegZ = value; break;
				case AdjacencyCross.PosZ: this.PosZ = value; break;
				default: throw new IndexOutOfRangeException();
			}
		}
	}

	/// <inheritdoc />
	public int Count => 7;

	public T Center { get; set; }

	public T NegX { get; set; }

	public T PosX { get; set; }

	public T NegY { get; set; }

	public T PosY { get; set; }

	public T NegZ { get; set; }

	public T PosZ { get; set; }

	public void SetAllTo(T value)
	{
		this.Center = value;
		this.NegX = value;
		this.PosX = value;
		this.NegY = value;
		this.PosY = value;
		this.NegZ = value;
		this.PosZ = value;
	}

	public void Clear() => this.SetAllTo(default);

	/// <inheritdoc />
	public IEnumerator<T> GetEnumerator()
	{
		yield return this.Center;
		yield return this.NegX;
		yield return this.PosX;
		yield return this.NegY;
		yield return this.PosY;
		yield return this.NegZ;
		yield return this.PosZ;
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
