﻿namespace HQS.VUE.OldEngine;

internal class CubeArrayProjection<T> : ICubeArrayView<T>
{
	private ICubeArrayProjection projection = CubeArrayProjection.Neutral;

	/// <inheritdoc />
	public T this[int index] => this[CubeArray.FromArrayIndex(this.SizeExponent, index)];

	/// <inheritdoc />
	public T this[Index3D index] => this.projection.Get(this.Source, index);

	/// <inheritdoc />
	public T this[int x, int y, int z] => this.projection.Get(this.Source, x, y, z);

	/// <inheritdoc />
	public int SizeExponent => this.Source.SizeExponent;

	/// <inheritdoc />
	public int SideLength => this.Source.SideLength;

	/// <inheritdoc />
	public int Length => this.Source.Length;

	/// <inheritdoc />
	public Index3D Dimensions => this.Source.Dimensions;

	/// <inheritdoc />
	public Index3D LowerBounds => this.Source.LowerBounds;

	/// <inheritdoc />
	public Index3D UpperBounds => this.Source.UpperBounds;

	public ICubeArrayView<T> Source { get; set; }

	public OrthoAxis Orientation
	{
		get => this.projection.Orientation;
		set => this.projection = CubeArrayProjection.Get(value);
	}

	public void SetProjection(ICubeArrayView<T> source, OrthoAxis orientation)
	{
		this.Source = source;
		this.Orientation = orientation;
	}

	public void Clear()
	{
		this.Source = null;
		this.projection = CubeArrayProjection.Neutral;
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index3D, T>> GetEnumerator() => this.projection.GetEnumerable(this.Source).GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
