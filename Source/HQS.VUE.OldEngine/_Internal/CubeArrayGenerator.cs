﻿namespace HQS.VUE.OldEngine;

internal static class CubeArrayGenerator
{
	public static void GenerateCheckered<T>(CubeArray<T> array, T value, T other)
	{
		Debug.Assert(array != null);

		bool alternate = false;
		var max = array.UpperBounds;
		for (int iX = 0; iX <= max.X; iX++)
		{
			for (int iY = 0; iY <= max.Y; iY++)
			{
				for (int iZ = 0; iZ <= max.Z; iZ++)
				{
					array[iX, iY, iZ] = alternate ? other : value;
					alternate = !alternate;
				}

				alternate = !alternate;
			}

			alternate = !alternate;
		}
	}

	public static void GenerateCheckeredSurface<T>(CubeArray<T> array, T value, T other)
	{
		Debug.Assert(array != null);

		GenerateCheckered(array, value, other);
		int max = array.SideLength - 2;
		for (int iX = 1; iX <= max; iX++)
		{
			for (int iY = 1; iY <= max; iY++)
			{
				for (int iZ = 1; iZ <= max; iZ++)
				{
					array[iX, iY, iZ] = value;
				}
			}
		}
	}
}
