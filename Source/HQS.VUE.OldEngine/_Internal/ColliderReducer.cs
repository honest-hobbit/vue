﻿namespace HQS.VUE.OldEngine;

internal class ColliderReducer
{
	private readonly CubeArray<bool> isSolid;

	private readonly int sideMax;

	public ColliderReducer(int sizeExponent)
	{
		Debug.Assert(sizeExponent >= 0);

		this.isSolid = new CubeArray<bool>(sizeExponent);
		this.sideMax = this.isSolid.SideLength - 1;
	}

	public int SizeExponent => this.isSolid.SizeExponent;

	public bool this[int x, int y, int z] => this.isSolid[x, y, z];

	public bool IsSolid(int x, int y, int z) => this.isSolid[x, y, z];

	public bool IsFullCube()
	{
		for (int index = 0; index < this.isSolid.Length; index++)
		{
			if (!this.isSolid[index])
			{
				return false;
			}
		}

		return true;
	}

	public void SetTo(ICubeArrayView<byte> voxels, int threshold)
	{
		Debug.Assert(voxels != null);
		Debug.Assert(voxels.SizeExponent == this.SizeExponent);
		Debug.Assert(threshold >= 0);

		for (int index = 0; index < voxels.Length; index++)
		{
			this.isSolid[index] = voxels[index] != 0;  // !voxels[index].IsEmpty;
		}

		if (threshold == 0)
		{
			return;
		}

		while (this.TryFill(threshold))
		{
		}
	}

	private bool TryFill(int threshold)
	{
		Debug.Assert(threshold >= 1);

		bool repeatFilling = false;
		for (int iX = 0; iX <= this.sideMax; iX++)
		{
			for (int iY = 0; iY <= this.sideMax; iY++)
			{
				for (int iZ = 0; iZ <= this.sideMax; iZ++)
				{
					if (this.TryFill(iX, iY, iZ, threshold))
					{
						repeatFilling = true;
					}
				}
			}
		}

		return repeatFilling;
	}

	private bool TryFill(int x, int y, int z, int threshold)
	{
		Debug.Assert(threshold >= 1);

		if (this.isSolid[x, y, z])
		{
			return false;
		}

		if (this.TryFillX(x, y, z, threshold))
		{
			return true;
		}

		if (this.TryFillY(x, y, z, threshold))
		{
			return true;
		}

		if (this.TryFillZ(x, y, z, threshold))
		{
			return true;
		}

		return false;
	}

	private bool TryFillX(int x, int y, int z, int threshold)
	{
		Debug.Assert(threshold >= 1);

		if (x > 0 && x < this.sideMax)
		{
			bool xNegIsSolid = this.isSolid[x - 1, y, z];
			bool xPosIsSolid = this.isSolid[x + 1, y, z];

			if (xNegIsSolid && xPosIsSolid)
			{
				this.isSolid[x, y, z] = true;
				return true;
			}

			if (!xNegIsSolid && !xPosIsSolid)
			{
				return false;
			}

			int increment = xNegIsSolid ? 1 : -1;
			int limit = xNegIsSolid ? this.sideMax + 1 : -1;
			int xOffset = x;

			for (int count = 1; count <= threshold; count++)
			{
				xOffset += increment;
				if (xOffset == limit)
				{
					return false;
				}

				if (this.isSolid[xOffset, y, z])
				{
					for (int iX = x; iX < xOffset; iX += increment)
					{
						this.isSolid[iX, y, z] = true;
					}

					return true;
				}
			}
		}

		return false;
	}

	private bool TryFillY(int x, int y, int z, int threshold)
	{
		Debug.Assert(threshold >= 1);

		if (y > 0 && y < this.sideMax)
		{
			bool yNegIsSolid = this.isSolid[x, y - 1, z];
			bool yPosIsSolid = this.isSolid[x, y + 1, z];

			if (yNegIsSolid && yPosIsSolid)
			{
				this.isSolid[x, y, z] = true;
				return true;
			}

			if (!yNegIsSolid && !yPosIsSolid)
			{
				return false;
			}

			int increment = yNegIsSolid ? 1 : -1;
			int limit = yNegIsSolid ? this.sideMax + 1 : -1;
			int yOffset = y;

			for (int count = 1; count <= threshold; count++)
			{
				yOffset += increment;
				if (yOffset == limit)
				{
					return false;
				}

				if (this.isSolid[x, yOffset, z])
				{
					for (int iY = y; iY < yOffset; iY += increment)
					{
						this.isSolid[x, iY, z] = true;
					}

					return true;
				}
			}
		}

		return false;
	}

	private bool TryFillZ(int x, int y, int z, int threshold)
	{
		Debug.Assert(threshold >= 1);

		if (z > 0 && z < this.sideMax)
		{
			bool zNegIsSolid = this.isSolid[x, y, z - 1];
			bool zPosIsSolid = this.isSolid[x, y, z + 1];

			if (zNegIsSolid && zPosIsSolid)
			{
				this.isSolid[x, y, z] = true;
				return true;
			}

			if (!zNegIsSolid && !zPosIsSolid)
			{
				return false;
			}

			int increment = zNegIsSolid ? 1 : -1;
			int limit = zNegIsSolid ? this.sideMax + 1 : -1;
			int zOffset = z;

			for (int count = 1; count <= threshold; count++)
			{
				zOffset += increment;
				if (zOffset == limit)
				{
					return false;
				}

				if (this.isSolid[x, y, zOffset])
				{
					for (int iZ = z; iZ < zOffset; iZ += increment)
					{
						this.isSolid[x, y, iZ] = true;
					}

					return true;
				}
			}
		}

		return false;
	}
}
