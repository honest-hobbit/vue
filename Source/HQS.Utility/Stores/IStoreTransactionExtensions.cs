﻿namespace HQS.Utility.Stores;

public static class IStoreTransactionExtensions
{
	public static ConfigStore<TEntity> Config<TEntity>(this IStoreTransaction store)
		where TEntity : class, IConfigEntity, new()
	{
		Debug.Assert(store != null);

		return new ConfigStore<TEntity>(store);
	}

	#region Get Methods

	public static TEntity Get<TKey, TEntity>(this IStoreTransaction store, TKey key)
		where TEntity : class, IKeyed<TKey>, new()
	{
		GetMethodContracts(store, key);

		if (store.TryGet(key, out TEntity entity))
		{
			return entity;
		}
		else
		{
			throw new InvalidOperationException($"No entity found with key: {key}");
		}
	}

	public static TEntity GetOrNull<TKey, TEntity>(this IStoreTransaction store, TKey key)
		where TEntity : class, IKeyed<TKey>, new()
	{
		GetMethodContracts(store, key);

		return store.TryGet(key, out TEntity entity) ? entity : null;
	}

	public static Try<TEntity> TryGet<TKey, TEntity>(this IStoreTransaction store, TKey key)
		where TEntity : class, IKeyed<TKey>, new()
	{
		GetMethodContracts(store, key);

		return store.TryGet(key, out TEntity entity) ? Try.Value(entity) : Try.None<TEntity>();
	}

	#endregion

	#region Params Overloads of 'Many' Methods

	public static void AddMany<TEntity>(this IStoreTransaction store, params TEntity[] entities)
		where TEntity : class, new()
	{
		ParamsMethodContracts(store, entities);

		store.AddMany(entities);
	}

	public static void AddOrUpdateMany<TEntity>(this IStoreTransaction store, params TEntity[] entities)
		where TEntity : class, new()
	{
		ParamsMethodContracts(store, entities);

		store.AddOrUpdateMany(entities);
	}

	public static void UpdateMany<TEntity>(this IStoreTransaction store, params TEntity[] entities)
		where TEntity : class, new()
	{
		ParamsMethodContracts(store, entities);

		store.UpdateMany(entities);
	}

	public static void RemoveMany<TEntity>(this IStoreTransaction store, params TEntity[] entities)
		where TEntity : class, new()
	{
		ParamsMethodContracts(store, entities);

		store.RemoveMany(entities);
	}

	public static void RemoveManyKeys<TKey, TEntity>(this IStoreTransaction store, params TKey[] keys)
		where TEntity : class, IKeyed<TKey>, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(keys.AllAndSelfNotNull());

		store.RemoveManyKeys<TKey, TEntity>(keys);
	}

	#endregion

	#region Initialize Methods

	public static void Initialize<T1, T2>(this IStoreTransaction store)
		where T1 : class, new()
		where T2 : class, new()
	{
		InitializeContracts(store);

		store.RunInTransaction(transaction =>
		{
			transaction.Initialize<T1>();
			transaction.Initialize<T2>();
		});
	}

	public static void Initialize<T1, T2, T3>(this IStoreTransaction store)
		where T1 : class, new()
		where T2 : class, new()
		where T3 : class, new()
	{
		InitializeContracts(store);

		store.RunInTransaction(transaction =>
		{
			transaction.Initialize<T1>();
			transaction.Initialize<T2>();
			transaction.Initialize<T3>();
		});
	}

	public static void Initialize<T1, T2, T3, T4>(this IStoreTransaction store)
		where T1 : class, new()
		where T2 : class, new()
		where T3 : class, new()
		where T4 : class, new()
	{
		InitializeContracts(store);

		store.RunInTransaction(transaction =>
		{
			transaction.Initialize<T1>();
			transaction.Initialize<T2>();
			transaction.Initialize<T3>();
			transaction.Initialize<T4>();
		});
	}

	public static void Initialize<T1, T2, T3, T4, T5>(this IStoreTransaction store)
		where T1 : class, new()
		where T2 : class, new()
		where T3 : class, new()
		where T4 : class, new()
		where T5 : class, new()
	{
		InitializeContracts(store);

		store.RunInTransaction(transaction =>
		{
			transaction.Initialize<T1>();
			transaction.Initialize<T2>();
			transaction.Initialize<T3>();
			transaction.Initialize<T4>();
			transaction.Initialize<T5>();
		});
	}

	public static void Initialize<T1, T2, T3, T4, T5, T6>(this IStoreTransaction store)
		where T1 : class, new()
		where T2 : class, new()
		where T3 : class, new()
		where T4 : class, new()
		where T5 : class, new()
		where T6 : class, new()
	{
		InitializeContracts(store);

		store.RunInTransaction(transaction =>
		{
			transaction.Initialize<T1>();
			transaction.Initialize<T2>();
			transaction.Initialize<T3>();
			transaction.Initialize<T4>();
			transaction.Initialize<T5>();
			transaction.Initialize<T6>();
		});
	}

	public static void Initialize<T1, T2, T3, T4, T5, T6, T7>(this IStoreTransaction store)
		where T1 : class, new()
		where T2 : class, new()
		where T3 : class, new()
		where T4 : class, new()
		where T5 : class, new()
		where T6 : class, new()
		where T7 : class, new()
	{
		InitializeContracts(store);

		store.RunInTransaction(transaction =>
		{
			transaction.Initialize<T1>();
			transaction.Initialize<T2>();
			transaction.Initialize<T3>();
			transaction.Initialize<T4>();
			transaction.Initialize<T5>();
			transaction.Initialize<T6>();
			transaction.Initialize<T7>();
		});
	}

	public static void Initialize<T1, T2, T3, T4, T5, T6, T7, T8>(this IStoreTransaction store)
		where T1 : class, new()
		where T2 : class, new()
		where T3 : class, new()
		where T4 : class, new()
		where T5 : class, new()
		where T6 : class, new()
		where T7 : class, new()
		where T8 : class, new()
	{
		InitializeContracts(store);

		store.RunInTransaction(transaction =>
		{
			transaction.Initialize<T1>();
			transaction.Initialize<T2>();
			transaction.Initialize<T3>();
			transaction.Initialize<T4>();
			transaction.Initialize<T5>();
			transaction.Initialize<T6>();
			transaction.Initialize<T7>();
			transaction.Initialize<T8>();
		});
	}

	public static void InitializeMany(this IStoreTransaction store, params Type[] types) =>
		store.InitializeMany((IEnumerable<Type>)types);

	public static void InitializeMany(this IStoreTransaction store, IEnumerable<Type> types)
	{
		InitializeContracts(store);
		Debug.Assert(types.AllAndSelfNotNull());

		store.RunInTransaction(transaction =>
		{
			foreach (var type in types)
			{
				transaction.Initialize(type);
			}
		});
	}

	#endregion

	#region Private Contracts

	[Conditional(CompilationSymbol.Debug)]
	private static void GetMethodContracts<TKey>(IStoreTransaction store, TKey key)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(key != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	private static void ParamsMethodContracts<TEntity>(IStoreTransaction store, TEntity[] entities)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entities.AllAndSelfNotNull());
	}

	[Conditional(CompilationSymbol.Debug)]
	private static void InitializeContracts(IStoreTransaction store)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
	}

	#endregion
}
