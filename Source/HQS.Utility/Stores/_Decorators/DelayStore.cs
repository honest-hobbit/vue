﻿namespace HQS.Utility.Stores;

public class DelayStore : AbstractDisposable, IFileStore
{
	private readonly IFileStore store;

	private readonly double minMilliseconds;

	private readonly double maxMilliseconds;

	private readonly Random random;

	public DelayStore(IFileStore store, TimeSpan maxDelay)
		: this(store, TimeSpan.Zero, maxDelay, new Random())
	{
	}

	public DelayStore(IFileStore store, TimeSpan maxDelay, Random random)
		: this(store, TimeSpan.Zero, maxDelay, random)
	{
	}

	public DelayStore(IFileStore store, TimeSpan minDelay, TimeSpan maxDelay)
		: this(store, minDelay, maxDelay, new Random())
	{
	}

	public DelayStore(IFileStore store, TimeSpan minDelay, TimeSpan maxDelay, Random random)
	{
		Debug.Assert(store != null);
		Debug.Assert(minDelay.IsFiniteDuration());
		Debug.Assert(maxDelay >= minDelay);
		Debug.Assert(random != null);

		this.store = store;
		this.minMilliseconds = minDelay.TotalMilliseconds;
		this.maxMilliseconds = maxDelay.TotalMilliseconds;
		this.random = random;
	}

	/// <inheritdoc />
	public string Path => this.store.Path;

	/// <inheritdoc />
	public void MoveFileTo(string path)
	{
		IFileStoreContracts.MoveFileTo(this, path);

		this.Delay();
		this.store.MoveFileTo(path);
	}

	/// <inheritdoc />
	public void CopyFileTo(string path, bool overwrite = false)
	{
		IFileStoreContracts.CopyFileTo(this, path);

		this.Delay();
		this.store.CopyFileTo(path, overwrite);
	}

	/// <inheritdoc />
	public void DisposeAndDeleteFile()
	{
		if (this.TrySetDisposed())
		{
			this.store.DisposeAndDeleteFile();
			this.ManagedDisposal();
		}
	}

	/// <inheritdoc />
	public void Initialize<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Initialize(this);

		this.Delay();
		this.store.Initialize<TEntity>();
	}

	/// <inheritdoc />
	public void Initialize(Type type)
	{
		IStoreTransactionContracts.Initialize(this, type);

		this.Delay();
		this.store.Initialize(type);
	}

	/// <inheritdoc />
	public IEnumerable<TEntity> All<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.All(this);

		this.Delay();
		return this.store.All<TEntity>();
	}

	/// <inheritdoc />
	public IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Where(this, predicate);

		this.Delay();
		return this.store.Where(predicate);
	}

	/// <inheritdoc />
	public bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.TryGet(this, key);

		this.Delay();
		return this.store.TryGet(key, out entity);
	}

	/// <inheritdoc />
	public void Add<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Add(this, entity);

		this.Delay();
		this.store.Add(entity);
	}

	/// <inheritdoc />
	public void AddMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddMany(this, entities);

		this.Delay();
		this.store.AddMany(entities);
	}

	/// <inheritdoc />
	public void AddOrUpdate<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddOrUpdate(this, entity);

		this.Delay();
		this.store.AddOrUpdate(entity);
	}

	/// <inheritdoc />
	public void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddOrUpdateMany(this, entities);

		this.Delay();
		this.store.AddOrUpdateMany(entities);
	}

	/// <inheritdoc />
	public void Update<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Update(this, entity);

		this.Delay();
		this.store.Update(entity);
	}

	/// <inheritdoc />
	public void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.UpdateMany(this, entities);

		this.Delay();
		this.store.UpdateMany(entities);
	}

	/// <inheritdoc />
	public void Remove<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Remove(this, entity);

		this.Delay();
		this.store.Remove(entity);
	}

	/// <inheritdoc />
	public void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.RemoveMany(this, entities);

		this.Delay();
		this.store.RemoveMany(entities);
	}

	/// <inheritdoc />
	public void RemoveAll<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.RemoveAll(this);

		this.Delay();
		this.store.RemoveAll<TEntity>();
	}

	/// <inheritdoc />
	public void RemoveKey<TKey, TEntity>(TKey key)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.RemoveKey(this, key);

		this.Delay();
		this.store.RemoveKey<TKey, TEntity>(key);
	}

	/// <inheritdoc />
	public void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.RemoveManyKeys(this, keys);

		this.Delay();
		this.store.RemoveManyKeys<TKey, TEntity>(keys);
	}

	/// <inheritdoc />
	public void RunInTransaction(Action<IStoreTransaction> action)
	{
		IStoreTransactionContracts.RunInTransaction(this, action);

		this.Delay();
		this.RunInTransaction(action);
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.store.Dispose();

	private void Delay() => Thread.Sleep(TimeSpan.FromMilliseconds(
		this.random.NextDouble(this.minMilliseconds, this.maxMilliseconds)));
}
