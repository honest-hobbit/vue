﻿namespace HQS.Utility.Stores;

public class TimeoutStore : AbstractStoreDecorator
{
	private readonly object padlock = new object();

	private readonly TimeSpan timeout;

	private readonly Func<IFileStore> createStore;

	private IFileStore store;

	private int count;

	public TimeoutStore(TimeSpan timeout, Func<IFileStore> createStore)
	{
		Debug.Assert(timeout.IsDuration());
		Debug.Assert(createStore != null);

		this.timeout = timeout;
		this.createStore = createStore;

		try
		{
			this.SetPath(this.GetStore().Path);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void MoveFileTo(string path)
	{
		IFileStoreContracts.MoveFileTo(this, path);

		try
		{
			var store = this.GetStore();
			store.MoveFileTo(path);
			this.SetPath(store.Path);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void CopyFileTo(string path, bool overwrite = false)
	{
		IFileStoreContracts.CopyFileTo(this, path);

		try
		{
			this.GetStore().CopyFileTo(path, overwrite);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void DisposeAndDeleteFile()
	{
		if (this.TrySetDisposed())
		{
			this.GetStore().DisposeAndDeleteFile();
			this.ManagedDisposal();
		}
	}

	/// <inheritdoc />
	public sealed override void Initialize<TEntity>()
	{
		IStoreTransactionContracts.Initialize(this);

		try
		{
			this.GetStore().Initialize<TEntity>();
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void Initialize(Type type)
	{
		IStoreTransactionContracts.Initialize(this, type);

		try
		{
			this.GetStore().Initialize(type);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override IEnumerable<TEntity> All<TEntity>()
	{
		IStoreTransactionContracts.All(this);

		try
		{
			return this.GetStore().All<TEntity>();
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
	{
		IStoreTransactionContracts.Where(this, predicate);

		try
		{
			return this.GetStore().Where(predicate);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
	{
		IStoreTransactionContracts.TryGet(this, key);

		try
		{
			return this.GetStore().TryGet(key, out entity);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void Add<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Add(this, entity);

		try
		{
			this.GetStore().Add(entity);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void AddMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.AddMany(this, entities);

		try
		{
			this.GetStore().AddMany(entities);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void AddOrUpdate<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.AddOrUpdate(this, entity);

		try
		{
			this.GetStore().AddOrUpdate(entity);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.AddOrUpdateMany(this, entities);

		try
		{
			this.GetStore().AddOrUpdateMany(entities);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void Update<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Update(this, entity);

		try
		{
			this.GetStore().Update(entity);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.UpdateMany(this, entities);

		try
		{
			this.GetStore().UpdateMany(entities);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void Remove<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Remove(this, entity);

		try
		{
			this.GetStore().Remove(entity);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.RemoveMany(this, entities);

		try
		{
			this.GetStore().RemoveMany(entities);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveAll<TEntity>()
	{
		IStoreTransactionContracts.RemoveAll(this);

		try
		{
			this.GetStore().RemoveAll<TEntity>();
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveKey<TKey, TEntity>(TKey key)
	{
		IStoreTransactionContracts.RemoveKey(this, key);

		try
		{
			this.GetStore().RemoveKey<TKey, TEntity>(key);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
	{
		IStoreTransactionContracts.RemoveManyKeys(this, keys);

		try
		{
			this.GetStore().RemoveManyKeys<TKey, TEntity>(keys);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public sealed override void RunInTransaction(Action<IStoreTransaction> action)
	{
		IStoreTransactionContracts.RunInTransaction(this, action);

		try
		{
			this.GetStore().RunInTransaction(action);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		lock (this.padlock)
		{
			if (this.store != null)
			{
				this.count = 0;
				try
				{
					this.store.Dispose();
				}
				finally
				{
					this.store = null;
				}
			}
		}
	}

	private IFileStore GetStore()
	{
		lock (this.padlock)
		{
			this.count++;
			try
			{
				return this.store ??= this.createStore();
			}
			catch (Exception)
			{
				this.count--;
				throw;
			}
		}
	}

	private async void StartTimeout()
	{
		if (this.timeout == Duration.Span.InfiniteTimeout)
		{
			return;
		}

		if (this.timeout != Duration.Span.InstantTimeout)
		{
			await Task.Delay(this.timeout).DontMarshallContext();
		}

		lock (this.padlock)
		{
			if (this.count == 0)
			{
				return;
			}

			this.count--;
			if (this.count == 0)
			{
				try
				{
					this.store.Dispose();
				}
				finally
				{
					this.store = null;
				}
			}
		}
	}
}
