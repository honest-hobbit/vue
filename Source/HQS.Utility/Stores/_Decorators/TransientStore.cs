﻿namespace HQS.Utility.Stores;

public class TransientStore : AbstractStoreDecorator
{
	private readonly Func<IFileStore> createStore;

	public TransientStore(Func<IFileStore> createStore)
	{
		Debug.Assert(createStore != null);

		this.createStore = createStore;

		using var store = this.createStore();
		this.SetPath(store.Path);
	}

	/// <inheritdoc />
	public sealed override void MoveFileTo(string path)
	{
		IFileStoreContracts.MoveFileTo(this, path);

		using var store = this.createStore();
		store.MoveFileTo(path);
		this.SetPath(store.Path);
	}

	/// <inheritdoc />
	public sealed override void CopyFileTo(string path, bool overwrite = false)
	{
		IFileStoreContracts.CopyFileTo(this, path);

		using var store = this.createStore();
		store.CopyFileTo(path, overwrite);
	}

	/// <inheritdoc />
	public sealed override void DisposeAndDeleteFile()
	{
		if (this.TrySetDisposed())
		{
			using var store = this.createStore();
			store.DisposeAndDeleteFile();

			this.ManagedDisposal();
		}
	}

	/// <inheritdoc />
	public sealed override void Initialize<TEntity>()
	{
		IStoreTransactionContracts.Initialize(this);

		using var store = this.createStore();
		store.Initialize<TEntity>();
	}

	/// <inheritdoc />
	public sealed override void Initialize(Type type)
	{
		IStoreTransactionContracts.Initialize(this, type);

		using var store = this.createStore();
		store.Initialize(type);
	}

	/// <inheritdoc />
	public sealed override IEnumerable<TEntity> All<TEntity>()
	{
		IStoreTransactionContracts.All(this);

		using var store = this.createStore();
		return store.All<TEntity>();
	}

	/// <inheritdoc />
	public sealed override IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
	{
		IStoreTransactionContracts.Where(this, predicate);

		using var store = this.createStore();
		return store.Where(predicate);
	}

	/// <inheritdoc />
	public sealed override bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
	{
		IStoreTransactionContracts.TryGet(this, key);

		using var store = this.createStore();
		return store.TryGet(key, out entity);
	}

	/// <inheritdoc />
	public sealed override void Add<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Add(this, entity);

		using var store = this.createStore();
		store.Add(entity);
	}

	/// <inheritdoc />
	public sealed override void AddMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.AddMany(this, entities);

		using var store = this.createStore();
		store.AddMany(entities);
	}

	/// <inheritdoc />
	public sealed override void AddOrUpdate<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.AddOrUpdate(this, entity);

		using var store = this.createStore();
		store.AddOrUpdate(entity);
	}

	/// <inheritdoc />
	public sealed override void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.AddOrUpdateMany(this, entities);

		using var store = this.createStore();
		store.AddOrUpdateMany(entities);
	}

	/// <inheritdoc />
	public sealed override void Update<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Update(this, entity);

		using var store = this.createStore();
		store.Update(entity);
	}

	/// <inheritdoc />
	public sealed override void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.UpdateMany(this, entities);

		using var store = this.createStore();
		store.UpdateMany(entities);
	}

	/// <inheritdoc />
	public sealed override void Remove<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Remove(this, entity);

		using var store = this.createStore();
		store.Remove(entity);
	}

	/// <inheritdoc />
	public sealed override void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.RemoveMany(this, entities);

		using var store = this.createStore();
		store.RemoveMany(entities);
	}

	/// <inheritdoc />
	public sealed override void RemoveAll<TEntity>()
	{
		IStoreTransactionContracts.RemoveAll(this);

		using var store = this.createStore();
		store.RemoveAll<TEntity>();
	}

	/// <inheritdoc />
	public sealed override void RemoveKey<TKey, TEntity>(TKey key)
	{
		IStoreTransactionContracts.RemoveKey(this, key);

		using var store = this.createStore();
		store.RemoveKey<TKey, TEntity>(key);
	}

	/// <inheritdoc />
	public sealed override void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
	{
		IStoreTransactionContracts.RemoveManyKeys(this, keys);

		using var store = this.createStore();
		store.RemoveManyKeys<TKey, TEntity>(keys);
	}

	/// <inheritdoc />
	public sealed override void RunInTransaction(Action<IStoreTransaction> action)
	{
		IStoreTransactionContracts.RunInTransaction(this, action);

		using var store = this.createStore();
		store.RunInTransaction(action);
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
	}
}
