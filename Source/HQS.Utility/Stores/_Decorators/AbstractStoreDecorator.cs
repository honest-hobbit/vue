﻿namespace HQS.Utility.Stores;

public abstract class AbstractStoreDecorator : AbstractDisposable, IFileStore
{
	private string path;

	/// <inheritdoc />
	public string Path => InterlockedValue.Read(ref this.path);

	/// <inheritdoc />
	public abstract void MoveFileTo(string path);

	/// <inheritdoc />
	public abstract void CopyFileTo(string path, bool overwrite = false);

	/// <inheritdoc />
	public abstract void DisposeAndDeleteFile();

	/// <inheritdoc />
	public abstract void Initialize<TEntity>()
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void Initialize(Type type);

	/// <inheritdoc />
	public abstract IEnumerable<TEntity> All<TEntity>()
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
		where TEntity : class, IKeyed<TKey>, new();

	/// <inheritdoc />
	public abstract void Add<TEntity>(TEntity entity)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void AddMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void AddOrUpdate<TEntity>(TEntity entity)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void Update<TEntity>(TEntity entity)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void Remove<TEntity>(TEntity entity)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void RemoveAll<TEntity>()
		where TEntity : class, new();

	/// <inheritdoc />
	public abstract void RemoveKey<TKey, TEntity>(TKey key)
		where TEntity : class, IKeyed<TKey>, new();

	/// <inheritdoc />
	public abstract void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
		where TEntity : class, IKeyed<TKey>, new();

	/// <inheritdoc />
	public abstract void RunInTransaction(Action<IStoreTransaction> action);

	protected void SetPath(string path)
	{
		Debug.Assert(!path.IsNullOrWhiteSpace());

		Interlocked.Exchange(ref this.path, path);
	}
}
