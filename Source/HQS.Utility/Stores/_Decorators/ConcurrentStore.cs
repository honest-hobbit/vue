﻿namespace HQS.Utility.Stores;

public class ConcurrentStore : AbstractStoreDecorator
{
	private readonly ReaderWriterLockSlim padlock = new ReaderWriterLockSlim();

	private readonly IFileStore store;

	public ConcurrentStore(IFileStore store)
	{
		Debug.Assert(store != null);

		this.store = store;
		this.SetPath(this.store.Path);
	}

	/// <inheritdoc />
	public sealed override void MoveFileTo(string path)
	{
		IFileStoreContracts.MoveFileTo(this, path);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.MoveFileTo(path);
			this.SetPath(this.store.Path);
		}
	}

	/// <inheritdoc />
	public sealed override void CopyFileTo(string path, bool overwrite = false)
	{
		IFileStoreContracts.CopyFileTo(this, path);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.CopyFileTo(path, overwrite);
		}
	}

	/// <inheritdoc />
	public sealed override void DisposeAndDeleteFile()
	{
		if (this.TrySetDisposed())
		{
			this.store.DisposeAndDeleteFile();
			this.ManagedDisposal();
		}
	}

	/// <inheritdoc />
	public sealed override void Initialize<TEntity>()
	{
		IStoreTransactionContracts.Initialize(this);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.Initialize<TEntity>();
		}
	}

	/// <inheritdoc />
	public sealed override void Initialize(Type type)
	{
		IStoreTransactionContracts.Initialize(this, type);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.Initialize(type);
		}
	}

	/// <inheritdoc />
	public sealed override IEnumerable<TEntity> All<TEntity>()
	{
		IStoreTransactionContracts.All(this);

		using (this.padlock.EnterReadLockInUsingBlock())
		{
			return this.store.All<TEntity>();
		}
	}

	/// <inheritdoc />
	public sealed override IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
	{
		IStoreTransactionContracts.Where(this, predicate);

		using (this.padlock.EnterReadLockInUsingBlock())
		{
			return this.store.Where(predicate);
		}
	}

	/// <inheritdoc />
	public sealed override bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
	{
		IStoreTransactionContracts.TryGet(this, key);

		using (this.padlock.EnterReadLockInUsingBlock())
		{
			return this.store.TryGet(key, out entity);
		}
	}

	/// <inheritdoc />
	public sealed override void Add<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Add(this, entity);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.Add(entity);
		}
	}

	/// <inheritdoc />
	public sealed override void AddMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.AddMany(this, entities);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.AddMany(entities);
		}
	}

	/// <inheritdoc />
	public sealed override void AddOrUpdate<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.AddOrUpdate(this, entity);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.AddOrUpdate(entity);
		}
	}

	/// <inheritdoc />
	public sealed override void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.AddOrUpdateMany(this, entities);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.AddOrUpdateMany(entities);
		}
	}

	/// <inheritdoc />
	public sealed override void Update<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Update(this, entity);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.Update(entity);
		}
	}

	/// <inheritdoc />
	public sealed override void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.UpdateMany(this, entities);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.UpdateMany(entities);
		}
	}

	/// <inheritdoc />
	public sealed override void Remove<TEntity>(TEntity entity)
	{
		IStoreTransactionContracts.Remove(this, entity);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.Remove(entity);
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
	{
		IStoreTransactionContracts.RemoveMany(this, entities);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.RemoveMany(entities);
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveAll<TEntity>()
	{
		IStoreTransactionContracts.RemoveAll(this);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.RemoveAll<TEntity>();
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveKey<TKey, TEntity>(TKey key)
	{
		IStoreTransactionContracts.RemoveKey(this, key);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.RemoveKey<TKey, TEntity>(key);
		}
	}

	/// <inheritdoc />
	public sealed override void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
	{
		IStoreTransactionContracts.RemoveManyKeys(this, keys);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.RemoveManyKeys<TKey, TEntity>(keys);
		}
	}

	/// <inheritdoc />
	public sealed override void RunInTransaction(Action<IStoreTransaction> action)
	{
		IStoreTransactionContracts.RunInTransaction(this, action);

		using (this.padlock.EnterWriteLockInUsingBlock())
		{
			this.store.RunInTransaction(action);
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.store.Dispose();
		this.padlock.Dispose();
	}
}
