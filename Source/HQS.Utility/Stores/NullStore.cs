﻿namespace HQS.Utility.Stores;

public class NullStore : AbstractDisposable, IStore
{
	/// <inheritdoc />
	public void Initialize<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Initialize(this);
	}

	/// <inheritdoc />
	public void Initialize(Type type)
	{
		IStoreTransactionContracts.Initialize(this, type);
	}

	/// <inheritdoc />
	public IEnumerable<TEntity> All<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.All(this);

		return Enumerable.Empty<TEntity>();
	}

	/// <inheritdoc />
	public IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Where(this, predicate);

		return Enumerable.Empty<TEntity>();
	}

	/// <inheritdoc />
	public bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.TryGet(this, key);

		entity = null;
		return false;
	}

	/// <inheritdoc />
	public void Add<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Add(this, entity);
	}

	/// <inheritdoc />
	public void AddMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddMany(this, entities);
	}

	/// <inheritdoc />
	public void AddOrUpdate<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddOrUpdate(this, entity);
	}

	/// <inheritdoc />
	public void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddOrUpdateMany(this, entities);
	}

	/// <inheritdoc />
	public void Update<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Update(this, entity);
	}

	/// <inheritdoc />
	public void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.UpdateMany(this, entities);
	}

	/// <inheritdoc />
	public void Remove<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Remove(this, entity);
	}

	/// <inheritdoc />
	public void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.RemoveMany(this, entities);
	}

	/// <inheritdoc />
	public void RemoveAll<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.RemoveAll(this);
	}

	/// <inheritdoc />
	public void RemoveKey<TKey, TEntity>(TKey key)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.RemoveKey(this, key);
	}

	/// <inheritdoc />
	public void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.RemoveManyKeys(this, keys);
	}

	/// <inheritdoc />
	public void RunInTransaction(Action<IStoreTransaction> action)
	{
		IStoreTransactionContracts.RunInTransaction(this, action);
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
	}
}
