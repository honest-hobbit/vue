﻿namespace HQS.Utility.Stores;

public interface IFileStore : IStore
{
	string Path { get; }

	void MoveFileTo(string path);

	void CopyFileTo(string path, bool overwrite = false);

	void DisposeAndDeleteFile();
}
