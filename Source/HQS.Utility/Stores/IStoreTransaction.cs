﻿namespace HQS.Utility.Stores;

public interface IStoreTransaction : IDisposed
{
	void Initialize<TEntity>()
		where TEntity : class, new();

	void Initialize(Type type);

	IEnumerable<TEntity> All<TEntity>()
		where TEntity : class, new();

	IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
		where TEntity : class, new();

	bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
		where TEntity : class, IKeyed<TKey>, new();

	void Add<TEntity>(TEntity entity)
		where TEntity : class, new();

	void AddMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	void AddOrUpdate<TEntity>(TEntity entity)
		where TEntity : class, new();

	void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	void Update<TEntity>(TEntity entity)
		where TEntity : class, new();

	void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	void Remove<TEntity>(TEntity entity)
		where TEntity : class, new();

	void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new();

	void RemoveAll<TEntity>()
		where TEntity : class, new();

	void RemoveKey<TKey, TEntity>(TKey key)
		where TEntity : class, IKeyed<TKey>, new();

	void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
		where TEntity : class, IKeyed<TKey>, new();

	void RunInTransaction(Action<IStoreTransaction> action);
}
