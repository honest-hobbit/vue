﻿namespace HQS.Utility.Stores;

public interface IConfigEntity : IKeyed<string>
{
	new string Key { get; set; }

	string Value { get; set; }
}
