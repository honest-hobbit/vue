﻿namespace HQS.Utility.Stores;

/// <summary>
/// Provides extension methods for <see cref="IConfigKey{T}"/>.
/// </summary>
public static class IConfigKeyExtensions
{
	public static Try<T> TryDeserialize<T>(this IConfigKey<T> key, string value)
	{
		Debug.Assert(key != null);

		return key.TryDeserialize(value, out var result) ? Try.Value(result) : Try.None<T>();
	}
}
