﻿namespace HQS.Utility.Stores;

public static class ConfigKey
{
	private const NumberStyles DefaultIntegral = NumberStyles.Integer | NumberStyles.AllowThousands;

	private const NumberStyles DefaultFloating = NumberStyles.Float | NumberStyles.AllowThousands;

	private const NumberStyles DefaultDecimal = NumberStyles.Number;

	#region Public For[Type] Methods

	public static IConfigKey<bool> ForBool(string key) => new BoolValueKey(key);

	public static IConfigKey<char> ForChar(string key) => new CharValueKey(key);

	public static IConfigKey<string> ForString(string key) => new StringValueKey(key);

	public static IConfigKey<Guid> ForGuid(string key) => new GuidValueKey(key);

	public static IConfigKey<DateTime> ForDateTime(string key, DateTimeStyles style = DateTimeStyles.None) =>
		new DateTimeValueKey(key, style);

	public static IConfigKey<DateTime> ForDateTime(string key, IFormatProvider provider, DateTimeStyles style = DateTimeStyles.None) =>
		new DateTimeValueKey(key, provider, style);

	public static IConfigKey<DateTime> ForDateTime(string key, string format, DateTimeStyles style = DateTimeStyles.None) =>
		new DateTimeFormatValueKey(key, format, style);

	public static IConfigKey<DateTime> ForDateTime(string key, string format, IFormatProvider provider, DateTimeStyles style = DateTimeStyles.None) =>
		new DateTimeFormatValueKey(key, format, provider, style);

	public static IConfigKey<DateTime> ForDateTimeISO(string key, DateTimeStyles style = DateTimeStyles.None) =>
		ForDateTime(key, "yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture, style);

	public static IConfigKey<float> ForFloat(string key, NumberStyles style = DefaultFloating) =>
		new FloatValueKey(key, style);

	public static IConfigKey<float> ForFloat(string key, IFormatProvider provider, NumberStyles style = DefaultFloating) =>
		new FloatValueKey(key, provider, style);

	public static IConfigKey<double> ForDouble(string key, NumberStyles style = DefaultFloating) =>
		new DoubleValueKey(key, style);

	public static IConfigKey<double> ForDouble(string key, IFormatProvider provider, NumberStyles style = DefaultFloating) =>
		new DoubleValueKey(key, provider, style);

	public static IConfigKey<decimal> ForDecimal(string key, NumberStyles style = DefaultDecimal) =>
		new DecimalValueKey(key, style);

	public static IConfigKey<decimal> ForDecimal(string key, IFormatProvider provider, NumberStyles style = DefaultDecimal) =>
		new DecimalValueKey(key, provider, style);

	public static IConfigKey<byte> ForByte(string key, NumberStyles style = NumberStyles.Integer) =>
		new ByteValueKey(key, style);

	public static IConfigKey<byte> ForByte(string key, IFormatProvider provider, NumberStyles style = NumberStyles.Integer) =>
		new ByteValueKey(key, provider, style);

	public static IConfigKey<sbyte> ForSByte(string key, NumberStyles style = NumberStyles.Integer) =>
		new SByteValueKey(key, style);

	public static IConfigKey<sbyte> ForSByte(string key, IFormatProvider provider, NumberStyles style = NumberStyles.Integer) =>
		new SByteValueKey(key, provider, style);

	public static IConfigKey<short> ForShort(string key, NumberStyles style = DefaultIntegral) =>
		new ShortValueKey(key, style);

	public static IConfigKey<short> ForShort(string key, IFormatProvider provider, NumberStyles style = DefaultIntegral) =>
		new ShortValueKey(key, provider, style);

	public static IConfigKey<ushort> ForUShort(string key, NumberStyles style = DefaultIntegral) =>
		new UShortValueKey(key, style);

	public static IConfigKey<ushort> ForUShort(string key, IFormatProvider provider, NumberStyles style = DefaultIntegral) =>
		new UShortValueKey(key, provider, style);

	public static IConfigKey<int> ForInt(string key, NumberStyles style = DefaultIntegral) =>
		new IntValueKey(key, style);

	public static IConfigKey<int> ForInt(string key, IFormatProvider provider, NumberStyles style = DefaultIntegral) =>
		new IntValueKey(key, provider, style);

	public static IConfigKey<uint> ForUInt(string key, NumberStyles style = DefaultIntegral) =>
		new UIntValueKey(key, style);

	public static IConfigKey<uint> ForUInt(string key, IFormatProvider provider, NumberStyles style = DefaultIntegral) =>
		new UIntValueKey(key, provider, style);

	public static IConfigKey<long> ForLong(string key, NumberStyles style = DefaultIntegral) =>
		new LongValueKey(key, style);

	public static IConfigKey<long> ForLong(string key, IFormatProvider provider, NumberStyles style = DefaultIntegral) =>
		new LongValueKey(key, provider, style);

	public static IConfigKey<ulong> ForULong(string key, NumberStyles style = DefaultIntegral) =>
		new ULongValueKey(key, style);

	public static IConfigKey<ulong> ForULong(string key, IFormatProvider provider, NumberStyles style = DefaultIntegral) =>
		new ULongValueKey(key, provider, style);

	#endregion

	#region Private Misc Value Classes

	private abstract class AbstractValueKey<T> : IConfigKey<T>
	{
		public AbstractValueKey(string key)
		{
			Debug.Assert(!key.IsNullOrWhiteSpace());

			this.Key = key;
		}

		/// <inheritdoc />
		public string Key { get; }

		/// <inheritdoc />
		public abstract string Serialize(T value);

		/// <inheritdoc />
		public abstract bool TryDeserialize(string value, out T result);

		/// <inheritdoc />
		public override string ToString() => this.Key;
	}

	private class BoolValueKey : AbstractValueKey<bool>
	{
		public BoolValueKey(string key)
			: base(key)
		{
		}

		/// <inheritdoc />
		public override string Serialize(bool value) => value.ToString();

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out bool result) =>
			bool.TryParse(value, out result);
	}

	private class CharValueKey : AbstractValueKey<char>
	{
		public CharValueKey(string key)
			: base(key)
		{
		}

		/// <inheritdoc />
		public override string Serialize(char value) => value.ToString();

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out char result) =>
			char.TryParse(value, out result);
	}

	private class StringValueKey : AbstractValueKey<string>
	{
		public StringValueKey(string key)
			: base(key)
		{
		}

		/// <inheritdoc />
		public override string Serialize(string value) => value;

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out string result)
		{
			result = value;
			return true;
		}
	}

	private class GuidValueKey : AbstractValueKey<Guid>
	{
		public GuidValueKey(string key)
			: base(key)
		{
		}

		/// <inheritdoc />
		public override string Serialize(Guid value) => value.ToString();

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out Guid result) => Guid.TryParse(value, out result);
	}

	#endregion

	#region Private DateTime Classes

	private abstract class AbstractDateTimeValueKey<T> : AbstractValueKey<T>
	{
		public AbstractDateTimeValueKey(string key, DateTimeStyles style)
			: this(key, CultureInfo.InvariantCulture, style)
		{
		}

		public AbstractDateTimeValueKey(string key, IFormatProvider provider, DateTimeStyles style)
			: base(key)
		{
			Debug.Assert(provider != null);

			this.Provider = provider;
			this.Style = style;
		}

		protected IFormatProvider Provider { get; }

		protected DateTimeStyles Style { get; }
	}

	private class DateTimeValueKey : AbstractDateTimeValueKey<DateTime>
	{
		public DateTimeValueKey(string key, DateTimeStyles style)
			: base(key, style)
		{
		}

		public DateTimeValueKey(string key, IFormatProvider provider, DateTimeStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(DateTime value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out DateTime result) =>
			DateTime.TryParse(value, this.Provider, this.Style, out result);
	}

	private class DateTimeFormatValueKey : AbstractDateTimeValueKey<DateTime>
	{
		private readonly string format;

		public DateTimeFormatValueKey(string key, string format, DateTimeStyles style)
			: base(key, style)
		{
			this.format = format;
		}

		public DateTimeFormatValueKey(string key, string format, IFormatProvider provider, DateTimeStyles style)
			: base(key, provider, style)
		{
			this.format = format;
		}

		/// <inheritdoc />
		public override string Serialize(DateTime value) => value.ToString(this.format, this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out DateTime result) =>
			DateTime.TryParseExact(value, this.format, this.Provider, this.Style, out result);
	}

	#endregion

	#region Private Number Classes

	private abstract class AbstractNumberValueKey<T> : AbstractValueKey<T>
	{
		public AbstractNumberValueKey(string key, NumberStyles style)
			: this(key, CultureInfo.InvariantCulture, style)
		{
		}

		public AbstractNumberValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key)
		{
			Debug.Assert(provider != null);

			this.Provider = provider;
			this.Style = style;
		}

		protected IFormatProvider Provider { get; }

		protected NumberStyles Style { get; }
	}

	private class FloatValueKey : AbstractNumberValueKey<float>
	{
		public FloatValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public FloatValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(float value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out float result) =>
			float.TryParse(value, this.Style, this.Provider, out result);
	}

	private class DoubleValueKey : AbstractNumberValueKey<double>
	{
		public DoubleValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public DoubleValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(double value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out double result) =>
			double.TryParse(value, this.Style, this.Provider, out result);
	}

	private class DecimalValueKey : AbstractNumberValueKey<decimal>
	{
		public DecimalValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public DecimalValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(decimal value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out decimal result) =>
			decimal.TryParse(value, this.Style, this.Provider, out result);
	}

	private class ByteValueKey : AbstractNumberValueKey<byte>
	{
		public ByteValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public ByteValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(byte value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out byte result) =>
			byte.TryParse(value, this.Style, this.Provider, out result);
	}

	private class ShortValueKey : AbstractNumberValueKey<short>
	{
		public ShortValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public ShortValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(short value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out short result) =>
			short.TryParse(value, this.Style, this.Provider, out result);
	}

	private class IntValueKey : AbstractNumberValueKey<int>
	{
		public IntValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public IntValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(int value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out int result) =>
			int.TryParse(value, this.Style, this.Provider, out result);
	}

	private class LongValueKey : AbstractNumberValueKey<long>
	{
		public LongValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public LongValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(long value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out long result) =>
			long.TryParse(value, this.Style, this.Provider, out result);
	}

	private class SByteValueKey : AbstractNumberValueKey<sbyte>
	{
		public SByteValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public SByteValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(sbyte value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out sbyte result) =>
			sbyte.TryParse(value, this.Style, this.Provider, out result);
	}

	private class UShortValueKey : AbstractNumberValueKey<ushort>
	{
		public UShortValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public UShortValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(ushort value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out ushort result) =>
			ushort.TryParse(value, this.Style, this.Provider, out result);
	}

	private class UIntValueKey : AbstractNumberValueKey<uint>
	{
		public UIntValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public UIntValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(uint value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out uint result) =>
			uint.TryParse(value, this.Style, this.Provider, out result);
	}

	private class ULongValueKey : AbstractNumberValueKey<ulong>
	{
		public ULongValueKey(string key, NumberStyles style)
			: base(key, style)
		{
		}

		public ULongValueKey(string key, IFormatProvider provider, NumberStyles style)
			: base(key, provider, style)
		{
		}

		/// <inheritdoc />
		public override string Serialize(ulong value) => value.ToString(this.Provider);

		/// <inheritdoc />
		public override bool TryDeserialize(string value, out ulong result) =>
			ulong.TryParse(value, this.Style, this.Provider, out result);
	}

	#endregion
}
