﻿namespace HQS.Utility.Stores;

public interface IConfigKey<T> : IKeyed<string>
{
	bool TryDeserialize(string value, out T result);

	string Serialize(T value);
}
