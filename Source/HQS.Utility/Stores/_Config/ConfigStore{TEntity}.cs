﻿namespace HQS.Utility.Stores;

public readonly struct ConfigStore<TEntity> : IDisposed, IEquatable<ConfigStore<TEntity>>
	where TEntity : class, IConfigEntity, new()
{
	private readonly IStoreTransaction store;

	internal ConfigStore(IStoreTransaction store)
	{
		Debug.Assert(store != null);

		store.Initialize<TEntity>();
		this.store = store;
	}

	/// <inheritdoc />
	public bool IsDisposed => this.store?.IsDisposed ?? false;

	public bool IsNull => this.store == null;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(ConfigStore<TEntity> other) => this.store.EqualsNullSafe(other.store);

	/// <inheritdoc />
	public override int GetHashCode() => this.store.GetHashCodeNullSafe();

	public static bool operator ==(ConfigStore<TEntity> left, ConfigStore<TEntity> right) => left.Equals(right);

	public static bool operator !=(ConfigStore<TEntity> left, ConfigStore<TEntity> right) => !(left == right);

	public bool TryGet<T>(IConfigKey<T> key, out T result)
	{
		this.SharedContracts(key);

		if (this.store.TryGet(key.Key, out TEntity entity))
		{
			return key.TryDeserialize(entity.Value, out result);
		}
		else
		{
			result = default;
			return false;
		}
	}

	public T Get<T>(IConfigKey<T> key)
	{
		// don't bother with SharedContracts because TryGet will immediately check them
		if (this.TryGet(key, out T value))
		{
			return value;
		}
		else
		{
			throw new InvalidOperationException($"No entity found with key: {key}");
		}
	}

	public Try<T> TryGet<T>(IConfigKey<T> key) => this.TryGet(key, out T value) ? Try.Value(value) : Try.None<T>();

	public T GetOrDefault<T>(IConfigKey<T> key) => this.GetOrDefault(key, default);

	public T GetOrDefault<T>(IConfigKey<T> key, T defaultValue) => this.TryGet(key, out T value) ? value : defaultValue;

	public T GetOrAddDefault<T>(IConfigKey<T> key) => this.GetOrAddDefault(key, default);

	public T GetOrAddDefault<T>(IConfigKey<T> key, T defaultValue)
	{
		this.SharedContracts(key);

		var result = defaultValue;
		this.store.RunInTransaction(store =>
		{
			if (store.TryGet(key.Key, out TEntity entity) && key.TryDeserialize(entity.Value, out result))
			{
					// do nothing here because result has already been assigned by the out parameter of TryDeserialize
				}
			else
			{
				result = defaultValue;
				store.AddOrUpdate(entity = new TEntity()
				{
					Key = key.Key,
					Value = key.Serialize(defaultValue),
				});
			}
		});

		return result;
	}

	public void Add<T>(IConfigKey<T> key, T value)
	{
		this.SharedContracts(key);

		this.store.Add(new TEntity()
		{
			Key = key.Key,
			Value = key.Serialize(value),
		});
	}

	public void AddOrUpdate<T>(IConfigKey<T> key, T value)
	{
		this.SharedContracts(key);

		this.store.AddOrUpdate(new TEntity()
		{
			Key = key.Key,
			Value = key.Serialize(value),
		});
	}

	public void Update<T>(IConfigKey<T> key, T value)
	{
		this.SharedContracts(key);

		this.store.Update(new TEntity()
		{
			Key = key.Key,
			Value = key.Serialize(value),
		});
	}

	public void Remove<T>(IConfigKey<T> key)
	{
		this.SharedContracts(key);

		this.store.RemoveKey<string, TEntity>(key.Key);
	}

	public void RemoveAll()
	{
		Debug.Assert(!this.IsNull);
		Debug.Assert(!this.IsDisposed);

		this.store.RemoveAll<TEntity>();
	}

	[Conditional(CompilationSymbol.Debug)]
	private void SharedContracts<T>(IConfigKey<T> key)
	{
		Debug.Assert(!this.IsNull);
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(key != null);
		Debug.Assert(!key.Key.IsNullOrWhiteSpace());
	}
}
