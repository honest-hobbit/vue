﻿namespace HQS.Utility.Stores;

public static class IFileStoreContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void MoveFileTo(IFileStore store, string path)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(PathUtility.IsPathValid(path));
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void CopyFileTo(IFileStore store, string path)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(PathUtility.IsPathValid(path));
	}
}
