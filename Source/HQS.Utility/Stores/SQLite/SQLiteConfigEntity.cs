﻿namespace HQS.Utility.Stores.SQLite;

[Table("Config")]
public class SQLiteConfigEntity : IConfigEntity
{
	[PrimaryKey]
	public string Key { get; set; }

	public string Value { get; set; }
}
