﻿namespace HQS.Utility.Stores.SQLite;

public class SQLiteStore : AbstractDisposable, IFileStore
{
	private SQLiteConnection connection;

	public SQLiteStore(string databasePath)
	{
		Debug.Assert(PathUtility.IsPathValid(databasePath));

		databasePath = System.IO.Path.GetFullPath(databasePath);
		this.connection = new SQLiteConnection(databasePath);
		this.Path = databasePath;
	}

	public static void CreateDirectoryForStore(string databasePath)
	{
		Debug.Assert(PathUtility.IsPathValid(databasePath));

		var directory = System.IO.Path.GetDirectoryName(databasePath);
		if (!directory.IsNullOrWhiteSpace())
		{
			Directory.CreateDirectory(directory);
		}
	}

	public static SQLiteStore CreateDirectoryAndStore(string databasePath)
	{
		Debug.Assert(PathUtility.IsPathValid(databasePath));

		CreateDirectoryForStore(databasePath);
		return new SQLiteStore(databasePath);
	}

	/// <inheritdoc />
	public string Path { get; private set; }

	/// <inheritdoc />
	public void MoveFileTo(string path)
	{
		IFileStoreContracts.MoveFileTo(this, path);

		path = System.IO.Path.GetFullPath(path);
		if (path == this.Path)
		{
			return;
		}

		this.connection.Dispose();
		try
		{
			File.Move(this.Path, path);
			this.Path = path;
		}
		finally
		{
			this.connection = new SQLiteConnection(this.Path);
		}
	}

	/// <inheritdoc />
	public void CopyFileTo(string path, bool overwrite = false)
	{
		IFileStoreContracts.CopyFileTo(this, path);

		path = System.IO.Path.GetFullPath(path);
		if (path == this.Path)
		{
			return;
		}

		this.connection.Dispose();
		try
		{
			File.Copy(this.Path, path, overwrite);
		}
		finally
		{
			this.connection = new SQLiteConnection(this.Path);
		}
	}

	/// <inheritdoc />
	public void DisposeAndDeleteFile()
	{
		if (this.TryDispose())
		{
			File.Delete(this.Path);
		}
	}

	/// <inheritdoc />
	public void Initialize<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Initialize(this);

		this.connection.CreateTable<TEntity>();
	}

	/// <inheritdoc />
	public void Initialize(Type type)
	{
		IStoreTransactionContracts.Initialize(this, type);

		this.connection.CreateTable(type);
	}

	/// <inheritdoc />
	public IEnumerable<TEntity> All<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.All(this);

		return this.connection.Table<TEntity>().ToArray().AsReadOnlyList();
	}

	/// <inheritdoc />
	public IEnumerable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Where(this, predicate);

		return this.connection.Table<TEntity>().Where(predicate).ToArray().AsReadOnlyList();
	}

	/// <inheritdoc />
	public bool TryGet<TKey, TEntity>(TKey key, out TEntity entity)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.TryGet(this, key);

		entity = this.connection.Find<TEntity>(key);
		return entity != null;
	}

	/// <inheritdoc />
	public void Add<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Add(this, entity);

		this.connection.Insert(entity);
	}

	/// <inheritdoc />
	public void AddMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddMany(this, entities);

		if (entities.Any())
		{
			this.connection.InsertAll(entities);
		}
	}

	/// <inheritdoc />
	public void AddOrUpdate<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddOrUpdate(this, entity);

		this.connection.InsertOrReplace(entity);
	}

	/// <inheritdoc />
	public void AddOrUpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.AddOrUpdateMany(this, entities);

		if (entities.Any())
		{
			this.connection.RunInTransaction(() =>
			{
				foreach (var entity in entities)
				{
					this.connection.InsertOrReplace(entity);
				}
			});
		}
	}

	/// <inheritdoc />
	public void Update<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Update(this, entity);

		this.connection.Update(entity);
	}

	/// <inheritdoc />
	public void UpdateMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.UpdateMany(this, entities);

		if (entities.Any())
		{
			this.connection.UpdateAll(entities);
		}
	}

	/// <inheritdoc />
	public void Remove<TEntity>(TEntity entity)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.Remove(this, entity);

		this.connection.Delete(entity);
	}

	/// <inheritdoc />
	public void RemoveMany<TEntity>(IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		IStoreTransactionContracts.RemoveMany(this, entities);

		if (entities.Any())
		{
			this.connection.RunInTransaction(() =>
			{
				foreach (var entity in entities)
				{
					this.connection.Delete(entity);
				}
			});
		}
	}

	/// <inheritdoc />
	public void RemoveAll<TEntity>()
		where TEntity : class, new()
	{
		IStoreTransactionContracts.RemoveAll(this);

		this.connection.DeleteAll<TEntity>();
	}

	/// <inheritdoc />
	public void RemoveKey<TKey, TEntity>(TKey key)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.RemoveKey(this, key);

		this.connection.Delete<TEntity>(key);
	}

	/// <inheritdoc />
	public void RemoveManyKeys<TKey, TEntity>(IEnumerable<TKey> keys)
		where TEntity : class, IKeyed<TKey>, new()
	{
		IStoreTransactionContracts.RemoveManyKeys(this, keys);

		if (keys.Any())
		{
			this.connection.RunInTransaction(() =>
			{
				foreach (var key in keys)
				{
					this.connection.Delete<TEntity>(key);
				}
			});
		}
	}

	/// <inheritdoc />
	public void RunInTransaction(Action<IStoreTransaction> action)
	{
		IStoreTransactionContracts.RunInTransaction(this, action);

		this.connection.RunInTransaction(() => action(this));
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.connection.Dispose();
		this.connection = null;
	}
}
