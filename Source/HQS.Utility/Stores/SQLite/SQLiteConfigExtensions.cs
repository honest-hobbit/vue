﻿namespace HQS.Utility.Stores.SQLite;

public static class SQLiteConfigExtensions
{
	public static ConfigStore<SQLiteConfigEntity> ConfigInSQLite(this IStoreTransaction store)
	{
		Debug.Assert(store != null);

		return new ConfigStore<SQLiteConfigEntity>(store);
	}
}
