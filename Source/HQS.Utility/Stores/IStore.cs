﻿namespace HQS.Utility.Stores;

/// <summary>
/// Defines a store for persisting entities.
/// </summary>
public interface IStore : IStoreTransaction, IVisiblyDisposable
{
}
