﻿namespace HQS.Utility.Stores;

public static class StoreTesting
{
	public static string GetStorePath() => Path.Combine(
		Path.GetDirectoryName(Uri.UnescapeDataString(new UriBuilder(Assembly.GetCallingAssembly().CodeBase).Path)),
		$"{Guid.NewGuid()}_Test.db");

	public static long Execute(Func<string, IFileStore> createStore, Action<IFileStore> action)
	{
		Debug.Assert(createStore != null);
		Debug.Assert(action != null);

		var path = GetStorePath();
		long bytes;

		try
		{
			using var store = createStore(path);
			action(store);
		}
		finally
		{
			bytes = new FileInfo(path).Length;
			File.Delete(path);
		}

		return bytes;
	}

	// TODO it might be nice to use this in with the ability to specify Recreate and Concurrency
	public static TestStore UseSQLite()
	{
		var path = GetStorePath();
		return new TestStore(path, SQLiteStore.CreateDirectoryAndStore(path));

		////return new TestStore(path, new RecreateStore(() => SQLiteStore.Create(path, entityTypes)));

		////SQLiteStore.Create(path, entityTypes).Dispose();
		////return new TestStore(path, new RecreateStore(() => SQLiteStore.Open(path)));
	}

	public readonly struct TestStore
	{
		private readonly string path;

		private readonly IFileStore store;

		internal TestStore(string path, IFileStore store)
		{
			Debug.Assert(!path.IsNullOrWhiteSpace());
			Debug.Assert(store != null);

			this.path = path;
			this.store = store;
		}

		public long Execute(Action<IFileStore> action)
		{
			Debug.Assert(action != null);

			long bytes;
			try
			{
				using (this.store)
				{
					action(this.store);
				}

				bytes = new FileInfo(this.path).Length;
			}
			finally
			{
				File.Delete(this.path);
			}

			return bytes;
		}
	}
}
