﻿namespace HQS.Utility.Stores;

public static class IStoreTransactionContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Initialize(IStoreTransaction store)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Initialize(IStoreTransaction store, Type type)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(type != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void All(IStoreTransaction store)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Where<TEntity>(IStoreTransaction store, Expression<Func<TEntity, bool>> predicate)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(predicate != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void TryGet<TKey>(IStoreTransaction store, TKey key)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(key != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Add<TEntity>(IStoreTransaction store, TEntity entity)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entity != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AddMany<TEntity>(IStoreTransaction store, IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entities.AllAndSelfNotNull());
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AddOrUpdate<TEntity>(IStoreTransaction store, TEntity entity)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entity != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AddOrUpdateMany<TEntity>(IStoreTransaction store, IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entities.AllAndSelfNotNull());
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Update<TEntity>(IStoreTransaction store, TEntity entity)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entity != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void UpdateMany<TEntity>(IStoreTransaction store, IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entities.AllAndSelfNotNull());
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Remove<TEntity>(IStoreTransaction store, TEntity entity)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entity != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void RemoveMany<TEntity>(IStoreTransaction store, IEnumerable<TEntity> entities)
		where TEntity : class, new()
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(entities.AllAndSelfNotNull());
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void RemoveAll(IStoreTransaction store)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void RemoveKey<TKey>(IStoreTransaction store, TKey key)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(key != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void RemoveManyKeys<TKey>(IStoreTransaction store, IEnumerable<TKey> keys)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(keys.AllAndSelfNotNull());
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void RunInTransaction(IStoreTransaction store, Action<IStore> action)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);
		Debug.Assert(action != null);
	}
}
