﻿namespace HQS.Utility.Reactive;

public class SingleSignalObservable : AbstractDisposable, IObservable<Nothing>
{
	private readonly Subject<Nothing> isSignaled = new Subject<Nothing>();

	/// <inheritdoc />
	public IDisposable Subscribe(IObserver<Nothing> observer) => this.isSignaled.Subscribe(observer);

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.isSignaled.OnNext(Nothing.Default);
		this.isSignaled.OnCompleted();
	}
}
