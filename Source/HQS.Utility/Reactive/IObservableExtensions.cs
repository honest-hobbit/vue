﻿namespace HQS.Utility.Reactive;

/// <summary>
/// Provides extension methods for <see cref="IObservable{T}"/>.
/// </summary>
public static class IObservableExtensions
{
	public static IObservable<SequentialPair<Try<T>>> PairLatest<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		var pairs = source.Scan(
			SequentialPair.New(Try.None<T>(), Try.None<T>()),
			(accumulator, current) => SequentialPair.New(accumulator.Next, Try.Value(current)));
		return pairs.Merge(pairs.TakeLast(1).Select(final => SequentialPair.New(final.Next, Try.None<T>())));
	}

	#region MinScan/MaxScan

	public static IObservable<byte> MinScan(this IObservable<byte> source) => source.MinScan(byte.MaxValue);

	public static IObservable<sbyte> MinScan(this IObservable<sbyte> source) => source.MinScan(sbyte.MaxValue);

	public static IObservable<short> MinScan(this IObservable<short> source) => source.MinScan(short.MaxValue);

	public static IObservable<ushort> MinScan(this IObservable<ushort> source) => source.MinScan(ushort.MaxValue);

	public static IObservable<int> MinScan(this IObservable<int> source) => source.MinScan(int.MaxValue);

	public static IObservable<uint> MinScan(this IObservable<uint> source) => source.MinScan(uint.MaxValue);

	public static IObservable<long> MinScan(this IObservable<long> source) => source.MinScan(long.MaxValue);

	public static IObservable<ulong> MinScan(this IObservable<ulong> source) => source.MinScan(ulong.MaxValue);

	public static IObservable<float> MinScan(this IObservable<float> source) => source.MinScan(float.MaxValue);

	public static IObservable<double> MinScan(this IObservable<double> source) => source.MinScan(double.MaxValue);

	public static IObservable<decimal> MinScan(this IObservable<decimal> source) => source.MinScan(decimal.MaxValue);

	public static IObservable<DateTime> MinScan(this IObservable<DateTime> source) => source.MinScan(DateTime.MaxValue);

	public static IObservable<T> MinScan<T>(this IObservable<T> source) => source.MinScan(default, Comparer<T>.Default);

	public static IObservable<T> MinScan<T>(this IObservable<T> source, T seed) => source.MinScan(seed, Comparer<T>.Default);

	public static IObservable<T> MinScan<T>(this IObservable<T> source, IComparer<T> comparer) => source.MinScan(default, comparer);

	public static IObservable<T> MinScan<T>(this IObservable<T> source, T seed, IComparer<T> comparer)
	{
		Debug.Assert(source != null);
		Debug.Assert(comparer != null);

		return source.Scan(seed, (min, next) => comparer.IsLessThan(next, min) ? next : min).DistinctUntilChanged();
	}

	public static IObservable<byte> MaxScan(this IObservable<byte> source) => source.MaxScan(byte.MinValue);

	public static IObservable<sbyte> MaxScan(this IObservable<sbyte> source) => source.MaxScan(sbyte.MinValue);

	public static IObservable<short> MaxScan(this IObservable<short> source) => source.MaxScan(short.MinValue);

	public static IObservable<ushort> MaxScan(this IObservable<ushort> source) => source.MaxScan(ushort.MinValue);

	public static IObservable<int> MaxScan(this IObservable<int> source) => source.MaxScan(int.MinValue);

	public static IObservable<uint> MaxScan(this IObservable<uint> source) => source.MaxScan(uint.MinValue);

	public static IObservable<long> MaxScan(this IObservable<long> source) => source.MaxScan(long.MinValue);

	public static IObservable<ulong> MaxScan(this IObservable<ulong> source) => source.MaxScan(ulong.MinValue);

	public static IObservable<float> MaxScan(this IObservable<float> source) => source.MaxScan(float.MinValue);

	public static IObservable<double> MaxScan(this IObservable<double> source) => source.MaxScan(double.MinValue);

	public static IObservable<decimal> MaxScan(this IObservable<decimal> source) => source.MaxScan(decimal.MinValue);

	public static IObservable<DateTime> MaxScan(this IObservable<DateTime> source) => source.MaxScan(DateTime.MinValue);

	public static IObservable<T> MaxScan<T>(this IObservable<T> source) => source.MaxScan(default, Comparer<T>.Default);

	public static IObservable<T> MaxScan<T>(this IObservable<T> source, T seed) => source.MaxScan(seed, Comparer<T>.Default);

	public static IObservable<T> MaxScan<T>(this IObservable<T> source, IComparer<T> comparer) => source.MaxScan(default, comparer);

	public static IObservable<T> MaxScan<T>(this IObservable<T> source, T seed, IComparer<T> comparer)
	{
		Debug.Assert(source != null);
		Debug.Assert(comparer != null);

		return source.Scan(seed, (max, next) => comparer.IsGreaterThan(next, max) ? next : max).DistinctUntilChanged();
	}

	#endregion

	#region Ignore methods

	public static IObservable<T> IgnoreError<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return Observable.Create<T>(observer => source.Subscribe(observer.OnNext, observer.OnCompleted));
	}

	public static IObservable<T> IgnoreCompletion<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return Observable.Create<T>(observer => source.Subscribe(observer.OnNext, observer.OnError));
	}

	public static IObservable<T> IgnoreTermination<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return Observable.Create<T>(observer => source.Subscribe(observer.OnNext));
	}

	#endregion

	#region Only methods

	public static IObservable<T> OnlyError<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return Observable.Create<T>(observer => source.Subscribe(value => { }, observer.OnError));
	}

	public static IObservable<T> OnlyCompletion<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return Observable.Create<T>(observer => source.Subscribe(value => { }, observer.OnCompleted));
	}

	// equivalent to IgnoreElements
	public static IObservable<T> OnlyTermination<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return source.IgnoreElements();
	}

	// equivalent to IgnoreTermination
	public static IObservable<T> OnlyElements<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return source.IgnoreTermination();
	}

	#endregion

	#region WhereSelect Try methods (equivalent methods for IEnumerable too)

	public static IObservable<T> WhereHasValueSelect<T>(this IObservable<Try<T>> values)
	{
		Debug.Assert(values != null);

		return values.Where(x => x.HasValue).Select(x => x.Value);
	}

	public static IObservable<KeyValuePair<TKey, TValue>> WhereHasValueSelectPair<TKey, TValue>(
		this IObservable<KeyValuePair<TKey, Try<TValue>>> values)
	{
		Debug.Assert(values != null);

		return values.Where(x => x.Value.HasValue).Select(x => KeyValuePair.Create(x.Key, x.Value.Value));
	}

	public static IObservable<TKey> WhereHasValueSelectKey<TKey, TValue>(this IObservable<KeyValuePair<TKey, Try<TValue>>> values)
	{
		Debug.Assert(values != null);

		return values.Where(x => x.Value.HasValue).Select(x => x.Key);
	}

	public static IObservable<TKey> WhereNoValueSelectKey<TKey, TValue>(this IObservable<KeyValuePair<TKey, Try<TValue>>> values)
	{
		Debug.Assert(values != null);

		return values.Where(x => !x.Value.HasValue).Select(x => x.Key);
	}

	#endregion

	public static IObservable<T> AsObservableOnly<T>(this IObservable<T> source)
	{
		Debug.Assert(source != null);

		return new ObservableOnly<T>(source);
	}

	public static IObserver<T> AsObserverOnly<T>(this IObserver<T> source)
	{
		Debug.Assert(source != null);

		return new ObserverOnly<T>(source);
	}

	private class ObservableOnly<T> : IObservable<T>
	{
		private readonly IObservable<T> source;

		public ObservableOnly(IObservable<T> source)
		{
			Debug.Assert(source != null);

			this.source = source;
		}

		/// <inheritdoc />
		public IDisposable Subscribe(IObserver<T> observer) => this.source.Subscribe(observer);
	}

	private class ObserverOnly<T> : IObserver<T>
	{
		private readonly IObserver<T> source;

		public ObserverOnly(IObserver<T> source)
		{
			Debug.Assert(source != null);

			this.source = source;
		}

		/// <inheritdoc />
		public void OnCompleted() => this.source.OnCompleted();

		/// <inheritdoc />
		public void OnError(Exception error) => this.source.OnError(error);

		/// <inheritdoc />
		public void OnNext(T value) => this.source.OnNext(value);
	}
}
