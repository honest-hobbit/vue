﻿namespace HQS.Utility.Reactive;

/// <summary>
/// A class that can marshal an observable sequence from one thread to another.
/// </summary>
/// <typeparam name="T">The type of the observable sequence.</typeparam>
public class QueuedObservable<T> : AbstractDisposable
{
	private readonly ConcurrentQueue<T> queue = new ConcurrentQueue<T>();

	private readonly Subject<T> result = new Subject<T>();

	private readonly object padlock = new object();

	private Exception error = null;

	private bool isSourceDone = false;

	private bool isResultDone = false;

	public QueuedObservable()
	{
		this.Source = new SourceObserver(this);
		this.Result = this.result.AsObservableOnly();
	}

	public IObserver<T> Source { get; }

	public IObservable<T> Result { get; }

	/// <summary>
	/// Call this from the thread you want Result to be observed on. It must be called periodically,
	/// otherwise values from Source will build up in the internal queue until you run out of memory.
	/// </summary>
	public void RunQueue()
	{
		Debug.Assert(!this.IsDisposed);

		while (this.queue.TryDequeue(out var value))
		{
			this.result.OnNext(value);
		}

		lock (this.padlock)
		{
			if (this.isSourceDone && !this.isResultDone)
			{
				this.isResultDone = true;
				if (this.error != null)
				{
					this.result.OnError(this.error);
				}
				else
				{
					this.result.OnCompleted();
				}
			}
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.result.Dispose();

	private class SourceObserver : IObserver<T>
	{
		private readonly QueuedObservable<T> parent;

		public SourceObserver(QueuedObservable<T> parent)
		{
			Debug.Assert(parent != null);

			this.parent = parent;
		}

		/// <inheritdoc />
		public void OnNext(T value)
		{
			Debug.Assert(!this.parent.IsDisposed);

			lock (this.parent.padlock)
			{
				if (this.parent.isSourceDone)
				{
					return;
				}
			}

			this.parent.queue.Enqueue(value);
		}

		/// <inheritdoc />
		public void OnError(Exception error)
		{
			Debug.Assert(!this.parent.IsDisposed);
			Debug.Assert(error != null);

			lock (this.parent.padlock)
			{
				if (this.parent.isSourceDone)
				{
					return;
				}

				this.parent.error = error;
				this.parent.isSourceDone = true;
			}
		}

		/// <inheritdoc />
		public void OnCompleted()
		{
			Debug.Assert(!this.parent.IsDisposed);

			lock (this.parent.padlock)
			{
				if (this.parent.isSourceDone)
				{
					return;
				}

				this.parent.isSourceDone = true;
			}
		}
	}
}
