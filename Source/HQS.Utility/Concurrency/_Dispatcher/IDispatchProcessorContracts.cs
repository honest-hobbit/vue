﻿namespace HQS.Utility.Concurrency;

public static class IDispatchProcessorContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void InvokeMany(TimeSpan duration)
	{
		Debug.Assert(duration.IsDuration());
	}
}
