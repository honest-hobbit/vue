﻿namespace HQS.Utility.Concurrency;

public static class IDispatcherExtensions
{
	public static void Submit(this IDispatcher dispatcher, params Action[] actions)
	{
		Debug.Assert(dispatcher != null);

		dispatcher.Submit(actions);
	}

	public static void Submit<T>(this IDispatcher dispatcher, T value, Action<T> action)
	{
		Debug.Assert(dispatcher != null);
		Debug.Assert(action != null);

		dispatcher.Submit(() => action(value));
	}
}
