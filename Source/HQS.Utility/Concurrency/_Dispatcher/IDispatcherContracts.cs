﻿namespace HQS.Utility.Concurrency;

public static class IDispatcherContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Submit(Action action)
	{
		Debug.Assert(action != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Submit(IEnumerable<Action> actions)
	{
		Debug.Assert(actions.AllAndSelfNotNull());
	}
}
