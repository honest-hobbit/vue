﻿namespace HQS.Utility.Concurrency;

public interface IDispatchProcessor
{
	IDispatcher Dispatcher { get; }

	void InvokeSingle();

	void InvokeAll();

	void InvokeMany(TimeSpan duration);
}
