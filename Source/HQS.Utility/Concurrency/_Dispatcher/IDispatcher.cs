﻿namespace HQS.Utility.Concurrency;

public interface IDispatcher
{
	void Submit(Action action);

	void Submit(IEnumerable<Action> actions);
}
