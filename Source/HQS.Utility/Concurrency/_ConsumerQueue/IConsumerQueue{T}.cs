﻿namespace HQS.Utility.Concurrency;

public interface IConsumerQueue<T> : IAsyncCompletable
{
	int Count { get; }

	void Add(T value);
}
