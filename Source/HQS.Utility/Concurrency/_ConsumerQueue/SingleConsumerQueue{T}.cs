﻿namespace HQS.Utility.Concurrency;

[SuppressMessage("Design", "CA1001", Justification = "IAsyncCompletable handles disposal.")]
public class SingleConsumerQueue<T> : AbstractCompletable, IConsumerQueue<T>
{
	private readonly BlockingCollection<T> queue = new BlockingCollection<T>();

	private readonly TaskCompletionSource completion = new TaskCompletionSource();

	private readonly Action<T> consume;

	public SingleConsumerQueue(Action<T> consume)
	{
		Debug.Assert(consume != null);

		this.consume = consume;
		new Thread(this.ConsumeLoop).Start();
	}

	/// <inheritdoc />
	public int Count => this.queue.Count;

	/// <inheritdoc />
	public void Add(T value)
	{
		Debug.Assert(!this.IsCompleting);

		this.queue.Add(value);
	}

	/// <inheritdoc />
	protected override Task CompleteAsync()
	{
		this.queue.CompleteAdding();
		return this.completion.Task;
	}

	private void ConsumeLoop()
	{
		try
		{
			foreach (var value in this.queue.GetConsumingEnumerable())
			{
				this.consume(value);
			}
		}
		catch (OperationCanceledException)
		{
			// cancelation is used to shut down the queue normally
		}
		catch (Exception error)
		{
			this.completion.SetException(error);
		}
		finally
		{
			this.completion.TrySetResult();
			this.queue.Dispose();
		}
	}
}
