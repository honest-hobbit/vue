﻿namespace HQS.Utility.Concurrency;

public class ConsumerQueue<T> : IConsumerQueue<T>
{
	private static readonly ExecutionDataflowBlockOptions DefaultOptions = new ExecutionDataflowBlockOptions();

	private readonly ConcurrentBool isCompleteCalled = new ConcurrentBool(false);

	private readonly AtomicInt count = new AtomicInt(1);

	private readonly Action<T> consume;

	private readonly ActionBlock<T> queue;

	public ConsumerQueue(Action<T> consume, ExecutionDataflowBlockOptions options = null)
	{
		Debug.Assert(consume != null);

		this.consume = consume;
		this.queue = new ActionBlock<T>(this.ConsumeAndSignal, options ?? DefaultOptions);
	}

	/// <inheritdoc />
	public int Count => this.queue.InputCount;

	/// <inheritdoc />
	public Task Completion => this.queue.Completion;

	/// <inheritdoc />
	public bool IsCompleting => this.isCompleteCalled.Value;

	/// <inheritdoc />
	public void Complete()
	{
		if (this.isCompleteCalled.ToggleIfFalse())
		{
			this.DecrementCount();
		}
	}

	/// <inheritdoc />
	public void Add(T value)
	{
		Debug.Assert(!this.IsCompleting);

		this.count.Increment();
		this.queue.Post(value);
	}

	private void ConsumeAndSignal(T value)
	{
		try
		{
			this.consume(value);
		}
		finally
		{
			this.DecrementCount();
		}
	}

	private void DecrementCount()
	{
		if (this.count.Decrement() == 0)
		{
			this.queue.Complete();
		}
	}
}
