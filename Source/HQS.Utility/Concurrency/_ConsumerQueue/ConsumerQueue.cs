﻿namespace HQS.Utility.Concurrency;

public static class ConsumerQueue
{
	public static IConsumerQueue<T> Create<T>(Action<T> consume, int maxParallelism)
	{
		Debug.Assert(consume != null);
		Debug.Assert(DegreeOfParallelism.IsValid(maxParallelism));

		if (maxParallelism == 1)
		{
			return new SingleConsumerQueue<T>(consume);
		}
		else
		{
			return new ConsumerQueue<T>(
				consume, new ExecutionDataflowBlockOptions() { MaxDegreeOfParallelism = maxParallelism });
		}
	}
}
