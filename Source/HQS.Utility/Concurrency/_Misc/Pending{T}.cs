﻿namespace HQS.Utility.Concurrency;

public class Pending<T>
{
	private readonly object padlock = new object();

	private bool isSet;

	private T value;

	private Exception exception;

	public Pending()
	{
		this.isSet = false;
	}

	public Pending(T value)
	{
		this.isSet = true;
		this.value = value;
	}

	public Pending(Exception exception)
	{
		Ensure.That(exception, nameof(exception)).IsNotNull();

		this.isSet = true;
		this.exception = exception;
	}

	public bool IsSet
	{
		get
		{
			lock (this.padlock)
			{
				return this.isSet;
			}
		}
	}

	public bool HasValue
	{
		get
		{
			lock (this.padlock)
			{
				return this.isSet && this.exception == null;
			}
		}
	}

	public bool HasException
	{
		get
		{
			lock (this.padlock)
			{
				return this.isSet && this.exception != null;
			}
		}
	}

	public T Value
	{
		get
		{
			lock (this.padlock)
			{
				this.HandleWaiting();
				return this.value;
			}
		}
	}

	public Exception Exception
	{
		get
		{
			lock (this.padlock)
			{
				return this.exception;
			}
		}
	}

	public void SetValue(T value)
	{
		lock (this.padlock)
		{
			this.ValidateNotSet();

			this.value = value;
			this.isSet = true;
			Monitor.PulseAll(this.padlock);
		}
	}

	public void SetException(Exception exception)
	{
		Ensure.That(exception, nameof(exception)).IsNotNull();

		lock (this.padlock)
		{
			this.ValidateNotSet();

			this.exception = exception;
			this.isSet = true;
			Monitor.PulseAll(this.padlock);
		}
	}

	public void Reset()
	{
		lock (this.padlock)
		{
			this.isSet = false;
			this.value = default;
			this.exception = null;
		}
	}

	public void WaitForResults(out T value, out Exception exception)
	{
		lock (this.padlock)
		{
			while (!this.isSet)
			{
				Monitor.Wait(this.padlock);
			}

			value = this.value;
			exception = this.exception;
		}
	}

	public void Wait()
	{
		lock (this.padlock)
		{
			this.HandleWaiting();
		}
	}

	public void Wait(int millisecondsTimeout)
	{
		lock (this.padlock)
		{
			while (!this.isSet)
			{
				Monitor.Wait(this.padlock, millisecondsTimeout);
			}

			if (this.exception != null)
			{
				throw this.exception;
			}
		}
	}

	public void Wait(TimeSpan timeout)
	{
		lock (this.padlock)
		{
			while (!this.isSet)
			{
				Monitor.Wait(this.padlock, timeout);
			}

			if (this.exception != null)
			{
				throw this.exception;
			}
		}
	}

	// only call while inside this.padlock
	private void HandleWaiting()
	{
		while (!this.isSet)
		{
			Monitor.Wait(this.padlock);
		}

		if (this.exception != null)
		{
			throw this.exception;
		}
	}

	// only call while inside this.padlock
	private void ValidateNotSet()
	{
		if (this.isSet)
		{
			throw new InvalidOperationException("This instance is already set.");
		}
	}
}
