﻿namespace HQS.Utility.Concurrency;

public class PendingSignal
{
	private readonly Pending<Nothing> pending;

	public PendingSignal()
	{
		this.pending = new Pending<Nothing>();
	}

	public PendingSignal(Exception exception)
	{
		this.pending = new Pending<Nothing>(exception);
	}

	public bool IsSet => this.pending.IsSet;

	public bool HasValue => this.pending.HasValue;

	public bool HasException => this.pending.HasException;

	public Exception Exception => this.pending.Exception;

	public void Set() => this.pending.SetValue(default);

	public void SetException(Exception exception) => this.pending.SetException(exception);

	public void Reset() => this.pending.Reset();

	public void WaitForResults(out Exception exception) => this.pending.WaitForResults(out var _, out exception);

	public void Wait() => this.pending.Wait();

	public void Wait(int millisecondsTimeout) => this.pending.Wait(millisecondsTimeout);

	public void Wait(TimeSpan timeout) => this.pending.Wait(timeout);
}
