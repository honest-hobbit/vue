﻿namespace HQS.Utility.Concurrency;

public static class ProducerConsumerGate
{
	public static ProducerConsumerGate<T> CreateQueue<T>() => new ProducerConsumerGate<T>(new ConcurrentQueue<T>(), new ConcurrentQueue<T>());

	public static ProducerConsumerGate<T> CreateStack<T>() => new ProducerConsumerGate<T>(new ConcurrentStack<T>(), new ConcurrentStack<T>());
}
