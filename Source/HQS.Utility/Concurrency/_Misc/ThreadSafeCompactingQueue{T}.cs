﻿namespace HQS.Utility.Concurrency;

public class ThreadSafeCompactingQueue<T>
{
	private readonly object padlock = new object();

	private readonly CompactingQueue<T> queue;

	public ThreadSafeCompactingQueue(Func<T, bool> isValueStale)
	{
		Debug.Assert(isValueStale != null);

		this.queue = new CompactingQueue<T>(isValueStale);
	}

	public ThreadSafeCompactingQueue(Func<T, bool> isValueStale, int capacity)
	{
		Debug.Assert(isValueStale != null);
		Debug.Assert(capacity >= 0);

		this.queue = new CompactingQueue<T>(isValueStale, capacity);
	}

	public int Count
	{
		get
		{
			lock (this.padlock)
			{
				return this.queue.Count;
			}
		}
	}

	public void Clear()
	{
		lock (this.padlock)
		{
			this.queue.Clear();
		}
	}

	public void Enqueue(T value)
	{
		lock (this.padlock)
		{
			this.queue.Enqueue(value);
		}
	}

	public bool TryDequeue(out T value)
	{
		lock (this.padlock)
		{
			return this.queue.TryDequeue(out value);
		}
	}
}
