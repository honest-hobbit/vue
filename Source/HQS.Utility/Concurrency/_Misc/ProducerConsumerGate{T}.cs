﻿namespace HQS.Utility.Concurrency;

public class ProducerConsumerGate<T>
{
	private readonly IProducerConsumerCollection<T> produced;

	private readonly IProducerConsumerCollection<T> consumed;

	public ProducerConsumerGate(IProducerConsumerCollection<T> produced, IProducerConsumerCollection<T> consumed)
	{
		Debug.Assert(produced != null);
		Debug.Assert(consumed != null);
		Debug.Assert(!produced.EqualsByReferenceNullSafe(consumed));

		this.produced = produced;
		this.consumed = consumed;
	}

	public void AddProduced(T value) => this.produced.TryAdd(value);

	public void AddConsumed(T value) => this.consumed.TryAdd(value);

	public bool TryTakeProduced(out T value) => this.produced.TryTake(out value);

	public bool TryTakeConsumed(out T value) => this.consumed.TryTake(out value);
}
