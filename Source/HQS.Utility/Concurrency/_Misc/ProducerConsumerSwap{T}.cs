﻿namespace HQS.Utility.Concurrency;

public class ProducerConsumerSwap<T>
{
	private readonly object padlock = new object();

	private bool isProduced = false;

	private T value;

	public ProducerConsumerSwap(T value)
	{
		Debug.Assert(value != null);

		this.value = value;
	}

	public bool TrySwapProducer(ref T value)
	{
		Debug.Assert(value != null);

		lock (this.padlock)
		{
			if (!this.isProduced)
			{
				this.isProduced = true;
				this.Swap(ref value);
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public bool TrySwapConsumer(ref T value)
	{
		Debug.Assert(value != null);

		lock (this.padlock)
		{
			if (this.isProduced)
			{
				this.isProduced = false;
				this.Swap(ref value);
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	private void Swap(ref T value)
	{
		var temp = value;
		value = this.value;
		this.value = temp;
	}
}
