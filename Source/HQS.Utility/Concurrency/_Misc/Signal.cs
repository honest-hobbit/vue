﻿namespace HQS.Utility.Concurrency;

public sealed class Signal
{
	private readonly object padlock = new object();

	private readonly bool autoResets;

	private bool isSet;

	private Signal(bool autoResets, bool isSet)
	{
		this.autoResets = autoResets;
		this.isSet = isSet;
	}

	public bool IsSet
	{
		get
		{
			lock (this.padlock)
			{
				return this.isSet;
			}
		}
	}

	public static Signal CreateAutoReset(bool isSet = false) => new Signal(true, isSet);

	public static Signal CreateManualReset(bool isSet = false) => new Signal(false, isSet);

	public void Wait()
	{
		lock (this.padlock)
		{
			while (!this.isSet)
			{
				Monitor.Wait(this.padlock);
			}

			if (this.autoResets)
			{
				this.isSet = false;
			}
		}
	}

	public void Set()
	{
		lock (this.padlock)
		{
			if (!this.isSet)
			{
				this.isSet = true;
				Monitor.PulseAll(this.padlock);
			}
		}
	}

	public void Reset()
	{
		lock (this.padlock)
		{
			this.isSet = false;
		}
	}
}
