﻿namespace HQS.Utility.Concurrency;

public class AtomicDouble : IAtomicValue<double>
{
	private double value;

	public AtomicDouble(double value)
	{
		this.value = value;
	}

	/// <inheritdoc />
	public double Add(double value)
	{
		var current = this.Read();
		while (true)
		{
			var newValue = current + value;
			var result = Interlocked.CompareExchange(ref this.value, newValue, current);

			if (result == current)
			{
				return newValue;
			}
			else
			{
				current = result;
			}
		}
	}

	/// <inheritdoc />
	public double CompareExchange(double value, double comparand)
	{
		return Interlocked.CompareExchange(ref this.value, value, comparand);
	}

	/// <inheritdoc />
	public double Decrement()
	{
		return this.Add(-1);
	}

	/// <inheritdoc />
	public double Exchange(double value)
	{
		return Interlocked.Exchange(ref this.value, value);
	}

	/// <inheritdoc />
	public double Increment()
	{
		return this.Add(1);
	}

	/// <inheritdoc />
	public double Read()
	{
		return InterlockedValue.Read(ref this.value);
	}

	/// <inheritdoc />
	public void Write(double value)
	{
		this.Exchange(value);
	}
}
