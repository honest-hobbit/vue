﻿namespace HQS.Utility.Concurrency;

public class AtomicLong : IAtomicValue<long>
{
	private long value;

	public AtomicLong(long value)
	{
		this.value = value;
	}

	/// <inheritdoc />
	public long Add(long value)
	{
		return Interlocked.Add(ref this.value, value);
	}

	/// <inheritdoc />
	public long CompareExchange(long value, long comparand)
	{
		return Interlocked.CompareExchange(ref this.value, value, comparand);
	}

	/// <inheritdoc />
	public long Decrement()
	{
		return Interlocked.Decrement(ref this.value);
	}

	/// <inheritdoc />
	public long Exchange(long value)
	{
		return Interlocked.Exchange(ref this.value, value);
	}

	/// <inheritdoc />
	public long Increment()
	{
		return Interlocked.Increment(ref this.value);
	}

	/// <inheritdoc />
	public long Read()
	{
		return InterlockedValue.Read(ref this.value);
	}

	/// <inheritdoc />
	public void Write(long value)
	{
		this.Exchange(value);
	}
}
