﻿namespace HQS.Utility.Concurrency;

public class AtomicInt : IAtomicValue<int>
{
	private int value;

	public AtomicInt(int value)
	{
		this.value = value;
	}

	/// <inheritdoc />
	public int Add(int value)
	{
		return Interlocked.Add(ref this.value, value);
	}

	/// <inheritdoc />
	public int CompareExchange(int value, int comparand)
	{
		return Interlocked.CompareExchange(ref this.value, value, comparand);
	}

	/// <inheritdoc />
	public int Decrement()
	{
		return Interlocked.Decrement(ref this.value);
	}

	/// <inheritdoc />
	public int Exchange(int value)
	{
		return Interlocked.Exchange(ref this.value, value);
	}

	/// <inheritdoc />
	public int Increment()
	{
		return Interlocked.Increment(ref this.value);
	}

	/// <inheritdoc />
	public int Read()
	{
		return InterlockedValue.Read(ref this.value);
	}

	/// <inheritdoc />
	public void Write(int value)
	{
		this.Exchange(value);
	}
}
