﻿namespace HQS.Utility.Concurrency;

public class AtomicFloat : IAtomicValue<float>
{
	private float value;

	public AtomicFloat(float value)
	{
		this.value = value;
	}

	/// <inheritdoc />
	public float Add(float value)
	{
		var current = this.Read();
		while (true)
		{
			var newValue = current + value;
			var result = Interlocked.CompareExchange(ref this.value, newValue, current);

			if (result == current)
			{
				return newValue;
			}
			else
			{
				current = result;
			}
		}
	}

	/// <inheritdoc />
	public float CompareExchange(float value, float comparand)
	{
		return Interlocked.CompareExchange(ref this.value, value, comparand);
	}

	/// <inheritdoc />
	public float Decrement()
	{
		return this.Add(-1);
	}

	/// <inheritdoc />
	public float Exchange(float value)
	{
		return Interlocked.Exchange(ref this.value, value);
	}

	/// <inheritdoc />
	public float Increment()
	{
		return this.Add(1);
	}

	/// <inheritdoc />
	public float Read()
	{
		return InterlockedValue.Read(ref this.value);
	}

	/// <inheritdoc />
	public void Write(float value)
	{
		this.Exchange(value);
	}
}
