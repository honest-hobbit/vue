﻿namespace HQS.Utility.Concurrency;

public static class ConcurrentStackExtensions
{
	public static IEnumerable<T> PopAll<T>(this ConcurrentStack<T> stack)
	{
		Debug.Assert(stack != null);

		while (stack.TryPop(out var value))
		{
			yield return value;
		}
	}
}
