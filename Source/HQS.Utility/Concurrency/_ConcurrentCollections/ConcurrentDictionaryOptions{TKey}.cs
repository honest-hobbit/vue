﻿namespace HQS.Utility.Concurrency;

public class ConcurrentDictionaryOptions<TKey>
{
	public IEqualityComparer<TKey> Comparer { get; set; }

	public ConcurrencyOptions? Concurrency { get; set; }

	public ConcurrentDictionary<TKey, TValue> CreateDictionary<TValue>()
	{
		if (this.Comparer == null)
		{
			if (this.Concurrency == null)
			{
				return new ConcurrentDictionary<TKey, TValue>();
			}
			else
			{
				var options = this.Concurrency.Value;
				return new ConcurrentDictionary<TKey, TValue>(options.ConcurrencyLevel, options.Capacity);
			}
		}
		else
		{
			if (this.Concurrency == null)
			{
				return new ConcurrentDictionary<TKey, TValue>(this.Comparer);
			}
			else
			{
				var options = this.Concurrency.Value;
				return new ConcurrentDictionary<TKey, TValue>(options.ConcurrencyLevel, options.Capacity, this.Comparer);
			}
		}
	}
}
