﻿namespace HQS.Utility.Concurrency;

public static class ConcurrentDictionaryExtensions
{
	public static ConcurrentDictionary<TKey, TValue> CreateDictionaryNullSafe<TKey, TValue>(
		this ConcurrentDictionaryOptions<TKey> options) =>
		options?.CreateDictionary<TValue>() ?? new ConcurrentDictionary<TKey, TValue>();

	/// <summary>
	/// Attempts to remove and return the value that has the specified key from the dictionary.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="dictionary">The dictionary.</param>
	/// <param name="key">The key of the element to remove.</param>
	/// <returns>True if the element was removed successfully; otherwise false.</returns>
	public static bool TryRemove<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary, TKey key)
	{
		Debug.Assert(dictionary != null);
		return dictionary.TryRemove(key, out _);
	}

	#region Lock free versions of existing API

	/// <summary>
	/// Gets the number of key/value pairs contained in the dictionary.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="dictionary">The dictionary.</param>
	/// <returns>The number of key/value pairs contained in the dictionary.</returns>
	/// <remarks>
	/// <para>
	/// This is a lock free implementation that does not use snapshot semantics. However, it is O(n) performance.
	/// This should still be preferred over using the <see cref="ICollection{T}.Count"/> property as such requires acquiring all
	/// the dictionary's locks. If you need to check if the collection is empty use <see cref="IsEmptyLockFree"/> instead.
	/// </para><para>
	/// This value could become invalid as soon as it returns. Therefore it is only suitable for debugging purposes.
	/// </para>
	/// </remarks>
	public static int CountLockFree<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary)
	{
		Debug.Assert(dictionary != null);

		// AsEnumerableOnly() is required here because LINQ is optimized for ICollections,
		// and will attempt to use the Count property when it is available
		return dictionary.AsEnumerableOnly().Count();
	}

	/// <summary>
	/// Gets a value that indicates whether the dictionary is empty.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="dictionary">The dictionary.</param>
	/// <returns>True if the dictionary is empty; otherwise, false.</returns>
	/// <remarks>
	/// <para>
	/// This is a lock free implementation that does not use snapshot semantics.
	/// </para><para>
	/// This value could become invalid as soon as it returns. Therefore it is only suitable for debugging purposes.
	/// </para>
	/// </remarks>
	public static bool IsEmptyLockFree<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary)
	{
		Debug.Assert(dictionary != null);

		// AsEnumerableOnly() is required here because LINQ is optimized for ICollections,
		// and will attempt to use the Count property when it is available
		return !dictionary.AsEnumerableOnly().Any();
	}

	/// <summary>
	/// Gets an enumerable containing the keys in the dictionary.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="dictionary">The dictionary.</param>
	/// <returns>An enumerable containing the keys in the dictionary.</returns>
	/// <remarks>
	/// <para>
	/// This is a lock free implementation that does not use snapshot semantics.
	/// </para><para>
	/// Keys can be removed from the dictionary the moment this enumerator returns them or added
	/// while the enumerator is running without returning them.
	/// </para>
	/// </remarks>
	public static IEnumerable<TKey> KeysLockFree<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary)
	{
		Debug.Assert(dictionary != null);

		return dictionary.Select(item => item.Key);
	}

	/// <summary>
	/// Gets an enumerable containing the values in the dictionary.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="dictionary">The dictionary.</param>
	/// <returns>An enumerable containing the values in the dictionary.</returns>
	/// <remarks>
	/// <para>
	/// This is a lock free implementation that does not use snapshot semantics.
	/// </para><para>
	/// Values can be removed from the dictionary the moment this enumerator returns them or added
	/// while the enumerator is running without returning them.
	/// </para>
	/// </remarks>
	public static IEnumerable<TValue> ValuesLockFree<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary)
	{
		Debug.Assert(dictionary != null);

		return dictionary.Select(item => item.Value);
	}

	#endregion
}
