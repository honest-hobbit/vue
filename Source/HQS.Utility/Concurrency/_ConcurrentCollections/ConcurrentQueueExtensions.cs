﻿namespace HQS.Utility.Concurrency;

public static class ConcurrentQueueExtensions
{
	public static IEnumerable<T> DequeueAll<T>(this ConcurrentQueue<T> queue)
	{
		Debug.Assert(queue != null);

		while (queue.TryDequeue(out var value))
		{
			yield return value;
		}
	}
}
