﻿namespace HQS.Utility.Concurrency;

public readonly record struct ConcurrencyOptions
{
	public ConcurrencyOptions(int concurrencyLevel, int capacity)
	{
		Ensure.That(concurrencyLevel, nameof(concurrencyLevel)).IsGte(1);
		Ensure.That(capacity, nameof(capacity)).IsGte(0);

		this.ConcurrencyLevel = concurrencyLevel;
		this.Capacity = capacity;
	}

	public int ConcurrencyLevel { get; }

	public int Capacity { get; }
}
