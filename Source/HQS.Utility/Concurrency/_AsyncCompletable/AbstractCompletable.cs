﻿namespace HQS.Utility.Concurrency;

public abstract class AbstractCompletable : IAsyncCompletable
{
	private readonly TaskCompletionSource signal = new TaskCompletionSource(TaskCreationOptions.RunContinuationsAsynchronously);

	public AbstractCompletable()
	{
		this.Completion = this.HandleCompletionAsync();
	}

	/// <inheritdoc />
	public Task Completion { get; }

	/// <inheritdoc />
	public bool IsCompleting => this.signal.Task.IsCompleted;

	/// <inheritdoc />
	public void Complete() => this.signal.TrySetResult();

	protected abstract Task CompleteAsync();

	private async Task HandleCompletionAsync()
	{
		await this.signal.Task.DontMarshallContext();
		await this.CompleteAsync().DontMarshallContext();
	}
}
