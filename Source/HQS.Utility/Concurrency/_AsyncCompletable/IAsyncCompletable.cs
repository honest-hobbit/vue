﻿namespace HQS.Utility.Concurrency;

public interface IAsyncCompletable
{
	Task Completion { get; }

	bool IsCompleting { get; }

	void Complete();
}
