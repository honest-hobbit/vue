﻿namespace HQS.Utility.Concurrency;

public class DisposeCompletable : AbstractCompletable
{
	private readonly IDisposable disposable;

	public DisposeCompletable(IDisposable disposable)
	{
		Debug.Assert(disposable != null);

		this.disposable = disposable;
	}

	/// <inheritdoc />
	protected override Task CompleteAsync()
	{
		this.disposable.Dispose();
		return Task.CompletedTask;
	}
}
