﻿namespace HQS.Utility.Concurrency;

public class AggregateCompletable : AbstractCompletable
{
	private readonly IAsyncCompletable[] completables;

	public AggregateCompletable(IEnumerable<IAsyncCompletable> completables)
	{
		Debug.Assert(completables.AllAndSelfNotNull());

		this.completables = completables.ToArrayExtended();
	}

	public AggregateCompletable(params IAsyncCompletable[] completables)
	{
		Debug.Assert(completables.AllAndSelfNotNull());

		this.completables = completables.Copy();
	}

	/// <inheritdoc />
	protected override async Task CompleteAsync()
	{
		List<Exception> exceptions = null;

		for (int i = 0; i < this.completables.Length; i++)
		{
			try
			{
				this.completables[i].Complete();
				await this.completables[i].Completion.DontMarshallContext();
			}
			catch (Exception error)
			{
				exceptions ??= new List<Exception>();
				exceptions.Add(error);
			}
		}

		if (exceptions != null)
		{
			throw new AggregateException(exceptions);
		}
	}
}
