﻿namespace HQS.Utility.Concurrency;

public abstract class AbstractRangeProcessor : AbstractDisposable
{
	private readonly object padlock = new object();

	private readonly List<Exception> errors = new List<Exception>();

	private readonly AtomicInt sharedIndex = new AtomicInt(-1);

	private readonly CountdownEvent threadsCompleted;

	private readonly WaitCallback runAction;

	private int maxDegreeOfParallelism;

	private int end;

	public AbstractRangeProcessor()
	{
		this.maxDegreeOfParallelism = DegreeOfParallelism.ProcessorCount;
		this.threadsCompleted = new CountdownEvent(this.maxDegreeOfParallelism);
		this.runAction = _ => this.HandleRunAction();
	}

	public int MaxDegreeOfParallelism
	{
		get => this.maxDegreeOfParallelism;
		set => this.maxDegreeOfParallelism = DegreeOfParallelism.ClampToProcessorCount(value);
	}

	public void Run(int count) => this.Run(0, count);

	public void Run(int start, int end)
	{
		if (end <= start)
		{
			return;
		}

		this.end = end;
		this.sharedIndex.Write(start - 1);
		this.threadsCompleted.Reset(this.maxDegreeOfParallelism);

		// start at 1 to skip a thread to account for the calling thread already being used
		for (int count = 1; count < this.maxDegreeOfParallelism; count++)
		{
			ThreadPool.QueueUserWorkItem(this.runAction);
		}

		this.HandleRunAction();
		this.threadsCompleted.Wait();

		// all background threads have finished so there's no need to lock here
		if (this.errors.Count > 0)
		{
			var error = new AggregateException(this.errors);
			this.errors.Clear();
			throw error;
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.threadsCompleted.Dispose();

	protected bool TryGetNextIndex(out int index)
	{
		index = this.sharedIndex.Increment();
		return index < this.end;
	}

	protected abstract void RunAction();

	private void HandleRunAction()
	{
		try
		{
			this.RunAction();
		}
		catch (Exception e)
		{
			lock (this.padlock)
			{
				this.errors.Add(e);
			}
		}
		finally
		{
			this.threadsCompleted.Signal();
		}
	}
}
