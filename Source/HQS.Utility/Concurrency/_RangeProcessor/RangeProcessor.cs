﻿namespace HQS.Utility.Concurrency;

public class RangeProcessor : AbstractRangeProcessor
{
	private readonly ProcessIndex processRange;

	public RangeProcessor(ProcessIndex processRange)
	{
		Ensure.That(processRange, nameof(processRange)).IsNotNull();

		this.processRange = processRange;
	}

	/// <inheritdoc />
	protected override void RunAction()
	{
		while (this.TryGetNextIndex(out int index))
		{
			this.processRange(index);
		}
	}
}
