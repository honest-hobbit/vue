﻿namespace HQS.Utility.Concurrency;

public class MergeRangeProcessor<TState, TResult> : AbstractDisposable
{
	private readonly object padlock = new object();

	private readonly List<TState> states;

	private readonly RangeProcessor<TState> processor;

	private readonly MergeState<TState, TResult> mergeStates;

	private readonly HandleState<TState> finalizeState;

	public MergeRangeProcessor(
		ProcessIndex<TState> processRange,
		MergeState<TState, TResult> mergeStates,
		HandleState<TState> initializeState,
		HandleState<TState> finalizeState = null)
	{
		Ensure.That(processRange, nameof(processRange)).IsNotNull();
		Ensure.That(mergeStates, nameof(mergeStates)).IsNotNull();
		Ensure.That(initializeState, nameof(initializeState)).IsNotNull();

		this.states = new List<TState>(DegreeOfParallelism.ProcessorCount);
		this.processor = new RangeProcessor<TState>(
			processRange, initializeState, this.FinalizeAndCopyState);

		this.mergeStates = mergeStates;
		this.finalizeState = finalizeState;
	}

	public int MaxDegreeOfParallelism
	{
		get => this.processor.MaxDegreeOfParallelism;
		set => this.processor.MaxDegreeOfParallelism = value;
	}

	public TResult Run(int count) => this.Run(0, count);

	public TResult Run(int start, int end)
	{
		this.states.Clear();
		try
		{
			this.processor.Run(start, end);

			// all background threads have finished so there's no need to lock here
			return this.mergeStates(this.states);
		}
		finally
		{
			this.states.Clear();
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.processor.Dispose();

	private void FinalizeAndCopyState(ref TState state)
	{
		try
		{
			this.finalizeState?.Invoke(ref state);
		}
		finally
		{
			lock (this.padlock)
			{
				this.states.Add(state);
			}
		}
	}
}
