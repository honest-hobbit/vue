﻿namespace HQS.Utility.Concurrency;

public class RangeProcessor<T> : AbstractRangeProcessor
{
	private readonly ProcessIndex<T> processRange;

	private readonly HandleState<T> initializeState;

	private readonly HandleState<T> deinitializeState;

	public RangeProcessor(
		ProcessIndex<T> processRange,
		HandleState<T> initializeState,
		HandleState<T> deinitializeState = null)
	{
		Ensure.That(processRange, nameof(processRange)).IsNotNull();
		Ensure.That(initializeState, nameof(initializeState)).IsNotNull();

		this.processRange = processRange;
		this.initializeState = initializeState;
		this.deinitializeState = deinitializeState;
	}

	/// <inheritdoc />
	protected override void RunAction()
	{
		var state = default(T);
		this.initializeState(ref state);

		try
		{
			while (this.TryGetNextIndex(out int index))
			{
				this.processRange(ref state, index);
			}
		}
		finally
		{
			this.deinitializeState?.Invoke(ref state);
		}
	}
}
