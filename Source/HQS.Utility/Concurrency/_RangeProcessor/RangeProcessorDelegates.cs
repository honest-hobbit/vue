﻿namespace HQS.Utility.Concurrency;

public delegate void ProcessIndex(int index);

public delegate void ProcessIndex<T>(ref T state, int index);

public delegate void HandleState<T>(ref T state);

public delegate TResult MergeState<TState, TResult>(List<TState> states);
