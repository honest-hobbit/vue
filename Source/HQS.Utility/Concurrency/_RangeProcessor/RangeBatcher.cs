﻿namespace HQS.Utility.Concurrency;

public readonly record struct RangeBatcher
{
	public RangeBatcher(int count, int batchSize)
	{
		Ensure.That(count, nameof(count)).IsGte(0);
		Ensure.That(batchSize, nameof(batchSize)).IsGte(1);

		this.Count = count;
		this.BatchSize = batchSize;
	}

	public int Count { get; }

	public int BatchSize { get; }

	public int BatchesCount => MathUtility.DivideRoundUp(this.Count, this.BatchSize);

	public void GetBatchedRange(int batchIndex, out int inclusiveStart, out int exclusiveEnd)
	{
		inclusiveStart = batchIndex * this.BatchSize;
		exclusiveEnd = (inclusiveStart + this.BatchSize).ClampUpper(this.Count);
	}

	public (int inclusiveStart, int exclusiveEnd) GetBatchedRange(int batchIndex)
	{
		int start = batchIndex * this.BatchSize;
		return (start, (start + this.BatchSize).ClampUpper(this.Count));
	}
}
