﻿namespace HQS.Utility.Concurrency;

public class MonitorLock
{
	private readonly object padlock = new object();

	public bool IsLockHeld => Monitor.IsEntered(this.padlock);

	public void EnterLock() => Monitor.Enter(this.padlock);

	public bool TryEnterLock() => Monitor.TryEnter(this.padlock);

	public bool TryEnterLock(int millisecondsTimeout) => Monitor.TryEnter(this.padlock, millisecondsTimeout);

	public bool TryEnterLock(TimeSpan timeout) => Monitor.TryEnter(this.padlock, timeout);

	public void ExitLock() => Monitor.Exit(this.padlock);
}
