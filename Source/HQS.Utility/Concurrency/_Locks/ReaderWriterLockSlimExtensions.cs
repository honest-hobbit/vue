﻿namespace HQS.Utility.Concurrency;

public static class ReaderWriterLockSlimExtensions
{
	public static ExitReadLock EnterReadLockInUsingBlock(this ReaderWriterLockSlim slimLock)
	{
		Debug.Assert(slimLock != null);

		slimLock.EnterReadLock();
		return new ExitReadLock(slimLock);
	}

	public static ExitWriteLock EnterWriteLockInUsingBlock(this ReaderWriterLockSlim slimLock)
	{
		Debug.Assert(slimLock != null);

		slimLock.EnterWriteLock();
		return new ExitWriteLock(slimLock);
	}
}
