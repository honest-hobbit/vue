﻿namespace HQS.Utility.Concurrency;

/// <summary>
/// Provides extension methods for <see cref="SemaphoreSlim"/>.
/// </summary>
public static class SemaphoreSlimExtensions
{
	public static ReleaseSemaphore WaitInUsingBlock(this SemaphoreSlim semaphore)
	{
		Debug.Assert(semaphore != null);

		semaphore.Wait();
		return new ReleaseSemaphore(semaphore);
	}
}
