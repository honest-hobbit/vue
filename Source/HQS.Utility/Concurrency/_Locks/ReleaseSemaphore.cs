﻿namespace HQS.Utility.Concurrency;

public readonly record struct ReleaseSemaphore : IDisposable
{
	// default struct constructor leaves this field as null so methods must be null safe
	private readonly SemaphoreSlim semaphore;

	internal ReleaseSemaphore(SemaphoreSlim semaphore)
	{
		Debug.Assert(semaphore != null);

		this.semaphore = semaphore;
	}

	/// <inheritdoc />
	public void Dispose() => this.semaphore?.Release();
}
