﻿namespace HQS.Utility.Concurrency;

// A lock similar to the reader writer lock where there may be many simultaneous readers and a single writer.
// However, readers may run at the same time as the single writer. Once a writer finishes it must enter
// the commit lock to actually commit its writes. All reads are blocked while in the commit lock.
//
// The write lock must be entered before entering the commit lock and the locks must be exited in the opposite order;
// enter write -> enter commit -> exit commit -> exit write.
//
// A writer can downgrade to a reader by entering the read lock before exiting the write lock.
// However, a reader can't upgrade to a writer. Recursively acquiring locks is also not allowed.
//
// This lock favors commits; while a thread is waiting to enter the commit lock all new attempts to enter the
// read or write locks will block until the waiting thread is able to enter and exit the commit lock. Threads that
// already hold a read or write lock will continue to run as normal to complete their work and exit their locks.
public class CommitLock : AbstractDisposable
{
	private readonly ReaderWriterLockSlim commitLock = new ReaderWriterLockSlim();

	public bool IsReadLockHeld => this.commitLock.IsReadLockHeld;

	public bool IsWriteLockHeld => this.commitLock.IsUpgradeableReadLockHeld;

	public bool IsCommitLockHeld => this.commitLock.IsWriteLockHeld;

	public bool HasReadAccess => this.IsReadLockHeld || this.IsWriteLockHeld || this.IsCommitLockHeld;

	public void EnterReadLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(!this.IsReadLockHeld);

		this.commitLock.EnterReadLock();
	}

	public bool TryEnterReadLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(!this.IsReadLockHeld);

		return this.commitLock.TryEnterReadLock(0);
	}

	// must exit on the same thread that entered
	public void ExitReadLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsReadLockHeld);

		this.commitLock.ExitReadLock();
	}

	public void EnterWriteLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(!this.IsReadLockHeld);
		Debug.Assert(!this.IsWriteLockHeld);
		Debug.Assert(!this.IsCommitLockHeld);

		this.commitLock.EnterUpgradeableReadLock();
	}

	public bool TryEnterWriteLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(!this.IsReadLockHeld);
		Debug.Assert(!this.IsWriteLockHeld);
		Debug.Assert(!this.IsCommitLockHeld);

		return this.commitLock.TryEnterUpgradeableReadLock(0);
	}

	// must exit on the same thread that entered
	public void ExitWriteLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsWriteLockHeld);
		Debug.Assert(!this.IsCommitLockHeld);

		this.commitLock.ExitUpgradeableReadLock();
	}

	public void EnterCommitLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsWriteLockHeld);
		Debug.Assert(!this.IsCommitLockHeld);

		this.commitLock.EnterWriteLock();
	}

	public bool TryEnterCommitLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsWriteLockHeld);
		Debug.Assert(!this.IsCommitLockHeld);

		return this.commitLock.TryEnterWriteLock(0);
	}

	// must exit on the same thread that entered
	public void ExitCommitLock()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsCommitLockHeld);
		Debug.Assert(this.IsWriteLockHeld);

		this.commitLock.ExitWriteLock();
	}

	public void DowngradeWriteLockToRead()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsWriteLockHeld);
		Debug.Assert(!this.IsCommitLockHeld);

		this.commitLock.EnterReadLock();
		this.commitLock.ExitUpgradeableReadLock();
	}

	public void DowngradeCommitLockToRead()
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsWriteLockHeld);
		Debug.Assert(this.IsCommitLockHeld);

		this.commitLock.ExitWriteLock();
		this.commitLock.EnterReadLock();
		this.commitLock.ExitUpgradeableReadLock();
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.commitLock.Dispose();
}
