﻿namespace HQS.Utility.Concurrency;

public readonly record struct ExitReadLock : IDisposable
{
	// default struct constructor leaves this field as null so methods must be null safe
	private readonly ReaderWriterLockSlim slimLock;

	internal ExitReadLock(ReaderWriterLockSlim slimLock)
	{
		Debug.Assert(slimLock != null);

		this.slimLock = slimLock;
	}

	/// <inheritdoc />
	public void Dispose() => this.slimLock?.ExitReadLock();
}
