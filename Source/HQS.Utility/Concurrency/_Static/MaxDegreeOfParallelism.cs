﻿namespace HQS.Utility.Concurrency;

public static class DegreeOfParallelism
{
	public static int ProcessorCount => Environment.ProcessorCount.ClampLower(1);

	public const int Unbounded = DataflowBlockOptions.Unbounded;

	public static int ClampToProcessorCount(int maxDegreeOfParallelism) =>
		ClampTo(maxDegreeOfParallelism, ProcessorCount);

	public static int ClampTo(int maxDegreeOfParallelism, int max) =>
		maxDegreeOfParallelism == Unbounded ? max : maxDegreeOfParallelism.Clamp(1, max);

	public static bool IsValid(int maxDegreeOfParallelism) =>
		maxDegreeOfParallelism >= 1 || maxDegreeOfParallelism == Unbounded;

	public static void Validate(int maxDegreeOfParallelism) =>
		Validate(maxDegreeOfParallelism, nameof(maxDegreeOfParallelism));

	public static void Validate(int maxDegreeOfParallelism, string paramName)
	{
		if (!IsValid(maxDegreeOfParallelism))
		{
			throw new ArgumentOutOfRangeException(
				paramName, $"Must be greater than 0 or equal to {nameof(Unbounded)}.");
		}
	}
}
