﻿namespace HQS.Utility.Concurrency;

public static class TaskExtensions
{
	#region PropagateResultTo

	public static Task PropagateResultTo(this Task task, TaskCompletionSource completion)
	{
		Debug.Assert(task != null);
		Debug.Assert(completion != null);

		return task.ContinueWith(
			completedTask =>
			{
				switch (completedTask.Status)
				{
					case TaskStatus.Canceled:
						completion.SetCanceled(); break;
					case TaskStatus.Faulted:
						completion.SetException(completedTask.Exception.InnerExceptions); break;
					case TaskStatus.RanToCompletion:
						completion.SetResult(); break;
				}
			},
			CancellationToken.None,
			TaskContinuationOptions.ExecuteSynchronously,
			TaskScheduler.Default);
	}

	public static Task PropagateResultTo<TResult>(this Task<TResult> task, TaskCompletionSource<TResult> completion)
	{
		Debug.Assert(task != null);
		Debug.Assert(completion != null);

		return task.ContinueWith(
			completedTask =>
			{
				switch (completedTask.Status)
				{
					case TaskStatus.Canceled:
						completion.SetCanceled(); break;
					case TaskStatus.Faulted:
						completion.SetException(completedTask.Exception.InnerExceptions); break;
					case TaskStatus.RanToCompletion:
						completion.SetResult(completedTask.Result); break;
				}
			},
			CancellationToken.None,
			TaskContinuationOptions.ExecuteSynchronously,
			TaskScheduler.Default);
	}

	#endregion

	#region [Dont]MarshallContext

	/// <summary>
	/// Configures the await to not attempt to marshal the continuation back to the original context captured.
	/// </summary>
	/// <param name="task">The task to configure the await for.</param>
	/// <returns>An object used to await this task.</returns>
	/// <remarks>
	/// This method makes it so that the code after the await can be ran on any available thread pool thread
	/// instead of being marshalled back to the same thread that the code was running on before the await.
	/// Avoiding this marshalling helps to slightly improve performance and helps avoid potential deadlocks. See
	/// <see href="http://blog.ciber.no/2014/05/19/using-task-configureawaitfalse-to-prevent-deadlocks-in-async-code/">
	/// this link</see> for more information.
	/// </remarks>
	public static ConfiguredTaskAwaitable DontMarshallContext(this Task task)
	{
		Debug.Assert(task != null);

		return task.ConfigureAwait(false);
	}

	/// <summary>
	/// Configures the await to not attempt to marshal the continuation back to the original context captured.
	/// </summary>
	/// <typeparam name="T">The type of the value returned by the task.</typeparam>
	/// <param name="task">The task to configure the await for.</param>
	/// <returns>
	/// An object used to await this task.
	/// </returns>
	/// <remarks>
	/// This method makes it so that the code after the await can be ran on any available thread pool thread
	/// instead of being marshalled back to the same thread that the code was running on before the await.
	/// Avoiding this marshalling helps to slightly improve performance and helps avoid potential deadlocks. See
	/// <see href="http://blog.ciber.no/2014/05/19/using-task-configureawaitfalse-to-prevent-deadlocks-in-async-code/">
	/// this link</see> for more information.
	/// </remarks>
	public static ConfiguredTaskAwaitable<T> DontMarshallContext<T>(this Task<T> task)
	{
		Debug.Assert(task != null);

		return task.ConfigureAwait(false);
	}

	/// <summary>
	/// Configures the await to attempt to marshal the continuation back to the original context captured.
	/// </summary>
	/// <param name="task">The task to configure the await for.</param>
	/// <returns>An object used to await this task.</returns>
	/// <remarks>
	/// This method makes it so that the code after the await will try to be ran on the original synchronization context.
	/// </remarks>
	public static ConfiguredTaskAwaitable MarshallContext(this Task task)
	{
		Debug.Assert(task != null);

		return task.ConfigureAwait(true);
	}

	/// <summary>
	/// Configures the await to attempt to marshal the continuation back to the original context captured.
	/// </summary>
	/// <typeparam name="T">The type of the value returned by the task.</typeparam>
	/// <param name="task">The task to configure the await for.</param>
	/// <returns>
	/// An object used to await this task.
	/// </returns>
	/// <remarks>
	/// This method makes it so that the code after the await will try to be ran on the original synchronization context.
	/// </remarks>
	public static ConfiguredTaskAwaitable<T> MarshallContext<T>(this Task<T> task)
	{
		Debug.Assert(task != null);

		return task.ConfigureAwait(true);
	}

	#endregion
}
