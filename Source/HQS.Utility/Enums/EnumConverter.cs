﻿namespace HQS.Utility.Enums;

public static class EnumConverter
{
	public static byte ToByte<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, byte>.ToPrimitive(value);

	public static sbyte ToSByte<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, sbyte>.ToPrimitive(value);

	public static short ToShort<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, short>.ToPrimitive(value);

	public static ushort ToUShort<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, ushort>.ToPrimitive(value);

	public static int ToInt<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, int>.ToPrimitive(value);

	public static uint ToUInt<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, uint>.ToPrimitive(value);

	public static long ToLong<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, long>.ToPrimitive(value);

	public static ulong ToULong<TEnum>(this TEnum value)
		where TEnum : struct, Enum => Convert<TEnum, ulong>.ToPrimitive(value);

	public static TEnum FromByteTo<TEnum>(byte value)
		where TEnum : struct, Enum => Convert<TEnum, byte>.ToEnum(value);

	public static TEnum FromSByteTo<TEnum>(sbyte value)
		where TEnum : struct, Enum => Convert<TEnum, sbyte>.ToEnum(value);

	public static TEnum FromShortTo<TEnum>(short value)
		where TEnum : struct, Enum => Convert<TEnum, short>.ToEnum(value);

	public static TEnum FromUShortTo<TEnum>(ushort value)
		where TEnum : struct, Enum => Convert<TEnum, ushort>.ToEnum(value);

	public static TEnum FromIntTo<TEnum>(int value)
		where TEnum : struct, Enum => Convert<TEnum, int>.ToEnum(value);

	public static TEnum FromUIntTo<TEnum>(uint value)
		where TEnum : struct, Enum => Convert<TEnum, uint>.ToEnum(value);

	public static TEnum FromLongTo<TEnum>(long value)
		where TEnum : struct, Enum => Convert<TEnum, long>.ToEnum(value);

	public static TEnum FromULongTo<TEnum>(ulong value)
		where TEnum : struct, Enum => Convert<TEnum, ulong>.ToEnum(value);

	private class Convert<TEnum, TValue>
		where TEnum : struct, Enum
		where TValue : struct
	{
		public static readonly Func<TEnum, TValue> ToPrimitive = CreateConverter<TEnum, TValue>();

		public static readonly Func<TValue, TEnum> ToEnum = CreateConverter<TValue, TEnum>();

		private static Func<TSource, TResult> CreateConverter<TSource, TResult>()
			where TSource : struct
			where TResult : struct
		{
			// TResult Convert(TSource value) => (TResult)value;
			var valueParameter = Expression.Parameter(typeof(TSource), "value");
			var convertExpression = Expression.Convert(valueParameter, typeof(TResult));
			return Expression.Lambda<Func<TSource, TResult>>(convertExpression, new[] { valueParameter }).Compile();
		}
	}
}
