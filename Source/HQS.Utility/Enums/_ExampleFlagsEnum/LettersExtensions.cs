﻿namespace HQS.Utility.Enums;

internal static class LettersExtensions
{
	/// <summary>
	/// Determines whether the current flags value contains the specified flag or flags (all of them).
	/// This implementation is faster than the Enum.HasFlag method because it avoids boxing of values.
	/// </summary>
	/// <param name="current">The current value to check for containing flags.</param>
	/// <param name="flag">The flag or combined flags to check for.</param>
	/// <returns>True if the current value contains the flag(s); otherwise false.</returns>
	public static bool Has(this Letters current, Letters flag) => (current & flag) == flag;

	public static bool HasAny(this Letters current, Letters flags) => (current & flags) != 0;

	public static Letters Add(this Letters current, Letters flags) => current | flags;

	public static Letters Remove(this Letters current, Letters flags) => current & ~flags;

	public static Letters Set(this Letters current, Letters flags, bool value) =>
		value ? current.Add(flags) : current.Remove(flags);
}
