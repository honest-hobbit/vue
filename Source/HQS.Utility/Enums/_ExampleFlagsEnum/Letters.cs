﻿namespace HQS.Utility.Enums;

[Flags]
internal enum Letters
{
	None = 0,

	A = 1,

	B = 1 << 1,

	C = 1 << 2,

	D = 1 << 3,

	All = A | B | C | D,
}
