﻿namespace HQS.Utility.Enums;

/// <summary>
/// Provides helper methods for Enum types.
/// </summary>
public static class Enumeration
{
	public static bool IsFlagsEnum<T>()
		where T : struct, Enum => Traits<T>.Instance.IsFlagsEnum;

	/// <summary>
	/// Gets the values of an enum as a type-safe read only list.
	/// </summary>
	/// <typeparam name="T">The type of the enum.</typeparam>
	/// <returns>The values of that enum.</returns>
	/// <remarks>
	/// The values are returned in ascending unsigned binary order. For more information see
	/// <see href="https://msdn.microsoft.com/en-us/library/system.enum.getvalues%28v=vs.110%29.aspx">this link</see>.
	/// </remarks>
	public static IReadOnlyList<T> Values<T>()
		where T : struct, Enum => Traits<T>.Instance.Values;

	/// <summary>
	/// Determines whether the specified value would convert to a valid enum value.
	/// </summary>
	/// <typeparam name="T">The type of the enum.</typeparam>
	/// <param name="value">The value to check.</param>
	/// <returns>True if the value would convert to a valid enum value; otherwise false.</returns>
	/// <remarks>This method cannot be used for flags enums.</remarks>
	public static bool IsDefined<T>(T value)
		where T : struct, Enum => Traits<T>.Instance.IsDefined(value);

	public static long MinValue<T>()
		where T : struct, Enum => Traits<T>.Instance.MinValue;

	public static long MaxValue<T>()
		where T : struct, Enum => Traits<T>.Instance.MaxValue;

	public static int? EnumIndexedArrayLength<T>()
		where T : struct, Enum => Traits<T>.Instance.EnumIndexedArrayLength;

	public static TValue[] CreateEnumIndexedArray<TEnum, TValue>()
		where TEnum : struct, Enum => CreateEnumIndexedArray<TEnum, TValue>(default);

	public static TValue[] CreateEnumIndexedArray<TEnum, TValue>(TValue value)
		where TEnum : struct, Enum
	{
		Debug.Assert(EnumIndexedArrayLength<TEnum>().HasValue);

		return Factory.CreateArray(Traits<TEnum>.Instance.EnumIndexedArrayLength.Value, value);
	}

	public static void ValidateIsDefined<T>(T value, string argumentName)
		where T : struct, Enum
	{
		Debug.Assert(!argumentName.IsNullOrWhiteSpace());

		if (!IsDefined(value))
		{
			throw InvalidEnumArgument.CreateException(argumentName, value);
		}
	}

	private sealed class Traits<T>
		where T : struct, Enum
	{
		public static Traits<T> Instance { get; } = new Traits<T>();

		private readonly HashSet<T> valuesSet;

		private Traits()
		{
			var type = typeof(T);

			this.IsFlagsEnum = type.GetCustomAttributes<FlagsAttribute>().Any();
			this.Values = ((T[])Enum.GetValues(type)).AsReadOnlyList();
			this.valuesSet = new HashSet<T>(this.Values);

			if (this.Values.Count == 0)
			{
				this.MinValue = 0;
				this.MaxValue = -1;
				this.EnumIndexedArrayLength = 0;
			}
			else
			{
				this.MinValue = this.Values.Select(x => x.ToLong()).Min();
				this.MaxValue = this.Values.Select(x => x.ToLong()).Max();

				long length = this.MaxValue + 1;
				if (this.MinValue < 0 || length > int.MaxValue)
				{
					this.EnumIndexedArrayLength = null;
				}
				else
				{
					this.EnumIndexedArrayLength = (int)length;
				}
			}
		}

		public bool IsFlagsEnum { get; }

		public IReadOnlyList<T> Values { get; }

		public long MinValue { get; }

		public long MaxValue { get; }

		public int? EnumIndexedArrayLength { get; }

		public bool IsDefined(T value)
		{
			Debug.Assert(!this.IsFlagsEnum);

			return this.valuesSet.Contains(value);
		}
	}
}
