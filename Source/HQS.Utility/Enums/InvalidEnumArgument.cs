﻿namespace HQS.Utility.Enums;

/// <summary>
/// Provides helper methods for creating instances of <see cref="InvalidEnumArgumentException"/>.
/// </summary>
public static class InvalidEnumArgument
{
	public static InvalidEnumArgumentException CreateException<TEnum>(string argumentName, TEnum value)
		where TEnum : struct, Enum
	{
		Debug.Assert(!argumentName.IsNullOrWhiteSpace());

		return new InvalidEnumArgumentException(argumentName, Convert.ToInt32(value), typeof(TEnum));
	}
}
