﻿namespace HQS.Utility;

[Serializable]
public class TypeArgumentException : ArgumentException
{
	public TypeArgumentException()
		: base()
	{
	}

	public TypeArgumentException(string message)
		: base(message)
	{
	}

	public TypeArgumentException(string message, Exception innerException)
		: base(message, innerException)
	{
	}

	public TypeArgumentException(string message, string paramName)
		: base(message, paramName)
	{
	}

	public TypeArgumentException(string message, string paramName, Exception innerException)
		: base(message, paramName, innerException)
	{
	}

	protected TypeArgumentException(SerializationInfo info, StreamingContext context)
		: base(info, context)
	{
	}
}
