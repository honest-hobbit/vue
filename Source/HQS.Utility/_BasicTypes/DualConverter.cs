﻿namespace HQS.Utility;

public static class DualConverter
{
	public static IDualConverter<T1, T2> Create<T1, T2>(Func<T1, T2> toResult, Func<T2, T1> toSource) =>
		new DualConverter<T1, T2>(toResult, toSource);

	public static IDualConverter<T1, T2> CastSub<T1, T2>()
		where T2 : T1 => new DualConverter<T1, T2>(x => (T2)x, x => x);

	public static IDualConverter<T1, T2> CastSuper<T1, T2>()
		where T1 : T2 => new DualConverter<T1, T2>(x => x, x => (T1)x);
}
