﻿namespace HQS.Utility;

[StructLayout(LayoutKind.Explicit, Size = 4)]
public struct ColorRGB : IEquatable<ColorRGB>
{
	[FieldOffset(0)]
	public uint Value;

	[FieldOffset(0)]
	public byte R;

	[FieldOffset(1)]
	public byte G;

	[FieldOffset(2)]
	public byte B;

	[FieldOffset(3)]
	public byte A;

	public ColorRGB(uint value)
	{
		this.R = 0;
		this.G = 0;
		this.B = 0;
		this.A = 0;
		this.Value = value;
	}

	public ColorRGB(byte r, byte g, byte b)
		: this(r, g, b, 255)
	{
	}

	public ColorRGB(byte r, byte g, byte b, byte a)
	{
		this.Value = 0;
		this.R = r;
		this.G = g;
		this.B = b;
		this.A = a;
	}

	public static ColorRGB From(Color color) => new ColorRGB(color.R, color.G, color.B, color.A);

	public static ColorRGB From(Vector4 color) => new ColorRGB(
		(color.X * 255).ClampToByte(),
		(color.Y * 255).ClampToByte(),
		(color.Z * 255).ClampToByte(),
		(color.W * 255).ClampToByte());

	public static ColorRGB From(Vector3 color) => new ColorRGB(
		(color.X * 255).ClampToByte(),
		(color.Y * 255).ClampToByte(),
		(color.Z * 255).ClampToByte(),
		255);

	public static ColorRGB From(ColorRGB color, int alpha) => new ColorRGB(
		color.R,
		color.G,
		color.B,
		alpha.ClampToByte());

	public static ColorRGB From(ColorRGB color, float alpha) => new ColorRGB(
		color.R,
		color.G,
		color.B,
		(alpha * 255).ClampToByte());

	public static ColorRGB From(int r, int g, int b) => new ColorRGB(
		r.ClampToByte(),
		g.ClampToByte(),
		b.ClampToByte(),
		255);

	public static ColorRGB From(int r, int g, int b, int a) => new ColorRGB(
		r.ClampToByte(),
		g.ClampToByte(),
		b.ClampToByte(),
		a.ClampToByte());

	public static ColorRGB From(float r, float g, float b) => new ColorRGB(
		(r * 255).ClampToByte(),
		(g * 255).ClampToByte(),
		(b * 255).ClampToByte(),
		255);

	public static ColorRGB From(float r, float g, float b, float a) => new ColorRGB(
		(r * 255).ClampToByte(),
		(g * 255).ClampToByte(),
		(b * 255).ClampToByte(),
		(a * 255).ClampToByte());

	public static ColorRGB Lerp(ColorRGB value1, ColorRGB value2, float amount)
	{
		amount = amount.Clamp(0, 1);
		return new ColorRGB(
			Lerp(value1.R, value2.R, amount).ClampToByte(),
			Lerp(value1.G, value2.G, amount).ClampToByte(),
			Lerp(value1.B, value2.B, amount).ClampToByte(),
			Lerp(value1.A, value2.A, amount).ClampToByte());
	}

	private static float Lerp(float value1, float value2, float amount) => value1 + ((value2 - value1) * amount);

	public static ColorRGB operator *(ColorRGB value, float scale) => value.Multiply(scale);

	public static bool operator ==(ColorRGB lhs, ColorRGB rhs) => lhs.Equals(rhs);

	public static bool operator !=(ColorRGB lhs, ColorRGB rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ColorRGB other) => this.Value == other.Value;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.Value.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => $"RGBA[{this.R}, {this.G}, {this.B}, {this.A}]";

	public ColorRGB Multiply(float scale) => new ColorRGB(
		(this.R * scale).ClampToByte(),
		(this.G * scale).ClampToByte(),
		(this.B * scale).ClampToByte(),
		(this.A * scale).ClampToByte());

	public Vector3 ToVector3() => new Vector3(this.R / 255f, this.G / 255f, this.B / 255f);

	public Vector4 ToVector4() => new Vector4(this.R / 255f, this.G / 255f, this.B / 255f, this.A / 255f);

	public Color ToSystem() => Color.FromArgb(this.A, this.R, this.G, this.B);
}
