﻿namespace HQS.Utility;

public static class ColorExtensions
{
	public static ColorRGB ToHQS(this Color color) => new ColorRGB(color.R, color.G, color.B, color.A);
}
