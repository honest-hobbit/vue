﻿namespace HQS.Utility;

public static class Colors
{
	/// <summary>
	/// Gets the transparent Black color (r:0,g:0,b:0,a:0).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB TransparentBlack => new ColorRGB(r: 0, g: 0, b: 0, a: 0);

	/// <summary>
	/// Gets the transparent Color (r:255,g:255,b:255,a:0).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB TransparentWhite => new ColorRGB(r: 255, g: 255, b: 255, a: 0);

	/// <summary>
	/// Gets the Alice Blue color (r:240,g:248,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB AliceBlue => new ColorRGB(r: 240, g: 248, b: 255, a: 255);

	/// <summary>
	/// Gets the Antique White color (r:250,g:235,b:215,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB AntiqueWhite => new ColorRGB(r: 250, g: 235, b: 215, a: 255);

	/// <summary>
	/// Gets the Aqua color (r:0,g:255,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Aqua => new ColorRGB(r: 0, g: 255, b: 255, a: 255);

	/// <summary>
	/// Gets the Aquamarine color (r:127,g:255,b:212,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Aquamarine => new ColorRGB(r: 127, g: 255, b: 212, a: 255);

	/// <summary>
	/// Gets the Azure color (r:240,g:255,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Azure => new ColorRGB(r: 240, g: 255, b: 255, a: 255);

	/// <summary>
	/// Gets the Beige color (r:245,g:245,b:220,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Beige => new ColorRGB(r: 245, g: 245, b: 220, a: 255);

	/// <summary>
	/// Gets the Bisque color (r:255,g:228,b:196,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Bisque => new ColorRGB(r: 255, g: 228, b: 196, a: 255);

	/// <summary>
	/// Gets the Black color (r:0,g:0,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Black => new ColorRGB(r: 0, g: 0, b: 0, a: 255);

	/// <summary>
	/// Gets the Blanched Almond color (r:255,g:235,b:205,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB BlanchedAlmond => new ColorRGB(r: 255, g: 235, b: 205, a: 255);

	/// <summary>
	/// Gets the Blue color (r:0,g:0,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Blue => new ColorRGB(r: 0, g: 0, b: 255, a: 255);

	/// <summary>
	/// Gets the Blue Violet color (r:138,g:43,b:226,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB BlueViolet => new ColorRGB(r: 138, g: 43, b: 226, a: 255);

	/// <summary>
	/// Gets the Brown color (r:165,g:42,b:42,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Brown => new ColorRGB(r: 165, g: 42, b: 42, a: 255);

	/// <summary>
	/// Gets the Burly Wood color (r:222,g:184,b:135,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB BurlyWood => new ColorRGB(r: 222, g: 184, b: 135, a: 255);

	/// <summary>
	/// Gets the Cadet Blue color (r:95,g:158,b:160,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB CadetBlue => new ColorRGB(r: 95, g: 158, b: 160, a: 255);

	/// <summary>
	/// Gets the Chartreuse color (r:127,g:255,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Chartreuse => new ColorRGB(r: 127, g: 255, b: 0, a: 255);

	/// <summary>
	/// Gets the Chocolate color (r:210,g:105,b:30,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Chocolate => new ColorRGB(r: 210, g: 105, b: 30, a: 255);

	/// <summary>
	/// Gets the Coral color (r:255,g:127,b:80,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Coral => new ColorRGB(r: 255, g: 127, b: 80, a: 255);

	/// <summary>
	/// Gets the Cornflower Blue color (r:100,g:149,b:237,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB CornflowerBlue => new ColorRGB(r: 100, g: 149, b: 237, a: 255);

	/// <summary>
	/// Gets the Cornsilk color (r:255,g:248,b:220,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Cornsilk => new ColorRGB(r: 255, g: 248, b: 220, a: 255);

	/// <summary>
	/// Gets the Crimson color (r:220,g:20,b:60,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Crimson => new ColorRGB(r: 220, g: 20, b: 60, a: 255);

	/// <summary>
	/// Gets the Cyan color (r:0,g:255,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Cyan => new ColorRGB(r: 0, g: 255, b: 255, a: 255);

	/// <summary>
	/// Gets the Dark Blue color (r:0,g:0,b:139,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkBlue => new ColorRGB(r: 0, g: 0, b: 139, a: 255);

	/// <summary>
	/// Gets the Dark Cyan color (r:0,g:139,b:139,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkCyan => new ColorRGB(r: 0, g: 139, b: 139, a: 255);

	/// <summary>
	/// Gets the Dark Goldenrod color (r:184,g:134,b:11,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkGoldenrod => new ColorRGB(r: 184, g: 134, b: 11, a: 255);

	/// <summary>
	/// Gets the Dark Gray color (r:169,g:169,b:169,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkGray => new ColorRGB(r: 169, g: 169, b: 169, a: 255);

	/// <summary>
	/// Gets the Dark Green color (r:0,g:100,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkGreen => new ColorRGB(r: 0, g: 100, b: 0, a: 255);

	/// <summary>
	/// Gets the Dark Khaki color (r:189,g:183,b:107,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkKhaki => new ColorRGB(r: 189, g: 183, b: 107, a: 255);

	/// <summary>
	/// Gets the Dark Magenta color (r:139,g:0,b:139,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkMagenta => new ColorRGB(r: 139, g: 0, b: 139, a: 255);

	/// <summary>
	/// Gets the Dark Olive Green color (r:85,g:107,b:47,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkOliveGreen => new ColorRGB(r: 85, g: 107, b: 47, a: 255);

	/// <summary>
	/// Gets the Dark Orange color (r:255,g:140,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkOrange => new ColorRGB(r: 255, g: 140, b: 0, a: 255);

	/// <summary>
	/// Gets the Dark Orchid color (r:153,g:50,b:204,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkOrchid => new ColorRGB(r: 153, g: 50, b: 204, a: 255);

	/// <summary>
	/// Gets the Dark Red color (r:139,g:0,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkRed => new ColorRGB(r: 139, g: 0, b: 0, a: 255);

	/// <summary>
	/// Gets the Dark Salmon color (r:233,g:150,b:122,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkSalmon => new ColorRGB(r: 233, g: 150, b: 122, a: 255);

	/// <summary>
	/// Gets the Dark Sea Green color (r:143,g:188,b:139,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkSeaGreen => new ColorRGB(r: 143, g: 188, b: 139, a: 255);

	/// <summary>
	/// Gets the Dark Slate Blue color (r:72,g:61,b:139,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkSlateBlue => new ColorRGB(r: 72, g: 61, b: 139, a: 255);

	/// <summary>
	/// Gets the Dark Slate Gray color (r:47,g:79,b:79,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkSlateGray => new ColorRGB(r: 47, g: 79, b: 79, a: 255);

	/// <summary>
	/// Gets the Dark Turquoise color (r:0,g:206,b:209,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkTurquoise => new ColorRGB(r: 0, g: 206, b: 209, a: 255);

	/// <summary>
	/// Gets the Dark Violet color (r:148,g:0,b:211,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DarkViolet => new ColorRGB(r: 148, g: 0, b: 211, a: 255);

	/// <summary>
	/// Gets the Deep Pink color (r:255,g:20,b:147,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DeepPink => new ColorRGB(r: 255, g: 20, b: 147, a: 255);

	/// <summary>
	/// Gets the Deep Sky Blue color (r:0,g:191,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DeepSkyBlue => new ColorRGB(r: 0, g: 191, b: 255, a: 255);

	/// <summary>
	/// Gets the Dim Gray color (r:105,g:105,b:105,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DimGray => new ColorRGB(r: 105, g: 105, b: 105, a: 255);

	/// <summary>
	/// Gets the Dodger Blue color (r:30,g:144,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB DodgerBlue => new ColorRGB(r: 30, g: 144, b: 255, a: 255);

	/// <summary>
	/// Gets the Firebrick color (r:178,g:34,b:34,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Firebrick => new ColorRGB(r: 178, g: 34, b: 34, a: 255);

	/// <summary>
	/// Gets the Floral White color (r:255,g:250,b:240,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB FloralWhite => new ColorRGB(r: 255, g: 250, b: 240, a: 255);

	/// <summary>
	/// Gets the Forest Green color (r:34,g:139,b:34,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB ForestGreen => new ColorRGB(r: 34, g: 139, b: 34, a: 255);

	/// <summary>
	/// Gets the Fuchsia color (r:255,g:0,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Fuchsia => new ColorRGB(r: 255, g: 0, b: 255, a: 255);

	/// <summary>
	/// Gets the Gainsboro color (r:220,g:220,b:220,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Gainsboro => new ColorRGB(r: 220, g: 220, b: 220, a: 255);

	/// <summary>
	/// Gets the Ghost White color (r:248,g:248,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB GhostWhite => new ColorRGB(r: 248, g: 248, b: 255, a: 255);

	/// <summary>
	/// Gets the Gold color (r:255,g:215,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Gold => new ColorRGB(r: 255, g: 215, b: 0, a: 255);

	/// <summary>
	/// Gets the Goldenrod color (r:218,g:165,b:32,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Goldenrod => new ColorRGB(r: 218, g: 165, b: 32, a: 255);

	/// <summary>
	/// Gets the Gray color (r:128,g:128,b:128,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Gray => new ColorRGB(r: 128, g: 128, b: 128, a: 255);

	/// <summary>
	/// Gets the Green color (r:0,g:128,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Green => new ColorRGB(r: 0, g: 128, b: 0, a: 255);

	/// <summary>
	/// Gets the Green Yellow color (r:173,g:255,b:47,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB GreenYellow => new ColorRGB(r: 173, g: 255, b: 47, a: 255);

	/// <summary>
	/// Gets the Honeydew color (r:240,g:255,b:240,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Honeydew => new ColorRGB(r: 240, g: 255, b: 240, a: 255);

	/// <summary>
	/// Gets the Hot Pink color (r:255,g:105,b:180,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB HotPink => new ColorRGB(r: 255, g: 105, b: 180, a: 255);

	/// <summary>
	/// Gets the Indian Red color (r:205,g:92,b:92,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB IndianRed => new ColorRGB(r: 205, g: 92, b: 92, a: 255);

	/// <summary>
	/// Gets the Indigo color (r:75,g:0,b:130,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Indigo => new ColorRGB(r: 75, g: 0, b: 130, a: 255);

	/// <summary>
	/// Gets the Ivory color (r:255,g:255,b:240,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Ivory => new ColorRGB(r: 255, g: 255, b: 240, a: 255);

	/// <summary>
	/// Gets the Khaki color (r:240,g:230,b:140,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Khaki => new ColorRGB(r: 240, g: 230, b: 140, a: 255);

	/// <summary>
	/// Gets the Lavender color (r:230,g:230,b:250,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Lavender => new ColorRGB(r: 230, g: 230, b: 250, a: 255);

	/// <summary>
	/// Gets the Lavender Blush color (r:255,g:240,b:245,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LavenderBlush => new ColorRGB(r: 255, g: 240, b: 245, a: 255);

	/// <summary>
	/// Gets the Lawn Green color (r:124,g:252,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LawnGreen => new ColorRGB(r: 124, g: 252, b: 0, a: 255);

	/// <summary>
	/// Gets the Lemon Chiffon color (r:255,g:250,b:205,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LemonChiffon => new ColorRGB(r: 255, g: 250, b: 205, a: 255);

	/// <summary>
	/// Gets the Light Blue color (r:173,g:216,b:230,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightBlue => new ColorRGB(r: 173, g: 216, b: 230, a: 255);

	/// <summary>
	/// Gets the Light Coral color (r:240,g:128,b:128,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightCoral => new ColorRGB(r: 240, g: 128, b: 128, a: 255);

	/// <summary>
	/// Gets the Light Cyan color (r:224,g:255,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightCyan => new ColorRGB(r: 224, g: 255, b: 255, a: 255);

	/// <summary>
	/// Gets the Light Goldenrod Yellow color (r:250,g:250,b:210,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightGoldenrodYellow => new ColorRGB(r: 250, g: 250, b: 210, a: 255);

	/// <summary>
	/// Gets the Light Gray color (r:211,g:211,b:211,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightGray => new ColorRGB(r: 211, g: 211, b: 211, a: 255);

	/// <summary>
	/// Gets the Light Green color (r:144,g:238,b:144,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightGreen => new ColorRGB(r: 144, g: 238, b: 144, a: 255);

	/// <summary>
	/// Gets the Light Pink color (r:255,g:182,b:193,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightPink => new ColorRGB(r: 255, g: 182, b: 193, a: 255);

	/// <summary>
	/// Gets the Light Salmon color (r:255,g:160,b:122,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightSalmon => new ColorRGB(r: 255, g: 160, b: 122, a: 255);

	/// <summary>
	/// Gets the Light Sea Green color (r:32,g:178,b:170,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightSeaGreen => new ColorRGB(r: 32, g: 178, b: 170, a: 255);

	/// <summary>
	/// Gets the Light Sky Blue color (r:135,g:206,b:250,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightSkyBlue => new ColorRGB(r: 135, g: 206, b: 250, a: 255);

	/// <summary>
	/// Gets the Light Slate Gray color (r:119,g:136,b:153,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightSlateGray => new ColorRGB(r: 119, g: 136, b: 153, a: 255);

	/// <summary>
	/// Gets the Light Steel Blue color (r:176,g:196,b:222,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightSteelBlue => new ColorRGB(r: 176, g: 196, b: 222, a: 255);

	/// <summary>
	/// Gets the Light Yellow color (r:255,g:255,b:224,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LightYellow => new ColorRGB(r: 255, g: 255, b: 224, a: 255);

	/// <summary>
	/// Gets the Lime color (r:0,g:255,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Lime => new ColorRGB(r: 0, g: 255, b: 0, a: 255);

	/// <summary>
	/// Gets the Lime Green color (r:50,g:205,b:50,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB LimeGreen => new ColorRGB(r: 50, g: 205, b: 50, a: 255);

	/// <summary>
	/// Gets the Linen color (r:250,g:240,b:230,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Linen => new ColorRGB(r: 250, g: 240, b: 230, a: 255);

	/// <summary>
	/// Gets the Magenta color (r:255,g:0,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Magenta => new ColorRGB(r: 255, g: 0, b: 255, a: 255);

	/// <summary>
	/// Gets the Maroon color (r:128,g:0,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Maroon => new ColorRGB(r: 128, g: 0, b: 0, a: 255);

	/// <summary>
	/// Gets the Medium Aquamarine color (r:102,g:205,b:170,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumAquamarine => new ColorRGB(r: 102, g: 205, b: 170, a: 255);

	/// <summary>
	/// Gets the Medium Blue color (r:0,g:0,b:205,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumBlue => new ColorRGB(r: 0, g: 0, b: 205, a: 255);

	/// <summary>
	/// Gets the Medium Orchid color (r:186,g:85,b:211,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumOrchid => new ColorRGB(r: 186, g: 85, b: 211, a: 255);

	/// <summary>
	/// Gets the Medium Purple color (r:147,g:112,b:219,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumPurple => new ColorRGB(r: 147, g: 112, b: 219, a: 255);

	/// <summary>
	/// Gets the Medium Sea Green color (r:60,g:179,b:113,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumSeaGreen => new ColorRGB(r: 60, g: 179, b: 113, a: 255);

	/// <summary>
	/// Gets the Medium Slate Blue color (r:123,g:104,b:238,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumSlateBlue => new ColorRGB(r: 123, g: 104, b: 238, a: 255);

	/// <summary>
	/// Gets the Medium Spring Green color (r:0,g:250,b:154,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumSpringGreen => new ColorRGB(r: 0, g: 250, b: 154, a: 255);

	/// <summary>
	/// Gets the Medium Turquoise color (r:72,g:209,b:204,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumTurquoise => new ColorRGB(r: 72, g: 209, b: 204, a: 255);

	/// <summary>
	/// Gets the Medium Violet Red color (r:199,g:21,b:133,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MediumVioletRed => new ColorRGB(r: 199, g: 21, b: 133, a: 255);

	/// <summary>
	/// Gets the Midnight Blue color (r:25,g:25,b:112,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MidnightBlue => new ColorRGB(r: 25, g: 25, b: 112, a: 255);

	/// <summary>
	/// Gets the Mint Cream color (r:245,g:255,b:250,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MintCream => new ColorRGB(r: 245, g: 255, b: 250, a: 255);

	/// <summary>
	/// Gets the Misty Rose color (r:255,g:228,b:225,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB MistyRose => new ColorRGB(r: 255, g: 228, b: 225, a: 255);

	/// <summary>
	/// Gets the Moccasin color (r:255,g:228,b:181,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Moccasin => new ColorRGB(r: 255, g: 228, b: 181, a: 255);

	/// <summary>
	/// Gets the Navajo White color (r:255,g:222,b:173,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB NavajoWhite => new ColorRGB(r: 255, g: 222, b: 173, a: 255);

	/// <summary>
	/// Gets the Navy color (r:0,g:0,b:128,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Navy => new ColorRGB(r: 0, g: 0, b: 128, a: 255);

	/// <summary>
	/// Gets the Old Lace color (r:253,g:245,b:230,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB OldLace => new ColorRGB(r: 253, g: 245, b: 230, a: 255);

	/// <summary>
	/// Gets the Olive color (r:128,g:128,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Olive => new ColorRGB(r: 128, g: 128, b: 0, a: 255);

	/// <summary>
	/// Gets the Olive Drab color (r:107,g:142,b:35,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB OliveDrab => new ColorRGB(r: 107, g: 142, b: 35, a: 255);

	/// <summary>
	/// Gets the Orange color (r:255,g:165,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Orange => new ColorRGB(r: 255, g: 165, b: 0, a: 255);

	/// <summary>
	/// Gets the Orange Red color (r:255,g:69,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB OrangeRed => new ColorRGB(r: 255, g: 69, b: 0, a: 255);

	/// <summary>
	/// Gets the Orchid color (r:218,g:112,b:214,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Orchid => new ColorRGB(r: 218, g: 112, b: 214, a: 255);

	/// <summary>
	/// Gets the Pale Goldenrod color (r:238,g:232,b:170,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB PaleGoldenrod => new ColorRGB(r: 238, g: 232, b: 170, a: 255);

	/// <summary>
	/// Gets the Pale Green color (r:152,g:251,b:152,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB PaleGreen => new ColorRGB(r: 152, g: 251, b: 152, a: 255);

	/// <summary>
	/// Gets the Pale Turquoise color (r:175,g:238,b:238,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB PaleTurquoise => new ColorRGB(r: 175, g: 238, b: 238, a: 255);

	/// <summary>
	/// Gets the Pale Violet Red color (r:219,g:112,b:147,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB PaleVioletRed => new ColorRGB(r: 219, g: 112, b: 147, a: 255);

	/// <summary>
	/// Gets the Papaya Whip color (r:255,g:239,b:213,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB PapayaWhip => new ColorRGB(r: 255, g: 239, b: 213, a: 255);

	/// <summary>
	/// Gets the Peach Puff color (r:255,g:218,b:185,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB PeachPuff => new ColorRGB(r: 255, g: 218, b: 185, a: 255);

	/// <summary>
	/// Gets the Peru color (r:205,g:133,b:63,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Peru => new ColorRGB(r: 205, g: 133, b: 63, a: 255);

	/// <summary>
	/// Gets the Pink color (r:255,g:192,b:203,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Pink => new ColorRGB(r: 255, g: 192, b: 203, a: 255);

	/// <summary>
	/// Gets the Plum color (r:221,g:160,b:221,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Plum => new ColorRGB(r: 221, g: 160, b: 221, a: 255);

	/// <summary>
	/// Gets the Powder Blue color (r:176,g:224,b:230,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB PowderBlue => new ColorRGB(r: 176, g: 224, b: 230, a: 255);

	/// <summary>
	///  Gets the Purple color (r:128,g:0,b:128,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Purple => new ColorRGB(r: 128, g: 0, b: 128, a: 255);

	/// <summary>
	/// Gets the Red color (r:255,g:0,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Red => new ColorRGB(r: 255, g: 0, b: 0, a: 255);

	/// <summary>
	/// Gets the Rosy Brown color (r:188,g:143,b:143,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB RosyBrown => new ColorRGB(r: 188, g: 143, b: 143, a: 255);

	/// <summary>
	/// Gets the Royal Blue color (r:65,g:105,b:225,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB RoyalBlue => new ColorRGB(r: 65, g: 105, b: 225, a: 255);

	/// <summary>
	/// Gets the Saddle Brown color (r:139,g:69,b:19,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SaddleBrown => new ColorRGB(r: 139, g: 69, b: 19, a: 255);

	/// <summary>
	/// Gets the Salmon color (r:250,g:128,b:114,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Salmon => new ColorRGB(r: 250, g: 128, b: 114, a: 255);

	/// <summary>
	/// Gets the Sandy Brown color (r:244,g:164,b:96,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SandyBrown => new ColorRGB(r: 244, g: 164, b: 96, a: 255);

	/// <summary>
	/// Gets the Sea Green color (r:46,g:139,b:87,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SeaGreen => new ColorRGB(r: 46, g: 139, b: 87, a: 255);

	/// <summary>
	/// Gets the Sea Shell color (r:255,g:245,b:238,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SeaShell => new ColorRGB(r: 255, g: 245, b: 238, a: 255);

	/// <summary>
	/// Gets the Sienna color (r:160,g:82,b:45,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Sienna => new ColorRGB(r: 160, g: 82, b: 45, a: 255);

	/// <summary>
	/// Gets the Silver color (r:192,g:192,b:192,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Silver => new ColorRGB(r: 192, g: 192, b: 192, a: 255);

	/// <summary>
	/// Gets the Sky Blue color (r:135,g:206,b:235,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SkyBlue => new ColorRGB(r: 135, g: 206, b: 235, a: 255);

	/// <summary>
	/// Gets the Slate Blue color (r:106,g:90,b:205,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SlateBlue => new ColorRGB(r: 106, g: 90, b: 205, a: 255);

	/// <summary>
	/// Gets the Slate Gray color (r:112,g:128,b:144,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SlateGray => new ColorRGB(r: 112, g: 128, b: 144, a: 255);

	/// <summary>
	/// Gets the Snow color (r:255,g:250,b:250,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Snow => new ColorRGB(r: 255, g: 250, b: 250, a: 255);

	/// <summary>
	/// Gets the Spring Green color (r:0,g:255,b:127,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SpringGreen => new ColorRGB(r: 0, g: 255, b: 127, a: 255);

	/// <summary>
	/// Gets the Steel Blue color (r:70,g:130,b:180,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB SteelBlue => new ColorRGB(r: 70, g: 130, b: 180, a: 255);

	/// <summary>
	/// Gets the Tan color (r:210,g:180,b:140,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Tan => new ColorRGB(r: 210, g: 180, b: 140, a: 255);

	/// <summary>
	/// Gets the Teal color (r:0,g:128,b:128,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Teal => new ColorRGB(r: 0, g: 128, b: 128, a: 255);

	/// <summary>
	/// Gets the Thistle color (r:216,g:191,b:216,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Thistle => new ColorRGB(r: 216, g: 191, b: 216, a: 255);

	/// <summary>
	/// Gets the Tomato color (r:255,g:99,b:71,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Tomato => new ColorRGB(r: 255, g: 99, b: 71, a: 255);

	/// <summary>
	/// Gets the Turquoise color (r:64,g:224,b:208,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Turquoise => new ColorRGB(r: 64, g: 224, b: 208, a: 255);

	/// <summary>
	/// Gets the Violet color (r:238,g:130,b:238,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Violet => new ColorRGB(r: 238, g: 130, b: 238, a: 255);

	/// <summary>
	/// Gets the Wheat color (r:245,g:222,b:179,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Wheat => new ColorRGB(r: 245, g: 222, b: 179, a: 255);

	/// <summary>
	/// Gets the White color (r:255,g:255,b:255,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB White => new ColorRGB(r: 255, g: 255, b: 255, a: 255);

	/// <summary>
	/// Gets the White Smoke color (r:245,g:245,b:245,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB WhiteSmoke => new ColorRGB(r: 245, g: 245, b: 245, a: 255);

	/// <summary>
	/// Gets the Yellow color (r:255,g:255,b:0,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB Yellow => new ColorRGB(r: 255, g: 255, b: 0, a: 255);

	/// <summary>
	/// Gets the Yellow Green color (r:154,g:205,b:50,a:255).
	/// </summary>
	/// <value>The color.</value>
	public static ColorRGB YellowGreen => new ColorRGB(r: 154, g: 205, b: 50, a: 255);
}
