﻿namespace HQS.Utility;

/// <summary>
/// Represents a value of any type that can be in an assigned or unassigned state.
/// This can include reference types that use null as a valid assigned value.
/// </summary>
/// <typeparam name="T">The type of the value contained within the try operation result.</typeparam>
/// <remarks>
/// The 'try' method pattern is a method that returns a boolean success value and a result value through
/// an output parameter. This wrapper can be used to represent such a method when the normal pattern cannot
/// be applied. For example; when providing an asynchronous version of such a method you can use a task that
/// returns a <see cref="Try{T}"/> as the return type and no output parameter because output parameters
/// don't work with asynchronous methods.
/// </remarks>
public readonly struct Try<T> : IEquatable<Try<T>>
{
	/// <summary>
	/// The value.
	/// </summary>
	private readonly T value;

	/// <summary>
	/// Initializes a new instance of the <see cref="Try{T}"/> struct.
	/// </summary>
	/// <param name="value">The value.</param>
	public Try(T value)
	{
		this.value = value;
		this.HasValue = true;
	}

	/// <summary>
	/// Gets a value indicating whether this instance has a value assigned to it.
	/// </summary>
	/// <value>
	/// <c>true</c> if this instance has a value; otherwise, <c>false</c>.
	/// </value>
	public bool HasValue { get; }

	/// <summary>
	/// Gets the value if it has been assigned. This may only be accessed if there is a value.
	/// </summary>
	/// <value>
	/// The value.
	/// </value>
	public T Value
	{
		get
		{
			Ensure.That(this.HasValue, nameof(this.HasValue)).IsTrue();

			return this.value;
		}
	}

	public static implicit operator Try<T>(T value) => new Try<T>(value);

	public static explicit operator T(Try<T> value) => value.Value;

	public static bool operator ==(Try<T> lhs, Try<T> rhs) => lhs.Equals(rhs);

	public static bool operator !=(Try<T> lhs, Try<T> rhs) => !lhs.Equals(rhs);

	public bool Equals(Try<T> other, IEqualityComparer<T> comparer)
	{
		Ensure.That(comparer, nameof(comparer)).IsNotNull();

		return (this.HasValue && other.HasValue && comparer.Equals(this.value, other.value))
			|| (!this.HasValue && !other.HasValue);
	}

	public bool Equals(T other, IEqualityComparer<T> comparer)
	{
		Ensure.That(comparer, nameof(comparer)).IsNotNull();

		return this.HasValue && comparer.Equals(this.value, other);
	}

	/// <inheritdoc />
	public bool Equals(Try<T> other) =>
		(this.HasValue && other.HasValue && this.value.EqualsNullSafe(other.value)) || (!this.HasValue && !other.HasValue);

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.value.GetHashCodeNullSafe();

	/// <inheritdoc />
	public override string ToString() => this.HasValue ? this.value.ToStringNullSafe() : "No Value";

	public T ToValueOrDefault(T defaultValue = default) => this.HasValue ? this.Value : defaultValue;

	public T ToValueOrDefault(Func<T> getDefaultValue)
	{
		Ensure.That(getDefaultValue, nameof(getDefaultValue)).IsNotNull();

		return this.HasValue ? this.Value : getDefaultValue();
	}

	public bool ToTryMethod(out T value)
	{
		if (this.HasValue)
		{
			value = this.Value;
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}

	public Try<TResult> Select<TResult>(Func<T, TResult> selector)
	{
		Ensure.That(selector, nameof(selector)).IsNotNull();

		if (this.HasValue)
		{
			return Try.Value(selector(this.Value));
		}
		else
		{
			return Try.None<TResult>();
		}
	}

	public Try<TResult> Select<TResult>(Func<T, Try<TResult>> selector)
	{
		Ensure.That(selector, nameof(selector)).IsNotNull();

		if (this.HasValue)
		{
			return selector(this.Value);
		}
		else
		{
			return Try.None<TResult>();
		}
	}
}
