﻿namespace HQS.Utility;

/// <summary>
/// Represents a previous-next pair of values from a sequence.
/// </summary>
/// <typeparam name="T">The type of the values.</typeparam>
[SuppressMessage("StyleCop", "SA1313", Justification = "Record syntax.")]
public readonly record struct SequentialPair<T>(T Previous, T Next);
