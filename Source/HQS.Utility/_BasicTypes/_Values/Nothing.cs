﻿namespace HQS.Utility;

/// <summary>
/// An empty struct to be used with generic classes that don't offer a non-generic version.
/// For example, task completion source.
/// </summary>
public readonly struct Nothing : IEquatable<Nothing>
{
	public static Nothing Default => default;

	public static bool operator ==(Nothing lhs, Nothing rhs) => lhs.Equals(rhs);

	public static bool operator !=(Nothing lhs, Nothing rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(Nothing other) => true;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => 0;
}
