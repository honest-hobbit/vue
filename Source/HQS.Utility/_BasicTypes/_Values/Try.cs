﻿namespace HQS.Utility;

public static class Try
{
	public static IEqualityComparer<Try<T>> StructComparer<T>()
		where T : struct, IEquatable<T> => Comparer<T>.Instance;

	/// <summary>
	/// Creates a failed try operation result containing no value.
	/// </summary>
	/// <typeparam name="T">The type of the value contained within the try operation result.</typeparam>
	/// <returns>The failed try operation result.</returns>
	public static Try<T> None<T>() => default;

	/// <summary>
	/// Creates a successful try operation result containing the specified value.
	/// </summary>
	/// <typeparam name="T">The type of the value contained within the try operation result.</typeparam>
	/// <param name="value">The value.</param>
	/// <returns>
	/// The successful try operation result.
	/// </returns>
	public static Try<T> Value<T>(T value) => new Try<T>(value);

	public static Try<T> ValueOrNone<T>(T value)
		where T : class => value != null ? Value(value) : None<T>();

	public static Try<T> ValueOrNone<T>(T? value)
		where T : struct => value.HasValue ? Value(value.Value) : None<T>();

	public static Try<T> FromTryMethod<T>(bool result, T value) => result ? new Try<T>(value) : None<T>();

	public static bool EqualsStruct<T>(this Try<T> value, Try<T> other)
		where T : struct, IEquatable<T> =>
		(value.HasValue && other.HasValue && value.Value.Equals(other.Value)) || (!value.HasValue && !other.HasValue);

	public static bool EqualsStruct<T>(this Try<T> value, T other)
		where T : struct, IEquatable<T> => value.HasValue && value.Value.Equals(other);

	private class Comparer<T> : IEqualityComparer<Try<T>>
		where T : struct, IEquatable<T>
	{
		private Comparer()
		{
		}

		public static Comparer<T> Instance { get; } = new Comparer<T>();

		/// <inheritdoc />
		public bool Equals(Try<T> x, Try<T> y) => x.EqualsStruct(y);

		/// <inheritdoc />
		public int GetHashCode(Try<T> obj) => obj.GetHashCode();
	}
}
