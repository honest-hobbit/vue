﻿namespace HQS.Utility;

public class DualConverter<T1, T2> : IDualConverter<T1, T2>
{
	private readonly Func<T1, T2> toResult;

	private readonly Func<T2, T1> toSource;

	public DualConverter(Func<T1, T2> toResult, Func<T2, T1> toSource)
	{
		Ensure.That(toResult, nameof(toResult)).IsNotNull();
		Ensure.That(toSource, nameof(toSource)).IsNotNull();

		this.toResult = toResult;
		this.toSource = toSource;
	}

	/// <inheritdoc />
	public T2 Convert(T1 value) => this.toResult(value);

	/// <inheritdoc />
	public T1 Convert(T2 value) => this.toSource(value);
}
