﻿namespace HQS.Utility;

public static class ReadOnlyArray
{
	public static ReadOnlyArray<T> From<T>(T[] array) => new ReadOnlyArray<T>(array);

	public static ReadOnlyArray<T> AsReadOnlyArray<T>(this T[] array) => new ReadOnlyArray<T>(array);
}
