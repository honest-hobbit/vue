﻿namespace HQS.Utility;

// this type can be used in places where ReadOnlySpan<T> can't because ReadOnlySpan<T> is a Ref Struct
public readonly record struct ReadOnlyArray<T> : IReadOnlyList<T>
{
	private readonly T[] array;

	public ReadOnlyArray(T[] array)
	{
		this.array = array;
	}

	public bool IsDefault => this.array == null;

	public int Length => this.array.Length;

	public ReadOnlySpan<T> AsSpan => new ReadOnlySpan<T>(this.array, 0, this.array.Length);

	/// <inheritdoc />
	int IReadOnlyCollection<T>.Count => this.array.Length;

	/// <inheritdoc />
	public T this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.array[index];
	}

	/// <inheritdoc />
	public override string ToString() => this.array.ToString();

	public void CopyTo(T[] array, int index = 0) => this.array.CopyTo(array, index);

	public void CopyTo(T[] array, long index) => this.array.CopyTo(array, index);

	public ReadOnlySpan<T> Slice(int index, int length) => new ReadOnlySpan<T>(this.array, index, length);

	public ArraySegment<T>.Enumerator GetEnumerator() => this.array.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
