﻿namespace HQS.Utility;

/// <summary>
/// Provides extension methods for one dimensional arrays.
/// </summary>
public static class ArrayExtensions
{
	public static bool HasCapacity(this Array array)
	{
		Ensure.That(array, nameof(array)).IsNotNull();

		return array.LongLength >= 1;
	}

	/// <summary>
	/// Gets the type-safe enumerator for an array.
	/// </summary>
	/// <typeparam name="T">The type of values to enumerate.</typeparam>
	/// <param name="array">The source array.</param>
	/// <returns>The type-safe enumerator for the array.</returns>
	public static ArraySegment<T>.Enumerator GetEnumeratorTypeSafe<T>(this T[] array) =>
		new ArraySegment<T>(array).GetEnumerator();

	public static bool ArrayEquals<T>(this T[] lhs, T[] rhs) => lhs.ArrayEquals(rhs, EqualityComparer<T>.Default);

	public static bool ArrayEquals<T>(this T[] lhs, T[] rhs, IEqualityComparer<T> comparer)
	{
		Ensure.That(lhs, nameof(lhs)).IsNotNull();
		Ensure.That(rhs, nameof(rhs)).IsNotNull();
		Ensure.That(comparer, nameof(comparer)).IsNotNull();

		if (lhs == rhs)
		{
			return true;
		}

		int length = lhs.Length;
		if (length != rhs.Length)
		{
			return false;
		}

		for (int i = 0; i < length; i++)
		{
			if (!comparer.Equals(lhs[i], rhs[i]))
			{
				return false;
			}
		}

		return true;
	}

	public static void SetAllTo<T>(this T[] array, T value)
	{
		Ensure.That(array, nameof(array)).IsNotNull();

		int length = array.Length;
		for (int i = 0; i < length; i++)
		{
			array[i] = value;
		}
	}

	public static void SetTo<T>(this T[] array, IEnumerable<T> values)
	{
		Ensure.That(array, nameof(array)).IsNotNull();
		Ensure.That(values, nameof(values)).IsNotNull();

		int length = array.Length;
		if (length == 0)
		{
			return;
		}

		int i = 0;
		foreach (var value in values)
		{
			array[i] = value;
			i++;
			if (i == length)
			{
				return;
			}
		}
	}

	public static T[] Copy<T>(this T[] array)
	{
		Ensure.That(array, nameof(array)).IsNotNull();

		T[] result = new T[array.LongLength];
		array.CopyTo(result, 0);
		return result;
	}

	public static void SwapIndices<T>(this T[] array, int index1, int index2)
	{
		Ensure.That(array, nameof(array)).IsNotNull();
		Ensure.That(index1, nameof(index1)).IsInRange(0, array.Length - 1);
		Ensure.That(index2, nameof(index2)).IsInRange(0, array.Length - 1);

		var temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}

	public static void Shuffle<T>(this T[] array)
	{
		Ensure.That(array, nameof(array)).IsNotNull();

		Shuffle(array, 0, array.Length, ThreadLocalRandom.Instance);
	}

	public static void Shuffle<T>(this T[] array, Random random)
	{
		Ensure.That(array, nameof(array)).IsNotNull();

		Shuffle(array, 0, array.Length, random);
	}

	public static void Shuffle<T>(this T[] array, int count) => Shuffle(array, 0, count, ThreadLocalRandom.Instance);

	public static void Shuffle<T>(this T[] array, int count, Random random) => Shuffle(array, 0, count, random);

	public static void Shuffle<T>(this T[] array, int index, int count, Random random)
	{
		Ensure.That(array, nameof(array)).IsNotNull();
		Ensure.That(index, nameof(index)).IsInRange(0, array.Length - 1);
		Ensure.That(count, nameof(count)).IsInRange(0, array.Length - index);
		Ensure.That(random, nameof(random)).IsNotNull();

		int n = count;
		while (n > 1)
		{
			n--;
			int k = index + random.Next(n + 1);
			int i = index + n;
			T value = array[k];
			array[k] = array[i];
			array[i] = value;
		}
	}
}
