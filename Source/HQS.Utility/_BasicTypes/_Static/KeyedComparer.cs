﻿namespace HQS.Utility;

public static class KeyedComparer
{
	public static EqualityComparer<TValue> Equality<TKey, TValue>()
		where TKey : IEquatable<TKey>
		where TValue : IKeyed<TKey> => KeyedEqualityComparer<TKey, TValue>.Instance;

	public static Comparer<TValue> Comparison<TKey, TValue>()
		where TKey : IComparable<TKey>
		where TValue : IKeyed<TKey> => KeyedComparison<TKey, TValue>.Instance;

	private class KeyedEqualityComparer<TKey, TValue> : EqualityComparer<TValue>
		where TKey : IEquatable<TKey>
		where TValue : IKeyed<TKey>
	{
		private KeyedEqualityComparer()
		{
		}

		public static KeyedEqualityComparer<TKey, TValue> Instance { get; } = new KeyedEqualityComparer<TKey, TValue>();

		/// <inheritdoc />
		public override bool Equals(TValue x, TValue y)
		{
			if (x == null)
			{
				return y == null;
			}

			if (y == null)
			{
				return false;
			}

			var xKey = x.Key;
			var yKey = y.Key;
			if (xKey == null)
			{
				return yKey == null;
			}

			if (yKey == null)
			{
				return false;
			}

			return xKey.Equals(yKey);
		}

		/// <inheritdoc />
		public override int GetHashCode(TValue obj) => obj?.Key?.GetHashCode() ?? 0;
	}

	private class KeyedComparison<TKey, TValue> : Comparer<TValue>
		where TKey : IComparable<TKey>
		where TValue : IKeyed<TKey>
	{
		private KeyedComparison()
		{
		}

		public static KeyedComparison<TKey, TValue> Instance { get; } = new KeyedComparison<TKey, TValue>();

		/// <inheritdoc />
		public override int Compare(TValue x, TValue y)
		{
			if (x == null)
			{
				return (y == null) ? 0 : -1;
			}

			if (y == null)
			{
				return 1;
			}

			var xKey = x.Key;
			var yKey = y.Key;
			if (xKey == null)
			{
				return (yKey == null) ? 0 : -1;
			}

			if (yKey == null)
			{
				return 1;
			}

			return xKey.CompareTo(yKey);
		}
	}
}
