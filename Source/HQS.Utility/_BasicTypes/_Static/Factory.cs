﻿namespace HQS.Utility;

public static class Factory
{
	public static T CreateNew<T>()
		where T : new() => new T();

	/// <summary>
	/// Creates a new one dimensional array filled in with the specified initial value.
	/// </summary>
	/// <typeparam name="T">The type of the values in the array.</typeparam>
	/// <param name="length">The length of array to create.</param>
	/// <param name="value">The initial value to copy to every slot of the array.</param>
	/// <returns>The new array with values filled in.</returns>
	/// <remarks>
	/// If <typeparamref name="T"/> is a reference type then each slot of the array will contain a reference
	/// back to the same object (or null if null was passed in). If you want a unique instance of an object
	/// for every slot of the array use one of the other overloads.
	/// </remarks>
	public static T[] CreateArray<T>(int length, T value)
	{
		Ensure.That(length, nameof(length)).IsGte(0);

		T[] result = new T[length];
		for (int index = 0; index < length; index++)
		{
			result[index] = value;
		}

		return result;
	}

	/// <summary>
	/// Creates a new one dimensional array filled in with values returned by the specified delegate.
	/// </summary>
	/// <typeparam name="T">The type of the values in the array.</typeparam>
	/// <param name="length">The length of array to create.</param>
	/// <param name="factory">The delegate used to return the initial values.</param>
	/// <returns>The new array with values filled in.</returns>
	public static T[] CreateArray<T>(int length, Func<T> factory)
	{
		Ensure.That(length, nameof(length)).IsGte(0);
		Ensure.That(factory, nameof(factory)).IsNotNull();

		T[] result = new T[length];
		for (int index = 0; index < length; index++)
		{
			result[index] = factory();
		}

		return result;
	}

	/// <summary>
	/// Creates a new one dimensional array filled in with values returned by the specified delegate.
	/// </summary>
	/// <typeparam name="T">The type of the values in the array.</typeparam>
	/// <param name="length">The length of array to create.</param>
	/// <param name="factory">The delegate used to return the initial values.</param>
	/// <returns>The new array with values filled in.</returns>
	/// <remarks>The delegate will be passed in the index of each value as they are created.</remarks>
	public static T[] CreateArray<T>(int length, Func<int, T> factory)
	{
		Ensure.That(length, nameof(length)).IsGte(0);
		Ensure.That(factory, nameof(factory)).IsNotNull();

		T[] result = new T[length];
		for (int index = 0; index < length; index++)
		{
			result[index] = factory(index);
		}

		return result;
	}

	public static IEnumerable<T> CreateMany<T>(int length, T value)
	{
		Ensure.That(length, nameof(length)).IsGte(0);

		return YieldCreate();
		IEnumerable<T> YieldCreate()
		{
			for (int index = 0; index < length; index++)
			{
				yield return value;
			}
		}
	}

	public static IEnumerable<T> CreateMany<T>(int length, Func<T> factory)
	{
		Ensure.That(length, nameof(length)).IsGte(0);
		Ensure.That(factory, nameof(factory)).IsNotNull();

		return YieldCreate();
		IEnumerable<T> YieldCreate()
		{
			for (int index = 0; index < length; index++)
			{
				yield return factory();
			}
		}
	}

	public static IEnumerable<T> CreateMany<T>(int length, Func<int, T> factory)
	{
		Ensure.That(length, nameof(length)).IsGte(0);
		Ensure.That(factory, nameof(factory)).IsNotNull();

		return YieldCreate();
		IEnumerable<T> YieldCreate()
		{
			for (int index = 0; index < length; index++)
			{
				yield return factory(index);
			}
		}
	}
}
