﻿namespace HQS.Utility;

public static class Capacity
{
	public const int DefaultForCollections = 4;

	public const int Unbounded = -1;
}
