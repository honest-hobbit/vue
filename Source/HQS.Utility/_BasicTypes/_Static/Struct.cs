﻿namespace HQS.Utility;

public static class Struct
{
	public static bool Equals<TStruct>(TStruct value, object other)
		where TStruct : struct, IEquatable<TStruct> => (other is TStruct otherValue) ? value.Equals(otherValue) : false;
}
