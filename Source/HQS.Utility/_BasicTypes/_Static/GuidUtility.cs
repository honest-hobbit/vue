﻿namespace HQS.Utility;

// TODO a lot of this can probably be improved with modern .NET and Spans
public static class GuidUtility
{
	public static Guid FromNullableArray(byte[] array) => array == null ? Guid.Empty : new Guid(array);

	public static byte[] ToNullableByteArray(this Guid value) => value != Guid.Empty ? value.ToByteArray() : null;

	public static void ToNullableByteArray(this Guid value, ref byte[] array) => array = value.ToNullableByteArray(array);

	public static byte[] ToNullableByteArray(this Guid value, byte[] array)
	{
		if (array == null)
		{
			return value.ToNullableByteArray();
		}
		else
		{
			Ensure.That(array.Length, $"{nameof(array)}.Length").Is(16);

			value.CopyTo(array);
			return array;
		}
	}

	public static Guid FromNullableString(string value) => !value.IsNullOrWhiteSpace() ? new Guid(value) : Guid.Empty;

	public static string ToNullableString(this Guid value) => value != Guid.Empty ? value.ToString() : null;

	public static void CopyTo(this Guid value, byte[] array, int index = 0)
	{
		Ensure.That(array, nameof(array)).IsNotNull();
		Ensure.That(array.Length, $"{nameof(array)}.Length").IsGte(index + 16);

		var bytes = new Span<byte>(array, index, 16);
		bool success = value.TryWriteBytes(bytes);
		Debug.Assert(success);
	}

	public static Guid FromBytes(
		byte byte0,
		byte byte1,
		byte byte2,
		byte byte3,
		byte byte4,
		byte byte5,
		byte byte6,
		byte byte7,
		byte byte8,
		byte byte9,
		byte byte10,
		byte byte11,
		byte byte12,
		byte byte13,
		byte byte14,
		byte byte15)
	{
		int a = (byte3 << 24) | (byte2 << 16) | (byte1 << 8) | byte0;
		short b = (short)((byte5 << 8) | byte4);
		short c = (short)((byte7 << 8) | byte6);
		return new Guid(a, b, c, byte8, byte9, byte10, byte11, byte12, byte13, byte14, byte15);
	}
}
