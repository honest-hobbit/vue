﻿namespace HQS.Utility;

public static class Length
{
	public static class OfBool
	{
		/// <summary>
		/// Gets the number of bits needed to represent a bool as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		/// <remarks>
		/// Although a bool only requires 1 bit to represent it, it will take at least a byte
		/// of memory to represent unless further optimization efforts are taken.
		/// </remarks>
		public const int InBits = 8;

		/// <summary>
		/// Gets the number of bytes needed to represent a bool as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		/// <remarks>
		/// Although a bool only requires 1 bit to represent it, it will take at least a byte
		/// of memory to represent unless further optimization efforts are taken.
		/// </remarks>
		public const int InBytes = 1;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a bool as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 5;
	}

	public static class OfChar
	{
		/// <summary>
		/// Gets the number of bits needed to represent a char as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 16;

		/// <summary>
		/// Gets the number of bytes needed to represent a char as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 2;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a char as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 1;
	}

	public static class OfFloat
	{
		/// <summary>
		/// Gets the number of bits needed to represent a float as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 32;

		/// <summary>
		/// Gets the number of bytes needed to represent a float as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 4;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a float as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 12;
	}

	public static class OfDouble
	{
		/// <summary>
		/// Gets the number of bits needed to represent a double as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 64;

		/// <summary>
		/// Gets the number of bytes needed to represent a double as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 8;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a double as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 21;
	}

	public static class OfDecimal
	{
		/// <summary>
		/// Gets the number of bits needed to represent a byte as a decimal primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 128;

		/// <summary>
		/// Gets the number of bytes needed to represent a byte as a decimal primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 16;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a decimal as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 30;
	}

	public static class OfByte
	{
		/// <summary>
		/// Gets the number of bits needed to represent a byte as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 8;

		/// <summary>
		/// Gets the number of bytes needed to represent a byte as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 1;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a byte as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 3;
	}

	public static class OfSByte
	{
		/// <summary>
		/// Gets the number of bits needed to represent a sbyte as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 8;

		/// <summary>
		/// Gets the number of bytes needed to represent a sbyte as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 1;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a sbyte as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 4;
	}

	public static class OfShort
	{
		/// <summary>
		/// Gets the number of bits needed to represent a short as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 16;

		/// <summary>
		/// Gets the number of bytes needed to represent a short as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 2;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a short as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 6;
	}

	public static class OfUShort
	{
		/// <summary>
		/// Gets the number of bits needed to represent a ushort as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 16;

		/// <summary>
		/// Gets the number of bytes needed to represent a ushort as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 2;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a ushort as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 5;
	}

	public static class OfInt
	{
		/// <summary>
		/// Gets the number of bits needed to represent a int as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 32;

		/// <summary>
		/// Gets the number of bytes needed to represent a int as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 4;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a int as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 11;
	}

	public static class OfUInt
	{
		/// <summary>
		/// Gets the number of bits needed to represent a uint as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 32;

		/// <summary>
		/// Gets the number of bytes needed to represent a uint as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 4;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a uint as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 10;
	}

	public static class OfLong
	{
		/// <summary>
		/// Gets the number of bits needed to represent a long as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 64;

		/// <summary>
		/// Gets the number of bytes needed to represent a long as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 8;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a long as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 20;
	}

	public static class OfULong
	{
		/// <summary>
		/// Gets the number of bits needed to represent a ulong as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bits.
		/// </value>
		public const int InBits = 64;

		/// <summary>
		/// Gets the number of bytes needed to represent a ulong as a binary primitive type.
		/// </summary>
		/// <value>
		/// The number of bytes.
		/// </value>
		public const int InBytes = 8;

		/// <summary>
		/// Gets the maximum number of characters needed to represent a ulong as a string.
		/// </summary>
		/// <value>
		/// The maximum number of chars.
		/// </value>
		/// <remarks>
		/// <para>The number of characters needed to represent a particular value is often less than this size.</para>
		/// <para>The string is in cultural invariant English with no commas delimiting size.</para>
		/// </remarks>
		public const int InChars = 20;
	}

	public static class OfLongestPrimitive
	{
		public const int InBits = OfDecimal.InBits;

		public const int InBytes = OfDecimal.InBytes;

		public const int InChars = OfDecimal.InChars;
	}
}
