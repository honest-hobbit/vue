﻿namespace HQS.Utility;

public static class StringFactory
{
	private const string Characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	public static string CreateRandom(int length) => CreateRandom(length, Characters, ThreadLocalRandom.Instance);

	public static string CreateRandom(int length, string characters) => CreateRandom(length, characters, ThreadLocalRandom.Instance);

	public static string CreateRandom(int length, Random random) => CreateRandom(length, Characters, random);

	public static string CreateRandom(int length, string characters, Random random)
	{
		Ensure.That(length, nameof(length)).IsGte(0);
		Ensure.That(characters, nameof(characters)).IsNotNullOrEmpty();
		Ensure.That(random, nameof(random)).IsNotNull();

		var result = new char[length];
		for (var i = 0; i < length; i++)
		{
			result[i] = characters[random.Next(0, characters.Length)];
		}

		return new string(result);
	}
}
