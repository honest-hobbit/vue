﻿namespace HQS.Utility;

public static class Bytes
{
	public static long PerKilobyte { get; } = 1024;

	public static long PerMetricKilobyte { get; } = 1000;

	public static long PerMegabyte { get; } = MathUtility.LongPower(1024, 2);

	public static long PerMetricMegabyte { get; } = MathUtility.LongPower(1000, 2);

	public static long PerGigabyte { get; } = MathUtility.LongPower(1024, 3);

	public static long PerMetricGigabyte { get; } = MathUtility.LongPower(1000, 3);

	public static long PerTerabyte { get; } = MathUtility.LongPower(1024, 4);

	public static long PerMetricTerabyte { get; } = MathUtility.LongPower(1000, 4);

	public static long PerPetabyte { get; } = MathUtility.LongPower(1024, 5);

	public static long PerMetricPetabyte { get; } = MathUtility.LongPower(1000, 5);
}
