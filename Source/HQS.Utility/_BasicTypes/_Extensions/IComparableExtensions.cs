﻿namespace HQS.Utility;

/// <summary>
/// Provides extension methods for types that implement the <see cref="IComparable{T}"/> interface.
/// </summary>
public static class IComparableExtensions
{
	/// <summary>
	/// Determines whether this value is less than the specified value.
	/// </summary>
	/// <typeparam name="T">The type of the comparable value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="other">The other value.</param>
	/// <returns>True if this value is less than the other value; otherwise false.</returns>
	public static bool IsLessThan<T>(this T value, T other)
		where T : IComparable<T> => value?.CompareTo(other) < 0;

	/// <summary>
	/// Determines whether this value is less than or equal to the specified value.
	/// </summary>
	/// <typeparam name="T">The type of the comparable value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="other">The other value.</param>
	/// <returns>True if this value is less than or equal to the other value; otherwise false.</returns>
	public static bool IsLessThanOrEqualTo<T>(this T value, T other)
		where T : IComparable<T> => value?.CompareTo(other) <= 0;

	/// <summary>
	/// Determines whether this value is greater than the specified value.
	/// </summary>
	/// <typeparam name="T">The type of the comparable value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="other">The other value.</param>
	/// <returns>True if this value is greater than the other value; otherwise false.</returns>
	public static bool IsGreaterThan<T>(this T value, T other)
		where T : IComparable<T> => value?.CompareTo(other) > 0;

	/// <summary>
	/// Determines whether this value is greater than or equal to than the specified value.
	/// </summary>
	/// <typeparam name="T">The type of the comparable value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="other">The other value.</param>
	/// <returns>True if this value is greater than or equal to the other value; otherwise false.</returns>
	public static bool IsGreaterThanOrEqualTo<T>(this T value, T other)
		where T : IComparable<T> => value?.CompareTo(other) >= 0;

	/// <summary>
	/// Determines whether this value is equal to the specified value.
	/// </summary>
	/// <typeparam name="T">The type of the comparable value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="other">The other value.</param>
	/// <returns>True if this value is equal to the other value; otherwise false.</returns>
	public static bool IsEqualTo<T>(this T value, T other)
		where T : IComparable<T> => value?.CompareTo(other) == 0;
}
