﻿namespace HQS.Utility;

/// <summary>
/// Provides extension methods for use with the <see cref="StringBuilder"/> class.
/// </summary>
public static class StringBuilderExtensions
{
	public static StringBuilder Append<T>(this StringBuilder builder, T value)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		if (value != null)
		{
			builder.Append(value.ToString());
		}

		return builder;
	}

	/// <summary>
	/// Clears all text from the builder.
	/// </summary>
	/// <param name="builder">The builder to clear.</param>
	public static void Clear(this StringBuilder builder)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		builder.Length = 0;
	}
}
