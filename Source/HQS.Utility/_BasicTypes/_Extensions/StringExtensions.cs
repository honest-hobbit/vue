﻿namespace HQS.Utility;

/// <summary>
/// Provides extension methods for use with <see cref="string"/> class.
/// </summary>
public static class StringExtensions
{
	/// <summary>
	/// Determines whether the string is empty. This is more efficient then comparing to <see cref="string.Empty"/>.
	/// </summary>
	/// <param name="value">The string.</param>
	/// <returns>True if the string is empty, false otherwise.</returns>
	public static bool IsEmpty(this string value)
	{
		Ensure.That(value, nameof(value)).IsNotNull();

		return value.Length == 0;
	}

	/// <summary>
	/// Determines whether the specified string is null or an empty string.
	/// </summary>
	/// <param name="value">The string.</param>
	/// <returns>True if the string is null or empty, false otherwise.</returns>
	public static bool IsNullOrEmpty(this string value) => string.IsNullOrEmpty(value);

	/// <summary>
	/// Determines whether a specified string is null, empty, or consists only of white-space characters.
	/// </summary>
	/// <param name="value">The string.</param>
	/// <returns>True if the string is null, empty, or white-space, false otherwise.</returns>
	public static bool IsNullOrWhiteSpace(this string value) =>
		string.IsNullOrEmpty(value) || value.All(character => char.IsWhiteSpace(character));
}
