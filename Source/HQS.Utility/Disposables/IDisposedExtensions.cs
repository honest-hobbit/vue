﻿namespace HQS.Utility.Disposables;

public static class IDisposedExtensions
{
	public static void ValidateNotDisposed(this IDisposed instance)
	{
		Debug.Assert(instance != null);

		if (instance.IsDisposed)
		{
			throw new ObjectDisposedException(instance.GetType().Name, "Disposed object can no longer be used.");
		}
	}
}
