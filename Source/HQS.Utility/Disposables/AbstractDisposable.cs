﻿namespace HQS.Utility.Disposables;

/// <summary>
/// Provides a base class implementation of <see cref="IVisiblyDisposable"/>. The disposal state of
/// this base class is thread safe. This class is designed to handle the disposal of managed types only.
/// </summary>
/// <threadsafety static="true" instance="true" />
public abstract class AbstractDisposable : IVisiblyDisposable
{
	private const int NotDisposed = 0;

	private const int Disposed = 1;

	/// <summary>
	/// A value indicating whether this instance is disposed of.
	/// </summary>
	private int isDisposed = NotDisposed;

	/// <inheritdoc />
	public bool IsDisposed => InterlockedValue.Read(ref this.isDisposed) == Disposed;

	/// <inheritdoc />
	public void Dispose()
	{
		if (this.TrySetDisposed())
		{
			GC.SuppressFinalize(this);
			this.ManagedDisposal();
		}
	}

	/// <summary>
	/// Releases managed resources.
	/// </summary>
	/// <remarks>
	/// Implementers do not need to ensure that this method is called only once. However, do not ever call
	/// this method directly from your code. Only ever call the the public <see cref="Dispose"/> method.
	/// Implementers should still call the base class's protected <see cref="ManagedDisposal"/> method at
	/// the end of their method if they are part of a class hierarchy.
	/// </remarks>
	protected abstract void ManagedDisposal();

	protected bool TryDispose()
	{
		if (this.TrySetDisposed())
		{
			this.ManagedDisposal();
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// Tries to set <see cref="IsDisposed"/> to true if it hasn't already been set.
	/// </summary>
	/// <returns>True if able to set <see cref="IsDisposed"/> to true; otherwise false.</returns>
	protected bool TrySetDisposed()
	{
		// if isDisposed is NotDisposed, set it to Disposed and return true
		// (CompareExchange returns original value, aka NotDisposed)
		// otherwise just return false (CompareExchange returns Disposed)
		return Interlocked.CompareExchange(ref this.isDisposed, Disposed, NotDisposed) == NotDisposed;
	}

	protected void ValidateNotDisposed()
	{
		if (this.IsDisposed)
		{
			throw new ObjectDisposedException(this.ToString(), "Disposed object can no longer be used.");
		}
	}
}
