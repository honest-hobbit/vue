﻿namespace HQS.Utility.Disposables;

/// <summary>
/// Provides a mechanism for releasing resources and also the ability to observe if the resources have been released yet.
/// </summary>
public interface IVisiblyDisposable : IDisposable, IDisposed
{
}
