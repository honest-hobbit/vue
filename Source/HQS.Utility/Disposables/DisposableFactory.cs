﻿namespace HQS.Utility.Disposables;

public static class DisposableFactory
{
	/// <summary>
	/// Gets the disposable that does nothing when disposed.
	/// </summary>
	/// <value>
	/// The disposable that does nothing when disposed.
	/// </value>
	/// <remarks>
	/// This always returns the same instance as it is completely immutable.
	/// </remarks>
	public static IDisposable Empty { get; } = new DoNothingDisposable();

	/// <summary>
	/// Creates a new visibly disposable instance that does nothing except update the <see cref="IDisposed.IsDisposed"/>
	/// property when disposed.
	/// </summary>
	/// <returns>The disposable that does nothing when disposed except become disposed.</returns>
	public static IVisiblyDisposable NewVisiblyDisposable() => new DoNothingVisiblyDisposable();

	public static IVisiblyDisposable Create(Action onDispose)
	{
		Debug.Assert(onDispose != null);

		return new ActionDisposable(onDispose);
	}

	public static IVisiblyDisposable Combine(IEnumerable<IDisposable> disposables)
	{
		Debug.Assert(disposables.AllAndSelfNotNull());

		return new AggregateDisposable(disposables.ToArrayExtended());
	}

	public static IVisiblyDisposable Combine(params IDisposable[] disposables)
	{
		Debug.Assert(disposables.AllAndSelfNotNull());

		return new AggregateDisposable(disposables.Copy());
	}

	/// <summary>
	/// Disposes the object if the object implements <see cref="IDisposable"/>.
	/// </summary>
	/// <typeparam name="T">The type of the value.</typeparam>
	/// <param name="value">The value to dispose if able.</param>
	/// <returns>True if the object was disposed; otherwise false.</returns>
	public static bool TryDispose<T>(T value)
	{
		if (value is IDisposable disposable)
		{
			disposable.Dispose();
			return true;
		}
		else
		{
			return false;
		}
	}

	#region Private Classes

	/// <summary>
	/// A disposable type that does nothing when disposed.
	/// </summary>
	private class DoNothingDisposable : IDisposable
	{
		/// <inheritdoc />
		public void Dispose()
		{
		}
	}

	/// <summary>
	/// A visibly disposable type that does nothing when disposed except update the <see cref="IDisposed.IsDisposed"/> property.
	/// </summary>
	private class DoNothingVisiblyDisposable : AbstractDisposable
	{
		/// <inheritdoc />
		protected override void ManagedDisposal()
		{
		}
	}

	private class ActionDisposable : AbstractDisposable
	{
		private readonly Action onDispose;

		public ActionDisposable(Action onDispose)
		{
			Debug.Assert(onDispose != null);

			this.onDispose = onDispose;
		}

		/// <inheritdoc />
		protected override void ManagedDisposal() => this.onDispose();
	}

	/// <summary>
	/// Represents one or more <see cref="IDisposable"/> objects that will be disposed of as a single group
	/// when this instance is disposed of.
	/// </summary>
	[DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
	private class AggregateDisposable : AbstractDisposable
	{
		/// <summary>
		/// The disposables to dispose of when this instance is disposed.
		/// </summary>
		private readonly IDisposable[] disposables;

		/// <summary>
		/// Initializes a new instance of the <see cref="AggregateDisposable"/> class.
		/// </summary>
		/// <param name="disposables">The disposables to dispose of when this instance is disposed.</param>
		public AggregateDisposable(IDisposable[] disposables)
		{
			Debug.Assert(disposables.AllAndSelfNotNull());

			this.disposables = disposables;
		}

		/// <inheritdoc />
		protected override void ManagedDisposal()
		{
			List<Exception> exceptions = null;

			for (int i = 0; i < this.disposables.Length; i++)
			{
				try
				{
					this.disposables[i].Dispose();
				}
				catch (Exception error)
				{
					// Dispose should not throw exceptions but just in case
					exceptions ??= new List<Exception>();
					exceptions.Add(error);
				}
			}

			if (exceptions != null)
			{
				var error = new AggregateException(exceptions);
				throw error;
			}
		}
	}

	#endregion
}
