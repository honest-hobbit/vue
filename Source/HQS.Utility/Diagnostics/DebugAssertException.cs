﻿namespace HQS.Utility.Diagnostics;

[Serializable]
public class DebugAssertException : Exception
{
	public DebugAssertException()
	{
	}

	public DebugAssertException(string message)
		: base(message)
	{
	}

	public DebugAssertException(string message, Exception innerException)
		: base(message, innerException)
	{
	}

	protected DebugAssertException(SerializationInfo info, StreamingContext context)
		: base(info, context)
	{
	}
}
