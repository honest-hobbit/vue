﻿namespace HQS.Utility.Diagnostics;

public class DebugAssertExceptionTraceListener : TraceListener
{
	public static void AddListener() => Trace.Listeners.Add(new DebugAssertExceptionTraceListener());

	public override void Write(string msg) => throw new DebugAssertException(msg);

	public override void WriteLine(string msg) => throw new DebugAssertException(msg);
}
