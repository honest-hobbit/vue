﻿namespace HQS.Utility.IO;

public static class PathUtility
{
	public static IReadOnlyList<char> InvalidFileNameChars { get; } = Path.GetInvalidFileNameChars().AsReadOnlyList();

	public static IReadOnlyList<char> InvalidPathChars { get; } = Path.GetInvalidFileNameChars().AsReadOnlyList();

	public static bool FileNameContainsInvalidChars(string path)
	{
		if (path == null)
		{
			return false;
		}

		return path.ContainsAny(InvalidFileNameChars);
	}

	public static bool PathContainsInvalidChars(string path)
	{
		if (path == null)
		{
			return false;
		}

		return path.ContainsAny(InvalidPathChars);
	}

	public static bool IsPathValid(string path)
	{
		if (path.IsNullOrWhiteSpace())
		{
			return false;
		}

		bool isValidUri = Uri.TryCreate(path, UriKind.Absolute, out var pathUri);
		return isValidUri && (pathUri?.IsLoopback ?? false);
	}

	public static void ValidateFileExtension(string fileExtension) =>
		ValidateFileExtension(fileExtension, nameof(fileExtension));

	public static void ValidateFileExtension(string fileExtension, string paramName)
	{
		Debug.Assert(!paramName.IsNullOrWhiteSpace());

		Ensure.That(fileExtension, paramName).IsNotEmptyOrWhiteSpace();

		if (fileExtension.IndexOf('.') > 0)
		{
			throw new ArgumentException(
				"The period must be either the first character or omitted entirely.", paramName);
		}

		var invalidChars = fileExtension.Intersect(InvalidFileNameChars);
		if (invalidChars.Any())
		{
			throw new ArgumentException(
				"File extension contains the following invalid characters: " + invalidChars.ToJoinString(", "),
				paramName);
		}
	}
}
