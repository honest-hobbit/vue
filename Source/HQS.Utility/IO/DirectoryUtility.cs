﻿namespace HQS.Utility.IO;

public static class DirectoryUtility
{
	public static void CopyDirectory(
		string sourcePath,
		string destinationPath,
		string searchPattern,
		SearchOption searchOption,
		bool overwrite) => CopyDirectory(
			sourcePath,
			destinationPath,
			searchPattern,
			searchOption,
			(FileInfo sourceFile, FileInfo destinationFile) =>
				sourceFile.CopyTo(destinationFile.FullName, overwrite));

	public static void CopyDirectory(
		string sourcePath,
		string destinationPath,
		string searchPattern,
		SearchOption searchOption,
		Action<FileInfo, FileInfo> copyAction)
	{
		Ensure.That(sourcePath, nameof(sourcePath)).IsNotNullOrWhiteSpace();
		Ensure.That(destinationPath, nameof(destinationPath)).IsNotNullOrWhiteSpace();
		Ensure.That(searchPattern, nameof(searchPattern)).IsNotNull();
		Enumeration.ValidateIsDefined(searchOption, nameof(searchOption));
		Ensure.That(copyAction, nameof(copyAction)).IsNotNull();

		// Get information about the source directory
		var sourceDirectory = new DirectoryInfo(sourcePath);

		// Check if the source directory exists
		if (!sourceDirectory.Exists)
		{
			throw new DirectoryNotFoundException($"Source directory not found: {sourceDirectory.FullName}");
		}

		// Cache directories before we start copying
		var subdirectories = sourceDirectory.GetDirectories();

		// Create the destination directory
		Directory.CreateDirectory(destinationPath);

		// Get the files in the source directory and copy to the destination directory
		// (using GetFiles creates a copy of the current files to cache before copying)
		foreach (var sourceFile in sourceDirectory.GetFiles(searchPattern, SearchOption.TopDirectoryOnly))
		{
			var destinationFile = new FileInfo(Path.Combine(destinationPath, sourceFile.Name));
			copyAction(sourceFile, destinationFile);
		}

		// If copying subdirectories, recursively call this method
		if (searchOption == SearchOption.AllDirectories)
		{
			foreach (var subDirectory in subdirectories)
			{
				string newDestinationDir = Path.Combine(destinationPath, subDirectory.Name);
				CopyDirectory(
					subDirectory.FullName, newDestinationDir, searchPattern, searchOption, copyAction);
			}
		}
	}
}
