﻿namespace HQS.Utility.Resources;

public static class IPoolExtensions
{
	public static PoolDiagnostics GetDiagnostics(this IPoolStats pool)
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		return PoolDiagnostics.From(pool);
	}

	public static int ReleaseAll(this IPoolControls pool)
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		return pool.Release(pool.Available);
	}

	public static void CreateAll(this IPoolControls pool)
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		if (!pool.BoundedCapacity.HasValue)
		{
			return;
		}

		pool.Create(pool.BoundedCapacity.Value);
	}

	public static bool TryRent<T>(this IPinPool<T> pool, out Pin<T> pooled, out T value)
		where T : class
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		if (pool.TryRent(out pooled))
		{
			value = pooled.Value;
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}

	public static Pin<T> Rent<T>(this IPinPool<T> pool, out T value)
		where T : class
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		var pooled = pool.Rent();
		value = pooled.Value;
		return pooled;
	}
}
