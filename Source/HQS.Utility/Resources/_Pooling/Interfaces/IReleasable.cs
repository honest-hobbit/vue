﻿namespace HQS.Utility.Resources;

public interface IReleasable
{
	void Release();
}
