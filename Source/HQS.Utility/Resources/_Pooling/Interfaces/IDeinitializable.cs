﻿namespace HQS.Utility.Resources;

public interface IDeinitializable
{
	void Deinitialize();
}
