﻿namespace HQS.Utility.Resources;

public interface IInitializable
{
	void Initialize();
}
