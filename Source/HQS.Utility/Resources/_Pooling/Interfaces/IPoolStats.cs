﻿namespace HQS.Utility.Resources;

public interface IPoolStats
{
	int Available { get; }

	long Active { get; }

	long Created { get; }

	long Released { get; }
}
