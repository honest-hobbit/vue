﻿namespace HQS.Utility.Resources;

public static class IPoolControlsContracts
{
	public static void Create(int count) => Ensure.That(count, nameof(count)).IsGte(0);

	public static void Release(int count) => Ensure.That(count, nameof(count)).IsGte(0);
}
