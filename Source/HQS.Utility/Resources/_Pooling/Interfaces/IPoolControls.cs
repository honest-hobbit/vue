﻿namespace HQS.Utility.Resources;

public interface IPoolControls : IPoolStats
{
	int? BoundedCapacity { get; }

	// The pool may have a bounded capacity or it may be used by multiple threads
	// so fewer than count elements may be created.
	// Returns how many elements were created.
	int Create(int count);

	// The pool may have fewer than count elements or it may be used by multiple threads
	// so fewer than count elements may be released.
	// Returns how many elements were released.
	int Release(int count);
}
