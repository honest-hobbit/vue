﻿namespace HQS.Utility.Resources;

public interface IPinPool<T> : IPoolControls
	where T : class
{
	bool TryRent(out Pin<T> value);

	Pin<T> Rent();
}
