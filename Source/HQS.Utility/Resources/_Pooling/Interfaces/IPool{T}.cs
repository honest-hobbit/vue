﻿namespace HQS.Utility.Resources;

public interface IPool<T> : IPoolControls
{
	// TODO maybe add a BulkReturn method for returning a collection of values to reduce locking overhead
	// in thread safe pools. The tricky part is avoiding the garbage produced by IEnumerable<T>.
	// BulkReturn could return a scope object with a Return(T value) and Complete() method so no use
	// of IEnumerable<T>. The scope object could be a single shared reused object per pool instance.
	void Return(T value);

	bool TryRent(out T value);

	T Rent();
}
