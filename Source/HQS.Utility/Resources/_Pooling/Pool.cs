﻿namespace HQS.Utility.Resources;

public static class Pool
{
	public static IPool<T> Create<T>(PoolOptions<T> options = default)
		where T : new() =>
		new Pool<T>(Factory.CreateNew<T>, PoolOptions.WithInterfaces(options));

	public static IPool<T> Create<T>(Func<T> factory, PoolOptions<T> options = default) =>
		new Pool<T>(factory, PoolOptions.WithInterfaces(options));

	public static class ThreadSafe
	{
		public static IPool<T> Create<T>(PoolOptions<T> options = default)
			where T : new() =>
			new ThreadSafePool<T>(Factory.CreateNew<T>, PoolOptions.WithInterfaces(options));

		public static IPool<T> Create<T>(Func<T> factory, PoolOptions<T> options = default) =>
			new ThreadSafePool<T>(factory, PoolOptions.WithInterfaces(options));

		public static class Pins
		{
			public static IPinPool<T> Create<T>(PoolOptions<T> options = default)
				where T : class, new() =>
				new PinPool<T>(Factory.CreateNew<T>, PoolOptions.WithInterfaces(options));

			public static IPinPool<T> Create<T>(Func<T> factory, PoolOptions<T> options = default)
				where T : class =>
				new PinPool<T>(factory, PoolOptions.WithInterfaces(options));
		}
	}
}
