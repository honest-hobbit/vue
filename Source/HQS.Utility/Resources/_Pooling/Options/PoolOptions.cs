﻿namespace HQS.Utility.Resources;

public static class PoolOptions
{
	public static PoolOptions<T> InterfacesFor<T>() => WithInterfaces<T>(default);

	public static PoolOptions<T> WithInterfaces<T>(PoolOptions<T> options) => new PoolOptions<T>()
	{
		Initialize = options.Initialize ?? (x => (x as IInitializable)?.Initialize()),
		Deinitialize = options.Deinitialize ?? (x => (x as IDeinitializable)?.Deinitialize()),
		Release = options.Release ?? (x =>
		{
			try
			{
				(x as IReleasable)?.Release();
			}
			finally
			{
				(x as IDisposable)?.Dispose();
			}
		}),
	};
}
