﻿namespace HQS.Utility.Resources;

[SuppressMessage("StyleCop", "SA1313", Justification = "Record syntax.")]
public record struct PoolOptions<T>(
	Action<T> Initialize,
	Action<T> Deinitialize,
	Action<T> Release,
	int? BoundedCapacity,
	IEqualityComparer<T> DuplicateComparer)
{
	public void Validate()
	{
		if (this.BoundedCapacity.HasValue)
		{
			Ensure.That(this.BoundedCapacity.Value, nameof(this.BoundedCapacity)).IsGte(0);
		}
	}
}
