﻿namespace HQS.Utility.Resources;

public class PinPool<T> : IPinPool<T>
	where T : class
{
	private readonly Func<T> factory;

	private readonly ThreadSafePool<PooledResource> pool;

	public PinPool(Func<T> factory, PoolOptions<T> options = default)
	{
		Ensure.That(factory, nameof(factory)).IsNotNull();
		options.Validate();

		this.factory = factory;

		if (options == null)
		{
			this.pool = new ThreadSafePool<PooledResource>(this.CreatePooledResource);
		}
		else
		{
			this.pool = new ThreadSafePool<PooledResource>(
				this.CreatePooledResource,
				new PoolOptions<PooledResource>()
				{
					// do not copy DuplicateComparer over because Pins already protect against duplicates
					Initialize = options.Initialize == null ? null : x => options.Initialize((T)x.Value),
					Deinitialize = options.Deinitialize == null ? null : x => options.Deinitialize((T)x.Value),
					Release = options.Release == null ? null : x => options.Release((T)x.Value),
					BoundedCapacity = options.BoundedCapacity,
				});
		}
	}

	/// <inheritdoc />
	public int? BoundedCapacity => this.pool.BoundedCapacity;

	/// <inheritdoc />
	public int Available => this.pool.Available;

	/// <inheritdoc />
	public long Active => this.pool.Active;

	/// <inheritdoc />
	public long Created => this.pool.Created;

	/// <inheritdoc />
	public long Released => this.pool.Released;

	/// <inheritdoc />
	public int Create(int count) => this.pool.Create(count);

	/// <inheritdoc />
	public int Release(int count) => this.pool.Release(count);

	/// <inheritdoc />
	public bool TryRent(out Pin<T> value)
	{
		if (this.pool.TryRent(out var resource))
		{
			value = resource.CreatePin<T>();
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}

	/// <inheritdoc />
	public Pin<T> Rent() => this.pool.Rent().CreatePin<T>();

	private PooledResource CreatePooledResource()
	{
		var value = this.factory();
		if (value == null)
		{
			throw new ArgumentNullException(
				$"{nameof(this.factory)}.Invoke",
				$"{nameof(IPinPool<T>)} does not support null values. Factory must not return null.");
		}

		return new PooledResource(this.pool, value);
	}

	private class PooledResource : IPooledResource
	{
		private readonly AtomicInt pinCount = new AtomicInt(0);

		private readonly ThreadSafePool<PooledResource> pool;

		public PooledResource(ThreadSafePool<PooledResource> pool, object value)
		{
			Debug.Assert(pool != null);
			Debug.Assert(value != null);

			this.pool = pool;
			this.Value = value;
		}

		public object Value { get; }

		public int PinCount => this.pinCount.Read();

		public Pin<TPin> CreatePin<TPin>()
			where TPin : class
		{
			if (this.pinCount.Increment() == int.MinValue)
			{
				throw new OverflowException(
					$"{nameof(this.PinCount)} overflowed. This means pins are not being unpinned.");
			}

			return PooledPin.GetPin<TPin>(this);
		}

		void IPooledResource.Unpin()
		{
			int count = this.pinCount.Decrement();
			if (count < 0)
			{
				this.pinCount.Increment();
				throw new InvalidOperationException(
					$"{nameof(IPooledResource.Unpin)} can't be called because {this.PinCount} is 0.");
			}

			if (count == 0)
			{
				this.pool.Return(this);
			}
		}
	}
}
