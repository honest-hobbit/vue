﻿namespace HQS.Utility.Resources;

[SuppressMessage("StyleCop", "SA1313", Justification = "Record syntax.")]
public readonly record struct PoolDiagnostics(
	int Available, long Active, long Created, long Released)
{
	public static int AverageStringLength => 100;

	public static PoolDiagnostics From(IPoolStats pool)
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		return new PoolDiagnostics(
			pool.Available, pool.Active, pool.Created, pool.Released);
	}
}
