﻿namespace HQS.Utility.Resources;

public class Pool<T> : IPool<T>
{
	// using Stack here to encourage frequent reuse of the same few objects
	private readonly Stack<T> values = new Stack<T>();

#if DEBUG
	private readonly HashSet<T> duplicates;
#endif

	private readonly Func<T> factory;

	private readonly Action<T> initialize;

	private readonly Action<T> deinitialize;

	private readonly Action<T> release;

	public Pool(Func<T> factory, PoolOptions<T> options = default)
	{
		Ensure.That(factory, nameof(factory)).IsNotNull();
		options.Validate();

		this.factory = factory;
		this.initialize = options.Initialize;
		this.deinitialize = options.Deinitialize;
		this.release = options.Release;
		this.BoundedCapacity = options.BoundedCapacity;

#if DEBUG
		if (options.DuplicateComparer != null)
		{
			this.duplicates = new HashSet<T>(options.DuplicateComparer);
		}
#endif
	}

	/// <inheritdoc />
	public int? BoundedCapacity { get; }

	/// <inheritdoc />
	public int Available => this.values.Count;

	/// <inheritdoc />
	public long Active { get; private set; }

	/// <inheritdoc />
	public long Created { get; private set; }

	/// <inheritdoc />
	public long Released { get; private set; }

	/// <inheritdoc />
	public void Return(T value)
	{
		this.Active--;
		this.deinitialize?.Invoke(value);

		if (!this.BoundedCapacity.HasValue || this.values.Count < this.BoundedCapacity)
		{
			this.ValidateNotDuplicate(value);

			this.values.Push(value);
			return;
		}

		this.Released++;
		this.release?.Invoke(value);
	}

	/// <inheritdoc />
	public bool TryRent(out T value)
	{
		if (this.TryPop(out value))
		{
			this.initialize?.Invoke(value);
			this.Active++;
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}

	/// <inheritdoc />
	public T Rent()
	{
		if (this.TryRent(out var value))
		{
			return value;
		}
		else
		{
			value = this.factory();
			this.initialize?.Invoke(value);
			this.Created++;
			this.Active++;
			return value;
		}
	}

	/// <inheritdoc />
	public int Create(int count)
	{
		IPoolControlsContracts.Create(count);

		if (this.BoundedCapacity.HasValue)
		{
			count = count.ClampUpper(this.BoundedCapacity.Value - this.values.Count);
		}

		int created = 0;
		try
		{
			while (created < count)
			{
				var value = this.factory();

				this.ValidateNotDuplicate(value);

				this.values.Push(value);
				created++;
			}
		}
		finally
		{
			this.Created += created;
		}

		return created;
	}

	/// <inheritdoc />
	public int Release(int count)
	{
		IPoolControlsContracts.Release(count);

		int released = 0;
		try
		{
			while (released < count && this.TryPop(out var value))
			{
				released++;
				this.release?.Invoke(value);
			}
		}
		finally
		{
			this.Released += released;
		}

		return released;
	}

	[Conditional(CompilationSymbol.Debug)]
	private void ValidateNotDuplicate(T value)
	{
#if DEBUG
		if (!this.duplicates?.Add(value) ?? false)
		{
			throw new ArgumentException("Value is already in the pool.", nameof(value));
		}
#endif
	}

	private bool TryPop(out T value)
	{
		if (this.values.TryPop(out value))
		{
#if DEBUG
			this.duplicates?.Remove(value);
#endif
			return true;
		}
		else
		{
			return false;
		}
	}
}
