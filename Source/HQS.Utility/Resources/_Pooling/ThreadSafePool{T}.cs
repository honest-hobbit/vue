﻿namespace HQS.Utility.Resources;

public class ThreadSafePool<T> : IPool<T>
{
	// using lock with regular collection instead of a concurrent collection
	// to avoid allocating more memory that will have to be garbage collected
	private readonly object padlock = new object();

	// using Stack here to encourage frequent reuse of the same few objects
	private readonly Stack<T> values = new Stack<T>();

#if DEBUG
	private readonly HashSet<T> duplicates;
#endif

	private readonly AtomicInt availableCount = new AtomicInt(0);

	private readonly AtomicLong activeCount = new AtomicLong(0);

	private readonly AtomicLong createdCount = new AtomicLong(0);

	private readonly AtomicLong releasedCount = new AtomicLong(0);

	private readonly Func<T> factory;

	private readonly Action<T> initialize;

	private readonly Action<T> deinitialize;

	private readonly Action<T> release;

	public ThreadSafePool(Func<T> factory, PoolOptions<T> options = default)
	{
		Ensure.That(factory, nameof(factory)).IsNotNull();
		options.Validate();

		this.factory = factory;
		this.initialize = options.Initialize;
		this.deinitialize = options.Deinitialize;
		this.release = options.Release;
		this.BoundedCapacity = options.BoundedCapacity;

#if DEBUG
		if (options.DuplicateComparer != null)
		{
			this.duplicates = new HashSet<T>(options.DuplicateComparer);
		}
#endif
	}

	/// <inheritdoc />
	public int? BoundedCapacity { get; }

	/// <inheritdoc />
	public int Available => this.availableCount.Read();

	/// <inheritdoc />
	public long Active => this.activeCount.Read();

	/// <inheritdoc />
	public long Created => this.createdCount.Read();

	/// <inheritdoc />
	public long Released => this.releasedCount.Read();

	/// <inheritdoc />
	public void Return(T value)
	{
		this.activeCount.Decrement();
		this.deinitialize?.Invoke(value);

		lock (this.padlock)
		{
			if (!this.BoundedCapacity.HasValue || this.values.Count < this.BoundedCapacity)
			{
				this.ValidateNotDuplicate(value);

				this.values.Push(value);
				this.availableCount.Increment();
				return;
			}
		}

		this.releasedCount.Increment();
		this.release?.Invoke(value);
	}

	/// <inheritdoc />
	public bool TryRent(out T value)
	{
		if (this.LockAndTryPop(out value))
		{
			this.initialize?.Invoke(value);
			this.activeCount.Increment();
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}

	/// <inheritdoc />
	public T Rent()
	{
		if (this.TryRent(out var value))
		{
			return value;
		}
		else
		{
			value = this.factory();
			this.initialize?.Invoke(value);
			this.createdCount.Increment();
			this.activeCount.Increment();
			return value;
		}
	}

	/// <inheritdoc />
	public int Create(int count)
	{
		IPoolControlsContracts.Create(count);

		lock (this.padlock)
		{
			if (this.BoundedCapacity.HasValue)
			{
				count = count.ClampUpper(this.BoundedCapacity.Value - this.values.Count);
			}

			int created = 0;
			try
			{
				while (created < count)
				{
					var value = this.factory();

					this.ValidateNotDuplicate(value);

					this.values.Push(value);
					created++;
				}
			}
			finally
			{
				this.createdCount.Add(created);
				this.availableCount.Add(created);
			}

			return created;
		}
	}

	/// <inheritdoc />
	public int Release(int count)
	{
		IPoolControlsContracts.Release(count);

		int released = 0;
		try
		{
			while (released < count && this.LockAndTryPop(out var value))
			{
				released++;
				this.release?.Invoke(value);
			}
		}
		finally
		{
			this.releasedCount.Add(released);
		}

		return released;
	}

	// only call this while in the lock
	[Conditional(CompilationSymbol.Debug)]
	private void ValidateNotDuplicate(T value)
	{
#if DEBUG
		if (!this.duplicates?.Add(value) ?? false)
		{
			throw new ArgumentException("Value is already in the pool.", nameof(value));
		}
#endif
	}

	private bool LockAndTryPop(out T value)
	{
		lock (this.padlock)
		{
			if (this.values.TryPop(out value))
			{
#if DEBUG
				this.duplicates?.Remove(value);
#endif
				this.availableCount.Decrement();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
