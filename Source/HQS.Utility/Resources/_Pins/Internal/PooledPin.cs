﻿namespace HQS.Utility.Resources;

internal class PooledPin
{
	// using lock with regular collection instead of a concurrent collection
	// to avoid allocating more memory that will have to be garbage collected
	private static readonly object Padlock = new object();

	// using Queue here to evenly spread reuse of pins to reduce frequency of version overflowing
	private static readonly Queue<PooledPin> PinPool = new Queue<PooledPin>();

	private readonly AtomicInt version = new AtomicInt(1);

	private IPooledResource resource;

	private object value;

	private PooledPin()
	{
	}

	public static Pin<T> GetPin<T>(IPooledResource resource)
		where T : class
	{
		Debug.Assert(resource != null);
		Debug.Assert(resource.PinCount >= 1);

		PooledPin pin = null;
		lock (Padlock)
		{
			PinPool.TryDequeue(out pin);
		}

		pin ??= new PooledPin();

		pin.resource = resource;
		pin.value = resource.Value;

		return new Pin<T>(pin, pin.version.Read());
	}

	public static bool AreSameValue(int versionA, PooledPin pinA, int versionB, PooledPin pinB)
	{
		if (pinA == null || pinB == null)
		{
			return false;
		}

		if (versionA != pinA.version.Read() || versionB != pinB.version.Read())
		{
			return false;
		}

		return pinA.resource.EqualsByReferenceNullSafe(pinB.resource);
	}

	public bool IsPinned(int version) => version == this.version.Read();

	public int GetPinCount(int version)
	{
		if (version != this.version.Read())
		{
			return 0;
		}

		return this.resource.PinCount;
	}

	public bool TryCreatePin<T>(int version, out Pin<T> pin)
		where T : class
	{
		if (version != this.version.Read())
		{
			pin = default;
			return false;
		}

		pin = this.resource.CreatePin<T>();
		return true;
	}

	public bool TryUnpin(int version)
	{
		if (version != this.version.CompareExchange(version + 1, version))
		{
			return false;
		}

		var resource = this.resource;
		this.resource = null;
		this.value = null;

		// return this instance to the pool to be reused
		lock (Padlock)
		{
			PinPool.Enqueue(this);
		}

		// Unpin may call ReturnToPool which might be a user defined subclass that could throw an exception
		resource.Unpin();

		return true;
	}

	public bool TryGetValue<T>(int version, out T value)
		where T : class
	{
		if (version != this.version.Read())
		{
			value = null;
			return false;
		}

		value = this.value as T;
		return value != null;
	}
}
