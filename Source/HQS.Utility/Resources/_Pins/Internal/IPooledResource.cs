﻿namespace HQS.Utility.Resources;

internal interface IPooledResource
{
	// this can be accessed even when PinCount is 0
	object Value { get; }

	int PinCount { get; }

	// this can be called even when PinCount is 0
	Pin<T> CreatePin<T>()
		where T : class;

	// this can only be called when PinCount is above 0
	void Unpin();
}
