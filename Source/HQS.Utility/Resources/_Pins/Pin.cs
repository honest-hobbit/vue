﻿namespace HQS.Utility.Resources;

public static class Pin
{
	public static Pin<T> WithoutPool<T>(T value)
		where T : class
	{
		Debug.Assert(value != null);

		return new NotPooledResource(value).CreatePin<T>();
	}

	private class NotPooledResource : IPooledResource
	{
		private readonly AtomicInt pinCount = new AtomicInt(0);

		public NotPooledResource(object value)
		{
			Debug.Assert(value != null);

			this.Value = value;
		}

		public object Value { get; }

		public int PinCount => this.pinCount.Read();

		public Pin<T> CreatePin<T>()
			where T : class
		{
			if (this.pinCount.Increment() == int.MinValue)
			{
				throw new OverflowException($"{nameof(this.PinCount)} overflowed. This means pins are not being unpinned.");
			}

			return PooledPin.GetPin<T>(this);
		}

		public void Unpin()
		{
			int count = this.pinCount.Decrement();

			Debug.Assert(count >= 0);
		}
	}
}
