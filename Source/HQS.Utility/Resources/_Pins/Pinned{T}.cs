﻿namespace HQS.Utility.Resources;

public readonly struct Pinned<T> : IEquatable<Pinned<T>>, IEquatable<Pin<T>>, IDisposed
	where T : class
{
	private readonly Pin<T> source;

	internal Pinned(Pin<T> source)
	{
		this.source = source;
	}

	public bool IsUnassigned => this.source.IsUnassigned;

	public bool IsAssigned => this.source.IsAssigned;

	/// <inheritdoc />
	public bool IsDisposed => this.source.IsDisposed;

	// returns true if this specific pin has not been unpinned yet, otherwise false
	public bool IsPinned => this.source.IsPinned;

	// returns how many pins this value has if this specific pin hasn't been unpinned yet, otherwise 0
	public int PinCount => this.source.PinCount;

	public T Value => this.source.Value;

	public T ValueOrNull => this.source.ValueOrNull;

	public static bool operator ==(Pinned<T> lhs, Pinned<T> rhs) => lhs.Equals(rhs);

	public static bool operator !=(Pinned<T> lhs, Pinned<T> rhs) => !lhs.Equals(rhs);

	public static bool operator ==(Pinned<T> lhs, Pin<T> rhs) => lhs.Equals(rhs);

	public static bool operator !=(Pinned<T> lhs, Pin<T> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(Pinned<T> other) => this.source.Equals(other.source);

	/// <inheritdoc />
	public bool Equals(Pin<T> other) => this.source.Equals(other);

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj) || Struct.Equals(this.source, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.source.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.source.ToString();

	public bool IsSameValueAs<TOther>(Pin<TOther> other)
		where TOther : class => this.source.IsSameValueAs(other);

	public bool IsSameValueAs<TOther>(Pinned<TOther> other)
		where TOther : class => this.source.IsSameValueAs(other.source);

	public Pin<T> CreatePin() => this.source.CreatePin();

	public bool TryGetValue(out T value) => this.source.TryGetValue(out value);

	public bool TryCastTo<TCast>(out Pinned<TCast> pinned)
		where TCast : class
	{
		var result = this.source.TryCastTo<TCast>(out var pooled);
		pinned = new Pinned<TCast>(pooled);
		return result;
	}

	public Pinned<TCast> CastTo<TCast>()
		where TCast : class => new Pinned<TCast>(this.source.CastTo<TCast>());

	public Pinned<TCast> CastToOrNone<TCast>()
		where TCast : class => new Pinned<TCast>(this.source.CastToOrNone<TCast>());
}
