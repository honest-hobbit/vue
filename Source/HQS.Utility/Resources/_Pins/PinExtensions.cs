﻿namespace HQS.Utility.Resources;

public static class PinExtensions
{
	public static void PinAllTo<T>(this IEnumerable<Pinned<T>> source, ICollection<Pin<T>> destination)
		where T : class
	{
		Debug.Assert(source != null);
		Debug.Assert(destination != null);

		foreach (var pin in source)
		{
			destination.Add(pin.CreatePin());
		}
	}

	public static void PinAllTo<T>(this IReadOnlyList<Pinned<T>> source, ICollection<Pin<T>> destination)
		where T : class
	{
		Debug.Assert(source != null);
		Debug.Assert(destination != null);

		int max = source.Count;
		for (int i = 0; i < max; i++)
		{
			destination.Add(source[i].CreatePin());
		}
	}

	public static void PinAllTo<T>(this IEnumerable<Pin<T>> source, ICollection<Pin<T>> destination)
		where T : class
	{
		Debug.Assert(source != null);
		Debug.Assert(destination != null);

		foreach (var pin in source)
		{
			destination.Add(pin.CreatePin());
		}
	}

	public static void PinAllTo<T>(this IReadOnlyList<Pin<T>> source, ICollection<Pin<T>> destination)
		where T : class
	{
		Debug.Assert(source != null);
		Debug.Assert(destination != null);

		int max = source.Count;
		for (int i = 0; i < max; i++)
		{
			destination.Add(source[i].CreatePin());
		}
	}

	public static void DisposeAllAndClear<T>(this ICollection<T> source)
		where T : IDisposable
	{
		Debug.Assert(source != null);

		foreach (var pin in source)
		{
			pin.Dispose();
		}

		source.Clear();
	}

	public static void DisposeAllAndClear<T>(this IList<T> source)
		where T : IDisposable
	{
		Debug.Assert(source != null);

		int max = source.Count;
		for (int i = 0; i < max; i++)
		{
			source[i].Dispose();
		}

		source.Clear();
	}

	public static void DisposeAllAndClear<TKey, TValue>(this ICollection<KeyValuePair<TKey, TValue>> source)
		where TValue : IDisposable
	{
		Debug.Assert(source != null);

		foreach (var pair in source)
		{
			pair.Value.Dispose();
		}

		source.Clear();
	}

	public static void DisposeAllAndClear<TKey, TValue>(this IList<KeyValuePair<TKey, TValue>> source)
		where TValue : IDisposable
	{
		Debug.Assert(source != null);

		int max = source.Count;
		for (int i = 0; i < max; i++)
		{
			source[i].Value.Dispose();
		}

		source.Clear();
	}
}
