﻿namespace HQS.Utility.Resources;

public readonly struct Pin<T> : IEquatable<Pin<T>>, IEquatable<Pinned<T>>, IVisiblyDisposable
	where T : class
{
	private readonly PooledPin source;

	private readonly int version;

	internal Pin(PooledPin source, int version)
	{
		this.source = source;
		this.version = version;
	}

	public Pinned<T> AsPinned => new Pinned<T>(this);

	public bool IsUnassigned => this.source == null;

	public bool IsAssigned => this.source != null;

	/// <inheritdoc />
	public bool IsDisposed => (!this.source?.IsPinned(this.version)) ?? false;

	// returns true if this specific pin has not been unpinned yet, otherwise false
	public bool IsPinned => this.source?.IsPinned(this.version) ?? false;

	// returns how many pins this value has if this specific pin hasn't been unpinned yet, otherwise 0
	public int PinCount => this.source?.GetPinCount(this.version) ?? 0;

	public T Value
	{
		get
		{
			this.ValidateIsAssigned();

			if (this.source.TryGetValue(this.version, out T value))
			{
				return value;
			}
			else
			{
				throw new InvalidOperationException(
					$"Can't access {nameof(this.Value)} because this was already disposed or the type cast is invalid.");
			}
		}
	}

	public T ValueOrNull
	{
		get
		{
			T value = null;
			this.source?.TryGetValue(this.version, out value);
			return value;
		}
	}

	public static bool operator ==(Pin<T> lhs, Pin<T> rhs) => lhs.Equals(rhs);

	public static bool operator !=(Pin<T> lhs, Pin<T> rhs) => !lhs.Equals(rhs);

	public static bool operator ==(Pin<T> lhs, Pinned<T> rhs) => lhs.Equals(rhs);

	public static bool operator !=(Pin<T> lhs, Pinned<T> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(Pin<T> other) => this.version == other.version && this.source == other.source;

	/// <inheritdoc />
	public bool Equals(Pinned<T> other) => other.Equals(this);

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj) || Struct.Equals(this.AsPinned, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.version, this.source);

	/// <inheritdoc />
	public override string ToString()
	{
		if (this.source == null)
		{
			return $"Unassigned {nameof(Pin<T>)}";
		}

		if (this.source.TryGetValue(this.version, out T value))
		{
			return $"Pin[{value}]";
		}
		else
		{
			return $"Inaccessible {nameof(Pin<T>)}";
		}
	}

	public bool IsSameValueAs<TOther>(Pin<TOther> other)
		where TOther : class => PooledPin.AreSameValue(this.version, this.source, other.version, other.source);

	public bool IsSameValueAs<TOther>(Pinned<TOther> other)
		where TOther : class => other.IsSameValueAs(this);

	public Pin<T> CreatePin()
	{
		this.ValidateIsAssigned();

		if (this.source.TryCreatePin<T>(this.version, out var pin))
		{
			return pin;
		}
		else
		{
			throw new ObjectDisposedException($"Can't {nameof(this.CreatePin)} because this was already disposed.");
		}
	}

	/// <inheritdoc />
	public void Dispose() => this.source?.TryUnpin(this.version);

	public bool TryGetValue(out T value)
	{
		value = default;
		return this.source?.TryGetValue(this.version, out value) ?? false;
	}

	public bool TryCastTo<TCast>(out Pin<TCast> pooled)
		where TCast : class
	{
		if (this.source?.TryGetValue(this.version, out TCast _) ?? false)
		{
			pooled = new Pin<TCast>(this.source, this.version);
			return true;
		}
		else
		{
			pooled = default;
			return false;
		}
	}

	public Pin<TCast> CastTo<TCast>()
		where TCast : class
	{
		if (this.TryCastTo<TCast>(out var result))
		{
			return result;
		}
		else
		{
			throw new InvalidCastException($"Can't cast to {typeof(TCast).Name}.");
		}
	}

	public Pin<TCast> CastToOrNone<TCast>()
		where TCast : class
	{
		this.TryCastTo<TCast>(out var result);
		return result;
	}

	private void ValidateIsAssigned()
	{
		if (this.source == null)
		{
			throw new InvalidOperationException($"{nameof(Pin<T>)} is unassigned.");
		}
	}
}
