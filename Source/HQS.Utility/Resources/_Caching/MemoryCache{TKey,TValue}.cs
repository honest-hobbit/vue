﻿namespace HQS.Utility.Resources;

public class MemoryCache<TKey, TValue>
	where TValue : class
{
	private readonly ConcurrentDictionary<TKey, Lazy<Entry>> values;

	private readonly ICacheValueFactory<TKey, TValue> factory;

	private readonly Func<TKey, Lazy<Entry>> createEntry;

	public MemoryCache(ICacheValueFactory<TKey, TValue> factory, IEqualityComparer<TKey> comparer = null)
	{
		Debug.Assert(factory != null);

		this.values = new ConcurrentDictionary<TKey, Lazy<Entry>>(comparer ?? EqualityComparer<TKey>.Default);
		this.factory = factory;
		this.createEntry = this.CreateEntry;
	}

	public MemoryCache(ICacheValueFactory<TKey, TValue> factory, ConcurrentDictionaryOptions<TKey> options = null)
	{
		Debug.Assert(factory != null);

		this.values = options.CreateDictionaryNullSafe<TKey, Lazy<Entry>>();
		this.factory = factory;
		this.createEntry = this.CreateEntry;
	}

	public int Count => this.values.Count;

	public Cached<TKey, TValue> GetCached(TKey key)
	{
		Debug.Assert(key != null);

		while (true)
		{
			var entry = this.values.GetOrAdd(key, this.createEntry).Value;
			if (entry.TryCreateCached(out var cached))
			{
				return cached;
			}
		}
	}

	public IEnumerable<ICacheEntry<TKey, TValue>> GetPinsSnapshot()
	{
		foreach (var pair in this.values.ToArray())
		{
			yield return pair.Value.Value;
		}
	}

	public IEnumerable<ICacheEntry<TKey, TValue>> GetPinsConcurrent()
	{
		foreach (var lazy in this.values.ValuesLockFree())
		{
			yield return lazy.Value;
		}
	}

	private Lazy<Entry> CreateEntry(TKey key)
	{
		Debug.Assert(key != null);

		return new Lazy<Entry>(() => new Entry(key, this.factory.CreateValue(key), this.factory, this.values));
	}

	private sealed class Entry : ICacheEntry<TKey, TValue>, IExpirable, IPooledResource
	{
		private const int Expired = -1;

		// this also acts as the lock object for this class
		private readonly AtomicInt tokenVersion = new AtomicInt(1);

		private readonly TValue value;

		private readonly ICacheValueExpiry<TKey, TValue> expiry;

		private readonly ConcurrentDictionary<TKey, Lazy<Entry>> dictionary;

		private int pinCount = 0;

		public Entry(
			TKey key,
			TValue value,
			ICacheValueExpiry<TKey, TValue> expiry,
			ConcurrentDictionary<TKey, Lazy<Entry>> dictionary)
		{
			Debug.Assert(key != null);
			Debug.Assert(expiry != null);
			Debug.Assert(dictionary != null);

			this.Key = key;
			this.value = value;
			this.expiry = expiry;
			this.dictionary = dictionary;
		}

		/// <inheritdoc />
		public TKey Key { get; }

		/// <inheritdoc />
		public object Value => this.value;

		/// <inheritdoc />
		public bool IsExpired
		{
			get
			{
				lock (this.tokenVersion)
				{
					return this.pinCount == Expired;
				}
			}
		}

		/// <inheritdoc />
		public bool IsPinned
		{
			get
			{
				lock (this.tokenVersion)
				{
					return this.pinCount > 0;
				}
			}
		}

		/// <inheritdoc />
		public int PinCount
		{
			get
			{
				lock (this.tokenVersion)
				{
					return this.pinCount.ClampLower(0);
				}
			}
		}

		/// <inheritdoc />
		public bool TryCreatePin(out Pin<TValue> pin)
		{
			lock (this.tokenVersion)
			{
				if (this.pinCount == Expired)
				{
					pin = default;
					return false;
				}

				this.IncrementPinCountInsideLock();
			}

			pin = PooledPin.GetPin<TValue>(this);
			return true;
		}

		/// <inheritdoc />
		public bool TryCreateCached(out Cached<TKey, TValue> cached)
		{
			if (this.TryCreatePin(out var pin))
			{
				cached = new Cached<TKey, TValue>(this, pin);
				return true;
			}
			else
			{
				cached = default;
				return false;
			}
		}

		/// <inheritdoc />
		public bool TryExpire()
		{
			lock (this.tokenVersion)
			{
				if (this.pinCount != 0)
				{
					return false;
				}

				this.ExpireInsideLock();
			}

			this.ExpireOutsideLock();
			return true;
		}

		/// <inheritdoc />
		public bool TryExpire(CacheExpiration token)
		{
			lock (this.tokenVersion)
			{
				if (token.Version != this.tokenVersion.Read())
				{
					return false;
				}

				Debug.Assert(this.pinCount == 0);

				this.ExpireInsideLock();
			}

			this.ExpireOutsideLock();
			return true;
		}

		/// <inheritdoc />
		public bool IsTokenStale(CacheExpiration token) => token.Version != this.tokenVersion.Read();

		/// <inheritdoc />
		public Pin<T> CreatePin<T>()
			where T : class
		{
			lock (this.tokenVersion)
			{
				Debug.Assert(this.pinCount >= 0);

				this.IncrementPinCountInsideLock();
			}

			return PooledPin.GetPin<T>(this);
		}

		/// <inheritdoc />
		public void Unpin()
		{
			bool noLongerPinned = false;
			int tokenVersion = 0;

			lock (this.tokenVersion)
			{
				Debug.Assert(this.pinCount >= 1);

				this.pinCount--;
				if (this.pinCount == 0)
				{
					noLongerPinned = true;
					tokenVersion = this.tokenVersion.Read();
				}
			}

			if (noLongerPinned)
			{
				this.expiry.EnqueueToken(this.Key, this.value, new CacheExpiration(this, tokenVersion));
			}
		}

		private void IncrementPinCountInsideLock()
		{
			if (this.pinCount == 0)
			{
				this.tokenVersion.Increment();
			}

			this.pinCount++;
		}

		private void ExpireInsideLock()
		{
			// only 1 thread will reach this point
			this.tokenVersion.Increment();
			this.pinCount = Expired;
		}

		private void ExpireOutsideLock()
		{
			// only 1 thread will reach this point
			this.dictionary.TryRemove(this.Key);
			this.expiry.ValueExpired(this.Key, this.value);
		}
	}
}
