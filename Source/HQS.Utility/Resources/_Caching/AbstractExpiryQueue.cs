﻿namespace HQS.Utility.Resources;

public abstract class AbstractExpiryQueue
{
	private readonly ThreadSafeCompactingQueue<CacheExpiration> queue;

	public AbstractExpiryQueue(ThreadSafeCompactingQueue<CacheExpiration> queue)
	{
		Debug.Assert(queue != null);

		this.queue = queue;
	}

	public AbstractExpiryQueue(int expiryCapacity)
	{
		Debug.Assert(expiryCapacity >= 0);

		this.queue = new ThreadSafeCompactingQueue<CacheExpiration>(x => x.IsStale, expiryCapacity);
	}

	public AbstractExpiryQueue()
	{
		this.queue = new ThreadSafeCompactingQueue<CacheExpiration>(x => x.IsStale);
	}

	public void AddExpiration(CacheExpiration token) => this.queue.Enqueue(token);

	public void ExpireAll()
	{
		while (this.TryExpireToken())
		{
		}
	}

	protected bool TryExpireToken()
	{
		while (this.queue.TryDequeue(out var token))
		{
			if (token.TryExpire())
			{
				return true;
			}
		}

		return false;
	}
}
