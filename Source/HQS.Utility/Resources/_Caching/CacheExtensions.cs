﻿namespace HQS.Utility.Resources;

public static class CacheExtensions
{
	// returns how many entries were successfully expired
	public static int TryExpireAll<TKey, TValue>(this IEnumerable<ICacheEntry<TKey, TValue>> pins)
		where TValue : class
	{
		Debug.Assert(pins != null);

		int expired = 0;
		foreach (var pin in pins)
		{
			if (pin.TryExpire())
			{
				expired++;
			}
		}

		return expired;
	}
}
