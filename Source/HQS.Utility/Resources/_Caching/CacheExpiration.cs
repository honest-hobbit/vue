﻿namespace HQS.Utility.Resources;

public readonly record struct CacheExpiration
{
	// default struct constructor leaves this field as null so methods must be null safe
	private readonly IExpirable entry;

	internal CacheExpiration(IExpirable entry, int version)
	{
		Debug.Assert(entry != null);

		this.entry = entry;
		this.Version = version;
	}

	public bool IsStale => this.entry?.IsTokenStale(this) ?? true;

	internal int Version { get; }

	public bool TryExpire() => this.entry?.TryExpire(this) ?? false;
}
