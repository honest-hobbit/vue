﻿namespace HQS.Utility.Resources;

internal interface IExpirable
{
	bool IsTokenStale(CacheExpiration token);

	bool TryExpire(CacheExpiration token);
}
