﻿namespace HQS.Utility.Resources;

public interface ICacheValueExpiry<TKey, TValue>
	where TValue : class
{
	void EnqueueToken(TKey key, TValue value, CacheExpiration token);

	void ValueExpired(TKey key, TValue value);
}
