﻿namespace HQS.Utility.Resources;

public interface ICacheValueFactory<TKey, TValue> : ICacheValueExpiry<TKey, TValue>
	where TValue : class
{
	TValue CreateValue(TKey key);
}
