﻿namespace HQS.Utility.Resources;

public interface ICacheEntry<TKey, TValue> : IKeyed<TKey>
	where TValue : class
{
	bool IsExpired { get; }

	bool IsPinned { get; }

	int PinCount { get; }

	bool TryCreatePin(out Pin<TValue> pin);

	bool TryCreateCached(out Cached<TKey, TValue> cached);

	bool TryExpire();
}
