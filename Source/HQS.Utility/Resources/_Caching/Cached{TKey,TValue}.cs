﻿namespace HQS.Utility.Resources;

public readonly record struct Cached<TKey, TValue> : IKeyed<TKey>
	where TValue : class
{
	internal Cached(ICacheEntry<TKey, TValue> entry, Pin<TValue> pin)
	{
		Debug.Assert(entry != null);
		Debug.Assert(entry.IsPinned);
		Debug.Assert(pin.IsPinned);

		this.Entry = entry;
		this.Pin = pin;
	}

	public bool IsUnassigned => this.Entry == null;

	public bool IsAssigned => this.Entry != null;

	public TKey Key => this.Entry != null ? this.Entry.Key : default;

	public ICacheEntry<TKey, TValue> Entry { get; }

	public Pin<TValue> Pin { get; }
}
