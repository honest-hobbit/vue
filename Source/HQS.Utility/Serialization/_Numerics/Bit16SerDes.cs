﻿namespace HQS.Utility.Serialization;

public class Bit16SerDes : CompositeSerDes<ushort, Bit16>
{
	public Bit16SerDes(ISerDes<ushort> serDes)
		: base(serDes)
	{
	}

	public static ISerDes<Bit16> Instance { get; } = new Bit16SerDes(SerDes.OfUShort);

	/// <inheritdoc />
	protected override Bit16 ComposeValue(ushort part) => new Bit16(part);

	/// <inheritdoc />
	protected override void DecomposeValue(Bit16 value, out ushort part)
	{
		part = value.Value;
	}
}
