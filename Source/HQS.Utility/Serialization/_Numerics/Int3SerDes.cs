﻿namespace HQS.Utility.Serialization;

public class Int3SerDes : CompositeSerDes<int, int, int, Int3>
{
	public Int3SerDes(ISerDes<int> serDes)
		: base(serDes, serDes, serDes)
	{
	}

	public Int3SerDes(ISerDes<int> xSerDes, ISerDes<int> ySerDes, ISerDes<int> zSerDes)
		: base(xSerDes, ySerDes, zSerDes)
	{
	}

	public static ISerDes<Int3> Instance { get; } = new Int3SerDes(SerDes.OfInt);

	/// <inheritdoc />
	protected override Int3 ComposeValue(int x, int y, int z) => new Int3(x, y, z);

	/// <inheritdoc />
	protected override void DecomposeValue(Int3 value, out int x, out int y, out int z)
	{
		x = value.X;
		y = value.Y;
		z = value.Z;
	}
}
