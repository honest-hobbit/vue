﻿namespace HQS.Utility.Serialization;

public class Int4SerDes : CompositeSerDes<int, int, int, int, Int4>
{
	public Int4SerDes(ISerDes<int> serDes)
		: base(serDes, serDes, serDes, serDes)
	{
	}

	public Int4SerDes(
		ISerDes<int> xSerDes, ISerDes<int> ySerDes, ISerDes<int> zSerDes, ISerDes<int> wSerDes)
		: base(xSerDes, ySerDes, zSerDes, wSerDes)
	{
	}

	public static ISerDes<Int4> Instance { get; } = new Int4SerDes(SerDes.OfInt);

	/// <inheritdoc />
	protected override Int4 ComposeValue(int x, int y, int z, int w) => new Int4(x, y, z, w);

	/// <inheritdoc />
	protected override void DecomposeValue(Int4 value, out int x, out int y, out int z, out int w)
	{
		x = value.X;
		y = value.Y;
		z = value.Z;
		w = value.W;
	}
}
