﻿namespace HQS.Utility.Serialization;

public class Bit8SerDes : CompositeSerDes<byte, Bit8>
{
	public Bit8SerDes(ISerDes<byte> serDes)
		: base(serDes)
	{
	}

	public static ISerDes<Bit8> Instance { get; } = new Bit8SerDes(SerDes.OfByte);

	/// <inheritdoc />
	protected override Bit8 ComposeValue(byte part) => new Bit8(part);

	/// <inheritdoc />
	protected override void DecomposeValue(Bit8 value, out byte part)
	{
		part = value.Value;
	}
}
