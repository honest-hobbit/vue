﻿namespace HQS.Utility.Serialization;

public static class IntXSerDes
{
	public static ISerDes<Int2> For2D => Int2SerDes.Instance;

	public static ISerDes<Int3> For3D => Int3SerDes.Instance;

	public static ISerDes<Int4> For4D => Int4SerDes.Instance;
}
