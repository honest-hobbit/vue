﻿namespace HQS.Utility.Serialization;

public class Bit64SerDes : CompositeSerDes<ulong, Bit64>
{
	public Bit64SerDes(ISerDes<ulong> serDes)
		: base(serDes)
	{
	}

	public static ISerDes<Bit64> Instance { get; } = new Bit64SerDes(SerDes.OfULong);

	/// <inheritdoc />
	protected override Bit64 ComposeValue(ulong part) => new Bit64(part);

	/// <inheritdoc />
	protected override void DecomposeValue(Bit64 value, out ulong part)
	{
		part = value.Value;
	}
}
