﻿namespace HQS.Utility.Serialization;

public class Bit256SerDes : CompositeSerDes<ulong, ulong, ulong, ulong, Bit256>
{
	public Bit256SerDes(ISerDes<ulong> serDes)
		: base(serDes, serDes, serDes, serDes)
	{
	}

	public static ISerDes<Bit256> Instance { get; } = new Bit256SerDes(SerDes.OfULong);

	/// <inheritdoc />
	protected override Bit256 ComposeValue(ulong part1, ulong part2, ulong part3, ulong part4) =>
		new Bit256(part1, part2, part3, part4);

	/// <inheritdoc />
	protected override void DecomposeValue(
		Bit256 value, out ulong part1, out ulong part2, out ulong part3, out ulong part4)
	{
		part1 = value.Value1;
		part2 = value.Value2;
		part3 = value.Value3;
		part4 = value.Value4;
	}
}
