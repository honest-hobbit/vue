﻿namespace HQS.Utility.Serialization;

public class Vector3SerDes : CompositeSerDes<float, float, float, Vector3>
{
	public Vector3SerDes(ISerDes<float> serDes)
		: base(serDes, serDes, serDes)
	{
	}

	public Vector3SerDes(ISerDes<float> xSerDes, ISerDes<float> ySerDes, ISerDes<float> zSerDes)
		: base(xSerDes, ySerDes, zSerDes)
	{
	}

	public static ISerDes<Vector3> Instance { get; } = new Vector3SerDes(SerDes.OfFloat);

	/// <inheritdoc />
	protected override Vector3 ComposeValue(float x, float y, float z) => new Vector3(x, y, z);

	/// <inheritdoc />
	protected override void DecomposeValue(Vector3 value, out float x, out float y, out float z)
	{
		x = value.X;
		y = value.Y;
		z = value.Z;
	}
}
