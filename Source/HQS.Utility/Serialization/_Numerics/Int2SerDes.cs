﻿namespace HQS.Utility.Serialization;

public class Int2SerDes : CompositeSerDes<int, int, Int2>
{
	public Int2SerDes(ISerDes<int> serDes)
		: base(serDes, serDes)
	{
	}

	public Int2SerDes(ISerDes<int> xSerDes, ISerDes<int> ySerDes)
		: base(xSerDes, ySerDes)
	{
	}

	public static ISerDes<Int2> Instance { get; } = new Int2SerDes(SerDes.OfInt);

	/// <inheritdoc />
	protected override Int2 ComposeValue(int x, int y) => new Int2(x, y);

	/// <inheritdoc />
	protected override void DecomposeValue(Int2 value, out int x, out int y)
	{
		x = value.X;
		y = value.Y;
	}
}
