﻿namespace HQS.Utility.Serialization;

public class Bit32SerDes : CompositeSerDes<uint, Bit32>
{
	public Bit32SerDes(ISerDes<uint> serDes)
		: base(serDes)
	{
	}

	public static ISerDes<Bit32> Instance { get; } = new Bit32SerDes(SerDes.OfUInt);

	/// <inheritdoc />
	protected override Bit32 ComposeValue(uint part) => new Bit32(part);

	/// <inheritdoc />
	protected override void DecomposeValue(Bit32 value, out uint part)
	{
		part = value.Value;
	}
}
