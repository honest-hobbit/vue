﻿namespace HQS.Utility.Serialization;

public class Vector2SerDes : CompositeSerDes<float, float, Vector2>
{
	public Vector2SerDes(ISerDes<float> serDes)
		: base(serDes, serDes)
	{
	}

	public Vector2SerDes(ISerDes<float> xSerDes, ISerDes<float> ySerDes)
		: base(xSerDes, ySerDes)
	{
	}

	public static ISerDes<Vector2> Instance { get; } = new Vector2SerDes(SerDes.OfFloat);

	/// <inheritdoc />
	protected override Vector2 ComposeValue(float x, float y) => new Vector2(x, y);

	/// <inheritdoc />
	protected override void DecomposeValue(Vector2 value, out float x, out float y)
	{
		x = value.X;
		y = value.Y;
	}
}
