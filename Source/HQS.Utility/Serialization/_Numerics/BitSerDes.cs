﻿namespace HQS.Utility.Serialization;

public static class BitSerDes
{
	public static ISerDes<Bit8> ForBit8 => Bit8SerDes.Instance;

	public static ISerDes<Bit16> ForBit16 => Bit16SerDes.Instance;

	public static ISerDes<Bit32> ForBit32 => Bit32SerDes.Instance;

	public static ISerDes<Bit64> ForBit64 => Bit64SerDes.Instance;

	public static ISerDes<Bit128> ForBit128 => Bit128SerDes.Instance;

	public static ISerDes<Bit256> ForBit256 => Bit256SerDes.Instance;
}
