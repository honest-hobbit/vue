﻿namespace HQS.Utility.Serialization;

public class Bit128SerDes : CompositeSerDes<ulong, ulong, Bit128>
{
	public Bit128SerDes(ISerDes<ulong> serDes)
		: base(serDes, serDes)
	{
	}

	public static ISerDes<Bit128> Instance { get; } = new Bit128SerDes(SerDes.OfULong);

	/// <inheritdoc />
	protected override Bit128 ComposeValue(ulong part1, ulong part2) => new Bit128(part1, part2);

	/// <inheritdoc />
	protected override void DecomposeValue(Bit128 value, out ulong part1, out ulong part2)
	{
		part1 = value.Value1;
		part2 = value.Value2;
	}
}
