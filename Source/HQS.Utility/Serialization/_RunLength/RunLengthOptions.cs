﻿namespace HQS.Utility.Serialization;

public class RunLengthOptions
{
	public CountSerDes CountSerDes { get; set; } = null;

	public CountSerDes RunLengthSerDes { get; set; } = null;
}
