﻿namespace HQS.Utility.Serialization;

public abstract class AbstractRunLengthCoder
{
	private readonly CountSerDes countSerDes;

	private readonly CountSerDes runLengthSerDes;

	public AbstractRunLengthCoder(RunLengthOptions options = null)
	{
		this.countSerDes = options?.CountSerDes ?? SerializeCount.AsInt;
		this.runLengthSerDes = options?.RunLengthSerDes ?? SerializeCount.AsSByte;

		Debug.Assert(this.countSerDes.MaxCount >= 1);
		Debug.Assert(this.runLengthSerDes.MaxCount >= 1);
		Debug.Assert(this.runLengthSerDes.MinCount <= -1);

		// a positive number indicates a run of 2 or more of the same value
		// 2 is added to runLength because a run of 0 or 1 is never used so the actual length is shifted down by 2
		this.MaxRepeatRunLength = ((long)this.runLengthSerDes.MaxCount + 2).ClampToInt();

		// a negative number indicates a run of 1 or more unique values
		this.MaxUniqueRunLength = (-(long)this.runLengthSerDes.MinCount).ClampToInt();
	}

	public int MaxRepeatRunLength { get; }

	public int MaxUniqueRunLength { get; }

	public int Count { get; private set; }

	public int RemainingCount { get; protected set; }

	public abstract bool IsStarted { get; }

	public virtual void Clear()
	{
		this.Count = 0;
		this.RemainingCount = 0;
	}

	protected void EncodeCount(int count, Action<byte> writeByte)
	{
		Debug.Assert(this.Count == 0);
		Debug.Assert(this.RemainingCount == 0);
		Debug.Assert(count >= 0);
		Debug.Assert(writeByte != null);

		this.countSerDes.Serialize(count, writeByte);
		this.Count = count;
		this.RemainingCount = count;
	}

	protected int DecodeCount(Func<byte> readByte)
	{
		Debug.Assert(this.Count == 0);
		Debug.Assert(this.RemainingCount == 0);
		Debug.Assert(readByte != null);

		int count = this.countSerDes.Deserialize(readByte);

		Debug.Assert(count >= 0);

		this.Count = count;
		this.RemainingCount = count;
		return count;
	}

	protected void EncodeRepeatRunLength(int runLength, Action<byte> writeByte)
	{
		Debug.Assert(this.IsStarted);
		Debug.Assert(runLength >= 1);
		Debug.Assert(runLength <= this.MaxRepeatRunLength);
		Debug.Assert(writeByte != null);

		// a positive number indicates a run of 2 or more of the same value
		// 2 is subtracted from runLength because a repeat run of 0 or 1 is never used
		// so the actual run length will be shifted up by 2 when decoded
		this.runLengthSerDes.Serialize(runLength - 2, writeByte);
	}

	protected void EncodeUniqueRunLength(int runLength, Action<byte> writeByte)
	{
		Debug.Assert(this.IsStarted);
		Debug.Assert(runLength >= 1);
		Debug.Assert(runLength <= this.MaxUniqueRunLength);
		Debug.Assert(writeByte != null);

		// a negative number indicates a run of 1 or more unique values
		this.runLengthSerDes.Serialize(-runLength, writeByte);
	}

	protected void DecodeRunLength(Func<byte> readByte, out int runLength, out bool isUnique)
	{
		Debug.Assert(this.IsStarted);
		Debug.Assert(readByte != null);

		runLength = this.runLengthSerDes.Deserialize(readByte);
		if (runLength >= 0)
		{
			// a positive number indicates a run of 2 or more of the same value
			// 2 is added to runLength because a repeat run of 0 or 1 is never used
			// so the actual run length was shifted down by 2 when encoded
			runLength += 2;
			isUnique = false;

			Debug.Assert(runLength <= this.MaxRepeatRunLength);
		}
		else
		{
			// a negative number indicates a run of 1 or more unique values
			runLength = -runLength;
			isUnique = true;

			Debug.Assert(runLength <= this.MaxUniqueRunLength);
		}

		Debug.Assert(runLength >= 1);
		Debug.Assert(runLength <= this.RemainingCount);
	}
}
