﻿namespace HQS.Utility.Serialization;

public class RunLengthDecoder<T> : AbstractRunLengthCoder
{
	private readonly IDeserializer<T> deserializer;

	private Func<byte> readByte;

	private int uniqueCount;

	public RunLengthDecoder(IDeserializer<T> deserializer, RunLengthOptions options = null)
		: base(options)
	{
		Debug.Assert(deserializer != null);

		this.deserializer = deserializer;
	}

	public override bool IsStarted => this.readByte != null;

	public override void Clear()
	{
		base.Clear();
		this.readByte = null;
		this.uniqueCount = 0;
	}

	public void StartDecoding(Func<byte> readByte)
	{
		Debug.Assert(!this.IsStarted);
		Debug.Assert(readByte != null);

		int count = this.DecodeCount(readByte);
		if (count > 0)
		{
			this.readByte = readByte;
		}
	}

	public bool TryDecodeNext(out int runLength, out T value)
	{
		if (this.RemainingCount == 0)
		{
			runLength = 0;
			value = default;
			return false;
		}
		else
		{
			this.DecodeNext(out runLength, out value);
			return true;
		}
	}

	public void DecodeNext(out int runLength, out T value)
	{
		Debug.Assert(this.IsStarted);

		if (this.uniqueCount > 0)
		{
			// decoding a run of unique values
			Debug.Assert(this.RemainingCount >= 1);

			this.uniqueCount--;
			this.RemainingCount--;
			runLength = 1;
			value = this.deserializer.Deserialize(this.readByte);
		}
		else
		{
			this.DecodeRunLength(this.readByte, out runLength, out bool isUnique);

			if (!isUnique)
			{
				this.RemainingCount -= runLength;
				value = this.deserializer.Deserialize(this.readByte);
			}
			else
			{
				// the first unique value is being returned now
				// the others will be returned by future calls to DecodeNext
				this.uniqueCount = runLength - 1;

				this.RemainingCount--;
				runLength = 1;
				value = this.deserializer.Deserialize(this.readByte);
			}
		}

		if (this.RemainingCount == 0)
		{
			this.Clear();
		}
	}
}
