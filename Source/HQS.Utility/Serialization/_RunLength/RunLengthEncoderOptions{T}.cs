﻿namespace HQS.Utility.Serialization;

public class RunLengthEncoderOptions<T> : RunLengthOptions
{
	private int capacity = HQS.Utility.Capacity.DefaultForCollections;

	public IEqualityComparer<T> Comparer { get; set; } = null;

	public int Capacity
	{
		get => this.capacity;
		set => this.capacity = value.ClampLower(0);
	}
}
