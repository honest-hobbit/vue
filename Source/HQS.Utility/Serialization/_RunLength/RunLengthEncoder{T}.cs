﻿namespace HQS.Utility.Serialization;

// TODO the use of Queue for uniqueValues could be replaced with a PagedList
// this might allow RunLengthEncodedStrategy to be simplified to not need IPools of encoders/decoders
public class RunLengthEncoder<T> : AbstractRunLengthCoder
{
	private readonly ISerializer<T> serializer;

	private readonly IEqualityComparer<T> comparer;

	private readonly Queue<T> uniqueValues;

	private T previousValue;

	private int previousRunLength;

	private Action<byte> writeByte;

	public RunLengthEncoder(ISerializer<T> serializer, RunLengthEncoderOptions<T> options = null)
		: base(options)
	{
		Debug.Assert(serializer != null);

		this.serializer = serializer;
		this.comparer = options?.Comparer ?? EqualityComparer<T>.Default;
		this.uniqueValues = new Queue<T>(options?.Capacity ?? Capacity.DefaultForCollections);
	}

	public override bool IsStarted => this.writeByte != null;

	public override void Clear()
	{
		base.Clear();
		this.writeByte = null;
		this.uniqueValues.Clear();
		this.previousValue = default;
		this.previousRunLength = 0;
	}

	public void StartEncoding(Action<byte> writeByte, int count)
	{
		Debug.Assert(!this.IsStarted);
		Debug.Assert(writeByte != null);
		Debug.Assert(count >= 0);

		this.EncodeCount(count, writeByte);
		if (count > 0)
		{
			this.writeByte = writeByte;
		}
	}

	public void EncodeNext(T value, int count = 1)
	{
		Debug.Assert(this.IsStarted);
		Debug.Assert(count >= 1);
		Debug.Assert(count <= this.RemainingCount);

		this.RemainingCount -= count;

		if (this.previousRunLength == 1)
		{
			// previous run is a single unique value
			if (this.comparer.Equals(value, this.previousValue))
			{
				// the next run of values is the same as the previous unique value so add them together
				this.previousRunLength += count;

				// a run of unique values is ending so serialize them out
				this.SerializeUnique();
			}
			else
			{
				// the previous unique value doesn't match the next value so add it to the unique run
				this.uniqueValues.Enqueue(this.previousValue);

				if (count >= 2)
				{
					// the next run is a run of repeated values so serialize the previous run of unique values
					this.SerializeUnique();
				}

				// store the next run of values as the previous for cmoparisons in the next call to EncodeNext
				this.previousValue = value;
				this.previousRunLength = count;
			}
		}
		else if (this.previousRunLength >= 2)
		{
			// previous run is a run of repeated values
			if (this.comparer.Equals(value, this.previousValue))
			{
				// the next run of values is the same as the previous so add them together
				this.previousRunLength += count;
			}
			else
			{
				// the next run of values doesn't match the previous so end the run of repeated values
				this.SerializeRepeat();

				if (count >= 2)
				{
					// When serializing a run of repeated values it is possible for a single value to be left over
					// because of this.MaxRepeatRunLength. If that happens the left over value becomes the start
					// of a run of unique values. If the next run of values is also a repeat run then this left over
					// run of unique values also immediately ends as a run of 1 unique value.
					this.SerializeUnique();
				}

				// store the next run of values as the previous for cmoparisons in the next call to EncodeNext
				this.previousRunLength = count;
				this.previousValue = value;
			}
		}
		else
		{
			// this is the first value being encoded
			this.previousRunLength = count;
			this.previousValue = value;
		}

		if (this.RemainingCount == 0)
		{
			// serialize any remaining runs of values before clearing
			this.SerializeRepeat();
			this.SerializeUnique();

			this.Clear();
		}
	}

	// if a value is left over because of this.MaxRepeatRunLength and because a repeat run must be of 2 or more values
	// the left over value is added to this.uniqueValues
	private void SerializeRepeat()
	{
		while (this.previousRunLength >= 2)
		{
			int runLength = this.previousRunLength.ClampUpper(this.MaxRepeatRunLength);
			this.previousRunLength -= runLength;

			this.EncodeRepeatRunLength(runLength, this.writeByte);
			this.serializer.Serialize(this.previousValue, this.writeByte);
		}

		if (this.previousRunLength == 1)
		{
			this.uniqueValues.Enqueue(this.previousValue);
			this.previousRunLength = 0;
		}

		Debug.Assert(this.previousRunLength == 0);
	}

	private void SerializeUnique()
	{
		while (this.uniqueValues.Count >= 1)
		{
			int uniqueRunLength = this.uniqueValues.Count.ClampUpper(this.MaxUniqueRunLength);

			this.EncodeUniqueRunLength(uniqueRunLength, this.writeByte);

			for (int count = 0; count < uniqueRunLength; count++)
			{
				this.serializer.Serialize(this.uniqueValues.Dequeue(), this.writeByte);
			}
		}

		Debug.Assert(this.uniqueValues.Count == 0);
	}
}
