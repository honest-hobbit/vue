﻿namespace HQS.Utility.Serialization;

public abstract class CompositeSerDes<T, TValue> : ISerDes<TValue>
{
	private readonly ISerDes<T> serDes;

	public CompositeSerDes(ISerDes<T> serDes)
	{
		Debug.Assert(serDes != null);

		this.serDes = serDes;
	}

	/// <inheritdoc />
	public void Serialize(TValue value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);

		this.DecomposeValue(value, out T part);

		this.serDes.Serialize(part, writeByte);
	}

	/// <inheritdoc />
	public TValue Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		T part1 = this.serDes.Deserialize(readByte);

		return this.ComposeValue(part1);
	}

	protected abstract TValue ComposeValue(T part);

	protected abstract void DecomposeValue(TValue value, out T part);
}
