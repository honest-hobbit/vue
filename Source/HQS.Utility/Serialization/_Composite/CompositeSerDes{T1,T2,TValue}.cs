﻿namespace HQS.Utility.Serialization;

public abstract class CompositeSerDes<T1, T2, TValue> : ISerDes<TValue>
{
	private readonly ISerDes<T1> serDes1;

	private readonly ISerDes<T2> serDes2;

	public CompositeSerDes(ISerDes<T1> serDes1, ISerDes<T2> serDes2)
	{
		Debug.Assert(serDes1 != null);
		Debug.Assert(serDes2 != null);

		this.serDes1 = serDes1;
		this.serDes2 = serDes2;
	}

	/// <inheritdoc />
	public void Serialize(TValue value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);

		this.DecomposeValue(value, out T1 part1, out T2 part2);

		this.serDes1.Serialize(part1, writeByte);
		this.serDes2.Serialize(part2, writeByte);
	}

	/// <inheritdoc />
	public TValue Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		T1 part1 = this.serDes1.Deserialize(readByte);
		T2 part2 = this.serDes2.Deserialize(readByte);

		return this.ComposeValue(part1, part2);
	}

	protected abstract TValue ComposeValue(T1 part1, T2 part2);

	protected abstract void DecomposeValue(TValue value, out T1 part1, out T2 part2);
}
