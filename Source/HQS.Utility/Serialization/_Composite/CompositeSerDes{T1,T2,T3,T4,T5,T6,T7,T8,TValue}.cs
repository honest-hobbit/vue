﻿namespace HQS.Utility.Serialization;

public abstract class CompositeSerDes<T1, T2, T3, T4, T5, T6, T7, T8, TValue> : ISerDes<TValue>
{
	private readonly ISerDes<T1> serDes1;

	private readonly ISerDes<T2> serDes2;

	private readonly ISerDes<T3> serDes3;

	private readonly ISerDes<T4> serDes4;

	private readonly ISerDes<T5> serDes5;

	private readonly ISerDes<T6> serDes6;

	private readonly ISerDes<T7> serDes7;

	private readonly ISerDes<T8> serDes8;

	public CompositeSerDes(
		ISerDes<T1> serDes1,
		ISerDes<T2> serDes2,
		ISerDes<T3> serDes3,
		ISerDes<T4> serDes4,
		ISerDes<T5> serDes5,
		ISerDes<T6> serDes6,
		ISerDes<T7> serDes7,
		ISerDes<T8> serDes8)
	{
		Debug.Assert(serDes1 != null);
		Debug.Assert(serDes2 != null);
		Debug.Assert(serDes3 != null);
		Debug.Assert(serDes4 != null);
		Debug.Assert(serDes5 != null);
		Debug.Assert(serDes6 != null);
		Debug.Assert(serDes7 != null);
		Debug.Assert(serDes8 != null);

		this.serDes1 = serDes1;
		this.serDes2 = serDes2;
		this.serDes3 = serDes3;
		this.serDes4 = serDes4;
		this.serDes5 = serDes5;
		this.serDes6 = serDes6;
		this.serDes7 = serDes7;
		this.serDes8 = serDes8;
	}

	/// <inheritdoc />
	public void Serialize(TValue value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);

		this.DecomposeValue(
			value, out T1 part1, out T2 part2, out T3 part3, out T4 part4, out T5 part5, out T6 part6, out T7 part7, out T8 part8);

		this.serDes1.Serialize(part1, writeByte);
		this.serDes2.Serialize(part2, writeByte);
		this.serDes3.Serialize(part3, writeByte);
		this.serDes4.Serialize(part4, writeByte);
		this.serDes5.Serialize(part5, writeByte);
		this.serDes6.Serialize(part6, writeByte);
		this.serDes7.Serialize(part7, writeByte);
		this.serDes8.Serialize(part8, writeByte);
	}

	/// <inheritdoc />
	public TValue Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		T1 part1 = this.serDes1.Deserialize(readByte);
		T2 part2 = this.serDes2.Deserialize(readByte);
		T3 part3 = this.serDes3.Deserialize(readByte);
		T4 part4 = this.serDes4.Deserialize(readByte);
		T5 part5 = this.serDes5.Deserialize(readByte);
		T6 part6 = this.serDes6.Deserialize(readByte);
		T7 part7 = this.serDes7.Deserialize(readByte);
		T8 part8 = this.serDes8.Deserialize(readByte);

		return this.ComposeValue(part1, part2, part3, part4, part5, part6, part7, part8);
	}

	protected abstract TValue ComposeValue(T1 part1, T2 part2, T3 part3, T4 part4, T5 part5, T6 part6, T7 part7, T8 part8);

	protected abstract void DecomposeValue(
		TValue value, out T1 part1, out T2 part2, out T3 part3, out T4 part4, out T5 part5, out T6 part6, out T7 part7, out T8 part8);
}
