﻿namespace HQS.Utility.Serialization;

public abstract class CompositeSerDes<T1, T2, T3, TValue> : ISerDes<TValue>
{
	private readonly ISerDes<T1> serDes1;

	private readonly ISerDes<T2> serDes2;

	private readonly ISerDes<T3> serDes3;

	public CompositeSerDes(ISerDes<T1> serDes1, ISerDes<T2> serDes2, ISerDes<T3> serDes3)
	{
		Debug.Assert(serDes1 != null);
		Debug.Assert(serDes2 != null);
		Debug.Assert(serDes3 != null);

		this.serDes1 = serDes1;
		this.serDes2 = serDes2;
		this.serDes3 = serDes3;
	}

	/// <inheritdoc />
	public void Serialize(TValue value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);

		this.DecomposeValue(value, out T1 part1, out T2 part2, out T3 part3);

		this.serDes1.Serialize(part1, writeByte);
		this.serDes2.Serialize(part2, writeByte);
		this.serDes3.Serialize(part3, writeByte);
	}

	/// <inheritdoc />
	public TValue Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		T1 part1 = this.serDes1.Deserialize(readByte);
		T2 part2 = this.serDes2.Deserialize(readByte);
		T3 part3 = this.serDes3.Deserialize(readByte);

		return this.ComposeValue(part1, part2, part3);
	}

	protected abstract TValue ComposeValue(T1 part1, T2 part2, T3 part3);

	protected abstract void DecomposeValue(TValue value, out T1 part1, out T2 part2, out T3 part3);
}
