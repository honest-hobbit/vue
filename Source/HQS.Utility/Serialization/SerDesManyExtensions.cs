﻿namespace HQS.Utility.Serialization;

public static class SerDesManyExtensions
{
	public static void SerializeMany<T>(
		this ISerializer<T> serializer, IEnumerable<T> values, Action<byte> writeByte)
	{
		Debug.Assert(serializer != null);
		Debug.Assert(values != null);
		Debug.Assert(writeByte != null);

		foreach (T value in values)
		{
			serializer.Serialize(value, writeByte);
		}
	}

	public static T[] DeserializeMany<T>(
		this IDeserializer<T> deserializer, int count, Func<byte> readByte)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(count >= 0);
		Debug.Assert(readByte != null);

		var result = new T[count];
		DeserializeManyInline(deserializer, count, readByte, result);
		return result;
	}

	public static void DeserializeManyInline<T>(
		this IDeserializer<T> deserializer, int count, Func<byte> readByte, T[] result)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(count >= 0);
		Debug.Assert(readByte != null);
		Debug.Assert(result != null);
		Debug.Assert(count <= result.Length);

		for (int currentCount = 0; currentCount < count; currentCount++)
		{
			result[currentCount] = deserializer.Deserialize(readByte);
		}
	}

	public static void DeserializeManyInline<T>(
		this IDeserializer<T> deserializer, int count, Func<byte> readByte, ICollection<T> result)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(count >= 0);
		Debug.Assert(readByte != null);
		Debug.Assert(result != null);

		for (int currentCount = 0; currentCount < count; currentCount++)
		{
			result.Add(deserializer.Deserialize(readByte));
		}
	}
}
