﻿namespace HQS.Utility.Serialization;

// Uses a variable numbers of bytes to serialize an unsigned integer using the technique described here;
// https://en.wikipedia.org/wiki/LEB128
// https://developers.google.com/protocol-buffers/docs/encoding#varints
public class UnsignedVarintSerDes :
	ISerDes<ulong>, ISerDes<uint>, ISerDes<ushort>, ISerDes<long>, ISerDes<int>, ISerDes<short>
{
	private const int BitsUsedPerByte = 7;

	private const ulong Last7BitsMask = 0b0111_1111UL;

	private const byte FirstBitMask = 0b1000_0000;

	private UnsignedVarintSerDes()
	{
	}

	public static UnsignedVarintSerDes Instance { get; } = new UnsignedVarintSerDes();

	/// <inheritdoc />
	public void Serialize(ulong value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);

		do
		{
			var nextByte = (byte)(value & Last7BitsMask);
			value >>= BitsUsedPerByte;
			if (value != 0)
			{
				nextByte |= FirstBitMask;
			}

			writeByte(nextByte);
		}
		while (value != 0);
	}

	/// <inheritdoc />
	public ulong Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		ulong result = 0;
		int shift = 0;
		while (true)
		{
			var nextByte = readByte();
			result |= (nextByte & Last7BitsMask) << shift;
			if ((nextByte & FirstBitMask) == 0)
			{
				return result;
			}

			shift += BitsUsedPerByte;
		}
	}

	/// <inheritdoc />
	public void Serialize(uint value, Action<byte> writeByte) => this.Serialize((ulong)value, writeByte);

	/// <inheritdoc />
	uint IDeserializer<uint>.Deserialize(Func<byte> readByte) => checked((uint)this.Deserialize(readByte));

	/// <inheritdoc />
	public void Serialize(ushort value, Action<byte> writeByte) => this.Serialize((ulong)value, writeByte);

	/// <inheritdoc />
	ushort IDeserializer<ushort>.Deserialize(Func<byte> readByte) => checked((ushort)this.Deserialize(readByte));

	/// <inheritdoc />
	public void Serialize(long value, Action<byte> writeByte) => this.Serialize(checked((ulong)value), writeByte);

	/// <inheritdoc />
	long IDeserializer<long>.Deserialize(Func<byte> readByte) => checked((long)this.Deserialize(readByte));

	/// <inheritdoc />
	public void Serialize(int value, Action<byte> writeByte) => this.Serialize(checked((ulong)value), writeByte);

	/// <inheritdoc />
	int IDeserializer<int>.Deserialize(Func<byte> readByte) => checked((int)this.Deserialize(readByte));

	/// <inheritdoc />
	public void Serialize(short value, Action<byte> writeByte) => this.Serialize(checked((ulong)value), writeByte);

	/// <inheritdoc />
	short IDeserializer<short>.Deserialize(Func<byte> readByte) => checked((short)this.Deserialize(readByte));
}
