﻿namespace HQS.Utility.Serialization;

public static class VarintSerDes
{
	public static SignedVarintSerDes ForSigned => SignedVarintSerDes.Instance;

	public static UnsignedVarintSerDes ForUnsigned => UnsignedVarintSerDes.Instance;
}
