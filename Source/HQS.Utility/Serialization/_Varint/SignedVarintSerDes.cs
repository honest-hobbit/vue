﻿namespace HQS.Utility.Serialization;

// Uses a variable numbers of bytes to serialize an signed integer using the technique described here;
// https://en.wikipedia.org/wiki/LEB128
// https://developers.google.com/protocol-buffers/docs/encoding#varints
// This utilizies the zig zag encoding of negative numbers to positive numbers as described here;
// https://gist.github.com/mfuerstenau/ba870a29e16536fdbaba
// https://developers.google.com/protocol-buffers/docs/encoding#types (More Value Types / Signed Integers)
public class SignedVarintSerDes : ISerDes<long>, ISerDes<int>, ISerDes<short>
{
	private SignedVarintSerDes()
	{
	}

	public static SignedVarintSerDes Instance { get; } = new SignedVarintSerDes();

	/// <inheritdoc />
	public void Serialize(long value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);

		UnsignedVarintSerDes.Instance.Serialize(ZigZagEncoding.OfLong.Convert(value), writeByte);
	}

	/// <inheritdoc />
	public long Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		return ZigZagEncoding.OfLong.Convert(UnsignedVarintSerDes.Instance.Deserialize(readByte));
	}

	/// <inheritdoc />
	public void Serialize(int value, Action<byte> writeByte) => this.Serialize(checked((long)value), writeByte);

	/// <inheritdoc />
	int IDeserializer<int>.Deserialize(Func<byte> readByte) => checked((int)this.Deserialize(readByte));

	/// <inheritdoc />
	public void Serialize(short value, Action<byte> writeByte) => this.Serialize(checked((long)value), writeByte);

	/// <inheritdoc />
	short IDeserializer<short>.Deserialize(Func<byte> readByte) => checked((short)this.Deserialize(readByte));
}
