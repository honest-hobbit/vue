﻿namespace HQS.Utility;

// TODO this is an absolute hack. Delete this once a better solution is available in modern .NET
[StructLayout(LayoutKind.Explicit, Size = 16)]
internal readonly struct DecimalParts
{
	[FieldOffset(0)]
	public readonly int Part0;

	[FieldOffset(4)]
	public readonly int Part1;

	[FieldOffset(8)]
	public readonly int Part2;

	[FieldOffset(12)]
	public readonly int Part3;

	[FieldOffset(0)]
	public readonly decimal Decimal;

	public DecimalParts(decimal value)
		: this()
	{
		this.Decimal = value;
	}

	public DecimalParts(int part0, int part1, int part2, int part3)
		: this()
	{
		this.Part0 = part0;
		this.Part1 = part1;
		this.Part2 = part2;
		this.Part3 = part3;
	}
}
