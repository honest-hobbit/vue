﻿namespace HQS.Utility.Serialization;

public class StringSerDes : EnumerableSerDes<char>, ISerDes<string>
{
	public StringSerDes(ISerDes<char> charSerDes, CountSerDes countSerDes = null)
		: base(EnumerableSerDesStrategy.UniqueValues(charSerDes, countSerDes))
	{
	}

	public StringSerDes(EnumerableSerDesStrategy<char, char> strategy)
		: base(strategy)
	{
	}

	public static ISerDes<string> LengthAsByte { get; } = new StringSerDes(SerDes.OfChar, SerializeCount.AsByte);

	public static ISerDes<string> LengthAsUShort { get; } = new StringSerDes(SerDes.OfChar, SerializeCount.AsUShort);

	public static ISerDes<string> LengthAsInt { get; } = new StringSerDes(SerDes.OfChar, SerializeCount.AsInt);

	public static ISerDes<string> LengthAsVarint { get; } = new StringSerDes(SerDes.OfChar, SerializeCount.AsUnsignedVarint);

	/// <inheritdoc />
	public void Serialize(string value, Action<byte> writeByte) => base.Serialize(value, writeByte);

	/// <inheritdoc />
	public new string Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		this.DeserializeValues(readByte, out int count, out var values);
		var result = new char[count];
		result.SetTo(values);
		return new string(result);
	}

	/// <inheritdoc />
	protected override int GetCount(IEnumerable<char> value)
	{
		if (value is string text)
		{
			return text.Length;
		}

		return base.GetCount(value);
	}
}
