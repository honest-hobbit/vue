﻿namespace HQS.Utility.Serialization;

public class NullableValueSerDes<T> : ISerDes<T?>
	where T : struct
{
	private readonly ISerDes<bool> hasValueSerDes;

	private readonly ISerDes<T> valueSerDes;

	public NullableValueSerDes(ISerDes<T> valueSerDes)
		: this(SerDes.OfBool, valueSerDes)
	{
	}

	public NullableValueSerDes(ISerDes<bool> hasValueSerDes, ISerDes<T> valueSerDes)
	{
		Ensure.That(hasValueSerDes, nameof(hasValueSerDes)).IsNotNull();
		Ensure.That(valueSerDes, nameof(valueSerDes)).IsNotNull();

		this.hasValueSerDes = hasValueSerDes;
		this.valueSerDes = valueSerDes;
	}

	public void Serialize(T? value, Action<byte> writeByte)
	{
		Ensure.That(writeByte, nameof(writeByte)).IsNotNull();

		if (value.HasValue)
		{
			this.hasValueSerDes.Serialize(true, writeByte);
			this.valueSerDes.Serialize(value.Value, writeByte);
		}
		else
		{
			this.hasValueSerDes.Serialize(false, writeByte);
		}
	}

	public T? Deserialize(Func<byte> readByte)
	{
		Ensure.That(readByte, nameof(readByte)).IsNotNull();

		if (this.hasValueSerDes.Deserialize(readByte))
		{
			return this.valueSerDes.Deserialize(readByte);
		}
		else
		{
			return null;
		}
	}
}
