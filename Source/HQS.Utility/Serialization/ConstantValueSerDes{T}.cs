﻿namespace HQS.Utility.Serialization;

/// <summary>
/// A serializer that doesn't actually serialize a value.
/// Instead it always returns the same constant value when deserializing.
/// </summary>
/// <typeparam name="T">The type of value to serialize.</typeparam>
public class ConstantValueSerDes<T> : ISerDes<T>
{
	private readonly IEqualityComparer<T> comparer;

	public ConstantValueSerDes(T constantValue, IEqualityComparer<T> comparer = null)
	{
		this.ConstantValue = constantValue;
		this.comparer = comparer ?? EqualityComparer<T>.Default;
	}

	public T ConstantValue { get; }

	/// <inheritdoc />
	public void Serialize(T value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);

		if (!this.comparer.Equals(value, this.ConstantValue))
		{
			throw new ArgumentException($"Value must be same as {this.ConstantValue}.", nameof(value));
		}
	}

	/// <inheritdoc />
	public T Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		return this.ConstantValue;
	}
}
