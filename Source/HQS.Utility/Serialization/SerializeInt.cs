﻿namespace HQS.Utility.Serialization;

public static class SerializeInt
{
	public static ISerDes<int> AsByte { get; } =
		ConverterSerDes.Create((int x) => checked((byte)x), (byte x) => x, SerDes.OfByte);

	public static ISerDes<int> AsSByte { get; } =
		ConverterSerDes.Create((int x) => checked((sbyte)x), (sbyte x) => x, SerDes.OfSByte);

	public static ISerDes<int> AsShort { get; } =
		ConverterSerDes.Create((int x) => checked((short)x), (short x) => x, SerDes.OfShort);

	public static ISerDes<int> AsUShort { get; } =
		ConverterSerDes.Create((int x) => checked((ushort)x), (ushort x) => x, SerDes.OfUShort);

	public static ISerDes<int> AsSignedVarint => VarintSerDes.ForSigned;

	public static ISerDes<int> AsUnsignedVarint => VarintSerDes.ForUnsigned;
}
