﻿namespace HQS.Utility.Serialization;

public class EnumerableSerDes<TSource, TResult> : ISerializer<IEnumerable<TSource>>, IDeserializer<IEnumerable<TResult>>
{
	private readonly EnumerableSerDesStrategy<TSource, TResult> strategy;

	public EnumerableSerDes(
		ISerializer<TSource> sourceSerializer, IDeserializer<TResult> resultDeserializer, CountSerDes countSerDes = null)
		: this(EnumerableSerDesStrategy.UniqueValues(sourceSerializer, resultDeserializer, countSerDes))
	{
	}

	public EnumerableSerDes(EnumerableSerDesStrategy<TSource, TResult> strategy)
	{
		Debug.Assert(strategy != null);

		this.strategy = strategy;
	}

	/// <inheritdoc />
	public void Serialize(IEnumerable<TSource> values, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(values, writeByte);

		this.strategy.SerializeValues(this.GetCount(values), values, writeByte);
	}

	/// <inheritdoc />
	public IEnumerable<TResult> Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		this.DeserializeValues(readByte, out int count, out var values);
		var result = new TResult[count];
		result.SetTo(values);
		return result;
	}

	// subclasses must force the eager evaluation of the returned enumerable once and only once
	protected void DeserializeValues(Func<byte> readByte, out int count, out IEnumerable<TResult> values)
	{
		IDeserializerContracts.Deserialize(readByte);

		values = this.strategy.DeserializeValues(out count, readByte);
	}

	protected virtual int GetCount(IEnumerable<TSource> values) => values.CountExtended();
}
