﻿namespace HQS.Utility.Serialization;

public abstract class EnumerableSerDesStrategy<TSource, TResult>
{
	public abstract void SerializeValues(int count, IEnumerable<TSource> source, Action<byte> writeByte);

	// must force the eager evaluation of the returned enumerable once and only once
	public abstract IEnumerable<TResult> DeserializeValues(out int count, Func<byte> readByte);
}
