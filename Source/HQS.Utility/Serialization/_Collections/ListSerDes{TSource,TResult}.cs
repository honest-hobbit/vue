﻿namespace HQS.Utility.Serialization;

public class ListSerDes<TSource, TResult> : ArraySerDes<TSource, TResult>,
	ISerializer<List<TSource>>, IDeserializer<List<TResult>>, IInlineDeserializer<List<TResult>>,
	ISerializer<IList<TSource>>, IDeserializer<IList<TResult>>, IInlineDeserializer<IList<TResult>>,
	ISerializer<ICollection<TSource>>, IDeserializer<ICollection<TResult>>, IInlineDeserializer<ICollection<TResult>>,
	ISerializer<IReadOnlyList<TSource>>, IDeserializer<IReadOnlyList<TResult>>,
	ISerializer<IReadOnlyCollection<TSource>>, IDeserializer<IReadOnlyCollection<TResult>>,
	ISerializer<IEnumerable<TSource>>, IDeserializer<IEnumerable<TResult>>
{
	public ListSerDes(
		ISerializer<TSource> sourceSerializer, IDeserializer<TResult> resultDeserializer, CountSerDes countSerDes = null)
		: base(sourceSerializer, resultDeserializer, countSerDes)
	{
	}

	public ListSerDes(EnumerableSerDesStrategy<TSource, TResult> strategy)
		: base(strategy)
	{
	}

	/// <inheritdoc />
	void ISerializer<IReadOnlyCollection<TSource>>.Serialize(IReadOnlyCollection<TSource> value, Action<byte> writeByte) => this.Serialize(value, writeByte);

	/// <inheritdoc />
	void ISerializer<IReadOnlyList<TSource>>.Serialize(IReadOnlyList<TSource> value, Action<byte> writeByte) => this.Serialize(value, writeByte);

	/// <inheritdoc />
	void ISerializer<ICollection<TSource>>.Serialize(ICollection<TSource> value, Action<byte> writeByte) => this.Serialize(value, writeByte);

	/// <inheritdoc />
	void ISerializer<IList<TSource>>.Serialize(IList<TSource> value, Action<byte> writeByte) => this.Serialize(value, writeByte);

	/// <inheritdoc />
	void ISerializer<List<TSource>>.Serialize(List<TSource> value, Action<byte> writeByte) => this.Serialize(value, writeByte);

	/// <inheritdoc />
	IEnumerable<TResult> IDeserializer<IEnumerable<TResult>>.Deserialize(Func<byte> readByte) => this.Deserialize(readByte);

	/// <inheritdoc />
	IReadOnlyCollection<TResult> IDeserializer<IReadOnlyCollection<TResult>>.Deserialize(Func<byte> readByte) => this.Deserialize(readByte);

	/// <inheritdoc />
	IReadOnlyList<TResult> IDeserializer<IReadOnlyList<TResult>>.Deserialize(Func<byte> readByte) => this.Deserialize(readByte);

	/// <inheritdoc />
	ICollection<TResult> IDeserializer<ICollection<TResult>>.Deserialize(Func<byte> readByte) => this.Deserialize(readByte);

	/// <inheritdoc />
	IList<TResult> IDeserializer<IList<TResult>>.Deserialize(Func<byte> readByte) => this.Deserialize(readByte);

	/// <inheritdoc />
	public new List<TResult> Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		this.DeserializeValues(readByte, out int count, out var values);
		var result = new List<TResult>(count);
		result.AddMany(values);
		return result;
	}

	/// <inheritdoc />
	[SuppressMessage("Design", "CA1061", Justification = "Deliberate hiding of method.")]
	public void DeserializeInline(Func<byte> readByte, ICollection<TResult> result)
	{
		IInlineDeserializerContracts.DeserializeInline(readByte, result);

		this.DeserializeValues(readByte, out int count, out var values);
		if (result is List<TResult> list)
		{
			list.EnsureCapacity(count);
		}

		result.AddMany(values);
	}

	/// <inheritdoc />
	void IInlineDeserializer<IList<TResult>>.DeserializeInline(Func<byte> readByte, IList<TResult> result) => this.DeserializeInline(readByte, result);

	/// <inheritdoc />
	public void DeserializeInline(Func<byte> readByte, List<TResult> result)
	{
		IInlineDeserializerContracts.DeserializeInline(readByte, result);

		this.DeserializeValues(readByte, out int count, out var values);
		result.EnsureCapacity(count);
		result.AddMany(values);
	}
}
