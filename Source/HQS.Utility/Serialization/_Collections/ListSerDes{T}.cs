﻿namespace HQS.Utility.Serialization;

public class ListSerDes<T> : ListSerDes<T, T>,
	ISerDes<T[]>, IInlineSerDes<T[]>,
	ISerDes<List<T>>, IInlineSerDes<List<T>>,
	ISerDes<IList<T>>, IInlineSerDes<IList<T>>,
	ISerDes<ICollection<T>>, IInlineSerDes<ICollection<T>>,
	ISerDes<IReadOnlyList<T>>,
	ISerDes<IReadOnlyCollection<T>>,
	ISerDes<IEnumerable<T>>
{
	public ListSerDes(ISerDes<T> serDes, CountSerDes countSerDes = null)
		: base(serDes, serDes, countSerDes)
	{
	}

	public ListSerDes(EnumerableSerDesStrategy<T, T> strategy)
		: base(strategy)
	{
	}
}
