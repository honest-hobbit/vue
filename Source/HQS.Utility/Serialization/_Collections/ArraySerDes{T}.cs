﻿namespace HQS.Utility.Serialization;

public class ArraySerDes<T> : ArraySerDes<T, T>,
	ISerDes<IEnumerable<T>>, ISerDes<T[]>, IInlineSerDes<T[]>, ISerDes<ReadOnlyArray<T>>
{
	public ArraySerDes(ISerDes<T> serDes, CountSerDes countSerDes = null)
		: base(serDes, serDes, countSerDes)
	{
	}

	public ArraySerDes(EnumerableSerDesStrategy<T, T> strategy)
		: base(strategy)
	{
	}
}
