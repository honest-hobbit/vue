﻿namespace HQS.Utility.Serialization;

public class CountSerDes : ISerDes<int>
{
	private readonly ISerDes<int> serializer;

	public CountSerDes(ISerDes<int> serializer, int minCount, int maxCount)
	{
		Debug.Assert(serializer != null);
		Debug.Assert(maxCount >= 1);
		Debug.Assert(minCount <= maxCount);

		this.serializer = serializer;
		this.MinCount = minCount;
		this.MaxCount = maxCount;
	}

	public int MinCount { get; }

	public int MaxCount { get; }

	/// <inheritdoc />
	public void Serialize(int value, Action<byte> writeByte)
	{
		Debug.Assert(value.IsIn(this.MinCount, this.MaxCount));

		this.serializer.Serialize(value, writeByte);
	}

	/// <inheritdoc />
	public int Deserialize(Func<byte> readByte)
	{
		int result = this.serializer.Deserialize(readByte);

		Debug.Assert(result.IsIn(this.MinCount, this.MaxCount));

		return result;
	}
}
