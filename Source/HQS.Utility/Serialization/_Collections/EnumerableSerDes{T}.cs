﻿namespace HQS.Utility.Serialization;

public class EnumerableSerDes<T> : EnumerableSerDes<T, T>, ISerDes<IEnumerable<T>>
{
	public EnumerableSerDes(ISerDes<T> serDes, CountSerDes countSerDes = null)
		: base(serDes, serDes, countSerDes)
	{
	}

	public EnumerableSerDes(EnumerableSerDesStrategy<T, T> strategy)
		: base(strategy)
	{
	}
}
