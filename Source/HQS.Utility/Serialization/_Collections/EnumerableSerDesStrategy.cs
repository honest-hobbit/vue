﻿namespace HQS.Utility.Serialization;

public static class EnumerableSerDesStrategy
{
	public static EnumerableSerDesStrategy<TSource, TResult> UniqueValues<TSource, TResult>(
		ISerializer<TSource> sourceSerializer,
		IDeserializer<TResult> resultDeserializer,
		CountSerDes countSerDes = null) =>
		new UniqueValuesStrategy<TSource, TResult>(sourceSerializer, resultDeserializer, countSerDes);

	public static EnumerableSerDesStrategy<T, T> UniqueValues<T>(
		ISerDes<T> serDes, CountSerDes countSerDes = null) =>
		UniqueValues(serDes, serDes, countSerDes);

	public static EnumerableSerDesStrategy<TSource, TResult> RunLengthEncoded<TSource, TResult>(
		IPool<RunLengthEncoder<TSource>> encoderPool,
		IPool<RunLengthDecoder<TResult>> decoderPool) =>
		new RunLengthEncodedStrategy<TSource, TResult>(encoderPool, decoderPool);

	public static EnumerableSerDesStrategy<TSource, TResult> RunLengthEncoded<TSource, TResult>(
		ISerializer<TSource> sourceSerializer,
		IDeserializer<TResult> resultDeserializer,
		RunLengthEncoderOptions<TSource> options = null) => RunLengthEncoded(
			new ThreadSafePool<RunLengthEncoder<TSource>>(
				() => new RunLengthEncoder<TSource>(sourceSerializer, options)),
			new ThreadSafePool<RunLengthDecoder<TResult>>(
				() => new RunLengthDecoder<TResult>(resultDeserializer, options)));

	public static EnumerableSerDesStrategy<T, T> RunLengthEncoded<T>(
		ISerDes<T> serDes, RunLengthEncoderOptions<T> options = null) => RunLengthEncoded(serDes, serDes, options);

	private class UniqueValuesStrategy<TSource, TResult> : EnumerableSerDesStrategy<TSource, TResult>
	{
		private readonly ISerializer<TSource> sourceSerializer;

		private readonly IDeserializer<TResult> resultDeserializer;

		private readonly CountSerDes countSerDes;

		public UniqueValuesStrategy(
			ISerializer<TSource> sourceSerializer,
			IDeserializer<TResult> resultDeserializer,
			CountSerDes countSerDes = null)
		{
			Debug.Assert(sourceSerializer != null);
			Debug.Assert(resultDeserializer != null);

			this.sourceSerializer = sourceSerializer;
			this.resultDeserializer = resultDeserializer;
			this.countSerDes = countSerDes ?? SerializeCount.AsInt;
		}

		/// <inheritdoc />
		public override void SerializeValues(int count, IEnumerable<TSource> source, Action<byte> writeByte)
		{
			Debug.Assert(count >= 0);
			Debug.Assert(source != null);
			Debug.Assert(writeByte != null);

			this.countSerDes.Serialize(count, writeByte);
			if (count == 0)
			{
				return;
			}

			int loopCount = 0;
			foreach (var value in source)
			{
				loopCount++;
				Debug.Assert(loopCount <= count);

				this.sourceSerializer.Serialize(value, writeByte);
			}

			Debug.Assert(loopCount == count);
		}

		/// <inheritdoc />
		public override IEnumerable<TResult> DeserializeValues(out int count, Func<byte> readByte)
		{
			Debug.Assert(readByte != null);

			count = this.countSerDes.Deserialize(readByte);
			return DeserializeValues(count);

			IEnumerable<TResult> DeserializeValues(int count)
			{
				for (int loopCount = 0; loopCount < count; loopCount++)
				{
					yield return this.resultDeserializer.Deserialize(readByte);
				}
			}
		}
	}

	private class RunLengthEncodedStrategy<TSource, TResult> : EnumerableSerDesStrategy<TSource, TResult>
	{
		private readonly IPool<RunLengthEncoder<TSource>> encoderPool;

		private readonly IPool<RunLengthDecoder<TResult>> decoderPool;

		public RunLengthEncodedStrategy(
			IPool<RunLengthEncoder<TSource>> encoderPool, IPool<RunLengthDecoder<TResult>> decoderPool)
		{
			Debug.Assert(encoderPool != null);
			Debug.Assert(decoderPool != null);

			this.encoderPool = encoderPool;
			this.decoderPool = decoderPool;
		}

		/// <inheritdoc />
		public override void SerializeValues(int count, IEnumerable<TSource> source, Action<byte> writeByte)
		{
			Debug.Assert(count >= 0);
			Debug.Assert(source != null);
			Debug.Assert(writeByte != null);

			var encoder = this.encoderPool.Rent();
			try
			{
				encoder.Clear();
				encoder.StartEncoding(writeByte, count);
				if (count == 0)
				{
					Debug.Assert(encoder.RemainingCount == 0);

					return;
				}

				foreach (var value in source)
				{
					encoder.EncodeNext(value, 1);
				}

				Debug.Assert(encoder.RemainingCount == 0);
			}
			finally
			{
				encoder.Clear();
				this.encoderPool.Return(encoder);
			}
		}

		/// <inheritdoc />
		public override IEnumerable<TResult> DeserializeValues(out int count, Func<byte> readByte)
		{
			Debug.Assert(readByte != null);

			var decoder = this.decoderPool.Rent();
			try
			{
				decoder.Clear();
				decoder.StartDecoding(readByte);
				count = decoder.Count;
			}
			catch (Exception)
			{
				decoder.Clear();
				this.decoderPool.Return(decoder);
				throw;
			}

			return this.DeserializeValues(decoder);
		}

		private IEnumerable<TResult> DeserializeValues(RunLengthDecoder<TResult> decoder)
		{
			Debug.Assert(decoder != null);

			try
			{
				while (decoder.TryDecodeNext(out int runLength, out var value))
				{
					for (int loopCount = 0; loopCount < runLength; loopCount++)
					{
						yield return value;
					}
				}
			}
			finally
			{
				decoder.Clear();
				this.decoderPool.Return(decoder);
			}
		}
	}
}
