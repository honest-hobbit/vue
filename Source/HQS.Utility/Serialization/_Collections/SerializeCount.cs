﻿namespace HQS.Utility.Serialization;

public static class SerializeCount
{
	public static CountSerDes AsByte { get; } = new CountSerDes(SerializeInt.AsByte, byte.MinValue, byte.MaxValue);

	public static CountSerDes AsSByte { get; } = new CountSerDes(SerializeInt.AsSByte, sbyte.MinValue, sbyte.MaxValue);

	public static CountSerDes AsShort { get; } = new CountSerDes(SerializeInt.AsShort, short.MinValue, short.MaxValue);

	public static CountSerDes AsUShort { get; } = new CountSerDes(SerializeInt.AsUShort, ushort.MinValue, ushort.MaxValue);

	public static CountSerDes AsInt { get; } = new CountSerDes(SerDes.OfInt, int.MinValue, int.MaxValue);

	public static CountSerDes AsSignedVarint { get; } = new CountSerDes(VarintSerDes.ForSigned, int.MinValue, int.MaxValue);

	public static CountSerDes AsUnsignedVarint { get; } = new CountSerDes(VarintSerDes.ForUnsigned, 0, int.MaxValue);

	public static CountSerDes AsConstant(int count)
	{
		Debug.Assert(count >= 0);

		return new CountSerDes(new ConstantValueSerDes<int>(count, EqualityComparer.ForStruct<int>()), count, count);
	}
}
