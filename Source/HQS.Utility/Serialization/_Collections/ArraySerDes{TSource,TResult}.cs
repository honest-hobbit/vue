﻿namespace HQS.Utility.Serialization;

public class ArraySerDes<TSource, TResult> : EnumerableSerDes<TSource, TResult>,
	ISerializer<TSource[]>,
	IDeserializer<TResult[]>,
	IInlineDeserializer<TResult[]>,
	ISerializer<ReadOnlyArray<TSource>>,
	IDeserializer<ReadOnlyArray<TResult>>
{
	public ArraySerDes(
		ISerializer<TSource> sourceSerializer, IDeserializer<TResult> resultDeserializer, CountSerDes countSerDes = null)
		: base(sourceSerializer, resultDeserializer, countSerDes)
	{
	}

	public ArraySerDes(EnumerableSerDesStrategy<TSource, TResult> strategy)
		: base(strategy)
	{
	}

	/// <inheritdoc />
	public void Serialize(TSource[] value, Action<byte> writeByte) => base.Serialize(value, writeByte);

	/// <inheritdoc />
	public new TResult[] Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		this.DeserializeValues(readByte, out int count, out var values);
		var result = new TResult[count];
		result.SetTo(values);
		return result;
	}

	/// <inheritdoc />
	public void DeserializeInline(Func<byte> readByte, TResult[] result)
	{
		IInlineDeserializerContracts.DeserializeInline(readByte, result);

		this.DeserializeValues(readByte, out int count, out var values);
		Debug.Assert(result.Length == count);
		result.SetTo(values);
	}

	/// <inheritdoc />
	public void Serialize(ReadOnlyArray<TSource> value, Action<byte> writeByte) => base.Serialize(value, writeByte);

	/// <inheritdoc />
	ReadOnlyArray<TResult> IDeserializer<ReadOnlyArray<TResult>>.Deserialize(Func<byte> readByte) =>
		new ReadOnlyArray<TResult>(this.Deserialize(readByte));
}
