﻿namespace HQS.Utility.Serialization;

public static class StreamExtensions
{
	public static byte? TryReadByte(this Stream stream)
	{
		Debug.Assert(stream != null);

		var result = stream.ReadByte();
		return result == -1 ? null : (byte?)result;
	}

	public static byte ReadByteAsByte(this Stream stream)
	{
		Debug.Assert(stream != null);

		var result = stream.ReadByte();
		if (result == -1)
		{
			throw new EndOfStreamException();
		}

		return (byte)result;
	}
}
