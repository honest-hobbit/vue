﻿namespace HQS.Utility.Serialization;

public static class ISerializerContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Serialize<T>(T value, Action<byte> writeByte)
	{
		Debug.Assert(value != null);
		Debug.Assert(writeByte != null);
	}
}
