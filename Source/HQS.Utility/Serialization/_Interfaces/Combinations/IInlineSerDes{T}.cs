﻿namespace HQS.Utility.Serialization;

public interface IInlineSerDes<T> : ISerializer<T>, IInlineDeserializer<T>
{
}
