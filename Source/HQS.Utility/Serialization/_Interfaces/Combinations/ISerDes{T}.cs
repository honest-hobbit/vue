﻿namespace HQS.Utility.Serialization;

public interface ISerDes<T> : ISerializer<T>, IDeserializer<T>
{
}
