﻿namespace HQS.Utility.Serialization;

public interface IInlineDeserializer<T>
{
	void DeserializeInline(Func<byte> readByte, T result);
}
