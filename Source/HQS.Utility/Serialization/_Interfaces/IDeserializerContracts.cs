﻿namespace HQS.Utility.Serialization;

public static class IDeserializerContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Deserialize(Func<byte> readByte)
	{
		Debug.Assert(readByte != null);
	}
}
