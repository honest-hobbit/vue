﻿namespace HQS.Utility.Serialization;

public static class IInlineDeserializerContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void DeserializeInline<T>(Func<byte> readByte, T result)
	{
		Debug.Assert(readByte != null);
		Debug.Assert(result != null);
	}
}
