﻿namespace HQS.Utility.Serialization;

public static class IByteBufferContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Serialize<T>(ISerializer<T> serializer, T value)
	{
		Debug.Assert(serializer != null);
		Debug.Assert(value != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Deserialize<T>(IDeserializer<T> deserializer, byte[] array)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(array != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result, byte[] array)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(result != null);
		Debug.Assert(array != null);
	}
}
