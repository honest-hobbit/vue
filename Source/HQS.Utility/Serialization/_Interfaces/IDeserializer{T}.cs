﻿namespace HQS.Utility.Serialization;

public interface IDeserializer<T>
{
	T Deserialize(Func<byte> readByte);
}
