﻿namespace HQS.Utility.Serialization;

public interface ISerializer<T>
{
	void Serialize(T value, Action<byte> writeByte);
}
