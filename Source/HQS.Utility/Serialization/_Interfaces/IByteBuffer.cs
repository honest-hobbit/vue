﻿namespace HQS.Utility.Serialization;

public interface IByteBuffer
{
	byte[] Serialize<T>(ISerializer<T> serializer, T value);

	T Deserialize<T>(IDeserializer<T> deserializer, byte[] array);

	void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result, byte[] array);
}
