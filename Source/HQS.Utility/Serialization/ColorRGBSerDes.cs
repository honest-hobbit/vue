﻿namespace HQS.Utility.Serialization;

public class ColorRGBSerDes : CompositeSerDes<byte, byte, byte, byte, ColorRGB>
{
	public ColorRGBSerDes(ISerDes<byte> serDes)
		: base(serDes, serDes, serDes, serDes)
	{
	}

	public ColorRGBSerDes(ISerDes<byte> colorSerDes, ISerDes<byte> alphaSerDes)
		: base(colorSerDes, colorSerDes, colorSerDes, alphaSerDes)
	{
	}

	public ColorRGBSerDes(
		ISerDes<byte> redSerDes, ISerDes<byte> greenSerDes, ISerDes<byte> blueSerDes, ISerDes<byte> alphaSerDes)
		: base(redSerDes, greenSerDes, blueSerDes, alphaSerDes)
	{
	}

	public static ISerDes<ColorRGB> IncludeAlpha { get; } = new RGBASerDes();

	public static ISerDes<ColorRGB> AlphaIs0 { get; } =
		new ColorRGBSerDes(SerDes.OfByte, new ConstantValueSerDes<byte>(0));

	public static ISerDes<ColorRGB> AlphaIs255 { get; } =
		new ColorRGBSerDes(SerDes.OfByte, new ConstantValueSerDes<byte>(255));

	/// <inheritdoc />
	protected override ColorRGB ComposeValue(byte r, byte g, byte b, byte a) => new ColorRGB(r, g, b, a);

	/// <inheritdoc />
	protected override void DecomposeValue(ColorRGB value, out byte r, out byte g, out byte b, out byte a)
	{
		r = value.R;
		g = value.G;
		b = value.B;
		a = value.A;
	}

	private class RGBASerDes : CompositeSerDes<uint, ColorRGB>
	{
		public RGBASerDes()
			: base(SerDes.OfUInt)
		{
		}

		/// <inheritdoc />
		protected override ColorRGB ComposeValue(uint value) => new ColorRGB(value);

		/// <inheritdoc />
		protected override void DecomposeValue(ColorRGB color, out uint value) => value = color.Value;
	}
}
