﻿namespace HQS.Utility.Serialization;

public enum LZ4Mode
{
	None,

	L00FAST,

	L03HC,

	L04HC,

	L05HC,

	L06HC,

	L07HC,

	L08HC,

	L09HC,

	L10OPT,

	L11OPT,

	L12MAX,
}
