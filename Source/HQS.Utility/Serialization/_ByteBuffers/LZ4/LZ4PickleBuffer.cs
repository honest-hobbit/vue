﻿namespace HQS.Utility.Serialization;

public class LZ4PickleBuffer : IByteBuffer
{
	private readonly ByteBufferDeserializer deserializeBuffer = new ByteBufferDeserializer();

	private readonly FastList<byte> dataBytes;

	private readonly Action<byte> writeDataByte;

	public LZ4PickleBuffer()
		: this(Length.OfLongestPrimitive.InBytes)
	{
	}

	public LZ4PickleBuffer(int capacity)
	{
		Debug.Assert(capacity >= 0);

		this.dataBytes = new FastList<byte>(capacity);
		this.writeDataByte = this.dataBytes.Add;
	}

	public LZ4Mode Mode { get; set; } = LZ4Mode.L00FAST;

	/// <inheritdoc />
	public byte[] Serialize<T>(ISerializer<T> serializer, T value)
	{
		IByteBufferContracts.Serialize(serializer, value);

		this.dataBytes.FastClear();
		serializer.Serialize(value, this.writeDataByte);

		return this.Mode == LZ4Mode.None ? this.dataBytes.ToArray() :
			LZ4Pickler.Pickle(this.dataBytes.Array, 0, this.dataBytes.Count, this.Mode.ToLevel());
	}

	/// <inheritdoc />
	public T Deserialize<T>(IDeserializer<T> deserializer, byte[] array)
	{
		IByteBufferContracts.Deserialize(deserializer, array);

		return this.deserializeBuffer.Deserialize(deserializer, this.CheckForDecompression(array));
	}

	/// <inheritdoc />
	public void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result, byte[] array)
	{
		IByteBufferContracts.DeserializeInline(deserializer, result, array);

		this.deserializeBuffer.DeserializeInline(deserializer, result, this.CheckForDecompression(array));
	}

	private byte[] CheckForDecompression(byte[] array)
	{
		Debug.Assert(array != null);

		return this.Mode == LZ4Mode.None ? array : LZ4Pickler.Unpickle(array);
	}
}
