﻿namespace HQS.Utility.Serialization;

[SuppressMessage("StyleCop", "SA1117", Justification = "Grouping of parameters by purpose.")]
public class LZ4ByteBuffer : IByteBuffer
{
	private const int MinimumLengthToCompress = 33;

	private readonly SimpleArrayBuilder<byte> serializeBuilder = new SimpleArrayBuilder<byte>();

	private readonly ByteBufferDeserializer deserializeBuffer = new ByteBufferDeserializer();

	private readonly CountSerDes lengthSerDes;

	private readonly FastList<byte> originalLength = new FastList<byte>(Length.OfInt.InBytes + 1);

	private readonly FastList<byte> compressedLength = new FastList<byte>(Length.OfInt.InBytes + 1);

	private readonly FastList<byte> originalData;

	private readonly FastList<byte> compressedData;

	private readonly Action<byte> writeOriginalData;

	private readonly Action<byte> writeOriginalLength;

	private readonly Action<byte> writeCompressedLength;

	public LZ4ByteBuffer()
		: this(SerializeCount.AsUnsignedVarint, Length.OfLongestPrimitive.InBytes)
	{
	}

	public LZ4ByteBuffer(CountSerDes countSerDes)
		: this(countSerDes, Length.OfLongestPrimitive.InBytes)
	{
	}

	public LZ4ByteBuffer(int capacity)
		: this(SerializeCount.AsUnsignedVarint, capacity)
	{
	}

	public LZ4ByteBuffer(CountSerDes countSerDes, int capacity)
	{
		Debug.Assert(countSerDes != null);
		Debug.Assert(capacity >= 0);

		this.lengthSerDes = countSerDes;
		this.originalData = new FastList<byte>(capacity);
		this.compressedData = new FastList<byte>(LZ4Codec.MaximumOutputSize(capacity));

		this.writeOriginalData = this.originalData.Add;
		this.writeOriginalLength = this.originalLength.Add;
		this.writeCompressedLength = this.compressedLength.Add;
	}

	public LZ4Mode Mode { get; set; } = LZ4Mode.L00FAST;

	/// <inheritdoc />
	public byte[] Serialize<T>(ISerializer<T> serializer, T value)
	{
		IByteBufferContracts.Serialize(serializer, value);

		this.originalData.FastClear();
		serializer.Serialize(value, this.writeOriginalData);

		if (this.Mode != LZ4Mode.None && this.originalData.Count >= MinimumLengthToCompress)
		{
			this.compressedData.FastClear();
			this.compressedData.EnsureCapacity(LZ4Codec.MaximumOutputSize(this.originalData.Count));

			int compressedLength = LZ4Codec.Encode(
				this.originalData.Array, 0, this.originalData.Count,
				this.compressedData.Array, 0, this.compressedData.Array.Length,
				this.Mode.ToLevel());

			if (compressedLength >= 0 && compressedLength < this.originalData.Count)
			{
				// data was successfully compressed and is smaller than the original data
				// using SetCount because LZ4Codec.Encode writes directly to Array instead of using Add
				this.compressedData.SetCount(compressedLength);
				this.originalLength.FastClear();
				this.compressedLength.FastClear();
				this.lengthSerDes.Serialize(this.originalData.Count, this.writeOriginalLength);
				this.lengthSerDes.Serialize(this.compressedData.Count, this.writeCompressedLength);

				this.serializeBuilder.StartArray(
					this.originalLength.Count + this.compressedLength.Count + this.compressedData.Count);
				this.serializeBuilder.CopyIn(this.originalLength.Array, 0, this.originalLength.Count);
				this.serializeBuilder.CopyIn(this.compressedLength.Array, 0, this.compressedLength.Count);
				this.serializeBuilder.CopyIn(this.compressedData.Array, 0, this.compressedData.Count);
				return this.serializeBuilder.GetArray();
			}
		}

		// data was not compressed because; Mode set to None, data length too short, or compression failed
		this.originalLength.FastClear();
		this.lengthSerDes.Serialize(this.originalData.Count, this.writeOriginalLength);

		this.serializeBuilder.StartArray((this.originalLength.Count * 2) + this.originalData.Count);
		this.serializeBuilder.CopyIn(this.originalLength.Array, 0, this.originalLength.Count);
		this.serializeBuilder.CopyIn(this.originalLength.Array, 0, this.originalLength.Count);
		this.serializeBuilder.CopyIn(this.originalData.Array, 0, this.originalData.Count);
		return this.serializeBuilder.GetArray();
	}

	/// <inheritdoc />
	public T Deserialize<T>(IDeserializer<T> deserializer, byte[] array)
	{
		IByteBufferContracts.Deserialize(deserializer, array);

		try
		{
			this.PrepareDataForDeserializing(array);
			return this.deserializeBuffer.Deserialize(deserializer);
		}
		finally
		{
			this.deserializeBuffer.Array = null;
		}
	}

	/// <inheritdoc />
	public void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result, byte[] array)
	{
		IByteBufferContracts.DeserializeInline(deserializer, result, array);

		try
		{
			this.PrepareDataForDeserializing(array);
			this.deserializeBuffer.DeserializeInline(deserializer, result);
		}
		finally
		{
			this.deserializeBuffer.Array = null;
		}
	}

	private void PrepareDataForDeserializing(byte[] array)
	{
		Debug.Assert(array != null);

		this.deserializeBuffer.Array = array;
		this.deserializeBuffer.Index = 0;

		int originalLength = this.deserializeBuffer.Deserialize(this.lengthSerDes);
		int compressedLength = this.deserializeBuffer.Deserialize(this.lengthSerDes);
		if (originalLength == compressedLength)
		{
			return;
		}

		this.originalData.FastClear();
		this.originalData.EnsureCapacity(originalLength);

		int bytesUsedByCounts = this.deserializeBuffer.Index;
		int result = LZ4Codec.Decode(
			array, bytesUsedByCounts, compressedLength,
			this.originalData.Array, 0, this.originalData.Array.Length);

		if (result < 0)
		{
			throw new SerializationException("Failed to decompress data using LZ4Codec.Decode.");
		}

		this.deserializeBuffer.Array = this.originalData.Array;
		this.deserializeBuffer.Index = 0;
	}
}
