﻿namespace HQS.Utility.Serialization;

internal static class LZ4ModeExtensions
{
	public static LZ4Level ToLevel(this LZ4Mode mode)
	{
		return mode switch
		{
			LZ4Mode.None => throw InvalidEnumArgument.CreateException(nameof(mode), mode),
			LZ4Mode.L00FAST => LZ4Level.L00_FAST,
			LZ4Mode.L03HC => LZ4Level.L03_HC,
			LZ4Mode.L04HC => LZ4Level.L04_HC,
			LZ4Mode.L05HC => LZ4Level.L05_HC,
			LZ4Mode.L06HC => LZ4Level.L06_HC,
			LZ4Mode.L07HC => LZ4Level.L07_HC,
			LZ4Mode.L08HC => LZ4Level.L08_HC,
			LZ4Mode.L09HC => LZ4Level.L09_HC,
			LZ4Mode.L10OPT => LZ4Level.L10_OPT,
			LZ4Mode.L11OPT => LZ4Level.L11_OPT,
			LZ4Mode.L12MAX => LZ4Level.L12_MAX,
			_ => throw InvalidEnumArgument.CreateException(nameof(mode), mode),
		};
	}
}
