﻿namespace HQS.Utility.Serialization;

public class ByteBuffer : IByteBuffer
{
	private readonly ByteBufferDeserializer deserializer = new ByteBufferDeserializer();

	private readonly ByteBufferSerializer serializer;

	public ByteBuffer()
		: this(Length.OfLongestPrimitive.InBytes)
	{
	}

	public ByteBuffer(int capacity)
	{
		Debug.Assert(capacity >= 0);

		this.serializer = new ByteBufferSerializer(capacity);
	}

	/// <inheritdoc />
	public byte[] Serialize<T>(ISerializer<T> serializer, T value)
	{
		IByteBufferContracts.Serialize(serializer, value);

		return this.serializer.Serialize(serializer, value);
	}

	/// <inheritdoc />
	public T Deserialize<T>(IDeserializer<T> deserializer, byte[] array)
	{
		IByteBufferContracts.Deserialize(deserializer, array);

		return this.deserializer.Deserialize(deserializer, array);
	}

	/// <inheritdoc />
	public void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result, byte[] array)
	{
		IByteBufferContracts.DeserializeInline(deserializer, result, array);

		this.deserializer.DeserializeInline(deserializer, result, array);
	}
}
