﻿namespace HQS.Utility.Serialization;

public class ByteBufferSerializer
{
	private readonly FastList<byte> bytes;

	private readonly Action<byte> writeByte;

	public ByteBufferSerializer()
		: this(Length.OfLongestPrimitive.InBytes)
	{
	}

	public ByteBufferSerializer(int capacity)
	{
		Debug.Assert(capacity >= 0);

		this.bytes = new FastList<byte>(capacity);
		this.writeByte = this.bytes.Add;
	}

	public byte[] Serialize<T>(ISerializer<T> serializer, T value)
	{
		Debug.Assert(serializer != null);
		Debug.Assert(value != null);

		this.bytes.FastClear();
		serializer.Serialize(value, this.writeByte);
		return this.bytes.ToArray();
	}
}
