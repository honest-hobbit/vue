﻿namespace HQS.Utility.Serialization;

public class ByteBufferDeserializer
{
	private readonly Func<byte> nextByte;

	public ByteBufferDeserializer()
	{
		this.nextByte = this.NextByte;
	}

	public byte[] Array { get; set; }

	public int Index { get; set; }

	public byte NextByte()
	{
		byte result = this.Array[this.Index];
		this.Index++;
		return result;
	}

	public T Deserialize<T>(IDeserializer<T> deserializer)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(this.Array != null);
		Debug.Assert(this.Array.IsIndexInBounds(this.Index));

		return deserializer.Deserialize(this.nextByte);
	}

	public void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(result != null);
		Debug.Assert(this.Array != null);
		Debug.Assert(this.Array.IsIndexInBounds(this.Index));

		deserializer.DeserializeInline(this.nextByte, result);
	}

	public T Deserialize<T>(IDeserializer<T> deserializer, byte[] array)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(array != null);

		this.Array = array;
		this.Index = 0;
		try
		{
			return deserializer.Deserialize(this.nextByte);
		}
		finally
		{
			this.Array = null;
			this.Index = 0;
		}
	}

	public void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result, byte[] array)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(result != null);
		Debug.Assert(array != null);

		this.Array = array;
		this.Index = 0;
		try
		{
			deserializer.DeserializeInline(this.nextByte, result);
		}
		finally
		{
			this.Array = null;
			this.Index = 0;
		}
	}
}
