﻿namespace HQS.Utility.Serialization;

public static class ByteBufferExtensions
{
	[ThreadStatic]
	private static ByteBufferSerializer serBuffer = null;

	[ThreadStatic]
	private static ByteBufferDeserializer desBuffer = null;

	private static ByteBufferSerializer SerBuffer => serBuffer ?? (serBuffer = new ByteBufferSerializer());

	private static ByteBufferDeserializer DesBuffer => desBuffer ?? (desBuffer = new ByteBufferDeserializer());

	public static byte[] Serialize<T>(this ISerializer<T> serializer, T value) =>
		SerBuffer.Serialize(serializer, value);

	public static T Deserialize<T>(this IDeserializer<T> deserializer, byte[] array) =>
		DesBuffer.Deserialize(deserializer, array);

	public static void DeserializeInline<T>(this IInlineDeserializer<T> deserializer, T result, byte[] array) =>
		DesBuffer.DeserializeInline(deserializer, result, array);
}
