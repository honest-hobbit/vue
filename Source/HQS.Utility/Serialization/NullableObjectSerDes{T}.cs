﻿namespace HQS.Utility.Serialization;

public class NullableObjectSerDes<T> : ISerDes<T>
	where T : class
{
	private readonly ISerDes<bool> hasObjectSerDes;

	private readonly ISerDes<T> objectSerDes;

	public NullableObjectSerDes(ISerDes<T> objectSerDes)
		: this(SerDes.OfBool, objectSerDes)
	{
	}

	public NullableObjectSerDes(ISerDes<bool> hasObjectSerDes, ISerDes<T> objectSerDes)
	{
		Ensure.That(hasObjectSerDes, nameof(hasObjectSerDes)).IsNotNull();
		Ensure.That(objectSerDes, nameof(objectSerDes)).IsNotNull();

		this.hasObjectSerDes = hasObjectSerDes;
		this.objectSerDes = objectSerDes;
	}

	public void Serialize(T value, Action<byte> writeByte)
	{
		Ensure.That(writeByte, nameof(writeByte)).IsNotNull();

		if (value != null)
		{
			this.hasObjectSerDes.Serialize(true, writeByte);
			this.objectSerDes.Serialize(value, writeByte);
		}
		else
		{
			this.hasObjectSerDes.Serialize(false, writeByte);
		}
	}

	public T Deserialize(Func<byte> readByte)
	{
		Ensure.That(readByte, nameof(readByte)).IsNotNull();

		if (this.hasObjectSerDes.Deserialize(readByte))
		{
			return this.objectSerDes.Deserialize(readByte);
		}
		else
		{
			return null;
		}
	}
}
