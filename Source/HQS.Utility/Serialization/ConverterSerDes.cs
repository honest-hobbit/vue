﻿namespace HQS.Utility.Serialization;

public static class ConverterSerDes
{
	public static ISerDes<TSource> Create<TSource, TResult>(
		Func<TSource, TResult> toResult, Func<TResult, TSource> toSource, ISerDes<TResult> serializer) =>
		new SerDes<TSource, TResult>(toResult, toSource, serializer);

	private class SerDes<TSource, TResult> : ISerDes<TSource>
	{
		private readonly Func<TSource, TResult> toResult;

		private readonly Func<TResult, TSource> toSource;

		private readonly ISerDes<TResult> serializer;

		public SerDes(Func<TSource, TResult> toResult, Func<TResult, TSource> toSource, ISerDes<TResult> serializer)
		{
			Debug.Assert(toResult != null);
			Debug.Assert(toSource != null);
			Debug.Assert(serializer != null);

			this.toResult = toResult;
			this.toSource = toSource;
			this.serializer = serializer;
		}

		/// <inheritdoc />
		public void Serialize(TSource value, Action<byte> writeByte) => this.serializer.Serialize(this.toResult(value), writeByte);

		/// <inheritdoc />
		public TSource Deserialize(Func<byte> readByte) => this.toSource(this.serializer.Deserialize(readByte));
	}
}
