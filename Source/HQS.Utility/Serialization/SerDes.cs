﻿namespace HQS.Utility.Serialization;

public static class SerDes
{
	public static ISerDes<bool> OfBool { get; } = new BoolSerializer();

	public static ISerDes<byte> OfByte { get; } = new ByteSerializer();

	public static ISerDes<sbyte> OfSByte { get; } = new SByteSerializer();

	public static ISerDes<short> OfShort { get; } = new ShortSerializer();

	public static ISerDes<ushort> OfUShort { get; } = new UShortSerializer();

	public static ISerDes<int> OfInt { get; } = new IntSerializer();

	public static ISerDes<uint> OfUInt { get; } = new UIntSerializer();

	public static ISerDes<long> OfLong { get; } = new LongSerializer();

	public static ISerDes<ulong> OfULong { get; } = new ULongSerializer();

	public static ISerDes<float> OfFloat { get; } = new FloatSerializer();

	public static ISerDes<double> OfDouble { get; } = new DoubleSerializer();

	public static ISerDes<decimal> OfDecimal { get; } = new DecimalSerializer(OfInt);

	public static ISerDes<Guid> OfGuid { get; } = new GuidSerializer();

	public static ISerDes<char> OfChar { get; } =
		ConverterSerDes.Create(x => x, x => (char)x, OfUShort);

	public static ISerDes<DateTime> OfDateTime { get; } =
		ConverterSerDes.Create(x => x.ToBinary(), x => DateTime.FromBinary(x), OfLong);

	public static ISerDes<TimeSpan> OfTimeSpan { get; } =
		ConverterSerDes.Create(x => x.Ticks, x => new TimeSpan(x), OfLong);

	#region Private Classes

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private static void WriteBytes(ReadOnlySpan<byte> bytes, Action<byte> writeByte)
	{
		int length = bytes.Length;
		for (int i = 0; i < length; i++)
		{
			writeByte(bytes[i]);
		}
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private static void ReadBytes(Span<byte> bytes, Func<byte> readByte)
	{
		int length = bytes.Length;
		for (int i = 0; i < length; i++)
		{
			bytes[i] = readByte();
		}
	}

	private class BoolSerializer : ISerDes<bool>
	{
		private const byte True = 1;

		private const byte False = 0;

		/// <inheritdoc />
		public void Serialize(bool value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			writeByte(value ? True : False);
		}

		/// <inheritdoc />
		public bool Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return readByte() switch
			{
				True => true,
				False => false,
				_ => throw new ArgumentOutOfRangeException(),
			};
		}
	}

	private class ByteSerializer : ISerDes<byte>
	{
		/// <inheritdoc />
		public void Serialize(byte value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			writeByte(value);
		}

		/// <inheritdoc />
		public byte Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return readByte();
		}
	}

	private class SByteSerializer : ISerDes<sbyte>
	{
		/// <inheritdoc />
		public void Serialize(sbyte value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			writeByte(new ByteUnion(value).Byte);
		}

		/// <inheritdoc />
		public sbyte Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return new ByteUnion(readByte()).SByte;
		}

		[StructLayout(LayoutKind.Explicit, Size = 1)]
		private readonly struct ByteUnion
		{
			[FieldOffset(0)]
			public readonly byte Byte;

			[FieldOffset(0)]
			public readonly sbyte SByte;

			public ByteUnion(byte value)
				: this()
			{
				this.Byte = value;
			}

			public ByteUnion(sbyte value)
				: this()
			{
				this.SByte = value;
			}
		}
	}

	private class ShortSerializer : ISerDes<short>
	{
		/// <inheritdoc />
		public void Serialize(short value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfShort.InBytes];
			BinaryPrimitives.WriteInt16LittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public short Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfShort.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitives.ReadInt16LittleEndian(bytes);
		}
	}

	private class UShortSerializer : ISerDes<ushort>
	{
		/// <inheritdoc />
		public void Serialize(ushort value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfUShort.InBytes];
			BinaryPrimitives.WriteUInt16LittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public ushort Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfUShort.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitives.ReadUInt16LittleEndian(bytes);
		}
	}

	private class IntSerializer : ISerDes<int>
	{
		/// <inheritdoc />
		public void Serialize(int value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfInt.InBytes];
			BinaryPrimitives.WriteInt32LittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public int Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfInt.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitives.ReadInt32LittleEndian(bytes);
		}
	}

	private class UIntSerializer : ISerDes<uint>
	{
		/// <inheritdoc />
		public void Serialize(uint value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfUInt.InBytes];
			BinaryPrimitives.WriteUInt32LittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public uint Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfUInt.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitives.ReadUInt32LittleEndian(bytes);
		}
	}

	private class LongSerializer : ISerDes<long>
	{
		/// <inheritdoc />
		public void Serialize(long value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfLong.InBytes];
			BinaryPrimitives.WriteInt64LittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public long Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfLong.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitives.ReadInt64LittleEndian(bytes);
		}
	}

	private class ULongSerializer : ISerDes<ulong>
	{
		/// <inheritdoc />
		public void Serialize(ulong value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfULong.InBytes];
			BinaryPrimitives.WriteUInt64LittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public ulong Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfULong.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitives.ReadUInt64LittleEndian(bytes);
		}
	}

	private class FloatSerializer : ISerDes<float>
	{
		/// <inheritdoc />
		public void Serialize(float value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfFloat.InBytes];
			BinaryPrimitivesUtility.WriteSingleLittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public float Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfFloat.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitivesUtility.ReadSingleLittleEndian(bytes);
		}
	}

	private class DoubleSerializer : ISerDes<double>
	{
		/// <inheritdoc />
		public void Serialize(double value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[Length.OfDouble.InBytes];
			BinaryPrimitivesUtility.WriteDoubleLittleEndian(bytes, value);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public double Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[Length.OfDouble.InBytes];
			ReadBytes(bytes, readByte);
			return BinaryPrimitivesUtility.ReadDoubleLittleEndian(bytes);
		}
	}

	private class DecimalSerializer : ISerDes<decimal>
	{
		private readonly ISerDes<int> partSerDes;

		public DecimalSerializer(ISerDes<int> partSerDes)
		{
			Debug.Assert(partSerDes != null);

			this.partSerDes = partSerDes;
		}

		/// <inheritdoc />
		public void Serialize(decimal value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			var parts = new DecimalParts(value);
			this.partSerDes.Serialize(parts.Part0, writeByte);
			this.partSerDes.Serialize(parts.Part1, writeByte);
			this.partSerDes.Serialize(parts.Part2, writeByte);
			this.partSerDes.Serialize(parts.Part3, writeByte);
		}

		/// <inheritdoc />
		public decimal Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			return new DecimalParts(
				this.partSerDes.Deserialize(readByte),
				this.partSerDes.Deserialize(readByte),
				this.partSerDes.Deserialize(readByte),
				this.partSerDes.Deserialize(readByte)).Decimal;
		}
	}

	private class GuidSerializer : ISerDes<Guid>
	{
		/// <inheritdoc />
		public void Serialize(Guid value, Action<byte> writeByte)
		{
			ISerializerContracts.Serialize(value, writeByte);

			Span<byte> bytes = stackalloc byte[16];
			bool success = value.TryWriteBytes(bytes);
			Debug.Assert(success);
			WriteBytes(bytes, writeByte);
		}

		/// <inheritdoc />
		public Guid Deserialize(Func<byte> readByte)
		{
			IDeserializerContracts.Deserialize(readByte);

			Span<byte> bytes = stackalloc byte[16];
			ReadBytes(bytes, readByte);
			return new Guid(bytes);
		}
	}

	#endregion
}
