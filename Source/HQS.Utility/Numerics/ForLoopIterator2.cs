﻿namespace HQS.Utility.Numerics;

public abstract class ForLoopIterator2
{
	public virtual void InvokeLoop(Int2 start, Int2 dimensions)
	{
		Debug.Assert(dimensions.X >= 0);
		Debug.Assert(dimensions.Y >= 0);

		if (dimensions.X == 0 || dimensions.Y == 0)
		{
			this.None();
			return;
		}

		if (dimensions.X == 1)
		{
			if (dimensions.Y == 1)
			{
				this.Single(start);
				return;
			}

			this.LineVerticalStartLow(start);
			this.LineVertical(new Int2(start.X, start.Y + 1), dimensions.Y - 2);
			this.LineVerticalEndHigh(new Int2(start.X, start.Y + dimensions.Y - 1));
			return;
		}

		if (dimensions.Y == 1)
		{
			this.LineHorizontalStartLow(start);
			this.LineHorizontal(new Int2(start.X + 1, start.Y), dimensions.X - 2);
			this.LineHorizontalEndHigh(new Int2(start.X + dimensions.X - 1, start.Y));
			return;
		}

		var max = start + dimensions - new Int2(1);

		this.CornerLowXLowY(start);
		this.CornerLowXHighY(new Int2(start.X, max.Y));
		this.CornerHighXLowY(new Int2(max.X, start.Y));
		this.CornerHighXHighY(max);

		dimensions -= new Int2(2);
		int x = start.X + 1;
		int y = start.Y + 1;

		this.EdgeHorizontalLowY(new Int2(x, start.Y), dimensions.X);
		this.EdgeHorizontalHighY(new Int2(x, max.Y), dimensions.X);
		this.EdgeVerticalLowX(new Int2(start.X, y), dimensions.Y);
		this.EdgeVerticalHighX(new Int2(max.X, y), dimensions.Y);

		this.Inside(new Int2(x, y), dimensions);
	}

	protected abstract void None();

	protected abstract void Single(Int2 index);

	protected abstract void LineHorizontalStartLow(Int2 index);

	protected abstract void LineHorizontalEndHigh(Int2 index);

	protected abstract void LineHorizontal(Int2 start, int lengthX);

	protected abstract void LineVerticalStartLow(Int2 index);

	protected abstract void LineVerticalEndHigh(Int2 index);

	protected abstract void LineVertical(Int2 start, int lengthY);

	protected abstract void CornerLowXLowY(Int2 index);

	protected abstract void CornerLowXHighY(Int2 index);

	protected abstract void CornerHighXLowY(Int2 index);

	protected abstract void CornerHighXHighY(Int2 index);

	protected abstract void EdgeHorizontalLowY(Int2 start, int lengthX);

	protected abstract void EdgeHorizontalHighY(Int2 start, int lengthX);

	protected abstract void EdgeVerticalLowX(Int2 start, int lengthY);

	protected abstract void EdgeVerticalHighX(Int2 start, int lengthY);

	protected abstract void Inside(Int2 start, Int2 dimensions);
}
