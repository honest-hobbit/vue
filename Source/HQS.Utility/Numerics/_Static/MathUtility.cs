﻿namespace HQS.Utility.Numerics;

/// <summary>
/// Provides additional constants and static methods for trigonometric, logarithmic, and other common mathematical functions
/// to be used along with the <see cref="Math"/> class.
/// </summary>
public static class MathUtility
{
	public const double DegreesPerRadian = Math.PI / 180d;

	public const double RadiansPerDegrees = 180d / Math.PI;

	private const float FloatNormal = (1 << 23) * float.Epsilon;

	private const double DoubleNormal = (1L << 52) * double.Epsilon;

	public static int CountDigits(int value)
	{
		if (value == 0)
		{
			return 0;
		}

		return (int)Math.Floor(Math.Log10(value.Abs()) + 1);
	}

	// round up means towards the greater number
	// round down means towards the lesser number
	// this applies for negatives as well
	public static int IntegerMidpoint(int value1, int value2, bool roundUp = false)
	{
		int sum = value1 + value2;
		var result = sum / 2;

		if (sum.IsOdd())
		{
			if (sum > 0)
			{
				if (roundUp)
				{
					result++;
				}
			}
			else
			{
				if (!roundUp)
				{
					result--;
				}
			}
		}

		return result;
	}

	#region Powers

	public static int NearestEncompassingPowerOf2(int number)
	{
		var result = 0b1;
		while (number > result)
		{
			result <<= 1;
		}

		return result;
	}

	public static bool IsPerfectSquare(int number)
	{
		Debug.Assert(number >= 0);

		return Math.Sqrt(number) % 1 == 0;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static int PowerOf2(int exponent)
	{
		Debug.Assert(exponent >= 0);

		return 1 << exponent;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static long LongPowerOf2(int exponent)
	{
		Debug.Assert(exponent >= 0);

		return 1L << exponent;
	}

	public static int IntegerPower(int baseNumber, int exponent)
	{
		Debug.Assert(exponent >= 0);

		int result = 1;
		for (int count = 0; count < exponent; count++)
		{
			result *= baseNumber;
		}

		return result;
	}

	public static long LongPower(long baseNumber, int exponent)
	{
		Debug.Assert(exponent >= 0);

		long result = 1;
		for (int count = 0; count < exponent; count++)
		{
			result *= baseNumber;
		}

		return result;
	}

	public static ulong ULongPower(ulong baseNumber, int exponent)
	{
		Debug.Assert(exponent >= 0);

		ulong result = 1;
		for (int count = 0; count < exponent; count++)
		{
			result *= baseNumber;
		}

		return result;
	}

	#endregion

	#region Divide

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static int DivideRoundUp(int value, int divisor)
	{
		Debug.Assert(value >= 0);
		Debug.Assert(divisor >= 1);

		return (value + divisor - 1) / divisor;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static long DivideRoundUp(long value, long divisor)
	{
		Debug.Assert(value >= 0);
		Debug.Assert(divisor >= 1);

		return (value + divisor - 1) / divisor;
	}

	#endregion

	#region ActualModulo

	/// <summary>
	/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
	/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="mod">The modulo to divide by.</param>
	/// <returns>The result of the modulo division.</returns>
	/// <remarks>
	/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
	/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
	/// for more information about modulo in C#.
	/// </remarks>
	public static int ActualModulo(int value, int mod)
	{
		Debug.Assert(mod != 0);

		mod = Math.Abs(mod);
		int result = value % mod;
		return result < 0 ? result + mod : result;
	}

	/// <summary>
	/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
	/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="mod">The modulo to divide by.</param>
	/// <returns>The result of the modulo division.</returns>
	/// <remarks>
	/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
	/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
	/// for more information about modulo in C#.
	/// </remarks>
	public static long ActualModulo(long value, long mod)
	{
		Debug.Assert(mod != 0);

		mod = Math.Abs(mod);
		long result = value % mod;
		return result < 0 ? result + mod : result;
	}

	/// <summary>
	/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
	/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="mod">The modulo to divide by.</param>
	/// <returns>The result of the modulo division.</returns>
	/// <remarks>
	/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
	/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
	/// for more information about modulo in C#.
	/// </remarks>
	public static float ActualModulo(float value, float mod)
	{
		Debug.Assert(mod != 0);

		mod = Math.Abs(mod);
		float result = value % mod;
		return result < 0 ? result + mod : result;
	}

	/// <summary>
	/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
	/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="mod">The modulo to divide by.</param>
	/// <returns>The result of the modulo division.</returns>
	/// <remarks>
	/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
	/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
	/// for more information about modulo in C#.
	/// </remarks>
	public static double ActualModulo(double value, double mod)
	{
		Debug.Assert(mod != 0);

		mod = Math.Abs(mod);
		double result = value % mod;
		return result < 0 ? result + mod : result;
	}

	/// <summary>
	/// Calculates the actual modulo value. This is not the same as the % operator, because the % operator is not
	/// actually modulus, it's the remainder. The two functions differ in how they handle negative numbers.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="mod">The modulo to divide by.</param>
	/// <returns>The result of the modulo division.</returns>
	/// <remarks>
	/// See <see href="http://stackoverflow.com/questions/10065080/mod-explanation">this</see> and
	/// <see href="http://stackoverflow.com/questions/1082917/mod-of-negative-number-is-melting-my-brain">this</see>
	/// for more information about modulo in C#.
	/// </remarks>
	public static decimal ActualModulo(decimal value, decimal mod)
	{
		Debug.Assert(mod != 0);

		mod = Math.Abs(mod);
		decimal result = value % mod;
		return result < 0 ? result + mod : result;
	}

	#endregion

	#region Degrees/Radians

	public static double DegreesToRadians(double degrees) => degrees * DegreesPerRadian;

	public static double RadiansToDegrees(double radians) => radians * RadiansPerDegrees;

	#endregion

	#region AreSimiliar

	// https://stackoverflow.com/questions/3874627/floating-point-comparison-functions-for-c-sharp
	// https://floating-point-gui.de/errors/comparison/
	public static bool AreSimiliar(float a, float b, float epsilon = .00001f)
	{
		Ensure.That(epsilon, nameof(epsilon)).IsGt(0);

		if (a == b)
		{
			// shortcut, handles infinities
			return true;
		}

		float diff = Math.Abs(a - b);

		if (a == 0.0f || b == 0.0f || diff < FloatNormal)
		{
			// a or b is zero, or both are extremely close to it.
			// relative error is less meaningful here
			return diff < (epsilon * FloatNormal);
		}

		// use relative error
		return diff / Math.Min(Math.Abs(a) + Math.Abs(b), float.MaxValue) < epsilon;
	}

	// https://stackoverflow.com/questions/3874627/floating-point-comparison-functions-for-c-sharp
	// https://floating-point-gui.de/errors/comparison/
	public static bool AreSimiliar(double a, double b, double epsilon = .00001)
	{
		Ensure.That(epsilon, nameof(epsilon)).IsGt(0);

		if (a == b)
		{
			// shortcut, handles infinities
			return true;
		}

		double diff = Math.Abs(a - b);

		if (a == 0.0 || b == 0.0 || diff < DoubleNormal)
		{
			// a or b is zero, or both are extremely close to it.
			// relative error is less meaningful here
			return diff < (epsilon * DoubleNormal);
		}

		// use relative error
		return diff / Math.Min(Math.Abs(a) + Math.Abs(b), double.MaxValue) < epsilon;
	}

	#endregion
}
