﻿namespace HQS.Utility.Numerics;

public static class VectorUtility
{
	public static float Slope(Vector2 a, Vector2 b) => (a.Y - b.Y) / (a.X - b.X);

	public static float Cross(Vector2 a, Vector2 b) => (a.X * b.Y) - (a.Y * b.X);
}
