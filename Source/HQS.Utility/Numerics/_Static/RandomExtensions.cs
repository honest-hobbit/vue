﻿namespace HQS.Utility.Numerics;

public static class RandomExtensions
{
	public static double NextDouble(this Random random, double maxValue)
	{
		Debug.Assert(random != null);
		Debug.Assert(maxValue >= 0);

		return random.NextDouble() * maxValue;
	}

	public static double NextDouble(this Random random, double minValue, double maxValue)
	{
		Debug.Assert(random != null);
		Debug.Assert(minValue <= maxValue);

		return (random.NextDouble() * (maxValue - minValue)) + minValue;
	}

	public static bool NextBool(this Random random)
	{
		Debug.Assert(random != null);

		return random.Next(0, 2) == 0 ? false : true;
	}

	public static bool NextChanceOutcome(this Random random, double chance)
	{
		Debug.Assert(random != null);
		Debug.Assert(chance >= 0 && chance <= 1);

		return chance switch
		{
			0 => false,
			1 => true,
			_ => random.NextDouble() < chance,
		};
	}

	/// <summary>
	/// Returns a random long from min (inclusive) to max (exclusive).
	/// </summary>
	/// <param name="random">The given random instance.</param>
	/// <param name="min">The inclusive minimum bound.</param>
	/// <param name="max">The exclusive maximum bound.  Must be greater than min.</param>
	/// <returns>A random number.</returns>
	public static long NextLong(this Random random, long min, long max)
	{
		Debug.Assert(random != null);
		Debug.Assert(min < max);

		var buffer = ByteBuffer.Instance;

		// Working with ulong so that modulo works correctly with values > long.MaxValue
		ulong uRange = (ulong)(max - min);

		// Prevent a modolo bias; see https://stackoverflow.com/a/10984975/238419
		// for more information.
		// In the worst case, the expected number of calls is 2 (though usually it's
		// much closer to 1) so this loop doesn't really hurt performance at all.
		ulong ulongRand;
		do
		{
			random.NextBytes(buffer);
			ulongRand = (ulong)BitConverter.ToInt64(buffer, 0);
		}
		while (ulongRand > ulong.MaxValue - (((ulong.MaxValue % uRange) + 1) % uRange));

		return (long)(ulongRand % uRange) + min;
	}

	/// <summary>
	/// Returns a random long from 0 (inclusive) to max (exclusive).
	/// </summary>
	/// <param name="random">The given random instance.</param>
	/// <param name="max">The exclusive maximum bound. Must be greater than 0.</param>
	/// <returns>A random number.</returns>
	public static long NextLong(this Random random, long max) => random.NextLong(0, max);

	/// <summary>
	/// Returns a random long over all possible values of long (except long.MaxValue, similar to
	/// random.Next()).
	/// </summary>
	/// <param name="random">The given random instance.</param>
	/// <returns>A random number.</returns>
	public static long NextLong(this Random random) => random.NextLong(long.MinValue, long.MaxValue);

	private static class ByteBuffer
	{
		[ThreadStatic]
		private static byte[] buffer = null;

		public static byte[] Instance => buffer ?? (buffer = new byte[8]);
	}
}
