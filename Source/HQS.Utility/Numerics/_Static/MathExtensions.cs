﻿namespace HQS.Utility.Numerics;

public static class MathExtensions
{
	#region Absolute Values

	public static float Abs(this float value) => Math.Abs(value);

	public static double Abs(this double value) => Math.Abs(value);

	public static decimal Abs(this decimal value) => Math.Abs(value);

	public static sbyte Abs(this sbyte value) => Math.Abs(value);

	public static short Abs(this short value) => Math.Abs(value);

	public static int Abs(this int value) => Math.Abs(value);

	public static long Abs(this long value) => Math.Abs(value);

	#endregion

	#region Clamp To

	public static byte ClampToByte(this int value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

	public static byte ClampToByte(this long value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

	public static byte ClampToByte(this float value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

	public static byte ClampToByte(this double value) => (byte)value.Clamp(byte.MinValue, byte.MaxValue);

	public static sbyte ClampToSByte(this int value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

	public static sbyte ClampToSByte(this long value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

	public static sbyte ClampToSByte(this float value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

	public static sbyte ClampToSByte(this double value) => (sbyte)value.Clamp(sbyte.MinValue, sbyte.MaxValue);

	public static short ClampToShort(this int value) => (short)value.Clamp(short.MinValue, short.MaxValue);

	public static short ClampToShort(this long value) => (short)value.Clamp(short.MinValue, short.MaxValue);

	public static short ClampToShort(this float value) => (short)value.Clamp(short.MinValue, short.MaxValue);

	public static short ClampToShort(this double value) => (short)value.Clamp(short.MinValue, short.MaxValue);

	public static ushort ClampToUShort(this int value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

	public static ushort ClampToUShort(this long value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

	public static ushort ClampToUShort(this float value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

	public static ushort ClampToUShort(this double value) => (ushort)value.Clamp(ushort.MinValue, ushort.MaxValue);

	public static int ClampToInt(this long value) => (int)value.Clamp(int.MinValue, int.MaxValue);

	public static int ClampToInt(this float value) => (int)value.Clamp(int.MinValue, int.MaxValue);

	public static int ClampToInt(this double value) => (int)value.Clamp(int.MinValue, int.MaxValue);

	public static uint ClampToUInt(this long value) => (uint)value.Clamp(uint.MinValue, uint.MaxValue);

	public static uint ClampToUInt(this float value) => (uint)value.Clamp(uint.MinValue, uint.MaxValue);

	public static uint ClampToUInt(this double value) => (uint)value.Clamp(uint.MinValue, uint.MaxValue);

	#endregion

	#region Comparable

	/// <summary>
	/// Clamps the value to be no greater than the maximum value.
	/// </summary>
	/// <typeparam name="T">The type of the value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="max">The maximum value.</param>
	/// <returns>The initial value if it is less than the maximum value; otherwise the maximum value.</returns>
	public static T ClampUpper<T>(this T value, T max)
		where T : IComparable<T> => value.IsGreaterThan(max) ? max : value;

	/// <summary>
	/// Clamps the value to be no less than the minimum value.
	/// </summary>
	/// <typeparam name="T">The type of the value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="min">The minimum value.</param>
	/// <returns>The initial value if it is greater than the minimum value; otherwise the minimum value.</returns>
	public static T ClampLower<T>(this T value, T min)
		where T : IComparable<T> => value.IsLessThan(min) ? min : value;

	/// <summary>
	/// Clamps the value to be no less than the minimum value and no greater than the maximum value.
	/// </summary>
	/// <typeparam name="T">The type of the value.</typeparam>
	/// <param name="value">The value.</param>
	/// <param name="min">The minimum value.</param>
	/// <param name="max">The maximum value.</param>
	/// <returns>The value clamped to the specified range.</returns>
	public static T Clamp<T>(this T value, T min, T max)
		where T : IComparable<T>
	{
		Debug.Assert(min.IsLessThanOrEqualTo(max));

		if (value.IsLessThanOrEqualTo(min))
		{
			return min;
		}

		if (value.IsGreaterThanOrEqualTo(max))
		{
			return max;
		}

		return value;
	}

	public static bool IsIn<T>(this T value, T min, T max)
		where T : IComparable<T>
	{
		Debug.Assert(min.IsLessThanOrEqualTo(max));

		return value.IsGreaterThanOrEqualTo(min) && value.IsLessThanOrEqualTo(max);
	}

	public static bool IsInbetween<T>(this T value, T min, T max)
		where T : IComparable<T>
	{
		Debug.Assert(min.IsLessThanOrEqualTo(max));

		return value.IsGreaterThan(min) && value.IsLessThan(max);
	}

	#endregion

	#region Is Divisible By

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this byte value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this byte value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this byte value, int divisor)
	{
		Debug.Assert(divisor != 0);

		return value % divisor == 0;
	}

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this sbyte value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this sbyte value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this sbyte value, int divisor)
	{
		Debug.Assert(divisor != 0);

		return value % divisor == 0;
	}

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this short value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this short value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this short value, int divisor)
	{
		Debug.Assert(divisor != 0);

		return value % divisor == 0;
	}

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this ushort value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this ushort value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this ushort value, int divisor)
	{
		Debug.Assert(divisor != 0);

		return value % divisor == 0;
	}

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this int value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this int value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this int value, int divisor)
	{
		Debug.Assert(divisor != 0);

		return value % divisor == 0;
	}

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this uint value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this uint value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this uint value, int divisor)
	{
		Debug.Assert(divisor != 0);

		return value % divisor == 0;
	}

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this long value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this long value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this long value, long divisor)
	{
		Debug.Assert(divisor != 0);

		return value % divisor == 0;
	}

	/// <summary>
	/// Determines whether the specified value is even.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if even, false otherwise.
	/// </returns>
	public static bool IsEven(this ulong value) => value % 2 == 0;

	/// <summary>
	/// Determines whether the specified value is odd.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <returns>
	/// True if odd, false otherwise.
	/// </returns>
	public static bool IsOdd(this ulong value) => value % 2 != 0;

	/// <summary>
	/// Determines whether the specified value is divisible by the given divisor.
	/// </summary>
	/// <param name="value">The value.</param>
	/// <param name="divisor">The divisor.</param>
	/// <returns>
	/// True if divisible by the divisor, false otherwise.
	/// </returns>
	public static bool IsDivisibleBy(this ulong value, long divisor)
	{
		Debug.Assert(divisor != 0);

		// divisor must be unsigned in order for this to compile, and sign of divisor doesn't matter
		// for the % operator anyway so discarding sign should still produce the same results
		return value % (ulong)Math.Abs(divisor) == 0;
	}

	#endregion

	#region Rounding

	public static int Floor(this float value) => (int)Math.Floor(value);

	public static int Floor(this double value) => (int)Math.Floor(value);

	public static int Floor(this decimal value) => (int)Math.Floor(value);

	public static int Ceiling(this float value) => (int)Math.Ceiling(value);

	public static int Ceiling(this double value) => (int)Math.Ceiling(value);

	public static int Ceiling(this decimal value) => (int)Math.Ceiling(value);

	public static int Round(this float value) => (int)Math.Round(value);

	public static int Round(this double value) => (int)Math.Round(value);

	public static int Round(this decimal value) => (int)Math.Round(value);

	public static int Round(this float value, MidpointRounding mode) => (int)Math.Round(value, mode);

	public static int Round(this double value, MidpointRounding mode) => (int)Math.Round(value, mode);

	public static int Round(this decimal value, MidpointRounding mode) => (int)Math.Round(value, mode);

	#endregion

	#region Enumerables

	public static long SumLong(this IEnumerable<int> values)
	{
		Debug.Assert(values != null);

		long result = 0;
		foreach (var value in values)
		{
			result += value;
		}

		return result;
	}

	public static int Multiply(this IEnumerable<int> values)
	{
		Debug.Assert(values != null);

		int result = 1;
		foreach (var value in values)
		{
			result *= value;
		}

		return result;
	}

	public static long MultiplyLong(this IEnumerable<int> values)
	{
		Debug.Assert(values != null);

		long result = 1;
		foreach (var value in values)
		{
			result *= value;
		}

		return result;
	}

	#endregion
}
