﻿using EnsureThat;
using static SQLite.SQLite3;

namespace HQS.Utility.Numerics;

public static class HexUtility
{
	public static char ToCharSingle(byte value) =>
		value switch
		{
			0 => '0',
			1 => '1',
			2 => '2',
			3 => '3',
			4 => '4',
			5 => '5',
			6 => '6',
			7 => '7',
			8 => '8',
			9 => '9',
			10 => 'A',
			11 => 'B',
			12 => 'C',
			13 => 'D',
			14 => 'E',
			15 => 'F',
			_ => throw new ArgumentOutOfRangeException(nameof(value), "Must be between 0 and 15 inclusive."),
		};

	public static byte ToByte(char value) =>
		value switch
		{
			'0' => 0x0,
			'1' => 0x1,
			'2' => 0x2,
			'3' => 0x3,
			'4' => 0x4,
			'5' => 0x5,
			'6' => 0x6,
			'7' => 0x7,
			'8' => 0x8,
			'9' => 0x9,
			'A' or 'a' => 0xA,
			'B' or 'b' => 0xB,
			'C' or 'c' => 0xC,
			'D' or 'd' => 0xD,
			'E' or 'e' => 0xE,
			'F' or 'f' => 0xF,
			_ => throw new ArgumentException("Not a valid hexadecimal character.", nameof(value)),
		};

	public static void ToChar(byte value, Span<char> result)
	{
		Ensure.That(result.Length, $"{nameof(result)}.Length").IsGte(2);

		result[0] = ToCharSingle((byte)(value >> 4));
		result[1] = ToCharSingle((byte)(value & 0b00001111));
	}

	public static byte ToByte(ReadOnlySpan<char> value)
	{
		Ensure.That(value.Length, $"{nameof(value)}.Length").IsGte(2);

		return (byte)((ToByte(value[0]) << 4) | ToByte(value[1]));
	}

	public static void ToChars(ReadOnlySpan<byte> value, Span<char> result)
	{
		Ensure.That(result.Length, $"{nameof(result)}.Length").IsGte(value.Length * 2);

		for (int i = 0; i < value.Length; i++)
		{
			ToChar(value[i], result.Slice(i << 1, 2));
		}
	}

	public static void ToBytes(ReadOnlySpan<char> value, Span<byte> result)
	{
		Ensure.That(result.Length, $"{nameof(result)}.Length").IsGte((int)Math.Ceiling(value.Length / 2f));

		for (int i = 0; i < value.Length; i += 2)
		{
			result[i >> 1] = ToByte(value.Slice(i, 2));
		}
	}
}
