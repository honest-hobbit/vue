﻿namespace HQS.Utility.Numerics;

public static class VectorExtensions
{
	public static Vector2 Abs(this Vector2 vec) => Vector2.Abs(vec);

	public static Vector3 Abs(this Vector3 vec) => Vector3.Abs(vec);

	public static Vector4 Abs(this Vector4 vec) => Vector4.Abs(vec);

	public static Vector2 ToVector2(this float value) => new Vector2(value, value);

	public static Vector3 ToVector3(this float value) => new Vector3(value, value, value);

	public static Vector4 ToVector4(this float value) => new Vector4(value, value, value, value);
}
