﻿namespace HQS.Utility.Numerics;

public static class BitMask
{
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static int CreateIntWithOnesInLowerBits(int count)
	{
		Debug.Assert(count >= 0);
		Debug.Assert(count <= 31);

		return (1 << count) - 1;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static uint CreateUIntWithOnesInLowerBits(int count)
	{
		Debug.Assert(count >= 0);
		Debug.Assert(count <= 31);

		return (1U << count) - 1;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static long CreateLongWithOnesInLowerBits(int count)
	{
		Debug.Assert(count >= 0);
		Debug.Assert(count <= 63);

		return (1L << count) - 1;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static ulong CreateULongWithOnesInLowerBits(int count)
	{
		Debug.Assert(count >= 0);
		Debug.Assert(count <= 63);

		return (1UL << count) - 1;
	}
}
