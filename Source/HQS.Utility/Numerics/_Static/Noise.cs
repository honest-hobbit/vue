﻿namespace HQS.Utility.Numerics;

// https://github.com/EDKarlsson/go-squirrelnoise/blob/main/SquirrelNoise5.go
// ///////////////////////////////////////////////////////////////////////////////////////////////
// SquirrelNoise5 - Squirrel's Raw Noise utilities (version 5)
//
// This code is made available under the Creative Commons attribution 3.0 license (CC-BY-3.0 US):
// Attribution in source code comments (even closed-source/commercial code) is sufficient.
// License summary and text available at: https://creativecommons.org/licenses/by/3.0/us/
//
// These noise functions were written by Squirrel Eiserloh as a cheap and simple substitute for
// the [sometimes awful] bit-noise sample code functions commonly found on the web, many of which
// are hugely biased or terribly patterned, e.g. having bits which are on (or off) 75% or even
// 100% of the time (or are excessively overkill/slow for our needs, such as MD5 or SHA).
//
// Note: This is work in progress; not all functions have been tested.  Use at your own risk.
// Please report any bugs, issues, or bothersome cases to SquirrelEiserloh at gmail.com.
//
// The following functions are all based on a simple bit-noise hash function which returns an
// unsigned integer containing 32 reasonably-well-scrambled bits, based on a given (signed)
// integer input parameter (position/index) and [optional] seed.  Kind of like looking up a
// value in an infinitely large [non-existent] table of previously rolled random numbers.
//
// These functions are deterministic and random-access / order-independent (i.e. state-free),
// so they are particularly well-suited for use in smoothed/fractal/simplex/Perlin noise
// functions and out-of-order (or or-demand) procedural content generation (i.e. that mountain
// village is the same whether you generated it first or last, ahead of time or just now).
//
// The N-dimensional variations simply hash their multidimensional coordinates down to a single
// 32-bit index and then proceed as usual, so while results are not unique they should
// (hopefully) not seem locally predictable or repetitive.
//
// ///////////////////////////////////////////////////////////////////////////////////////////////
public static class Noise
{
	private const uint Noise1 = 0xd2a80a3f; // 11010010101010000000101000111111

	private const uint Noise2 = 0xa884f197; // 10101000100001001111000110010111

	private const uint Noise3 = 0x6C736F4B; // 01101100011100110110111101001011

	private const uint Noise4 = 0xB79F3ABB; // 10110111100111110011101010111011

	private const uint Noise5 = 0x1b56c4f5; // 00011011010101101100010011110101

	private const int Prime1 = 198491317;   // Large prime number with non-boring bits

	private const int Prime2 = 6542989;	 // Large prime number with distinct and non-boring bits

	private const int Prime3 = 357239;	  // Large prime number with distinct and non-boring bits

	private const double OneOverMaxUint = 1.0 / uint.MaxValue;

	private const double OneOverMaxInt = 1.0 / int.MaxValue;

	// -----------------------------------------------------------------------------------------------
	// Fast hash of an int32 into a different (unrecognizable) uint32.
	//
	// Returns an unsigned integer containing 32 reasonably-well-scrambled bits, based on the hash
	// of a given (signed) integer input parameter (position/index) and [optional] seed.  Kind of
	// like looking up a value in an infinitely large table of previously generated random numbers.
	//
	// I call this particular approach SquirrelNoise5 (5th iteration of my 1D raw noise function).
	//
	// Many thanks to Peter Schmidt-Nielsen whose outstanding analysis helped identify a weakness
	// in the SquirrelNoise3 code I originally used in my GDC 2017 talk, "Noise-based RNG".
	// Version 5 avoids a noise repetition found in version 3 at extremely high position values
	// caused by a lack of influence by some of the high input bits onto some of the low output bits.
	//
	// The revised SquirrelNoise5 function ensures all input bits affect all output bits, and to
	// (for me) a statistically acceptable degree.  I believe the worst-case here is in the amount
	// of influence input position bit #30 has on output noise bit #0 (49.99%, vs. 50% ideal).
	public static uint Get1D(int x, uint seed = 0)
	{
		uint mangledBits;
		mangledBits = (uint)x;
		mangledBits *= Noise1;
		mangledBits += seed;
		mangledBits ^= mangledBits >> 9;
		mangledBits += Noise2;
		mangledBits ^= mangledBits >> 11;
		mangledBits *= Noise3;
		mangledBits ^= mangledBits >> 13;
		mangledBits += Noise4;
		mangledBits ^= mangledBits >> 15;
		mangledBits *= Noise5;
		mangledBits ^= mangledBits >> 17;
		return mangledBits;
	}

	public static uint Get2D(int x, int y, uint seed = 0) =>
		Get1D(x + (Prime1 * y), seed);

	public static uint Get3D(int x, int y, int z, uint seed = 0) =>
		Get1D(x + (Prime1 * y) + (Prime2 * z), seed);

	public static uint Get4D(int x, int y, int z, int w, uint seed = 0) =>
		Get1D(x + (Prime1 * y) + (Prime2 * z) + (Prime3 * w), seed);

	public static float Get1DZeroToOne(int x, uint seed = 0) =>
		(float)(OneOverMaxUint * Get1D(x, seed));

	public static float Get2DZeroToOne(int x, int y, uint seed = 0) =>
		(float)(OneOverMaxUint * Get2D(x, y, seed));

	public static float Get3DZeroToOne(int x, int y, int z, uint seed = 0) =>
		(float)(OneOverMaxUint * Get3D(x, y, z, seed));

	public static float Get4DZeroToOne(int x, int y, int z, int w, uint seed = 0) =>
		(float)(OneOverMaxUint * Get4D(x, y, z, w, seed));

	public static float Get1DNegOneToOne(int x, uint seed = 0) =>
		(float)(OneOverMaxInt * (int)Get1D(x, seed));

	public static float Get2DNegOneToOne(int x, int y, uint seed = 0) =>
		(float)(OneOverMaxInt * (int)Get2D(x, y, seed));

	public static float Get3DNegOneToOne(int x, int y, int z, uint seed = 0) =>
		(float)(OneOverMaxInt * (int)Get3D(x, y, z, seed));

	public static float Get4DNegOneToOne(int x, int y, int z, int w, uint seed = 0) =>
		(float)(OneOverMaxInt * (int)Get4D(x, y, z, w, seed));
}
