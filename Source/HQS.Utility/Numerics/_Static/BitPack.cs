﻿namespace HQS.Utility.Numerics;

// Bit indices are numbered right to left, IE 0 is the right most bit (the least significant bit)
// and higher number bits are to the left (more significant). Bit indices are in the opposite order
// to which the bits are added, IE the last bit added ends up with index 0 and the first bit added
// has the highest index.
public static class BitPack
{
	public static void Add(ref int bits, bool value)
	{
		bits <<= 1;
		if (value)
		{
			bits |= 1;
		}
	}

	public static int GetReadMask(int bitIndex) => 1 << bitIndex;

	public static bool ReadUsingMask(int bits, int mask) => (bits & mask) != 0;

	public static bool Read(int bits, int bitIndex) => ReadUsingMask(bits, GetReadMask(bitIndex));
}
