﻿namespace HQS.Utility.Numerics;

// Implements zig zag encoding to convert signed numbers to unsigned by alternating between values.
// Example; 0 => 0, -1 => 1, 1 => 2, -2 => 3, 2 => 4, -3 => 5, 3 => 6 ...
// https://gist.github.com/mfuerstenau/ba870a29e16536fdbaba
public static class ZigZagEncoding
{
	public static IDualConverter<long, ulong> OfLong { get; } = new LongConverter();

	public static IDualConverter<int, uint> OfInt { get; } = new IntConverter();

	public static IDualConverter<short, ushort> OfShort { get; } = new ShortConverter();

	public static IDualConverter<sbyte, byte> OfSByte { get; } = new SByteConverter();

	private class LongConverter : IDualConverter<long, ulong>
	{
		/// <inheritdoc />
		public ulong Convert(long value) => unchecked((ulong)(value >> 63 ^ value << 1));

		/// <inheritdoc />
		public long Convert(ulong value) => unchecked((long)(value >> 1) ^ -((long)value & 1L));
	}

	private class IntConverter : IDualConverter<int, uint>
	{
		/// <inheritdoc />
		public uint Convert(int value) => unchecked((uint)(value >> 31 ^ value << 1));

		/// <inheritdoc />
		public int Convert(uint value) => unchecked((int)(value >> 1) ^ -((int)value & 1));
	}

	private class ShortConverter : IDualConverter<short, ushort>
	{
		/// <inheritdoc />
		public ushort Convert(short value) => unchecked((ushort)(value >> 15 ^ value << 1));

		/// <inheritdoc />
		public short Convert(ushort value) => unchecked((short)((int)((uint)value >> 1) ^ -(value & 1)));
	}

	private class SByteConverter : IDualConverter<sbyte, byte>
	{
		/// <inheritdoc />
		public byte Convert(sbyte value) => unchecked((byte)(value >> 7 ^ value << 1));

		/// <inheritdoc />
		public sbyte Convert(byte value) => unchecked((sbyte)((int)((uint)value >> 1) ^ -(value & 1)));
	}
}
