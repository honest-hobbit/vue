﻿namespace HQS.Utility.Numerics;

public struct BitEnumerator<T> : IEnumerator<bool>
	where T : struct, IReadOnlyList<bool>
{
	private readonly int count;

	// this should be readonly but isn't in order to avoid copying on every access
	private T source;

	private int index;

	private bool current;

	internal BitEnumerator(T source)
	{
		this.source = source;
		this.count = source.Count;
		this.index = 0;
		this.current = default;
	}

	/// <inheritdoc />
	public bool Current => this.current;

	/// <inheritdoc />
	object IEnumerator.Current => this.current;

	/// <inheritdoc />
	public bool MoveNext()
	{
		if (this.index < this.count)
		{
			this.current = this.source[this.index];
			this.index++;
			return true;
		}
		else
		{
			this.index = 0;
			this.current = default;
			return false;
		}
	}

	/// <inheritdoc />
	public void Dispose()
	{
	}

	/// <inheritdoc />
	void IEnumerator.Reset() => throw new NotSupportedException();
}
