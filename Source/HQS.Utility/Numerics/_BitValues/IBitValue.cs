﻿namespace HQS.Utility.Numerics;

public interface IBitValue : IReadOnlyList<bool>
{
	new bool this[int index] { get; set; }
}
