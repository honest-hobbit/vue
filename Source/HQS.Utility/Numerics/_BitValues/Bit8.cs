﻿namespace HQS.Utility.Numerics;

public record struct Bit8 : IBitValue
{
	private byte value;

	public Bit8(byte value)
	{
		this.value = value;
	}

	public Bit8(Bit8 value)
	{
		this.value = value.value;
	}

	public static Bit8 AllFalse => new Bit8(0);

	public static Bit8 AllTrue => new Bit8(byte.MaxValue);

	public byte Value => this.value;

	/// <inheritdoc />
	public int Count => 8;

	/// <inheritdoc />
	public bool this[int index]
	{
		get
		{
			IBitValueContracts.Indexer(this, index);

			return BitValueUtility.Getter(this.value, index);
		}

		set
		{
			IBitValueContracts.Indexer(this, index);

			this.value = (byte)BitValueUtility.Setter(this.value, index, value);
		}
	}

	/// <inheritdoc />
	public override string ToString() => BitValueUtility.ToString(this);

	public BitEnumerator<Bit8> GetEnumerator() => new BitEnumerator<Bit8>(this);

	/// <inheritdoc />
	IEnumerator<bool> IEnumerable<bool>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
