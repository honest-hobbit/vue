﻿namespace HQS.Utility.Numerics;

public record struct Bit256 : IBitValue
{
	private ulong value1;

	private ulong value2;

	private ulong value3;

	private ulong value4;

	public Bit256(ulong value1, ulong value2, ulong value3, ulong value4)
	{
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
	}

	public Bit256(Bit256 value)
	{
		this.value1 = value.value1;
		this.value2 = value.value2;
		this.value3 = value.value3;
		this.value4 = value.value4;
	}

	public static Bit256 AllFalse => new Bit256(0, 0, 0, 0);

	public static Bit256 AllTrue => new Bit256(ulong.MaxValue, ulong.MaxValue, ulong.MaxValue, ulong.MaxValue);

	public ulong Value1 => this.value1;

	public ulong Value2 => this.value2;

	public ulong Value3 => this.value3;

	public ulong Value4 => this.value4;

	/// <inheritdoc />
	public int Count => 256;

	/// <inheritdoc />
	public bool this[int index]
	{
		get
		{
			IBitValueContracts.Indexer(this, index);

			return (index / 64) switch
			{
				0 => BitValueUtility.Getter(this.value1, index),
				1 => BitValueUtility.Getter(this.value2, index - 64),
				2 => BitValueUtility.Getter(this.value3, index - 128),
				3 => BitValueUtility.Getter(this.value4, index - 192),
				_ => throw new ArgumentOutOfRangeException(nameof(index)),
			};
		}

		set
		{
			IBitValueContracts.Indexer(this, index);

			switch (index / 64)
			{
				case 0: this.value1 = BitValueUtility.Setter(this.value1, index, value); break;
				case 1: this.value2 = BitValueUtility.Setter(this.value2, index - 64, value); break;
				case 2: this.value3 = BitValueUtility.Setter(this.value3, index - 128, value); break;
				case 3: this.value4 = BitValueUtility.Setter(this.value4, index - 192, value); break;
				default: throw new ArgumentOutOfRangeException(nameof(index));
			}
		}
	}

	/// <inheritdoc />
	public override string ToString() => BitValueUtility.ToString(this);

	public BitEnumerator<Bit256> GetEnumerator() => new BitEnumerator<Bit256>(this);

	/// <inheritdoc />
	IEnumerator<bool> IEnumerable<bool>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
