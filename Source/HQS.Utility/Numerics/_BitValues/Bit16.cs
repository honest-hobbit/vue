﻿namespace HQS.Utility.Numerics;

public record struct Bit16 : IBitValue
{
	private ushort value;

	public Bit16(ushort value)
	{
		this.value = value;
	}

	public Bit16(Bit16 value)
	{
		this.value = value.value;
	}

	public static Bit16 AllFalse => new Bit16(0);

	public static Bit16 AllTrue => new Bit16(ushort.MaxValue);

	public ushort Value => this.value;

	/// <inheritdoc />
	public int Count => 16;

	/// <inheritdoc />
	public bool this[int index]
	{
		get
		{
			IBitValueContracts.Indexer(this, index);

			return BitValueUtility.Getter(this.value, index);
		}

		set
		{
			IBitValueContracts.Indexer(this, index);

			this.value = (ushort)BitValueUtility.Setter(this.value, index, value);
		}
	}

	/// <inheritdoc />
	public override string ToString() => BitValueUtility.ToString(this);

	public BitEnumerator<Bit16> GetEnumerator() => new BitEnumerator<Bit16>(this);

	/// <inheritdoc />
	IEnumerator<bool> IEnumerable<bool>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
