﻿namespace HQS.Utility.Numerics;

internal static class BitValueUtility
{
	public static bool Getter(ulong current, int index)
	{
		Debug.Assert(index >= 0 && index < 64);

		ulong mask = 1ul << index;
		return (current & mask) == mask;
	}

	public static ulong Setter(ulong current, int index, bool value)
	{
		Debug.Assert(index >= 0 && index < 64);

		ulong mask = 1ul << index;
		if (value)
		{
			return current |= mask;
		}
		else
		{
			return current &= ~mask;
		}
	}

	public static string ToString<T>(T value)
		where T : struct, IReadOnlyList<bool>
	{
		return string.Create(value.Count + 2, value, (chars, bits) =>
		{
			chars[0] = '[';

			int length = bits.Count;
			for (int i = 0; i < length; i++)
			{
				chars[^(i + 2)] = bits[i] ? '1' : '0';
			}

			chars[^1] = ']';
		});
	}
}
