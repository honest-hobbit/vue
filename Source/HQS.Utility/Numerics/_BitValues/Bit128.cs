﻿namespace HQS.Utility.Numerics;

public record struct Bit128 : IBitValue
{
	private ulong value1;

	private ulong value2;

	public Bit128(ulong value1, ulong value2)
	{
		this.value1 = value1;
		this.value2 = value2;
	}

	public Bit128(Bit128 value)
	{
		this.value1 = value.value1;
		this.value2 = value.value2;
	}

	public static Bit128 AllFalse => new Bit128(0, 0);

	public static Bit128 AllTrue => new Bit128(ulong.MaxValue, ulong.MaxValue);

	public ulong Value1 => this.value1;

	public ulong Value2 => this.value2;

	/// <inheritdoc />
	public int Count => 128;

	/// <inheritdoc />
	public bool this[int index]
	{
		get
		{
			IBitValueContracts.Indexer(this, index);

			return index < 64 ?
				BitValueUtility.Getter(this.value1, index) :
				BitValueUtility.Getter(this.value2, index - 64);
		}

		set
		{
			IBitValueContracts.Indexer(this, index);

			if (index < 64)
			{
				this.value1 = BitValueUtility.Setter(this.value1, index, value);
			}
			else
			{
				this.value2 = BitValueUtility.Setter(this.value2, index - 64, value);
			}
		}
	}

	/// <inheritdoc />
	public override string ToString() => BitValueUtility.ToString(this);

	public BitEnumerator<Bit128> GetEnumerator() => new BitEnumerator<Bit128>(this);

	/// <inheritdoc />
	IEnumerator<bool> IEnumerable<bool>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
