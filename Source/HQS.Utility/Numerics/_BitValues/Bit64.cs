﻿namespace HQS.Utility.Numerics;

public record struct Bit64 : IBitValue
{
	private ulong value;

	public Bit64(ulong value)
	{
		this.value = value;
	}

	public Bit64(Bit64 value)
	{
		this.value = value.value;
	}

	public static Bit64 AllFalse => new Bit64(0);

	public static Bit64 AllTrue => new Bit64(ulong.MaxValue);

	public ulong Value => this.value;

	/// <inheritdoc />
	public int Count => 64;

	/// <inheritdoc />
	public bool this[int index]
	{
		get
		{
			IBitValueContracts.Indexer(this, index);

			return BitValueUtility.Getter(this.value, index);
		}

		set
		{
			IBitValueContracts.Indexer(this, index);

			this.value = BitValueUtility.Setter(this.value, index, value);
		}
	}

	/// <inheritdoc />
	public override string ToString() => BitValueUtility.ToString(this);

	public BitEnumerator<Bit64> GetEnumerator() => new BitEnumerator<Bit64>(this);

	/// <inheritdoc />
	IEnumerator<bool> IEnumerable<bool>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
