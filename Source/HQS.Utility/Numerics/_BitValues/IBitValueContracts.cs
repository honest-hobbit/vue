﻿namespace HQS.Utility.Numerics;

public static class IBitValueContracts
{
	public static void Indexer<T>(T instance, int index)
		where T : IReadOnlyList<bool>
	{
		Debug.Assert(instance != null);

		Ensure.That(index, nameof(index)).IsInRange(0, instance.Count - 1);
	}
}
