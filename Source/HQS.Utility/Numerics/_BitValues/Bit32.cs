﻿namespace HQS.Utility.Numerics;

public record struct Bit32 : IBitValue
{
	private uint value;

	public Bit32(uint value)
	{
		this.value = value;
	}

	public Bit32(Bit32 value)
	{
		this.value = value.value;
	}

	public static Bit32 AllFalse => new Bit32(0);

	public static Bit32 AllTrue => new Bit32(uint.MaxValue);

	public uint Value => this.value;

	/// <inheritdoc />
	public int Count => 32;

	/// <inheritdoc />
	public bool this[int index]
	{
		get
		{
			IBitValueContracts.Indexer(this, index);

			return BitValueUtility.Getter(this.value, index);
		}

		set
		{
			IBitValueContracts.Indexer(this, index);

			this.value = (uint)BitValueUtility.Setter(this.value, index, value);
		}
	}

	/// <inheritdoc />
	public override string ToString() => BitValueUtility.ToString(this);

	public BitEnumerator<Bit32> GetEnumerator() => new BitEnumerator<Bit32>(this);

	/// <inheritdoc />
	IEnumerator<bool> IEnumerable<bool>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
