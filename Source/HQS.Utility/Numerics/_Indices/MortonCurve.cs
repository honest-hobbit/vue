﻿namespace HQS.Utility.Numerics;

public static class MortonCurve
{
	public static class EqualityComparer
	{
		public static EqualityComparer<Int2> ForInt2 { get; } = new MortonComparer<Int2>(MortonCurve2D.Instance);

		public static EqualityComparer<Int3> ForInt3 { get; } = new MortonComparer<Int3>(MortonCurve3D.Instance);

		public static EqualityComparer<Int4> ForInt4 { get; } = new MortonComparer<Int4>(MortonCurve4D.Instance);

		private class MortonComparer<T> : EqualityComparer<T>
			where T : struct, IEquatable<T>
		{
			private readonly ISpaceFillingCurve<T> curve;

			public MortonComparer(ISpaceFillingCurve<T> curve)
			{
				Debug.Assert(curve != null);

				this.curve = curve;
			}

			public override bool Equals(T x, T y) => x.Equals(y);

			public override int GetHashCode(T value) => this.curve.Encode(value);
		}
	}

	public static ISpaceFillingCurve<Int2> ForInt2 => MortonCurve2D.Instance;

	public static ISpaceFillingCurve<Int3> ForInt3 => MortonCurve3D.Instance;

	public static ISpaceFillingCurve<Int4> ForInt4 => MortonCurve4D.Instance;

	private class MortonCurve2D : ISpaceFillingCurve<Int2>
	{
		private const uint Mask1 = 0b0000_0000_0000_0000_1111_1111_1111_1111;

		private const uint Mask2 = 0b0000_0000_1111_1111_0000_0000_1111_1111;

		private const uint Mask3 = 0b0000_1111_0000_1111_0000_1111_0000_1111;

		private const uint Mask4 = 0b0011_0011_0011_0011_0011_0011_0011_0011;

		private const uint Mask5 = 0b0101_0101_0101_0101_0101_0101_0101_0101;

		private MortonCurve2D()
		{
		}

		public static ISpaceFillingCurve<Int2> Instance { get; } = new MortonCurve2D();

		/// <inheritdoc />
		public IEnumerable<Int2> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return iterations switch
			{
				0 => Enumerable.Empty<Int2>(),
				1 => SingleIteration(),
				_ => RecursiveCurve(),
			};

			IEnumerable<Int2> RecursiveCurve()
			{
				foreach (var outterIndex in Instance.GetCurve(iterations - 1))
				{
					var offsetIndex = outterIndex << 1;
					foreach (var innerIndex in SingleIteration())
					{
						yield return innerIndex + offsetIndex;
					}
				}
			}

			static IEnumerable<Int2> SingleIteration()
			{
				yield return new Int2(0, 0);
				yield return new Int2(1, 0);
				yield return new Int2(0, 1);
				yield return new Int2(1, 1);
			}
		}

		/// <inheritdoc />
		public Int2 Decode(int mortonCode) => new Int2(Compact(mortonCode), Compact(mortonCode >> 1));

		/// <inheritdoc />
		public int Encode(Int2 index) => Split(index.X) | (Split(index.Y) << 1);

		private static int Split(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask1;
				x = (x | (x << 8)) & Mask2;
				x = (x | (x << 4)) & Mask3;
				x = (x | (x << 2)) & Mask4;
				x = (x | (x << 1)) & Mask5;
				return (int)x;
			}
		}

		private static int Compact(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask5;
				x = (x | (x >> 1)) & Mask4;
				x = (x | (x >> 2)) & Mask3;
				x = (x | (x >> 4)) & Mask2;
				x = (x | (x >> 8)) & Mask1;
				return (int)x;
			}
		}
	}

	private class MortonCurve3D : ISpaceFillingCurve<Int3>
	{
		private const uint Mask1 = 0b0000_0000_0000_0000_0000_0011_1111_1111;

		private const uint Mask2 = 0b0000_0011_0000_0000_0000_0000_1111_1111;

		private const uint Mask3 = 0b0000_0011_0000_0000_1111_0000_0000_1111;

		private const uint Mask4 = 0b0000_0011_0000_1100_0011_0000_1100_0011;

		private const uint Mask5 = 0b0000_1001_0010_0100_1001_0010_0100_1001;

		private MortonCurve3D()
		{
		}

		public static ISpaceFillingCurve<Int3> Instance { get; } = new MortonCurve3D();

		/// <inheritdoc />
		public IEnumerable<Int3> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return iterations switch
			{
				0 => Enumerable.Empty<Int3>(),
				1 => SingleIteration(),
				_ => RecursiveCurve(),
			};

			IEnumerable<Int3> RecursiveCurve()
			{
				foreach (var outterIndex in Instance.GetCurve(iterations - 1))
				{
					var offsetIndex = outterIndex << 1;
					foreach (var innerIndex in SingleIteration())
					{
						yield return innerIndex + offsetIndex;
					}
				}
			}

			static IEnumerable<Int3> SingleIteration()
			{
				yield return new Int3(0, 0, 0);
				yield return new Int3(1, 0, 0);
				yield return new Int3(0, 1, 0);
				yield return new Int3(1, 1, 0);
				yield return new Int3(0, 0, 1);
				yield return new Int3(1, 0, 1);
				yield return new Int3(0, 1, 1);
				yield return new Int3(1, 1, 1);
			}
		}

		/// <inheritdoc />
		public Int3 Decode(int mortonCode) =>
			new Int3(Compact(mortonCode), Compact(mortonCode >> 1), Compact(mortonCode >> 2));

		/// <inheritdoc />
		public int Encode(Int3 index) => Split(index.X) | (Split(index.Y) << 1) | (Split(index.Z) << 2);

		private static int Split(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask1;
				x = (x | (x << 16)) & Mask2;
				x = (x | (x << 8)) & Mask3;
				x = (x | (x << 4)) & Mask4;
				x = (x | (x << 2)) & Mask5;
				return (int)x;
			}
		}

		private static int Compact(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask5;
				x = (x | (x >> 2)) & Mask4;
				x = (x | (x >> 4)) & Mask3;
				x = (x | (x >> 8)) & Mask2;
				x = (x | (x >> 16)) & Mask1;
				return (int)x;
			}
		}
	}

	private class MortonCurve4D : ISpaceFillingCurve<Int4>
	{
		private const uint Mask1 = 0b0000_0000_0000_0000_0000_0000_1111_1111;

		private const uint Mask2 = 0b0000_0000_0000_1111_0000_0000_0000_1111;

		private const uint Mask3 = 0b0000_0011_0000_0011_0000_0011_0000_0011;

		private const uint Mask4 = 0b0001_0001_0001_0001_0001_0001_0001_0001;

		private MortonCurve4D()
		{
		}

		public static ISpaceFillingCurve<Int4> Instance { get; } = new MortonCurve4D();

		/// <inheritdoc />
		public IEnumerable<Int4> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return iterations switch
			{
				0 => Enumerable.Empty<Int4>(),
				1 => SingleIteration(),
				_ => RecursiveCurve(),
			};

			IEnumerable<Int4> RecursiveCurve()
			{
				foreach (var outterIndex in Instance.GetCurve(iterations - 1))
				{
					var offsetIndex = outterIndex << 1;
					foreach (var innerIndex in SingleIteration())
					{
						yield return innerIndex + offsetIndex;
					}
				}
			}

			static IEnumerable<Int4> SingleIteration()
			{
				yield return new Int4(0, 0, 0, 0);
				yield return new Int4(1, 0, 0, 0);
				yield return new Int4(0, 1, 0, 0);
				yield return new Int4(1, 1, 0, 0);
				yield return new Int4(0, 0, 1, 0);
				yield return new Int4(1, 0, 1, 0);
				yield return new Int4(0, 1, 1, 0);
				yield return new Int4(1, 1, 1, 0);
				yield return new Int4(0, 0, 0, 1);
				yield return new Int4(1, 0, 0, 1);
				yield return new Int4(0, 1, 0, 1);
				yield return new Int4(1, 1, 0, 1);
				yield return new Int4(0, 0, 1, 1);
				yield return new Int4(1, 0, 1, 1);
				yield return new Int4(0, 1, 1, 1);
				yield return new Int4(1, 1, 1, 1);
			}
		}

		/// <inheritdoc />
		public Int4 Decode(int mortonCode) =>
			new Int4(Compact(mortonCode), Compact(mortonCode >> 1), Compact(mortonCode >> 2), Compact(mortonCode >> 3));

		/// <inheritdoc />
		public int Encode(Int4 index) =>
			Split(index.X) | (Split(index.Y) << 1) | (Split(index.Z) << 2) | (Split(index.W) << 3);

		private static int Split(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask1;
				x = (x | (x << 12)) & Mask2;
				x = (x | (x << 6)) & Mask3;
				x = (x | (x << 3)) & Mask4;
				return (int)x;
			}
		}

		private static int Compact(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask4;
				x = (x | (x >> 3)) & Mask3;
				x = (x | (x >> 6)) & Mask2;
				x = (x | (x >> 12)) & Mask1;
				return (int)x;
			}
		}
	}
}
