﻿namespace HQS.Utility.Numerics;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
[Serializable]
public record struct Int2
{
	public int X;

	public int Y;

	public Int2(int value)
	{
		this.X = value;
		this.Y = value;
	}

	public Int2(int x, int y)
	{
		this.X = x;
		this.Y = y;
	}

	public static Int2 Zero => new Int2(0);

	public static Int2 One => new Int2(1);

	public static Int2 UnitX => new Int2(1, 0);

	public static Int2 UnitY => new Int2(0, 1);

	public static Int2 MinValue => new Int2(int.MinValue);

	public static Int2 MaxValue => new Int2(int.MaxValue);

	public static Int2 operator -(Int2 value) => new Int2(-value.X, -value.Y);

	public static Int2 operator +(Int2 lhs, Int2 rhs) => new Int2(lhs.X + rhs.X, lhs.Y + rhs.Y);

	public static Int2 operator +(Int2 value, int scalar) => new Int2(value.X + scalar, value.Y + scalar);

	public static Int2 operator +(int scalar, Int2 value) => new Int2(value.X + scalar, value.Y + scalar);

	public static Int2 operator -(Int2 lhs, Int2 rhs) => new Int2(lhs.X - rhs.X, lhs.Y - rhs.Y);

	public static Int2 operator -(Int2 value, int scalar) => new Int2(value.X - scalar, value.Y - scalar);

	public static Int2 operator *(Int2 lhs, Int2 rhs) => new Int2(lhs.X * rhs.X, lhs.Y * rhs.Y);

	public static Int2 operator *(Int2 value, int scalar) => new Int2(value.X * scalar, value.Y * scalar);

	public static Int2 operator *(int scalar, Int2 value) => new Int2(value.X * scalar, value.Y * scalar);

	public static Int2 operator /(Int2 lhs, Int2 rhs) => new Int2(lhs.X / rhs.X, lhs.Y / rhs.Y);

	public static Int2 operator /(Int2 value, int scalar) => new Int2(value.X / scalar, value.Y / scalar);

	public static Int2 operator %(Int2 lhs, Int2 rhs) => new Int2(lhs.X % rhs.X, lhs.Y % rhs.Y);

	public static Int2 operator %(Int2 value, int scalar) => new Int2(value.X % scalar, value.Y % scalar);

	public static Int2 operator &(Int2 value, Int2 mask) => new Int2(value.X & mask.X, value.Y & mask.Y);

	public static Int2 operator &(Int2 value, int mask) => new Int2(value.X & mask, value.Y & mask);

	public static Int2 operator |(Int2 value, Int2 mask) => new Int2(value.X | mask.X, value.Y | mask.Y);

	public static Int2 operator |(Int2 value, int mask) => new Int2(value.X | mask, value.Y | mask);

	public static Int2 operator ^(Int2 value, Int2 mask) => new Int2(value.X ^ mask.X, value.Y ^ mask.Y);

	public static Int2 operator ^(Int2 value, int mask) => new Int2(value.X ^ mask, value.Y ^ mask);

	public static Int2 operator ~(Int2 value) => new Int2(~value.X, ~value.Y);

	public static Int2 operator <<(Int2 value, int shift) => new Int2(value.X << shift, value.Y << shift);

	public static Int2 operator >>(Int2 value, int shift) => new Int2(value.X >> shift, value.Y >> shift);

	/// <inheritdoc />
	public override string ToString() => $"[{this.X}, {this.Y}]";

	public Vector2 ToVector() => new Vector2(this.X, this.Y);

	public T[,] ToArray<T>() => new T[this.X, this.Y];

	public void Deconstruct(out int x, out int y)
	{
		x = this.X;
		y = this.Y;
	}
}
