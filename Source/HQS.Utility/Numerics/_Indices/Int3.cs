﻿namespace HQS.Utility.Numerics;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
[Serializable]
public record struct Int3
{
	public int X;

	public int Y;

	public int Z;

	public Int3(int value)
	{
		this.X = value;
		this.Y = value;
		this.Z = value;
	}

	public Int3(int x, int y, int z)
	{
		this.X = x;
		this.Y = y;
		this.Z = z;
	}

	public Int3(Int2 value, int z)
	{
		this.X = value.X;
		this.Y = value.Y;
		this.Z = z;
	}

	public static Int3 Zero => new Int3(0);

	public static Int3 One => new Int3(1);

	public static Int3 UnitX => new Int3(1, 0, 0);

	public static Int3 UnitY => new Int3(0, 1, 0);

	public static Int3 UnitZ => new Int3(0, 0, 1);

	public static Int3 MinValue => new Int3(int.MinValue);

	public static Int3 MaxValue => new Int3(int.MaxValue);

	public static Int3 operator -(Int3 value) => new Int3(-value.X, -value.Y, -value.Z);

	public static Int3 operator +(Int3 lhs, Int3 rhs) =>
		new Int3(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z);

	public static Int3 operator +(Int3 value, int scalar) =>
		new Int3(value.X + scalar, value.Y + scalar, value.Z + scalar);

	public static Int3 operator +(int scalar, Int3 value) =>
		new Int3(value.X + scalar, value.Y + scalar, value.Z + scalar);

	public static Int3 operator -(Int3 lhs, Int3 rhs) =>
		new Int3(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z);

	public static Int3 operator -(Int3 value, int scalar) =>
		new Int3(value.X - scalar, value.Y - scalar, value.Z - scalar);

	public static Int3 operator *(Int3 lhs, Int3 rhs) =>
		new Int3(lhs.X * rhs.X, lhs.Y * rhs.Y, lhs.Z * rhs.Z);

	public static Int3 operator *(Int3 value, int scalar) =>
		new Int3(value.X * scalar, value.Y * scalar, value.Z * scalar);

	public static Int3 operator *(int scalar, Int3 value) =>
		new Int3(value.X * scalar, value.Y * scalar, value.Z * scalar);

	public static Int3 operator /(Int3 lhs, Int3 rhs) =>
		new Int3(lhs.X / rhs.X, lhs.Y / rhs.Y, lhs.Z / rhs.Z);

	public static Int3 operator /(Int3 value, int scalar) =>
		new Int3(value.X / scalar, value.Y / scalar, value.Z / scalar);

	public static Int3 operator %(Int3 lhs, Int3 rhs) =>
		new Int3(lhs.X % rhs.X, lhs.Y % rhs.Y, lhs.Z % rhs.Z);

	public static Int3 operator %(Int3 value, int scalar) =>
		new Int3(value.X % scalar, value.Y % scalar, value.Z % scalar);

	public static Int3 operator &(Int3 value, Int3 mask) =>
		new Int3(value.X & mask.X, value.Y & mask.Y, value.Z & mask.Z);

	public static Int3 operator &(Int3 value, int mask) =>
		new Int3(value.X & mask, value.Y & mask, value.Z & mask);

	public static Int3 operator |(Int3 value, Int3 mask) =>
		new Int3(value.X | mask.X, value.Y | mask.Y, value.Z | mask.Z);

	public static Int3 operator |(Int3 value, int mask) =>
		new Int3(value.X | mask, value.Y | mask, value.Z | mask);

	public static Int3 operator ^(Int3 value, Int3 mask) =>
		new Int3(value.X ^ mask.X, value.Y ^ mask.Y, value.Z ^ mask.Z);

	public static Int3 operator ^(Int3 value, int mask) =>
		new Int3(value.X ^ mask, value.Y ^ mask, value.Z ^ mask);

	public static Int3 operator ~(Int3 value) => new Int3(~value.X, ~value.Y, ~value.Z);

	public static Int3 operator <<(Int3 value, int shift) =>
		new Int3(value.X << shift, value.Y << shift, value.Z << shift);

	public static Int3 operator >>(Int3 value, int shift) =>
		new Int3(value.X >> shift, value.Y >> shift, value.Z >> shift);

	/// <inheritdoc />
	public override string ToString() => $"[{this.X}, {this.Y}, {this.Z}]";

	public Vector3 ToVector() => new Vector3(this.X, this.Y, this.Z);

	public T[,,] ToArray<T>() => new T[this.X, this.Y, this.Z];

	public void Deconstruct(out int x, out int y, out int z)
	{
		x = this.X;
		y = this.Y;
		z = this.Z;
	}
}
