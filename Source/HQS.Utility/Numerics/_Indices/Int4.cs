﻿namespace HQS.Utility.Numerics;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
[Serializable]
public record struct Int4
{
	public int X;

	public int Y;

	public int Z;

	public int W;

	public Int4(int value)
	{
		this.X = value;
		this.Y = value;
		this.Z = value;
		this.W = value;
	}

	public Int4(int x, int y, int z, int w)
	{
		this.X = x;
		this.Y = y;
		this.Z = z;
		this.W = w;
	}

	public Int4(Int2 value, int z, int w)
	{
		this.X = value.X;
		this.Y = value.Y;
		this.Z = z;
		this.W = w;
	}

	public Int4(Int3 value, int w)
	{
		this.X = value.X;
		this.Y = value.Y;
		this.Z = value.Z;
		this.W = w;
	}

	public static Int4 Zero => new Int4(0);

	public static Int4 One => new Int4(1);

	public static Int4 UnitX => new Int4(1, 0, 0, 0);

	public static Int4 UnitY => new Int4(0, 1, 0, 0);

	public static Int4 UnitZ => new Int4(0, 0, 1, 0);

	public static Int4 UnitW => new Int4(0, 0, 0, 1);

	public static Int4 MinValue => new Int4(int.MinValue);

	public static Int4 MaxValue => new Int4(int.MaxValue);

	public static Int4 operator -(Int4 value) => new Int4(-value.X, -value.Y, -value.Z, -value.W);

	public static Int4 operator +(Int4 lhs, Int4 rhs) =>
		new Int4(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z, lhs.W + rhs.W);

	public static Int4 operator +(Int4 value, int scalar) =>
		new Int4(value.X + scalar, value.Y + scalar, value.Z + scalar, value.W + scalar);

	public static Int4 operator +(int scalar, Int4 value) =>
		new Int4(value.X + scalar, value.Y + scalar, value.Z + scalar, value.W + scalar);

	public static Int4 operator -(Int4 lhs, Int4 rhs) =>
		new Int4(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z, lhs.W - rhs.W);

	public static Int4 operator -(Int4 value, int scalar) =>
		new Int4(value.X - scalar, value.Y - scalar, value.Z - scalar, value.W - scalar);

	public static Int4 operator *(Int4 lhs, Int4 rhs) =>
		new Int4(lhs.X * rhs.X, lhs.Y * rhs.Y, lhs.Z * rhs.Z, lhs.W * rhs.W);

	public static Int4 operator *(Int4 value, int scalar) =>
		new Int4(value.X * scalar, value.Y * scalar, value.Z * scalar, value.W * scalar);

	public static Int4 operator *(int scalar, Int4 value) =>
		new Int4(value.X * scalar, value.Y * scalar, value.Z * scalar, value.W * scalar);

	public static Int4 operator /(Int4 lhs, Int4 rhs) =>
		new Int4(lhs.X / rhs.X, lhs.Y / rhs.Y, lhs.Z / rhs.Z, lhs.W / rhs.W);

	public static Int4 operator /(Int4 value, int scalar) =>
		new Int4(value.X / scalar, value.Y / scalar, value.Z / scalar, value.W / scalar);

	public static Int4 operator %(Int4 lhs, Int4 rhs) =>
		new Int4(lhs.X % rhs.X, lhs.Y % rhs.Y, lhs.Z % rhs.Z, lhs.W % rhs.W);

	public static Int4 operator %(Int4 value, int scalar) =>
		new Int4(value.X % scalar, value.Y % scalar, value.Z % scalar, value.W % scalar);

	public static Int4 operator &(Int4 value, Int4 mask) =>
		new Int4(value.X & mask.X, value.Y & mask.Y, value.Z & mask.Z, value.W & mask.W);

	public static Int4 operator &(Int4 value, int mask) =>
		new Int4(value.X & mask, value.Y & mask, value.Z & mask, value.W & mask);

	public static Int4 operator |(Int4 value, Int4 mask) =>
		new Int4(value.X | mask.X, value.Y | mask.Y, value.Z | mask.Z, value.W | mask.W);

	public static Int4 operator |(Int4 value, int mask) =>
		new Int4(value.X | mask, value.Y | mask, value.Z | mask, value.W | mask);

	public static Int4 operator ^(Int4 value, Int4 mask) =>
		new Int4(value.X ^ mask.X, value.Y ^ mask.Y, value.Z ^ mask.Z, value.W ^ mask.W);

	public static Int4 operator ^(Int4 value, int mask) =>
		new Int4(value.X ^ mask, value.Y ^ mask, value.Z ^ mask, value.W ^ mask);

	public static Int4 operator ~(Int4 value) => new Int4(~value.X, ~value.Y, ~value.Z, ~value.W);

	public static Int4 operator <<(Int4 value, int shift) =>
		new Int4(value.X << shift, value.Y << shift, value.Z << shift, value.W << shift);

	public static Int4 operator >>(Int4 value, int shift) =>
		new Int4(value.X >> shift, value.Y >> shift, value.Z >> shift, value.W >> shift);

	/// <inheritdoc />
	public override string ToString() => $"[{this.X}, {this.Y}, {this.Z}, {this.W}]";

	public Vector4 ToVector() => new Vector4(this.X, this.Y, this.Z, this.W);

	public T[,,,] ToArray<T>() => new T[this.X, this.Y, this.Z, this.W];

	public void Deconstruct(out int x, out int y, out int z, out int w)
	{
		x = this.X;
		y = this.Y;
		z = this.Z;
		w = this.W;
	}
}
