﻿namespace HQS.Utility.Numerics;

public interface ISpaceFillingCurve<T>
{
	int Encode(T value);

	T Decode(int code);

	IEnumerable<T> GetCurve(int iterations);
}
