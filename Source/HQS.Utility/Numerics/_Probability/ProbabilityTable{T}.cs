﻿namespace HQS.Utility.Numerics;

public class ProbabilityTable<T>
{
	private readonly ChanceList staticList = new ChanceList(true);

	private readonly ChanceList dynamicList = new ChanceList(false);

	public double StaticChance => this.staticList.TotalChance;

	public double DynamicChance => 1 - this.staticList.TotalChance;

	public IList<Probability<T>> Static => this.staticList;

	public IList<Probability<T>> Dynamic => this.dynamicList;

	public T GetValue(Random random)
	{
		Ensure.That(random, nameof(random)).IsNotNull();

		return this.GetValue(random.NextDouble());
	}

	public T GetValue(double chance)
	{
		if (this.TryGetValue(chance, out T value))
		{
			return value;
		}
		else
		{
			throw new ArgumentOutOfRangeException(nameof(chance), $"No value was found for the given chance of {chance}.");
		}
	}

	public bool TryGetValue(Random random, out T value)
	{
		Ensure.That(random, nameof(random)).IsNotNull();

		return this.TryGetValue(random.NextDouble(), out value);
	}

	public bool TryGetValue(double chance, out T value)
	{
		Ensure.That(chance, nameof(chance)).IsGte(0);
		Ensure.That(chance, nameof(chance)).IsLt(1);

		if (chance < this.StaticChance && this.staticList.TryGetValue(chance, out value))
		{
			return true;
		}
		else
		{
			if (this.dynamicList.Count == 0)
			{
				// there are no dynamic chance values
				value = default;
				return false;
			}

			// because there's at least 1 dynamic chance value it is guaranteed to be returned
			chance = Interpolation.ConvertRange(
				chance - this.StaticChance, 0, this.DynamicChance, 0, this.dynamicList.TotalChance);
			if (!this.dynamicList.TryGetValue(chance, out value))
			{
				// just in case no value is returned, use the last dynamic value
				// (this might happen due to floating point precision errors)
				value = this.dynamicList[this.dynamicList.Count - 1].Value;
			}

			return true;
		}
	}

	private class ChanceList : Collection<Probability<T>>
	{
		private readonly bool ensureMax;

		public ChanceList(bool ensureMax)
		{
			this.ensureMax = ensureMax;
		}

		public double TotalChance { get; private set; }

		public bool TryGetValue(double chance, out T value)
		{
			int max = this.Count;
			for (int i = 0; i < max; i++)
			{
				var probability = this.Items[i];
				if (chance < probability.Chance)
				{
					value = probability.Value;
					return true;
				}

				chance -= probability.Chance;
			}

			value = default;
			return false;
		}

		/// <inheritdoc />
		protected override void SetItem(int index, Probability<T> item)
		{
			var replacingChance = this.Items[index].Chance;
			if (this.ensureMax)
			{
				Ensure.That(item.Chance, $"{nameof(item)}.{nameof(item.Chance)}").IsLte(1 - this.TotalChance + replacingChance);

				this.TotalChance = (this.TotalChance + replacingChance - item.Chance).Clamp(0, 1);
			}
			else
			{
				this.TotalChance = this.TotalChance + replacingChance - item.Chance;
			}

			base.SetItem(index, item);
		}

		/// <inheritdoc />
		protected override void InsertItem(int index, Probability<T> item)
		{
			if (this.ensureMax)
			{
				Ensure.That(item.Chance, $"{nameof(item)}.{nameof(item.Chance)}").IsLte(1 - this.TotalChance);

				this.TotalChance = (this.TotalChance + item.Chance).ClampUpper(1);
			}
			else
			{
				this.TotalChance += item.Chance;
			}

			base.InsertItem(index, item);
		}

		/// <inheritdoc />
		protected override void RemoveItem(int index)
		{
			this.TotalChance -= this.Items[index].Chance;
			if (this.ensureMax)
			{
				this.TotalChance = this.TotalChance.ClampLower(0);
			}

			base.RemoveItem(index);
		}

		/// <inheritdoc />
		protected override void ClearItems()
		{
			this.TotalChance = 0;
			base.ClearItems();
		}
	}
}
