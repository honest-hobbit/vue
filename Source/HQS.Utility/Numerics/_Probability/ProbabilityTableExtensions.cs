﻿namespace HQS.Utility.Numerics;

public static class ProbabilityTableExtensions
{
	public static void Add<T>(this IList<Probability<T>> list, double chance, T value)
	{
		Debug.Assert(list != null);

		list.Add(new Probability<T>(chance, value));
	}

	public static void AddIfHasChance<T>(this IList<Probability<T>> list, double chance, T value)
	{
		Debug.Assert(list != null);

		if (chance > 0)
		{
			list.Add(new Probability<T>(chance, value));
		}
	}
}
