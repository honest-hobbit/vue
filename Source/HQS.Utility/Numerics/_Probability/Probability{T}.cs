﻿namespace HQS.Utility.Numerics;

public readonly record struct Probability<T>
{
	public Probability(double chance, T value)
	{
		Ensure.That(chance, nameof(chance)).IsGte(0);

		this.Chance = chance;
		this.Value = value;
	}

	public double Chance { get; }

	public T Value { get; }
}
