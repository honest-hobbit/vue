﻿namespace HQS.Utility.Numerics;

public static class SymmetricalArray2D
{
	public static int GetArrayLength(int sideLength)
	{
		Ensure.That(sideLength, nameof(sideLength)).IsGte(0);

		return sideLength * (sideLength + 1) / 2;
	}
}
