﻿namespace HQS.Utility.Numerics;

// TODO investigate using MortonCurve in _Indices to improve data locality/cache coherence
public readonly record struct CubeArrayIndexer : IIndexer<Int3>
{
	private readonly byte exponent;

	public CubeArrayIndexer(int exponent)
	{
		Ensure.That(exponent, nameof(exponent)).IsInRange(0, MaxExponent);

		this.exponent = (byte)exponent;
	}

	// max of 10 is dictated by int.MaxValue, (2^10)^3 <= 2^31
	public const int MaxExponent = 10;

	public SquareArrayIndexer AsSquare => new SquareArrayIndexer(this.exponent);

	/// <inheritdoc />
	public Int3 Dimensions => new Int3(this.SideLength);

	/// <inheritdoc />
	public Int3 LowerBounds => Int3.Zero;

	/// <inheritdoc />
	public Int3 UpperBounds => new Int3(this.SideLength - 1);

	/// <inheritdoc />
	public int Exponent
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.exponent;
	}

	/// <inheritdoc />
	public int SideLength
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => MathUtility.PowerOf2(this.exponent);
	}

	/// <inheritdoc />
	public int Length
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => MathUtility.PowerOf2(this.exponent + this.exponent + this.exponent);
	}

	public int this[int x, int y, int z]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => x | ((y | (z << this.exponent)) << this.exponent);
	}

	/// <inheritdoc />
	public int this[Int3 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => index.X | ((index.Y | (index.Z << this.exponent)) << this.exponent);
	}

	/// <inheritdoc />
	public Int3 this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			int mask = BitMask.CreateIntWithOnesInLowerBits(this.exponent);
			int x = index & mask;
			index >>= this.exponent;
			int y = index & mask;
			int z = index >> this.exponent;
			return new Int3(x, y, z);
		}
	}

	/// <inheritdoc />
	public bool IsInBounds(Int3 index)
	{
		if (index.X < 0 || index.Y < 0 || index.Z < 0) { return false; }
		int sideLength = MathUtility.PowerOf2(this.exponent);
		return index.X < sideLength && index.Y < sideLength && index.Z < sideLength;
	}

	/// <inheritdoc />
	public override string ToString() => this.exponent.ToString();
}
