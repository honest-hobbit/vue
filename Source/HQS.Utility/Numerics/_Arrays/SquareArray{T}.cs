﻿namespace HQS.Utility.Numerics;

public readonly record struct SquareArray<T> : ISquareArray<T>
{
	public SquareArray(SquareArrayIndexer indexer)
		: this(indexer, new T[indexer.Length])
	{
	}

	public SquareArray(SquareArrayIndexer indexer, T[] array)
	{
		if (array != null)
		{
			Ensure.That(array.Length, $"{nameof(array)}.Length").Is(indexer.Length);
		}

		this.Indexer = indexer;
		this.Array = array;
	}

	public bool IsDefault => this.Array == null;

	public ReadOnlySquareArray<T> AsReadOnly => new ReadOnlySquareArray<T>(this);

	/// <inheritdoc />
	public SquareArrayIndexer Indexer { get; }

	/// <inheritdoc />
	public T[] Array { get; }

	/// <inheritdoc />
	ReadOnlyArray<T> IReadOnlySquareArray<T>.Array => new ReadOnlyArray<T>(this.Array);

	/// <inheritdoc />
	public int Count => this.Array.Length;

	/// <inheritdoc />
	public T this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.Array[index];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.Array[index] = value;
	}

	/// <inheritdoc />
	public T this[int x, int y]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.Array[this.Indexer[x, y]];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.Array[this.Indexer[x, y]] = value;
	}

	/// <inheritdoc />
	public T this[Int2 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.Array[this.Indexer[index]];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.Array[this.Indexer[index]] = value;
	}

	public ArraySegment<T>.Enumerator GetEnumerator() => this.Array.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
