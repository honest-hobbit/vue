﻿namespace HQS.Utility.Numerics;

public readonly record struct ReadOnlyCubeArray<T> : IReadOnlyCubeArray<T>
{
	private readonly T[] array;

	public ReadOnlyCubeArray(CubeArray<T> array)
	{
		this.Indexer = array.Indexer;
		this.array = array.Array;
	}

	public ReadOnlyCubeArray(CubeArrayIndexer indexer, T[] array)
	{
		if (array != null)
		{
			Ensure.That(array.Length, $"{nameof(array)}.Length").Is(indexer.Length);
		}

		this.Indexer = indexer;
		this.array = array;
	}

	public bool IsDefault => this.array == null;

	/// <inheritdoc />
	public CubeArrayIndexer Indexer { get; }

	/// <inheritdoc />
	public ReadOnlyArray<T> Array => new ReadOnlyArray<T>(this.array);

	/// <inheritdoc />
	public int Count => this.array.Length;

	/// <inheritdoc />
	public T this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.array[index];
	}

	/// <inheritdoc />
	public T this[int x, int y, int z]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.array[this.Indexer[x, y, z]];
	}

	/// <inheritdoc />
	public T this[Int3 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.array[this.Indexer[index]];
	}

	public ArraySegment<T>.Enumerator GetEnumerator() => this.array.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
