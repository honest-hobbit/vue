﻿namespace HQS.Utility.Numerics;

// TODO investigate using MortonCurve in _Indices to improve data locality/cache coherence
public readonly record struct SquareArrayIndexer : IIndexer<Int2>
{
	private readonly byte exponent;

	public SquareArrayIndexer(int exponent)
	{
		Ensure.That(exponent, nameof(exponent)).IsInRange(0, MaxExponent);

		this.exponent = (byte)exponent;
	}

	// max of 15 is dictated by int.MaxValue, (2^15)^2 <= 2^31
	public const int MaxExponent = 15;

	public CubeArrayIndexer AsCube => new CubeArrayIndexer(this.exponent);

	/// <inheritdoc />
	public Int2 Dimensions => new Int2(this.SideLength);

	/// <inheritdoc />
	public Int2 LowerBounds => Int2.Zero;

	/// <inheritdoc />
	public Int2 UpperBounds => new Int2(this.SideLength - 1);

	/// <inheritdoc />
	public int Exponent
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.exponent;
	}

	/// <inheritdoc />
	public int SideLength
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => MathUtility.PowerOf2(this.exponent);
	}

	/// <inheritdoc />
	public int Length
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => MathUtility.PowerOf2(this.exponent + this.exponent);
	}

	public int this[int x, int y]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => x | (y << this.exponent);
	}

	/// <inheritdoc />
	public int this[Int2 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => index.X | (index.Y << this.exponent);
	}

	/// <inheritdoc />
	public Int2 this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			int mask = BitMask.CreateIntWithOnesInLowerBits(this.exponent);
			int x = index & mask;
			int y = index >> this.exponent;
			return new Int2(x, y);
		}
	}

	/// <inheritdoc />
	public bool IsInBounds(Int2 index)
	{
		if (index.X < 0 || index.Y < 0) { return false; }
		int sideLength = MathUtility.PowerOf2(this.exponent);
		return index.X < sideLength && index.Y < sideLength;
	}

	/// <inheritdoc />
	public override string ToString() => this.exponent.ToString();
}
