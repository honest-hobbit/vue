﻿namespace HQS.Utility.Numerics;

public readonly record struct CubeArray<T> : ICubeArray<T>
{
	public CubeArray(CubeArrayIndexer indexer)
		: this(indexer, new T[indexer.Length])
	{
	}

	public CubeArray(CubeArrayIndexer indexer, T[] array)
	{
		if (array != null)
		{
			Ensure.That(array.Length, $"{nameof(array)}.Length").Is(indexer.Length);
		}

		this.Indexer = indexer;
		this.Array = array;
	}

	public bool IsDefault => this.Array == null;

	public ReadOnlyCubeArray<T> AsReadOnly => new ReadOnlyCubeArray<T>(this);

	/// <inheritdoc />
	public CubeArrayIndexer Indexer { get; }

	/// <inheritdoc />
	public T[] Array { get; }

	/// <inheritdoc />
	ReadOnlyArray<T> IReadOnlyCubeArray<T>.Array => new ReadOnlyArray<T>(this.Array);

	/// <inheritdoc />
	public int Count => this.Array.Length;

	/// <inheritdoc />
	public T this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.Array[index];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.Array[index] = value;
	}

	/// <inheritdoc />
	public T this[int x, int y, int z]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.Array[this.Indexer[x, y, z]];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.Array[this.Indexer[x, y, z]] = value;
	}

	/// <inheritdoc />
	public T this[Int3 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.Array[this.Indexer[index]];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.Array[this.Indexer[index]] = value;
	}

	public ArraySegment<T>.Enumerator GetEnumerator() => this.Array.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
