﻿namespace HQS.Utility.Numerics;

public readonly record struct SymmetricalArray2D<T> : IEnumerable<T>
{
	private readonly T[] values;

	public SymmetricalArray2D(int sideLength)
		: this(sideLength, new T[SymmetricalArray2D.GetArrayLength(sideLength)])
	{
	}

	public SymmetricalArray2D(int sideLength, T[] array)
	{
		Ensure.That(sideLength, nameof(sideLength)).IsGte(0);
		Ensure.That(array, nameof(array)).IsNotNull();
		Ensure.That(array.Length, $"{nameof(array)}.Length").Is(SymmetricalArray2D.GetArrayLength(sideLength));

		this.SideLength = sideLength;
		this.values = array;
	}

	public bool IsDefault => this.values == null;

	public T[] Array => this.values;

	public int Length => this.values.Length;

	public int SideLength { get; }

	public T this[int i]
	{
		get => this.values[i];
		set => this.values[i] = value;
	}

	public T this[int x, int y]
	{
		get => this.values[x > y ? (x * (x + 1) / 2) + y : (y * (y + 1) / 2) + x];
		set => this.values[x > y ? (x * (x + 1) / 2) + y : (y * (y + 1) / 2) + x] = value;
	}

	public IEnumerable<Int2> EnumerateUniqueIndices()
	{
		for (int x = 0; x < this.SideLength; x++)
		{
			// y <= x creates a triangle shaped sequence of values
			// where each column is 1 taller than the previous column
			for (int y = 0; y <= x; y++)
			{
				yield return new Int2(x, y);
			}
		}
	}

	public ArraySegment<T>.Enumerator GetEnumerator() => this.values.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
