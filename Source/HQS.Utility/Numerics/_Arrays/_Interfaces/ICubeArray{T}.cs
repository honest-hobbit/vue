﻿namespace HQS.Utility.Numerics;

public interface ICubeArray<T> : IReadOnlyCubeArray<T>
{
	new T[] Array { get; }

	new T this[int index] { get; set; }

	new T this[int x, int y, int z] { get; set; }

	new T this[Int3 index] { get; set; }
}
