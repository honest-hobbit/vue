﻿namespace HQS.Utility.Numerics;

public interface IReadOnlyCubeArray<T> : IReadOnlyList<T>
{
	CubeArrayIndexer Indexer { get; }

	ReadOnlyArray<T> Array { get; }

	T this[int x, int y, int z] { get; }

	T this[Int3 index] { get; }
}
