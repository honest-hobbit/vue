﻿namespace HQS.Utility.Numerics;

public interface IIndexer<T>
{
	T Dimensions { get; }

	T LowerBounds { get; }

	T UpperBounds { get; }

	int Exponent { get; }

	int SideLength { get; }

	int Length { get; }

	int this[T index] { get; }

	T this[int index] { get; }

	bool IsInBounds(T index);
}
