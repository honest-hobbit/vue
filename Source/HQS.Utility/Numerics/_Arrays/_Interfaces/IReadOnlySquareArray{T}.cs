﻿namespace HQS.Utility.Numerics;

public interface IReadOnlySquareArray<T> : IReadOnlyList<T>
{
	SquareArrayIndexer Indexer { get; }

	ReadOnlyArray<T> Array { get; }

	T this[int x, int y] { get; }

	T this[Int2 index] { get; }
}
