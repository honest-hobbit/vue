﻿namespace HQS.Utility.Numerics;

public interface ISquareArray<T> : IReadOnlySquareArray<T>
{
	new T[] Array { get; }

	new T this[int index] { get; set; }

	new T this[int x, int y] { get; set; }

	new T this[Int2 index] { get; set; }
}
