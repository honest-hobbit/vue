﻿namespace HQS.Utility.Numerics;

[Flags]
public enum Axes
{
	None = 0,

	NegativeX = 1,

	PositiveX = 1 << 1,

	NegativeY = 1 << 2,

	PositiveY = 1 << 3,

	NegativeZ = 1 << 4,

	PositiveZ = 1 << 5,
}
