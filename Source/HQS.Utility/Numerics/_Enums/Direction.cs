﻿namespace HQS.Utility.Numerics;

public enum Direction : byte
{
	NegativeX = 0,

	PositiveX = 1,

	NegativeY = 2,

	PositiveY = 3,

	NegativeZ = 4,

	PositiveZ = 5,
}
