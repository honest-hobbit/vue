﻿namespace EnsureThat;

public static class ParamExtensions
{
	public static void IsAssigned<T>(this in Param<T> param)
		where T : struct, IEquatable<T>
	{
		if (param.Value.Equals(default))
		{
			throw Ensure.ExceptionFactory.ArgumentException(
				ExceptionMessages.ValueTypes_IsNotDefault_Failed, param.Name, param.OptsFn);
		}
	}
}
