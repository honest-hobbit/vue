﻿namespace HQS.Utility.Collections;

internal static class CollectionContractsUtility
{
	public static void ValidateNotReadOnly<T>(ICollection<T> instance, string methodName)
	{
		Debug.Assert(instance != null);
		Debug.Assert(!methodName.IsNullOrWhiteSpace());

		if (instance.IsReadOnly)
		{
			throw new NotSupportedException($"Can not {methodName} to read only collection.");
		}
	}
}
