﻿namespace HQS.Utility.Collections;

public static class IReadOnlyListContracts
{
	public static void Indexer<T>(IReadOnlyList<T> instance, int index)
	{
		Debug.Assert(instance != null);

		Ensure.That(index, nameof(index)).IsInRange(0, instance.Count - 1);
	}
}
