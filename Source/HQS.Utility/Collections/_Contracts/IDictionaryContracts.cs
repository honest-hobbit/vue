﻿namespace HQS.Utility.Collections;

/// <summary>
/// Provides contracts for the <see cref="IDictionary{TKey, TValue}"/> interface.
/// </summary>
public static class IDictionaryContracts
{
	/// <summary>
	/// Contracts for <see cref="IDictionary{TKey, TValue}"/>'s index getter.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="key">The key.</param>
	public static void IndexerGet<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
	{
		Debug.Assert(instance != null);

		// ContainsKey contract will check that key has value
		if (!instance.ContainsKey(key))
		{
			throw new KeyNotFoundException($"Does not contain key: {key}.");
		}
	}

	/// <summary>
	/// Contracts for <see cref="IDictionary{TKey, TValue}"/>'s index setter.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="key">The key.</param>
	public static void IndexerSet<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
	{
		Debug.Assert(instance != null);

		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(IndexerSet));
		Ensure.That(key, nameof(key)).HasValue();
	}

	/// <summary>
	/// Contracts for <see cref="IDictionary{TKey, TValue}.Add(TKey, TValue)"/>.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="key">The key.</param>
	public static void Add<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
	{
		Debug.Assert(instance != null);

		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(Add));

		// ContainsKey contract will check that key has value
		if (instance.ContainsKey(key))
		{
			throw new ArgumentException($"An element with the same key already exists in the dictionary. Duplicate key: {key}.");
		}
	}

	/// <summary>
	/// Contracts for <see cref="IDictionary{TKey, TValue}.ContainsKey(TKey)"/>.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <param name="key">The key.</param>
	public static void ContainsKey<TKey>(TKey key)
	{
		Ensure.That(key, nameof(key)).HasValue();
	}

	/// <summary>
	/// Contracts for <see cref="IDictionary{TKey, TValue}.Remove(TKey)"/>.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <typeparam name="TValue">The type of the value.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="key">The key.</param>
	public static void Remove<TKey, TValue>(IDictionary<TKey, TValue> instance, TKey key)
	{
		Debug.Assert(instance != null);

		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(Remove));
		Ensure.That(key, nameof(key)).HasValue();
	}

	/// <summary>
	/// Contracts for <see cref="IDictionary{TKey, TValue}.TryGetValue(TKey, out TValue)"/>.
	/// </summary>
	/// <typeparam name="TKey">The type of the key.</typeparam>
	/// <param name="key">The key.</param>
	public static void TryGetValue<TKey>(TKey key)
	{
		Ensure.That(key, nameof(key)).HasValue();
	}
}
