﻿namespace HQS.Utility.Collections;

/// <summary>
/// Provides contracts for the <see cref="ICollection{T}"/> interface.
/// </summary>
public static class ICollectionContracts
{
	/// <summary>
	/// Contracts for <see cref="ICollection{T}.Add(T)"/>.
	/// </summary>
	/// <typeparam name="T">The type of value stored in the collection.</typeparam>
	/// <param name="instance">The instance.</param>
	public static void Add<T>(ICollection<T> instance) =>
		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(Add));

	/// <summary>
	/// Contracts for <see cref="ICollection{T}.Add(T)"/>.
	/// </summary>
	/// <typeparam name="T">The type of value stored in the collection.</typeparam>
	/// <param name="instance">The instance.</param>
	public static void Remove<T>(ICollection<T> instance) =>
		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(Remove));

	/// <summary>
	/// Contracts for <see cref="ICollection{T}.Clear"/>.
	/// </summary>
	/// <typeparam name="T">The type of value stored in the collection.</typeparam>
	/// <param name="instance">The instance.</param>
	public static void Clear<T>(ICollection<T> instance) =>
		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(Clear));

	/// <summary>
	/// Contracts for <see cref="ICollection{T}.CopyTo(T[], int)"/>.
	/// </summary>
	/// <typeparam name="T">The type of value stored in the collection.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="array">The array.</param>
	/// <param name="index">Index of the array.</param>
	public static void CopyTo<T>(ICollection<T> instance, T[] array, int index)
	{
		Debug.Assert(instance != null);

		Ensure.That(array, nameof(array)).IsNotNull();
		Ensure.That(index, nameof(index)).IsInRange(0, array.Length - instance.Count);
	}
}
