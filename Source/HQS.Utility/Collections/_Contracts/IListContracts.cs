﻿namespace HQS.Utility.Collections;

/// <summary>
/// Provides contracts for <see cref="IList{T}"/> interface.
/// </summary>
public static class IListContracts
{
	/// <summary>
	/// Contracts for <see cref="IList{T}"/>'s index getter.
	/// </summary>
	/// <typeparam name="T">The type of the values stored in the list.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="index">The index.</param>
	public static void IndexerGet<T>(IList<T> instance, int index)
	{
		Debug.Assert(instance != null);

		Ensure.That(index, nameof(index)).IsInRange(0, instance.Count - 1);
	}

	/// <summary>
	/// Contracts for <see cref="IList{T}"/>'s index setter.
	/// </summary>
	/// <typeparam name="T">The type of the values stored in the list.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="index">The index.</param>
	public static void IndexerSet<T>(IList<T> instance, int index)
	{
		Debug.Assert(instance != null);

		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(IndexerSet));
		Ensure.That(index, nameof(index)).IsInRange(0, instance.Count - 1);
	}

	/// <summary>
	/// Contracts for <see cref="IList{T}.Insert(int, T)"/>.
	/// </summary>
	/// <typeparam name="T">The type of the values stored in the list.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="index">The index.</param>
	public static void Insert<T>(IList<T> instance, int index)
	{
		Debug.Assert(instance != null);

		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(Insert));
		Ensure.That(index, nameof(index)).IsInRange(0, instance.Count - 1);
	}

	/// <summary>
	/// Contracts for <see cref="IList{T}.RemoveAt(int)"/>.
	/// </summary>
	/// <typeparam name="T">The type of the values stored in the list.</typeparam>
	/// <param name="instance">The instance.</param>
	/// <param name="index">The index.</param>
	public static void RemoveAt<T>(IList<T> instance, int index)
	{
		Debug.Assert(instance != null);

		CollectionContractsUtility.ValidateNotReadOnly(instance, nameof(RemoveAt));
		Ensure.That(index, nameof(index)).IsInRange(0, instance.Count - 1);
	}
}
