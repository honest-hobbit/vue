﻿namespace HQS.Utility.Collections;

public static class IReadOnlyTypeSetContracts
{
	public static void Contains<TBase>(Type type)
	{
		// IsAssignableToType checks not null already
		Ensure.ThatType(type, nameof(type)).IsAssignableToType(typeof(TBase));
	}

	public static void TryGet<TBase>(Type type)
	{
		// IsAssignableToType checks not null already
		Ensure.ThatType(type, nameof(type)).IsAssignableToType(typeof(TBase));
	}

	public static void Get<TBase>(Type type)
	{
		// IsAssignableToType checks not null already
		Ensure.ThatType(type, nameof(type)).IsAssignableToType(typeof(TBase));
	}

	public static void Get<TBase, T>(IReadOnlyTypeSet<TBase> instance)
		where T : TBase
	{
		Debug.Assert(instance != null);

		if (!instance.Contains<T>())
		{
			throw new KeyNotFoundException($"Does not contain Type {typeof(T).FullName}.");
		}
	}
}
