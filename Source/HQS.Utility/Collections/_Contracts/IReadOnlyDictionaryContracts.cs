﻿namespace HQS.Utility.Collections;

public static class IReadOnlyDictionaryContracts
{
	public static void Indexer<TKey, TValue>(IReadOnlyDictionary<TKey, TValue> instance, TKey key)
	{
		Debug.Assert(instance != null);

		// ContainsKey contract will check that key has value
		if (!instance.ContainsKey(key))
		{
			throw new KeyNotFoundException($"Does not contain key: {key}.");
		}
	}

	public static void ContainsKey<TKey>(TKey key)
	{
		Ensure.That(key, nameof(key)).HasValue();
	}

	public static void TryGetValue<TKey>(TKey key)
	{
		Ensure.That(key, nameof(key)).HasValue();
	}
}
