﻿namespace HQS.Utility.Collections;

public class TypeSet : TypeSet<object>
{
	public TypeSet(int count)
		: base(count)
	{
	}

	public TypeSet()
		: base()
	{
	}
}
