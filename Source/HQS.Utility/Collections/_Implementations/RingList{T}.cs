﻿namespace HQS.Utility.Collections;

[DebuggerTypeProxy(typeof(EnumerableDebugView<>))]
public class RingList<T> : IReadOnlyList<T>
{
	private const int DefaultCapacity = 4;

	private T[] values;

	/// <summary>
	/// Index of the first element in buffer.
	/// </summary>
	private int first;

	/// <summary>
	/// Index of the last element in the buffer.
	/// </summary>
	private int last;

	public RingList()
		: this(DefaultCapacity)
	{
	}

	public RingList(int capacity)
	{
		Debug.Assert(capacity >= 0);

		this.values = new T[capacity];
		this.Count = 0;
		this.ResetFirstAndLastIndices();
	}

	public RingList(IEnumerable<T> items)
	{
		Debug.Assert(items != null);

		if (items.TryGetCount(out int count))
		{
			this.values = new T[count];
			this.Count = count;
			this.ResetFirstAndLastIndices();

			int i = 0;
			foreach (var item in items)
			{
				this.values[i] = item;
				i++;
			}
		}
		else
		{
			this.values = new T[DefaultCapacity];
			this.Count = 0;
			this.ResetFirstAndLastIndices();

			foreach (var item in items)
			{
				this.PushLast(item);
			}
		}
	}

	/// <summary>
	/// Gets the maximum capacity of the buffer.
	/// </summary>
	/// <value>The maximum capacity of the buffer.</value>
	public int Capacity => this.values.Length;

	public bool IsFull => this.Count == this.Capacity;

	public bool IsEmpty => this.Count == 0;

	/// <inheritdoc />
	public int Count { get; private set; }

	/// <summary>
	/// Gets the first element in the buffer, this[0].
	/// </summary>
	/// <value>The first element.</value>
	public T First
	{
		get
		{
			Debug.Assert(!this.IsEmpty);

			return this.values[this.first];
		}
	}

	/// <summary>
	/// Gets the last element in the buffer, this[Count - 1].
	/// </summary>
	/// <value>The last element.</value>
	public T Last
	{
		get
		{
			Debug.Assert(!this.IsEmpty);

			return this.values[this.last];
		}
	}

	public T this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			IReadOnlyListContracts.Indexer(this, index);

			return this.values[this.InternalIndex(index)];
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set
		{
			IReadOnlyListContracts.Indexer(this, index);

			this.values[this.InternalIndex(index)] = value;
		}
	}

	public void Clear()
	{
		var segment = this.SegmentOne();
		int max = segment.Offset + segment.Count;
		for (int i = segment.Offset; i < max; i++)
		{
			this.values[i] = default;
		}

		segment = this.SegmentTwo();
		max = segment.Offset + segment.Count;
		for (int i = segment.Offset; i < max; i++)
		{
			this.values[i] = default;
		}

		this.Count = 0;
		this.ResetFirstAndLastIndices();
	}

	/// <summary>
	/// Pushes a new element to the front of the buffer. First and this[0] will now return this element.
	/// The buffer will expand as necessary to fit the new element.
	/// </summary>
	/// <param name="item">Item to push to the front of the buffer.</param>
	public void PushFirst(T item)
	{
		this.EnsureCapacity();
		this.Decrement(ref this.first);
		this.values[this.first] = item;
		this.Count++;
	}

	/// <summary>
	/// Pushes a new element to the back of the buffer. Last and this[Size-1] will now return this element.
	/// The buffer will expand as necessary to fit the new element.
	/// </summary>
	/// <param name="item">Item to push to the back of the buffer.</param>
	public void PushLast(T item)
	{
		this.EnsureCapacity();
		this.Increment(ref this.last);
		this.values[this.last] = item;
		this.Count++;
	}

	/// <summary>
	/// Pushes a new element to the front of the buffer. First and this[0] will now return this element.
	/// When the buffer is full, the element at Back()/this[Size-1] will be
	/// popped to allow for this new element to fit.
	/// </summary>
	/// <param name="item">Item to push to the front of the buffer.</param>
	public void PushFirstFixedCapacity(T item)
	{
		if (this.Capacity == 0)
		{
			return;
		}

		this.Decrement(ref this.first);
		this.values[this.first] = item;

		if (this.IsFull)
		{
			this.Decrement(ref this.last);
		}
		else
		{
			this.Count++;
		}
	}

	/// <summary>
	/// Pushes a new element to the back of the buffer. Last and this[Size-1] will now return this element.
	/// When the buffer is full, the element at Front()/this[0] will be
	/// popped to allow for this new element to fit.
	/// </summary>
	/// <param name="item">Item to push to the back of the buffer.</param>
	public void PushLastFixedCapacity(T item)
	{
		if (this.Capacity == 0)
		{
			return;
		}

		this.Increment(ref this.last);
		this.values[this.last] = item;

		if (this.IsFull)
		{
			this.Increment(ref this.first);
		}
		else
		{
			this.Count++;
		}
	}

	/// <summary>
	/// Removes and returns the element at the front of the buffer.
	/// </summary>
	/// <returns>The removed element.</returns>
	public T PopFirst()
	{
		Debug.Assert(!this.IsEmpty);

		var result = this.values[this.first];
		this.values[this.first] = default;
		this.Increment(ref this.first);
		this.Count--;
		return result;
	}

	/// <summary>
	/// Removes and returns the element at the back of the buffer.
	/// </summary>
	/// <returns>The removed element.</returns>
	public T PopLast()
	{
		Debug.Assert(!this.IsEmpty);

		var result = this.values[this.last];
		this.values[this.last] = default;
		this.Decrement(ref this.last);
		this.Count--;
		return result;
	}

	/// <summary>
	/// Copies the buffer contents to an array, according to the logical
	/// contents of the buffer (i.e. independent of the internal order/contents).
	/// </summary>
	/// <returns>A new array with a copy of the buffer contents.</returns>
	public T[] ToArray()
	{
		var newArray = new T[this.Count];
		int newArrayOffset = 0;

		var segment = this.SegmentOne();
		Array.Copy(segment.Array, segment.Offset, newArray, newArrayOffset, segment.Count);
		newArrayOffset += segment.Count;

		segment = this.SegmentTwo();
		Array.Copy(segment.Array, segment.Offset, newArray, newArrayOffset, segment.Count);

		return newArray;
	}

	/// <inheritdoc />
	public IEnumerator<T> GetEnumerator()
	{
		var segment = this.SegmentOne();
		int max = segment.Offset + segment.Count;
		for (int i = segment.Offset; i < max; i++)
		{
			yield return this.values[i];
		}

		segment = this.SegmentTwo();
		max = segment.Offset + segment.Count;
		for (int i = segment.Offset; i < max; i++)
		{
			yield return this.values[i];
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	/// <summary>
	/// Increments the provided index variable by one, wrapping around if necessary.
	/// </summary>
	/// <param name="index">The index.</param>
	private void Increment(ref int index)
	{
		index++;
		if (index >= this.Capacity)
		{
			index = 0;
		}
	}

	/// <summary>
	/// Decrements the provided index variable by one, wrapping around if necessary.
	/// </summary>
	/// <param name="index">The index.</param>
	private void Decrement(ref int index)
	{
		if (index <= 0)
		{
			index = this.Capacity;
		}

		index--;
	}

	/// <summary>
	/// Converts the index in the argument to an index in the buffer.
	/// </summary>
	/// <returns>
	/// The transformed index.
	/// </returns>
	/// <param name='index'>
	/// External index.
	/// </param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private int InternalIndex(int index) =>
		this.first + ((index < (this.Capacity - this.first)) ? index : index - this.Capacity);

	private void ResetFirstAndLastIndices()
	{
		this.first = 0;
		this.last = this.Count;
		this.Decrement(ref this.last);
	}

	private void EnsureCapacity()
	{
		if (this.Count < this.Capacity)
		{
			return;
		}

		var newArray = new T[this.Capacity == 0 ? DefaultCapacity : this.Capacity * 2];
		int newArrayOffset = 0;

		var segment = this.SegmentOne();
		Array.Copy(segment.Array, segment.Offset, newArray, newArrayOffset, segment.Count);
		newArrayOffset += segment.Count;

		segment = this.SegmentTwo();
		Array.Copy(segment.Array, segment.Offset, newArray, newArrayOffset, segment.Count);

		this.values = newArray;
		this.ResetFirstAndLastIndices();
	}

	// Doing ArrayOne and ArrayTwo methods returning ArraySegment<T> as seen here:
	// http://www.boost.org/doc/libs/1_37_0/libs/circular_buffer/doc/circular_buffer.html#classboost_1_1circular__buffer_1957cccdcb0c4ef7d80a34a990065818d
	// http://www.boost.org/doc/libs/1_37_0/libs/circular_buffer/doc/circular_buffer.html#classboost_1_1circular__buffer_1f5081a54afbc2dfc1a7fb20329df7d5b
	// The array is composed by at most two non-contiguous segments, the next two methods allow easy access to those.
	private ArraySegment<T> SegmentOne()
	{
		if (this.IsEmpty)
		{
			return new ArraySegment<T>(Array.Empty<T>());
		}
		else if (this.first <= this.last)
		{
			return new ArraySegment<T>(this.values, this.first, this.last - this.first + 1);
		}
		else
		{
			return new ArraySegment<T>(this.values, this.first, this.values.Length - this.first);
		}
	}

	private ArraySegment<T> SegmentTwo()
	{
		if (this.IsEmpty)
		{
			return new ArraySegment<T>(Array.Empty<T>());
		}
		else if (this.first <= this.last)
		{
			return new ArraySegment<T>(this.values, this.last, 0);
		}
		else
		{
			return new ArraySegment<T>(this.values, 0, this.last + 1);
		}
	}
}
