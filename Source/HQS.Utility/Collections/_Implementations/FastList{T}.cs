﻿namespace HQS.Utility.Collections;

// Based on: http://benbowen.blog/post/clearly_too_slow/
public sealed class FastList<T> : IReadOnlyList<T>
{
	private const int DefaultCapacity = 4;

	private int count;

	private T[] array;

	public FastList()
		: this(DefaultCapacity, int.MaxValue)
	{
	}

	public FastList(int initialCapacity)
		: this(initialCapacity, int.MaxValue)
	{
	}

	public FastList(int initialCapacity, int maxCapacity)
	{
		Ensure.That(maxCapacity, nameof(maxCapacity)).IsGte(0);
		Ensure.That(initialCapacity, nameof(initialCapacity)).IsInRange(0, maxCapacity);

		this.MaxCapacity = maxCapacity;
		this.array = new T[MathUtility.NearestEncompassingPowerOf2(
			Math.Max(initialCapacity, DefaultCapacity)).ClampUpper(maxCapacity)];
	}

	/// <inheritdoc />
	public int Count => this.count;

	public T[] Array => this.array;

	public int MaxCapacity { get; private set; }

	/// <inheritdoc />
	public T this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			IReadOnlyListContracts.Indexer(this, index);
			////Debug.Assert(index >= 0 && index < this.count);

			return this.array[index];
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set
		{
			IReadOnlyListContracts.Indexer(this, index);
			////Debug.Assert(index >= 0 && index < this.count);

			this.array[index] = value;
		}
	}

	public void FastClear() => this.count = 0;

	// favor using Add over this
	public void SetCount(int count)
	{
		Ensure.That(count, nameof(count)).IsInRange(0, this.array.Length);

		this.count = count;
	}

	public void Add(T item)
	{
		int length = this.array.Length;
		if (this.count >= length)
		{
			if (this.count == this.MaxCapacity)
			{
				throw new InvalidOperationException(
					$"Can't {nameof(this.Add)} because {nameof(this.Count)} is equal to {nameof(this.MaxCapacity)}.");
			}

			var newArray = new T[(length << 1).ClampUpper(this.MaxCapacity)];
			System.Array.Copy(this.array, newArray, this.count);
			this.array = newArray;
		}

		this.array[this.count] = item;
		this.count++;
	}

	public void AddFixedCapacity(T item)
	{
		// TODO maybe this should be Ensure
		Debug.Assert(this.count < this.array.Length);

		this.array[this.count] = item;
		this.count++;
	}

	public void EnsureCapacity(int capacity)
	{
		Ensure.That(capacity, nameof(capacity)).IsInRange(0, this.MaxCapacity);

		int length = this.array.Length;
		if (capacity <= length)
		{
			return;
		}

		do
		{
			length <<= 1;
		}
		while (length < capacity);

		var newArray = new T[length.ClampUpper(this.MaxCapacity)];
		System.Array.Copy(this.array, newArray, this.count);
		this.array = newArray;
	}

	public T[] ToArray()
	{
		var result = new T[this.count];
		System.Array.Copy(this.array, result, this.count);
		return result;
	}

	// A snapshot of Count is taken when calling this method.
	// Do not use Add or EnsureCapacity while still using the Span.
	public Span<T> AsSpan() => new Span<T>(this.array, 0, this.count);

	// A snapshot of Count is taken when calling this method.
	// Do not use Add or EnsureCapacity while still using the ReadOnlySpan.
	public ReadOnlySpan<T> AsReadOnlySpan() => new ReadOnlySpan<T>(this.array, 0, this.count);

	public ArraySegment<T>.Enumerator GetEnumerator() =>
		new ArraySegment<T>(this.array, 0, this.count).GetEnumerator();

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
