﻿namespace HQS.Utility.Collections;

public class ListIterator<T>
{
	private const int InvalidIndex = -1;

	private T value = default;

	public ListIterator(IReadOnlyList<T> list)
	{
		Debug.Assert(list != null);

		this.List = list;
		if (list.Count >= 1)
		{
			this.SetIndex(0);
		}
	}

	public IReadOnlyList<T> List { get; }

	public T Value
	{
		get
		{
			Debug.Assert(this.Index != InvalidIndex);

			return this.value;
		}
	}

	public int Index { get; private set; } = InvalidIndex;

	public bool TryMovePrevious()
	{
		if (this.Index > 0)
		{
			this.SetIndex(this.Index - 1);
			return true;
		}
		else
		{
			return false;
		}
	}

	public bool TryMoveNext()
	{
		if (this.Index != InvalidIndex && this.Index < this.List.Count - 1)
		{
			this.SetIndex(this.Index + 1);
			return true;
		}
		else
		{
			return false;
		}
	}

	public void SetIndex(int index)
	{
		Debug.Assert(this.List.IsIndexInBoundsReadOnly(index));

		this.Index = index;
		this.value = this.List[index];
	}
}
