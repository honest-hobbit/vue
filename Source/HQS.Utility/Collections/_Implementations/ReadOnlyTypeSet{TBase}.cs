﻿namespace HQS.Utility.Collections;

public class ReadOnlyTypeSet<TBase> : AbstractTypeSet<TBase>
{
	public ReadOnlyTypeSet(IEnumerable<TBase> values)
	{
		Debug.Assert(values != null);

		var dictionary = new Dictionary<Type, TBase>();
		foreach (var value in values)
		{
			dictionary.Add(value.GetType(), value);
		}

		this.Values = dictionary;
	}

	public ReadOnlyTypeSet(IEnumerable<KeyValuePair<Type, TBase>> values)
	{
		Debug.Assert(values != null);
		Debug.Assert(values.All(x =>
			x.Key != null &&
			x.Value != null &&
			x.Key.IsAssignableFrom(x.Value.GetType())));

		var dictionary = new Dictionary<Type, TBase>();
		foreach (var pair in values)
		{
			dictionary.Add(pair.Key, pair.Value);
		}

		this.Values = dictionary;
	}

	/// <inheritdoc />
	protected override IReadOnlyDictionary<Type, TBase> Values { get; }
}
