﻿namespace HQS.Utility.Collections;

// TODO this could possibly be merged together with FastList<T>
public class SimpleArrayBuilder<T>
{
	private T[] array;

	private int index;

	public void StartArray(int length)
	{
		Debug.Assert(length >= 0);

		this.array = new T[length];
		this.index = 0;
	}

	public void CopyIn(IReadOnlyList<T> values)
	{
		Debug.Assert(this.array != null);
		Debug.Assert(values != null);
		Debug.Assert(values.Count + this.index <= this.array.Length);

		for (int i = 0; i < values.Count; i++)
		{
			this.array[this.index] = values[i];
			this.index++;
		}
	}

	public void CopyIn(IList<T> values)
	{
		Debug.Assert(this.array != null);
		Debug.Assert(values != null);
		Debug.Assert(values.Count + this.index <= this.array.Length);

		for (int i = 0; i < values.Count; i++)
		{
			this.array[this.index] = values[i];
			this.index++;
		}
	}

	public void CopyIn(T[] values)
	{
		Debug.Assert(values != null);

		this.CopyIn(values, 0, values.Length);
	}

	public void CopyIn(T[] values, int offset, int length)
	{
		Debug.Assert(this.array != null);
		Debug.Assert(values != null);
		Debug.Assert(offset >= 0);
		Debug.Assert(offset < values.Length);
		Debug.Assert(length >= 0);
		Debug.Assert(length + offset <= values.Length);
		Debug.Assert(length + this.index <= this.array.Length);

		Array.Copy(values, offset, this.array, this.index, length);
		this.index += length;
	}

	public T[] GetArray()
	{
		Debug.Assert(this.array != null);

		var result = this.array;
		this.array = null;
		return result;
	}
}
