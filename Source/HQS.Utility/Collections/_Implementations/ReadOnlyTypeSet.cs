﻿namespace HQS.Utility.Collections;

public class ReadOnlyTypeSet : ReadOnlyTypeSet<object>
{
	public ReadOnlyTypeSet(IEnumerable<object> values)
		: base(values)
	{
	}

	public ReadOnlyTypeSet(IEnumerable<KeyValuePair<Type, object>> values)
		: base(values)
	{
	}
}
