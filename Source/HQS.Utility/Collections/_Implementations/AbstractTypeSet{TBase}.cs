﻿namespace HQS.Utility.Collections;

public abstract class AbstractTypeSet<TBase> : IReadOnlyTypeSet<TBase>
{
	/// <inheritdoc />
	public int Count => this.Values.Count;

	protected abstract IReadOnlyDictionary<Type, TBase> Values { get; }

	/// <inheritdoc />
	public bool Contains<T>()
		where T : TBase => this.Values.ContainsKey(typeof(T));

	/// <inheritdoc />
	public bool Contains(Type type)
	{
		IReadOnlyTypeSetContracts.Contains<TBase>(type);

		return this.Values.ContainsKey(type);
	}

	/// <inheritdoc />
	public T Get<T>()
		where T : TBase
	{
		IReadOnlyTypeSetContracts.Get<TBase, T>(this);

		return (T)this.Values[typeof(T)];
	}

	/// <inheritdoc />
	public TBase Get(Type type)
	{
		IReadOnlyTypeSetContracts.Get<TBase>(type);

		return this.Values[type];
	}

	/// <inheritdoc />
	public bool TryGet<T>(out T value)
		where T : TBase
	{
		if (this.Values.TryGetValue(typeof(T), out var result))
		{
			value = (T)result;
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}

	/// <inheritdoc />
	public bool TryGet(Type type, out TBase value)
	{
		IReadOnlyTypeSetContracts.TryGet<TBase>(type);

		if (this.Values.TryGetValue(type, out value))
		{
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}

	/// <inheritdoc />
	public Try<T> TryGet<T>()
		where T : TBase
	{
		return this.TryGet(out T value) ? Try.Value(value) : Try.None<T>();
	}

	/// <inheritdoc />
	public Try<TBase> TryGet(Type type)
	{
		IReadOnlyTypeSetContracts.TryGet<TBase>(type);

		return this.TryGet(type, out var value) ? Try.Value(value) : Try.None<TBase>();
	}

	/// <inheritdoc />
	public IEnumerator<KeyValuePair<Type, TBase>> GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
