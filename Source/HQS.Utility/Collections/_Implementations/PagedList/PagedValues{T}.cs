﻿namespace HQS.Utility.Collections;

public sealed class PagedValues<T> : IReadOnlyCollection<T>
{
	private readonly PagedList<T> parent;

	internal PagedValues(PagedList<T> parent)
	{
		Debug.Assert(parent != null);

		this.parent = parent;
	}

	/// <inheritdoc />
	public int Count => this.parent.ValuesCount;

	public PagedValuesEnumerator<T> GetEnumerator() => new PagedValuesEnumerator<T>(this.parent.First);

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => new PagedValuesEnumerator<T>(this.parent.First);

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => new PagedValuesEnumerator<T>(this.parent.First);
}
