﻿namespace HQS.Utility.Collections;

// default(PagedValuesSequence<T>) is safe and valid to use.
// Constructor parameter Page<T> first can safely be null.
public readonly record struct PagedValuesSequence<T> : IReadOnlyCollection<T>
{
	internal PagedValuesSequence(Page<T> first, int count)
	{
		Debug.Assert(count >= 0);

		this.First = first;
		this.Count = count;
	}

	public Page<T> First { get; }

	/// <inheritdoc />
	public int Count { get; }

	public void ReturnToPool(IPool<Page<T>> pool, bool clearPageValues = true)
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		PagedListUtility.ReturnAllPages(pool, this.First, clearPageValues);
	}

	public PagedValuesEnumerator<T> GetEnumerator() => new PagedValuesEnumerator<T>(this.First);

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => new PagedValuesEnumerator<T>(this.First);

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => new PagedValuesEnumerator<T>(this.First);
}
