﻿namespace HQS.Utility.Collections;

// default(PagesSequence<T>) is safe and valid to use.
// Constructor parameters Page<T> first and Page<T> last can safely be null.
public readonly record struct PagesSequence<T> : IReadOnlyCollection<Page<T>>
{
	internal PagesSequence(Page<T> first, Page<T> last, int count, int valuesCount)
	{
		Debug.Assert(count >= 0);
		Debug.Assert(valuesCount >= 0);

		this.First = first;
		this.Last = last;
		this.Count = count;
		this.ValuesCount = valuesCount;
	}

	public Page<T> First { get; }

	public Page<T> Last { get; }

	/// <inheritdoc />
	public int Count { get; }

	public int ValuesCount { get; }

	public void ReturnToPool(IPool<Page<T>> pool, bool clearPageValues = true)
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		PagedListUtility.ReturnAllPages(pool, this.First, clearPageValues);
	}

	public PagedValuesSequence<T> AsValues => new PagedValuesSequence<T>(this.First, this.ValuesCount);

	public PagesEnumerator<T> GetEnumerator() => new PagesEnumerator<T>(this.First);

	/// <inheritdoc />
	IEnumerator<Page<T>> IEnumerable<Page<T>>.GetEnumerator() => new PagesEnumerator<T>(this.First);

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => new PagesEnumerator<T>(this.First);
}
