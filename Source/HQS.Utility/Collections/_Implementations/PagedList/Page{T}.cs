﻿namespace HQS.Utility.Collections;

public sealed class Page<T> : IReadOnlyList<T>
{
	private readonly T[] values;

	public Page(int capacity)
	{
		Ensure.That(capacity, nameof(capacity)).IsGte(1);

		this.values = new T[capacity];
	}

	public Page<T> Previous { get; private set; }

	public Page<T> Next { get; private set; }

	public int Capacity => this.values.Length;

	/// <inheritdoc />
	public int Count { get; private set; }

	public bool IsFull => this.Count == this.values.Length;

	/// <inheritdoc />
	public T this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			Debug.Assert(index >= 0 && index < this.Count);

			return this.values[index];
		}
	}

	// A snapshot of Count is taken when calling this method.
	public ReadOnlySpan<T> AsReadOnlySpan() => new ReadOnlySpan<T>(this.values, 0, this.Count);

	public SinglePageEnumerator<T> GetEnumerator() => new SinglePageEnumerator<T>(this);

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => new SinglePageEnumerator<T>(this);

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => new SinglePageEnumerator<T>(this);

	internal void LinkNext(Page<T> next)
	{
		Debug.Assert(this.Next == null);
		Debug.Assert(next != null);
		Debug.Assert(next.Previous == null);

		this.Next = next;
		next.Previous = this;
	}

	internal void Add(T value)
	{
		Debug.Assert(!this.IsFull);

		this.values[this.Count] = value;
		this.Count++;
	}

	internal void Add(Page<T> other, int index, int count)
	{
		Debug.Assert(other != null);
		Debug.Assert(index >= 0);
		Debug.Assert(index < other.Count);
		Debug.Assert(count >= 1);
		Debug.Assert(count <= other.Count - index);
		Debug.Assert(count <= this.Capacity - this.Count);

		Array.Copy(other.values, index, this.values, this.Count, count);
		this.Count += count;
	}

	internal void Clear()
	{
		int count = this.Count;
		for (int i = 0; i < count; i++)
		{
			this.values[i] = default;
		}

		this.ClearFast();
	}

	internal void ClearFast()
	{
		this.Previous = null;
		this.Next = null;
		this.Count = 0;
	}
}
