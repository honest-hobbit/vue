﻿namespace HQS.Utility.Collections;

public sealed class PagedList<T> : IReadOnlyCollection<Page<T>>
{
	private readonly IPool<Page<T>> pool;

	public PagedList(IPool<Page<T>> pool)
	{
		Ensure.That(pool, nameof(pool)).IsNotNull();

		this.pool = pool;
		this.Values = new PagedValues<T>(this);
		this.Reset();
	}

	public PagedValues<T> Values { get; }

	public Page<T> First { get; private set; }

	public Page<T> Last { get; private set; }

	/// <inheritdoc />
	public int Count { get; private set; }

	internal int ValuesCount { get; private set; }

	public void Add(T value)
	{
		this.GetPageForAdding().Add(value);
		this.ValuesCount++;
	}

	public void Add(ref PagedValuesEnumerator<T> values, int count)
	{
		if (count == 0 || values.IsEmpty)
		{
			return;
		}

		Ensure.That(count, nameof(count)).IsGte(0);

		var page = this.GetPageForAdding();
		int totalCopied = values.CopyTo(page, count);

		while (totalCopied < count && !values.IsEmpty)
		{
			Debug.Assert(page.IsFull);

			page = this.GetAndAddNewLastPage();
			totalCopied += values.CopyTo(page, count - totalCopied);
		}

		Debug.Assert(totalCopied <= count);

		this.ValuesCount += totalCopied;
	}

	public void Concat(PagedList<T> other)
	{
		Ensure.That(other, nameof(other)).IsNotNull();
		if (other == this)
		{
			throw new ArgumentException(
				$"Can't {nameof(this.Concat)} a {nameof(PagedList<T>)} to itself.", nameof(other));
		}

		if (other.ValuesCount == 0)
		{
			// other has nothing so do nothing
			return;
		}

		if (this.ValuesCount == 0)
		{
			// this has nothing so return the empty page to the pool
			// and set this to essentially be a copy of other
			this.pool.Return(this.First);
			this.First = other.First;
			this.Last = other.Last;
			this.Count = other.Count;
			this.ValuesCount = other.ValuesCount;
			other.Reset();
		}
		else
		{
			// this and other are both not empty
			this.Last.LinkNext(other.First);
			this.Last = other.Last;
			this.Count += other.Count;
			this.ValuesCount += other.ValuesCount;
			other.Reset();
		}
	}

	public void Clear(bool clearPageValues = true)
	{
		PagedListUtility.ReturnAllPages(this.pool, this.First, clearPageValues);
		this.Reset();
	}

	public PagesSequence<T> DetachSequence()
	{
		if (this.ValuesCount == 0)
		{
			return default;
		}

		var result = new PagesSequence<T>(this.First, this.Last, this.Count, this.ValuesCount);
		this.Reset();
		return result;
	}

	public PagedValuesSequence<T> DetachValuesSequence() => this.DetachSequence().AsValues;

	public void ReturnToPool(PagesSequence<T> sequence, bool clearPageValues = true) =>
		PagedListUtility.ReturnAllPages(this.pool, sequence.First, clearPageValues);

	public void ReturnToPool(PagedValuesSequence<T> sequence, bool clearPageValues = true) =>
		PagedListUtility.ReturnAllPages(this.pool, sequence.First, clearPageValues);

	public PagesEnumerator<T> GetEnumerator() => new PagesEnumerator<T>(this.First);

	/// <inheritdoc />
	IEnumerator<Page<T>> IEnumerable<Page<T>>.GetEnumerator() => new PagesEnumerator<T>(this.First);

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => new PagesEnumerator<T>(this.First);

	private Page<T> GetPageFromPool()
	{
		var page = this.pool.Rent();
		Ensure.That(page, nameof(page)).IsNotNull();
		Ensure.That(page.Count, $"{nameof(page)}.{nameof(page.Count)}").Is(0);

		Debug.Assert(!page.IsFull);

		return page;
	}

	private Page<T> GetAndAddNewLastPage()
	{
		var page = this.GetPageFromPool();
		this.Last.LinkNext(page);
		this.Last = page;
		this.Count++;
		return page;
	}

	private Page<T> GetPageForAdding()
	{
		var page = this.Last;
		if (page.IsFull)
		{
			page = this.GetAndAddNewLastPage();
		}

		Debug.Assert(!page.IsFull);

		return page;
	}

	// this assigns a new node but does not return any nodes to the pool
	private void Reset()
	{
		this.ValuesCount = 0;
		this.Count = 1;
		this.First = this.GetPageFromPool();
		this.Last = this.First;
	}
}
