﻿namespace HQS.Utility.Collections;

// default(PagedValuesEnumerator<T>) is safe and valid to use.
// Constructor parameter Page<T> page can safely be null.
public struct PagedValuesEnumerator<T> : IEnumerator<T>
{
	private Page<T> page;

	private int index;

	private T current;

	internal PagedValuesEnumerator(Page<T> page)
	{
		this.page = page;
		this.index = 0;
		this.current = default;
	}

	/// <inheritdoc />
	public T Current => this.current;

	/// <inheritdoc />
	object IEnumerator.Current => this.current;

	/// <inheritdoc />
	public bool MoveNext()
	{
		if (this.page == null)
		{
			return false;
		}

		while (true)
		{
			if (this.index < this.page.Count)
			{
				this.current = this.page[this.index];
				this.index++;
				return true;
			}
			else if (this.page.Next != null)
			{
				this.page = this.page.Next;
				this.index = 0;
			}
			else
			{
				this.page = null;
				this.current = default;
				return false;
			}
		}
	}

	/// <inheritdoc />
	public void Dispose()
	{
	}

	/// <inheritdoc />
	void IEnumerator.Reset() => throw new NotSupportedException();

	public bool IsEmpty => this.page == null;

	// This also moves the enumerator forward similar to MoveNext() except it doesn't update Current.
	// If you want to use Current you must call MoveNext() again after calling this method.
	// Returns how much was copied.
	internal int CopyTo(Page<T> destination, int count)
	{
		Debug.Assert(destination != null);
		Debug.Assert(!destination.IsFull);
		Debug.Assert(count > 0);

		int maxCountToCopy = (destination.Capacity - destination.Count).ClampUpper(count);
		int totalCountCopied = 0;

		while (true)
		{
			if (this.index < this.page.Count)
			{
				int countCopied = (this.page.Count - this.index).ClampUpper(maxCountToCopy);
				destination.Add(this.page, this.index, countCopied);

				this.index += countCopied;
				totalCountCopied += countCopied;
				maxCountToCopy -= countCopied;

				if (maxCountToCopy == 0) { return totalCountCopied; }
			}
			else if (this.page.Next != null)
			{
				this.page = this.page.Next;
				this.index = 0;
			}
			else
			{
				this.page = null;
				this.current = default;
				return totalCountCopied;
			}
		}
	}
}
