﻿namespace HQS.Utility.Collections;

// default(PagesEnumerator<T>) is safe and valid to use.
// Constructor parameter Page<T> page can safely be null.
public struct PagesEnumerator<T> : IEnumerator<Page<T>>
{
	private Page<T> nextPage;

	private Page<T> currentPage;

	internal PagesEnumerator(Page<T> page)
	{
		this.nextPage = page;
		this.currentPage = null;
	}

	/// <inheritdoc />
	public Page<T> Current => this.currentPage;

	/// <inheritdoc />
	object IEnumerator.Current => this.currentPage;

	/// <inheritdoc />
	public bool MoveNext()
	{
		if (this.nextPage != null)
		{
			this.currentPage = this.nextPage;
			this.nextPage = this.currentPage.Next;
			return true;
		}
		else
		{
			this.currentPage = null;
			return false;
		}
	}

	/// <inheritdoc />
	public void Dispose()
	{
	}

	/// <inheritdoc />
	void IEnumerator.Reset() => throw new NotSupportedException();
}
