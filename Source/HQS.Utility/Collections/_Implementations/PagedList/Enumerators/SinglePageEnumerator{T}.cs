﻿namespace HQS.Utility.Collections;

// Do not use default(SinglePageEnumerator<T>).
// Constructor parameter Page<T> page must not be null.
public struct SinglePageEnumerator<T> : IEnumerator<T>
{
	private readonly Page<T> page;

	private int index;

	private T current;

	internal SinglePageEnumerator(Page<T> page)
	{
		Debug.Assert(page != null);

		this.page = page;
		this.index = 0;
		this.current = default;
	}

	/// <inheritdoc />
	public T Current => this.current;

	/// <inheritdoc />
	object IEnumerator.Current => this.current;

	/// <inheritdoc />
	public bool MoveNext()
	{
		if (this.index < this.page.Count)
		{
			this.current = this.page[this.index];
			this.index++;
			return true;
		}
		else
		{
			this.index = 0;
			this.current = default;
			return false;
		}
	}

	/// <inheritdoc />
	public void Dispose()
	{
	}

	/// <inheritdoc />
	void IEnumerator.Reset() => throw new NotSupportedException();
}
