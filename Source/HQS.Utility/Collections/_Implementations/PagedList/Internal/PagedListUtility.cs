﻿namespace HQS.Utility.Collections;

internal static class PagedListUtility
{
	// TODO this could be made faster by adding a BulkReturn API to IPool
	public static void ReturnAllPages<T>(IPool<Page<T>> pool, Page<T> firstPage, bool clearPageValues)
	{
		Debug.Assert(pool != null);

		if (clearPageValues)
		{
			while (firstPage != null)
			{
				var nextPage = firstPage.Next;
				firstPage.Clear();
				pool.Return(firstPage);
				firstPage = nextPage;
			}
		}
		else
		{
			while (firstPage != null)
			{
				var nextPage = firstPage.Next;
				firstPage.ClearFast();
				pool.Return(firstPage);
				firstPage = nextPage;
			}
		}
	}
}
