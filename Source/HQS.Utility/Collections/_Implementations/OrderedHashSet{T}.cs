﻿namespace HQS.Utility.Collections;

public class OrderedHashSet<T> : KeyedCollection<T, T>, IReadOnlySet<T>
{
	public OrderedHashSet()
		: base()
	{
	}

	public OrderedHashSet(IEnumerable<T> values)
		: base()
	{
		Debug.Assert(values != null);

		this.AddMany(values);
	}

	public OrderedHashSet(IEnumerable<T> values, IEqualityComparer<T> comparer)
		: base(comparer)
	{
		Debug.Assert(values != null);

		this.AddMany(values);
	}

	public OrderedHashSet(IEqualityComparer<T> comparer)
		: base(comparer)
	{
	}

	public new bool Add(T item)
	{
		if (this.Contains(item))
		{
			return false;
		}
		else
		{
			base.Add(item);
			return true;
		}
	}

	/// <inheritdoc />
	protected override T GetKeyForItem(T item) => item;
}
