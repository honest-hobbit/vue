﻿namespace HQS.Utility.Collections;

public class OrderedDictionary<TKey, TValue> : IOrderedDictionary<TKey, TValue>, IReadOnlyOrderedDictionary<TKey, TValue>
{
	private readonly List<KeyValuePair<TKey, TValue>> list;

	private readonly Dictionary<TKey, TValue> dictionary;

	private readonly IEqualityComparer<TKey> keyComparer;

	private readonly ReadOnlyListConverter<TKey> keys;

	private readonly ReadOnlyListConverter<TValue> values;

	public OrderedDictionary()
		: this(null)
	{
	}

	public OrderedDictionary(int capacity)
		: this(capacity, null)
	{
	}

	public OrderedDictionary(IEqualityComparer<TKey> comparer)
	{
		this.keyComparer = comparer ?? EqualityComparer<TKey>.Default;
		this.list = new List<KeyValuePair<TKey, TValue>>();
		this.dictionary = new Dictionary<TKey, TValue>(this.keyComparer);
		this.keys = new ReadOnlyListConverter<TKey>(this.list, x => x.Key, this.keyComparer);
		this.values = new ReadOnlyListConverter<TValue>(this.list, x => x.Value, null);
	}

	public OrderedDictionary(int capacity, IEqualityComparer<TKey> comparer)
	{
		Debug.Assert(capacity >= 0);

		this.keyComparer = comparer ?? EqualityComparer<TKey>.Default;
		this.list = new List<KeyValuePair<TKey, TValue>>(capacity);
		this.dictionary = new Dictionary<TKey, TValue>(capacity, this.keyComparer);
		this.keys = new ReadOnlyListConverter<TKey>(this.list, x => x.Key, this.keyComparer);
		this.values = new ReadOnlyListConverter<TValue>(this.list, x => x.Value, null);
	}

	/// <inheritdoc />
	public bool IsReadOnly => false;

	/// <inheritdoc />
	public int Count => this.list.Count;

	public IReadOnlyList<TKey> Keys => this.keys;

	public IReadOnlyList<TValue> Values => this.values;

	/// <inheritdoc />
	IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => this.keys;

	/// <inheritdoc />
	IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => this.values;

	/// <inheritdoc />
	ICollection<TKey> IDictionary<TKey, TValue>.Keys => this.keys;

	/// <inheritdoc />
	ICollection<TValue> IDictionary<TKey, TValue>.Values => this.values;

	/// <inheritdoc />
	public TValue this[TKey key]
	{
		get
		{
			IReadOnlyDictionaryContracts.Indexer(this, key);

			return this.dictionary[key];
		}

		set
		{
			IDictionaryContracts.IndexerSet(this, key);

			if (this.dictionary.ContainsKey(key))
			{
				this.dictionary[key] = value;
				this.list[this.IndexOf(key)] = new KeyValuePair<TKey, TValue>(key, value);
			}
			else
			{
				this.Add(key, value);
			}
		}
	}

	/// <inheritdoc />
	public KeyValuePair<TKey, TValue> this[int index]
	{
		get
		{
			IReadOnlyListContracts.Indexer(this, index);

			return this.list[index];
		}

		set
		{
			IListContracts.IndexerSet(this, index);
			Debug.Assert(value.Key != null);

			var oldKey = this.list[index].Key;
			if (!this.keyComparer.Equals(value.Key, oldKey))
			{
				this.dictionary.Remove(oldKey);
			}

			this.list[index] = value;
			this.dictionary[value.Key] = value.Value;
		}
	}

	/// <inheritdoc />
	public bool ContainsKey(TKey key)
	{
		IReadOnlyDictionaryContracts.ContainsKey(key);

		return this.dictionary.ContainsKey(key);
	}

	/// <inheritdoc />
	public bool TryGetValue(TKey key, out TValue value)
	{
		IReadOnlyDictionaryContracts.TryGetValue(key);

		return this.dictionary.TryGetValue(key, out value);
	}

	public int IndexOf(TKey key)
	{
		Debug.Assert(key != null);

		int count = this.list.Count;
		for (int i = 0; i < count; i++)
		{
			if (this.keyComparer.Equals(this.list[i].Key, key))
			{
				return i;
			}
		}

		return -1;
	}

	/// <inheritdoc />
	public int IndexOf(KeyValuePair<TKey, TValue> item)
	{
		Debug.Assert(item.Key != null);

		var valueComparer = EqualityComparer<TValue>.Default;

		int count = this.list.Count;
		for (int i = 0; i < count; i++)
		{
			var pair = this.list[i];
			if (this.keyComparer.Equals(pair.Key, item.Key) && valueComparer.Equals(pair.Value, item.Value))
			{
				return i;
			}
		}

		return -1;
	}

	/// <inheritdoc />
	public bool Contains(KeyValuePair<TKey, TValue> item)
	{
		Debug.Assert(item.Key != null);

		if (this.dictionary.TryGetValue(item.Key, out var value))
		{
			return EqualityComparer<TValue>.Default.Equals(value, item.Value);
		}
		else
		{
			return false;
		}
	}

	public bool Contains(TKey key, TValue value) => this.Contains(new KeyValuePair<TKey, TValue>(key, value));

	/// <inheritdoc />
	public void Add(KeyValuePair<TKey, TValue> item)
	{
		Debug.Assert(!this.ContainsKey(item.Key));

		this.dictionary.Add(item.Key, item.Value);
		this.list.Add(item);
	}

	/// <inheritdoc />
	public void Add(TKey key, TValue value) => this.Add(new KeyValuePair<TKey, TValue>(key, value));

	/// <inheritdoc />
	public void Insert(int index, KeyValuePair<TKey, TValue> item)
	{
		IListContracts.Insert(this, index);
		Debug.Assert(!this.ContainsKey(item.Key));

		this.dictionary.Add(item.Key, item.Value);
		this.list.Insert(index, item);
	}

	public void Insert(int index, TKey key, TValue value) => this.Insert(index, new KeyValuePair<TKey, TValue>(key, value));

	public TValue GetValueAt(int index)
	{
		IReadOnlyListContracts.Indexer(this, index);

		return this.list[index].Value;
	}

	public void SetValueAt(int index, TValue value)
	{
		IListContracts.IndexerSet(this, index);

		var key = this.list[index].Key;
		this.list[index] = new KeyValuePair<TKey, TValue>(key, value);
		this.dictionary[key] = value;
	}

	/// <inheritdoc />
	public void RemoveAt(int index)
	{
		IListContracts.RemoveAt(this, index);

		this.dictionary.Remove(this.list[index].Key);
		this.list.RemoveAt(index);
	}

	/// <inheritdoc />
	public bool Remove(TKey key)
	{
		IDictionaryContracts.Remove(this, key);

		if (this.dictionary.Remove(key))
		{
			this.list.RemoveAt(this.IndexOf(key));
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <inheritdoc />
	public bool Remove(KeyValuePair<TKey, TValue> item)
	{
		if (this.Contains(item))
		{
			this.dictionary.Remove(item.Key);
			this.list.RemoveAt(this.IndexOf(item.Key));
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <inheritdoc />
	public void Clear()
	{
		this.dictionary.Clear();
		this.list.Clear();
	}

	/// <inheritdoc />
	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
	{
		ICollectionContracts.CopyTo(this, array, arrayIndex);

		this.list.CopyTo(array, arrayIndex);
	}

	/// <inheritdoc />
	public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.list.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private class ReadOnlyListConverter<T> : IReadOnlyList<T>, ICollection<T>
	{
		private readonly List<KeyValuePair<TKey, TValue>> source;

		private readonly Func<KeyValuePair<TKey, TValue>, T> select;

		private readonly IEqualityComparer<T> comparer;

		public ReadOnlyListConverter(
			List<KeyValuePair<TKey, TValue>> source,
			Func<KeyValuePair<TKey, TValue>, T> select,
			IEqualityComparer<T> comparer)
		{
			Debug.Assert(source != null);
			Debug.Assert(select != null);

			this.source = source;
			this.select = select;
			this.comparer = comparer ?? EqualityComparer<T>.Default;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public bool IsReadOnly => true;

		/// <inheritdoc />
		public T this[int index]
		{
			get
			{
				IReadOnlyListContracts.Indexer(this, index);

				return this.select(this.source[index]);
			}
		}

		/// <inheritdoc />
		public bool Contains(T item)
		{
			int count = this.source.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.comparer.Equals(this.select(this.source[i]), item))
				{
					return true;
				}
			}

			return false;
		}

		/// <inheritdoc />
		public void CopyTo(T[] array, int arrayIndex)
		{
			ICollectionContracts.CopyTo(this, array, arrayIndex);

			int count = this.source.Count;
			for (int i = 0; i < count; i++)
			{
				array[arrayIndex] = this.select(this.source[i]);
				arrayIndex++;
			}
		}

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator()
		{
			int count = this.source.Count;
			for (int i = 0; i < count; i++)
			{
				yield return this.select(this.source[i]);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		/// <inheritdoc />
		void ICollection<T>.Add(T item) => throw new NotSupportedException();

		/// <inheritdoc />
		void ICollection<T>.Clear() => throw new NotSupportedException();

		/// <inheritdoc />
		bool ICollection<T>.Remove(T item) => throw new NotSupportedException();
	}
}
