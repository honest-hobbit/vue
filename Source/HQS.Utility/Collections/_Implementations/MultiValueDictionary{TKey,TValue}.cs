﻿namespace HQS.Utility.Collections;

public class MultiValueDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
	where TValue : class
{
	private readonly IDictionary<TKey, object> dictionary;

	public MultiValueDictionary()
	{
		this.dictionary = new Dictionary<TKey, object>();
	}

	public MultiValueDictionary(int capacity)
	{
		Debug.Assert(capacity >= 0);

		this.dictionary = new Dictionary<TKey, object>(capacity);
	}

	public MultiValueDictionary(IEqualityComparer<TKey> comparer)
	{
		Debug.Assert(comparer != null);

		this.dictionary = new Dictionary<TKey, object>(comparer);
	}

	public MultiValueDictionary(int capacity, IEqualityComparer<TKey> comparer)
	{
		Debug.Assert(capacity >= 0);
		Debug.Assert(comparer != null);

		this.dictionary = new Dictionary<TKey, object>(capacity, comparer);
	}

	public void Add(TKey key, TValue value)
	{
		Debug.Assert(key != null);
		Debug.Assert(value != null);

		if (this.dictionary.TryGetValue(key, out object obj))
		{
			if (obj is TValue typeSafeValue)
			{
				this.dictionary[key] = new List<TValue>
					{
						typeSafeValue,
						value,
					};
			}
			else
			{
				((List<TValue>)obj).Add(value);
			}
		}
		else
		{
			this.dictionary.Add(key, value);
		}
	}

	public void ForEach(TKey key, Action<TValue> action)
	{
		Debug.Assert(key != null);
		Debug.Assert(action != null);

		if (!this.dictionary.TryGetValue(key, out object obj))
		{
			return;
		}

		if (obj is TValue typeSafeValue)
		{
			action(typeSafeValue);
		}
		else
		{
			((List<TValue>)obj).ForEach(action);
		}
	}

	public bool RemoveAll(TKey key)
	{
		Debug.Assert(key != null);

		return this.dictionary.Remove(key);
	}

	public void Clear() => this.dictionary.Clear();

	public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
	{
		foreach (var pair in this.dictionary)
		{
			if (pair.Value is TValue typeSafeValue)
			{
				yield return KeyValuePair.Create(pair.Key, typeSafeValue);
			}
			else
			{
				foreach (var value in (List<TValue>)pair.Value)
				{
					yield return KeyValuePair.Create(pair.Key, value);
				}
			}
		}
	}

	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
