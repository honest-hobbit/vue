﻿namespace HQS.Utility.Collections;

public class CompactingQueue<T>
{
	private const float ThresholdGrowthMultiplier = 2f;

	private const float TargetEvictionPercentage = .1f;

	private readonly Func<T, bool> isValueStale;

	private readonly Queue<T> values;

	private int evictionThreshold;

	public CompactingQueue(Func<T, bool> isValueStale)
		: this(isValueStale, 32)
	{
	}

	public CompactingQueue(Func<T, bool> isValueStale, int capacity)
	{
		Debug.Assert(isValueStale != null);
		Debug.Assert(capacity >= 0);

		this.isValueStale = isValueStale;
		this.values = new Queue<T>(capacity);
		this.evictionThreshold = capacity;
	}

	public int Count => this.values.Count;

	public void Clear() => this.values.Clear();

	public void Enqueue(T value)
	{
		if (this.isValueStale(value))
		{
			return;
		}

		// check if an eviction needs to be done to make room for a new value
		int countSnapshot = this.values.Count;
		if (countSnapshot == this.evictionThreshold)
		{
			// perform an eviction by dequeuing all values and only reenqueue the not stale ones
			for (int count = 0; count < countSnapshot; count++)
			{
				var checkValue = this.values.Dequeue();
				if (!this.isValueStale(checkValue))
				{
					this.values.Enqueue(checkValue);
				}
			}

			// if not enough stale values were evicted, increase the threshold for triggering the next eviction
			if (countSnapshot - this.values.Count < (countSnapshot * TargetEvictionPercentage).Ceiling())
			{
				this.evictionThreshold = (this.evictionThreshold * ThresholdGrowthMultiplier).Ceiling();
			}
		}

		this.values.Enqueue(value);
	}

	public bool TryDequeue(out T value)
	{
		while (this.values.TryDequeue(out value))
		{
			if (!this.isValueStale(value))
			{
				return true;
			}
		}

		value = default;
		return false;
	}
}
