﻿namespace HQS.Utility.Collections;

public class TypeSet<TBase> : AbstractTypeSet<TBase>
{
	private readonly Dictionary<Type, TBase> values;

	public TypeSet(int count)
	{
		Debug.Assert(count >= 0);

		this.values = new Dictionary<Type, TBase>(count);
	}

	public TypeSet()
	{
		this.values = new Dictionary<Type, TBase>();
	}

	/// <inheritdoc />
	protected override IReadOnlyDictionary<Type, TBase> Values => this.values;

	public bool TryAdd<T>(T value)
		where T : TBase => this.values.TryAdd(typeof(T), value);

	public bool TryAdd(TBase value) => this.TryAdd(value.GetType(), value);

	public bool TryAdd(Type type, TBase value)
	{
		RequireMatchingType(type, value);

		return this.values.TryAdd(type, value);
	}

	public void Add<T>(T value)
		where T : TBase
	{
		Debug.Assert(!this.Contains<T>());

		this.values.Add(typeof(T), value);
	}

	public void Add(TBase value) => this.Add(value.GetType(), value);

	public void Add(Type type, TBase value)
	{
		RequireMatchingType(type, value);
		Debug.Assert(!this.Contains(type));

		this.values.Add(type, value);
	}

	public bool TryRemove<T>(out T value)
		where T : TBase => this.TryGet(out value) ? this.TryRemove<T>() : false;

	public bool TryRemove(Type type, out TBase value)
	{
		RequireMatchingType(type);

		return this.TryGet(type, out value) ? this.TryRemove(type) : false;
	}

	public bool TryRemove<T>()
		where T : TBase => this.values.Remove(typeof(T));

	public bool TryRemove(Type type)
	{
		RequireMatchingType(type);

		return this.values.Remove(type);
	}

	public void Clear() => this.values.Clear();

	[Conditional(CompilationSymbol.Debug)]
	private static void RequireMatchingType(Type type, TBase value)
	{
		Debug.Assert(type != null);
		Debug.Assert(value != null);
		Debug.Assert(type.IsAssignableFrom(value.GetType()));
	}

	[Conditional(CompilationSymbol.Debug)]
	private static void RequireMatchingType(Type type)
	{
		Debug.Assert(type != null);
		Debug.Assert(typeof(TBase).IsAssignableFrom(type));
	}
}
