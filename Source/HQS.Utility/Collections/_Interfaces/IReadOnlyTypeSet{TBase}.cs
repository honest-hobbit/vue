﻿namespace HQS.Utility.Collections;

public interface IReadOnlyTypeSet<TBase> : IReadOnlyCollection<KeyValuePair<Type, TBase>>
{
	bool Contains<T>()
		where T : TBase;

	bool Contains(Type type);

	bool TryGet<T>(out T value)
		where T : TBase;

	bool TryGet(Type type, out TBase value);

	Try<T> TryGet<T>()
		where T : TBase;

	Try<TBase> TryGet(Type type);

	T Get<T>()
		where T : TBase;

	TBase Get(Type type);
}
