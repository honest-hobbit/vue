﻿namespace HQS.Utility.Collections;

public interface IReadOnlyOrderedDictionary<TKey, TValue> :
	IReadOnlyDictionary<TKey, TValue>, IReadOnlyList<KeyValuePair<TKey, TValue>>
{
}
