﻿namespace HQS.Utility.Collections;

public interface IOrderedDictionary<TKey, TValue> :
	IDictionary<TKey, TValue>, IList<KeyValuePair<TKey, TValue>>
{
}
