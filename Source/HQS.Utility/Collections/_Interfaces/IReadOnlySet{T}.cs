﻿namespace HQS.Utility.Collections;

public interface IReadOnlySet<T> : IReadOnlyCollection<T>
{
	bool Contains(T value);
}
