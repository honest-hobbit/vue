﻿namespace HQS.Utility.Collections;

/// <summary>
/// Provides extension methods for <see cref="Stack{T}"/>.
/// </summary>
public static class StackExtensions
{
	public static bool TryPop<T>(this Stack<T> stack, out T result)
	{
		Debug.Assert(stack != null);

		if (stack.Count == 0)
		{
			result = default;
			return false;
		}
		else
		{
			result = stack.Pop();
			return true;
		}
	}

	public static bool TryPeek<T>(this Stack<T> stack, out T result)
	{
		Debug.Assert(stack != null);

		if (stack.Count == 0)
		{
			result = default;
			return false;
		}
		else
		{
			result = stack.Peek();
			return true;
		}
	}

	public static Try<T> TryPop<T>(this Stack<T> stack)
	{
		Debug.Assert(stack != null);

		return stack.Count == 0 ? Try.None<T>() : Try.Value(stack.Pop());
	}

	public static Try<T> TryPeek<T>(this Stack<T> stack)
	{
		Debug.Assert(stack != null);

		return stack.Count == 0 ? Try.None<T>() : Try.Value(stack.Peek());
	}

	public static void PushMany<T>(this Stack<T> stack, params T[] values)
	{
		stack.PushMany((IEnumerable<T>)values);
	}

	public static void PushMany<T>(this Stack<T> stack, IEnumerable<T> values)
	{
		Debug.Assert(stack != null);
		Debug.Assert(values != null);

		foreach (T value in values)
		{
			stack.Push(value);
		}
	}

	public static IEnumerable<T> PopAll<T>(this Stack<T> stack)
	{
		Debug.Assert(stack != null);

		if (stack.Count == 0)
		{
			return Enumerable.Empty<T>();
		}

		var values = stack.ToArray();
		stack.Clear();
		return values;
	}
}
