﻿namespace HQS.Utility.Collections;

public static class ListExtensions
{
	// EnsureCapacity will expand the list if needed but never shrink it
	public static void EnsureCapacity<T>(this List<T> list, int capacity)
	{
		Debug.Assert(list != null);
		Debug.Assert(capacity >= 0);

		if (capacity > list.Capacity)
		{
			list.Capacity = capacity;
		}
	}

	public static void AddManyMaxCapacity<T>(this List<T> list, IEnumerable<T> values, int maxCapacity)
	{
		Debug.Assert(list != null);
		Debug.Assert(values != null);
		Debug.Assert(maxCapacity >= 0);
		Debug.Assert(list.Count + values.CountExtended() <= maxCapacity);

		if (list.Capacity >= maxCapacity)
		{
			list.AddMany(values);
			return;
		}

		var newCount = list.Count + values.CountExtended();
		if (newCount > list.Capacity)
		{
			int newCapacity = list.Capacity;
			do
			{
				newCapacity = (newCapacity * 2).ClampUpper(maxCapacity);
			}
			while (newCapacity < newCount);

			list.Capacity = newCapacity;
		}

		list.AddMany(values);
	}
}
