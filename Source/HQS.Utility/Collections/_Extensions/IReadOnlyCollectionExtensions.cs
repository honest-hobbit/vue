﻿namespace HQS.Utility.Collections;

public static class IReadOnlyCollectionExtensions
{
	public static T[] ToArrayFromReadOnly<T>(this IReadOnlyCollection<T> source)
	{
		Debug.Assert(source != null);

		var result = new T[source.Count];
		source.CopyTo(result);
		return result;
	}

	public static List<T> ToListFromReadOnly<T>(this IReadOnlyCollection<T> source)
	{
		Debug.Assert(source != null);

		var result = new List<T>(source.Count);
		result.AddMany(source);
		return result;
	}

	public static IReadOnlyCollection<T> CopyReadOnlyCollection<T>(this IReadOnlyCollection<T> source)
	{
		Debug.Assert(source != null);

		return source.ToArrayFromReadOnly().AsReadOnlyCollection();
	}
}
