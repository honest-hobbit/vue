﻿namespace HQS.Utility.Collections;

/// <summary>
/// Provides extension methods for <see cref="IList{T}"/>.
/// </summary>
public static class IListExtensions
{
	#region Mutable methods

	public static void SetAllTo<T>(this IList<T> list, T value)
	{
		Debug.Assert(list != null);

		for (int index = 0; index < list.Count; index++)
		{
			list[index] = value;
		}
	}

	public static void SetAllTo<T>(this IList<T> list, Func<int, T> getValue)
	{
		Debug.Assert(list != null);
		Debug.Assert(getValue != null);

		for (int index = 0; index < list.Count; index++)
		{
			list[index] = getValue(index);
		}
	}

	/// <summary>
	/// Shuffles by randomly swapping every index in the list.
	/// Performance will suffer if not used on a list with O(1) access time.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the list.</typeparam>
	/// <param name="list">The list to be shuffled.</param>
	public static void ShuffleList<T>(this IList<T> list) => ShuffleList(list, ThreadLocalRandom.Instance);

	/// <summary>
	/// Shuffles by randomly swapping every index in the list.
	/// Performance will suffer if not used on a list with O(1) access time.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the list.</typeparam>
	/// <param name="list">The list to be shuffled.</param>
	/// <param name="random">The Random used to determine the shuffling. This can be used to allow deterministic shuffling.</param>
	public static void ShuffleList<T>(this IList<T> list, Random random)
	{
		Debug.Assert(list != null);
		Debug.Assert(random != null);

		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = random.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	public static void RemoveAtBySwappingWithLast<T>(this IList<T> list, int index)
	{
		Debug.Assert(list != null);
		Debug.Assert(list.IsIndexInBounds(index));

		int last = list.Count - 1;
		if (index == last)
		{
			list.RemoveAt(last);
			return;
		}

		list[index] = list[last];
		list.RemoveAt(last);
	}

	#endregion

	#region Read only methods

	public static IEnumerable<T> ReverseEfficient<T>(this IList<T> list)
	{
		Debug.Assert(list != null);

		for (int index = list.Count - 1; index >= 0; index--)
		{
			yield return list[index];
		}
	}

	public static IEnumerable<T> ReverseEfficientReadOnly<T>(this IReadOnlyList<T> list)
	{
		Debug.Assert(list != null);

		for (int index = list.Count - 1; index >= 0; index--)
		{
			yield return list[index];
		}
	}

	public static bool IsIndexInBounds<T>(this IList<T> list, int index)
	{
		Debug.Assert(list != null);

		return index >= 0 && index < list.Count;
	}

	public static bool IsIndexInBoundsReadOnly<T>(this IReadOnlyList<T> list, int index)
	{
		Debug.Assert(list != null);

		return index >= 0 && index < list.Count;
	}

	public static void ForEachListItem<T>(this IList<T> list, Action<T> action)
	{
		Debug.Assert(list != null);
		Debug.Assert(action != null);

		for (int index = 0; index < list.Count; index++)
		{
			action(list[index]);
		}
	}

	public static void ForEachListItemReadOnly<T>(this IReadOnlyList<T> list, Action<T> action)
	{
		Debug.Assert(list != null);
		Debug.Assert(action != null);

		for (int index = 0; index < list.Count; index++)
		{
			action(list[index]);
		}
	}

	public static void ForEachListItem<T>(this IList<T> list, Action<int, T> action)
	{
		Debug.Assert(list != null);
		Debug.Assert(action != null);

		for (int index = 0; index < list.Count; index++)
		{
			action(index, list[index]);
		}
	}

	public static void ForEachListItemReadOnly<T>(this IReadOnlyList<T> list, Action<int, T> action)
	{
		Debug.Assert(list != null);
		Debug.Assert(action != null);

		for (int index = 0; index < list.Count; index++)
		{
			action(index, list[index]);
		}
	}

	#endregion
}
