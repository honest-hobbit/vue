﻿namespace HQS.Utility.Collections;

// TODO should there be extension methods for ReadOnly to ReadOnly?
// unfortunately method names would collide unless the read only methods use different names
public static class AsReadOnlyExtensions
{
	public static IReadOnlyCollection<T> AsReadOnlyCollection<T>(this ICollection<T> collection) =>
		collection == null ? ReadOnlyCollection.Empty<T>() : ReadOnlyCollection.From(collection);

	public static IReadOnlyList<T> AsReadOnlyList<T>(this IList<T> list) =>
		list == null ? ReadOnlyList.Empty<T>() : ReadOnlyList.From(list);

	public static IReadOnlySet<T> AsReadOnlySet<T>(this ISet<T> set) =>
		set == null ? ReadOnlySet.Empty<T>() : ReadOnlySet.From(set);

	public static IReadOnlySet<TKey> KeysAsReadOnlySet<TKey, TValue>(this IDictionary<TKey, TValue> dictionary) =>
		dictionary == null ? ReadOnlySet.Empty<TKey>() : ReadOnlySet.FromKeys(dictionary);

	public static IReadOnlyDictionary<TKey, TValue> AsReadOnlyDictionary<TKey, TValue>(this IDictionary<TKey, TValue> dictionary) =>
		dictionary == null ? ReadOnlyDictionary.Empty<TKey, TValue>() : ReadOnlyDictionary.From(dictionary);
}
