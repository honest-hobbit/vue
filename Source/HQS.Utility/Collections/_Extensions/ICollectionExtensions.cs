﻿namespace HQS.Utility.Collections;

/// <summary>
/// Provides extension methods for the <see cref="ICollection{T}"/> interface.
/// </summary>
public static class ICollectionExtensions
{
	public static void Add<TKey, TValue>(this ICollection<KeyValuePair<TKey, TValue>> collection, TKey key, TValue value)
	{
		Debug.Assert(collection != null);

		collection.Add(KeyValuePair.Create(key, value));
	}

	public static void AddMany<T>(this ICollection<T> collection, params T[] values)
	{
		collection.AddMany((IEnumerable<T>)values);
	}

	/// <summary>
	/// Adds the values from the specified enumerable to the specified collection.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the collection.</typeparam>
	/// <param name="collection">The collection to add values to.</param>
	/// <param name="values">The values to add to the collection.</param>
	public static void AddMany<T>(this ICollection<T> collection, IEnumerable<T> values)
	{
		Debug.Assert(collection != null);
		Debug.Assert(values != null);

		foreach (T value in values)
		{
			collection.Add(value);
		}
	}

	/// <summary>
	/// Removes the values in the specified enumerable from the specified collection.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the collection.</typeparam>
	/// <param name="collection">The collection to remove values from.</param>
	/// <param name="values">The values to remove from the collection.</param>
	public static void RemoveMany<T>(this ICollection<T> collection, IEnumerable<T> values)
	{
		Debug.Assert(collection != null);
		Debug.Assert(values != null);

		foreach (T value in values)
		{
			collection.Remove(value);
		}
	}

	public static bool TryDequeue<T>(this ICollection<T> collection, out T result)
	{
		Debug.Assert(collection != null);

		if (collection.Count >= 1)
		{
			result = collection.First();
			return collection.Remove(result);
		}
		else
		{
			result = default;
			return false;
		}
	}
}
