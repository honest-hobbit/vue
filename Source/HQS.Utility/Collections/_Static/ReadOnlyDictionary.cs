﻿namespace HQS.Utility.Collections;

public static class ReadOnlyDictionary
{
	public static IReadOnlyDictionary<TKey, TValue> Empty<TKey, TValue>() => EmptyDictionary<TKey, TValue>.Instance;

	public static IReadOnlyDictionary<TKey, TValue> From<TKey, TValue>(IDictionary<TKey, TValue> source) =>
		new DictionaryWrapper<TKey, TValue>(source);

	public static IReadOnlyDictionary<TKey, TValue> FromReadOnly<TKey, TValue>(IReadOnlyDictionary<TKey, TValue> source) =>
		new ReadOnlyDictionaryWrapper<TKey, TValue>(source);

	public static IReadOnlyDictionary<TKey, TResult> Convert<TKey, TSource, TResult>(
		IDictionary<TKey, TSource> source, Func<TSource, TResult> converter) =>
		new DictionaryConverter<TKey, TSource, TResult>(source, converter);

	public static IReadOnlyDictionary<TKey, TResult> ConvertReadOnly<TKey, TSource, TResult>(
		IReadOnlyDictionary<TKey, TSource> source, Func<TSource, TResult> converter) =>
		new ReadOnlyDictionaryConverter<TKey, TSource, TResult>(source, converter);

	#region Private Classes

	private class EmptyDictionary<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>
	{
		private EmptyDictionary()
		{
		}

		public static IReadOnlyDictionary<TKey, TValue> Instance { get; } = new EmptyDictionary<TKey, TValue>();

		/// <inheritdoc />
		public int Count => 0;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys => Array.Empty<TKey>();

		/// <inheritdoc />
		public IEnumerable<TValue> Values => Array.Empty<TValue>();

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get
			{
				IReadOnlyDictionaryContracts.Indexer(this, key);

				throw new KeyNotFoundException();
			}
		}

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => false;

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value)
		{
			value = default;
			return false;
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			yield break;
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class DictionaryWrapper<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>
	{
		private readonly IDictionary<TKey, TValue> source;

		public DictionaryWrapper(IDictionary<TKey, TValue> source)
		{
			Debug.Assert(source != null);

			this.source = source;
			this.Keys = this.source.Keys.AsReadOnlyCollection();
			this.Values = this.source.Values.AsReadOnlyCollection();
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<TValue> Values { get; }

		/// <inheritdoc />
		public TValue this[TKey key] => this.source[key];

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value) => this.source.TryGetValue(key, out value);

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlyDictionaryWrapper<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>
	{
		private readonly IReadOnlyDictionary<TKey, TValue> source;

		public ReadOnlyDictionaryWrapper(IReadOnlyDictionary<TKey, TValue> source)
		{
			Debug.Assert(source != null);

			this.source = source;
			this.Keys = this.source.Keys.AsEnumerableOnly();
			this.Values = this.source.Values.AsEnumerableOnly();
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<TValue> Values { get; }

		/// <inheritdoc />
		public TValue this[TKey key] => this.source[key];

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value) => this.source.TryGetValue(key, out value);

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class DictionaryConverter<TKey, TSource, TResult> : IReadOnlyDictionary<TKey, TResult>
	{
		private readonly IDictionary<TKey, TSource> source;

		private readonly Func<TSource, TResult> converter;

		public DictionaryConverter(IDictionary<TKey, TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
			this.Keys = this.source.Keys.AsReadOnlyCollection();
			this.Values = ReadOnlyCollection.Convert(this.source.Values, converter);
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<TResult> Values { get; }

		/// <inheritdoc />
		public TResult this[TKey key] => this.converter(this.source[key]);

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TResult value)
		{
			if (this.source.TryGetValue(key, out var source))
			{
				value = this.converter(source);
				return true;
			}
			else
			{
				value = default;
				return false;
			}
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TResult>> GetEnumerator()
		{
			foreach (var pair in this.source)
			{
				yield return new KeyValuePair<TKey, TResult>(pair.Key, this.converter(pair.Value));
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlyDictionaryConverter<TKey, TSource, TResult> : IReadOnlyDictionary<TKey, TResult>
	{
		private readonly IReadOnlyDictionary<TKey, TSource> source;

		private readonly Func<TSource, TResult> converter;

		public ReadOnlyDictionaryConverter(IReadOnlyDictionary<TKey, TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
			this.Keys = this.source.Keys.AsEnumerableOnly();
			this.Values = this.source.Values.Select(this.converter);
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<TResult> Values { get; }

		/// <inheritdoc />
		public TResult this[TKey key] => this.converter(this.source[key]);

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TResult value)
		{
			if (this.source.TryGetValue(key, out var source))
			{
				value = this.converter(source);
				return true;
			}
			else
			{
				value = default;
				return false;
			}
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TResult>> GetEnumerator()
		{
			foreach (var pair in this.source)
			{
				yield return new KeyValuePair<TKey, TResult>(pair.Key, this.converter(pair.Value));
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	#endregion
}
