﻿namespace HQS.Utility.Collections;

public static class StructComparer
{
	public static EqualityComparer<T> Equality<T>()
		where T : struct, IEquatable<T> => EqualityComparerSingleton<T>.Instance;

	public static Comparer<T> Comparison<T>()
		where T : struct, IComparable<T> => ComparerSingleton<T>.Instance;

	private class EqualityComparerSingleton<T> : EqualityComparer<T>
		where T : struct, IEquatable<T>
	{
		private EqualityComparerSingleton()
		{
		}

		public static EqualityComparer<T> Instance { get; } = new EqualityComparerSingleton<T>();

		/// <inheritdoc />
		public override bool Equals(T a, T b) => a.Equals(b);

		/// <inheritdoc />
		public override int GetHashCode(T obj) => obj.GetHashCode();
	}

	private class ComparerSingleton<T> : Comparer<T>
		where T : struct, IComparable<T>
	{
		private ComparerSingleton()
		{
		}

		public static Comparer<T> Instance { get; } = new ComparerSingleton<T>();

		/// <inheritdoc />
		public override int Compare(T x, T y) => x.CompareTo(y);
	}
}
