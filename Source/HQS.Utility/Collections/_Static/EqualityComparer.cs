﻿using HQS.Utility.Enums;

namespace HQS.Utility.Collections;

public static class EqualityComparer
{
	public static EqualityComparer<T> ForClass<T>()
		where T : class => EqualityComparer<T>.Default;

	public static EqualityComparer<T> ForStruct<T>()
		where T : struct, IEquatable<T> => StructComparer.Equality<T>();

	public static EqualityComparer<T> ForEnum<T>()
		where T : struct, Enum => EnumEquality.Comparer<T>();

	public static EqualityComparer<T> ByIdentity<T>()
		where T : class => IdentityEquality.Comparer<T>();
}
