﻿namespace HQS.Utility.Collections;

public static class Comparer
{
	public static Comparer<T> Create<T>(Comparison<T> comparison)
	{
		Debug.Assert(comparison != null);

		return Comparer<T>.Create(comparison);
	}

	public static Comparer<T> CreateFrom<T>(Func<T, T, int> comparison)
	{
		Debug.Assert(comparison != null);

		return Comparer<T>.Create((x, y) => comparison(x, y));
	}

	public static Comparer<TSource> CreateComparable<TSource, TResult>(Func<TSource, TResult> getValue)
		where TResult : IComparable<TResult>
	{
		Debug.Assert(getValue != null);

		return Comparer<TSource>.Create((x, y) => getValue(x).CompareTo(getValue(y)));
	}

	public static Comparer<T> CreateComparable<T>()
		where T : IComparable<T>
	{
		return Comparer<T>.Create((x, y) => x.CompareTo(y));
	}
}
