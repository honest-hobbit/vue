﻿namespace HQS.Utility.Collections;

public static class ReadOnlyList
{
	public static IReadOnlyList<T> Empty<T>() => Array.Empty<T>();

	public static IReadOnlyList<T> From<T>(IList<T> source) => new ListWrapper<T>(source);

	public static IReadOnlyList<T> FromReadOnly<T>(IReadOnlyList<T> source) => new ReadOnlyListWrapper<T>(source);

	public static IReadOnlyList<TResult> Convert<TSource, TResult>(
		IList<TSource> source, Func<TSource, TResult> converter) =>
		new ListConverter<TSource, TResult>(source, converter);

	public static IReadOnlyList<TResult> ConvertReadOnly<TSource, TResult>(
		IReadOnlyList<TSource> source, Func<TSource, TResult> converter) =>
		new ReadOnlyListConverter<TSource, TResult>(source, converter);

	#region Private Classes

	private class ListWrapper<T> : IReadOnlyList<T>
	{
		private readonly IList<T> source;

		public ListWrapper(IList<T> source)
		{
			Debug.Assert(source != null);

			this.source = source;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public T this[int index] => this.source[index];

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlyListWrapper<T> : IReadOnlyList<T>
	{
		private readonly IReadOnlyList<T> source;

		public ReadOnlyListWrapper(IReadOnlyList<T> source)
		{
			Debug.Assert(source != null);

			this.source = source;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public T this[int index] => this.source[index];

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ListConverter<TSource, TResult> : IReadOnlyList<TResult>
	{
		private readonly IList<TSource> source;

		private readonly Func<TSource, TResult> converter;

		public ListConverter(IList<TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public TResult this[int index] => this.converter(this.source[index]);

		/// <inheritdoc />
		public IEnumerator<TResult> GetEnumerator()
		{
			int count = this.source.Count;
			for (int i = 0; i < count; i++)
			{
				yield return this.converter(this.source[i]);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlyListConverter<TSource, TResult> : IReadOnlyList<TResult>
	{
		private readonly IReadOnlyList<TSource> source;

		private readonly Func<TSource, TResult> converter;

		public ReadOnlyListConverter(IReadOnlyList<TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public TResult this[int index] => this.converter(this.source[index]);

		/// <inheritdoc />
		public IEnumerator<TResult> GetEnumerator()
		{
			int count = this.source.Count;
			for (int i = 0; i < count; i++)
			{
				yield return this.converter(this.source[i]);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	#endregion
}
