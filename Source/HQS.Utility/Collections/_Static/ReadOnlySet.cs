﻿namespace HQS.Utility.Collections;

public static class ReadOnlySet
{
	public static IReadOnlySet<T> Empty<T>() => EmptySet<T>.Instance;

	public static IReadOnlySet<T> From<T>(ISet<T> source) => new SetWrapper<T>(source);

	public static IReadOnlySet<TKey> FromKeys<TKey, TValue>(IDictionary<TKey, TValue> source) =>
		new DictionaryKeysWrapper<TKey, TValue>(source);

	public static IReadOnlySet<TResult> ConvertReadOnly<TSource, TResult>(
		ISet<TSource> source, IDualConverter<TSource, TResult> converter) =>
		new SetConverter<TSource, TResult>(source, converter);

	public static IReadOnlySet<TResult> ConvertReadOnly<TSource, TResult>(
		IReadOnlySet<TSource> source, IDualConverter<TSource, TResult> converter) =>
		new ReadOnlySetConverter<TSource, TResult>(source, converter);

	public static IReadOnlySet<T> CreateUnordered<T>(IEnumerable<T> source, IEqualityComparer<T> comparer = null) =>
		new HashSet<T>(source, comparer).AsReadOnlySet();

	public static IReadOnlySet<T> CreateOrdered<T>(IEnumerable<T> source, IEqualityComparer<T> comparer = null) =>
		new OrderedHashSet<T>(source, comparer);

	#region Private Classes

	private class EmptySet<T> : IReadOnlySet<T>
	{
		private EmptySet()
		{
		}

		public static IReadOnlySet<T> Instance { get; } = new EmptySet<T>();

		/// <inheritdoc />
		public int Count => 0;

		/// <inheritdoc />
		public bool Contains(T value) => false;

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator()
		{
			yield break;
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class SetWrapper<T> : IReadOnlySet<T>
	{
		private readonly ISet<T> set;

		public SetWrapper(ISet<T> set)
		{
			Debug.Assert(set != null);

			this.set = set;
		}

		/// <inheritdoc />
		public int Count => this.set.Count;

		/// <inheritdoc />
		public bool Contains(T value) => this.set.Contains(value);

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator() => this.set.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class DictionaryKeysWrapper<TKey, TValue> : IReadOnlySet<TKey>
	{
		private readonly IDictionary<TKey, TValue> source;

		public DictionaryKeysWrapper(IDictionary<TKey, TValue> source)
		{
			Debug.Assert(source != null);

			this.source = source;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public bool Contains(TKey value) => this.source.ContainsKey(value);

		/// <inheritdoc />
		public IEnumerator<TKey> GetEnumerator() => this.source.Keys.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class SetConverter<TSource, TResult> : IReadOnlySet<TResult>
	{
		private readonly ISet<TSource> set;

		private readonly IDualConverter<TSource, TResult> converter;

		public SetConverter(ISet<TSource> set, IDualConverter<TSource, TResult> converter)
		{
			Debug.Assert(set != null);
			Debug.Assert(converter != null);

			this.set = set;
			this.converter = converter;
		}

		/// <inheritdoc />
		public int Count => this.set.Count;

		/// <inheritdoc />
		public bool Contains(TResult value) => this.set.Contains(this.converter.Convert(value));

		/// <inheritdoc />
		public IEnumerator<TResult> GetEnumerator()
		{
			foreach (var value in this.set)
			{
				yield return this.converter.Convert(value);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlySetConverter<TSource, TResult> : IReadOnlySet<TResult>
	{
		private readonly IReadOnlySet<TSource> set;

		private readonly IDualConverter<TSource, TResult> converter;

		public ReadOnlySetConverter(IReadOnlySet<TSource> set, IDualConverter<TSource, TResult> converter)
		{
			Debug.Assert(set != null);
			Debug.Assert(converter != null);

			this.set = set;
			this.converter = converter;
		}

		/// <inheritdoc />
		public int Count => this.set.Count;

		/// <inheritdoc />
		public bool Contains(TResult value) => this.set.Contains(this.converter.Convert(value));

		/// <inheritdoc />
		public IEnumerator<TResult> GetEnumerator()
		{
			foreach (var value in this.set)
			{
				yield return this.converter.Convert(value);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	#endregion
}
