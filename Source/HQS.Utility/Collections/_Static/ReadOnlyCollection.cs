﻿namespace HQS.Utility.Collections;

public static class ReadOnlyCollection
{
	public static IReadOnlyCollection<T> Empty<T>() => ReadOnlyList.Empty<T>();

	public static IReadOnlyCollection<T> From<T>(ICollection<T> source) => new CollectionWrapper<T>(source);

	public static IReadOnlyCollection<T> FromReadOnly<T>(IReadOnlyCollection<T> source) => new ReadOnlyCollectionWrapper<T>(source);

	public static IReadOnlyCollection<TResult> Convert<TSource, TResult>(
		ICollection<TSource> source, Func<TSource, TResult> converter) =>
		new CollectionConverter<TSource, TResult>(source, converter);

	public static IReadOnlyCollection<TResult> ConvertReadOnly<TSource, TResult>(
		IReadOnlyCollection<TSource> source, Func<TSource, TResult> converter) =>
		new ReadOnlyCollectionConverter<TSource, TResult>(source, converter);

	#region Private Classes

	private class CollectionWrapper<T> : IReadOnlyCollection<T>
	{
		private readonly ICollection<T> source;

		public CollectionWrapper(ICollection<T> source)
		{
			Debug.Assert(source != null);

			this.source = source;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlyCollectionWrapper<T> : IReadOnlyCollection<T>
	{
		private readonly IReadOnlyCollection<T> source;

		public ReadOnlyCollectionWrapper(IReadOnlyCollection<T> source)
		{
			Debug.Assert(source != null);

			this.source = source;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerator<T> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class CollectionConverter<TSource, TResult> : IReadOnlyCollection<TResult>
	{
		private readonly ICollection<TSource> source;

		private readonly Func<TSource, TResult> converter;

		public CollectionConverter(ICollection<TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerator<TResult> GetEnumerator()
		{
			foreach (var value in this.source)
			{
				yield return this.converter(value);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlyCollectionConverter<TSource, TResult> : IReadOnlyCollection<TResult>
	{
		private readonly IReadOnlyCollection<TSource> source;

		private readonly Func<TSource, TResult> converter;

		public ReadOnlyCollectionConverter(IReadOnlyCollection<TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerator<TResult> GetEnumerator()
		{
			foreach (var value in this.source)
			{
				yield return this.converter(value);
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	#endregion
}
