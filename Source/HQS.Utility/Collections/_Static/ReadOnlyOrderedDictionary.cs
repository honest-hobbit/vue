﻿namespace HQS.Utility.Collections;

// TODO there's no FromReadOnly method yet
public static class ReadOnlyOrderedDictionary
{
	public static IReadOnlyOrderedDictionary<TKey, TValue> Empty<TKey, TValue>() => EmptyDictionary<TKey, TValue>.Instance;

	public static IReadOnlyOrderedDictionary<TKey, TValue> From<TKey, TValue>(IOrderedDictionary<TKey, TValue> source) =>
		new DictionaryWrapper<TKey, TValue>(source);

	public static IReadOnlyOrderedDictionary<TKey, TResult> Convert<TKey, TSource, TResult>(
		IOrderedDictionary<TKey, TSource> source, Func<TSource, TResult> converter) =>
		new DictionaryConverter<TKey, TSource, TResult>(source, converter);

	public static IReadOnlyOrderedDictionary<TKey, TResult> ConvertReadOnly<TKey, TSource, TResult>(
		IReadOnlyOrderedDictionary<TKey, TSource> source, Func<TSource, TResult> converter) =>
		new ReadOnlyDictionaryConverter<TKey, TSource, TResult>(source, converter);

	#region Private Classes

	private class EmptyDictionary<TKey, TValue> : IReadOnlyOrderedDictionary<TKey, TValue>
	{
		private EmptyDictionary()
		{
		}

		public static IReadOnlyOrderedDictionary<TKey, TValue> Instance { get; } = new EmptyDictionary<TKey, TValue>();

		/// <inheritdoc />
		public int Count => 0;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys => Array.Empty<TKey>();

		/// <inheritdoc />
		public IEnumerable<TValue> Values => Array.Empty<TValue>();

		/// <inheritdoc />
		public KeyValuePair<TKey, TValue> this[int index]
		{
			get
			{
				IReadOnlyListContracts.Indexer(this, index);

				throw new IndexOutOfRangeException();
			}
		}

		/// <inheritdoc />
		public TValue this[TKey key]
		{
			get
			{
				IReadOnlyDictionaryContracts.Indexer(this, key);

				throw new KeyNotFoundException();
			}
		}

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => false;

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value)
		{
			value = default;
			return false;
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			yield break;
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class DictionaryWrapper<TKey, TValue> : IReadOnlyOrderedDictionary<TKey, TValue>
	{
		private readonly IOrderedDictionary<TKey, TValue> source;

		public DictionaryWrapper(IOrderedDictionary<TKey, TValue> source)
		{
			Debug.Assert(source != null);

			this.source = source;
			this.Keys = ReadOnlyCollection.From(source.Keys);
			this.Values = ReadOnlyCollection.From(source.Values);
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<TValue> Values { get; }

		/// <inheritdoc />
		public TValue this[TKey key] => this.source[key];

		/// <inheritdoc />
		public KeyValuePair<TKey, TValue> this[int index] => this.source[index];

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TValue value) => this.source.TryGetValue(key, out value);

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.source.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class DictionaryConverter<TKey, TSource, TResult> : IReadOnlyOrderedDictionary<TKey, TResult>
	{
		private readonly IOrderedDictionary<TKey, TSource> source;

		private readonly Func<TSource, TResult> converter;

		public DictionaryConverter(IOrderedDictionary<TKey, TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
			this.Keys = this.source.Keys.AsReadOnlyCollection();
			this.Values = ReadOnlyCollection.Convert(this.source.Values, converter);
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<TResult> Values { get; }

		/// <inheritdoc />
		public KeyValuePair<TKey, TResult> this[int index]
		{
			get
			{
				var pair = this.source[index];
				return new KeyValuePair<TKey, TResult>(pair.Key, this.converter(pair.Value));
			}
		}

		/// <inheritdoc />
		public TResult this[TKey key] => this.converter(this.source[key]);

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TResult value)
		{
			if (this.source.TryGetValue(key, out var source))
			{
				value = this.converter(source);
				return true;
			}
			else
			{
				value = default;
				return false;
			}
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TResult>> GetEnumerator()
		{
			int count = this.source.Count;
			for (int i = 0; i < count; i++)
			{
				var pair = this.source[i];
				yield return new KeyValuePair<TKey, TResult>(pair.Key, this.converter(pair.Value));
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	private class ReadOnlyDictionaryConverter<TKey, TSource, TResult> : IReadOnlyOrderedDictionary<TKey, TResult>
	{
		private readonly IReadOnlyOrderedDictionary<TKey, TSource> source;

		private readonly Func<TSource, TResult> converter;

		public ReadOnlyDictionaryConverter(IReadOnlyOrderedDictionary<TKey, TSource> source, Func<TSource, TResult> converter)
		{
			Debug.Assert(source != null);
			Debug.Assert(converter != null);

			this.source = source;
			this.converter = converter;
			this.Keys = this.source.Keys.AsEnumerableOnly();
			this.Values = this.source.Values.Select(this.converter);
		}

		/// <inheritdoc />
		public int Count => this.source.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<TResult> Values { get; }

		/// <inheritdoc />
		public KeyValuePair<TKey, TResult> this[int index]
		{
			get
			{
				var pair = this.source[index];
				return new KeyValuePair<TKey, TResult>(pair.Key, this.converter(pair.Value));
			}
		}

		/// <inheritdoc />
		public TResult this[TKey key] => this.converter(this.source[key]);

		/// <inheritdoc />
		public bool ContainsKey(TKey key) => this.source.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TResult value)
		{
			if (this.source.TryGetValue(key, out var source))
			{
				value = this.converter(source);
				return true;
			}
			else
			{
				value = default;
				return false;
			}
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TResult>> GetEnumerator()
		{
			int count = this.source.Count;
			for (int i = 0; i < count; i++)
			{
				var pair = this.source[i];
				yield return new KeyValuePair<TKey, TResult>(pair.Key, this.converter(pair.Value));
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	#endregion
}
