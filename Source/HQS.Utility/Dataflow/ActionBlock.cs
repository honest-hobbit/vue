﻿namespace HQS.Utility.Dataflow;

public static class ActionBlock
{
	public static ActionBlock<T> Create<T>(Action<T> action)
	{
		Debug.Assert(action != null);

		return new ActionBlock<T>(action);
	}

	public static ActionBlock<T> Create<T>(Action<T> action, ExecutionDataflowBlockOptions options)
	{
		Debug.Assert(action != null);
		Debug.Assert(options != null);

		return new ActionBlock<T>(action, options);
	}

	public static ActionBlock<T> CreateAsync<T>(Func<T, Task> action)
	{
		Debug.Assert(action != null);

		return new ActionBlock<T>(action);
	}

	public static ActionBlock<T> CreateAsync<T>(Func<T, Task> action, ExecutionDataflowBlockOptions options)
	{
		Debug.Assert(action != null);
		Debug.Assert(options != null);

		return new ActionBlock<T>(action, options);
	}
}
