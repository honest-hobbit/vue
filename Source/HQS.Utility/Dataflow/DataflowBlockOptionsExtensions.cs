﻿namespace HQS.Utility.Dataflow;

/// <summary>
/// Provides extension methods for <see cref="DataflowBlockOptions"/>.
/// </summary>
public static class DataflowBlockOptionsExtensions
{
	public static DataflowBlockOptions Copy(this DataflowBlockOptions options)
	{
		Debug.Assert(options != null);

		var result = new DataflowBlockOptions();
		options.CopyTo(result);
		return result;
	}

	public static void CopyTo(this DataflowBlockOptions source, DataflowBlockOptions destination)
	{
		Debug.Assert(source != null);
		Debug.Assert(destination != null);

		destination.BoundedCapacity = source.BoundedCapacity;
		destination.CancellationToken = source.CancellationToken;
		destination.MaxMessagesPerTask = source.MaxMessagesPerTask;
		destination.NameFormat = source.NameFormat;
		destination.TaskScheduler = source.TaskScheduler;
	}

	public static ExecutionDataflowBlockOptions ToExecutionOptions(this DataflowBlockOptions options)
	{
		Debug.Assert(options != null);

		var result = new ExecutionDataflowBlockOptions();
		options.CopyTo(result);
		return result;
	}

	public static GroupingDataflowBlockOptions ToGroupingOptions(this DataflowBlockOptions options)
	{
		Debug.Assert(options != null);

		var result = new GroupingDataflowBlockOptions();
		options.CopyTo(result);
		return result;
	}
}
