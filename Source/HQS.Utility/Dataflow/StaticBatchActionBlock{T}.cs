﻿namespace HQS.Utility.Dataflow;

public sealed class StaticBatchActionBlock<T> : ITargetBlock<T>
{
	private readonly BatchBlock<T> buffer;

	private readonly ActionBlock<T[]> actionBlock;

	public StaticBatchActionBlock(Action<IReadOnlyList<T>> action, int batchSize)
		: this(action, batchSize, new DataflowBlockOptions())
	{
	}

	public StaticBatchActionBlock(Action<IReadOnlyList<T>> action, int batchSize, DataflowBlockOptions options)
		: this(action, null, batchSize, options)
	{
	}

	public StaticBatchActionBlock(Func<IReadOnlyList<T>, Task> action, int batchSize)
		: this(action, batchSize, new DataflowBlockOptions())
	{
	}

	public StaticBatchActionBlock(Func<IReadOnlyList<T>, Task> action, int batchSize, DataflowBlockOptions options)
		: this(null, action, batchSize, options)
	{
	}

	private StaticBatchActionBlock(
		Action<IReadOnlyList<T>> action, Func<IReadOnlyList<T>, Task> asyncAction, int batchSize, DataflowBlockOptions options)
	{
		Debug.Assert((action != null && asyncAction == null) || (action == null && asyncAction != null));
		Debug.Assert(options != null);
		Debug.Assert(batchSize > 0);
		Debug.Assert(batchSize <= options.BoundedCapacity || options.BoundedCapacity == DataflowBlockOptions.Unbounded);

		this.buffer = new BatchBlock<T>(batchSize, options.ToGroupingOptions());

		var executionOptions = options.ToExecutionOptions();
		executionOptions.MaxDegreeOfParallelism = 1;
		executionOptions.BoundedCapacity = 1;

		if (action != null)
		{
			this.actionBlock = new ActionBlock<T[]>(array => action(array.AsReadOnlyList()), executionOptions);
		}
		else
		{
			this.actionBlock = new ActionBlock<T[]>(array => asyncAction(array.AsReadOnlyList()), executionOptions);
		}

		this.buffer.LinkTo(this.actionBlock, new DataflowLinkOptions() { PropagateCompletion = true });
	}

	/// <inheritdoc />
	public Task Completion => this.actionBlock.Completion;

	/// <inheritdoc />
	public void Complete() => this.actionBlock.Complete();

	/// <inheritdoc />
	void IDataflowBlock.Fault(Exception exception) => ((IDataflowBlock)this.buffer).Fault(exception);

	/// <inheritdoc />
	DataflowMessageStatus ITargetBlock<T>.OfferMessage(
		DataflowMessageHeader messageHeader, T messageValue, ISourceBlock<T> source, bool consumeToAccept) =>
		((ITargetBlock<T>)this.buffer).OfferMessage(messageHeader, messageValue, source, consumeToAccept);
}
