﻿namespace HQS.Utility.Dataflow;

public static class BatchActionBlock
{
	private static readonly DataflowBlockOptions DefaultOptions = new DataflowBlockOptions();

	public static ITargetBlock<T> Create<T>(Action<IReadOnlyList<T>> action, int maxBatchSize, BatchMode batching) =>
		Create(action, maxBatchSize, batching, DefaultOptions);

	public static ITargetBlock<T> Create<T>(Action<IReadOnlyList<T>> action, int maxBatchSize, BatchMode batching, DataflowBlockOptions options)
	{
		Debug.Assert(action != null);
		Debug.Assert(options != null);

		switch (batching)
		{
			case BatchMode.Dynamic:
				options = options.Copy();
				options.BoundedCapacity = maxBatchSize;
				return new DynamicBatchActionBlock<T>(action, options);
			case BatchMode.Static:
				return new StaticBatchActionBlock<T>(action, maxBatchSize, options);
			default: throw InvalidEnumArgument.CreateException(nameof(batching), batching);
		}
	}

	public static ITargetBlock<T> CreateAsync<T>(Func<IReadOnlyList<T>, Task> action, int maxBatchSize, BatchMode batching) =>
		CreateAsync(action, maxBatchSize, batching, DefaultOptions);

	public static ITargetBlock<T> CreateAsync<T>(Func<IReadOnlyList<T>, Task> action, int maxBatchSize, BatchMode batching, DataflowBlockOptions options)
	{
		Debug.Assert(action != null);
		Debug.Assert(options != null);

		switch (batching)
		{
			case BatchMode.Dynamic:
				options = options.Copy();
				options.BoundedCapacity = maxBatchSize;
				return new DynamicBatchActionBlock<T>(action, options);
			case BatchMode.Static:
				return new StaticBatchActionBlock<T>(action, maxBatchSize, options);
			default: throw InvalidEnumArgument.CreateException(nameof(batching), batching);
		}
	}
}
