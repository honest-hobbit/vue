﻿////using System;

////namespace HQS.VUE
////{
////	/*
////	 * Version numbers are set to match the incremented version number of the volume when the change is made.
////	 * When a voxel chunk is changed and its adjacent chunks recontoured, their ContourDependencyVersion
////	 * is set to be the highest VoxelVersion of any adjacent chunk. Their ContourVersion is only updated
////	 * if new contours were actually created (in which case the ContourVersion is set to ContourDependencyVersion).
////	 * Otherwise only ContourDependencyVersion is updated and ContourVersion remains the same. When loading
////	 * contours, check ContourDependencyVersion against all adjacent VoxelVersions to see if any VoxelVersion
////	 * is higher. If not then the contour is up-to-date, otherwise the contour must be recontoured.
////	 * Checking ContourVersion against previous ContourVersions allows you to see if the contour did actually change.
////	 */
////	internal class MetadataChunk : IChunk<ReadOnlyMetadataChunk>
////	{
////		/// <inheritdoc />
////		public ChunkLoadingSignal Signal { get; } = new ChunkLoadingSignal();

////		/// <inheritdoc />
////		public ReadOnlyMetadataChunk AsReadOnly => new ReadOnlyMetadataChunk(this);

////		/// <inheritdoc />
////		public Guid EntityKey { get; set; }

////		public long VoxelVersion { get; set; }

////		public long ContourVersion { get; set; }

////		public long ContourDependencyVersion { get; set; }

////		/// <inheritdoc />
////		public bool IsPersisting(PersistVolume saveParts) => saveParts.SaveAnything();

////		/// <inheritdoc />
////		public void SetEmpty()
////		{
////			this.EntityKey = Guid.Empty;
////			this.VoxelVersion = 0;
////			this.ContourVersion = 0;
////			this.ContourDependencyVersion = 0;
////		}
////	}
////}
