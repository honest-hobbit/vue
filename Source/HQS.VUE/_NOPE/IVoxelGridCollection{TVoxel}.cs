﻿////using System;
////using System.Collections.Generic;
////using HQS.VUE.Assets.Stores;

////namespace HQS.VUE.Core
////{
////	public interface IVoxelGridCollection<TVoxel> : IReadOnlyDictionary<Guid, IWritableVoxelGrid<TVoxel>>
////		where TVoxel : struct, IEquatable<TVoxel>
////	{
////		IWritableVoxelGrid<TVoxel> CreateGrid<TConfig>()
////			where TConfig : IChunkConfig;

////		IWritableVoxelGrid<TVoxel> CreateGrid<TConfig>(Guid key)
////			where TConfig : IChunkConfig;

////		// this should only be used to create a VoxelGrid from a saved file
////		// do not create a brand new  empty VoxelGrid using this method
////		IWritableVoxelGrid<TVoxel> CreateGrid<TConfig>(VoxelGridRecord record)
////			where TConfig : IChunkConfig;

////		IWritableVoxelGrid<TVoxel> CreateGrid<TConfig>(TConfig config)
////			where TConfig : IChunkConfig;

////		IWritableVoxelGrid<TVoxel> CreateGrid<TConfig>(Guid key, TConfig config)
////			where TConfig : IChunkConfig;

////		// TODO maybe there should be a VoxelGridRecord<TConfig> to combine both???????????????????????
////		// this should only be used to create a VoxelGrid from a saved file
////		// do not create a brand new  empty VoxelGrid using this method
////		IWritableVoxelGrid<TVoxel> CreateGrid<TConfig>(VoxelGridRecord record, TConfig config)
////			where TConfig : IChunkConfig;

////		void DiscardChanges();
////	}
////}
