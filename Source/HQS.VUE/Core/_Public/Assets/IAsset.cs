﻿namespace HQS.VUE.Core;

public interface IAsset : IKeyed<Guid>
{
	DateTime Created { get; }

	DateTime LastModified { get; }

	Guid VersionStamp { get; }

	long VersionCount { get; }

	string DisplayName { get; }

	bool AutoSave { get; }

	// returns true if the asset was changed in a previous job without also saving
	// those changes at the end of that job, or if this instance is a Writer and
	// any changes have been made to it during this job. Otherwise false
	bool HasUnsavedChanges { get; }

	object Store { get; }
}
