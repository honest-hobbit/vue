﻿namespace HQS.VUE.Core;

public class MultipleAssetWritersException : Exception
{
	public MultipleAssetWritersException()
	{
	}

	public MultipleAssetWritersException(string message)
		: base(message)
	{
	}

	public MultipleAssetWritersException(string message, Exception innerException)
		: base(message, innerException)
	{
	}

	protected MultipleAssetWritersException(SerializationInfo info, StreamingContext context)
		: base(info, context)
	{
	}
}
