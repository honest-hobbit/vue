﻿namespace HQS.VUE.Core;

public interface IAssetWriter : IAsset
{
	new string DisplayName { get; set; }

	new bool AutoSave { get; set; }

	bool ToBeIgnored { get; set; }

	bool ToBeSaved { get; set; }

	bool ToBeUnloaded { get; set; }

	bool ToBeDeleted { get; set; }

	// assigning a new key treats this as a new asset
	// the original asset with its original key will continue to exist
	void AssignNewKey();

	void SetStore(IAssetStore store);
}
