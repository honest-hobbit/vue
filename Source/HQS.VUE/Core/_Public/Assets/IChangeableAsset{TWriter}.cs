﻿namespace HQS.VUE.Core;

public interface IChangeableAsset<TWriter> : IAsset
	where TWriter : IAssetWriter
{
	TWriter CreateChanges();
}
