﻿namespace HQS.VUE.Core;

public interface IAssetWriter<TPrevious> : IAssetWriter
	where TPrevious : IAsset
{
	TPrevious Previous { get; }
}
