﻿namespace HQS.VUE.Core;

public interface IJob
{
	void RunOn(IJobAssets assets);
}
