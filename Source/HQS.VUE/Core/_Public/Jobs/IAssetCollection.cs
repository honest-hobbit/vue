﻿namespace HQS.VUE.Core;

public interface IAssetCollection
{
	// TODO this is only needed in JobAssets to assign the correct key
	// maybe there's a better way to handle this
	// also by returning null this factory will be treated as the NullAssetFactory
	Type CollectionType { get; }

	Type AssetType { get; }
}
