﻿namespace HQS.VUE.Core;

public interface ITypePool : IKeyed<PooledType>
{
	KeyValuePair<PooledType, PoolDiagnostics> Diagnostics { get; }

	// The pool may have a bounded capacity or it may be used by multiple threads
	// so fewer than count elements may be created.
	// Returns how many elements were created.
	int Create(int count);
}
