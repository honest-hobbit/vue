﻿namespace HQS.VUE.Core;

public static class CorePooledTypes
{
	public static PooledType RunJobCommand { get; } = new PooledType(
		new Guid("b46d0d5f-47c2-49c3-adf7-49645fe72296"),
		CreateName(nameof(RunJobCommand)),
		typeof(RunJobCommand));

	public static IEnumerable<PooledType> Enumerate()
	{
		yield return RunJobCommand;
	}

	private static string CreateName(string name) => $"{nameof(Core)}.{name}";
}
