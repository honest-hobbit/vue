﻿namespace HQS.VUE.Core;

public class PooledType : IKeyed<Guid>
{
	internal PooledType(Guid key, string displayName, Type resourceType)
	{
		Debug.Assert(key != Guid.Empty);
		Debug.Assert(!displayName.IsNullOrWhiteSpace());
		Debug.Assert(resourceType != null);

		this.Key = key;
		this.DisplayName = displayName;
		this.ResourceType = resourceType;
	}

	/// <inheritdoc />
	public Guid Key { get; }

	public string DisplayName { get; }

	internal Type ResourceType { get; }

	/// <inheritdoc />
	public override string ToString() => this.DisplayName;
}
