﻿namespace HQS.VUE.Core;

public interface ITypePoolCollection :
	IReadOnlyDictionary<PooledType, ITypePool>,
	IReadOnlyDictionary<Guid, ITypePool>
{
	new int Count { get; }

	new IEnumerable<PooledType> Keys { get; }

	new IEnumerable<ITypePool> Values { get; }

	new IEnumerator<KeyValuePair<PooledType, ITypePool>> GetEnumerator();
}
