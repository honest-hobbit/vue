﻿namespace HQS.VUE.Core;

public interface IJobSubmitter
{
	Guid Submit(IJob job);

	Guid Submit<TJob>(Pinned<TJob> job)
		where TJob : class, IJob;

	// this will dispose the pin passed in
	Guid Submit<TJob>(Pin<TJob> job)
		where TJob : class, IJob;

	Guid Submit(IJob job, out Task<IJob> completion);

	// the pin returned by completion Task must be disposed by the caller after awaiting
	Guid Submit<TJob>(Pinned<TJob> job, out Task<Pin<TJob>> completion)
		where TJob : class, IJob;

	// this will dispose the pin passed in
	// the pin returned by completion Task must be disposed by the caller after awaiting
	Guid Submit<TJob>(Pin<TJob> job, out Task<Pin<TJob>> completion)
		where TJob : class, IJob;
}
