﻿namespace HQS.VUE.Core;

public class VoxelUniverseBuilder
{
	private readonly List<Action<Container>> actions = new List<Action<Container>>();

	public IVoxelUniverse BuildUniverse()
	{
		var container = new Container();
		container.RegisterVoxelUniverseCore();

		for (int i = 0; i < this.actions.Count; i++)
		{
			this.actions[i](container);
		}

		container.VerifyInDebugOnly();

		return new VoxelUniverse(container);
	}

	internal void AddRegistrations(Action<Container> addRegistrations)
	{
		Ensure.That(addRegistrations, nameof(addRegistrations)).IsNotNull();

		this.actions.Add(addRegistrations);
	}

	private class VoxelUniverse : AbstractCompletable, IVoxelUniverse
	{
		private readonly Container container;

		private readonly VoxelUniverseEngine engine;

		public VoxelUniverse(Container container)
		{
			Debug.Assert(container != null);

			this.container = container;
			this.engine = this.container.GetInstance<VoxelUniverseEngine>();
		}

		/// <inheritdoc />
		public IDiagnostics Diagnostics => this.engine.Diagnostics;

		/// <inheritdoc />
		public IJobSubmitter Jobs => this.engine.Jobs;

		/// <inheritdoc />
		protected override async Task CompleteAsync()
		{
			try
			{
				this.engine.Complete();
				await this.engine.Completion.DontMarshallContext();
			}
			finally
			{
				this.container.Dispose();
			}
		}
	}
}
