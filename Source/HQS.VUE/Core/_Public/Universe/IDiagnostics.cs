﻿namespace HQS.VUE.Core;

public interface IDiagnostics
{
	IObservable<Exception> ErrorOccurred { get; }

	ITypePoolCollection Pools { get; }
}
