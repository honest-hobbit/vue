﻿namespace HQS.VUE.Core;

public interface IVoxelUniverse : IAsyncCompletable
{
	IDiagnostics Diagnostics { get; }

	IJobSubmitter Jobs { get; }
}
