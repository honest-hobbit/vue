﻿namespace HQS.VUE.Core;

public readonly struct ChunkDocument<TInt> : IKeyed<Guid>, IEquatable<ChunkDocument<TInt>>
	where TInt : IEquatable<TInt>
{
	public ChunkDocument(Guid key, Guid assetKey, TInt location, long version, Stream body)
		: this(new ChunkHeader<TInt>(key, assetKey, location, version), body)
	{
	}

	public ChunkDocument(ChunkHeader<TInt> header, Stream body)
	{
		this.Header = header;
		this.Body = body;
	}

	/// <inheritdoc />
	public Guid Key => this.Header.Key;

	public ChunkHeader<TInt> Header { get; }

	public Stream Body { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(ChunkDocument<TInt> other) => this.Key.Equals(other.Key);

	/// <inheritdoc />
	public override int GetHashCode() => this.Key.GetHashCode();

	public static bool operator ==(ChunkDocument<TInt> left, ChunkDocument<TInt> right) => left.Equals(right);

	public static bool operator !=(ChunkDocument<TInt> left, ChunkDocument<TInt> right) => !(left == right);
}
