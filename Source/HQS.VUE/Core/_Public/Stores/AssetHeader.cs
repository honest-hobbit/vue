﻿namespace HQS.VUE.Core;

public readonly struct AssetHeader : IKeyed<Guid>, IEquatable<AssetHeader>
{
	public AssetHeader(Guid key, string assetType, Stream data)
	{
		this.Key = key;
		this.AssetType = assetType;
		this.Data = data;
	}

	/// <inheritdoc />
	public Guid Key { get; }

	public string AssetType { get; }

	public Stream Data { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(AssetHeader other) => this.Key == other.Key;

	/// <inheritdoc />
	public override int GetHashCode() => this.Key.GetHashCode();

	public static bool operator ==(AssetHeader left, AssetHeader right) => left.Equals(right);

	public static bool operator !=(AssetHeader left, AssetHeader right) => !(left == right);
}
