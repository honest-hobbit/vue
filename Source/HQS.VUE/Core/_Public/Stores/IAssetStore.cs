﻿namespace HQS.VUE.Core;

// TODO incomplete tasks
// - copy asset from 1 store to another
// - get chunk headers newer than certain version?
// - get assets newer than certain version / date?
// TODO - Guid version history so you can find at what point a resource desyncs from source control
// Guid Version History System -
// Every time a new version of the object is saved the previous Guid version value is saved in a list of Guid versions.
// That way when you want to check at what point 2 copies of a resource diverged from one another you can look
// back through all the previous version Guids to see how many changes you're missing.
// The chunks only save the version number of when they were last changed. By finding the lastest Guid version that
// matches the version you have you can find all chunks that have a version number greater than or equal to that and
// download / recontour / etc just those chunks.
public interface IAssetStore : IVisiblyDisposable
{
	void GetAllAssetHeaders(ICollection<AssetHeader> assets, PagingArgs paging);

	void GetAssetHeaders(
		string assetType, ICollection<AssetHeader> assets, PagingArgs paging);

	void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int2>> chunks, PagingArgs paging);

	void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int3>> chunks, PagingArgs paging);

	bool TryGetAsset(Guid assetKey, out AssetDocument asset, bool getHeaderData, bool getBodyData);

	bool TryGetChunk(Guid assetKey, Int2 location, out ChunkDocument<Int2> chunk);

	bool TryGetChunk(Guid assetKey, Int3 location, out ChunkDocument<Int3> chunk);

	// asset.Header.Data and asset.Body must not be null
	void UpdateAsset(AssetDocument asset);

	// asset.Header.Data and asset.Body must not be null
	void UpdateAsset(AssetDocument asset, IReadOnlyList<ChunkDocument<Int2>> chunks);

	// asset.Header.Data and asset.Body must not be null
	void UpdateAsset(AssetDocument asset, IReadOnlyList<ChunkDocument<Int3>> chunks);

	void DeleteAsset(Guid assetKey);

	void DeleteAllAssets();
}
