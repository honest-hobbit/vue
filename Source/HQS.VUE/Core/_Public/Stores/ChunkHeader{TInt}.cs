﻿namespace HQS.VUE.Core;

public readonly struct ChunkHeader<TInt> : IKeyed<Guid>, IEquatable<ChunkHeader<TInt>>
	where TInt : IEquatable<TInt>
{
	public ChunkHeader(Guid key, Guid assetKey, TInt location, long version)
	{
		this.Key = key;
		this.AssetKey = assetKey;
		this.Location = location;
		this.Version = version;
	}

	/// <inheritdoc />
	public Guid Key { get; }

	public Guid AssetKey { get; }

	public TInt Location { get; }

	public long Version { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(ChunkHeader<TInt> other) => this.Key == other.Key;

	/// <inheritdoc />
	public override int GetHashCode() => this.Key.GetHashCode();

	public static bool operator ==(ChunkHeader<TInt> left, ChunkHeader<TInt> right) => left.Equals(right);

	public static bool operator !=(ChunkHeader<TInt> left, ChunkHeader<TInt> right) => !(left == right);
}
