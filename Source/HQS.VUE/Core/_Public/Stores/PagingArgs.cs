﻿namespace HQS.VUE.Core;

public struct PagingArgs
{
	public Guid FirstKey;

	public int Count;

	public bool Reverse;

	public PagingArgs(Guid firstKey, int count, bool reverse)
	{
		this.FirstKey = firstKey;
		this.Count = count;
		this.Reverse = reverse;
	}

	public static PagingArgs GetAll => new PagingArgs()
	{
		FirstKey = Guid.Empty,
		Count = int.MaxValue,
		Reverse = false,
	};
}
