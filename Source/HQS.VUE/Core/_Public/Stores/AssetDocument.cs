﻿namespace HQS.VUE.Core;

public readonly struct AssetDocument : IKeyed<Guid>, IEquatable<AssetDocument>
{
	public AssetDocument(Guid key, string assetType, Stream header, Stream body)
		: this(new AssetHeader(key, assetType, header), body)
	{
	}

	public AssetDocument(AssetHeader header, Stream body)
	{
		this.Header = header;
		this.Body = body;
	}

	/// <inheritdoc />
	public Guid Key => this.Header.Key;

	public AssetHeader Header { get; }

	public Stream Body { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(AssetDocument other) => this.Key == other.Key;

	/// <inheritdoc />
	public override int GetHashCode() => this.Key.GetHashCode();

	public static bool operator ==(AssetDocument left, AssetDocument right) => left.Equals(right);

	public static bool operator !=(AssetDocument left, AssetDocument right) => !(left == right);
}
