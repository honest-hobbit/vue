﻿namespace HQS.VUE.Core;

internal class MainCommandProcessor : AbstractCommandProcessor<IMainCommand>
{
	public MainCommandProcessor(IReporter<Exception> errorReporter)
		: base(errorReporter, isParallel: false)
	{
	}
}
