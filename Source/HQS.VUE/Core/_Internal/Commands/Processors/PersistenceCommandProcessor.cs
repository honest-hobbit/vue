﻿namespace HQS.VUE.Core;

internal class PersistenceCommandProcessor : AbstractCommandProcessor<IPersistenceCommand>
{
	public PersistenceCommandProcessor(IReporter<Exception> errorReporter)
		: base(errorReporter, isParallel: false)
	{
	}
}
