﻿namespace HQS.VUE.Core;

internal abstract class AbstractCommandProcessor<TMarker> : ICommandProcessor<TMarker>, IAsyncCompletable
	where TMarker : class, IRunnable
{
	private readonly IReporter<Exception> errorReporter;

	private readonly IConsumerQueue<Pin<TMarker>> queue;

	public AbstractCommandProcessor(IReporter<Exception> errorReporter, bool isParallel)
	{
		Debug.Assert(errorReporter != null);

		this.errorReporter = errorReporter;
		this.queue = ConsumerQueue.Create<Pin<TMarker>>(
			this.RunCommand, isParallel ? DataflowBlockOptions.Unbounded : 1);
	}

	/// <inheritdoc />
	public Task Completion => this.queue.Completion;

	/// <inheritdoc />
	public bool IsCompleting => this.queue.IsCompleting;

	/// <inheritdoc />
	public void Complete() => this.queue.Complete();

	/// <inheritdoc />
	public void Submit<TCommand>(Pinned<TCommand> command)
		where TCommand : class, TMarker => this.Submit(command.CreatePin());

	/// <inheritdoc />
	public void Submit<TCommand>(Pin<TCommand> command)
		where TCommand : class, TMarker
	{
		Debug.Assert(!this.IsCompleting);
		Debug.Assert(command.IsPinned);

		this.queue.Add(command.CastTo<TMarker>());
	}

	private void RunCommand(Pin<TMarker> pooled)
	{
		try
		{
			pooled.Value.Run();
		}
		catch (Exception error)
		{
			this.errorReporter.Report(error);
			throw;
		}
		finally
		{
			pooled.Dispose();
		}
	}
}
