﻿namespace HQS.VUE.Core;

internal class ParallelCommandProcessor : AbstractCommandProcessor<IParallelCommand>
{
	public ParallelCommandProcessor(IReporter<Exception> errorReporter)
		: base(errorReporter, isParallel: true)
	{
	}
}
