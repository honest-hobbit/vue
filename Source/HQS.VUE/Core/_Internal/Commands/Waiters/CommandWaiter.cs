﻿namespace HQS.VUE.Core;

internal class CommandWaiter : AbstractCommandWaiter<IWaitable>
{
	public void Add<TCommand>(Pinned<TCommand> pin)
		where TCommand : class, IWaitable
	{
		Debug.Assert(!this.HasBeenWaited);
		Debug.Assert(pin.IsPinned);

		base.Add(pin.CastTo<IWaitable>().CreatePin());
	}

	public void Add<TCommand>(Pin<TCommand> pin)
		where TCommand : class, IWaitable
	{
		Debug.Assert(!this.HasBeenWaited);
		Debug.Assert(pin.IsPinned);

		base.Add(pin.CastTo<IWaitable>());
	}
}
