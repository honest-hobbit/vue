﻿namespace HQS.VUE.Core;

internal abstract class AbstractCommandWaiter<TCommand> : IWaitable
	where TCommand : class, IWaitable
{
	private readonly List<Pin<TCommand>> pins = new List<Pin<TCommand>>();

	private Exception exception;

	public bool HasBeenWaited { get; private set; }

	public int Count => this.pins.Count;

	protected IReadOnlyList<Pin<TCommand>> Pins => this.pins;

	public void Add(Pinned<TCommand> pin)
	{
		Debug.Assert(!this.HasBeenWaited);
		Debug.Assert(pin.IsPinned);

		this.pins.Add(pin.CreatePin());
	}

	public void Add(Pin<TCommand> pin)
	{
		Debug.Assert(!this.HasBeenWaited);
		Debug.Assert(pin.IsPinned);

		this.pins.Add(pin);
	}

	public virtual void Clear()
	{
		this.pins.DisposeAllAndClear();
		this.HasBeenWaited = false;
		this.exception = null;
	}

	/// <inheritdoc />
	public virtual void Wait()
	{
		if (this.SkipWaiting())
		{
			return;
		}

		List<Exception> exceptions = null;

		int max = this.pins.Count;
		for (int i = 0; i < max; i++)
		{
			try
			{
				this.pins[i].Value.Wait();
			}
			catch (Exception error)
			{
				exceptions ??= new List<Exception>();
				exceptions.Add(error);
			}
		}

		this.SetWaited(exceptions);
	}

	protected bool SkipWaiting()
	{
		if (this.HasBeenWaited)
		{
			if (this.exception != null)
			{
				throw this.exception;
			}

			return true;
		}

		return false;
	}

	protected void SetWaited(List<Exception> exceptions)
	{
		Debug.Assert(!this.HasBeenWaited);

		this.HasBeenWaited = true;
		if (exceptions != null)
		{
			this.exception = new AggregateException(exceptions);
			throw this.exception;
		}
	}
}
