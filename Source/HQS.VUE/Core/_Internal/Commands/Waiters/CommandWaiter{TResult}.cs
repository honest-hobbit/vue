﻿namespace HQS.VUE.Core;

internal class CommandWaiter<TResult> : AbstractCommandWaiter<IWaitable<TResult>>, IWaitable<IReadOnlyList<TResult>>
{
	private readonly List<TResult> results = new List<TResult>();

	private readonly IReadOnlyList<TResult> readOnlyResults;

	public CommandWaiter()
	{
		this.readOnlyResults = this.results.AsReadOnlyList();
	}

	/// <inheritdoc />
	public IReadOnlyList<TResult> Result
	{
		get
		{
			this.Wait();
			return this.readOnlyResults;
		}
	}

	public void Add<TCommand>(Pinned<TCommand> pin)
		where TCommand : class, IWaitable<TResult>
	{
		Debug.Assert(!this.HasBeenWaited);
		Debug.Assert(pin.IsPinned);

		base.Add(pin.CastTo<IWaitable<TResult>>().CreatePin());
	}

	public void Add<TCommand>(Pin<TCommand> pin)
		where TCommand : class, IWaitable<TResult>
	{
		Debug.Assert(!this.HasBeenWaited);
		Debug.Assert(pin.IsPinned);

		base.Add(pin.CastTo<IWaitable<TResult>>());
	}

	/// <inheritdoc />
	public override void Clear()
	{
		base.Clear();
		this.results.Clear();
	}

	/// <inheritdoc />
	public override void Wait()
	{
		if (this.SkipWaiting())
		{
			return;
		}

		List<Exception> exceptions = null;

		int max = this.Pins.Count;
		for (int i = 0; i < max; i++)
		{
			try
			{
				this.results.Add(this.Pins[i].Value.Result);
			}
			catch (Exception error)
			{
				exceptions ??= new List<Exception>();
				exceptions.Add(error);
			}
		}

		if (exceptions != null)
		{
			this.results.Clear();
		}

		this.SetWaited(exceptions);
	}
}
