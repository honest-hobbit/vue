﻿namespace HQS.VUE.Core;

internal interface ICommand : IRunnable, IWaitable
{
}
