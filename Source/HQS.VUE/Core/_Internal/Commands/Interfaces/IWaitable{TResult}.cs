﻿namespace HQS.VUE.Core;

internal interface IWaitable<TResult> : IWaitable
{
	TResult Result { get; }
}
