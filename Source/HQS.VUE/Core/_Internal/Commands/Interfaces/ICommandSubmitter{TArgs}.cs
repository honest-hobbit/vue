﻿namespace HQS.VUE.Core;

internal interface ICommandSubmitter<TArgs>
{
	void Submit(TArgs args);

	Pin<IWaitable> SubmitAndGetWaiter(TArgs args);
}
