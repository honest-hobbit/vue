﻿namespace HQS.VUE.Core;

internal interface IInitializable<TArgs>
{
	void Initialize(TArgs args);
}
