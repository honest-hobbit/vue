﻿namespace HQS.VUE.Core;

internal interface ICommandProcessor<TMarker>
	where TMarker : class, IRunnable
{
	public void Submit<TCommand>(Pinned<TCommand> command)
		where TCommand : class, TMarker;

	public void Submit<TCommand>(Pin<TCommand> command)
		where TCommand : class, TMarker;
}
