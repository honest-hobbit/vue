﻿namespace HQS.VUE.Core;

internal interface IWaitable
{
	void Wait();
}
