﻿namespace HQS.VUE.Core;

internal interface IRunnable
{
	void Run();
}
