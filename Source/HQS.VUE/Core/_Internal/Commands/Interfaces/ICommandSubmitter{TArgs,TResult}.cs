﻿namespace HQS.VUE.Core;

internal interface ICommandSubmitter<TArgs, TResult> : ICommandSubmitter<TArgs>
{
	Pin<IWaitable<TResult>> SubmitAndGetWaitableResult(TArgs args);
}
