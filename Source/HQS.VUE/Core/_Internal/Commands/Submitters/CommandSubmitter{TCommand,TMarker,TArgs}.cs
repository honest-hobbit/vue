﻿namespace HQS.VUE.Core;

internal class CommandSubmitter<TCommand, TMarker, TArgs> : ICommandSubmitter<TArgs>
	where TCommand : class, TMarker, ICommand, IInitializable<TArgs>
	where TMarker : class, IRunnable
{
	private readonly ICommandProcessor<TMarker> processor;

	private readonly IPinPool<TCommand> pool;

	public CommandSubmitter(ICommandProcessor<TMarker> processor, IPinPool<TCommand> pool)
	{
		Debug.Assert(processor != null);
		Debug.Assert(pool != null);

		this.processor = processor;
		this.pool = pool;
	}

	/// <inheritdoc />
	public void Submit(TArgs args)
	{
		var pin = this.pool.Rent(out var command);
		try
		{
			command.Initialize(args);
			this.processor.Submit(pin);
		}
		catch (Exception)
		{
			pin.Dispose();
			throw;
		}
	}

	/// <inheritdoc />
	public Pin<IWaitable> SubmitAndGetWaiter(TArgs args) => this.SubmitAndGetCommand(args).CastTo<IWaitable>();

	protected Pin<TCommand> SubmitAndGetCommand(TArgs args)
	{
		var pin = this.pool.Rent(out var command);
		try
		{
			command.Initialize(args);
			this.processor.Submit(pin.CreatePin());
			return pin;
		}
		catch (Exception)
		{
			pin.Dispose();
			throw;
		}
	}
}
