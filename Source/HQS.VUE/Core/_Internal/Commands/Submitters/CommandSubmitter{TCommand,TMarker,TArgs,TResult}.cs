﻿namespace HQS.VUE.Core;

internal class CommandSubmitter<TCommand, TMarker, TArgs, TResult> :
	CommandSubmitter<TCommand, TMarker, TArgs>, ICommandSubmitter<TArgs, TResult>
	where TCommand : class, TMarker, ICommand, IInitializable<TArgs>, IWaitable<TResult>
	where TMarker : class, IRunnable
{
	public CommandSubmitter(ICommandProcessor<TMarker> processor, IPinPool<TCommand> pool)
		: base(processor, pool)
	{
	}

	/// <inheritdoc />
	public Pin<IWaitable<TResult>> SubmitAndGetWaitableResult(TArgs args) =>
		this.SubmitAndGetCommand(args).CastTo<IWaitable<TResult>>();
}
