﻿namespace HQS.VUE.Core;

internal abstract class AbstractCommand : AbstractCommand<Nothing>
{
	/// <inheritdoc />
	protected sealed override Nothing RunAndGetResult()
	{
		this.OnRun();
		return default;
	}

	protected abstract void OnRun();
}
