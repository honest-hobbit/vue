﻿namespace HQS.VUE.Core;

internal abstract class AbstractCommand<T> : ICommand, IDeinitializable, IReleasable
{
	private readonly ConcurrentBool isInitialized = new ConcurrentBool(false);

	private readonly ConcurrentBool isRunning = new ConcurrentBool(false);

	private readonly Pending<T> completion = new Pending<T>();

	private IDisposable scope;

	public bool IsInitialized => this.isInitialized.Value;

	public bool IsRunning => this.isRunning.Value;

	public bool IsCompleted => this.completion.IsSet;

	public bool IsFaulted => this.Exception != null;

	public Exception Exception => this.completion.Exception;

	/// <inheritdoc />
	void IDeinitializable.Deinitialize()
	{
		try
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
			Ensure.That(this.IsRunning, nameof(this.IsRunning)).IsFalse();
			Ensure.That(this.IsCompleted, nameof(this.IsCompleted)).IsTrue();

			this.OnDeinitialize();
		}
		finally
		{
			this.completion.Reset();
			this.isRunning.SetFalse();
			this.isInitialized.SetFalse();
		}
	}

	/// <inheritdoc />
	void IRunnable.Run()
	{
		try
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
			Ensure.That(this.IsCompleted, nameof(this.IsCompleted)).IsFalse();

			if (!this.isRunning.ToggleIfFalse())
			{
				throw new InvalidOperationException("Command is already running.");
			}

			this.completion.SetValue(this.RunAndGetResult());
		}
		catch (Exception error)
		{
			this.completion.SetException(error);
			throw;
		}
		finally
		{
			this.isRunning.SetFalse();
		}
	}

	/// <inheritdoc />
	public void Wait()
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

		this.completion.Wait();
	}

	/// <inheritdoc />
	void IReleasable.Release()
	{
		try
		{
			this.OnRelease();
		}
		finally
		{
			this.scope?.Dispose();
		}
	}

	protected void SetScope(IDisposable scope)
	{
		Ensure.That(scope, nameof(scope)).IsNotNull();

		if (this.scope != null)
		{
			throw new InvalidOperationException($"{nameof(this.SetScope)} must be called only once.");
		}

		if (this.isInitialized)
		{
			throw new InvalidOperationException($"{nameof(this.SetScope)} must be called before command is ever initialized.");
		}

		this.scope = scope;
	}

	protected void ValidateAndSetInitialized()
	{
		if (!this.isInitialized.ToggleIfFalse())
		{
			throw new InvalidOperationException("Command is already initialized.");
		}
	}

	protected T GetResult()
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

		return this.completion.Value;
	}

	protected abstract T RunAndGetResult();

	protected virtual void OnDeinitialize()
	{
	}

	protected virtual void OnRelease()
	{
	}
}
