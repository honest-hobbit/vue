﻿namespace HQS.VUE.Core;

internal abstract class AbstractResultCommand<T> : AbstractCommand<T>, IWaitable<T>
{
	/// <inheritdoc />
	public T Result => this.GetResult();
}
