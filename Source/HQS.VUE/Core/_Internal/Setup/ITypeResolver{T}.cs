﻿namespace HQS.VUE.Core;

internal interface ITypeResolver<T>
{
	T GetInstance();
}
