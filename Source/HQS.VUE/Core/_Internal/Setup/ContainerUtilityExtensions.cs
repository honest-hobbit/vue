﻿namespace HQS.VUE.Core;

internal static class ContainerUtilityExtensions
{
	[Conditional(CompilationSymbol.Debug)]
	public static void VerifyInDebugOnly(this Container container)
	{
		Debug.Assert(container != null);

		container.Verify();
	}

	public static void RegisterEmpty<TService>(
		this ContainerCollectionRegistrator registrator, Lifestyle lifestyle = null)
		where TService : class
	{
		Debug.Assert(registrator != null);

		lifestyle ??= registrator.Container.Options.DefaultLifestyle;
		registrator.Register<TService>(Enumerable.Empty<Type>(), lifestyle);
	}

	public static void RegisterAssetFactory<TConcrete>(this Container container)
		where TConcrete : class, IAssetFactory
	{
		Debug.Assert(container != null);

		container.Register<TConcrete>(Lifestyle.Scoped);
		container.Collection.Append<IAssetFactory, TConcrete>(Lifestyle.Scoped);
	}

	public static void RegisterAssetCollection<TService, TConcrete>(this Container container)
		where TService : class, IAssetCollection
		where TConcrete : class, TService
	{
		Debug.Assert(container != null);

		container.Register<TService, TConcrete>(Lifestyle.Scoped);
		container.Collection.Append<IAssetCollection, TConcrete>(Lifestyle.Scoped);
	}

	public static void AutoResolveFactories(this Container container, bool autoResolveServiceTypes = false)
	{
		Debug.Assert(container != null);

		container.ResolveUnregisteredType += (s, e) =>
		{
			var type = e.UnregisteredServiceType;
			if (!type.IsClosedTypeOf(typeof(Func<>)))
			{
				return;
			}

			var serviceType = type.GetGenericArguments().First();
			var producer = autoResolveServiceTypes ?
				container.Options.DefaultLifestyle.CreateProducer(serviceType, serviceType, container) :
				container.GetRegistration(serviceType, true);

			var factoryDelegate = Expression.Lambda(
				typeof(Func<>).MakeGenericType(serviceType), producer.BuildExpression()).Compile();
			e.Register(Expression.Constant(factoryDelegate));
		};
	}

	public static void RegisterFactory<TConcrete>(this Container container, Lifestyle lifestyle = null)
		where TConcrete : class => RegisterFactory<TConcrete, TConcrete>(container, lifestyle);

	public static void RegisterFactory<TService, TConcrete>(this Container container, Lifestyle lifestyle = null)
		where TService : class
		where TConcrete : class, TService
	{
		Debug.Assert(container != null);

		lifestyle ??= container.Options.DefaultLifestyle;

		if (typeof(IRequireScope).IsAssignableFrom(typeof(TConcrete)))
		{
			container.Register<TService, TConcrete>(lifestyle);
			container.RegisterInstance<Func<TService>>(() =>
			{
				var scope = new Scope(container);
				var instance = scope.GetInstance<TService>();
				((IRequireScope)instance).SetScope(scope);
				return instance;
			});
		}
		else
		{
			var producer = lifestyle.CreateProducer<TService, TConcrete>(container);
			container.RegisterInstance<Func<TService>>(producer.GetInstance);
		}
	}

	public static void RegisterAllPools(this Container container, IEnumerable<PooledType> pooledTypes)
	{
		Debug.Assert(container != null);
		Debug.Assert(pooledTypes.AllAndSelfNotNull());
		pooledTypes.VerifyAllAreUniqueInDebugOnly();

		foreach (var pooledType in pooledTypes)
		{
			container.RegisterPool(pooledType);
		}
	}

	public static void RegisterPool(this Container container, PooledType pooledType)
	{
		Debug.Assert(container != null);
		Debug.Assert(pooledType != null);

		var poolType = typeof(TypeFactoryPool<>).MakeGenericType(pooledType.ResourceType);

		container.RegisterSingleton(typeof(IPinPool<>), poolType);
		container.Collection.Append(typeof(ITypePool), poolType, Lifestyle.Singleton);
		container.Collection.AppendInstance(pooledType);
	}

	public static void RegisterAllTypeResolversFromAssembly(this Container container, Assembly assembly)
	{
		Debug.Assert(container != null);
		Debug.Assert(assembly != null);

		foreach (var type in container.GetTypesToRegister(typeof(ITypeResolver<>), assembly))
		{
			container.RegisterSingleton(typeof(ITypeResolver<>), type);
		}
	}

	public static void RegisterAllCommandsFromAssembly<TNamespace>(this Container container, Assembly assembly)
		where TNamespace : ICommand
	{
		Debug.Assert(container != null);
		Debug.Assert(assembly != null);

		foreach (var type in container.GetTypesToRegister(typeof(TNamespace), assembly))
		{
			container.RegisterCommand(type);
		}
	}

	public static void RegisterCommand(this Container container, Type concreteType)
	{
		Debug.Assert(container != null);
		Debug.Assert(concreteType != null);

		Type interfaceType = null;
		CheckCommandSubtype(typeof(IMainCommand));
		CheckCommandSubtype(typeof(IHostCommand));
		CheckCommandSubtype(typeof(IPersistenceCommand));
		CheckCommandSubtype(typeof(IParallelCommand));

		if (interfaceType == null)
		{
			throw new InvalidCastException("Type does not implement a command subtype interface.");
		}

		var initializableArgsType = concreteType.GetInterfaces()
			.Where(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IInitializable<>))
			.Single().GetGenericArguments().Single();

		var waitableResultType = concreteType.GetInterfaces()
			.Where(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IWaitable<>))
			.SingleOrDefault()?.GetGenericArguments().Single();

		if (waitableResultType != null)
		{
			Register.SubmitterWithResults.MakeGenericMethod(
				concreteType, interfaceType, initializableArgsType, waitableResultType)
				.Invoke(null, new object[] { container });
		}
		else
		{
			Register.Submitter.MakeGenericMethod(concreteType, interfaceType, initializableArgsType)
				.Invoke(null, new object[] { container });
		}

		void CheckCommandSubtype(Type checkInterfaceType)
		{
			var foundInterface = checkInterfaceType.IsAssignableFrom(concreteType) ? checkInterfaceType : null;
			if (foundInterface != null && interfaceType != null)
			{
				throw new InvalidCastException("Type cannot implement more than one command subtype interface.");
			}

			interfaceType ??= foundInterface;
		}
	}

	private static class Register
	{
		public static MethodInfo Submitter { get; } = typeof(Register).GetMethod(
			nameof(RegisterSubmitter), BindingFlags.NonPublic | BindingFlags.Static);

		public static MethodInfo SubmitterWithResults { get; } = typeof(Register).GetMethod(
			nameof(RegisterSubmitterWithResults), BindingFlags.NonPublic | BindingFlags.Static);

		private static void RegisterSubmitter<TCommand, TMarker, TArgs>(Container container)
			where TCommand : class, TMarker, ICommand, IInitializable<TArgs>
			where TMarker : class, IRunnable
		{
			Debug.Assert(container != null);

			container.RegisterFactory<TCommand>();
			container.RegisterSingleton<ICommandSubmitter<TArgs>, CommandSubmitter<TCommand, TMarker, TArgs>>();
		}

		private static void RegisterSubmitterWithResults<TCommand, TMarker, TArgs, TResult>(Container container)
			where TCommand : class, TMarker, ICommand, IInitializable<TArgs>, IWaitable<TResult>
			where TMarker : class, IRunnable
		{
			Debug.Assert(container != null);

			container.RegisterFactory<TCommand>();
			container.RegisterSingleton<ICommandSubmitter<TArgs>, CommandSubmitter<TCommand, TMarker, TArgs>>();
			container.RegisterSingleton<ICommandSubmitter<TArgs, TResult>, CommandSubmitter<TCommand, TMarker, TArgs, TResult>>();
		}
	}
}
