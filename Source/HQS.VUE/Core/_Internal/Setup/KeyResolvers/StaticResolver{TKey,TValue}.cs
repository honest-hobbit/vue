﻿namespace HQS.VUE.Core;

internal class StaticResolver<TKey, TValue> : IKeyResolver<TKey, TValue>
{
	private readonly Dictionary<TKey, TValue> values;

	public StaticResolver(params KeyValuePair<TKey, TValue>[] pairs)
		: this(EqualityComparer<TKey>.Default, (IEnumerable<KeyValuePair<TKey, TValue>>)pairs)
	{
	}

	public StaticResolver(IEnumerable<KeyValuePair<TKey, TValue>> pairs)
		: this(EqualityComparer<TKey>.Default, pairs)
	{
	}

	public StaticResolver(IEqualityComparer<TKey> comparer, params KeyValuePair<TKey, TValue>[] pairs)
		: this(comparer, (IEnumerable<KeyValuePair<TKey, TValue>>)pairs)
	{
	}

	public StaticResolver(IEqualityComparer<TKey> comparer, IEnumerable<KeyValuePair<TKey, TValue>> pairs)
	{
		Debug.Assert(comparer != null);
		Debug.Assert(pairs != null);

		this.values = new Dictionary<TKey, TValue>(comparer);

		foreach (var pair in pairs)
		{
			this.values.Add(pair.Key, pair.Value);
		}
	}

	/// <inheritdoc />
	public TValue Get(TKey key) => this.values[key];

	/// <inheritdoc />
	public IEnumerator<TValue> GetEnumerator() => this.values.Values.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
