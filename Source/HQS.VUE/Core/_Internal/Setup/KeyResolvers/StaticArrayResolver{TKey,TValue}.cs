﻿namespace HQS.VUE.Core;

internal class StaticArrayResolver<TKey, TValue> : IKeyResolver<TKey, TValue>
{
	private readonly Func<TKey, int> toIndex;

	private readonly TValue[] values;

	public StaticArrayResolver(Func<TKey, int> toIndex, params KeyValuePair<TKey, TValue>[] pairs)
		: this(toIndex, (IEnumerable<KeyValuePair<TKey, TValue>>)pairs)
	{
	}

	public StaticArrayResolver(Func<TKey, int> toIndex, IEnumerable<KeyValuePair<TKey, TValue>> pairs)
	{
		Debug.Assert(toIndex != null);
		Debug.Assert(pairs != null);

		this.toIndex = toIndex;

		var list = pairs.ToList();
		this.values = new TValue[list.Select(x => toIndex(x.Key)).Max() + 1];

		foreach (var pair in list)
		{
			this.values[toIndex(pair.Key)] = pair.Value;
		}
	}

	/// <inheritdoc />
	public TValue Get(TKey key)
	{
		int index = this.toIndex(key);

		Debug.Assert(this.values[index] != null);

		return this.values[index];
	}

	/// <inheritdoc />
	public IEnumerator<TValue> GetEnumerator()
	{
		int length = this.values.Length;
		for (int i = 0; i < length; i++)
		{
			if (this.values[i] != null)
			{
				yield return this.values[i];
			}
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
