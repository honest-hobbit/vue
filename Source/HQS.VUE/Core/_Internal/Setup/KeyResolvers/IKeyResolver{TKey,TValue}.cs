﻿namespace HQS.VUE.Core;

internal interface IKeyResolver<TKey, out TValue> : IEnumerable<TValue>
{
	TValue Get(TKey key);
}
