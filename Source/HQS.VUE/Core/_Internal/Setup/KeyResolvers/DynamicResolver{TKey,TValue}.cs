﻿namespace HQS.VUE.Core;

internal class DynamicResolver<TKey, TValue> : IKeyResolver<TKey, TValue>
{
	private readonly object padlock = new object();

	private readonly Func<TKey, TValue> factory;

	private readonly Dictionary<TKey, TValue> values;

	public DynamicResolver(Func<TKey, TValue> factory, IEqualityComparer<TKey> comparer = null)
	{
		Debug.Assert(factory != null);

		this.factory = factory;
		this.values = new Dictionary<TKey, TValue>(comparer ?? EqualityComparer<TKey>.Default);
	}

	/// <inheritdoc />
	public TValue Get(TKey key)
	{
		lock (this.padlock)
		{
			if (!this.values.TryGetValue(key, out var value))
			{
				value = this.factory(key);
				this.values[key] = value;
			}

			return value;
		}
	}

	/// <inheritdoc />
	public IEnumerator<TValue> GetEnumerator()
	{
		TValue[] values;
		lock (this.padlock)
		{
			values = this.values.Values.ToArray();
		}

		int length = values.Length;
		for (int i = 0; i < length; i++)
		{
			yield return values[i];
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
