﻿namespace HQS.VUE.Core;

internal static class ContainerRegistrations
{
	public static void RegisterVoxelUniverseCore(this Container container)
	{
		Debug.Assert(container != null);

		container.Options.ResolveUnregisteredConcreteTypes = false;
		container.Options.DefaultLifestyle = Lifestyle.Transient;
		container.Options.DefaultScopedLifestyle = ScopedLifestyle.Flowing;

		////container.RegisterInstance(universeConfig);

		container.AutoResolveFactories(autoResolveServiceTypes: false);

		// TODO this might need to be split into separate registrations per namespace
		container.RegisterAllTypeResolversFromAssembly(
			typeof(ContainerRegistrations).Assembly);

		container.RegisterAllPools(CorePooledTypes.Enumerate());
		container.RegisterAllCommandsFromAssembly<INamespaceCore>(
			typeof(ContainerRegistrations).Assembly);

		container.RegisterSingleton<TypePoolCollection>();
		container.RegisterSingleton<IPooledTypeResolver, PooledTypeResolver>();
		container.RegisterSingleton(typeof(ITypeResolver<>), typeof(SingletonResolver<>));

		container.RegisterSingleton<Diagnostics>();
		container.RegisterSingleton<JobSubmitter>();
		container.RegisterSingleton<EngineStatus>();
		container.RegisterSingleton(typeof(Reporter<>), typeof(Reporter<>));
		container.RegisterSingleton(typeof(IReporter<>), typeof(Reporter<>));
		container.RegisterSingleton<VoxelUniverseEngine>();

		container.RegisterSingleton<MainCommandProcessor>();
		container.RegisterSingleton<ICommandProcessor<IMainCommand>, MainCommandProcessor>();
		container.RegisterSingleton<ParallelCommandProcessor>();
		container.RegisterSingleton<ICommandProcessor<IParallelCommand>, ParallelCommandProcessor>();
		container.RegisterSingleton<PersistenceCommandProcessor>();
		container.RegisterSingleton<ICommandProcessor<IPersistenceCommand>, PersistenceCommandProcessor>();

		container.Collection.RegisterEmpty<IAssetFactory>(Lifestyle.Scoped);
		container.Register<AssetFactoryInitializer>(Lifestyle.Scoped);

		container.Collection.RegisterEmpty<IAssetCollection>(Lifestyle.Scoped);
		container.Register<IJobAssets, JobAssets>(Lifestyle.Scoped);

		// TODO need to reorder namespace dependencies for this
		container.Register(typeof(AssetWriterTracker<>), typeof(AssetWriterTracker<>), Lifestyle.Scoped);
		container.Register(typeof(CommittedAssets<>), typeof(CommittedAssets<>), Lifestyle.Scoped);
		container.Register(typeof(ICommittedAssets<>), typeof(CommittedAssets<>), Lifestyle.Scoped);

		////container.Register<IByteBuffer, VUEByteBuffer>();
	}
}
