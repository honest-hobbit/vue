﻿namespace HQS.VUE.Core;

internal interface IRequireScope
{
	void SetScope(IDisposable scope);
}
