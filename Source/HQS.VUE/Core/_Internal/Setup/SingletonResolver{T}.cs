﻿namespace HQS.VUE.Core;

internal class SingletonResolver<T> : ITypeResolver<T>
	where T : class
{
	private readonly Container container;

	private T instance;

	public SingletonResolver(Container container)
	{
		Debug.Assert(container != null);

		this.container = container;
	}

	public T GetInstance() => this.instance ??= this.container.GetInstance<T>();
}
