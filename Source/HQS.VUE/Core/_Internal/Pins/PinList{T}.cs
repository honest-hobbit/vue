﻿namespace HQS.VUE.Core;

internal class PinList<T> : IReadOnlyList<Pinned<T>>
	where T : class
{
	private readonly List<Pin<T>> pins = new List<Pin<T>>();

	/// <inheritdoc />
	public int Count => this.pins.Count;

	/// <inheritdoc />
	public Pinned<T> this[int index] => this.pins[index].AsPinned;

	public void Add(Pin<T> pin)
	{
		Debug.Assert(pin.IsPinned);

		this.pins.Add(pin);
	}

	public void Add(Pinned<T> pin)
	{
		Debug.Assert(pin.IsPinned);

		this.pins.Add(pin.CreatePin());
	}

	public void AddMany(IReadOnlyList<Pin<T>> pins)
	{
		Debug.Assert(pins != null);

		int count = pins.Count;
		for (int i = 0; i < count; i++)
		{
			this.Add(pins[i]);
		}
	}

	public void AddMany(IReadOnlyList<Pinned<T>> pins)
	{
		Debug.Assert(pins != null);

		int count = pins.Count;
		for (int i = 0; i < count; i++)
		{
			this.Add(pins[i]);
		}
	}

	public void DisposeAllAndClear() => this.pins.DisposeAllAndClear();

	/// <inheritdoc />
	public IEnumerator<Pinned<T>> GetEnumerator()
	{
		int count = this.pins.Count;
		for (int i = 0; i < count; i++)
		{
			yield return this.pins[i].AsPinned;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
