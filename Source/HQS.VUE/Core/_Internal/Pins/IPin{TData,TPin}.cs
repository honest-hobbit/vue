﻿namespace HQS.VUE.Core;

internal interface IPin<TData, TPin> : IPinnable<TData, TPin>, IDisposable
	where TData : IPinnable<TData, TPin>
	where TPin : IPin<TData, TPin>
{
	TData Data { get; }
}
