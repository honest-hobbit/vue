﻿namespace HQS.VUE.Core;

internal class PinList<TData, TPin> : IReadOnlyList<TData>
	where TData : IPinnable<TData, TPin>
	where TPin : IPin<TData, TPin>
{
	private readonly List<TPin> pins = new List<TPin>();

	/// <inheritdoc />
	public int Count => this.pins.Count;

	/// <inheritdoc />
	public TData this[int index] => this.pins[index].Data;

	public void Add(TPin pin)
	{
		Debug.Assert(pin.HasData);

		this.pins.Add(pin);
	}

	public void Add(TData pin)
	{
		Debug.Assert(pin.HasData);

		this.pins.Add(pin.CreatePin());
	}

	public void AddMany(IReadOnlyList<TPin> pins)
	{
		Debug.Assert(pins != null);

		int count = pins.Count;
		for (int i = 0; i < count; i++)
		{
			this.Add(pins[i]);
		}
	}

	public void AddMany(IReadOnlyList<TData> pins)
	{
		Debug.Assert(pins != null);

		int count = pins.Count;
		for (int i = 0; i < count; i++)
		{
			this.Add(pins[i]);
		}
	}

	public void DisposeAllAndClear() => this.pins.DisposeAllAndClear();

	/// <inheritdoc />
	public IEnumerator<TData> GetEnumerator()
	{
		int count = this.pins.Count;
		for (int i = 0; i < count; i++)
		{
			yield return this.pins[i].Data;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
