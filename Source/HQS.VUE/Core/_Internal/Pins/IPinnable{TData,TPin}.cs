﻿namespace HQS.VUE.Core;

internal interface IPinnable<TData, TPin>
	where TData : IPinnable<TData, TPin>
	where TPin : IPin<TData, TPin>
{
	bool HasData { get; }

	TPin CreatePin();
}
