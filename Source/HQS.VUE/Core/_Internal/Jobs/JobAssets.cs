﻿namespace HQS.VUE.Core;

internal class JobAssets : IJobAssets
{
	private readonly TypeSet<IAssetCollection> collections = new TypeSet<IAssetCollection>();

	public JobAssets(IReadOnlyList<IAssetCollection> collections)
	{
		Debug.Assert(collections.AllAndSelfNotNull());

		for (int i = 0; i < collections.Count; i++)
		{
			var collection = collections[i];

			Debug.Assert(collection.CollectionType != null);
			Debug.Assert(collection.AssetType != null);

			this.collections.Add(collection.CollectionType, collection);
		}
	}

	/// <inheritdoc />
	public int Count => this.collections.Count;

	/// <inheritdoc />
	public bool Contains<T>()
		where T : IAssetCollection => this.collections.Contains<T>();

	/// <inheritdoc />
	public bool Contains(Type type)
	{
		Ensure.That(type, nameof(type)).IsNotNull();

		return this.collections.Contains(type);
	}

	/// <inheritdoc />
	public T Get<T>()
		where T : IAssetCollection
	{
		if (this.collections.TryGet<T>(out var resource))
		{
			return resource;
		}
		else
		{
			throw new TypeArgumentException($"Type '{typeof(T).FullName}' not found.", nameof(T));
		}
	}

	/// <inheritdoc />
	public IAssetCollection Get(Type type)
	{
		Ensure.That(type, nameof(type)).IsNotNull();

		if (this.collections.TryGet(type, out var resource))
		{
			return resource;
		}
		else
		{
			throw new TypeArgumentException($"Type '{type.FullName}' not found.", nameof(type));
		}
	}

	/// <inheritdoc />
	public bool TryGet<T>(out T value)
		where T : IAssetCollection => this.collections.TryGet(out value);

	/// <inheritdoc />
	public bool TryGet(Type type, out IAssetCollection value)
	{
		Ensure.That(type, nameof(type)).IsNotNull();

		return this.collections.TryGet(type, out value);
	}

	/// <inheritdoc />
	public Try<T> TryGet<T>()
		where T : IAssetCollection => this.collections.TryGet<T>();

	/// <inheritdoc />
	public Try<IAssetCollection> TryGet(Type type)
	{
		Ensure.That(type, nameof(type)).IsNotNull();

		return this.collections.TryGet(type);
	}

	/// <inheritdoc />
	public IEnumerator<KeyValuePair<Type, IAssetCollection>> GetEnumerator()
		=> this.collections.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
