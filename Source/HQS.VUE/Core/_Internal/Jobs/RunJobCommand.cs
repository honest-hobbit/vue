﻿namespace HQS.VUE.Core;

internal class RunJobCommand :
	AbstractCommand,
	IRequireScope,
	IInitializable<RunJobCommand.Args>,
	IMainCommand,
	INamespaceCore
{
	private readonly AssetFactoryInitializer factoryInitializer;

	private readonly IJobAssets jobAssets;

	private readonly IReporter<Exception> errorReporter;

	private Guid jobKey;

	private Pin<IJob> job;

	private TaskCompletionSource completion;

	public RunJobCommand(
		AssetFactoryInitializer factoryInitializer,
		IJobAssets jobAssets,
		IReporter<Exception> errorReporter)
	{
		Debug.Assert(factoryInitializer != null);
		Debug.Assert(jobAssets != null);
		Debug.Assert(errorReporter != null);

		this.factoryInitializer = factoryInitializer;
		this.jobAssets = jobAssets;
		this.errorReporter = errorReporter;
	}

	/// <inheritdoc />
	void IRequireScope.SetScope(IDisposable scope) => this.SetScope(scope);

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.JobKey != Guid.Empty);
		Debug.Assert(args.Job.IsPinned);
		this.ValidateAndSetInitialized();

		this.jobKey = args.JobKey;
		this.job = args.Job.CreatePin();
		this.completion = args.Completion;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		try
		{
			// pre running setup
			this.factoryInitializer.InitializeFactories(DateTime.UtcNow);

			// run job
			this.job.Value.RunOn(this.jobAssets);

			// check if any changes
			// cleanup
			this.completion?.SetResult();
		}
		catch (Exception error)
		{
			this.factoryInitializer.DeinitializeFactories();

			// purposely catching all exceptions without rethrowing
			// user jobs are fault tolerant
			this.completion?.SetException(error);
			this.errorReporter.Report(error);
		}
	}

	protected override void OnDeinitialize()
	{
		this.jobKey = Guid.Empty;
		this.job.Dispose();
		this.job = default;
		this.completion = null;
	}

	public struct Args
	{
		public Guid JobKey;

		public Pinned<IJob> Job;

		public TaskCompletionSource Completion;
	}
}
