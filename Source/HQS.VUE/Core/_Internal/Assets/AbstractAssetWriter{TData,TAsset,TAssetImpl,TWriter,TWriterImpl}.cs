﻿namespace HQS.VUE.Core;

internal abstract class AbstractAssetWriter<TData, TAsset, TAssetImpl, TWriter, TWriterImpl> :
	AbstractAssetWriter,
	IAssetWriter<TAsset>,
	ITrackableAsset<TWriterImpl>,
	ICommittableAsset<TAsset>
	where TAsset : class, IAsset
	where TAssetImpl : AbstractAsset<TData, TAsset, TAssetImpl, TWriter, TWriterImpl>, TAsset
	where TWriter : class, IAssetWriter
	where TWriterImpl : AbstractAssetWriter<TData, TAsset, TAssetImpl, TWriter, TWriterImpl>, TWriter
{
	private readonly IPinPool<TAssetImpl> assetPool;

	private Pin<TAssetImpl> previousPin;

	private AssetWriterTracker<TWriterImpl> writerTracker;

	public AbstractAssetWriter(IPinPool<TAssetImpl> assetPool)
	{
		Debug.Assert(assetPool != null);

		this.assetPool = assetPool;
	}

	/// <inheritdoc />
	public TAsset Previous { get; private set; }

	/// <inheritdoc />
	public void SetAssetTracker(AssetWriterTracker<TWriterImpl> writerTracker)
	{
		Debug.Assert(writerTracker != null);

		this.writerTracker = writerTracker;
	}

	public void InitializeFrom(TData data)
	{
		this.previousPin = this.assetPool.Rent(out var previous);
		previous.SetAssetTracker(this.writerTracker);
		previous.InitializeFrom(data);
		this.InitializeFrom(previous);
	}

	public void InitializeFrom(TAssetImpl previous)
	{
		Debug.Assert(previous != null);

		this.Previous = previous;
		this.SetData(previous.GetData());
	}

	/// <inheritdoc />
	public Pin<TAsset> ToCommittedAsset()
	{
		var pin = this.assetPool.Rent(out var asset);
		asset.SetAssetTracker(this.writerTracker);
		asset.InitializeFrom(this.GetData());
		return pin.CastTo<TAsset>();
	}

	/// <inheritdoc />
	public sealed override void Deinitialize()
	{
		base.Deinitialize();
		this.writerTracker = null;
		this.previousPin.Dispose();
		this.previousPin = default;
		this.ClearData();
	}

	protected abstract TData GetData();

	protected abstract void SetData(TData data);

	protected abstract void ClearData();
}
