﻿namespace HQS.VUE.Core;

internal abstract class AbstractAssetWriter : IAssetWriter, IDeinitializable
{
	private IAssetStore store;

	/// <inheritdoc />
	public Guid Key { get; private set; }

	/// <inheritdoc />
	public DateTime Created { get; private set; }

	/// <inheritdoc />
	public DateTime LastModified { get; private set; }

	/// <inheritdoc />
	public Guid VersionStamp { get; private set; }

	/// <inheritdoc />
	public long VersionCount { get; private set; }

	/// <inheritdoc />
	public string DisplayName { get; set; }

	/// <inheritdoc />
	public bool AutoSave { get; set; }

	/// <inheritdoc />
	public bool ToBeIgnored { get; set; }

	/// <inheritdoc />
	public bool ToBeSaved { get; set; }

	/// <inheritdoc />
	public bool ToBeUnloaded { get; set; }

	/// <inheritdoc />
	public bool ToBeDeleted { get; set; }

	// TODO HasUnsavedChanges hasn't been implemented yet
	/// <inheritdoc />
	public bool HasUnsavedChanges { get; private set; }

	/// <inheritdoc />
	public object Store => this.store;

	/// <inheritdoc />
	public void AssignNewKey() => this.Key = Guid.NewGuid();

	/// <inheritdoc />
	public void SetStore(IAssetStore store)
	{
		Ensure.That(store, nameof(store)).IsNotNull();

		// TODO this should be changed to allow changing of stores
		Ensure.That(this.Store != null, nameof(this.Store)).IsTrue();

		throw new NotImplementedException();
	}

	public IAssetStore GetStore() => this.store;

	/// <inheritdoc />
	public virtual void Deinitialize()
	{
		this.InitializeBase(AssetData.Empty, null);
	}

	protected void InitializeBase(AssetData data, IAssetStore store)
	{
		this.Key = data.Key;
		this.Created = data.Created;
		this.LastModified = data.LastModified;
		this.VersionStamp = data.VersionStamp;
		this.VersionCount = data.VersionCount;
		this.DisplayName = data.DisplayName;
		this.AutoSave = data.AutoSave;

		this.store = store;

		this.ToBeIgnored = false;
		this.ToBeSaved = data.AutoSave;
		this.ToBeUnloaded = false;
		this.ToBeDeleted = false;

		// TODO HasUnsavedChanges hasn't been implemented yet
		this.HasUnsavedChanges = false;
	}

	protected AssetData GetAssetData() => AssetData.From(this);
}
