﻿namespace HQS.VUE.Core;

internal struct AssetData
{
	public Guid Key;

	public DateTime Created;

	public DateTime LastModified;

	public Guid VersionStamp;

	public long VersionCount;

	public string DisplayName;

	public bool AutoSave;

	public static AssetData Empty => new AssetData()
	{
		Key = Guid.Empty,
		Created = DateTime.MinValue,
		LastModified = DateTime.MinValue,
		VersionStamp = Guid.Empty,
		VersionCount = 0,
		DisplayName = string.Empty,
		AutoSave = false,
	};

	public static AssetData ForNewAsset(DateTime created) => new AssetData()
	{
		Key = Guid.NewGuid(),
		Created = created,
		LastModified = created,
		VersionStamp = Guid.NewGuid(),
		VersionCount = 0,
		DisplayName = "New Asset",
		AutoSave = false,
	};

	public static AssetData From(IAsset source)
	{
		Debug.Assert(source != null);

		return new AssetData()
		{
			Key = source.Key,
			Created = source.Created,
			LastModified = source.LastModified,
			VersionStamp = source.VersionStamp,
			VersionCount = source.VersionCount,
			DisplayName = source.DisplayName,
			AutoSave = source.AutoSave,
		};
	}
}
