﻿namespace HQS.VUE.Core;

internal abstract class AbstractAsset<TData, TAsset, TAssetImpl, TWriter, TWriterImpl> :
	AbstractAsset,
	IChangeableAsset<TWriter>,
	ITrackableAsset<TWriterImpl>
	where TAsset : class, IAsset
	where TAssetImpl : AbstractAsset<TData, TAsset, TAssetImpl, TWriter, TWriterImpl>, TAsset
	where TWriter : class, IAssetWriter
	where TWriterImpl : AbstractAssetWriter<TData, TAsset, TAssetImpl, TWriter, TWriterImpl>, TWriter
{
	private AssetWriterTracker<TWriterImpl> writerTracker;

	/// <inheritdoc />
	public void SetAssetTracker(AssetWriterTracker<TWriterImpl> writerTracker)
	{
		Debug.Assert(writerTracker != null);

		this.writerTracker = writerTracker;
	}

	/// <inheritdoc />
	public TWriter CreateChanges()
	{
		var writer = this.writerTracker.GetAssetWriter();
		writer.InitializeFrom(this.Self);
		return writer;
	}

	/// <inheritdoc />
	public sealed override void Deinitialize()
	{
		base.Deinitialize();
		this.writerTracker = null;
		this.ClearData();
	}

	public abstract void InitializeFrom(TData data);

	public abstract TData GetData();

	protected abstract void ClearData();

	protected abstract TAssetImpl Self { get; }
}
