﻿namespace HQS.VUE.Core;

internal class CommittedAssets<TAsset> : ICommittedAssets<TAsset>
	where TAsset : class
{
	private readonly Dictionary<Guid, Pin<TAsset>> committed =
		new Dictionary<Guid, Pin<TAsset>>(EqualityComparer.ForStruct<Guid>());

	public CommittedAssets()
	{
		this.Committed = ReadOnlyDictionary.Convert(this.committed, pin => pin.Value);
	}

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, TAsset> Committed { get; }

	// TODO nothing uses this yet
	public void AddOrUpdate<TWriter>(TWriter writer)
		where TWriter : IAssetWriter, ICommittableAsset<TAsset>
	{
		Debug.Assert(writer != null);

		if (this.committed.TryGetValue(writer.Key, out var oldPin))
		{
			oldPin.Dispose();
		}

		this.committed[writer.Key] = writer.ToCommittedAsset();
	}

	public void Remove(Guid assetKey)
	{
		if (this.committed.TryRemove(assetKey, out var oldPin))
		{
			oldPin.Dispose();
		}
	}

	public void Clear()
	{
		foreach (var pin in this.committed.Values)
		{
			pin.Dispose();
		}

		this.committed.Clear();
	}
}
