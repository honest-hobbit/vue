﻿namespace HQS.VUE.Core;

internal class AssetWriterTracker<TWriter>
	where TWriter : class, IAssetWriter, ITrackableAsset<TWriter>
{
	private readonly ConcurrentQueue<Pin<TWriter>> newWriters = new ConcurrentQueue<Pin<TWriter>>();

	private readonly HashSet<Guid> usedKeys = new HashSet<Guid>(EqualityComparer.ForStruct<Guid>());

	private readonly List<Pin<TWriter>> staged = new List<Pin<TWriter>>();

	private readonly IPinPool<TWriter> pool;

	public AssetWriterTracker(IPinPool<TWriter> pool)
	{
		Debug.Assert(pool != null);

		this.pool = pool;
		this.Staged = ReadOnlyList.Convert(this.staged, x => x.AsPinned);
	}

	public IReadOnlyList<Pinned<TWriter>> Staged { get; }

	public TWriter GetAssetWriter()
	{
		var pin = this.pool.Rent(out var writer);
		this.newWriters.Enqueue(pin);
		writer.SetAssetTracker(this);
		return writer;
	}

	// TODO nothing uses this yet
	public void StageChanges()
	{
		try
		{
			while (this.newWriters.TryDequeue(out var pin))
			{
				var writer = pin.Value;
				if (writer.ToBeIgnored)
				{
					// writer is ignored so skip to next iteration of loop
					pin.Dispose();
					continue;
				}

				if (!this.usedKeys.Add(writer.Key))
				{
					// multiple writers for the same asset
					throw new MultipleAssetWritersException();
				}

				this.staged.Add(pin);
			}
		}
		catch (Exception)
		{
			this.Clear();
			throw;
		}
	}

	public void Clear()
	{
		while (this.newWriters.TryDequeue(out var pin))
		{
			pin.Dispose();
		}

		for (int i = 0; i < this.staged.Count; i++)
		{
			this.staged[i].Dispose();
		}

		this.staged.Clear();
		this.usedKeys.Clear();
	}
}
