﻿namespace HQS.VUE.Core;

internal class AbstractAssetFactory : IAssetFactory
{
	protected bool IsInitialized { get; private set; } = false;

	protected DateTime CurrentTime { get; private set; }

	/// <inheritdoc />
	public void Initialize(DateTime currentTime)
	{
		Debug.Assert(!this.IsInitialized);

		this.IsInitialized = true;
		this.CurrentTime = currentTime;
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		this.IsInitialized = false;
	}
}
