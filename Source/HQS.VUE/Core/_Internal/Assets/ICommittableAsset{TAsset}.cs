﻿namespace HQS.VUE.Core;

internal interface ICommittableAsset<TAsset> : IAssetWriter
	where TAsset : class
{
	Pin<TAsset> ToCommittedAsset();
}
