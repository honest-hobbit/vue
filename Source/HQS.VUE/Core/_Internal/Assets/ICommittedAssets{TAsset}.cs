﻿namespace HQS.VUE.Core;

internal interface ICommittedAssets<TAsset>
{
	IReadOnlyDictionary<Guid, TAsset> Committed { get; }
}
