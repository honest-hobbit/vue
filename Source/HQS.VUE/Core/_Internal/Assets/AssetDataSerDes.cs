﻿namespace HQS.VUE.Core;

internal class AssetDataSerDes : CompositeSerDes<
	Guid, DateTime, DateTime, Guid, long, string, bool, AssetData>
{
	public AssetDataSerDes(
		ISerDes<Guid> key,
		ISerDes<DateTime> created,
		ISerDes<DateTime> lastModified,
		ISerDes<Guid> versionStamp,
		ISerDes<long> versionCount,
		ISerDes<string> displayName,
		ISerDes<bool> autoSave)
		: base(key, created, lastModified, versionStamp, versionCount, displayName, autoSave)
	{
	}

	public static ISerDes<AssetData> Instance { get; } = new AssetDataSerDes(
		new ConstantValueSerDes<Guid>(Guid.Empty),
		SerDes.OfDateTime,
		SerDes.OfDateTime,
		SerDes.OfGuid,
		SerDes.OfLong,
		StringSerDes.LengthAsVarint,
		SerDes.OfBool);

	/// <inheritdoc />
	protected override AssetData ComposeValue(
		Guid key,
		DateTime created,
		DateTime lastModified,
		Guid versionStamp,
		long versionCount,
		string displayName,
		bool autoSave) => new AssetData()
		{
			Key = key,
			Created = created,
			LastModified = lastModified,
			VersionStamp = versionStamp,
			VersionCount = versionCount,
			DisplayName = displayName,
			AutoSave = autoSave,
		};

	/// <inheritdoc />
	protected override void DecomposeValue(
		AssetData data,
		out Guid key,
		out DateTime created,
		out DateTime lastModified,
		out Guid versionStamp,
		out long versionCount,
		out string displayName,
		out bool autoSave)
	{
		key = data.Key;
		created = data.Created;
		lastModified = data.LastModified;
		versionStamp = data.VersionStamp;
		versionCount = data.VersionCount;
		displayName = data.DisplayName;
		autoSave = data.AutoSave;
	}
}
