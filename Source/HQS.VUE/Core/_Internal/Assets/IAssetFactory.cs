﻿namespace HQS.VUE.Core;

internal interface IAssetFactory
{
	void Initialize(DateTime currentTime);

	void Deinitialize();
}
