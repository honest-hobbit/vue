﻿namespace HQS.VUE.Core;

internal class AssetFactoryInitializer
{
	private readonly IAssetFactory[] factories;

	public AssetFactoryInitializer(IReadOnlyList<IAssetFactory> factories)
	{
		Debug.Assert(factories.AllAndSelfNotNull());

		this.factories = factories.ToArrayFromReadOnly();
	}

	public void InitializeFactories(DateTime currentTime)
	{
		for (int i = 0; i < this.factories.Length; i++)
		{
			this.factories[i].Initialize(currentTime);
		}
	}

	public void DeinitializeFactories()
	{
		for (int i = 0; i < this.factories.Length; i++)
		{
			this.factories[i].Deinitialize();
		}
	}
}
