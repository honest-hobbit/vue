﻿namespace HQS.VUE.Core;

internal interface ITrackableAsset<TWriter>
	where TWriter : class, IAssetWriter, ITrackableAsset<TWriter>
{
	void SetAssetTracker(AssetWriterTracker<TWriter> tracker);
}
