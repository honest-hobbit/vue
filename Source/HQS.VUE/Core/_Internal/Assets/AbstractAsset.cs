﻿namespace HQS.VUE.Core;

internal abstract class AbstractAsset : IAsset, IDeinitializable
{
	private IAssetStore store;

	/// <inheritdoc />
	public Guid Key { get; private set; }

	/// <inheritdoc />
	public DateTime Created { get; private set; }

	/// <inheritdoc />
	public DateTime LastModified { get; private set; }

	/// <inheritdoc />
	public Guid VersionStamp { get; private set; }

	/// <inheritdoc />
	public long VersionCount { get; private set; }

	/// <inheritdoc />
	public string DisplayName { get; private set; }

	/// <inheritdoc />
	public bool AutoSave { get; private set; }

	// TODO HasUnsavedChanges hasn't been implemented yet
	/// <inheritdoc />
	public bool HasUnsavedChanges { get; private set; }

	/// <inheritdoc />
	public object Store => this.store;

	public IAssetStore GetStore() => this.store;

	/// <inheritdoc />
	public virtual void Deinitialize()
	{
		this.InitializeBase(AssetData.Empty, null);
	}

	protected void InitializeBase(AssetData data, IAssetStore store)
	{
		this.Key = data.Key;
		this.Created = data.Created;
		this.LastModified = data.LastModified;
		this.VersionStamp = data.VersionStamp;
		this.VersionCount = data.VersionCount;
		this.DisplayName = data.DisplayName;
		this.AutoSave = data.AutoSave;

		this.store = store;

		// TODO HasUnsavedChanges hasn't been implemented yet
		this.HasUnsavedChanges = false;
	}

	protected AssetData GetAssetData() => AssetData.From(this);
}
