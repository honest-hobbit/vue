﻿namespace HQS.VUE.Core;

internal static class PooledTypeExtensions
{
	[Conditional(CompilationSymbol.Debug)]
	public static void VerifyAllAreUniqueInDebugOnly(this IEnumerable<PooledType> pooledTypes)
	{
		Debug.Assert(pooledTypes != null);

		var guids = new HashSet<Guid>(EqualityComparer.ForStruct<Guid>());
		var names = new HashSet<string>();
		var types = new HashSet<Type>();

		foreach (var pooledType in pooledTypes)
		{
			if (pooledType == null)
			{
				throw new ArgumentNullException(nameof(pooledTypes));
			}

			if (!guids.Add(pooledType.Key))
			{
				throw new ArgumentException($"{nameof(PooledType.Key)} must be unique.", nameof(pooledTypes));
			}

			if (!names.Add(pooledType.DisplayName))
			{
				throw new ArgumentException($"{nameof(PooledType.DisplayName)} must be unique.", nameof(pooledTypes));
			}

			if (!types.Add(pooledType.ResourceType))
			{
				throw new ArgumentException($"{nameof(PooledType.ResourceType)} must be unique.", nameof(pooledTypes));
			}
		}
	}
}
