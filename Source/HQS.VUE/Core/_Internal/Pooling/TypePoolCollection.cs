﻿namespace HQS.VUE.Core;

internal class TypePoolCollection : ITypePoolCollection
{
	private readonly Dictionary<Guid, ITypePool> pools;

	private readonly IEnumerable<Guid> keys;

	public TypePoolCollection(EngineStatus validator, IReadOnlyList<ITypePool> pools)
	{
		Debug.Assert(validator != null);
		Debug.Assert(pools.AllAndSelfNotNull());

		this.pools = new Dictionary<Guid, ITypePool>(pools.Count, EqualityComparer.ForStruct<Guid>());
		for (int i = 0; i < pools.Count; i++)
		{
			var pool = pools[i];

			// deliberately using Add so that a duplicate key will throw an exception
			this.pools.Add(pool.Key.Key, new TypePoolWrapper(validator, pool));
		}

		this.Keys = this.pools.Values.Select(x => x.Key).AsEnumerableOnly();
		this.Values = this.pools.Values.AsEnumerableOnly();
		this.keys = this.pools.Keys.AsEnumerableOnly();

		Debug.Assert(this.pools.Count == pools.Count);
	}

	/// <inheritdoc />
	public ITypePool this[PooledType key]
	{
		get
		{
			Ensure.That(key, nameof(key)).IsNotNull();

			return this.pools[key.Key];
		}
	}

	/// <inheritdoc />
	public ITypePool this[Guid key] => this.pools[key];

	/// <inheritdoc />
	public int Count => this.pools.Count;

	/// <inheritdoc />
	public IEnumerable<PooledType> Keys { get; }

	/// <inheritdoc />
	public IEnumerable<ITypePool> Values { get; }

	/// <inheritdoc />
	IEnumerable<Guid> IReadOnlyDictionary<Guid, ITypePool>.Keys => this.keys;

	/// <inheritdoc />
	public bool ContainsKey(PooledType key)
	{
		Ensure.That(key, nameof(key)).IsNotNull();

		return this.pools.ContainsKey(key.Key);
	}

	/// <inheritdoc />
	public bool ContainsKey(Guid key) => this.pools.ContainsKey(key);

	/// <inheritdoc />
	public bool TryGetValue(PooledType key, out ITypePool value)
	{
		Ensure.That(key, nameof(key)).IsNotNull();

		return this.pools.TryGetValue(key.Key, out value);
	}

	/// <inheritdoc />
	public bool TryGetValue(Guid key, out ITypePool value) => this.pools.TryGetValue(key, out value);

	/// <inheritdoc />
	public IEnumerator<KeyValuePair<PooledType, ITypePool>> GetEnumerator()
	{
		foreach (var pool in this.pools.Values)
		{
			yield return new KeyValuePair<PooledType, ITypePool>(pool.Key, pool);
		}
	}

	/// <inheritdoc />
	IEnumerator<KeyValuePair<Guid, ITypePool>> IEnumerable<KeyValuePair<Guid, ITypePool>>.GetEnumerator() => this.pools.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
