﻿namespace HQS.VUE.Core;

internal class PooledTypeResolver : IPooledTypeResolver
{
	private readonly Dictionary<Type, PooledType> pooledTypes = new Dictionary<Type, PooledType>();

	public PooledTypeResolver(IEnumerable<PooledType> types)
	{
		Debug.Assert(types != null);

		foreach (var type in types)
		{
			// deliberately using Add so that a duplicate key will throw an exception
			this.pooledTypes.Add(type.ResourceType, type);
		}
	}

	/// <inheritdoc />
	public PooledType this[Type resourceType]
	{
		get
		{
			Debug.Assert(resourceType != null);

			return this.pooledTypes[resourceType];
		}
	}
}
