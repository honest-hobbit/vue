﻿namespace HQS.VUE.Core;

internal interface IPooledTypeResolver
{
	PooledType this[Type resourceType] { get; }
}
