﻿namespace HQS.VUE.Core;

internal class TypePoolWrapper : ITypePool
{
	private readonly EngineStatus validator;

	private readonly ITypePool pool;

	public TypePoolWrapper(EngineStatus validator, ITypePool pool)
	{
		Debug.Assert(validator != null);
		Debug.Assert(pool != null);

		this.validator = validator;
		this.pool = pool;
	}

	/// <inheritdoc />
	public PooledType Key => this.pool.Key;

	/// <inheritdoc />
	public KeyValuePair<PooledType, PoolDiagnostics> Diagnostics => this.pool.Diagnostics;

	/// <inheritdoc />
	public int Create(int count)
	{
		this.validator.ValidateEngineStatus();
		Ensure.That(count, nameof(count)).IsGte(0);

		return this.pool.Create(count);
	}
}
