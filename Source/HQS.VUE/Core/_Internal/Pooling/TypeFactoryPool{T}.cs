﻿namespace HQS.VUE.Core;

internal class TypeFactoryPool<T> : AbstractDisposable, ITypePool, IPinPool<T>
	where T : class
{
	private readonly IPinPool<T> pool;

	public TypeFactoryPool(IPooledTypeResolver resolver, Func<T> factory)
	{
		Debug.Assert(resolver != null);
		Debug.Assert(factory != null);

		var key = resolver[typeof(T)];

		Debug.Assert(key != null);

		this.Key = key;
		this.pool = Pool.ThreadSafe.Pins.Create(factory);
	}

	/// <inheritdoc />
	public PooledType Key { get; }

	/// <inheritdoc />
	public KeyValuePair<PooledType, PoolDiagnostics> Diagnostics =>
		new KeyValuePair<PooledType, PoolDiagnostics>(this.Key, this.pool.GetDiagnostics());

	/// <inheritdoc />
	public int? BoundedCapacity => this.pool.BoundedCapacity;

	/// <inheritdoc />
	public int Available => this.pool.Available;

	/// <inheritdoc />
	public long Active => this.pool.Active;

	/// <inheritdoc />
	public long Created => this.pool.Created;

	/// <inheritdoc />
	public long Released => this.pool.Released;

	/// <inheritdoc />
	public int Create(int count) => this.pool.Create(count);

	/// <inheritdoc />
	public int Release(int count) => this.pool.Release(count);

	/// <inheritdoc />
	public Pin<T> Rent() => this.pool.Rent();

	/// <inheritdoc />
	public bool TryRent(out Pin<T> value) => this.pool.TryRent(out value);

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.pool.ReleaseAll();
}
