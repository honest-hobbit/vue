﻿namespace HQS.VUE.Core;

internal class Diagnostics : IDiagnostics
{
	public Diagnostics(Reporter<Exception> errorReporter, TypePoolCollection pools)
	{
		Debug.Assert(errorReporter != null);
		Debug.Assert(pools != null);

		this.ErrorOccurred = errorReporter.Values;
		this.Pools = pools;
	}

	public IObservable<Exception> ErrorOccurred { get; }

	public ITypePoolCollection Pools { get; }
}
