﻿namespace HQS.VUE.Core;

internal class EngineStatus
{
	private readonly ConcurrentBool isRunning = new ConcurrentBool(true);

	public bool IsRunning => this.isRunning.Value;

	public void Complete() => this.isRunning.ToggleIfTrue();

	public void ValidateEngineStatus()
	{
		if (!this.IsRunning)
		{
			throw new InvalidOperationException($"{nameof(IVoxelUniverse.Complete)} was already called.");
		}
	}
}
