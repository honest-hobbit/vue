﻿namespace HQS.VUE.Core;

internal class Reporter<T> : AbstractDisposable, IReporter<T>
{
	private readonly Subject<T> subject = new Subject<T>();

	public Reporter()
	{
		this.Values = this.subject.Synchronize();
	}

	public IObservable<T> Values { get; }

	/// <inheritdoc />
	public void Report(T value) => this.subject.OnNext(value);

	public void Complete() => this.subject.OnCompleted();

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.subject.Dispose();
}
