﻿namespace HQS.VUE.Core;

internal interface IReporter<T>
{
	void Report(T value);
}
