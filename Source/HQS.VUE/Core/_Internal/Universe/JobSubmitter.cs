﻿namespace HQS.VUE.Core;

internal class JobSubmitter : IJobSubmitter
{
	private readonly EngineStatus validator;

	private readonly ICommandSubmitter<RunJobCommand.Args> submitter;

	public JobSubmitter(EngineStatus validator, ICommandSubmitter<RunJobCommand.Args> submitter)
	{
		Debug.Assert(validator != null);
		Debug.Assert(submitter != null);

		this.validator = validator;
		this.submitter = submitter;
	}

	/// <inheritdoc />
	public Guid Submit(IJob job) => this.Submit(Pin.WithoutPool(job));

	/// <inheritdoc />
	public Guid Submit<TJob>(Pinned<TJob> job)
		where TJob : class, IJob => this.SubmitJobNoCompletion(job.CastTo<IJob>());

	/// <inheritdoc />
	public Guid Submit<TJob>(Pin<TJob> job)
		where TJob : class, IJob
	{
		using (job)
		{
			return this.Submit(job.AsPinned);
		}
	}

	/// <inheritdoc />
	public Guid Submit(IJob job, out Task<IJob> completion)
	{
		using var pooled = Pin.WithoutPool(job);
		var jobKey = this.SubmitJobWithCompletion(pooled.AsPinned, out var completionTask);
		////completion = completionTask.ContinueWith(t => job, TaskScheduler.Default);
		completion = ReturnJobAsync();
		return jobKey;

		async Task<IJob> ReturnJobAsync()
		{
			await completionTask.DontMarshallContext();
			return job;
		}
	}

	/// <inheritdoc />
	public Guid Submit<TJob>(Pinned<TJob> job, out Task<Pin<TJob>> completion)
		where TJob : class, IJob
	{
		var resultJob = job.CreatePin();
		try
		{
			var jobKey = this.SubmitJobWithCompletion(job.CastTo<IJob>(), out var completionTask);
			////completion = completionTask.ContinueWith(t => resultJob, TaskScheduler.Default);
			completion = ReturnJobAsync();
			return jobKey;

			async Task<Pin<TJob>> ReturnJobAsync()
			{
				try
				{
					await completionTask.DontMarshallContext();
					return resultJob;
				}
				catch (Exception)
				{
					resultJob.Dispose();
					throw;
				}
			}
		}
		catch (Exception)
		{
			resultJob.Dispose();
			throw;
		}
	}

	/// <inheritdoc />
	public Guid Submit<TJob>(Pin<TJob> job, out Task<Pin<TJob>> completion)
		where TJob : class, IJob
	{
		using (job)
		{
			return this.Submit(job.AsPinned, out completion);
		}
	}

	private Guid SubmitJobNoCompletion(Pinned<IJob> job)
	{
		this.ValidateJob(job);

		var args = new RunJobCommand.Args()
		{
			JobKey = Guid.NewGuid(),
			Job = job,
		};

		this.submitter.Submit(args);
		return args.JobKey;
	}

	private Guid SubmitJobWithCompletion(Pinned<IJob> job, out Task completion)
	{
		this.ValidateJob(job);

		var args = new RunJobCommand.Args()
		{
			JobKey = Guid.NewGuid(),
			Job = job,

			// TaskCreationOptions.RunContinuationsAsynchronously
			Completion = new TaskCompletionSource(),
		};

		this.submitter.Submit(args);
		completion = args.Completion.Task;
		return args.JobKey;
	}

	private void ValidateJob(Pinned<IJob> job)
	{
		this.validator.ValidateEngineStatus();

		if (!job.IsPinned)
		{
			throw new ArgumentException($"Pooled job must be assigned and pinned.", nameof(job));
		}

		if (job.Value == null)
		{
			throw new ArgumentNullException(nameof(job));
		}
	}
}
