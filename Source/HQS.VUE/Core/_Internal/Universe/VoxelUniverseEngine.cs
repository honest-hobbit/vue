﻿namespace HQS.VUE.Core;

internal class VoxelUniverseEngine : AbstractCompletable, IVoxelUniverse
{
	private readonly EngineStatus validator;

	private readonly Reporter<Exception> errorReporter;

	private readonly AggregateCompletable completables;

	public VoxelUniverseEngine(
		Diagnostics diagnostics,
		JobSubmitter jobs,
		EngineStatus validator,
		Reporter<Exception> errorReporter,
		MainCommandProcessor mainProcessor,
		ParallelCommandProcessor parallelProcessor,
		PersistenceCommandProcessor persistenceProcessor)
	{
		Debug.Assert(diagnostics != null);
		Debug.Assert(jobs != null);
		Debug.Assert(validator != null);
		Debug.Assert(errorReporter != null);
		Debug.Assert(mainProcessor != null);
		Debug.Assert(parallelProcessor != null);
		Debug.Assert(persistenceProcessor != null);

		this.Diagnostics = diagnostics;
		this.Jobs = jobs;

		this.validator = validator;
		this.errorReporter = errorReporter;

		this.completables = new AggregateCompletable(mainProcessor, parallelProcessor, persistenceProcessor);
	}

	/// <inheritdoc />
	public IDiagnostics Diagnostics { get; }

	/// <inheritdoc />
	public IJobSubmitter Jobs { get; }

	/// <inheritdoc />
	protected override async Task CompleteAsync()
	{
		try
		{
			this.validator.Complete();
			this.completables.Complete();
			await this.completables.Completion.DontMarshallContext();
		}
		catch (Exception error)
		{
			this.errorReporter.Report(error);
			throw;
		}
		finally
		{
			this.errorReporter.Complete();
		}
	}
}
