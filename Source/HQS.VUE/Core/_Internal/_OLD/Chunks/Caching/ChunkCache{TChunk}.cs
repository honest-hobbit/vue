﻿namespace HQS.VUE.Core;

internal class ChunkCache<TChunk> : AbstractDisposable, IKeyed<Guid>, IDeinitializable
{
	private readonly ICachedChunkFactory<TChunk> factory;

	private readonly MemoryCache<Int3, CachedChunk<TChunk>> cache;

	private IAssetStore store;

	private IChunkGenerator<TChunk> generator;

	public ChunkCache(ICachedChunkFactory<TChunk> factory)
	{
		Debug.Assert(factory != null);

		this.factory = factory;
		this.cache = new MemoryCache<Int3, CachedChunk<TChunk>>(factory, MortonCurve.EqualityComparer.ForInt3);
	}

	public bool IsInitialized => this.Key != Guid.Empty;

	/// <inheritdoc />
	public Guid Key { get; private set; }

	// store and generator can be null
	public void Initialize(
		Guid chunkGridKey,
		IAssetStore store,
		IChunkGenerator<TChunk> generator)
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(chunkGridKey != Guid.Empty);

		this.Key = chunkGridKey;
		this.store = store;
		this.generator = generator;
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		this.Key = default;
		this.store = null;
		this.generator = null;
	}

	public Pin<CachedChunk<TChunk>> GetChunkPin(Int3 chunkLocation)
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(this.IsInitialized);

		var pin = this.cache.GetCached(chunkLocation).Pin;
		var cachedChunk = pin.Value;
		if (cachedChunk.TryStartLoading())
		{
			try
			{
				if (this.store != null)
				{
					this.factory.LoadChunk(this.Key, pin.AsPinned, this.store, this.generator);
				}
				else if (this.generator != null)
				{
					cachedChunk.FinishedLoadingWithGeneratedData(this.generator.GenerateChunk(chunkLocation));
				}
				else
				{
					cachedChunk.FinishedLoadingWithoutData();
				}
			}
			catch (Exception error)
			{
				cachedChunk.FinishedLoadingWithError(error);
				throw;
			}
		}

		return pin;
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.cache.GetPinsSnapshot().TryExpireAll();
}
