﻿namespace HQS.VUE.Core;

internal class VersionedChunk<TChunk, TPin> : VersionedChunk<TChunk>, IDeinitializable
	where TChunk : IPinnable<TChunk, TPin>
	where TPin : IPin<TChunk, TPin>
{
	private const long UnitializedVersion = -1;

	private Int3 key;

	private Guid recordKey;

	private long version;

	private bool hasChunk;

	private TPin chunkPin;

	public VersionedChunk()
	{
		((IDeinitializable)this).Deinitialize();
	}

	public override bool IsInitialized => this.version == UnitializedVersion;

	/// <inheritdoc />
	public override Int3 Key
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.key;
		}
	}

	/// <inheritdoc />
	public override Guid RecordKey
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.recordKey;
		}
	}

	/// <inheritdoc />
	public override long Version
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.version;
		}
	}

	/// <inheritdoc />
	public override bool HasChunk
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.hasChunk;
		}
	}

	/// <inheritdoc />
	public override TChunk Chunk
	{
		get
		{
			Debug.Assert(this.IsInitialized);
			Debug.Assert(this.hasChunk);

			return this.chunkPin.Data;
		}
	}

	/// <inheritdoc />
	public override void InitializeNoChunk(Int3 chunkLocation, Guid recordKey)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(recordKey != Guid.Empty);

		this.key = chunkLocation;
		this.recordKey = recordKey;
	}

	/// <inheritdoc />
	public override void InitializeHasChunk(Int3 chunkLocation, Guid recordKey, long version, TChunk chunk)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(recordKey != Guid.Empty);
		Debug.Assert(version >= 0);
		Debug.Assert(chunk.HasData);

		this.key = chunkLocation;
		this.recordKey = recordKey;
		this.version = version;
		this.hasChunk = true;
		this.chunkPin = chunk.CreatePin();
	}

	/// <inheritdoc />
	void IDeinitializable.Deinitialize()
	{
		this.key = default;
		this.recordKey = default;
		this.version = UnitializedVersion;
		this.hasChunk = false;
		this.chunkPin.Dispose();
		this.chunkPin = default;
	}
}
