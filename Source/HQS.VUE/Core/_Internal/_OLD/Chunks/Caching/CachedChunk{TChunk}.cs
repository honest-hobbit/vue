﻿namespace HQS.VUE.Core;

internal class CachedChunk<TChunk> : IKeyed<Int3>, IDeinitializable
{
	private readonly ConcurrentBool hasLoadingStarted = new ConcurrentBool(false);

	private readonly PendingSignal loading = new PendingSignal();

	private readonly IPinPool<VersionedChunk<TChunk>> chunkPool;

	private Pin<IVersionedChunk<TChunk>> chunkPin;

	private Pin<CachedChunk<TChunk>> selfPoolPin;

	private Int3 key;

	public CachedChunk(IPinPool<VersionedChunk<TChunk>> chunkPool)
	{
		Debug.Assert(chunkPool != null);

		this.chunkPool = chunkPool;
	}

	public CachedChunkStatus Status { get; private set; } = CachedChunkStatus.Uninitialized;

	/// <inheritdoc />
	public Int3 Key
	{
		get
		{
			Debug.Assert(this.Status != CachedChunkStatus.Uninitialized);

			return this.key;
		}
	}

	public Pinned<IVersionedChunk<TChunk>> Data
	{
		get
		{
			Debug.Assert(this.Status != CachedChunkStatus.Uninitialized);

			this.loading.Wait();

			Debug.Assert(this.chunkPin.IsPinned);

			return this.chunkPin.AsPinned;
		}
	}

	public bool HasData => this.loading.HasValue;

	public bool HasException => this.loading.HasException;

	public void WaitForResults(out Pinned<IVersionedChunk<TChunk>> data, out Exception exception)
	{
		Debug.Assert(this.Status != CachedChunkStatus.Uninitialized);

		this.loading.WaitForResults(out exception);
		data = this.chunkPin.AsPinned;
	}

	public void Initialize<TSelf>(Pin<TSelf> selfPoolPin, Int3 key)
		where TSelf : CachedChunk<TChunk>
	{
		Debug.Assert(this.Status == CachedChunkStatus.Uninitialized);
		Debug.Assert(!this.loading.IsSet);
		Debug.Assert(this.selfPoolPin.IsUnassigned);
		Debug.Assert(selfPoolPin.IsPinned);
		Debug.Assert(selfPoolPin.Value == this);

		this.Status = CachedChunkStatus.Initialized;
		this.selfPoolPin = selfPoolPin.CastTo<CachedChunk<TChunk>>();
		this.key = key;
	}

	public bool TryStartLoading()
	{
		Debug.Assert(this.Status != CachedChunkStatus.Uninitialized);
		Debug.Assert(this.selfPoolPin.IsPinned);

		if (this.hasLoadingStarted.ToggleIfFalse())
		{
			this.Status = CachedChunkStatus.Loading;
			this.loading.Reset();
			return true;
		}
		else
		{
			return false;
		}
	}

	public void FinishedLoadingWithError(Exception exception)
	{
		this.FinishedLoadingContracts();
		Debug.Assert(exception != null);

		this.Status = CachedChunkStatus.Loaded;
		this.loading.SetException(exception);
		this.hasLoadingStarted.SetFalse();
	}

	public void FinishedLoadingWithoutData()
	{
		this.FinishedLoadingContracts();

		this.Status = CachedChunkStatus.Loaded;
		this.AssignAndGetVersionedChunk().InitializeNoChunk(this.key, Guid.NewGuid());
		this.loading.Set();
	}

	public void FinishedLoadingWithGeneratedData(TChunk chunk)
	{
		this.FinishedLoadingContracts();

		this.Status = CachedChunkStatus.Loaded;
		this.AssignAndGetVersionedChunk().InitializeHasChunk(this.key, Guid.NewGuid(), 0, chunk);
		this.loading.Set();
	}

	public void FinishedLoadingWithStoredData(Guid recordKey, long version, TChunk chunk)
	{
		this.FinishedLoadingContracts();
		Debug.Assert(recordKey != Guid.Empty);
		Debug.Assert(version >= 0);

		this.Status = CachedChunkStatus.Loaded;
		this.AssignAndGetVersionedChunk().InitializeHasChunk(this.key, recordKey, version, chunk);
		this.loading.Set();
	}

	public void UpdateData(long version, TChunk chunk)
	{
		Debug.Assert(this.Status == CachedChunkStatus.Loaded);
		Debug.Assert(this.loading.IsSet);
		Debug.Assert(this.selfPoolPin.IsPinned);
		Debug.Assert(this.chunkPin.IsPinned);
		Debug.Assert(version >= 0);
		Debug.Assert(version > this.chunkPin.Value.Version);

		using var previousPin = this.chunkPin;
		this.AssignAndGetVersionedChunk().InitializeHasChunk(
			this.key, previousPin.Value.RecordKey, version, chunk);
	}

	public void DisposeSelfPoolPin()
	{
		Debug.Assert(this.Status == CachedChunkStatus.Loaded);
		Debug.Assert(this.loading.IsSet);
		Debug.Assert(this.selfPoolPin.IsPinned);

		this.selfPoolPin.Dispose();
	}

	/// <inheritdoc />
	void IDeinitializable.Deinitialize()
	{
		Debug.Assert(this.Status == CachedChunkStatus.Loaded);
		Debug.Assert(this.loading.IsSet);
		Debug.Assert(this.selfPoolPin.IsDisposed);

		this.key = default;
		this.Status = CachedChunkStatus.Uninitialized;
		this.hasLoadingStarted.SetFalse();
		this.loading.Reset();
		this.chunkPin.Dispose();
		this.chunkPin = default;
		this.selfPoolPin = default;
	}

	[Conditional(CompilationSymbol.Debug)]
	private void FinishedLoadingContracts()
	{
		Debug.Assert(this.Status == CachedChunkStatus.Loading);
		Debug.Assert(this.hasLoadingStarted);
		Debug.Assert(!this.loading.IsSet);
		Debug.Assert(this.chunkPin.IsUnassigned);
		Debug.Assert(this.selfPoolPin.IsPinned);
	}

	private VersionedChunk<TChunk> AssignAndGetVersionedChunk()
	{
		this.chunkPin = this.chunkPool.Rent(out var chunk).CastTo<IVersionedChunk<TChunk>>();
		return chunk;
	}
}
