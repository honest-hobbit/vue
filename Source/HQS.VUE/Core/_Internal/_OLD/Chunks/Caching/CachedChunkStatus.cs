﻿namespace HQS.VUE.Core;

internal enum CachedChunkStatus
{
	Uninitialized,

	Initialized,

	Loading,

	Loaded,
}
