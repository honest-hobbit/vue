﻿namespace HQS.VUE.Core;

internal abstract class VersionedChunk<TChunk> : IVersionedChunk<TChunk>
{
	public abstract bool IsInitialized { get; }

	public abstract Int3 Key { get; }

	public abstract Guid RecordKey { get; }

	public abstract long Version { get; }

	public abstract bool HasChunk { get; }

	public abstract TChunk Chunk { get; }

	public abstract void InitializeNoChunk(Int3 chunkLocation, Guid recordKey);

	public abstract void InitializeHasChunk(Int3 chunkLocation, Guid recordKey, long version, TChunk chunk);
}
