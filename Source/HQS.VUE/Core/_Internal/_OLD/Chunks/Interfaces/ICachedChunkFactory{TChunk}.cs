﻿namespace HQS.VUE.Core;

internal interface ICachedChunkFactory<TChunk> :
	ICacheValueFactory<Int3, CachedChunk<TChunk>>
{
	// store must not be null
	// generator can be null
	void LoadChunk(
		Guid chunkGridKey,
		Pinned<CachedChunk<TChunk>> chunkPin,
		IAssetStore store,
		IChunkGenerator<TChunk> generator);
}
