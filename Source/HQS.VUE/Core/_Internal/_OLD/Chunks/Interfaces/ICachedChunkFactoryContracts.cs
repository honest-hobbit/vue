﻿namespace HQS.VUE.Core;

internal static class ICachedChunkFactoryContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void CreateValue(Int3 chunkLocation)
	{
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void EnqueueToken<TChunk>(Int3 chunkLocation, CachedChunk<TChunk> chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Status == CachedChunkStatus.Loaded);
		Debug.Assert(chunk.Key == chunkLocation);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void ValueExpired<TChunk>(Int3 chunkLocation, CachedChunk<TChunk> chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Status == CachedChunkStatus.Loaded);
		Debug.Assert(chunk.Key == chunkLocation);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void LoadChunk<TChunk>(
		Guid chunkGridKey,
		Pinned<CachedChunk<TChunk>> chunkPin,
		IAssetStore store,
		IChunkGenerator<TChunk> generator)
	{
		Debug.Assert(chunkGridKey != Guid.Empty);
		Debug.Assert(chunkPin.IsPinned);
		var chunk = chunkPin.Value;
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Status == CachedChunkStatus.Loading);
		Debug.Assert(store != null);
	}
}
