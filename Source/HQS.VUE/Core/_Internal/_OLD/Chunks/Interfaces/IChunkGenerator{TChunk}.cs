﻿namespace HQS.VUE.Core;

internal interface IChunkGenerator<TChunk>
{
	TChunk GenerateChunk(Int3 chunkLocation);
}
