﻿namespace HQS.VUE.Core;

internal interface IVersionedChunk<TChunk> : IKeyed<Int3>
{
	Guid RecordKey { get; }

	long Version { get; }

	bool HasChunk { get; }

	TChunk Chunk { get; }
}
