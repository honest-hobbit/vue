﻿namespace HQS.VUE.Core;

internal interface IChunkBuilder<TChunk, TPin>
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
{
	void SetCopyOf(TChunk copyingChunk);

	void SetCopyOf(TPin copyingPin);

	TPin Build(bool optimize = true);

	bool TryBuildChangeset(out ChunkChangeset<TChunk, TPin>.Pin changesetPin);
}
