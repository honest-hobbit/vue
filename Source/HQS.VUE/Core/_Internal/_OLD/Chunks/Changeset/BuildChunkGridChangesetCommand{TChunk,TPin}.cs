﻿namespace HQS.VUE.Core;

internal class BuildChunkGridChangesetCommand<TChunk, TPin> :
	AbstractResultCommand<Pinned<ChunkGridChangeset<TChunk, TPin>>>,
	IParallelCommand, IInitializable<BuildChunkGridChangesetCommand<TChunk, TPin>.Args>
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
{
	private readonly List<ChunkBuilderPair<TChunk, TPin>.Pin> builderPairs = new List<ChunkBuilderPair<TChunk, TPin>.Pin>();

	private readonly PinList<CachedChunk<TChunk>> cachedChunkPins = new PinList<CachedChunk<TChunk>>();

	private readonly CommandWaiter<ChunkChangesetPair<TChunk, TPin>?> commandWaiter =
		new CommandWaiter<ChunkChangesetPair<TChunk, TPin>?>();

	private readonly ICommandSubmitter<ChunkBuilderPair<TChunk, TPin>, ChunkChangesetPair<TChunk, TPin>?> buildChunkSubmitter;

	private readonly IPinPool<ChunkGridChangeset<TChunk, TPin>> gridChangesetPool;

	private Guid chunkGridKey;

	private long chunkGridVersion;

	private Pin<ChunkGridChangeset<TChunk, TPin>> gridChangesetPin;

	public BuildChunkGridChangesetCommand(
		ICommandSubmitter<ChunkBuilderPair<TChunk, TPin>, ChunkChangesetPair<TChunk, TPin>?> buildChunkSubmitter,
		IPinPool<ChunkGridChangeset<TChunk, TPin>> gridChangesetPool)
	{
		Debug.Assert(buildChunkSubmitter != null);
		Debug.Assert(gridChangesetPool != null);

		this.buildChunkSubmitter = buildChunkSubmitter;
		this.gridChangesetPool = gridChangesetPool;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.ChunkGridKey != Guid.Empty);
		Debug.Assert(args.ChunkGridVersion >= 0);
		Debug.Assert(args.BuilderPairs != null);
		Debug.Assert(args.CachedChunks != null);
		this.ValidateAndSetInitialized();

		this.chunkGridKey = args.ChunkGridKey;
		this.chunkGridVersion = args.ChunkGridVersion;

		int count = args.BuilderPairs.Count;
		for (int i = 0; i < count; i++)
		{
			this.builderPairs.Add(args.BuilderPairs[i].CreatePin());
		}

		this.cachedChunkPins.AddMany(args.CachedChunks);
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.builderPairs.DisposeAllAndClear();
		this.cachedChunkPins.DisposeAllAndClear();
		this.commandWaiter.Clear();
		this.chunkGridKey = Guid.Empty;
		this.chunkGridVersion = -1;
		this.gridChangesetPin.Dispose();
		this.gridChangesetPin = default;
	}

	/// <inheritdoc />
	protected override Pinned<ChunkGridChangeset<TChunk, TPin>> RunAndGetResult()
	{
		int count = this.builderPairs.Count;
		for (int i = 0; i < count; i++)
		{
			this.commandWaiter.Add(this.buildChunkSubmitter.SubmitAndGetWaitableResult(this.builderPairs[i].Data));
		}

		this.builderPairs.DisposeAllAndClear();

		this.gridChangesetPin = this.gridChangesetPool.Rent(out var gridChangeset);
		gridChangeset.Initialize(this.chunkGridKey, this.chunkGridVersion, this.commandWaiter.Result, this.cachedChunkPins);

		return this.gridChangesetPin.AsPinned;
	}

	public struct Args
	{
		public Guid ChunkGridKey;

		public long ChunkGridVersion;

		public IReadOnlyList<ChunkBuilderPair<TChunk, TPin>> BuilderPairs;

		public IReadOnlyList<Pinned<CachedChunk<TChunk>>> CachedChunks;
	}
}
