﻿namespace HQS.VUE.Core;

internal class BuildChunkChangesetCommand<TChunk, TPin> :
	AbstractResultCommand<ChunkChangesetPair<TChunk, TPin>?>, IParallelCommand, IInitializable<ChunkBuilderPair<TChunk, TPin>>
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
{
	private ChunkBuilderPair<TChunk, TPin>.Pin builderPin;

	private ChunkChangesetPair<TChunk, TPin>.Pin changesetPin;

	/// <inheritdoc />
	public void Initialize(ChunkBuilderPair<TChunk, TPin> pair)
	{
		Debug.Assert(pair.HasData);
		this.ValidateAndSetInitialized();

		this.builderPin = pair.CreatePin();
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.builderPin.Dispose();
		this.builderPin = default;
		this.changesetPin.Dispose();
		this.changesetPin = default;
	}

	/// <inheritdoc />
	protected override ChunkChangesetPair<TChunk, TPin>? RunAndGetResult()
	{
		var builderPair = this.builderPin.Data;
		if (builderPair.ChunkBuilder.Value.TryBuildChangeset(out var changesetPin))
		{
			this.changesetPin = new ChunkChangesetPair<TChunk, TPin>.Pin(builderPair.CachedChunk.CreatePin(), changesetPin);
			return this.changesetPin.Data;
		}
		else
		{
			return null;
		}
	}
}
