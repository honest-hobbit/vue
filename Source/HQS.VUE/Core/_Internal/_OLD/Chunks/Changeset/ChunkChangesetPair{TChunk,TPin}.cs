﻿namespace HQS.VUE.Core;

internal readonly struct ChunkChangesetPair<TChunk, TPin> :
	IPinnable<ChunkChangesetPair<TChunk, TPin>, ChunkChangesetPair<TChunk, TPin>.Pin>
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
{
	private readonly Pin<CachedChunk<TChunk>> cachedChunkPin;

	private readonly ChunkChangeset<TChunk, TPin>.Pin changesetPin;

	private ChunkChangesetPair(Pin<CachedChunk<TChunk>> cachedChunkPin, ChunkChangeset<TChunk, TPin>.Pin changesetPin)
	{
		this.cachedChunkPin = cachedChunkPin;
		this.changesetPin = changesetPin;
	}

	/// <inheritdoc />
	public bool HasData => this.cachedChunkPin.IsPinned;

	public Pinned<CachedChunk<TChunk>> CachedChunk => this.cachedChunkPin.AsPinned;

	public ChunkChangeset<TChunk, TPin> Changeset => this.changesetPin.Data;

	/// <inheritdoc />
	public Pin CreatePin()
	{
		Debug.Assert(this.HasData);

		return new Pin(this.cachedChunkPin.CreatePin(), this.changesetPin.Data.CreatePin());
	}

	public readonly struct Pin : IPin<ChunkChangesetPair<TChunk, TPin>, Pin>
	{
		public Pin(Pin<CachedChunk<TChunk>> cachedChunkPin, ChunkChangeset<TChunk, TPin>.Pin changesetPin)
		{
			Debug.Assert(cachedChunkPin.IsPinned);
			Debug.Assert(changesetPin.Data.HasData);

			this.Data = new ChunkChangesetPair<TChunk, TPin>(cachedChunkPin, changesetPin);
		}

		/// <inheritdoc />
		public ChunkChangesetPair<TChunk, TPin> Data { get; }

		/// <inheritdoc />
		public bool HasData => this.Data.HasData;

		/// <inheritdoc />
		public Pin CreatePin() => this.Data.CreatePin();

		/// <inheritdoc />
		public void Dispose()
		{
			this.Data.cachedChunkPin.Dispose();
			this.Data.changesetPin.Dispose();
		}
	}
}
