﻿namespace HQS.VUE.Core;

internal readonly struct ChunkBuilderPair<TChunk, TPin> :
	IPinnable<ChunkBuilderPair<TChunk, TPin>, ChunkBuilderPair<TChunk, TPin>.Pin>
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
{
	private readonly Pin<CachedChunk<TChunk>> cachedChunkPin;

	private readonly Pin<IChunkBuilder<TChunk, TPin>> chunkBuilderPin;

	private ChunkBuilderPair(Pin<CachedChunk<TChunk>> cachedChunkPin, Pin<IChunkBuilder<TChunk, TPin>> chunkBuilderPin)
	{
		this.cachedChunkPin = cachedChunkPin;
		this.chunkBuilderPin = chunkBuilderPin;
	}

	/// <inheritdoc />
	public bool HasData => this.cachedChunkPin.IsPinned;

	public Pinned<CachedChunk<TChunk>> CachedChunk => this.cachedChunkPin.AsPinned;

	public Pinned<IChunkBuilder<TChunk, TPin>> ChunkBuilder => this.chunkBuilderPin.AsPinned;

	/// <inheritdoc />
	public Pin CreatePin()
	{
		Debug.Assert(this.HasData);

		return new Pin(this.cachedChunkPin.CreatePin(), this.chunkBuilderPin.CreatePin());
	}

	public readonly struct Pin : IPin<ChunkBuilderPair<TChunk, TPin>, Pin>
	{
		public Pin(Pin<CachedChunk<TChunk>> cachedChunkPin, Pin<IChunkBuilder<TChunk, TPin>> chunkBuilderPin)
		{
			Debug.Assert(cachedChunkPin.IsPinned);
			Debug.Assert(chunkBuilderPin.IsPinned);

			this.Data = new ChunkBuilderPair<TChunk, TPin>(cachedChunkPin, chunkBuilderPin);
		}

		/// <inheritdoc />
		public ChunkBuilderPair<TChunk, TPin> Data { get; }

		/// <inheritdoc />
		public bool HasData => this.Data.HasData;

		/// <inheritdoc />
		public Pin CreatePin() => this.Data.CreatePin();

		/// <inheritdoc />
		public void Dispose()
		{
			this.Data.cachedChunkPin.Dispose();
			this.Data.chunkBuilderPin.Dispose();
		}
	}
}
