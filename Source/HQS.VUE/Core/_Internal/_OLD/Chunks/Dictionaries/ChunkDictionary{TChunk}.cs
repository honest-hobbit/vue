﻿namespace HQS.VUE.Core;

internal class ChunkDictionary<TChunk> : IReadOnlyList<KeyValuePair<Int3, Pinned<TChunk>>>
	where TChunk : class
{
	private readonly Dictionary<Int3, Pinned<TChunk>> dictionary =
		new Dictionary<Int3, Pinned<TChunk>>(MortonCurve.EqualityComparer.ForInt3);

	private readonly List<KeyValuePair<Int3, Pin<TChunk>>> list =
		new List<KeyValuePair<Int3, Pin<TChunk>>>();

	private readonly Func<Int3, Pin<TChunk>> factory;

	public ChunkDictionary(Func<Int3, Pin<TChunk>> factory)
	{
		Debug.Assert(factory != null);

		this.factory = factory;
	}

	/// <inheritdoc />
	public int Count => this.list.Count;

	/// <inheritdoc />
	public KeyValuePair<Int3, Pinned<TChunk>> this[int index]
	{
		get
		{
			IReadOnlyListContracts.Indexer(this, index);

			var pair = this.list[index];
			return new KeyValuePair<Int3, Pinned<TChunk>>(pair.Key, pair.Value.AsPinned);
		}
	}

	public Pinned<TChunk> this[Int3 chunkLocation]
	{
		get
		{
			if (!this.dictionary.TryGetValue(chunkLocation, out var pinView))
			{
				var pin = this.factory(chunkLocation);
				this.list.Add(new KeyValuePair<Int3, Pin<TChunk>>(chunkLocation, pin));
				pinView = pin.AsPinned;
				this.dictionary[chunkLocation] = pinView;
			}

			return pinView;
		}
	}

	public void Add(Int3 location, Pinned<TChunk> pin)
	{
		Debug.Assert(pin.IsPinned);

		if (!this.dictionary.ContainsKey(location))
		{
			var newPin = pin.CreatePin();
			this.dictionary[location] = newPin.AsPinned;
			this.list.Add(new KeyValuePair<Int3, Pin<TChunk>>(location, newPin));
		}
	}

	public void Add(KeyValuePair<Int3, Pinned<TChunk>> pair) => this.Add(pair.Key, pair.Value);

	public void AddMany(IReadOnlyList<KeyValuePair<Int3, Pinned<TChunk>>> pins)
	{
		Debug.Assert(pins != null);

		int count = pins.Count;
		for (int i = 0; i < count; i++)
		{
			this.Add(pins[i]);
		}
	}

	public void Clear()
	{
		int count = this.list.Count;
		for (int i = 0; i < count; i++)
		{
			this.list[i].Value.Dispose();
		}

		this.list.Clear();
		this.dictionary.Clear();
	}

	/// <inheritdoc />
	public IEnumerator<KeyValuePair<Int3, Pinned<TChunk>>> GetEnumerator()
	{
		int count = this.Count;
		for (int i = 0; i < count; i++)
		{
			yield return this[i];
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
