﻿namespace HQS.VUE.Core;

internal class ChunkBuilderDictionary<TChunk, TPin, TBuilder> : IKeyed<Guid>
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
	where TBuilder : class, IChunkBuilder<TChunk, TPin>
{
	private readonly PinList<ChunkBuilderPair<TChunk, TPin>, ChunkBuilderPair<TChunk, TPin>.Pin> builderPins =
		new PinList<ChunkBuilderPair<TChunk, TPin>, ChunkBuilderPair<TChunk, TPin>.Pin>();

	private readonly CachedChunkDictionary<TChunk> cachedChunks;

	private readonly IPinPool<TBuilder> chunkBuilderPool;

	private readonly ChunkDictionary<TBuilder> chunkBuilders;

	public ChunkBuilderDictionary(
		CachedChunkDictionary<TChunk> cachedChunks,
		IPinPool<TBuilder> chunkBuilderPool)
	{
		Debug.Assert(cachedChunks != null);
		Debug.Assert(chunkBuilderPool != null);

		this.cachedChunks = cachedChunks;
		this.chunkBuilderPool = chunkBuilderPool;
		this.chunkBuilders = new ChunkDictionary<TBuilder>(this.GetChunkBuilderPin);
	}

	public bool IsInitialized => this.cachedChunks.IsInitialized;

	/// <inheritdoc />
	public Guid Key => this.cachedChunks.Key;

	public IReadOnlyList<ChunkBuilderPair<TChunk, TPin>> BuilderPairs
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.builderPins;
		}
	}

	public IReadOnlyList<Pinned<CachedChunk<TChunk>>> CachedChunks
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.cachedChunks;
		}
	}

	public Pinned<TBuilder> this[Int3 chunkLocation]
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.chunkBuilders[chunkLocation];
		}
	}

	public void DiscardChanges()
	{
		Debug.Assert(this.IsInitialized);

		this.builderPins.DisposeAllAndClear();
		this.chunkBuilders.Clear();
	}

	private Pin<TBuilder> GetChunkBuilderPin(Int3 chunkLocation)
	{
		Debug.Assert(this.IsInitialized);

		var builderPin = this.chunkBuilderPool.Rent(out var builder);

		var cachedChunkPin = this.cachedChunks[chunkLocation];
		var versionedChunk = cachedChunkPin.Value.Data.Value;

		Debug.Assert(versionedChunk.HasChunk);

		builder.SetCopyOf(versionedChunk.Chunk);
		this.builderPins.Add(new ChunkBuilderPair<TChunk, TPin>.Pin(
			cachedChunkPin.CreatePin(), builderPin.CastTo<IChunkBuilder<TChunk, TPin>>().CreatePin()));

		return builderPin;
	}
}
