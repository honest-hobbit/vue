﻿namespace HQS.VUE.Core;

internal class CachedChunkDictionary<TChunk> : IKeyed<Guid>, IReadOnlyList<Pinned<CachedChunk<TChunk>>>
{
	private readonly ChunkCache<TChunk> chunkCache;

	private readonly ChunkDictionary<CachedChunk<TChunk>> chunks;

	public CachedChunkDictionary(ChunkCache<TChunk> chunkCache)
	{
		Debug.Assert(chunkCache != null);

		this.chunkCache = chunkCache;
		this.chunks = new ChunkDictionary<CachedChunk<TChunk>>(this.GetChunkPin);
	}

	public bool IsInitialized => this.chunkCache.IsInitialized;

	/// <inheritdoc />
	public Guid Key => this.chunkCache.Key;

	/// <inheritdoc />
	public int Count => this.chunks.Count;

	public Pinned<CachedChunk<TChunk>> this[int index]
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.chunks[index].Value;
		}
	}

	public Pinned<CachedChunk<TChunk>> this[Int3 chunkLocation]
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.chunks[chunkLocation];
		}
	}

	public void AddMany(IReadOnlyList<Pinned<CachedChunk<TChunk>>> pins)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(pins != null);

		int count = pins.Count;
		for (int i = 0; i < count; i++)
		{
			var pin = pins[i];
			this.chunks.Add(pin.Value.Key, pin);
		}
	}

	public void Clear() => this.chunks.Clear();

	/// <inheritdoc />
	public IEnumerator<Pinned<CachedChunk<TChunk>>> GetEnumerator()
	{
		int count = this.chunks.Count;
		for (int i = 0; i < count; i++)
		{
			yield return this.chunks[i].Value;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private Pin<CachedChunk<TChunk>> GetChunkPin(Int3 chunkLocation)
	{
		Debug.Assert(this.IsInitialized);

		return this.chunkCache.GetChunkPin(chunkLocation);
	}
}
