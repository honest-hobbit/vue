﻿namespace HQS.VUE.Core;

internal class SerializeChunkCommand<TChunk> :
	AbstractResultCommand<ChunkDocument<Int3>>, IParallelCommand, IInitializable<Pinned<IVersionedChunk<TChunk>>>
{
	private readonly ISerializer<TChunk> serializer;

	private readonly IPinPool<IByteBuffer> buffers;

	private Pin<IVersionedChunk<TChunk>> chunkPin;

	public SerializeChunkCommand(ISerializer<TChunk> serializer, IPinPool<IByteBuffer> buffers)
	{
		Debug.Assert(serializer != null);
		Debug.Assert(buffers != null);

		this.serializer = serializer;
		this.buffers = buffers;
	}

	/// <inheritdoc />
	public void Initialize(Pinned<IVersionedChunk<TChunk>> chunkPin)
	{
		Debug.Assert(chunkPin.IsPinned);
		Debug.Assert(chunkPin.Value.HasChunk);
		this.ValidateAndSetInitialized();

		this.chunkPin = chunkPin.CreatePin();
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.chunkPin.Dispose();
		this.chunkPin = default;
	}

	/// <inheritdoc />
	protected override ChunkDocument<Int3> RunAndGetResult()
	{
		var chunk = this.chunkPin.Value;
		using var buffer = this.buffers.Rent();

		// TODO broke this when switching to Streams
		return default;
		////return new ChunkRecord<Int3>(
		////	chunk.RecordKey,
		////	chunk.Key,
		////	chunk.Version,
		////	buffer.Value.Serialize(this.serializer, chunk.Chunk));
	}
}
