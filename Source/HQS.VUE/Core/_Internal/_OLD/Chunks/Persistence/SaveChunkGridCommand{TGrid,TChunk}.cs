﻿////using System;
////using System.Collections.Generic;
////using System.Diagnostics.CodeAnalysis;
////using HQS.Utility;
////using HQS.Utility.Contracts;
////using HQS.Utility.Resources;

////namespace HQS.VUE.Core
////{
////	internal class SaveChunkGridCommand<TGrid, TChunk> :
////		AbstractCommand, IPersistenceCommand, IInitializable<SaveChunkGridCommand<TGrid, TChunk>.Args>
////		where TChunk : IKeyed<ChunkKey>
////	{
////		private readonly CommandWaiter<ChunkRecord> serializing = new CommandWaiter<ChunkRecord>();

////		private readonly ICommandSubmitter<PinView<IVersionedChunk<TChunk>>, ChunkRecord> serializeProcessor;

////		private readonly EngineErrorReporter errorReporter;

////		private Volume volume;

////		private IChunkGridStore<TGrid> store;

////		private TGrid gridRecord;

////		//private Pin<PinSet<TChunk>> chunkPins;

////		public SaveChunkGridCommand(
////			ICommandSubmitter<PinView<IVersionedChunk<TChunk>>, ChunkRecord> serializeProcessor,
////			////ChunkGridSavedSubmitter savedProcessor,
////			////SaveChunkGridFailedSubmitter failedProcessor,
////			EngineErrorReporter errorReporter)
////		{
////			Debug.Assert(serializeProcessor != null);
////			////Debug.Assert(savedProcessor != null);
////			////Debug.Assert(failedProcessor != null);
////			Debug.Assert(errorReporter != null);

////			this.serializeProcessor = serializeProcessor;
////			////this.SavedProcessor = savedProcessor;
////			////this.FailedProcessor = failedProcessor;
////			this.errorReporter = errorReporter;
////		}

////		/// <inheritdoc />
////		public void Initialize(Args args)
////		{
////			Debug.Assert(args.Store != null);
////			this.ValidateAndSetInitialized();

////			this.store = args.Store;
////			this.gridRecord = args.GridRecord;

////			if (args.Chunks != null)
////			{
////				int count = args.Chunks.Count;
////				for (int i = 0; i < count; i++)
////				{
////					this.serializing.Add(this.serializeProcessor.SubmitAndGetResult(args.Chunks[i]));
////				}
////			}
////		}

////		/// <inheritdoc />
////		protected override void OnDeinitialize()
////		{
////			this.serializing.Clear();
////			this.volume = null;
////			this.store = null;
////			this.gridRecord = default;
////			//this.chunkPins = default;
////		}

////		/// <inheritdoc />
////		protected override void OnRun()
////		{
////			try
////			{
////				this.store.UpdateChunks(this.gridRecord, this.serializing.Result);
////				//this.chunkPins.Dispose();

////				//this.SubmitSaveCompletedCommand(this.volume, this.gridRecord);
////			}
////			catch (Exception error)
////			{
////				this.errorReporter.ReportError(error);
////				//this.SubmitSaveFailedCommand(this.volume.EngineData, this.chunkPins);
////			}
////		}

////		public struct Args
////		{
////			public IChunkGridStore<TGrid> Store;

////			public TGrid GridRecord;

////			public IReadOnlyList<PinView<IVersionedChunk<TChunk>>> Chunks;
////		}
////	}
////}
