﻿namespace HQS.VUE.Core;

internal class LoadChunkCommand<TChunk, TPin> :
	AbstractCommand, IParallelCommand, IInitializable<LoadChunkCommand<TChunk, TPin>.Args>
	where TChunk : IPinnable<TChunk, TPin>
	where TPin : IPin<TChunk, TPin>
{
	private readonly IDeserializer<TPin> deserializer;

	private readonly IPinPool<IByteBuffer> buffers;

	private Guid chunkGridKey;

	private Pin<CachedChunk<TChunk>> chunkPin;

	private IAssetStore store;

	private IChunkGenerator<TChunk> generator;

	public LoadChunkCommand(IDeserializer<TPin> deserializer, IPinPool<IByteBuffer> buffers)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(buffers != null);

		this.deserializer = deserializer;
		this.buffers = buffers;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.ChunkGridKey != Guid.Empty);
		Debug.Assert(args.ChunkPin.IsPinned);
		var chunk = args.ChunkPin.Value;
		Debug.Assert(chunk.Status == CachedChunkStatus.Loading);
		Debug.Assert(args.Store != null);
		this.ValidateAndSetInitialized();

		this.chunkGridKey = args.ChunkGridKey;
		this.chunkPin = args.ChunkPin.CreatePin();
		this.store = args.Store;
		this.generator = args.Generator;
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.chunkPin.Dispose();
		this.chunkPin = default;
		this.store = null;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		var cachedChunk = this.chunkPin.Value;

		try
		{
			if (this.store.TryGetChunk(this.chunkGridKey, cachedChunk.Key, out var record))
			{
				using var buffer = this.buffers.Rent();

				// TODO broke this when switching to Streams
				////using var chunkPin = buffer.Value.Deserialize(this.deserializer, record.Data);
				////cachedChunk.FinishedLoadingWithStoredData(record.Key, record.ID.Version, chunkPin.Data);
			}
			else
			{
				if (this.generator != null)
				{
					cachedChunk.FinishedLoadingWithGeneratedData(this.generator.GenerateChunk(cachedChunk.Key));
				}
				else
				{
					cachedChunk.FinishedLoadingWithoutData();
				}
			}
		}
		catch (Exception error)
		{
			cachedChunk.FinishedLoadingWithError(error);
			throw;
		}
	}

	internal struct Args
	{
		public Guid ChunkGridKey;

		public Pinned<CachedChunk<TChunk>> ChunkPin;

		// must not be null
		public IAssetStore Store;

		// can be null
		public IChunkGenerator<TChunk> Generator;
	}
}
