﻿namespace HQS.VUE.Core;

internal class PinnedChunkSet<TChunk> : IKeyed<Guid>, IReadOnlyList<Pinned<CachedChunk<TChunk>>>, IDeinitializable
{
	private readonly HashSet<Int3> pinLocations = new HashSet<Int3>(MortonCurve.EqualityComparer.ForInt3);

	private readonly List<Pin<CachedChunk<TChunk>>> pins = new List<Pin<CachedChunk<TChunk>>>();

	public bool IsInitialized => this.Key != Guid.Empty;

	/// <inheritdoc />
	public Guid Key { get; private set; }

	/// <inheritdoc />
	public int Count => this.pins.Count;

	/// <inheritdoc />
	public Pinned<CachedChunk<TChunk>> this[int index] => this.pins[index].AsPinned;

	public void AddMany(IReadOnlyList<Pinned<CachedChunk<TChunk>>> chunkPins)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(chunkPins != null);

		int count = chunkPins.Count;
		for (int i = 0; i < count; i++)
		{
			this.Add(chunkPins[i]);
		}
	}

	public void Add(Pinned<CachedChunk<TChunk>> chunkPin)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(chunkPin.IsPinned);

		if (this.pinLocations.Add(chunkPin.Value.Key))
		{
			this.pins.Add(chunkPin.CreatePin());
		}
	}

	public void Add(Pin<CachedChunk<TChunk>> chunkPin)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(chunkPin.IsPinned);

		if (this.pinLocations.Add(chunkPin.Value.Key))
		{
			this.pins.Add(chunkPin);
		}
	}

	public void Initialize(Guid chunkGridKey)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(chunkGridKey != Guid.Empty);

		this.Key = chunkGridKey;
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		this.Key = Guid.Empty;
		this.pins.DisposeAllAndClear();
		this.pinLocations.Clear();
	}

	/// <inheritdoc />
	public IEnumerator<Pinned<CachedChunk<TChunk>>> GetEnumerator()
	{
		int count = this.pins.Count;
		for (int i = 0; i < count; i++)
		{
			yield return this.pins[i].AsPinned;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
