﻿namespace HQS.VUE.Core;

internal class ChunkGridChangeset<TChunk, TPin> : IKeyed<Guid>, IDeinitializable
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
{
	private readonly PinList<CachedChunk<TChunk>> cachedChunkPins = new PinList<CachedChunk<TChunk>>();

	private readonly OrderedDictionary<Int3, ChunkChangesetPair<TChunk, TPin>.Pin> changesets;

	private readonly OrderedDictionary<Int3, Pin<IVersionedChunk<TChunk>>> committed;

	public ChunkGridChangeset()
	{
		var comparer = MortonCurve.EqualityComparer.ForInt3;
		this.changesets = new OrderedDictionary<Int3, ChunkChangesetPair<TChunk, TPin>.Pin>(comparer);
		this.committed = new OrderedDictionary<Int3, Pin<IVersionedChunk<TChunk>>>(comparer);

		this.Changesets = ReadOnlyOrderedDictionary.Convert(this.changesets, x => x.Data.Changeset);
		this.Committed = ReadOnlyOrderedDictionary.Convert(this.committed, x => x.AsPinned);
	}

	public bool IsInitialized => this.Key != Guid.Empty;

	/// <inheritdoc />
	public Guid Key { get; private set; }

	public long Version { get; private set; }

	public IReadOnlyOrderedDictionary<Int3, ChunkChangeset<TChunk, TPin>> Changesets { get; }

	public IReadOnlyOrderedDictionary<Int3, Pinned<IVersionedChunk<TChunk>>> Committed { get; }

	public bool AreChangesCommitted { get; private set; }

	public void CommitChanges()
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.AreChangesCommitted);

		this.AreChangesCommitted = true;
		int count = this.changesets.Count;
		for (int i = 0; i < count; i++)
		{
			var pair = this.changesets[i].Value.Data;
			var cachedChunk = pair.CachedChunk.Value;

			cachedChunk.UpdateData(this.Version, pair.Changeset.Next);

			var pin = cachedChunk.Data.CreatePin();
			this.committed.Add(cachedChunk.Key, pin);
		}
	}

	public void AddCachedChunksTo(CachedChunkDictionary<TChunk> dictionary)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(dictionary != null);
		Debug.Assert(dictionary.IsInitialized);
		Debug.Assert(dictionary.Key == this.Key);

		dictionary.AddMany(this.cachedChunkPins);
	}

	public void Initialize(
		Guid chunkGridKey,
		long chunkGridVersion,
		IReadOnlyList<ChunkChangesetPair<TChunk, TPin>?> changesetPairs,
		IReadOnlyList<Pinned<CachedChunk<TChunk>>> cachedChunks)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(chunkGridKey != Guid.Empty);
		Debug.Assert(chunkGridVersion >= 0);
		Debug.Assert(changesetPairs != null);
		Debug.Assert(cachedChunks != null);

		this.Key = chunkGridKey;
		this.Version = chunkGridVersion;

		int count = changesetPairs.Count;
		for (int i = 0; i < count; i++)
		{
			var pair = changesetPairs[i];
			if (pair.HasValue)
			{
				var pin = pair.Value.CreatePin();
				this.changesets.Add(pin.Data.CachedChunk.Value.Key, pin);
			}
		}

		this.cachedChunkPins.AddMany(cachedChunks);
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		this.Key = default;
		this.Version = -1;
		this.AreChangesCommitted = false;
		this.cachedChunkPins.DisposeAllAndClear();
		this.changesets.DisposeAllAndClear();
		this.committed.DisposeAllAndClear();
	}
}
