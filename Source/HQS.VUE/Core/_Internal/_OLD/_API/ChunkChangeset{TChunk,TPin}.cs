﻿namespace HQS.VUE.Core;

internal readonly struct ChunkChangeset<TChunk, TPin> :
	IPinnable<ChunkChangeset<TChunk, TPin>, ChunkChangeset<TChunk, TPin>.Pin>
	where TChunk : IPinnable<TChunk, TPin>, IEquatable<TChunk>
	where TPin : IPin<TChunk, TPin>
{
	private readonly TPin previous;

	private readonly TPin next;

	private ChunkChangeset(TPin previous, TPin next)
	{
		this.previous = previous;
		this.next = next;
	}

	/// <inheritdoc />
	public bool HasData => this.previous.Data.HasData;

	public TChunk Previous => this.previous.Data;

	public TChunk Next => this.next.Data;

	/// <inheritdoc />
	public Pin CreatePin()
	{
		Debug.Assert(this.HasData);

		return new Pin(this.previous.CreatePin(), this.next.CreatePin());
	}

	public readonly struct Pin : IPin<ChunkChangeset<TChunk, TPin>, Pin>
	{
		public Pin(TPin previous, TPin next)
		{
			Debug.Assert(previous.HasData);
			Debug.Assert(next.HasData);
			Debug.Assert(!previous.Data.Equals(next.Data));

			this.Data = new ChunkChangeset<TChunk, TPin>(previous, next);
		}

		/// <inheritdoc />
		public ChunkChangeset<TChunk, TPin> Data { get; }

		/// <inheritdoc />
		public bool HasData => this.Data.HasData;

		/// <inheritdoc />
		public Pin CreatePin() => this.Data.CreatePin();

		/// <inheritdoc />
		public void Dispose()
		{
			this.Data.previous.Dispose();
			this.Data.next.Dispose();
		}
	}
}
