﻿namespace HQS.VUE.Core;

internal interface IChunkConfigDependent<TConfig>
	where TConfig : IEquatable<TConfig>
{
	TConfig ChunkConfig { get; }
}
