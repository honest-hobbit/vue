﻿namespace HQS.VUE.Assets.Coplanar;

internal class VoxelPaletteFactory : AbstractAssetFactory
{
	private readonly AssetWriterTracker<VoxelPaletteWriter> tracker;

	public VoxelPaletteFactory(AssetWriterTracker<VoxelPaletteWriter> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public IVoxelPaletteWriter CreateNew()
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

		var data = VoxelPaletteData.Empty;
		data.Asset = AssetData.ForNewAsset(this.CurrentTime);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(data);
		return writer;
	}

	public IVoxelPaletteWriter LoadFrom(IAssetStore store, Guid key)
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
		Ensure.That(store, nameof(store)).IsNotNull();

		// TODO need to deserialize data
		////var data = store.GetAsset(key);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(VoxelPaletteData.Empty);

		return writer;
	}
}
