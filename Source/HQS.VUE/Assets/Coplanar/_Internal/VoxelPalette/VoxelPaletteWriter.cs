﻿namespace HQS.VUE.Assets.Coplanar;

internal class VoxelPaletteWriter : AbstractAssetWriter<
	VoxelPaletteData, IVoxelPalette, VoxelPalette, IVoxelPaletteWriter, VoxelPaletteWriter>,
	IVoxelPaletteWriter
{
	public VoxelPaletteWriter(IPinPool<VoxelPalette> pool)
		: base(pool)
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public IPalette<ContouringRule> ContouringRules => throw new NotImplementedException();

	/// <inheritdoc />
	public IPalette<VoxelType> VoxelTypes => throw new NotImplementedException();

	/// <inheritdoc />
	IReadOnlyList<ContouringRule> IVoxelPaletteBase.ContouringRules => this.ContouringRules;

	/// <inheritdoc />
	IReadOnlyList<VoxelType> IVoxelPaletteBase.VoxelTypes => this.VoxelTypes;

	/// <inheritdoc />
	protected override void SetData(VoxelPaletteData data)
	{
		this.InitializeBase(data.Asset, data.Store);
	}

	/// <inheritdoc />
	protected override VoxelPaletteData GetData()
	{
		return new VoxelPaletteData()
		{
			Asset = this.GetAssetData(),
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
	}
}
