﻿namespace HQS.VUE.Assets.Coplanar;

internal struct VoxelPaletteData
{
	public AssetData Asset;

	public IAssetStore Store;

	public static VoxelPaletteData Empty => new VoxelPaletteData()
	{
		Asset = AssetData.Empty,
		Store = null,
	};
}
