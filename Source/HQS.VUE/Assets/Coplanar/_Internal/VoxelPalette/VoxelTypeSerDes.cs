﻿namespace HQS.VUE.Assets.Coplanar;

internal class VoxelTypeSerDes : CompositeSerDes<byte, ushort, Bit8, VoxelType>
{
	private VoxelTypeSerDes()
		: base(SerDes.OfByte, VarintSerDes.ForUnsigned, BitSerDes.ForBit8)
	{
	}

	public static ISerDes<VoxelType> Instance { get; } = new VoxelTypeSerDes();

	/// <inheritdoc />
	protected override VoxelType ComposeValue(
		byte contouringRuleIndex, ushort surfaceTypeIndex, Bit8 bools) =>
		new VoxelType(contouringRuleIndex, surfaceTypeIndex, bools);

	/// <inheritdoc />
	protected override void DecomposeValue(
		VoxelType value,
		out byte contouringRuleIndex,
		out ushort surfaceTypeIndex,
		out Bit8 bools)
	{
		contouringRuleIndex = value.ContouringRuleIndex;
		surfaceTypeIndex = value.SurfaceTypeIndex;
		bools = value.Bools;
	}
}
