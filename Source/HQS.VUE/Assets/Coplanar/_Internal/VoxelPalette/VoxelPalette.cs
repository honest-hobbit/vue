﻿namespace HQS.VUE.Assets.Coplanar;

internal class VoxelPalette : AbstractAsset<
	VoxelPaletteData, IVoxelPalette, VoxelPalette, IVoxelPaletteWriter, VoxelPaletteWriter>,
	IVoxelPalette
{
	public VoxelPalette()
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public IReadOnlyList<ContouringRule> ContouringRules => throw new NotImplementedException();

	/// <inheritdoc />
	public IReadOnlyList<VoxelType> VoxelTypes => throw new NotImplementedException();

	/// <inheritdoc />
	protected override VoxelPalette Self => this;

	/// <inheritdoc />
	public override void InitializeFrom(VoxelPaletteData data)
	{
		this.InitializeBase(data.Asset, data.Store);
	}

	/// <inheritdoc />
	public override VoxelPaletteData GetData()
	{
		return new VoxelPaletteData()
		{
			Asset = this.GetAssetData(),
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
	}
}
