﻿namespace HQS.VUE.Assets.Coplanar;

internal class VoxelPaletteAssets : IVoxelPaletteAssets
{
	private readonly VoxelPaletteFactory assetFactory;

	public VoxelPaletteAssets(
		VoxelPaletteFactory assetFactory,
		ICommittedAssets<IVoxelPalette> commitedAssets)
	{
		Debug.Assert(assetFactory != null);
		Debug.Assert(commitedAssets != null);

		this.assetFactory = assetFactory;
		this.Committed = commitedAssets.Committed;
	}

	/// <inheritdoc />
	public Type CollectionType => typeof(IVoxelPaletteAssets);

	/// <inheritdoc />
	public Type AssetType => typeof(IVoxelPalette);

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, IVoxelPalette> Committed { get; }

	/// <inheritdoc />
	public IVoxelPaletteWriter CreateNew() => this.assetFactory.CreateNew();

	/// <inheritdoc />
	public IVoxelPaletteWriter LoadFrom(IAssetStore store, Guid key) =>
		this.assetFactory.LoadFrom(store, key);
}
