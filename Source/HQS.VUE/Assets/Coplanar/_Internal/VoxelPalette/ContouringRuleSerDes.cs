﻿namespace HQS.VUE.Assets.Coplanar;

internal class ContouringRuleSerDes : CompositeSerDes<string, Bit64, ContouringRule>
{
	private ContouringRuleSerDes()
		: base(StringSerDes.LengthAsVarint, BitSerDes.ForBit64)
	{
	}

	public static ISerDes<ContouringRule> Instance { get; } = new ContouringRuleSerDes();

	/// <inheritdoc />
	protected override ContouringRule ComposeValue(string displayName, Bit64 presentSurfaces) =>
		new ContouringRule(displayName, presentSurfaces);

	/// <inheritdoc />
	protected override void DecomposeValue(
		ContouringRule value, out string displayName, out Bit64 presentSurfaces)
	{
		displayName = value.DisplayName;
		presentSurfaces = value.PresentSurfaces;
	}
}
