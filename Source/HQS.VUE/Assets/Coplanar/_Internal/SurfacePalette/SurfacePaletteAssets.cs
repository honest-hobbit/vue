﻿namespace HQS.VUE.Assets.Coplanar;

internal class SurfacePaletteAssets : ISurfacePaletteAssets
{
	private readonly SurfacePaletteFactory assetFactory;

	public SurfacePaletteAssets(
		SurfacePaletteFactory assetFactory,
		ICommittedAssets<ISurfacePalette> commitedAssets)
	{
		Debug.Assert(assetFactory != null);
		Debug.Assert(commitedAssets != null);

		this.assetFactory = assetFactory;
		this.Committed = commitedAssets.Committed;
	}

	/// <inheritdoc />
	public Type CollectionType => typeof(ISurfacePaletteAssets);

	/// <inheritdoc />
	public Type AssetType => typeof(ISurfacePalette);

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, ISurfacePalette> Committed { get; }

	/// <inheritdoc />
	public ISurfacePaletteWriter CreateNew() => this.assetFactory.CreateNew();

	/// <inheritdoc />
	public ISurfacePaletteWriter LoadFrom(IAssetStore store, Guid key) =>
		this.assetFactory.LoadFrom(store, key);
}
