﻿namespace HQS.VUE.Assets.Coplanar;

internal class SurfaceTypeSerDes : CompositeSerDes<ushort, ushort, SurfaceType>
{
	private SurfaceTypeSerDes()
		: base(VarintSerDes.ForUnsigned, VarintSerDes.ForUnsigned)
	{
	}

	public static ISerDes<SurfaceType> Instance { get; } = new SurfaceTypeSerDes();

	/// <inheritdoc />
	protected override SurfaceType ComposeValue(ushort meshTypeIndex, ushort paletteIndex) =>
		new SurfaceType(meshTypeIndex, paletteIndex);

	/// <inheritdoc />
	protected override void DecomposeValue(
		SurfaceType value, out ushort meshTypeIndex, out ushort paletteIndex)
	{
		meshTypeIndex = value.MeshTypeIndex;
		paletteIndex = value.PaletteIndex;
	}
}
