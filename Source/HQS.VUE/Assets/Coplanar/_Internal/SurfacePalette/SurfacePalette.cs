﻿namespace HQS.VUE.Assets.Coplanar;

internal class SurfacePalette : AbstractAsset<
	SurfacePaletteData, ISurfacePalette, SurfacePalette, ISurfacePaletteWriter, SurfacePaletteWriter>,
	ISurfacePalette
{
	public SurfacePalette()
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public IReadOnlyList<MeshType> MeshTypes => throw new NotImplementedException();

	/// <inheritdoc />
	public IReadOnlyList<SurfaceType> SurfaceTypes => throw new NotImplementedException();

	/// <inheritdoc />
	protected override SurfacePalette Self => this;

	/// <inheritdoc />
	public override void InitializeFrom(SurfacePaletteData data)
	{
		this.InitializeBase(data.Asset, data.Store);
	}

	/// <inheritdoc />
	public override SurfacePaletteData GetData()
	{
		return new SurfacePaletteData()
		{
			Asset = this.GetAssetData(),
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
	}
}
