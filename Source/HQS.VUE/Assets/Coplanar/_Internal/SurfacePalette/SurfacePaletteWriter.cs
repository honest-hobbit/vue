﻿namespace HQS.VUE.Assets.Coplanar;

internal class SurfacePaletteWriter : AbstractAssetWriter<
	SurfacePaletteData, ISurfacePalette, SurfacePalette, ISurfacePaletteWriter, SurfacePaletteWriter>,
	ISurfacePaletteWriter
{
	public SurfacePaletteWriter(IPinPool<SurfacePalette> pool)
		: base(pool)
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public IPalette<MeshType> MeshTypes => throw new NotImplementedException();

	/// <inheritdoc />
	public IPalette<SurfaceType> SurfaceTypes => throw new NotImplementedException();

	/// <inheritdoc />
	IReadOnlyList<MeshType> ISurfacePaletteBase.MeshTypes => this.MeshTypes;

	/// <inheritdoc />
	IReadOnlyList<SurfaceType> ISurfacePaletteBase.SurfaceTypes => this.SurfaceTypes;

	/// <inheritdoc />
	protected override void SetData(SurfacePaletteData data)
	{
		this.InitializeBase(data.Asset, data.Store);
	}

	/// <inheritdoc />
	protected override SurfacePaletteData GetData()
	{
		return new SurfacePaletteData()
		{
			Asset = this.GetAssetData(),
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
	}
}
