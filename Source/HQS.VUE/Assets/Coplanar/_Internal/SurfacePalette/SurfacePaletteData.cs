﻿namespace HQS.VUE.Assets.Coplanar;

internal struct SurfacePaletteData
{
	public AssetData Asset;

	public IAssetStore Store;

	public static SurfacePaletteData Empty => new SurfacePaletteData()
	{
		Asset = AssetData.Empty,
		Store = null,
	};
}
