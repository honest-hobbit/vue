﻿namespace HQS.VUE.Assets.Coplanar;

internal class MeshTypeSerDes : CompositeSerDes<Guid, string, MeshType>
{
	private MeshTypeSerDes()
		: base(SerDes.OfGuid, StringSerDes.LengthAsVarint)
	{
	}

	public static ISerDes<MeshType> Instance { get; } = new MeshTypeSerDes();

	/// <inheritdoc />
	protected override MeshType ComposeValue(Guid key, string displayName) =>
		new MeshType(key, displayName);

	/// <inheritdoc />
	protected override void DecomposeValue(MeshType value, out Guid key, out string displayName)
	{
		key = value.Key;
		displayName = value.DisplayName;
	}
}
