﻿namespace HQS.VUE.Assets.Coplanar;

internal class SurfacePaletteFactory : AbstractAssetFactory
{
	private readonly AssetWriterTracker<SurfacePaletteWriter> tracker;

	public SurfacePaletteFactory(AssetWriterTracker<SurfacePaletteWriter> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public ISurfacePaletteWriter CreateNew()
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

		var data = SurfacePaletteData.Empty;
		data.Asset = AssetData.ForNewAsset(this.CurrentTime);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(data);
		return writer;
	}

	public ISurfacePaletteWriter LoadFrom(IAssetStore store, Guid key)
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
		Ensure.That(store, nameof(store)).IsNotNull();

		// TODO need to deserialize data
		////var data = store.GetAsset(key);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(SurfacePaletteData.Empty);

		return writer;
	}
}
