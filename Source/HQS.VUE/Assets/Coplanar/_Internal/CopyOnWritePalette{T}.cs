﻿namespace HQS.VUE.Assets.Coplanar;

// TODO should the isShared status be public?
internal class CopyOnWritePalette<T> : IPalette<T>
	where T : struct, IEquatable<T>
{
	private T[] array = Array.Empty<T>();

	private bool isShared = false;

	public CopyOnWritePalette(int maxCapacity)
	{
		Debug.Assert(maxCapacity >= 0);

		this.MaxCapacity = maxCapacity;
	}

	/// <inheritdoc />
	public int MaxCapacity { get; private set; }

	/// <inheritdoc />
	public int Capacity
	{
		get => this.array.Length;
		set
		{
			Ensure.That(value, nameof(this.Capacity)).IsInRange(0, this.MaxCapacity);

			if (this.array.Length == value)
			{
				return;
			}

			if (value == 0)
			{
				this.array = Array.Empty<T>();
				this.isShared = false;
				return;
			}

			var newArray = new T[value];
			Array.Copy(this.array, newArray, Math.Min(this.array.Length, newArray.Length));
			this.array = newArray;
			this.isShared = false;
		}
	}

	/// <inheritdoc />
	public int Count => this.array.Length;

	/// <inheritdoc />
	public T this[int index]
	{
		get => this.array[index];
		set
		{
			if (this.isShared)
			{
				if (value.Equals(this.array[index]))
				{
					return;
				}

				this.array = this.array.Copy();
				this.isShared = false;
			}

			this.array[index] = value;
		}
	}

	/// <inheritdoc />
	T IReadOnlyList<T>.this[int index] => this[index];

	public void InitializeAsCopyOf(CopyOnWritePalette<T> source)
	{
		Debug.Assert(source != null);

		this.array = source.array;
		this.isShared = source.array.Length > 0;
		this.MaxCapacity = source.MaxCapacity;
	}

	/// <inheritdoc />
	public IEnumerator<T> GetEnumerator() => this.array.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
