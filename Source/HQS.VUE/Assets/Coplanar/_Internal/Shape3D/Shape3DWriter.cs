﻿namespace HQS.VUE.Assets.Coplanar;

internal class Shape3DWriter : AbstractAssetWriter<
	Shape3DData, IShape3D, Shape3D, IShape3DWriter, Shape3DWriter>,
	IShape3DWriter
{
	public Shape3DWriter(IPinPool<Shape3D> pool)
		: base(pool)
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public Guid GridKey { get; set; }

	/// <inheritdoc />
	public IGrid3D Grid => throw new NotImplementedException();

	/// <inheritdoc />
	public Guid MapKey { get; set; }

	/// <inheritdoc />
	public IVoxelPalette Map => throw new NotImplementedException();

	/// <inheritdoc />
	public bool AutoContour { get; set; }

	/// <inheritdoc />
	public bool AutoCollisionCheck { get; set; }

	/// <inheritdoc />
	public bool ToBeContoured { get; set; }

	/// <inheritdoc />
	public bool ToBeCollisionChecked { get; set; }

	/// <inheritdoc />
	protected override void SetData(Shape3DData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.GridKey = data.GridKey;
		this.MapKey = data.MapKey;
		this.AutoContour = data.AutoContour;
		this.AutoCollisionCheck = data.AutoCollisionCheck;

		this.ToBeContoured = data.AutoContour;
		this.ToBeCollisionChecked = data.AutoCollisionCheck;
	}

	/// <inheritdoc />
	protected override Shape3DData GetData()
	{
		return new Shape3DData()
		{
			Asset = this.GetAssetData(),
			GridKey = this.GridKey,
			MapKey = this.MapKey,
			AutoContour = this.AutoContour,
			AutoCollisionCheck = this.AutoCollisionCheck,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.GridKey = Guid.Empty;
		this.MapKey = Guid.Empty;
		this.AutoContour = false;
		this.AutoCollisionCheck = false;

		this.ToBeContoured = false;
		this.ToBeCollisionChecked = false;
	}
}
