﻿namespace HQS.VUE.Assets.Coplanar;

internal class Shape3DFactory : AbstractAssetFactory
{
	private readonly AssetWriterTracker<Shape3DWriter> tracker;

	public Shape3DFactory(AssetWriterTracker<Shape3DWriter> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public IShape3DWriter CreateNew()
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

		var data = Shape3DData.Empty;
		data.Asset = AssetData.ForNewAsset(this.CurrentTime);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(data);
		return writer;
	}

	public IShape3DWriter LoadFrom(IAssetStore store, Guid key)
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
		Ensure.That(store, nameof(store)).IsNotNull();

		// TODO need to deserialize data
		////var data = store.GetAsset(key);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(Shape3DData.Empty);

		return writer;
	}
}
