﻿namespace HQS.VUE.Assets.Coplanar;

internal struct Shape3DData
{
	public AssetData Asset;

	public Guid GridKey;

	public Guid MapKey;

	public bool AutoContour;

	public bool AutoCollisionCheck;

	public IAssetStore Store;

	public static Shape3DData Empty => new Shape3DData()
	{
		Asset = AssetData.Empty,
		GridKey = Guid.Empty,
		MapKey = Guid.Empty,
		AutoContour = false,
		AutoCollisionCheck = false,
		Store = null,
	};
}
