﻿namespace HQS.VUE.Assets.Coplanar;

internal class Shape3DAssets : IShape3DAssets
{
	private readonly Shape3DFactory assetFactory;

	public Shape3DAssets(
		Shape3DFactory assetFactory,
		ICommittedAssets<IShape3D> commitedAssets)
	{
		Debug.Assert(assetFactory != null);
		Debug.Assert(commitedAssets != null);

		this.assetFactory = assetFactory;
		this.Committed = commitedAssets.Committed;
	}

	/// <inheritdoc />
	public Type CollectionType => typeof(IShape3DAssets);

	/// <inheritdoc />
	public Type AssetType => typeof(IShape3D);

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, IShape3D> Committed { get; }

	/// <inheritdoc />
	public IShape3DWriter CreateNew() => this.assetFactory.CreateNew();

	/// <inheritdoc />
	public IShape3DWriter LoadFrom(IAssetStore store, Guid key) =>
		this.assetFactory.LoadFrom(store, key);
}
