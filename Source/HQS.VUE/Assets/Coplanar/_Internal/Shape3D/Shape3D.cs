﻿namespace HQS.VUE.Assets.Coplanar;

internal class Shape3D : AbstractAsset<
	Shape3DData, IShape3D, Shape3D, IShape3DWriter, Shape3DWriter>,
	IShape3D
{
	public Shape3D()
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public Guid GridKey { get; private set; }

	/// <inheritdoc />
	public IGrid3D Grid => throw new NotImplementedException();

	/// <inheritdoc />
	public Guid MapKey { get; private set; }

	/// <inheritdoc />
	public IVoxelPalette Map => throw new NotImplementedException();

	/// <inheritdoc />
	public bool AutoContour { get; private set; }

	/// <inheritdoc />
	public bool AutoCollisionCheck { get; private set; }

	/// <inheritdoc />
	protected override Shape3D Self => this;

	/// <inheritdoc />
	public override void InitializeFrom(Shape3DData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.GridKey = data.GridKey;
		this.MapKey = data.MapKey;
		this.AutoContour = data.AutoContour;
		this.AutoCollisionCheck = data.AutoCollisionCheck;
	}

	/// <inheritdoc />
	public override Shape3DData GetData()
	{
		return new Shape3DData()
		{
			Asset = this.GetAssetData(),
			GridKey = this.GridKey,
			MapKey = this.MapKey,
			AutoContour = this.AutoContour,
			AutoCollisionCheck = this.AutoCollisionCheck,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.GridKey = Guid.Empty;
		this.MapKey = Guid.Empty;
		this.AutoContour = false;
		this.AutoCollisionCheck = false;
	}
}
