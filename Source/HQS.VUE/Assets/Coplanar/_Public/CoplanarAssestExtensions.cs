﻿namespace HQS.VUE.Assets.Coplanar;

public static class CoplanarAssestExtensions
{
	public static IShape3DAssets GetShapes3D(this IJobAssets resources)
	{
		Ensure.That(resources, nameof(resources)).IsNotNull();

		return resources.Get<IShape3DAssets>();
	}

	public static IVoxelPaletteAssets GetVoxelPalettes(this IJobAssets resources)
	{
		Ensure.That(resources, nameof(resources)).IsNotNull();

		return resources.Get<IVoxelPaletteAssets>();
	}

	public static ISurfacePaletteAssets GetSurfacePalettes(this IJobAssets resources)
	{
		Ensure.That(resources, nameof(resources)).IsNotNull();

		return resources.Get<ISurfacePaletteAssets>();
	}

	public static void RegisterCoplanerAssets(this VoxelUniverseBuilder builder)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		builder.AddRegistrations(container =>
		{
			container.RegisterAllPools(CoplanarPooledTypes.Enumerate());
			container.RegisterAllCommandsFromAssembly<INamespaceCoplanar>(
				typeof(CoplanarAssestExtensions).Assembly);

			container.Register<Shape3D>();
			container.Register<Shape3DWriter>();
			container.RegisterAssetFactory<Shape3DFactory>();
			container.RegisterAssetCollection<IShape3DAssets, Shape3DAssets>();

			container.Register<VoxelPalette>();
			container.Register<VoxelPaletteWriter>();
			container.RegisterAssetFactory<VoxelPaletteFactory>();
			container.RegisterAssetCollection<IVoxelPaletteAssets, VoxelPaletteAssets>();

			container.Register<SurfacePalette>();
			container.Register<SurfacePaletteWriter>();
			container.RegisterAssetFactory<SurfacePaletteFactory>();
			container.RegisterAssetCollection<ISurfacePaletteAssets, SurfacePaletteAssets>();
		});
	}
}
