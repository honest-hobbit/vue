﻿namespace HQS.VUE.Assets.Coplanar;

public interface ISurfacePalette :
	ISurfacePaletteBase, IChangeableAsset<ISurfacePaletteWriter>
{
}
