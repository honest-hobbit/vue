﻿namespace HQS.VUE.Assets.Coplanar;

// TODO maybe these belong entirely on the host application side?
public interface ISurfacePaletteAssets : IAssetCollection
{
	IReadOnlyDictionary<Guid, ISurfacePalette> Committed { get; }

	ISurfacePaletteWriter CreateNew();

	ISurfacePaletteWriter LoadFrom(IAssetStore store, Guid key);
}
