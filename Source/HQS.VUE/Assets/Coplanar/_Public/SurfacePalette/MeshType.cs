﻿namespace HQS.VUE.Assets.Coplanar;

public readonly struct MeshType : IEquatable<MeshType>, IKeyed<Guid>
{
	public MeshType(Guid key, string displayName)
	{
		this.Key = key;
		this.DisplayName = displayName;
	}

	// this Guid is used to look up the Material in the host application side
	/// <inheritdoc />
	public Guid Key { get; }

	public string DisplayName { get; }

	public static bool operator ==(MeshType lhs, MeshType rhs) => lhs.Equals(rhs);

	public static bool operator !=(MeshType lhs, MeshType rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(MeshType other) =>
		this.Key == other.Key && this.DisplayName == other.DisplayName;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Key, this.DisplayName);

	/// <inheritdoc />
	public override string ToString() => $"[{this.Key}, {this.DisplayName}]";
}
