﻿namespace HQS.VUE.Assets.Coplanar;

public interface ISurfacePaletteBase : IAsset
{
	IReadOnlyList<MeshType> MeshTypes { get; }

	IReadOnlyList<SurfaceType> SurfaceTypes { get; }
}
