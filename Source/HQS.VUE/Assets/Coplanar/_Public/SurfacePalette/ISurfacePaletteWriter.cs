﻿namespace HQS.VUE.Assets.Coplanar;

public interface ISurfacePaletteWriter : ISurfacePaletteBase, IAssetWriter<ISurfacePalette>
{
	new IPalette<MeshType> MeshTypes { get; }

	new IPalette<SurfaceType> SurfaceTypes { get; }
}
