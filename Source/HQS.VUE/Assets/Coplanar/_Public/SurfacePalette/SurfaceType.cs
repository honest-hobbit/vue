﻿namespace HQS.VUE.Assets.Coplanar;

public readonly struct SurfaceType : IEquatable<SurfaceType>
{
	public SurfaceType(ushort meshTypeIndex, ushort paletteIndex)
	{
		this.MeshTypeIndex = meshTypeIndex;
		this.PaletteIndex = paletteIndex;
	}

	public ushort MeshTypeIndex { get; }

	// TODO is this still going to be a thing? Should it have a new name?
	// this is an optional index into a palette that's unique to each Material instance
	public ushort PaletteIndex { get; }

	public static bool operator ==(SurfaceType lhs, SurfaceType rhs) => lhs.Equals(rhs);

	public static bool operator !=(SurfaceType lhs, SurfaceType rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(SurfaceType other) =>
		this.MeshTypeIndex == other.MeshTypeIndex && this.PaletteIndex == other.PaletteIndex;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.MeshTypeIndex, this.PaletteIndex);

	/// <inheritdoc />
	public override string ToString() => $"[{this.MeshTypeIndex}, {this.PaletteIndex}]";
}
