﻿namespace HQS.VUE.Assets.Coplanar;

public static class PaletteConstants
{
	public const int ContouringRulesMaxCapacity = 64;

	public const int VoxelTypesMaxCapacity = ushort.MaxValue + 1;

	public const int MeshTypesMaxCapacity = ushort.MaxValue + 1;

	public const int SurfaceTypesMaxCapacity = ushort.MaxValue + 1;

	public const int PaletteMaxCapacity = ushort.MaxValue + 1;
}
