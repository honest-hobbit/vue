﻿namespace HQS.VUE.Assets.Coplanar;

public interface IVoxelPaletteWriter : IVoxelPaletteBase, IAssetWriter<IVoxelPalette>
{
	new IPalette<ContouringRule> ContouringRules { get; }

	new IPalette<VoxelType> VoxelTypes { get; }
}
