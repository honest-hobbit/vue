﻿namespace HQS.VUE.Assets.Coplanar;

public interface IVoxelPaletteAssets : IAssetCollection
{
	IReadOnlyDictionary<Guid, IVoxelPalette> Committed { get; }

	IVoxelPaletteWriter CreateNew();

	IVoxelPaletteWriter LoadFrom(IAssetStore store, Guid key);
}
