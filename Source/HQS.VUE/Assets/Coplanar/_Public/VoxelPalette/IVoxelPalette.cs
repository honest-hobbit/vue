﻿namespace HQS.VUE.Assets.Coplanar;

public interface IVoxelPalette :
	IVoxelPaletteBase, IChangeableAsset<IVoxelPaletteWriter>
{
}
