﻿namespace HQS.VUE.Assets.Coplanar;

public readonly struct ContouringRule : IEquatable<ContouringRule>
{
	public ContouringRule(string displayName)
	{
		this.DisplayName = displayName;
		this.PresentSurfaces = default;
	}

	internal ContouringRule(string displayName, Bit64 presentSurfaces)
	{
		this.DisplayName = displayName;
		this.PresentSurfaces = presentSurfaces;
	}

	public string DisplayName { get; }

	internal Bit64 PresentSurfaces { get; }

	public bool IsSurfacePresent(byte contouringRuleIndex)
	{
		Ensure.That<int>(contouringRuleIndex, nameof(contouringRuleIndex))
			.IsLt(PaletteConstants.ContouringRulesMaxCapacity);

		return this.PresentSurfaces[contouringRuleIndex];
	}

	public ContouringRule WithDisplayName(string displayName) =>
		new ContouringRule(displayName, this.PresentSurfaces);

	public ContouringRule WithIsSurfacePresent(byte contouringRuleIndex, bool isSurfacePresent)
	{
		Ensure.That<int>(contouringRuleIndex, nameof(contouringRuleIndex))
			.IsLt(PaletteConstants.ContouringRulesMaxCapacity);

		var presentSurfaces = this.PresentSurfaces;
		presentSurfaces[contouringRuleIndex] = isSurfacePresent;
		return new ContouringRule(this.DisplayName, presentSurfaces);
	}

	public static bool operator ==(ContouringRule lhs, ContouringRule rhs) => lhs.Equals(rhs);

	public static bool operator !=(ContouringRule lhs, ContouringRule rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ContouringRule other) =>
		this.PresentSurfaces == other.PresentSurfaces && this.DisplayName == other.DisplayName;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.DisplayName, this.PresentSurfaces);

	/// <inheritdoc />
	public override string ToString() => this.DisplayName;
}
