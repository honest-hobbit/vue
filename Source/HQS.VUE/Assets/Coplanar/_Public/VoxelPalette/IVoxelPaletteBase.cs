﻿namespace HQS.VUE.Assets.Coplanar;

public interface IVoxelPaletteBase : IAsset
{
	IReadOnlyList<ContouringRule> ContouringRules { get; }

	IReadOnlyList<VoxelType> VoxelTypes { get; }
}
