﻿namespace HQS.VUE.Assets.Coplanar;

public readonly struct VoxelType : IEquatable<VoxelType>
{
	private const int OptimizeSurfaceBit = 0;

	private const int CreateInternalSurfacesBit = 1;

	public VoxelType(byte contouringRuleIndex, ushort surfaceTypeIndex)
		: this(contouringRuleIndex, surfaceTypeIndex, true, false)
	{
	}

	public VoxelType(
		byte contouringRuleIndex, ushort surfaceTypeIndex, bool optimizeSurface, bool createInternalSurfaces)
	{
		this.ContouringRuleIndex = contouringRuleIndex;
		this.SurfaceTypeIndex = surfaceTypeIndex;

		var bools = new Bit8(0);
		bools[OptimizeSurfaceBit] = optimizeSurface;
		bools[CreateInternalSurfacesBit] = createInternalSurfaces;
		this.Bools = bools;
	}

	internal VoxelType(byte contouringRuleIndex, ushort surfaceTypeIndex, Bit8 bools)
	{
		this.ContouringRuleIndex = contouringRuleIndex;
		this.SurfaceTypeIndex = surfaceTypeIndex;
		this.Bools = bools;
	}

	public byte ContouringRuleIndex { get; }

	public ushort SurfaceTypeIndex { get; }

	public bool OptimizeSurface => this.Bools[OptimizeSurfaceBit];

	public bool CreateInternalSurfaces => this.Bools[CreateInternalSurfacesBit];

	internal Bit8 Bools { get; }

	public static bool operator ==(VoxelType lhs, VoxelType rhs) => lhs.Equals(rhs);

	public static bool operator !=(VoxelType lhs, VoxelType rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(VoxelType other) =>
		this.SurfaceTypeIndex == other.SurfaceTypeIndex &&
		this.ContouringRuleIndex == other.ContouringRuleIndex &&
		this.OptimizeSurface == other.OptimizeSurface &&
		this.CreateInternalSurfaces == other.CreateInternalSurfaces;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.ContouringRuleIndex, this.SurfaceTypeIndex);

	/// <inheritdoc />
	public override string ToString() =>
		$"[{this.ContouringRuleIndex}, {this.SurfaceTypeIndex}, {this.OptimizeSurface}, {this.CreateInternalSurfaces}]";
}
