﻿namespace HQS.VUE.Assets.Coplanar;

public readonly struct CubeVoxel : IEquatable<CubeVoxel>
{
	public CubeVoxel(ushort typeIndex)
	{
		this.TypeIndex = typeIndex;
	}

	public static CubeVoxel Empty => default;

	public ushort TypeIndex { get; }

	// this is slower :(
	////public bool IsEmpty => this.TypeIndex == 0;

	public static bool operator ==(CubeVoxel lhs, CubeVoxel rhs) => lhs.Equals(rhs);

	public static bool operator !=(CubeVoxel lhs, CubeVoxel rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(CubeVoxel other) => this.TypeIndex == other.TypeIndex;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.TypeIndex.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.TypeIndex.ToString();
}
