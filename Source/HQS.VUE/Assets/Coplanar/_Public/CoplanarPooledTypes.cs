﻿namespace HQS.VUE.Assets.Coplanar;

public static class CoplanarPooledTypes
{
	public static PooledType Shape3D { get; } = new PooledType(
		new Guid("2c54e10a-2eb0-45b5-bd0b-1d5c22d9f690"),
		CreateName(nameof(Shape3D)),
		typeof(Shape3D));

	public static PooledType Shape3DWriter { get; } = new PooledType(
		new Guid("a12fc894-2ffe-488c-bbcb-8c23cec71748"),
		CreateName(nameof(Shape3DWriter)),
		typeof(Shape3DWriter));

	public static PooledType VoxelPalette { get; } = new PooledType(
		new Guid("13433cc3-1471-4883-9f6b-c1d8ab638894"),
		CreateName(nameof(VoxelPalette)),
		typeof(VoxelPalette));

	public static PooledType VoxelPaletteWriter { get; } = new PooledType(
		new Guid("d5c601ac-72b4-489f-89ec-4d603ce2220d"),
		CreateName(nameof(VoxelPaletteWriter)),
		typeof(VoxelPaletteWriter));

	public static PooledType SurfacePalette { get; } = new PooledType(
		new Guid("7c18f18a-462a-4fff-a9be-63aa85a16580"),
		CreateName(nameof(SurfacePalette)),
		typeof(SurfacePalette));

	public static PooledType SurfacePaletteWriter { get; } = new PooledType(
		new Guid("b8e03952-dae3-4ab7-9995-0c41d10100b6"),
		CreateName(nameof(SurfacePaletteWriter)),
		typeof(SurfacePaletteWriter));

	public static IEnumerable<PooledType> Enumerate()
	{
		yield return Shape3D;
		yield return Shape3DWriter;
		yield return VoxelPalette;
		yield return VoxelPaletteWriter;
		yield return SurfacePalette;
		yield return SurfacePaletteWriter;
	}

	private static string CreateName(string name) => $"{nameof(Coplanar)}.{name}";
}
