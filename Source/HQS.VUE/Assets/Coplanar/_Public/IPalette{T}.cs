﻿namespace HQS.VUE.Assets.Coplanar;

public interface IPalette<T> : IReadOnlyList<T>
{
	int MaxCapacity { get; }

	int Capacity { get; set; }

	new T this[int index] { get; set; }
}
