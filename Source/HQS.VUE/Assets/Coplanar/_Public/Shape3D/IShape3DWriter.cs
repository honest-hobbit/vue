﻿namespace HQS.VUE.Assets.Coplanar;

public interface IShape3DWriter : IShape3DBase, IAssetWriter<IShape3D>
{
	new Guid GridKey { get; set; }

	new Guid MapKey { get; set; }

	new bool AutoContour { get; set; }

	new bool AutoCollisionCheck { get; set; }

	bool ToBeContoured { get; set; }

	bool ToBeCollisionChecked { get; set; }
}
