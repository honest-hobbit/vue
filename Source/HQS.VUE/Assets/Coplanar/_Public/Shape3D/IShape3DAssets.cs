﻿namespace HQS.VUE.Assets.Coplanar;

public interface IShape3DAssets : IAssetCollection
{
	IReadOnlyDictionary<Guid, IShape3D> Committed { get; }

	IShape3DWriter CreateNew();

	IShape3DWriter LoadFrom(IAssetStore store, Guid key);
}
