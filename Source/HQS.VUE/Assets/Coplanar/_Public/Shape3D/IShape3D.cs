﻿namespace HQS.VUE.Assets.Coplanar;

public interface IShape3D :
	IShape3DBase, IChangeableAsset<IShape3DWriter>
{
}
