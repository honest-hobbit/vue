﻿namespace HQS.VUE.Assets.Coplanar;

public interface IShape3DBase : IAsset
{
	Guid GridKey { get; }

	IGrid3D Grid { get; }

	Guid MapKey { get; }

	IVoxelPalette Map { get; }

	bool AutoContour { get; }

	bool AutoCollisionCheck { get; }
}
