﻿namespace HQS.VUE.Assets.Grids;

internal class Grid3DWriter : AbstractAssetWriter<
	Grid3DData, IGrid3D, Grid3D, IGrid3DWriter, Grid3DWriter>,
	IGrid3DWriter
{
	public Grid3DWriter(IPinPool<Grid3D> assetPool)
		: base(assetPool)
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public Type CellType { get; private set; }

	/// <inheritdoc />
	public ICells3DWriter<T> GetCells<T>()
		where T : struct, IEquatable<T>
	{
		throw new NotImplementedException();
	}

	/// <inheritdoc />
	protected override void SetData(Grid3DData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.CellType = data.CellType;
	}

	/// <inheritdoc />
	protected override Grid3DData GetData()
	{
		return new Grid3DData()
		{
			Asset = this.GetAssetData(),
			CellType = this.CellType,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.CellType = null;
	}
}
