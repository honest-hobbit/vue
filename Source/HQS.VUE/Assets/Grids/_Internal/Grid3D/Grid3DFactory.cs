﻿namespace HQS.VUE.Assets.Grids;

internal class Grid3DFactory : AbstractAssetFactory
{
	private readonly AssetWriterTracker<Grid3DWriter> tracker;

	public Grid3DFactory(AssetWriterTracker<Grid3DWriter> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public IGrid3DWriter CreateNew<T>(ChunkConfig config)
		where T : struct, IEquatable<T>
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

		var data = Grid3DData.Empty;
		data.Asset = AssetData.ForNewAsset(this.CurrentTime);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(data);
		return writer;
	}

	public IGrid3DWriter LoadFrom<T>(IAssetStore store, Guid key)
		where T : struct, IEquatable<T>
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
		Ensure.That(store, nameof(store)).IsNotNull();

		// TODO need to deserialize data
		////var data = store.GetAsset(key);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(Grid3DData.Empty);

		return writer;
	}
}
