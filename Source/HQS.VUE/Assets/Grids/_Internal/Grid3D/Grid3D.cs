﻿namespace HQS.VUE.Assets.Grids;

internal class Grid3D : AbstractAsset<
	Grid3DData, IGrid3D, Grid3D, IGrid3DWriter, Grid3DWriter>,
	IGrid3D
{
	public Grid3D()
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public Type CellType { get; private set; }

	/// <inheritdoc />
	protected override Grid3D Self => this;

	/// <inheritdoc />
	public ICells3D<T> GetCells<T>()
		where T : struct, IEquatable<T>
	{
		throw new NotImplementedException();
	}

	/// <inheritdoc />
	public override void InitializeFrom(Grid3DData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.CellType = data.CellType;
	}

	/// <inheritdoc />
	public override Grid3DData GetData()
	{
		return new Grid3DData()
		{
			Asset = this.GetAssetData(),
			CellType = this.CellType,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.CellType = null;
	}
}
