﻿namespace HQS.VUE.Assets.Grids;

internal class Grid3DAssets : IGrid3DAssets
{
	private readonly Grid3DFactory assetFactory;

	public Grid3DAssets(
		Grid3DFactory assetFactory,
		ICommittedAssets<IGrid3D> commitedAssets)
	{
		Debug.Assert(assetFactory != null);
		Debug.Assert(commitedAssets != null);

		this.assetFactory = assetFactory;
		this.Committed = commitedAssets.Committed;
	}

	/// <inheritdoc />
	public Type CollectionType => typeof(IGrid3DAssets);

	/// <inheritdoc />
	public Type AssetType => typeof(IGrid3D);

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, IGrid3D> Committed { get; }

	/// <inheritdoc />
	public IGrid3DWriter CreateNew<T>(ChunkConfig config)
		where T : struct, IEquatable<T> => this.assetFactory.CreateNew<T>(config);

	/// <inheritdoc />
	public IGrid3DWriter LoadFrom<T>(IAssetStore store, Guid key)
		where T : struct, IEquatable<T> => this.assetFactory.LoadFrom<T>(store, key);
}
