﻿namespace HQS.VUE.Assets.Grids;

internal struct Grid3DData
{
	public AssetData Asset;

	public Type CellType;

	public IAssetStore Store;

	public static Grid3DData Empty => new Grid3DData()
	{
		Asset = AssetData.Empty,
		CellType = null,
		Store = null,
	};
}
