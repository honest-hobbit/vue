﻿namespace HQS.VUE.Assets.Grids;

internal class Grid2DAssets : IGrid2DAssets
{
	private readonly Grid2DFactory assetFactory;

	public Grid2DAssets(
		Grid2DFactory assetFactory,
		ICommittedAssets<IGrid2D> commitedAssets)
	{
		Debug.Assert(assetFactory != null);
		Debug.Assert(commitedAssets != null);

		this.assetFactory = assetFactory;
		this.Committed = commitedAssets.Committed;
	}

	/// <inheritdoc />
	public Type CollectionType => typeof(IGrid2DAssets);

	/// <inheritdoc />
	public Type AssetType => typeof(IGrid2D);

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, IGrid2D> Committed { get; }

	/// <inheritdoc />
	public IGrid2DWriter CreateNew<T>(ChunkConfig config)
		where T : struct, IEquatable<T> => this.assetFactory.CreateNew<T>(config);

	/// <inheritdoc />
	public IGrid2DWriter LoadFrom<T>(IAssetStore store, Guid key)
		where T : struct, IEquatable<T> => this.assetFactory.LoadFrom<T>(store, key);
}
