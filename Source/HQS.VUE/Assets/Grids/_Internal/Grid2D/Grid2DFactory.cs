﻿namespace HQS.VUE.Assets.Grids;

internal class Grid2DFactory : AbstractAssetFactory
{
	private readonly AssetWriterTracker<Grid2DWriter> tracker;

	public Grid2DFactory(AssetWriterTracker<Grid2DWriter> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public IGrid2DWriter CreateNew<T>(ChunkConfig config)
		where T : struct, IEquatable<T>
	{
		var data = Grid2DData.Empty;
		data.Asset = AssetData.ForNewAsset(this.CurrentTime);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(data);
		return writer;
	}

	public IGrid2DWriter LoadFrom<T>(IAssetStore store, Guid key)
		where T : struct, IEquatable<T>
	{
		Ensure.That(store, nameof(store)).IsNotNull();

		// TODO need to deserialize data
		////var data = store.GetAsset(key);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(Grid2DData.Empty);

		return writer;
	}
}
