﻿namespace HQS.VUE.Assets.Grids;

internal class Grid2D : AbstractAsset<
	Grid2DData, IGrid2D, Grid2D, IGrid2DWriter, Grid2DWriter>,
	IGrid2D
{
	public Grid2D()
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public Type CellType { get; private set; }

	/// <inheritdoc />
	protected override Grid2D Self => this;

	/// <inheritdoc />
	public ICells2D<T> GetCells<T>()
		where T : struct, IEquatable<T>
	{
		throw new NotImplementedException();
	}

	/// <inheritdoc />
	public override void InitializeFrom(Grid2DData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.CellType = data.CellType;
	}

	/// <inheritdoc />
	public override Grid2DData GetData()
	{
		return new Grid2DData()
		{
			Asset = this.GetAssetData(),
			CellType = this.CellType,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.CellType = null;
	}
}
