﻿namespace HQS.VUE.Assets.Grids;

internal class Grid2DWriter : AbstractAssetWriter<
	Grid2DData, IGrid2D, Grid2D, IGrid2DWriter, Grid2DWriter>,
	IGrid2DWriter
{
	public Grid2DWriter(IPinPool<Grid2D> assetPool)
		: base(assetPool)
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public Type CellType { get; private set; }

	/// <inheritdoc />
	public ICells2DWriter<T> GetCells<T>()
		where T : struct, IEquatable<T>
	{
		throw new NotImplementedException();
	}

	/// <inheritdoc />
	protected override void SetData(Grid2DData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.CellType = data.CellType;
	}

	/// <inheritdoc />
	protected override Grid2DData GetData()
	{
		return new Grid2DData()
		{
			Asset = this.GetAssetData(),
			CellType = this.CellType,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.CellType = null;
	}
}
