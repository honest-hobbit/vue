﻿namespace HQS.VUE.Assets.Grids;

internal struct Grid2DData
{
	public AssetData Asset;

	public Type CellType;

	public IAssetStore Store;

	public static Grid2DData Empty => new Grid2DData()
	{
		Asset = AssetData.Empty,
		CellType = null,
		Store = null,
	};
}
