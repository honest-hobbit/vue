﻿namespace HQS.VUE.Assets.Grids;

public interface ICells3DWriter<T> : ICells3D<T>
	where T : struct, IEquatable<T>
{
	new IGrid3DWriter Grid { get; }

	ICells3D<T> Previous { get; }

	bool HasChanges { get; }

	new T this[Int3 location] { get; set; }

	new T this[int x, int y, int z] { get; set; }

	T Replace(Int3 location, T value);

	T Replace(int x, int y, int z, T value);

	// TODO this needs some way of specifying the dimensions of the source to copy in
	void MergeWith(ICells3D<T> source, Func<T, T, T> reducer, Int3 offset);

	// TODO this needs some way of specifying the dimensions of the source to copy in
	void MergeWith(IEnumerable<ICells3D<T>> sources, Func<T, T, T> reducer, Int3 offset);

	void DiscardChanges();
}
