﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid3D :
	IGrid3DBase, IChangeableAsset<IGrid3DWriter>
{
	ICells3D<T> GetCells<T>()
		where T : struct, IEquatable<T>;
}
