﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid3DAssets : IAssetCollection
{
	IReadOnlyDictionary<Guid, IGrid3D> Committed { get; }

	IGrid3DWriter CreateNew<T>(ChunkConfig config)
		where T : struct, IEquatable<T>;

	// TODO not sure generic T should be here.
	// The saved asset should already know it's type T.
	IGrid3DWriter LoadFrom<T>(IAssetStore store, Guid key)
		where T : struct, IEquatable<T>;
}
