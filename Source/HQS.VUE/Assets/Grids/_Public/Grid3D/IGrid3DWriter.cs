﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid3DWriter : IGrid3DBase, IAssetWriter<IGrid3D>
{
	ICells3DWriter<T> GetCells<T>()
		where T : struct, IEquatable<T>;
}
