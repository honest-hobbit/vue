﻿namespace HQS.VUE.Assets.Grids;

public interface ICells3D<T> : IKeyed<Guid>
	where T : struct, IEquatable<T>
{
	IGrid3D Grid { get; }

	T this[Int3 location] { get; }

	T this[int x, int y, int z] { get; }
}
