﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid3DBase : IAsset
{
	Type CellType { get; }
}
