﻿namespace HQS.VUE.Assets.Grids;

public static class GridAssestExtensions
{
	public static IGrid2DAssets GetGrids2D(this IJobAssets resources)
	{
		Ensure.That(resources, nameof(resources)).IsNotNull();

		return resources.Get<IGrid2DAssets>();
	}

	public static IGrid3DAssets GetGrids3D(this IJobAssets resources)
	{
		Ensure.That(resources, nameof(resources)).IsNotNull();

		return resources.Get<IGrid3DAssets>();
	}

	public static void RegisterGridsAssets(this VoxelUniverseBuilder builder)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		builder.AddRegistrations(container =>
		{
			container.RegisterAllPools(GridsPooledTypes.Enumerate());
			container.RegisterAllCommandsFromAssembly<INamespaceGrids>(
				typeof(GridAssestExtensions).Assembly);

			container.Register<Grid2D>();
			container.Register<Grid2DWriter>();
			container.RegisterAssetFactory<Grid2DFactory>();
			container.RegisterAssetCollection<IGrid2DAssets, Grid2DAssets>();

			container.Register<Grid3D>();
			container.Register<Grid3DWriter>();
			container.RegisterAssetFactory<Grid3DFactory>();
			container.RegisterAssetCollection<IGrid3DAssets, Grid3DAssets>();
		});
	}
}
