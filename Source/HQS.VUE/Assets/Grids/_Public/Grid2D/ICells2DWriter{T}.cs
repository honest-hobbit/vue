﻿namespace HQS.VUE.Assets.Grids;

public interface ICells2DWriter<T> : ICells2D<T>
	where T : struct, IEquatable<T>
{
	new IGrid2DWriter Grid { get; }

	ICells2D<T> Previous { get; }

	bool HasChanges { get; }

	new T this[Int2 location] { get; set; }

	new T this[int x, int y] { get; set; }

	T Replace(Int2 location, T value);

	T Replace(int x, int y, T value);

	// TODO this needs some way of specifying the dimensions of the source to copy in
	void MergeWith(ICells2D<T> source, Func<T, T, T> reducer, Int2 offset);

	// TODO this needs some way of specifying the dimensions of the source to copy in
	void MergeWith(IEnumerable<ICells2D<T>> sources, Func<T, T, T> reducer, Int2 offset);

	void DiscardChanges();
}
