﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid2DBase : IAsset
{
	Type CellType { get; }
}
