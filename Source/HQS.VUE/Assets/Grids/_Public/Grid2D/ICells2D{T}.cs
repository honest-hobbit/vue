﻿namespace HQS.VUE.Assets.Grids;

public interface ICells2D<T> : IKeyed<Guid>
	where T : struct, IEquatable<T>
{
	IGrid2D Grid { get; }

	T this[Int2 location] { get; }

	T this[int x, int y] { get; }
}
