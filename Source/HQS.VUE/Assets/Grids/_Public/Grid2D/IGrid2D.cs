﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid2D :
	IGrid2DBase, IChangeableAsset<IGrid2DWriter>
{
	ICells2D<T> GetCells<T>()
		where T : struct, IEquatable<T>;
}
