﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid2DWriter : IGrid2DBase, IAssetWriter<IGrid2D>
{
	ICells2DWriter<T> GetCells<T>()
		where T : struct, IEquatable<T>;
}
