﻿namespace HQS.VUE.Assets.Grids;

public interface IGrid2DAssets : IAssetCollection
{
	IReadOnlyDictionary<Guid, IGrid2D> Committed { get; }

	IGrid2DWriter CreateNew<T>(ChunkConfig config)
		where T : struct, IEquatable<T>;

	// TODO not sure generic T should be here.
	// The saved asset should already know it's type T.
	IGrid2DWriter LoadFrom<T>(IAssetStore store, Guid key)
		where T : struct, IEquatable<T>;
}
