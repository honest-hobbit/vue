﻿namespace HQS.VUE.Assets.Grids;

public readonly struct ChunkConfig : IEquatable<ChunkConfig>
{
	private const byte CellMask = 0b_0000_1111;

	public ChunkConfig(int subchunkExponent, int cellExponent)
	{
		ValidateExponent(subchunkExponent, nameof(subchunkExponent));
		ValidateExponent(cellExponent, nameof(cellExponent));

		this.Value = (byte)(((subchunkExponent & CellMask) << 4) | (cellExponent & CellMask));

		Debug.Assert(IsExponentValid(this.SubchunkExponent));
		Debug.Assert(IsExponentValid(this.CellExponent));
	}

	public ChunkConfig(byte value)
	{
		this.Value = value;

		ValidateExponent(this.SubchunkExponent, nameof(this.SubchunkExponent));
		ValidateExponent(this.CellExponent, nameof(this.CellExponent));
	}

	// Min of 1 was chosen to ensure some efficiency of batching data together occurs.
	// Min of 0 could work but significantly degrades performance and also IsAssigned uses 0 as not assigned.
	public static int MinExponent => 1;

	// Max of 5 was chosen so that Shorts can be used to index into 3D arrays mapped to 1D
	// [(2^5)^3 - 1 <= 2^15 - 1 aka Short.MaxValue]. Also (2^5)^3 = 32,768 which is a pretty large array already.
	// Otherwise it would have been a max of 7 because MeshQuadSlim and BoxColliderStructSlim use single bytes
	// and need to store a range of 2^x + 1 so therefore 2^7 + 1 <= 2^8 aka Byte.MaxValue, but
	// (2^7)^3 = 2,097,152 which is an extremely large array and not good for performance.
	public static int MaxExponent => 5;

	public static IEnumerable<int> EnumerateExponents()
	{
		for (int exponent = MinExponent; exponent <= MaxExponent; exponent++)
		{
			yield return exponent;
		}
	}

	public static IEnumerable<ChunkConfig> EnumerateSubchunkExponents()
	{
		for (int exponent = MinExponent; exponent <= MaxExponent; exponent++)
		{
			yield return new ChunkConfig(exponent, MinExponent);
		}
	}

	public static IEnumerable<ChunkConfig> EnumerateCellExponents()
	{
		for (int exponent = MinExponent; exponent <= MaxExponent; exponent++)
		{
			yield return new ChunkConfig(MinExponent, exponent);
		}
	}

	public bool IsUnassigned => this.Value == 0;

	public bool IsAssigned => this.Value != 0;

	public byte Value { get; }

	public int CombinedExponent => this.SubchunkExponent + this.CellExponent;

	public int SubchunkExponent => this.Value >> 4;

	public int CellExponent => this.Value & CellMask;

	public SquareArrayIndexer CombinedIndexer2D => new SquareArrayIndexer(this.CombinedExponent);

	public SquareArrayIndexer SubchunkIndexer2D => new SquareArrayIndexer(this.SubchunkExponent);

	public SquareArrayIndexer CellIndexer2D => new SquareArrayIndexer(this.CellExponent);

	public CubeArrayIndexer CombinedIndexer3D => new CubeArrayIndexer(this.CombinedExponent);

	public CubeArrayIndexer SubchunkIndexer3D => new CubeArrayIndexer(this.SubchunkExponent);

	public CubeArrayIndexer CellIndexer3D => new CubeArrayIndexer(this.CellExponent);

	public static bool IsExponentValid(int exponent) => exponent.IsIn(MinExponent, MaxExponent);

	internal static void ValidateExponent(int exponent, string exponentName)
	{
		Debug.Assert(!exponentName.IsNullOrWhiteSpace());

		if (!IsExponentValid(exponent))
		{
			throw new ArgumentOutOfRangeException(
				exponentName, $"{exponentName} of {exponent} is not in the range of {MinExponent} to {MaxExponent} inclusive.");
		}
	}

	public static bool operator ==(ChunkConfig lhs, ChunkConfig rhs) => lhs.Equals(rhs);

	public static bool operator !=(ChunkConfig lhs, ChunkConfig rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ChunkConfig other) => this.Value == other.Value;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.Value.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => $"[{this.SubchunkExponent}, {this.CellExponent}]";
}
