﻿namespace HQS.VUE.Assets.Grids;

public static class GridsPooledTypes
{
	public static PooledType Grid2D { get; } = new PooledType(
		new Guid("659f298a-1505-4afe-b5eb-7b1b26a89b11"),
		CreateName(nameof(Grid2D)),
		typeof(Grid2D));

	public static PooledType Grid2DWriter { get; } = new PooledType(
		new Guid("3adc536f-757d-4429-9d30-abfa948e4ec1"),
		CreateName(nameof(Grid2DWriter)),
		typeof(Grid2DWriter));

	public static PooledType Grid3D { get; } = new PooledType(
		new Guid("82faa7d6-3f40-4854-9361-0166dfdec783"),
		CreateName(nameof(Grid3D)),
		typeof(Grid3D));

	public static PooledType Grid3DWriter { get; } = new PooledType(
		new Guid("ca43673f-70d3-46e4-834f-4d51782142d1"),
		CreateName(nameof(Grid3DWriter)),
		typeof(Grid3DWriter));

	public static IEnumerable<PooledType> Enumerate()
	{
		yield return Grid2D;
		yield return Grid2DWriter;
		yield return Grid3D;
		yield return Grid3DWriter;
	}

	private static string CreateName(string name) => $"{nameof(Grids)}.{name}";
}
