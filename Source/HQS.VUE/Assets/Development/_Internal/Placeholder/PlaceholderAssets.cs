﻿namespace HQS.VUE.Assets.Development;

internal class PlaceholderAssets : IPlaceholderAssets
{
	private readonly PlaceholderFactory assetFactory;

	public PlaceholderAssets(
		PlaceholderFactory assetFactory,
		ICommittedAssets<IPlaceholder> commitedAssets)
	{
		Debug.Assert(assetFactory != null);
		Debug.Assert(commitedAssets != null);

		this.assetFactory = assetFactory;
		this.Committed = commitedAssets.Committed;
	}

	/// <inheritdoc />
	public Type CollectionType => typeof(IPlaceholderAssets);

	/// <inheritdoc />
	public Type AssetType => typeof(IPlaceholder);

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, IPlaceholder> Committed { get; }

	/// <inheritdoc />
	public IPlaceholderWriter CreateNew() => this.assetFactory.CreateNew();

	/// <inheritdoc />
	public IPlaceholderWriter LoadFrom(IAssetStore store, Guid key) =>
		this.assetFactory.LoadFrom(store, key);
}
