﻿namespace HQS.VUE.Assets.Development;

internal class PlaceholderDataSerDes : CompositeSerDes<
	AssetData, string, double, PlaceholderData>
{
	public PlaceholderDataSerDes(
		ISerDes<AssetData> assetData, ISerDes<string> message, ISerDes<double> number)
		: base(assetData, message, number)
	{
	}

	public static ISerDes<PlaceholderData> Instance { get; } = new PlaceholderDataSerDes(
		AssetDataSerDes.Instance,
		StringSerDes.LengthAsVarint,
		SerDes.OfDouble);

	/// <inheritdoc />
	protected override PlaceholderData ComposeValue(
		AssetData assetData, string message, double number) => new PlaceholderData()
		{
			Asset = assetData,
			Message = message,
			Number = number,
		};

	/// <inheritdoc />
	protected override void DecomposeValue(
		PlaceholderData placeholderData, out AssetData assetData, out string message, out double number)
	{
		assetData = placeholderData.Asset;
		message = placeholderData.Message;
		number = placeholderData.Number;
	}
}
