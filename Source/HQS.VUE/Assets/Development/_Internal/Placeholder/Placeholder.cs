﻿namespace HQS.VUE.Assets.Development;

internal class Placeholder : AbstractAsset<
	PlaceholderData, IPlaceholder, Placeholder, IPlaceholderWriter, PlaceholderWriter>,
	IPlaceholder
{
	public Placeholder()
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public string Message { get; private set; }

	/// <inheritdoc />
	public double Number { get; private set; }

	/// <inheritdoc />
	protected override Placeholder Self => this;

	/// <inheritdoc />
	public override void InitializeFrom(PlaceholderData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.Message = data.Message;
		this.Number = data.Number;
	}

	/// <inheritdoc />
	public override PlaceholderData GetData()
	{
		return new PlaceholderData()
		{
			Asset = this.GetAssetData(),
			Message = this.Message,
			Number = this.Number,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.Message = string.Empty;
		this.Number = 0;
	}
}
