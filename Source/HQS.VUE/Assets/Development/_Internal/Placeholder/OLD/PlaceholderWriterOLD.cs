﻿namespace HQS.VUE.Assets.Development;

internal class PlaceholderWriterOLD :
	AbstractAssetWriter,
	IPlaceholderWriter,
	ITrackableAsset<PlaceholderWriterOLD>,
	ICommittableAsset<IPlaceholder>,
	IDeinitializable
{
	private readonly IPinPool<PlaceholderOLD> pool;

	private Pin<PlaceholderOLD> previousPin;

	private AssetWriterTracker<PlaceholderWriterOLD> tracker;

	public PlaceholderWriterOLD(IPinPool<PlaceholderOLD> pool)
	{
		Debug.Assert(pool != null);

		this.pool = pool;
	}

	/// <inheritdoc />
	public IPlaceholder Previous { get; private set; }

	/// <inheritdoc />
	public string Message { get; set; }

	/// <inheritdoc />
	public double Number { get; set; }

	/// <inheritdoc />
	public void SetAssetTracker(AssetWriterTracker<PlaceholderWriterOLD> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public void InitializeFrom(PlaceholderData data)
	{
		this.previousPin = this.pool.Rent(out var previous);
		previous.SetTracker(this.tracker);
		previous.InitializeFrom(data);
		this.InitializeFrom(previous);
	}

	public void InitializeFrom(PlaceholderOLD previous)
	{
		Debug.Assert(previous != null);

		this.Previous = previous;

		var data = previous.GetData();
		this.InitializeBase(data.Asset, data.Store);
		this.Message = data.Message;
		this.Number = data.Number;
	}

	/// <inheritdoc />
	public Pin<IPlaceholder> ToCommittedAsset()
	{
		var pin = this.pool.Rent(out var asset);
		asset.SetTracker(this.tracker);
		asset.InitializeFrom(this.GetData());
		return pin.CastTo<IPlaceholder>();
	}

	public PlaceholderData GetData()
	{
		return new PlaceholderData()
		{
			Asset = AssetData.From(this),
			Message = this.Message,
			Number = this.Number,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	public override void Deinitialize()
	{
		base.Deinitialize();
		this.previousPin.Dispose();
		this.previousPin = default;
	}
}
