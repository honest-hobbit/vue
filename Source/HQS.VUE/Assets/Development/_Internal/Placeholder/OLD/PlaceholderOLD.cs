﻿namespace HQS.VUE.Assets.Development;

internal class PlaceholderOLD : AbstractAsset, IPlaceholder
{
	private AssetWriterTracker<PlaceholderWriterOLD> tracker;

	/// <inheritdoc />
	public string Message { get; private set; }

	/// <inheritdoc />
	public double Number { get; private set; }

	/// <inheritdoc />
	public IPlaceholderWriter CreateChanges()
	{
		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(this);
		return writer;
	}

	public void SetTracker(AssetWriterTracker<PlaceholderWriterOLD> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public void InitializeFrom(PlaceholderData data)
	{
		this.InitializeBase(data.Asset, data.Store);

		this.Message = data.Message;
		this.Number = data.Number;
	}

	public PlaceholderData GetData()
	{
		return new PlaceholderData()
		{
			Asset = AssetData.From(this),
			Message = this.Message,
			Number = this.Number,
			Store = this.GetStore(),
		};
	}
}
