﻿namespace HQS.VUE.Assets.Development;

internal class PlaceholderFactory : AbstractAssetFactory
{
	private readonly AssetWriterTracker<PlaceholderWriter> tracker;

	public PlaceholderFactory(AssetWriterTracker<PlaceholderWriter> tracker)
	{
		Debug.Assert(tracker != null);

		this.tracker = tracker;
	}

	public IPlaceholderWriter CreateNew()
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

		var data = PlaceholderData.Empty;
		data.Asset = AssetData.ForNewAsset(this.CurrentTime);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(data);
		return writer;
	}

	public IPlaceholderWriter LoadFrom(IAssetStore store, Guid key)
	{
		Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
		Ensure.That(store, nameof(store)).IsNotNull();

		// TODO need to deserialize data
		////var data = store.GetAsset(key);

		var writer = this.tracker.GetAssetWriter();
		writer.InitializeFrom(PlaceholderData.Empty);

		return writer;
	}
}
