﻿namespace HQS.VUE.Assets.Development;

internal struct PlaceholderData
{
	public AssetData Asset;

	public string Message;

	public double Number;

	public IAssetStore Store;

	public static PlaceholderData Empty => new PlaceholderData()
	{
		Asset = AssetData.Empty,
		Message = string.Empty,
		Number = 0,
		Store = null,
	};
}
