﻿namespace HQS.VUE.Assets.Development;

internal class PlaceholderWriter : AbstractAssetWriter<
	PlaceholderData, IPlaceholder, Placeholder, IPlaceholderWriter, PlaceholderWriter>,
	IPlaceholderWriter
{
	public PlaceholderWriter(IPinPool<Placeholder> pool)
		: base(pool)
	{
		this.Deinitialize();
	}

	/// <inheritdoc />
	public string Message { get; set; }

	/// <inheritdoc />
	public double Number { get; set; }

	/// <inheritdoc />
	protected override void SetData(PlaceholderData data)
	{
		this.InitializeBase(data.Asset, data.Store);
		this.Message = data.Message;
		this.Number = data.Number;
	}

	/// <inheritdoc />
	protected override PlaceholderData GetData()
	{
		return new PlaceholderData()
		{
			Asset = this.GetAssetData(),
			Message = this.Message,
			Number = this.Number,
			Store = this.GetStore(),
		};
	}

	/// <inheritdoc />
	protected override void ClearData()
	{
		this.Message = string.Empty;
		this.Number = 0;
	}
}
