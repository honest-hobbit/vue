﻿namespace HQS.VUE.Assets.Development;

public static class DevelopmentAssetExtensions
{
	public static IPlaceholderAssets GetPlaceholders(this IJobAssets resources)
	{
		Ensure.That(resources, nameof(resources)).IsNotNull();

		return resources.Get<IPlaceholderAssets>();
	}

	public static void RegisterDevelopmentAssets(this VoxelUniverseBuilder builder)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		builder.AddRegistrations(container =>
		{
			container.RegisterAllPools(DevelopmentPooledTypes.Enumerate());
			container.RegisterAllCommandsFromAssembly<INamespaceDevelopment>(
				typeof(DevelopmentAssetExtensions).Assembly);

			container.Register<Placeholder>();
			container.Register<PlaceholderWriter>();
			container.RegisterAssetFactory<PlaceholderFactory>();
			container.RegisterAssetCollection<IPlaceholderAssets, PlaceholderAssets>();
		});
	}
}
