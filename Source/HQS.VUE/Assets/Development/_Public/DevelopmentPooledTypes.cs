﻿namespace HQS.VUE.Assets.Development;

public static class DevelopmentPooledTypes
{
	public static PooledType Placeholder { get; } = new PooledType(
		new Guid("e95af3ed-6666-4525-b761-842083395e77"),
		CreateName(nameof(Placeholder)),
		typeof(Placeholder));

	public static PooledType PlaceholderWriter { get; } = new PooledType(
		new Guid("70b547ed-dbf3-4ae8-b6fc-7d49202adc4f"),
		CreateName(nameof(PlaceholderWriter)),
		typeof(PlaceholderWriter));

	public static IEnumerable<PooledType> Enumerate()
	{
		yield return Placeholder;
		yield return PlaceholderWriter;
	}

	private static string CreateName(string name) => $"{nameof(Development)}.{name}";
}
