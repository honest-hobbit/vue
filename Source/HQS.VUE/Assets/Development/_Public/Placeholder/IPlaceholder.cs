﻿namespace HQS.VUE.Assets.Development;

public interface IPlaceholder :
	IPlaceholderBase, IChangeableAsset<IPlaceholderWriter>
{
}
