﻿namespace HQS.VUE.Assets.Development;

public interface IPlaceholderAssets : IAssetCollection
{
	IReadOnlyDictionary<Guid, IPlaceholder> Committed { get; }

	IPlaceholderWriter CreateNew();

	IPlaceholderWriter LoadFrom(IAssetStore store, Guid key);
}
