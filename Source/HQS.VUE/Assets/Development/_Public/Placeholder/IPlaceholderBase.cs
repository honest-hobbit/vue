﻿namespace HQS.VUE.Assets.Development;

public interface IPlaceholderBase : IAsset
{
	string Message { get; }

	double Number { get; }
}
