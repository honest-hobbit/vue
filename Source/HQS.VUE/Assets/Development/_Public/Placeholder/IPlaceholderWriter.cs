﻿namespace HQS.VUE.Assets.Development;

public interface IPlaceholderWriter : IPlaceholderBase, IAssetWriter<IPlaceholder>
{
	new string Message { get; set; }

	new double Number { get; set; }
}
