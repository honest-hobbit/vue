﻿namespace HQS.VUE.Assets.Stores;

public class TimeoutAssetStore : AbstractDisposable, IAssetStore
{
	private readonly object padlock = new object();

	private readonly TimeSpan timeout;

	private readonly Func<IAssetStore> createStore;

	private IAssetStore store;

	private int count;

	public TimeoutAssetStore(TimeSpan timeout, Func<IAssetStore> createStore)
	{
		Duration.ValidateIsDuration(timeout, nameof(timeout));
		Ensure.That(createStore, nameof(createStore)).IsNotNull();

		this.timeout = timeout;
		this.createStore = createStore;
	}

	/// <inheritdoc />
	public void GetAllAssetHeaders(ICollection<AssetHeader> assets, PagingArgs paging)
	{
		try
		{
			this.GetStore().GetAllAssetHeaders(assets, paging);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void GetAssetHeaders(
		string assetType, ICollection<AssetHeader> assets, PagingArgs paging)
	{
		try
		{
			this.GetStore().GetAssetHeaders(assetType, assets, paging);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int2>> chunks, PagingArgs paging)
	{
		try
		{
			this.GetStore().GetChunkHeaders(assetKey, chunks, paging);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int3>> chunks, PagingArgs paging)
	{
		try
		{
			this.GetStore().GetChunkHeaders(assetKey, chunks, paging);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public bool TryGetAsset(Guid assetKey, out AssetDocument asset, bool getHeaderData, bool getBodyData)
	{
		try
		{
			return this.GetStore().TryGetAsset(assetKey, out asset, getHeaderData, getBodyData);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public bool TryGetChunk(Guid assetKey, Int2 location, out ChunkDocument<Int2> chunk)
	{
		try
		{
			return this.GetStore().TryGetChunk(assetKey, location, out chunk);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public bool TryGetChunk(Guid assetKey, Int3 location, out ChunkDocument<Int3> chunk)
	{
		try
		{
			return this.GetStore().TryGetChunk(assetKey, location, out chunk);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void UpdateAsset(AssetDocument asset)
	{
		try
		{
			this.GetStore().UpdateAsset(asset);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void UpdateAsset(AssetDocument asset, IReadOnlyList<ChunkDocument<Int2>> chunks)
	{
		try
		{
			this.GetStore().UpdateAsset(asset, chunks);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void UpdateAsset(AssetDocument asset, IReadOnlyList<ChunkDocument<Int3>> chunks)
	{
		try
		{
			this.GetStore().UpdateAsset(asset, chunks);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void DeleteAsset(Guid assetKey)
	{
		try
		{
			this.GetStore().DeleteAsset(assetKey);
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	public void DeleteAllAssets()
	{
		try
		{
			this.GetStore().DeleteAllAssets();
		}
		finally
		{
			this.StartTimeout();
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		lock (this.padlock)
		{
			if (this.store != null)
			{
				this.count = 0;
				try
				{
					this.store.Dispose();
				}
				finally
				{
					this.store = null;
				}
			}
		}
	}

	private IAssetStore GetStore()
	{
		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.count++;
			try
			{
				return this.store ??= this.createStore();
			}
			catch (Exception)
			{
				this.count--;
				this.store?.Dispose();
				this.store = null;
				throw;
			}
		}
	}

	private async void StartTimeout()
	{
		if (this.timeout == Duration.Span.InfiniteTimeout)
		{
			return;
		}

		if (this.timeout != Duration.Span.InstantTimeout)
		{
			// TODO this could be optimized to reuse 1 Task for an ongoing timeout
			// instead of awaiting a new Task every time
			await Task.Delay(this.timeout).DontMarshallContext();
		}

		lock (this.padlock)
		{
			if (this.count == 0)
			{
				return;
			}

			this.count--;
			if (this.count == 0)
			{
				try
				{
					this.store?.Dispose();
				}
				finally
				{
					this.store = null;
				}
			}
		}
	}
}
