﻿namespace HQS.VUE.Assets.Stores;

internal class TryGetAssetCommand : AbstractSingleStoreCommand
{
	private readonly SqliteParameter parameterKey = new SqliteParameter("$Key", SqliteType.Blob);

	private readonly Func<Stream> streamFactory;

	public TryGetAssetCommand(Func<Stream> streamFactory)
	{
		Debug.Assert(streamFactory != null);

		this.streamFactory = streamFactory;
		this.Command.CommandText = @"
				SELECT rowid, Type
				FROM Assets
				WHERE Key = $Key
				LIMIT 1";

		this.Command.Parameters.Add(this.parameterKey);
	}

	public bool Execute(Guid assetKey, out AssetDocument asset, bool getHeaderData, bool getBodyData)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(assetKey != Guid.Empty);

		this.parameterKey.Value = assetKey;

		using var reader = this.Command.ExecuteReader();
		if (!reader.Read())
		{
			asset = default;
			return false;
		}

		var rowid = reader.GetInt64(0);
		var assetType = reader.GetString(1);
		var headerStream = getHeaderData ? GetStream("Header") : null;
		var bodyStream = getBodyData ? GetStream("Body") : null;

		asset = new AssetDocument(assetKey, assetType, headerStream, bodyStream);
		return true;

		Stream GetStream(string columnName)
		{
			using var readStream = new SqliteBlob(
				this.Command.Connection, "Assets", columnName, rowid, readOnly: true);

			var outputStream = this.streamFactory();
			Debug.Assert(outputStream != null);
			try
			{
				long position = outputStream.Position;
				readStream.CopyTo(outputStream);
				outputStream.Position = position;
			}
			catch (Exception)
			{
				outputStream.Dispose();
				throw;
			}

			return outputStream;
		}
	}
}
