﻿namespace HQS.VUE.Assets.Stores;

internal class GetAssetHeadersCommand : AbstractSingleStoreCommand
{
	private readonly SqliteParameter parameterType = new SqliteParameter("$Type", SqliteType.Text);

	private readonly SqliteParameter parameterFirstKey = new SqliteParameter("$FirstKey", SqliteType.Blob);

	private readonly SqliteParameter parameterCount = new SqliteParameter("$Count", SqliteType.Integer);

	private readonly Func<Stream> streamFactory;

	public GetAssetHeadersCommand(Func<Stream> streamFactory, bool reverse)
	{
		Debug.Assert(streamFactory != null);

		this.streamFactory = streamFactory;
		if (reverse)
		{
			this.Command.CommandText = @"
					SELECT rowid, Key, Type
					FROM Assets
					WHERE Key <= $FirstKey AND ($Type IS NULL OR Type = $Type)
					ORDER BY Key DESC
					LIMIT $Count";
		}
		else
		{
			this.Command.CommandText = @"
					SELECT rowid, Key, Type
					FROM Assets
					WHERE Key >= $FirstKey AND ($Type IS NULL OR Type = $Type)
					ORDER BY Key ASC
					LIMIT $Count";
		}

		this.Command.Parameters.Add(this.parameterType);
		this.Command.Parameters.Add(this.parameterFirstKey);
		this.Command.Parameters.Add(this.parameterCount);
	}

	public void Execute(
		string assetType,
		ICollection<AssetHeader> assets,
		Guid firstKey,
		int count)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(assetType == null || !assetType.IsNullOrWhiteSpace());
		Debug.Assert(assets != null);
		Debug.Assert(count >= 0);

		this.parameterType.Value = (object)assetType ?? DBNull.Value;
		this.parameterFirstKey.Value = firstKey;
		this.parameterCount.Value = count;

		using var reader = this.Command.ExecuteReader();
		while (reader.Read())
		{
			var rowid = reader.GetInt64(0);

			using var readHeaderStream = new SqliteBlob(
				this.Command.Connection, "Assets", "Header", rowid, readOnly: true);

			var headerData = this.streamFactory();
			Debug.Assert(headerData != null);
			try
			{
				long position = headerData.Position;
				readHeaderStream.CopyTo(headerData);
				headerData.Position = position;
			}
			catch (Exception)
			{
				headerData.Dispose();
				throw;
			}

			assets.Add(new AssetHeader(
				reader.GetGuid(1),
				assetType ?? reader.GetString(2),
				headerData));
		}
	}
}
