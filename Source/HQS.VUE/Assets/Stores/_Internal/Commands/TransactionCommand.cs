﻿namespace HQS.VUE.Assets.Stores;

internal class TransactionCommand : AbstractStoreCommand
{
	private readonly SqliteCommand beginCommand;

	private readonly SqliteCommand commitCommand;

	private readonly SqliteCommand rollbackCommand;

	public TransactionCommand()
	{
		this.beginCommand = new SqliteCommand(@"BEGIN TRANSACTION");
		this.commitCommand = new SqliteCommand(@"COMMIT");
		this.rollbackCommand = new SqliteCommand(@"ROLLBACK");
	}

	public void BeginTransaction()
	{
		Debug.Assert(this.IsInitialized);

		this.beginCommand.ExecuteNonQuery();
	}

	public void CommitTransaction()
	{
		Debug.Assert(this.IsInitialized);

		this.commitCommand.ExecuteNonQuery();
	}

	public void RollbackTransaction()
	{
		Debug.Assert(this.IsInitialized);

		this.rollbackCommand.ExecuteNonQuery();
	}

	/// <inheritdoc />
	protected override void HandleInitialize(SqliteConnection connection)
	{
		this.beginCommand.Connection = connection;
		this.commitCommand.Connection = connection;
		this.rollbackCommand.Connection = connection;
	}

	/// <inheritdoc />
	protected override void HandleDeinitialize()
	{
		this.beginCommand.Connection = null;
		this.commitCommand.Connection = null;
		this.rollbackCommand.Connection = null;
	}

	/// <inheritdoc />
	protected override void HandleDisposal()
	{
		this.beginCommand.Dispose();
		this.commitCommand.Dispose();
		this.rollbackCommand.Dispose();
	}
}
