﻿namespace HQS.VUE.Assets.Stores;

internal class DeleteAllAssetsCommand : AbstractSingleStoreCommand
{
	public DeleteAllAssetsCommand()
	{
		this.Command.CommandText = @"
				BEGIN TRANSACTION;

				DELETE FROM Chunks;

				DELETE FROM Assets;

				COMMIT;";
	}

	public void Execute()
	{
		Debug.Assert(this.IsInitialized);

		this.Command.ExecuteNonQuery();
	}
}
