﻿namespace HQS.VUE.Assets.Stores;

internal class UpdateAssetCommand : AbstractSingleStoreCommand
{
	private readonly SqliteParameter parameterKey =
		new SqliteParameter("$Key", SqliteType.Blob);

	private readonly SqliteParameter parameterType =
		new SqliteParameter("$Type", SqliteType.Text);

	private readonly SqliteParameter parameterHeaderLength =
		new SqliteParameter("$HeaderLength", SqliteType.Integer);

	private readonly SqliteParameter parameterBodyLength =
		new SqliteParameter("$BodyLength", SqliteType.Integer);

	public UpdateAssetCommand()
	{
		this.Command.CommandText = @"
				REPLACE INTO Assets(Key, Type, Header, Body)
				VALUES ($Key, $Type, zeroblob($HeaderLength), zeroblob($BodyLength));

				SELECT last_insert_rowid();";

		this.Command.Parameters.Add(this.parameterKey);
		this.Command.Parameters.Add(this.parameterType);
		this.Command.Parameters.Add(this.parameterHeaderLength);
		this.Command.Parameters.Add(this.parameterBodyLength);
	}

	public void Execute(AssetDocument asset)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(asset.Key != Guid.Empty);
		Debug.Assert(!asset.Header.AssetType.IsNullOrWhiteSpace());
		Debug.Assert(asset.Header.Data != null);
		Debug.Assert(asset.Body != null);

		this.parameterKey.Value = asset.Key;
		this.parameterType.Value = asset.Header.AssetType;
		this.parameterHeaderLength.Value = asset.Header.Data.Length;
		this.parameterBodyLength.Value = asset.Body.Length;

		var rowid = (long)this.Command.ExecuteScalar();

		using var writeHeaderStream = new SqliteBlob(
			this.Command.Connection, "Assets", "Header", rowid, readOnly: false);
		asset.Header.Data.CopyTo(writeHeaderStream);

		using var writeBodyStream = new SqliteBlob(
			this.Command.Connection, "Assets", "Body", rowid, readOnly: false);
		asset.Body.CopyTo(writeBodyStream);
	}
}
