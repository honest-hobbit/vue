﻿namespace HQS.VUE.Assets.Stores;

internal class InitializeCommand : AbstractSingleStoreCommand
{
	public InitializeCommand()
	{
		// write ahead logging could boost Concurrency but does generate extra files
		// https://docs.microsoft.com/en-us/dotnet/standard/data/sqlite/async
		// https://docs.microsoft.com/en-us/dotnet/standard/data/sqlite/database-errors
		// https://www.sqlite.org/wal.html
		// however WAL cannot do atomic commits across multiple database files
		// https://www.sqlite.org/atomiccommit.html#_multi_file_commit
		// this.Command.CommandText = @"PRAGMA journal_mode = 'wal'";
		this.Command.CommandText = @"
				BEGIN TRANSACTION;

				CREATE TABLE IF NOT EXISTS Assets(
					Key BLOB NOT NULL PRIMARY KEY,
					Type TEXT NOT NULL,
					Header BLOB,
					Body BLOB);

				CREATE INDEX IF NOT EXISTS AssetsByType ON Assets(Type);

				CREATE TABLE IF NOT EXISTS Chunks(
					Key BLOB NOT NULL PRIMARY KEY,
					AssetKey BLOB NOT NULL,
					X INTEGER NOT NULL,
					Y INTEGER NOT NULL,
					Z INTEGER NOT NULL,
					Version INTEGER NOT NULL,
					Data BLOB,
					FOREIGN KEY(AssetKey) REFERENCES Assets(Key),
					UNIQUE(AssetKey, X, Y, Z));

				CREATE UNIQUE INDEX IF NOT EXISTS ChunksByLocation ON Chunks(AssetKey, X, Y, Z);

				CREATE INDEX IF NOT EXISTS ChunksByVersion ON Chunks(AssetKey, Version);

				COMMIT;";
	}

	public void Execute()
	{
		Debug.Assert(this.IsInitialized);

		this.Command.ExecuteNonQuery();
	}
}
