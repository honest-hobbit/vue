﻿namespace HQS.VUE.Assets.Stores;

internal class DeleteAssetCommand : AbstractSingleStoreCommand
{
	private readonly SqliteParameter parameterKey = new SqliteParameter("$Key", SqliteType.Blob);

	public DeleteAssetCommand()
	{
		this.Command.CommandText = @"
				BEGIN TRANSACTION;

				DELETE FROM Chunks
				WHERE AssetKey = $Key;

				DELETE FROM Assets
				WHERE Key = $Key;

				COMMIT;";

		this.Command.Parameters.Add(this.parameterKey);
	}

	public void Execute(Guid assetKey)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(assetKey != Guid.Empty);

		this.parameterKey.Value = assetKey;

		this.Command.ExecuteNonQuery();
	}
}
