﻿namespace HQS.VUE.Assets.Stores;

internal class TryGetChunkCommand : AbstractSingleStoreCommand
{
	private readonly SqliteParameter parameterAssetKey = new SqliteParameter("$AssetKey", SqliteType.Blob);

	private readonly SqliteParameter parameterX = new SqliteParameter("$X", SqliteType.Integer);

	private readonly SqliteParameter parameterY = new SqliteParameter("$Y", SqliteType.Integer);

	private readonly SqliteParameter parameterZ = new SqliteParameter("$Z", SqliteType.Integer);

	private readonly Func<Stream> streamFactory;

	public TryGetChunkCommand(Func<Stream> streamFactory)
	{
		Debug.Assert(streamFactory != null);

		this.streamFactory = streamFactory;
		this.Command.CommandText = @"
				SELECT rowid, Key, Version
				FROM Chunks
				WHERE AssetKey = $AssetKey AND X = $X AND Y = $Y AND Z = $Z
				LIMIT 1";

		this.Command.Parameters.Add(this.parameterAssetKey);
		this.Command.Parameters.Add(this.parameterX);
		this.Command.Parameters.Add(this.parameterY);
		this.Command.Parameters.Add(this.parameterZ);
	}

	public bool Execute(
		Guid assetKey,
		int x,
		int y,
		int z,
		out Guid chunkKey,
		out long version,
		out Stream chunkData)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(assetKey != Guid.Empty);

		this.parameterAssetKey.Value = assetKey;
		this.parameterX.Value = x;
		this.parameterY.Value = y;
		this.parameterZ.Value = z;

		using var reader = this.Command.ExecuteReader();
		if (!reader.Read())
		{
			chunkKey = default;
			version = default;
			chunkData = null;
			return false;
		}

		chunkKey = reader.GetGuid(1);
		version = reader.GetInt64(2);

		var rowid = reader.GetInt64(0);
		using var readStream = new SqliteBlob(
			this.Command.Connection, "Chunks", "Data", rowid, readOnly: true);

		var outputStream = this.streamFactory();
		Debug.Assert(outputStream != null);
		try
		{
			long position = outputStream.Position;
			readStream.CopyTo(outputStream);
			outputStream.Position = position;
		}
		catch (Exception)
		{
			outputStream.Dispose();
			throw;
		}

		chunkData = outputStream;
		return true;
	}
}
