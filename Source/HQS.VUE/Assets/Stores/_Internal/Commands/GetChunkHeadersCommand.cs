﻿namespace HQS.VUE.Assets.Stores;

internal class GetChunkHeadersCommand : AbstractSingleStoreCommand
{
	private readonly SqliteParameter parameterAssetKey = new SqliteParameter("$AssetKey", SqliteType.Blob);

	private readonly SqliteParameter parameterFirstKey = new SqliteParameter("$FirstKey", SqliteType.Blob);

	private readonly SqliteParameter parameterCount = new SqliteParameter("$Count", SqliteType.Integer);

	public GetChunkHeadersCommand(bool reverse)
	{
		if (reverse)
		{
			this.Command.CommandText = @"
					SELECT Key, X, Y, Z, Version
					FROM Chunks
					WHERE Key <= $FirstKey AND AssetKey = $AssetKey
					ORDER BY Key DESC
					LIMIT $Count";
		}
		else
		{
			this.Command.CommandText = @"
					SELECT Key, X, Y, Z, Version
					FROM Chunks
					WHERE Key >= $FirstKey AND AssetKey = $AssetKey
					ORDER BY Key ASC
					LIMIT $Count";
		}

		this.Command.Parameters.Add(this.parameterAssetKey);
		this.Command.Parameters.Add(this.parameterFirstKey);
		this.Command.Parameters.Add(this.parameterCount);
	}

	public void Execute(
		Guid assetKey,
		ICollection<ChunkHeader<Int2>> chunkLabels2D,
		ICollection<ChunkHeader<Int3>> chunkLabels3D,
		Guid firstKey,
		int count)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(assetKey != Guid.Empty);
		Debug.Assert(chunkLabels2D != null || chunkLabels3D != null);
		Debug.Assert(chunkLabels2D == null || chunkLabels3D == null);
		Debug.Assert(count >= 0);

		this.parameterAssetKey.Value = assetKey;
		this.parameterFirstKey.Value = firstKey;
		this.parameterCount.Value = count;

		using var reader = this.Command.ExecuteReader();
		while (reader.Read())
		{
			var key = reader.GetGuid(0);
			var x = reader.GetInt32(1);
			var y = reader.GetInt32(2);
			var z = reader.GetInt32(3);
			var version = reader.GetInt64(4);

			if (chunkLabels2D != null)
			{
				chunkLabels2D.Add(new ChunkHeader<Int2>(key, assetKey, new Int2(x, y), version));
			}
			else
			{
				chunkLabels3D.Add(new ChunkHeader<Int3>(key, assetKey, new Int3(x, y, z), version));
			}
		}
	}
}
