﻿namespace HQS.VUE.Assets.Stores;

internal class UpdateChunkCommand : AbstractSingleStoreCommand
{
	private readonly SqliteParameter parameterKey = new SqliteParameter("$Key", SqliteType.Blob);

	private readonly SqliteParameter parameterAssetKey = new SqliteParameter("$AssetKey", SqliteType.Blob);

	private readonly SqliteParameter parameterX = new SqliteParameter("$X", SqliteType.Integer);

	private readonly SqliteParameter parameterY = new SqliteParameter("$Y", SqliteType.Integer);

	private readonly SqliteParameter parameterZ = new SqliteParameter("$Z", SqliteType.Integer);

	private readonly SqliteParameter parameterVersion = new SqliteParameter("$Version", SqliteType.Integer);

	private readonly SqliteParameter parameterLength = new SqliteParameter("$Length", SqliteType.Integer);

	public UpdateChunkCommand()
	{
		this.Command.CommandText = @"
				REPLACE INTO Chunks(Key, AssetKey, X, Y, Z, Version, Data)
				VALUES ($Key, $AssetKey, $X, $Y, $Z, $Version, zeroblob($Length));

				SELECT last_insert_rowid();";

		this.Command.Parameters.Add(this.parameterKey);
		this.Command.Parameters.Add(this.parameterAssetKey);
		this.Command.Parameters.Add(this.parameterX);
		this.Command.Parameters.Add(this.parameterY);
		this.Command.Parameters.Add(this.parameterZ);
		this.Command.Parameters.Add(this.parameterVersion);
		this.Command.Parameters.Add(this.parameterLength);
	}

	public void Execute(Guid chunkKey, Guid assetKey, int x, int y, int z, long version, Stream chunkData)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(chunkKey != Guid.Empty);
		Debug.Assert(assetKey != Guid.Empty);
		Debug.Assert(version >= 0);
		Debug.Assert(chunkData != null);

		this.parameterKey.Value = chunkKey;
		this.parameterAssetKey.Value = assetKey;
		this.parameterX.Value = x;
		this.parameterY.Value = y;
		this.parameterZ.Value = z;

		this.parameterVersion.Value = version;
		this.parameterLength.Value = chunkData.Length;

		var rowid = (long)this.Command.ExecuteScalar();

		using var writeStream = new SqliteBlob(this.Command.Connection, "Chunks", "Data", rowid, readOnly: false);
		chunkData.CopyTo(writeStream);
	}
}
