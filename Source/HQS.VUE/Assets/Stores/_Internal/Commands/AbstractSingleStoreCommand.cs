﻿namespace HQS.VUE.Assets.Stores;

internal abstract class AbstractSingleStoreCommand : AbstractStoreCommand
{
	protected SqliteCommand Command { get; } = new SqliteCommand();

	protected override void HandleInitialize(SqliteConnection connection) => this.Command.Connection = connection;

	protected override void HandleDeinitialize() => this.Command.Connection = null;

	protected override void HandleDisposal() => this.Command.Dispose();
}
