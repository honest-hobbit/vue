﻿namespace HQS.VUE.Assets.Stores;

internal abstract class AbstractStoreCommand : AbstractDisposable, IDeinitializable
{
	public bool IsInitialized { get; private set; } = false;

	public void Initialize(SqliteConnection connection)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(connection != null);

		this.HandleInitialize(connection);
		this.IsInitialized = true;
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		if (this.IsInitialized)
		{
			this.HandleDeinitialize();
			this.IsInitialized = false;
		}
	}

	/// <inheritdoc />
	protected sealed override void ManagedDisposal()
	{
		this.Deinitialize();
		this.HandleDisposal();
	}

	protected abstract void HandleInitialize(SqliteConnection connection);

	protected abstract void HandleDeinitialize();

	protected abstract void HandleDisposal();
}
