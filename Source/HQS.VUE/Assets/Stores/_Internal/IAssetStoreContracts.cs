﻿namespace HQS.VUE.Assets.Stores;

// purposely using Exceptions here for public facing API instead of Requires
internal static class IAssetStoreContracts
{
	public static void GetAllAssetHeaders(
		ICollection<AssetHeader> assets, PagingArgs paging)
	{
		Ensure.That(assets, nameof(assets)).IsNotNull();
		ValidatePagingArgs(paging);
	}

	public static void GetAssetHeaders(
		string assetType, ICollection<AssetHeader> assets, PagingArgs paging)
	{
		Ensure.That(assetType, nameof(assetType)).IsNotNullOrWhiteSpace();
		Ensure.That(assets, nameof(assets)).IsNotNull();
		ValidatePagingArgs(paging);
	}

	public static void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int2>> chunks, PagingArgs paging)
	{
		Ensure.That(assetKey, nameof(assetKey)).IsNotEmpty();
		Ensure.That(chunks, nameof(chunks)).IsNotNull();
		ValidatePagingArgs(paging);
	}

	public static void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int3>> chunks, PagingArgs paging)
	{
		Ensure.That(assetKey, nameof(assetKey)).IsNotEmpty();
		Ensure.That(chunks, nameof(chunks)).IsNotNull();
		ValidatePagingArgs(paging);
	}

	public static void TryGetAsset(Guid assetKey)
	{
		Ensure.That(assetKey, nameof(assetKey)).IsNotEmpty();
	}

	public static void TryGetChunk(Guid assetKey)
	{
		Ensure.That(assetKey, nameof(assetKey)).IsNotEmpty();
	}

	public static void UpdateAsset(AssetDocument asset)
	{
		// TODO these nameofs only contain the last property
		Ensure.That(asset.Key, nameof(asset.Key)).IsNotEmpty();
		Ensure.That(asset.Header.AssetType, nameof(asset.Header.AssetType)).IsNotNullOrWhiteSpace();
		Ensure.That(asset.Header.Data, nameof(asset.Header.Data)).IsNotNull();
		Ensure.That(asset.Body, nameof(asset.Body)).IsNotNull();
	}

	public static void UpdateAsset(
		AssetDocument asset, IReadOnlyList<ChunkDocument<Int2>> chunks)
	{
		UpdateAsset(asset);
		Ensure.That(chunks, nameof(chunks)).IsNotNull();

		for (int i = 0; i < chunks.Count; i++)
		{
			var record = chunks[i];

			// TODO these nameofs only contain the last property
			Ensure.That(record.Header.Key, nameof(record.Header.Key)).IsNotEmpty();
			Ensure.That(record.Header.AssetKey, nameof(record.Header.AssetKey)).Is(asset.Key);
			Ensure.That(record.Header.Version, nameof(record.Header.Version)).IsGte(0);
			Ensure.That(record.Body, nameof(record.Body)).IsNotNull();
		}
	}

	public static void UpdateAsset(
		AssetDocument asset, IReadOnlyList<ChunkDocument<Int3>> chunks)
	{
		UpdateAsset(asset);
		Ensure.That(chunks, nameof(chunks)).IsNotNull();

		for (int i = 0; i < chunks.Count; i++)
		{
			var record = chunks[i];

			// TODO these nameofs only contain the last property
			Ensure.That(record.Header.Key, nameof(record.Header.Key)).IsNotEmpty();
			Ensure.That(record.Header.AssetKey, nameof(record.Header.AssetKey)).Is(asset.Key);
			Ensure.That(record.Header.Version, nameof(record.Header.Version)).IsGte(0);
			Ensure.That(record.Body, nameof(record.Body)).IsNotNull();
		}
	}

	public static void DeleteAsset(Guid assetKey)
	{
		Ensure.That(assetKey, nameof(assetKey)).IsNotEmpty();
	}

	private static void ValidatePagingArgs(PagingArgs paging)
	{
		Ensure.That(paging.Count, $"{nameof(paging)}.{nameof(paging.Count)}").IsGte(0);
	}
}
