﻿namespace HQS.VUE.Assets.Stores;

public class SqliteAssetStore : AbstractDisposable, IAssetStore
{
	private readonly object padlock = new object();

	private readonly TransactionCommand transaction = new TransactionCommand();

	private readonly InitializeCommand initialize = new InitializeCommand();

	private readonly GetChunkHeadersCommand getChunks = new GetChunkHeadersCommand(reverse: false);

	private readonly GetChunkHeadersCommand getChunksReversed = new GetChunkHeadersCommand(reverse: true);

	private readonly UpdateAssetCommand updateAsset = new UpdateAssetCommand();

	private readonly UpdateChunkCommand updateChunk = new UpdateChunkCommand();

	private readonly DeleteAssetCommand deleteAsset = new DeleteAssetCommand();

	private readonly DeleteAllAssetsCommand deleteAllAssets = new DeleteAllAssetsCommand();

	private readonly GetAssetHeadersCommand getAssets;

	private readonly GetAssetHeadersCommand getAssetsReversed;

	private readonly TryGetAssetCommand tryGetAsset;

	private readonly TryGetChunkCommand tryGetChunk;

	private readonly string connectionString;

	private readonly bool alwaysOpen;

	private SqliteConnection connection;

	public SqliteAssetStore(
		SqliteConnectionStringBuilder connectionStringBuilder,
		Func<Stream> streamFactory = null,
		bool alwaysOpen = false)
		: this(connectionStringBuilder?.ToString(), streamFactory, alwaysOpen)
	{
	}

	public SqliteAssetStore(
		string connectionString,
		Func<Stream> streamFactory = null,
		bool alwaysOpen = false)
	{
		Ensure.That(connectionString, nameof(connectionString)).IsNotNullOrWhiteSpace();

		this.connectionString = connectionString;
		this.alwaysOpen = alwaysOpen;

		streamFactory = PrepareStreamFactory(streamFactory);

		this.getAssets = new GetAssetHeadersCommand(streamFactory, reverse: false);
		this.getAssetsReversed = new GetAssetHeadersCommand(streamFactory, reverse: true);
		this.tryGetAsset = new TryGetAssetCommand(streamFactory);
		this.tryGetChunk = new TryGetChunkCommand(streamFactory);
	}

	public void Initialize()
	{
		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			if (this.alwaysOpen)
			{
				this.OpenConnection();
			}

			this.HandleAutoOpen();
			try
			{
				this.initialize.Execute();
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void GetAllAssetHeaders(ICollection<AssetHeader> assets, PagingArgs paging)
	{
		IAssetStoreContracts.GetAllAssetHeaders(assets, paging);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				if (paging.Reverse)
				{
					this.getAssetsReversed.Execute(
						null, assets, paging.FirstKey, paging.Count);
				}
				else
				{
					this.getAssets.Execute(
						null, assets, paging.FirstKey, paging.Count);
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void GetAssetHeaders(
		string assetType, ICollection<AssetHeader> assets, PagingArgs paging)
	{
		IAssetStoreContracts.GetAssetHeaders(assetType, assets, paging);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				if (paging.Reverse)
				{
					this.getAssetsReversed.Execute(
						assetType, assets, paging.FirstKey, paging.Count);
				}
				else
				{
					this.getAssets.Execute(
						assetType, assets, paging.FirstKey, paging.Count);
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int2>> chunks, PagingArgs paging)
	{
		IAssetStoreContracts.GetChunkHeaders(assetKey, chunks, paging);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				if (paging.Reverse)
				{
					this.getChunksReversed.Execute(
						assetKey, chunks, null, paging.FirstKey, paging.Count);
				}
				else
				{
					this.getChunks.Execute(
						assetKey, chunks, null, paging.FirstKey, paging.Count);
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void GetChunkHeaders(
		Guid assetKey, ICollection<ChunkHeader<Int3>> chunks, PagingArgs paging)
	{
		IAssetStoreContracts.GetChunkHeaders(assetKey, chunks, paging);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				if (paging.Reverse)
				{
					this.getChunksReversed.Execute(
						assetKey, null, chunks, paging.FirstKey, paging.Count);
				}
				else
				{
					this.getChunks.Execute(
						assetKey, null, chunks, paging.FirstKey, paging.Count);
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public bool TryGetAsset(
		Guid assetKey, out AssetDocument asset, bool getHeaderData, bool getBodyData)
	{
		IAssetStoreContracts.TryGetAsset(assetKey);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				return this.tryGetAsset.Execute(assetKey, out asset, getHeaderData, getBodyData);
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public bool TryGetChunk(Guid assetKey, Int2 location, out ChunkDocument<Int2> chunk)
	{
		IAssetStoreContracts.TryGetChunk(assetKey);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				if (this.tryGetChunk.Execute(
					assetKey,
					location.X,
					location.Y,
					0,
					out var chunkKey,
					out var version,
					out var chunkBody))
				{
					chunk = new ChunkDocument<Int2>(
						chunkKey, assetKey, location, version, chunkBody);
					return true;
				}
				else
				{
					chunk = default;
					return false;
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public bool TryGetChunk(Guid assetKey, Int3 location, out ChunkDocument<Int3> chunk)
	{
		IAssetStoreContracts.TryGetChunk(assetKey);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				if (this.tryGetChunk.Execute(
					assetKey,
					location.X,
					location.Y,
					location.Z,
					out var chunkKey,
					out var version,
					out var chunkBody))
				{
					chunk = new ChunkDocument<Int3>(
						chunkKey, assetKey, location, version, chunkBody);
					return true;
				}
				else
				{
					chunk = default;
					return false;
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void UpdateAsset(AssetDocument asset)
	{
		IAssetStoreContracts.UpdateAsset(asset);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				this.updateAsset.Execute(asset);
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void UpdateAsset(AssetDocument asset, IReadOnlyList<ChunkDocument<Int2>> chunks)
	{
		IAssetStoreContracts.UpdateAsset(asset, chunks);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				this.transaction.BeginTransaction();
				try
				{
					this.updateAsset.Execute(asset);

					for (int i = 0; i < chunks.Count; i++)
					{
						var record = chunks[i];
						this.updateChunk.Execute(
							record.Key,
							record.Header.AssetKey,
							record.Header.Location.X,
							record.Header.Location.Y,
							0,
							record.Header.Version,
							record.Body);
					}

					this.transaction.CommitTransaction();
				}
				catch (Exception)
				{
					this.transaction.RollbackTransaction();
					throw;
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void UpdateAsset(AssetDocument asset, IReadOnlyList<ChunkDocument<Int3>> chunks)
	{
		IAssetStoreContracts.UpdateAsset(asset, chunks);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				this.transaction.BeginTransaction();
				try
				{
					this.updateAsset.Execute(asset);

					for (int i = 0; i < chunks.Count; i++)
					{
						var record = chunks[i];
						this.updateChunk.Execute(
							record.Key,
							record.Header.AssetKey,
							record.Header.Location.X,
							record.Header.Location.Y,
							record.Header.Location.Z,
							record.Header.Version,
							record.Body);
					}

					this.transaction.CommitTransaction();
				}
				catch (Exception)
				{
					this.transaction.RollbackTransaction();
					throw;
				}
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void DeleteAsset(Guid assetKey)
	{
		IAssetStoreContracts.DeleteAsset(assetKey);

		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				this.deleteAsset.Execute(assetKey);
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	public void DeleteAllAssets()
	{
		lock (this.padlock)
		{
			this.ValidateNotDisposed();

			this.HandleAutoOpen();
			try
			{
				this.deleteAllAssets.Execute();
			}
			finally
			{
				this.HandleAutoClose();
			}
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		lock (this.padlock)
		{
			this.transaction.Dispose();
			this.initialize.Dispose();

			this.getAssets.Dispose();
			this.getAssetsReversed.Dispose();

			this.getChunks.Dispose();
			this.getChunksReversed.Dispose();

			this.tryGetAsset.Dispose();
			this.updateAsset.Dispose();

			this.tryGetChunk.Dispose();
			this.updateChunk.Dispose();

			this.deleteAsset.Dispose();
			this.deleteAllAssets.Dispose();

			this.connection?.Dispose();
			this.connection = null;
		}
	}

	private static Func<Stream> PrepareStreamFactory(Func<Stream> streamFactory)
	{
		if (streamFactory == null)
		{
			return () => new MemoryStream();
		}
		else
		{
			return () =>
			{
				var stream = streamFactory();
				if (stream == null)
				{
					throw new ArgumentNullException(
						nameof(stream), $"Func<Stream> {nameof(streamFactory)} must not return a null Stream.");
				}

				return stream;
			};
		}
	}

	private void HandleAutoOpen()
	{
		if (this.alwaysOpen)
		{
			return;
		}

		this.OpenConnection();
	}

	private void HandleAutoClose()
	{
		if (this.alwaysOpen)
		{
			return;
		}

		this.CloseConnection();
	}

	private void OpenConnection()
	{
		if (this.connection != null)
		{
			return;
		}

		this.connection = new SqliteConnection(this.connectionString);

		this.transaction.Initialize(this.connection);
		this.initialize.Initialize(this.connection);

		this.getAssets.Initialize(this.connection);
		this.getAssetsReversed.Initialize(this.connection);

		this.getChunks.Initialize(this.connection);
		this.getChunksReversed.Initialize(this.connection);

		this.tryGetAsset.Initialize(this.connection);
		this.updateAsset.Initialize(this.connection);

		this.tryGetChunk.Initialize(this.connection);
		this.updateChunk.Initialize(this.connection);

		this.deleteAsset.Initialize(this.connection);
		this.deleteAllAssets.Initialize(this.connection);

		this.connection.Open();
	}

	private void CloseConnection()
	{
		if (this.connection == null)
		{
			return;
		}

		this.transaction.Deinitialize();
		this.initialize.Deinitialize();

		this.getAssets.Deinitialize();
		this.getAssetsReversed.Deinitialize();

		this.getChunks.Deinitialize();
		this.getChunksReversed.Deinitialize();

		this.tryGetAsset.Deinitialize();
		this.updateAsset.Deinitialize();

		this.tryGetChunk.Deinitialize();
		this.updateChunk.Deinitialize();

		this.deleteAsset.Deinitialize();
		this.deleteAllAssets.Deinitialize();

		this.connection.Dispose();
		this.connection = null;
	}
}
