﻿namespace HQS.VUE.Neo;

// TODO temporary prototyping purposes only
internal static class SurfacePlaneFillExtensions
{
	public static void FillArea(
		this SurfacePlane<Surface16> surfaces, Int2 origin, Int2 dimensions, Surface16 value) =>
		surfaces.FillArea(origin, dimensions, _ => value);

	public static void FillArea(
		this SurfacePlane<Surface16> surfaces, Int2 origin, Int2 dimensions, Func<Int2, Surface16> getValue)
	{
		Ensure.That(surfaces, nameof(surfaces)).IsNotNull();
		Ensure.That(getValue, nameof(getValue)).IsNotNull();

		var max = origin + dimensions;
		for (int iX = origin.X; iX < max.X; iX++)
		{
			for (int iY = origin.Y; iY < max.Y; iY++)
			{
				surfaces[iX, iY] = new SurfaceQuad<Surface16>(getValue(new Int2(iX, iY)));
			}
		}
	}

	public static void FillArea(
		this SurfacePlane<Surface16> surfaces, Int2 origin, Int2 dimensions, SurfaceQuad<Surface16> value) =>
		surfaces.FillArea(origin, dimensions, _ => value);

	public static void FillArea(
		this SurfacePlane<Surface16> surfaces, Int2 origin, Int2 dimensions, Func<Int2, SurfaceQuad<Surface16>> getValue)
	{
		Ensure.That(surfaces, nameof(surfaces)).IsNotNull();
		Ensure.That(getValue, nameof(getValue)).IsNotNull();

		var max = origin + dimensions;
		for (int iX = origin.X; iX < max.X; iX++)
		{
			for (int iY = origin.Y; iY < max.Y; iY++)
			{
				surfaces[iX, iY] = getValue(new Int2(iX, iY));
			}
		}
	}

	public static void FillNoise(
		this SurfacePlane<Surface16> surfaces,
		Int2 origin,
		Int2 dimensions,
		float depth,
		FastNoiseLite noise,
		Func<float, Surface16> getValue)
	{
		Ensure.That(surfaces, nameof(surfaces)).IsNotNull();
		Ensure.That(noise, nameof(noise)).IsNotNull();
		Ensure.That(getValue, nameof(getValue)).IsNotNull();

		var max = origin + dimensions;
		for (int iX = origin.X; iX < max.X; iX++)
		{
			for (int iY = origin.Y; iY < max.Y; iY++)
			{
				surfaces[iX, iY] = new SurfaceQuad<Surface16>()
				{
					Left = getValue(noise.GetNoise(iX - .25f, iY, depth)),
					Right = getValue(noise.GetNoise(iX + .25f, iY, depth)),
					Bottom = getValue(noise.GetNoise(iX, iY - .25f, depth)),
					Top = getValue(noise.GetNoise(iX, iY + .25f, depth)),
				};
			}
		}
	}
}
