﻿namespace HQS.VUE.Neo;

// TODO temporary prototyping purposes only
internal class SurfacePlaneFiller
{
	public SurfacePattern Pattern { get; set; } = SurfacePattern.Empty;

	public ushort SurfaceEmpty { get; set; } = 0;

	public ushort SurfaceTypeA { get; set; } = 0;

	public ushort SurfaceTypeB { get; set; } = 1;

	public ushort SurfaceTypeC { get; set; } = 2;

	public ushort SurfaceTypeD { get; set; } = 3;

	public ushort SurfaceTypeE { get; set; } = 4;

	public int NoiseSeed { get; set; } = 0;

	public float NoiseDepth { get; set; } = 0;

	public float NoiseFrequency { get; set; } = .1f;

	public float NoiseWeight { get; set; } = 1f;

	public int NoiseCountOfVoxelTypes { get; set; } = 1;

	public void FillPlane(SurfacePlane<Surface16> surfaces)
	{
		Ensure.That(surfaces, nameof(surfaces)).IsNotNull();

		surfaces.SetAllTo(this.SurfaceEmpty);

		var origin = new Int2(0, 0);
		var dimensions = new Int2(surfaces.Indexer.SideLength);

		switch (this.Pattern)
		{
			case SurfacePattern.Empty:
				break;

			case SurfacePattern.Uniform:
				surfaces.FillArea(origin, dimensions, this.SurfaceTypeB);
				break;

			case SurfacePattern.Quadrants:
				var half = dimensions / 2;
				surfaces.FillArea(origin, half, this.SurfaceTypeB);
				surfaces.FillArea(new Int2(half.X, 0), half, this.SurfaceTypeC);
				surfaces.FillArea(new Int2(0, half.Y), half, this.SurfaceTypeD);
				surfaces.FillArea(half, half, this.SurfaceTypeE);
				break;

			case SurfacePattern.Checkered:
				surfaces.FillArea(origin, dimensions, p =>
					(p.X + p.Y).IsOdd() ? this.SurfaceTypeB : this.SurfaceTypeC);
				break;

			case SurfacePattern.Diamonds:
				surfaces.FillArea(origin, dimensions, new SurfaceQuad<Surface16>(
					this.SurfaceTypeB, this.SurfaceTypeC, this.SurfaceTypeB, this.SurfaceTypeC));
				break;

			case SurfacePattern.Triangles:
				surfaces.FillArea(origin, dimensions, new SurfaceQuad<Surface16>(
					this.SurfaceTypeB, this.SurfaceTypeC, this.SurfaceTypeD, this.SurfaceTypeE));
				break;

			case SurfacePattern.Pentagons:
				surfaces.FillArea(origin, dimensions, p => (p.X + p.Y).IsOdd() ?
				new SurfaceQuad<Surface16>(this.SurfaceTypeB) :
				new SurfaceQuad<Surface16>(this.SurfaceTypeC, this.SurfaceTypeC, this.SurfaceTypeB, this.SurfaceTypeC));
				break;

			case SurfacePattern.Grid:
				surfaces.FillArea(origin, dimensions, p =>
					(p.X.IsOdd() && p.Y.IsOdd() ? this.SurfaceTypeA : this.SurfaceTypeB));
				break;

			case SurfacePattern.SpinesVertical:
				surfaces.FillArea(origin, dimensions, p =>
					((p.X.IsOdd() && p.Y.IsOdd()) || (p.X % 4 == 0 && p.Y > 0) ? this.SurfaceTypeA : this.SurfaceTypeB));
				break;

			case SurfacePattern.SpinesHorizontal:
				surfaces.FillArea(origin, dimensions, p =>
					((p.X.IsOdd() && p.Y.IsOdd()) || (p.X > 0 && p.Y % 4 == 0) ? this.SurfaceTypeA : this.SurfaceTypeB));
				break;

			case SurfacePattern.Noise:
				var noise = new FastNoiseLite();
				noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
				noise.SetFrequency(this.NoiseFrequency);
				noise.SetSeed(this.NoiseSeed);
				surfaces.FillNoise(origin, dimensions, this.NoiseDepth, noise, value =>
					(ushort)((value * this.NoiseCountOfVoxelTypes) + this.NoiseWeight).Clamp(0, this.NoiseCountOfVoxelTypes));
				break;

			case SurfacePattern.Special_Bug:
				this.FillSpecialBug(surfaces);
				break;

			case SurfacePattern.Special_ConcaveQuadsA:
				this.FillSpecialConcaveQuadsA(surfaces);
				break;

			case SurfacePattern.Special_ConcaveQuadsB:
				this.FillSpecialConcaveQuadsB(surfaces);
				break;
		}
	}

	private void FillSpecialBug(SurfacePlane<Surface16> surfaces)
	{
		Debug.Assert(surfaces != null);

		Ensure.That(surfaces.Size.Exponent, $"{nameof(surfaces)}.{nameof(surfaces.Size)}").IsGte(2);

		for (int iX = 0; iX < 4; iX++)
		{
			for (int iY = 0; iY < 4; iY++)
			{
				surfaces[iX, iY] = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
			}
		}

		surfaces[1, 1] = new SurfaceQuad<Surface16>(this.SurfaceTypeB)
		{
			Top = this.SurfaceTypeC,
		};

		surfaces[1, 2] = new SurfaceQuad<Surface16>(this.SurfaceTypeB)
		{
			Bottom = this.SurfaceTypeC,
		};

		surfaces[2, 1] = new SurfaceQuad<Surface16>(this.SurfaceTypeC)
		{
			Bottom = this.SurfaceTypeB,
		};

		surfaces[2, 2] = new SurfaceQuad<Surface16>(this.SurfaceTypeC)
		{
			Top = this.SurfaceTypeB,
		};
	}

	private void FillSpecialConcaveQuadsA(SurfacePlane<Surface16> surfaces)
	{
		Debug.Assert(surfaces != null);

		Ensure.That(surfaces.Size.Exponent, $"{nameof(surfaces)}.{nameof(surfaces.Size)}").IsGte(2);

		for (int iX = 0; iX < 4; iX++)
		{
			for (int iY = 0; iY < 4; iY++)
			{
				surfaces[iX, iY] = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
			}
		}

		var quad = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
		quad.Left = this.SurfaceTypeC;
		quad.Bottom = this.SurfaceTypeC;

		surfaces[0, 2] = quad;
		surfaces[1, 1] = quad;
		surfaces[3, 0] = quad;
		surfaces[2, 3] = quad;
		surfaces[3, 2] = quad;

		quad = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
		quad.Right = this.SurfaceTypeC;
		quad.Bottom = this.SurfaceTypeC;

		surfaces[1, 2] = quad;
		surfaces[1, 0] = quad;
		surfaces[2, 1] = quad;

		quad = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
		quad.Left = this.SurfaceTypeC;
		quad.Top = this.SurfaceTypeC;

		surfaces[0, 0] = quad;

		quad = new SurfaceQuad<Surface16>(this.SurfaceTypeC);

		surfaces[0, 1] = quad;
		surfaces[2, 0] = quad;
		surfaces[2, 2] = quad;
	}

	private void FillSpecialConcaveQuadsB(SurfacePlane<Surface16> surfaces)
	{
		Debug.Assert(surfaces != null);

		Ensure.That(surfaces.Size.Exponent, $"{nameof(surfaces)}.{nameof(surfaces.Size)}").IsGte(2);

		for (int iX = 0; iX < 4; iX++)
		{
			for (int iY = 0; iY < 4; iY++)
			{
				surfaces[iX, iY] = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
			}
		}

		var quad = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
		quad.Left = this.SurfaceTypeC;
		quad.Bottom = this.SurfaceTypeC;

		surfaces[0, 2] = quad;
		surfaces[1, 1] = quad;
		surfaces[3, 0] = quad;

		quad = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
		quad.Right = this.SurfaceTypeC;
		quad.Bottom = this.SurfaceTypeC;

		surfaces[1, 0] = quad;
		surfaces[2, 1] = quad;
		surfaces[3, 3] = quad;

		quad = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
		quad.Right = this.SurfaceTypeC;
		quad.Top = this.SurfaceTypeC;

		surfaces[0, 3] = quad;
		surfaces[2, 2] = quad;
		surfaces[3, 1] = quad;

		quad = new SurfaceQuad<Surface16>(this.SurfaceTypeB);
		quad.Left = this.SurfaceTypeC;
		quad.Top = this.SurfaceTypeC;

		surfaces[0, 0] = quad;
		surfaces[1, 2] = quad;
		surfaces[2, 3] = quad;

		quad = new SurfaceQuad<Surface16>(this.SurfaceTypeC);
		surfaces[0, 1] = quad;
		surfaces[1, 3] = quad;
		surfaces[2, 0] = quad;
		surfaces[3, 2] = quad;
	}
}
