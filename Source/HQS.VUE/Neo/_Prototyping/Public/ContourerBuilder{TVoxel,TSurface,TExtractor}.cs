﻿namespace HQS.VUE.Neo;

public class ContourerBuilder<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	private readonly CubeArrayPools<TVoxel> cubeArrayPools;

	private readonly WedgeArrayPools<TVoxel> wedgeArrayPools;

	private readonly IPool<Page<PlaneGroup>> planePages;

	private readonly IPool<Page<SurfaceGroup<TSurface>>> surfacePages;

	private readonly IPool<Page<CompactTriangle>> trianglePages;

	private readonly IPool<DualPlanes<TSurface, TExtractor>> dualPlanesPool;

	// TODO should this take a Min and Max ChunkSize? CubeArrayPools supports it.
	// DualPlanesContourerPool would need to be updated to support multiple sizes too.
	public ContourerBuilder(
		ChunkSize size, TExtractor extractor, ContourerBuilderOptions? options = null)
	{
		size.Validate(nameof(size));
		Ensure.That(extractor, nameof(extractor)).HasValue();

		var opts = options ??= ContourerBuilderOptions.Preset;
		opts.Validate();

		this.Size = size;
		this.Extractor = extractor;
		this.Options = opts;

		this.cubeArrayPools = new CubeArrayPools<TVoxel>(
			length => new ThreadSafePool<TVoxel[]>(() => new TVoxel[length]));

		this.CubeChunkPools = new CubeChunkPools<TVoxel>(
			new ThreadSafePool<CubeChunk<TVoxel>.Builder>(
				() => new CubeChunk<TVoxel>.Builder(this.cubeArrayPools)),
			this.cubeArrayPools);

		this.wedgeArrayPools = new WedgeArrayPools<TVoxel>(
			length => new ThreadSafePool<Wedge<TVoxel>[]>(() => new Wedge<TVoxel>[length]));

		this.WedgeChunkPools = new WedgeChunkPools<TVoxel>(
			new ThreadSafePool<WedgeChunk<TVoxel>.Builder>(
				() => new WedgeChunk<TVoxel>.Builder(this.wedgeArrayPools)),
			this.wedgeArrayPools);

		this.planePages = new ThreadSafePool<Page<PlaneGroup>>(
			() => new Page<PlaneGroup>(this.Options.PlanesPageCapacity));
		this.surfacePages = new ThreadSafePool<Page<SurfaceGroup<TSurface>>>(
			() => new Page<SurfaceGroup<TSurface>>(this.Options.SurfacesPageCapacity));
		this.trianglePages = new ThreadSafePool<Page<CompactTriangle>>(
			() => new Page<CompactTriangle>(this.Options.TrianglesPageCapacity));

		var coplanarChunkBuilders = new ThreadSafePool<CoplanarChunk<TSurface>.Builder>(
			() => new CoplanarChunk<TSurface>.Builder(this.planePages, this.surfacePages, this.trianglePages));

		this.CoplanarChunkPools = new CoplanarChunkPools<TSurface>(
			coplanarChunkBuilders, this.planePages, this.surfacePages, this.trianglePages);

		this.dualPlanesPool = new ThreadSafePool<DualPlanes<TSurface, TExtractor>>(
			() => new DualPlanes<TSurface, TExtractor>(
				this.CreatePlaneContourerForDualPlanes(),
				new SurfacePlane<TSurface>(this.Size),
				new SurfacePlane<TSurface>(this.Size),
				this.CreateCoplanarChunkBuilder(),
				this.CreateCoplanarChunkBuilder()));
	}

	public ChunkSize Size { get; }

	public TExtractor Extractor { get; }

	public ContourerBuilderOptions Options { get; }

	public CubeChunkPools<TVoxel> CubeChunkPools { get; }

	public WedgeChunkPools<TVoxel> WedgeChunkPools { get; }

	public CoplanarChunkPools<TSurface> CoplanarChunkPools { get; }

	public IPoolControls DualPlanes => this.dualPlanesPool;

	public CubeChunk<TVoxel>.Builder CreateCubeChunkBuilder() =>
		new CubeChunk<TVoxel>.Builder(this.cubeArrayPools);

	public WedgeChunk<TVoxel>.Builder CreateWedgeChunkBuilder() =>
		new WedgeChunk<TVoxel>.Builder(this.wedgeArrayPools);

	public CoplanarChunk<TSurface>.Builder CreateCoplanarChunkBuilder() =>
		new CoplanarChunk<TSurface>.Builder(this.planePages, this.surfacePages, this.trianglePages);

	public ICubeChunkContourer<TVoxel, TSurface, TExtractor> CreateCubeChunkContourer() =>
		new CubeChunkContourer<TVoxel, TSurface, TExtractor>(
			this.Size,
			this.dualPlanesPool,
			this.CreateCoplanarChunkBuilder())
		{
			Extractor = this.Extractor,
		};

	public IWedgeChunkContourer<TVoxel, TSurface, TExtractor> CreateWedgeChunkContourer() =>
		new WedgeChunkContourer<TVoxel, TSurface, TExtractor>(
			this.Size,
			this.dualPlanesPool,
			this.CreateCoplanarChunkBuilder())
		{
			Extractor = this.Extractor,
		};

	// TODO temporary prototyping purposes only
	internal IPlaneContourer<TSurface, TExtractor> CreatePlaneContourer()
	{
		if (this.Options.UseSimpleContourer)
		{
			return new SimplePlaneContourer<TSurface, TExtractor>(this.Size)
			{
				Surfaces = new SurfacePlane<TSurface>(this.Size),
				Comparer = this.Extractor,
				Output = this.CreateCoplanarChunkBuilder(),
			};
		}
		else
		{
			return new TessalatePlaneContourer<TSurface, TExtractor>(
				this.Size, new LibTessDotNet.Pools(), this.Options.ContourerTrianglesCapacity)
			{
				Surfaces = new SurfacePlane<TSurface>(this.Size),
				Comparer = this.Extractor,
				Output = this.CreateCoplanarChunkBuilder(),
			};
		}
	}

	private IPlaneContourer<TSurface, TExtractor> CreatePlaneContourerForDualPlanes()
	{
		if (this.Options.UseSimpleContourer)
		{
			return new SimplePlaneContourer<TSurface, TExtractor>(this.Size);
		}
		else
		{
			return new TessalatePlaneContourer<TSurface, TExtractor>(
				this.Size, new LibTessDotNet.Pools(), this.Options.ContourerTrianglesCapacity);
		}
	}
}
