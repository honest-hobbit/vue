﻿namespace HQS.VUE.Neo;

[SuppressMessage("StyleCop", "SA1313", Justification = "Record syntax.")]
public record struct ContourerBuilderOptions(
	bool UseSimpleContourer,
	int ContourerTrianglesCapacity,
	int PlanesPageCapacity,
	int SurfacesPageCapacity,
	int TrianglesPageCapacity)
{
	public static ContourerBuilderOptions Preset => new ContourerBuilderOptions(
		false, 128 * 128, 16, 16, 256);

	public ContourerBuilderOptions With(bool useSimpleContourer)
	{
		var result = this;
		result.UseSimpleContourer = useSimpleContourer;
		return result;
	}

	public void Validate()
	{
		Ensure.That(this.ContourerTrianglesCapacity, nameof(this.ContourerTrianglesCapacity)).IsGte(0);
		Ensure.That(this.PlanesPageCapacity, nameof(this.PlanesPageCapacity)).IsGte(1);
		Ensure.That(this.SurfacesPageCapacity, nameof(this.SurfacesPageCapacity)).IsGte(1);
		Ensure.That(this.TrianglesPageCapacity, nameof(this.TrianglesPageCapacity)).IsGte(1);
	}
}
