﻿namespace HQS.VUE.Neo;

public static class ContourerBuilder
{
	public static ContourerBuilder<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>> CreatePalette(
		ChunkSize size, bool useSimpleContourer) =>
		new ContourerBuilder<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>>(
			size,
			CreatePalette(),
			ContourerBuilderOptions.Preset.With(useSimpleContourer));

	public static ContourerBuilder<Voxel16, Surface16, Voxel16SurfaceExtractor> Create(
		ChunkSize size, bool useSimpleContourer) =>
		new ContourerBuilder<Voxel16, Surface16, Voxel16SurfaceExtractor>(
			size,
			Voxel16SurfaceExtractor.Default,
			ContourerBuilderOptions.Preset.With(useSimpleContourer));

	public static ContourerBuilder<Voxel16, Surface16, Voxel16SurfaceExtractorClass> CreateClass(
		ChunkSize size, bool useSimpleContourer) =>
		new ContourerBuilder<Voxel16, Surface16, Voxel16SurfaceExtractorClass>(
			size,
			Voxel16SurfaceExtractorClass.Default,
			ContourerBuilderOptions.Preset.With(useSimpleContourer));

	public static ContourerBuilder<Voxel16, Surface16, ISurfaceExtractor<Voxel16, Surface16>> CreateBoxed(
		ChunkSize size, bool useSimpleContourer) =>
		new ContourerBuilder<Voxel16, Surface16, ISurfaceExtractor<Voxel16, Surface16>>(
			size,
			Voxel16SurfaceExtractor.Default,
			ContourerBuilderOptions.Preset.With(useSimpleContourer));

	public static ContourerBuilder<Voxel16, Surface16, ISurfaceExtractor<Voxel16, Surface16>> CreateInterface(
		ChunkSize size, bool useSimpleContourer) =>
		new ContourerBuilder<Voxel16, Surface16, ISurfaceExtractor<Voxel16, Surface16>>(
			size,
			Voxel16SurfaceExtractorClass.Default,
			ContourerBuilderOptions.Preset.With(useSimpleContourer));

	private static VoxelPalette<Voxel16, Surface16> CreatePalette()
	{
		var paletteBuilder = new VoxelPalette<Voxel16, Surface16>.Builder(8, ushort.MaxValue);

		int empty = 0;
		int solid = 1;
		int trans = 2;

		paletteBuilder.SurfaceCategoryNames[empty] = "Empty";
		paletteBuilder.SurfaceCategoryNames[solid] = "Solid";
		paletteBuilder.SurfaceCategoryNames[trans] = "Transparent";

		// voxel type 0 is empty air
		var typeBuilder = default(VoxelTypeBuilder<Surface16>);
		typeBuilder.Name = "Empty";
		typeBuilder.SurfaceCategoryBits[empty] = true;
		typeBuilder.SurfaceType = new Surface16(0);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		// voxel type 1 is solid and has internal surfaces
		typeBuilder = default;
		typeBuilder.SurfaceCategoryBits[solid] = true;
		typeBuilder.IsSurfacePresentBits[empty] = true;
		typeBuilder.IsSurfacePresentBits[trans] = true;
		typeBuilder.HasInternalSurfaces = true;
		////typeBuilder.IsSurfacePresentBits[solid] = true;

		typeBuilder.Name = "Type 1";
		typeBuilder.SurfaceType = new Surface16(1);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		// voxel type 2 is transparent
		typeBuilder = default;
		typeBuilder.SurfaceCategoryBits[trans] = true;
		typeBuilder.IsSurfacePresentBits[empty] = true;

		typeBuilder.Name = "Type 2";
		typeBuilder.SurfaceType = new Surface16(2);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		// voxel type 3 and above are solid
		typeBuilder = default;
		typeBuilder.SurfaceCategoryBits[solid] = true;
		typeBuilder.IsSurfacePresentBits[empty] = true;
		typeBuilder.IsSurfacePresentBits[trans] = true;
		////typeBuilder.IsSurfacePresentBits[solid] = true;

		typeBuilder.Name = "Type 3";
		typeBuilder.SurfaceType = new Surface16(3);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		typeBuilder.Name = "Type 4";
		typeBuilder.SurfaceType = new Surface16(4);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		typeBuilder.Name = "Type 5";
		typeBuilder.SurfaceType = new Surface16(5);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		typeBuilder.Name = "Type 6";
		typeBuilder.SurfaceType = new Surface16(6);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		typeBuilder.Name = "Type 7";
		typeBuilder.SurfaceType = new Surface16(7);
		paletteBuilder.VoxelTypes.Add(typeBuilder);

		return paletteBuilder.BuildPalette();
	}
}
