﻿namespace HQS.VUE.Neo;

public interface IChunkFiller<TChunk>
	where TChunk : class
{
	void FillChunk(TChunk chunk);
}
