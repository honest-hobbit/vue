﻿using static HQS.VUE.Neo.WedgeCorners;

namespace HQS.VUE.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[Serializable]
public class FancyNoiseWedgeChunkFiller : IChunkFiller<WedgeChunk<Voxel16>.Builder>
{
	public enum MaterialMode
	{
		Single,

		Multiple,
	}

	[NonSerialized]
	public WedgeTable Table;

	public MaterialMode Mode;

	public ushort VoxelTypeA = 1;

	public ushort VoxelTypeB = 2;

	public ushort VoxelTypeC = 3;

	public int Seed = 0;

	public float Frequency = .1f;

	public float Weight = 1f;

	public void FillChunk(WedgeChunk<Voxel16>.Builder chunk)
	{
		IChunkFillerContracts.FillChunk(chunk);
		Ensure.That(this.Table, nameof(this.Table)).IsNotNull();

		switch (this.Mode)
		{
			case MaterialMode.Single: this.FillChunkSingleMode(chunk); break;
			case MaterialMode.Multiple: this.FillChunkMultipleMode(chunk); break;
		}
	}

	private static WedgeCorners GetEnclosed(WedgeCorners corners)
	{
		var result = None;
		if (corners.Has(NNN | PNN | NPN | NNP)) { result |= NNN; }
		if (corners.Has(NNP | PNP | NPP | NNN)) { result |= NNP; }
		if (corners.Has(NPN | PPN | NNN | NPP)) { result |= NPN; }
		if (corners.Has(NPP | PPP | NNP | NPN)) { result |= NPP; }
		if (corners.Has(PNN | NNN | PPN | PNP)) { result |= PNN; }
		if (corners.Has(PNP | NNP | PPP | PNN)) { result |= PNP; }
		if (corners.Has(PPN | NPN | PNN | PPP)) { result |= PPN; }
		if (corners.Has(PPP | NPP | PNP | PPN)) { result |= PPP; }
		return result;
	}

	private void FillChunkSingleMode(WedgeChunk<Voxel16>.Builder chunk)
	{
		Debug.Assert(chunk != null);

		chunk.IsUniform = false;

		var noise = new FastNoiseLite();
		noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise.SetFrequency(this.Frequency);
		noise.SetSeed(this.Seed);

		var voxels = chunk.Voxels;
		var lower = voxels.Indexer.LowerBounds;
		var upper = voxels.Indexer.UpperBounds;

		float lo = 0f;
		float hi = 1 - lo;

		for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
		{
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				for (int iY = lower.Y; iY <= upper.Y; iY++)
				{
					var sharedCorners =
						(noise.GetNoise(iX + lo, iY + lo, iZ + lo) + 1 <= this.Weight ? NNN : None) |
						(noise.GetNoise(iX + hi, iY + lo, iZ + lo) + 1 <= this.Weight ? PNN : None) |
						(noise.GetNoise(iX + lo, iY + hi, iZ + lo) + 1 <= this.Weight ? NPN : None) |
						(noise.GetNoise(iX + hi, iY + hi, iZ + lo) + 1 <= this.Weight ? PPN : None) |
						(noise.GetNoise(iX + lo, iY + lo, iZ + hi) + 1 <= this.Weight ? NNP : None) |
						(noise.GetNoise(iX + hi, iY + lo, iZ + hi) + 1 <= this.Weight ? PNP : None) |
						(noise.GetNoise(iX + lo, iY + hi, iZ + hi) + 1 <= this.Weight ? NPP : None) |
						(noise.GetNoise(iX + hi, iY + hi, iZ + hi) + 1 <= this.Weight ? PPP : None);

					var shapeIndex = this.Table.GetWedgeByFilledCorners(sharedCorners);
					if (shapeIndex.Index == 0) { continue; }
					var type = this.Table.GetDefinition(shapeIndex).Type;
					if (type == WedgeType.Face || type == WedgeType.FaceSplit) { continue; }

					var voxelType = this.VoxelTypeA;

					float weightB = this.Weight * 1.2f;
					noise.SetSeed(this.Seed + 1);
					if (noise.GetNoise(iX + .5f, iY + .5f, iZ + .5f) + 1 <= weightB)
					{
						voxelType = this.VoxelTypeB;
					}
					else
					{
						float weightC = this.Weight * 1.2f;
						noise.SetSeed(this.Seed + 2);
						if (noise.GetNoise(iX + .5f, iY + .5f, iZ + .5f) + 1 <= weightC)
						{
							voxelType = this.VoxelTypeC;
						}
					}

					voxels[iX, iY, iZ][0] = new Wedge<Voxel16>(shapeIndex, voxelType);
					noise.SetSeed(this.Seed);
				}
			}
		}
	}

	private void FillChunkMultipleMode(WedgeChunk<Voxel16>.Builder chunk)
	{
		Debug.Assert(chunk != null);

		chunk.IsUniform = false;
		chunk.SetChannels(3);

		var noise = new FastNoiseLite();
		noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise.SetFrequency(this.Frequency);
		noise.SetSeed(this.Seed);

		var voxels = chunk.Voxels;
		var lower = voxels.Indexer.LowerBounds;
		var upper = voxels.Indexer.UpperBounds;

		float lo = 0f;
		float hi = 1 - lo;

		for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
		{
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				for (int iY = lower.Y; iY <= upper.Y; iY++)
				{
					var sharedCorners =
						(noise.GetNoise(iX + lo, iY + lo, iZ + lo) + 1 <= this.Weight ? NNN : None) |
						(noise.GetNoise(iX + hi, iY + lo, iZ + lo) + 1 <= this.Weight ? PNN : None) |
						(noise.GetNoise(iX + lo, iY + hi, iZ + lo) + 1 <= this.Weight ? NPN : None) |
						(noise.GetNoise(iX + hi, iY + hi, iZ + lo) + 1 <= this.Weight ? PPN : None) |
						(noise.GetNoise(iX + lo, iY + lo, iZ + hi) + 1 <= this.Weight ? NNP : None) |
						(noise.GetNoise(iX + hi, iY + lo, iZ + hi) + 1 <= this.Weight ? PNP : None) |
						(noise.GetNoise(iX + lo, iY + hi, iZ + hi) + 1 <= this.Weight ? NPP : None) |
						(noise.GetNoise(iX + hi, iY + hi, iZ + hi) + 1 <= this.Weight ? PPP : None);

					var shapeIndex = this.Table.GetWedgeByFilledCorners(sharedCorners);
					if (shapeIndex.Index == 0) { continue; }
					var type = this.Table.GetDefinition(shapeIndex).Type;
					if (type == WedgeType.Face || type == WedgeType.FaceSplit) { continue; }

					int wedgeIndex = 0;

					float weightB = this.Weight * 1.4f;
					noise.SetSeed(this.Seed + 1);
					var materialCornersB =
						(sharedCorners.Has(NNN) && noise.GetNoise(iX + lo, iY + lo, iZ + lo) + 1 <= weightB ? NNN : None) |
						(sharedCorners.Has(PNN) && noise.GetNoise(iX + hi, iY + lo, iZ + lo) + 1 <= weightB ? PNN : None) |
						(sharedCorners.Has(NPN) && noise.GetNoise(iX + lo, iY + hi, iZ + lo) + 1 <= weightB ? NPN : None) |
						(sharedCorners.Has(PPN) && noise.GetNoise(iX + hi, iY + hi, iZ + lo) + 1 <= weightB ? PPN : None) |
						(sharedCorners.Has(NNP) && noise.GetNoise(iX + lo, iY + lo, iZ + hi) + 1 <= weightB ? NNP : None) |
						(sharedCorners.Has(PNP) && noise.GetNoise(iX + hi, iY + lo, iZ + hi) + 1 <= weightB ? PNP : None) |
						(sharedCorners.Has(NPP) && noise.GetNoise(iX + lo, iY + hi, iZ + hi) + 1 <= weightB ? NPP : None) |
						(sharedCorners.Has(PPP) && noise.GetNoise(iX + hi, iY + hi, iZ + hi) + 1 <= weightB ? PPP : None);

					if (materialCornersB != None)
					{
						var shapeIndexB = this.Table.GetWedgeByFilledCorners(materialCornersB);
						if (shapeIndexB.Index != 0)
						{
							var typeB = this.Table.GetDefinition(shapeIndexB).Type;
							if (typeB != WedgeType.Face && typeB != WedgeType.FaceSplit)
							{
								voxels[iX, iY, iZ][wedgeIndex] = new Wedge<Voxel16>(shapeIndexB, this.VoxelTypeB);
								wedgeIndex++;
								sharedCorners = sharedCorners.Remove(GetEnclosed(materialCornersB));
							}
						}
					}

					float weightC = this.Weight * 1.4f;
					noise.SetSeed(this.Seed + 2);
					var materialCornersC =
						(sharedCorners.Has(NNN) && noise.GetNoise(iX + lo, iY + lo, iZ + lo) + 1 <= weightC ? NNN : None) |
						(sharedCorners.Has(PNN) && noise.GetNoise(iX + hi, iY + lo, iZ + lo) + 1 <= weightC ? PNN : None) |
						(sharedCorners.Has(NPN) && noise.GetNoise(iX + lo, iY + hi, iZ + lo) + 1 <= weightC ? NPN : None) |
						(sharedCorners.Has(PPN) && noise.GetNoise(iX + hi, iY + hi, iZ + lo) + 1 <= weightC ? PPN : None) |
						(sharedCorners.Has(NNP) && noise.GetNoise(iX + lo, iY + lo, iZ + hi) + 1 <= weightC ? NNP : None) |
						(sharedCorners.Has(PNP) && noise.GetNoise(iX + hi, iY + lo, iZ + hi) + 1 <= weightC ? PNP : None) |
						(sharedCorners.Has(NPP) && noise.GetNoise(iX + lo, iY + hi, iZ + hi) + 1 <= weightC ? NPP : None) |
						(sharedCorners.Has(PPP) && noise.GetNoise(iX + hi, iY + hi, iZ + hi) + 1 <= weightC ? PPP : None);

					if (materialCornersC != None)
					{
						var shapeIndexC = this.Table.GetWedgeByFilledCorners(materialCornersC);
						if (shapeIndexC.Index != 0)
						{
							var typeC = this.Table.GetDefinition(shapeIndexC).Type;
							if (typeC != WedgeType.Face && typeC != WedgeType.FaceSplit)
							{
								voxels[iX, iY, iZ][wedgeIndex] = new Wedge<Voxel16>(shapeIndexC, this.VoxelTypeC);
								wedgeIndex++;
								sharedCorners = sharedCorners.Remove(GetEnclosed(materialCornersC));
							}
						}
					}

					noise.SetSeed(this.Seed);

					shapeIndex = this.Table.GetWedgeByFilledCorners(sharedCorners);
					if (shapeIndex.Index == 0) { continue; }
					type = this.Table.GetDefinition(shapeIndex).Type;
					if (type == WedgeType.Face || type == WedgeType.FaceSplit) { continue; }

					voxels[iX, iY, iZ][wedgeIndex] = new Wedge<Voxel16>(shapeIndex, this.VoxelTypeA);
				}
			}
		}
	}
}