﻿using static HQS.VUE.Neo.WedgeCorners;

namespace HQS.VUE.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[Serializable]
public class HillsWedgeChunkFiller : IChunkFiller<WedgeChunk<Voxel16>.Builder>
{
	[NonSerialized]
	public WedgeTable Table;

	public ushort VoxelTypeA = 1;

	public ushort VoxelTypeB = 2;

	public ushort VoxelTypeC = 3;

	public int Seed = 0;

	public float Frequency = .1f;

	public float Weight = 1f;

	public void FillChunk(WedgeChunk<Voxel16>.Builder chunk)
	{
		IChunkFillerContracts.FillChunk(chunk);
		Ensure.That(this.Table, nameof(this.Table)).IsNotNull();

		chunk.IsUniform = false;

		var noise3D = new FastNoiseLite();
		noise3D.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise3D.SetFrequency(this.Frequency);
		noise3D.SetSeed(this.Seed);

		var noise2D = new FastNoiseLite();
		noise2D.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise2D.SetFrequency(this.Frequency / 6);
		noise2D.SetSeed(this.Seed - 1);

		int sideLength = chunk.Size.SideLength;
		var voxels = chunk.Voxels;
		var lower = voxels.Indexer.LowerBounds;
		var upper = voxels.Indexer.UpperBounds;

		float lo = 0f;
		float hi = 1 - lo;

		for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
		{
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				var heightLoLo = (noise2D.GetNoise(iX + lo, iZ + lo) + 1f) * (sideLength / 6);
				var heightHiLo = (noise2D.GetNoise(iX + hi, iZ + lo) + 1f) * (sideLength / 6);
				var heightLoHi = (noise2D.GetNoise(iX + lo, iZ + hi) + 1f) * (sideLength / 6);
				var heightHiHi = (noise2D.GetNoise(iX + hi, iZ + hi) + 1f) * (sideLength / 6);

				for (int iY = lower.Y; iY <= upper.Y; iY++)
				{
					float groundLoLo = (heightLoLo - iY) * .25f;
					float groundHiLo = (heightHiLo - iY) * .25f;
					float groundLoHi = (heightLoHi - iY) * .25f;
					float groundHiHi = (heightHiHi - iY) * .25f;

					////var sharedCorners =
					////	(groundLoLo > 0 ? NNN : None) |
					////	(groundHiLo > 0 ? PNN : None) |
					////	(groundLoLo > 1 ? NPN : None) |
					////	(groundHiLo > 1 ? PPN : None) |
					////	(groundLoHi > 0 ? NNP : None) |
					////	(groundHiHi > 0 ? PNP : None) |
					////	(groundLoHi > 1 ? NPP : None) |
					////	(groundHiHi > 1 ? PPP : None);

					var sharedCorners =
						(noise3D.GetNoise(iX + lo, iY + lo, iZ + lo) <= groundLoLo + 1 ? NNN : None) |
						(noise3D.GetNoise(iX + hi, iY + lo, iZ + lo) <= groundHiLo + 1 ? PNN : None) |
						(noise3D.GetNoise(iX + lo, iY + hi, iZ + lo) <= groundLoLo + 0 ? NPN : None) |
						(noise3D.GetNoise(iX + hi, iY + hi, iZ + lo) <= groundHiLo + 0 ? PPN : None) |
						(noise3D.GetNoise(iX + lo, iY + lo, iZ + hi) <= groundLoHi + 1 ? NNP : None) |
						(noise3D.GetNoise(iX + hi, iY + lo, iZ + hi) <= groundHiHi + 1 ? PNP : None) |
						(noise3D.GetNoise(iX + lo, iY + hi, iZ + hi) <= groundLoHi + 0 ? NPP : None) |
						(noise3D.GetNoise(iX + hi, iY + hi, iZ + hi) <= groundHiHi + 0 ? PPP : None);

					var shapeIndex = this.Table.GetWedgeByFilledCorners(sharedCorners);
					if (shapeIndex.Index == 0) { continue; }
					var type = this.Table.GetDefinition(shapeIndex).Type;
					if (type == WedgeType.Face || type == WedgeType.FaceSplit) { continue; }

					noise3D.SetSeed(this.Seed + 1);
					var voxelType = this.VoxelTypeA;

					if (noise3D.GetNoise(iX + .5f, iY + .5f, iZ + .5f) <= .1f)
					{
						voxelType = this.VoxelTypeB;
					}
					else
					{
						noise3D.SetSeed(this.Seed + 2);
						if (noise3D.GetNoise(iX + .5f, iY + .5f, iZ + .5f) <= .1f)
						{
							voxelType = this.VoxelTypeC;
						}
					}

					voxels[iX, iY, iZ][0] = new Wedge<Voxel16>(shapeIndex, voxelType);
					noise3D.SetSeed(this.Seed);
				}
			}
		}
	}

	// TODO more generation experiments later
	public void FillChunkEXPERIMENT(WedgeChunk<Voxel16>.Builder chunk)
	{
		IChunkFillerContracts.FillChunk(chunk);
		Ensure.That(this.Table, nameof(this.Table)).IsNotNull();

		chunk.IsUniform = false;

		var noise3D = new FastNoiseLite();
		noise3D.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise3D.SetFrequency(this.Frequency);
		noise3D.SetSeed(this.Seed);

		var noise2D = new FastNoiseLite();
		noise2D.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise2D.SetFrequency(this.Frequency / 6);
		noise2D.SetSeed(this.Seed - 1);

		int sideLength = chunk.Size.SideLength;
		var voxels = chunk.Voxels;
		var lower = voxels.Indexer.LowerBounds;
		var upper = voxels.Indexer.UpperBounds;

		float lo = 0f;
		float hi = 1 - lo;

		for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
		{
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				for (int iY = lower.Y; iY <= upper.Y; iY++)
				{
					////if (noise3D.GetNoise(iX, iY, iZ) + 1 > this.Weight) { continue; }

					var sharedCorners = WedgeCorners.None;
					if (noise3D.GetNoise(iX - 1, iY, iZ) + 1 <= this.Weight) { sharedCorners |= NNN | NNP | NPN | NPP; }
					if (noise3D.GetNoise(iX + 1, iY, iZ) + 1 <= this.Weight) { sharedCorners |= PNN | PNP | PPN | PPP; }
					if (noise3D.GetNoise(iX, iY - 1, iZ) + 1 <= this.Weight) { sharedCorners |= NNN | NNP | PNN | PNP; }
					if (noise3D.GetNoise(iX, iY + 1, iZ) + 1 <= this.Weight) { sharedCorners |= NPN | NPP | PPN | PPP; }
					if (noise3D.GetNoise(iX, iY, iZ - 1) + 1 <= this.Weight) { sharedCorners |= NNN | NPN | PNN | PPN; }
					if (noise3D.GetNoise(iX, iY, iZ + 1) + 1 <= this.Weight) { sharedCorners |= NNP | NPP | PNP | PPP; }

					if (noise3D.GetNoise(iX, iY - 1, iZ - 1) + 1 <= this.Weight) { sharedCorners |= NNN | PNN; }
					if (noise3D.GetNoise(iX, iY - 1, iZ + 1) + 1 <= this.Weight) { sharedCorners |= NNP | PNP; }
					if (noise3D.GetNoise(iX, iY + 1, iZ - 1) + 1 <= this.Weight) { sharedCorners |= NPN | PPN; }
					if (noise3D.GetNoise(iX, iY + 1, iZ + 1) + 1 <= this.Weight) { sharedCorners |= NPP | PPP; }

					if (noise3D.GetNoise(iX - 1, iY, iZ - 1) + 1 <= this.Weight) { sharedCorners |= NNN | NPN; }
					if (noise3D.GetNoise(iX - 1, iY, iZ + 1) + 1 <= this.Weight) { sharedCorners |= NNP | NPP; }
					if (noise3D.GetNoise(iX + 1, iY, iZ - 1) + 1 <= this.Weight) { sharedCorners |= PNN | PPN; }
					if (noise3D.GetNoise(iX + 1, iY, iZ + 1) + 1 <= this.Weight) { sharedCorners |= PNP | PPP; }

					if (noise3D.GetNoise(iX - 1, iY - 1, iZ) + 1 <= this.Weight) { sharedCorners |= NNN | NNP; }
					if (noise3D.GetNoise(iX - 1, iY + 1, iZ) + 1 <= this.Weight) { sharedCorners |= NPN | NPP; }
					if (noise3D.GetNoise(iX + 1, iY - 1, iZ) + 1 <= this.Weight) { sharedCorners |= PNN | PNP; }
					if (noise3D.GetNoise(iX + 1, iY + 1, iZ) + 1 <= this.Weight) { sharedCorners |= PPN | PPP; }

					////if (noise3D.GetNoise(iX - 1, iY - 1, iZ - 1) + 1 <= this.Weight) { sharedCorners |= NNN; }
					////if (noise3D.GetNoise(iX - 1, iY - 1, iZ + 1) + 1 <= this.Weight) { sharedCorners |= NNP; }
					////if (noise3D.GetNoise(iX - 1, iY + 1, iZ - 1) + 1 <= this.Weight) { sharedCorners |= NPN; }
					////if (noise3D.GetNoise(iX - 1, iY + 1, iZ + 1) + 1 <= this.Weight) { sharedCorners |= NPP; }
					////if (noise3D.GetNoise(iX + 1, iY - 1, iZ - 1) + 1 <= this.Weight) { sharedCorners |= PNN; }
					////if (noise3D.GetNoise(iX + 1, iY - 1, iZ + 1) + 1 <= this.Weight) { sharedCorners |= PNP; }
					////if (noise3D.GetNoise(iX + 1, iY + 1, iZ - 1) + 1 <= this.Weight) { sharedCorners |= PPN; }
					////if (noise3D.GetNoise(iX + 1, iY + 1, iZ + 1) + 1 <= this.Weight) { sharedCorners |= PPP; }

					var shapeIndex = this.Table.GetWedgeByFilledCorners(sharedCorners);
					if (shapeIndex.Index == 0) { continue; }
					var type = this.Table.GetDefinition(shapeIndex).Type;
					if (type == WedgeType.Face || type == WedgeType.FaceSplit) { continue; }

					noise3D.SetSeed(this.Seed + 1);
					var voxelType = this.VoxelTypeA;

					if (noise3D.GetNoise(iX + .5f, iY + .5f, iZ + .5f) <= .1f)
					{
						voxelType = this.VoxelTypeB;
					}
					else
					{
						noise3D.SetSeed(this.Seed + 2);
						if (noise3D.GetNoise(iX + .5f, iY + .5f, iZ + .5f) <= .1f)
						{
							voxelType = this.VoxelTypeC;
						}
					}

					voxels[iX, iY, iZ][0] = new Wedge<Voxel16>(shapeIndex, voxelType);
					noise3D.SetSeed(this.Seed);
				}
			}
		}
	}
}