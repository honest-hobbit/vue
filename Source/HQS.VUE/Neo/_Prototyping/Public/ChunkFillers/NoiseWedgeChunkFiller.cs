﻿namespace HQS.VUE.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[Serializable]
public class NoiseWedgeChunkFiller : IChunkFiller<WedgeChunk<Voxel16>.Builder>
{
	public Wedge16 VoxelTypeA = new Wedge16(1, 1);

	public Wedge16 VoxelTypeB = new Wedge16(1, 2);

	public Wedge16 VoxelTypeC = new Wedge16(1, 3);

	public int Seed = 0;

	public float Frequency = .1f;

	public float Weight = 1f;

	public void FillChunk(WedgeChunk<Voxel16>.Builder chunk)
	{
		IChunkFillerContracts.FillChunk(chunk);

		chunk.IsUniform = false;

		var noise = new FastNoiseLite();
		noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise.SetFrequency(this.Frequency);
		noise.SetSeed(this.Seed);

		var voxels = chunk.Voxels;
		var lower = voxels.Indexer.LowerBounds;
		var upper = voxels.Indexer.UpperBounds;

		for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
		{
			for (int iY = lower.Y; iY <= upper.Y; iY++)
			{
				for (int iX = lower.X; iX <= upper.X; iX++)
				{
					if (noise.GetNoise(iX, iY, iZ) + 1 > this.Weight) { continue; }

					var voxelType = this.VoxelTypeA;

					float weightB = this.Weight * 1.2f;
					noise.SetSeed(this.Seed + 1);
					if (noise.GetNoise(iX + .5f, iY + .5f, iZ + .5f) + 1 <= weightB)
					{
						voxelType = this.VoxelTypeB;
					}
					else
					{
						float weightC = this.Weight * 1.2f;
						noise.SetSeed(this.Seed + 2);
						if (noise.GetNoise(iX + .5f, iY + .5f, iZ + .5f) + 1 <= weightC)
						{
							voxelType = this.VoxelTypeC;
						}
					}

					voxels[iX, iY, iZ][0] = voxelType;
					noise.SetSeed(this.Seed);
				}
			}
		}
	}
}
