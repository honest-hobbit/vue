﻿namespace HQS.VUE.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[Serializable]
public class DebugCubeChunkFiller : IChunkFiller<CubeChunk<Voxel16>.Builder>
{
	public ushort VoxelTypeA = 1;

	public ushort VoxelTypeB = 2;

	public ushort VoxelTypeC = 3;

	public ushort VoxelTypeD = 4;

	public void FillChunk(CubeChunk<Voxel16>.Builder chunk)
	{
		IChunkFillerContracts.FillChunk(chunk);

		chunk.IsUniform = false;

		var voxels = chunk.Voxels;
		var lower = voxels.Indexer.LowerBounds;
		var upper = voxels.Indexer.UpperBounds;

		var mid = (upper + lower) / 2;

		voxels[lower.X, lower.Y, lower.Z] = this.VoxelTypeA;
		voxels[lower.X, lower.Y, upper.Z] = this.VoxelTypeA;
		voxels[upper.X, lower.Y, lower.Z] = this.VoxelTypeA;
		voxels[upper.X, lower.Y, upper.Z] = this.VoxelTypeA;

		voxels[lower.X, upper.Y, lower.Z] = this.VoxelTypeA;
		voxels[lower.X, upper.Y, upper.Z] = this.VoxelTypeA;
		voxels[upper.X, upper.Y, lower.Z] = this.VoxelTypeA;
		voxels[upper.X, upper.Y, upper.Z] = this.VoxelTypeA;

		for (int iY = lower.Y; iY <= upper.Y; iY++)
		{
			for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
			{
				voxels[mid.X, iY, iZ] = this.VoxelTypeB;
			}
		}

		for (int iX = lower.X; iX <= upper.X; iX++)
		{
			for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
			{
				voxels[iX, mid.Y, iZ] = this.VoxelTypeC;
			}
		}

		for (int iX = lower.X; iX <= upper.X; iX++)
		{
			for (int iY = lower.Y; iY <= upper.Y; iY++)
			{
				voxels[iX, iY, mid.Z] = this.VoxelTypeD;
			}
		}
	}
}
