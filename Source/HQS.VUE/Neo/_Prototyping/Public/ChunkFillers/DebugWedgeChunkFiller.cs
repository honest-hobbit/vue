﻿namespace HQS.VUE.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[Serializable]
public class DebugWedgeChunkFiller : IChunkFiller<WedgeChunk<Voxel16>.Builder>
{
	public Wedge16 VoxelTypeA = new Wedge16(1, 1);

	public Wedge16 VoxelTypeB = new Wedge16(1, 2);

	public Wedge16 VoxelTypeC = new Wedge16(1, 3);

	public Wedge16 VoxelTypeD = new Wedge16(1, 4);

	public void FillChunk(WedgeChunk<Voxel16>.Builder chunk)
	{
		IChunkFillerContracts.FillChunk(chunk);

		chunk.IsUniform = false;

		var voxels = chunk.Voxels;
		var lower = voxels.Indexer.LowerBounds;
		var upper = voxels.Indexer.UpperBounds;

		var mid = (upper + lower) / 2;

		voxels[lower.X, lower.Y, upper.Z][0] = this.VoxelTypeA;
		voxels[upper.X, lower.Y, lower.Z][0] = this.VoxelTypeA;
		voxels[upper.X, lower.Y, upper.Z][0] = this.VoxelTypeA;
		voxels[lower.X, lower.Y, lower.Z][0] = this.VoxelTypeA;

		voxels[lower.X, upper.Y, lower.Z][0] = this.VoxelTypeA;
		voxels[lower.X, upper.Y, upper.Z][0] = this.VoxelTypeA;
		voxels[upper.X, upper.Y, lower.Z][0] = this.VoxelTypeA;
		voxels[upper.X, upper.Y, upper.Z][0] = this.VoxelTypeA;

		for (int iY = lower.Y; iY <= upper.Y; iY++)
		{
			for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
			{
				voxels[mid.X, iY, iZ][0] = this.VoxelTypeB;
			}
		}

		for (int iX = lower.X; iX <= upper.X; iX++)
		{
			for (int iZ = lower.Z; iZ <= upper.Z; iZ++)
			{
				voxels[iX, mid.Y, iZ][0] = this.VoxelTypeC;
			}
		}

		for (int iX = lower.X; iX <= upper.X; iX++)
		{
			for (int iY = lower.Y; iY <= upper.Y; iY++)
			{
				voxels[iX, iY, mid.Z][0] = this.VoxelTypeD;
			}
		}
	}
}
