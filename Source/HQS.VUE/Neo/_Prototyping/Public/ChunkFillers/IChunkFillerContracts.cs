﻿namespace HQS.VUE.Neo;

public static class IChunkFillerContracts
{
	public static void FillChunk<TChunk>(TChunk chunk)
		where TChunk : class
	{
		Ensure.That(chunk, nameof(chunk)).IsNotNull();
	}
}
