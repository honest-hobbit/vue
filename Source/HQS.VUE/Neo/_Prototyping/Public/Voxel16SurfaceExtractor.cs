﻿namespace HQS.VUE.Neo;

public readonly struct Voxel16SurfaceExtractor : ISurfaceExtractor<Voxel16, Surface16>
{
	public static Voxel16SurfaceExtractor Default => default;

	/// <inheritdoc />
	public Voxel16 EmptyVoxel
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => 0;
	}

	/// <inheritdoc />
	public Surface16 EmptySurface
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => 0;
	}

	/// <inheritdoc />
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public bool IsEmpty(Surface16 voxel) => voxel == 0;

	/// <inheritdoc />
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public bool IsSame(Surface16 a, Surface16 b) => a == b;

	/// <inheritdoc />
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public int Compare(Surface16 a, Surface16 b) => a - b;

	/// <inheritdoc />
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void GetSurfaces(Voxel16 voxel, ref bool hasSurface, ref Surface16 surface)
	{
		surface = voxel;
		hasSurface |= voxel != 0;
	}

	/// <inheritdoc />
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void GetSurfaces(
		Voxel16 voxelA,
		Voxel16 voxelB,
		ref bool hasSurfaceA,
		ref Surface16 surfaceA,
		ref bool hasSurfaceB,
		ref Surface16 surfaceB)
	{
		// make a surface only between empty and filled voxels
		if (voxelA != voxelB)
		{
			if (voxelA == 0)
			{
				surfaceA = 0;
				hasSurfaceB = true;
				surfaceB = voxelB;
			}
			else if (voxelB == 0)
			{
				surfaceB = 0;
				hasSurfaceA = true;
				surfaceA = voxelA;
			}
			else
			{
				surfaceA = 0;
				surfaceB = 0;
			}
		}
		else
		{
			surfaceA = 0;
			surfaceB = 0;
		}

		// always make a surface
		////surfaceA = voxelA;
		////surfaceB = voxelB;

		// make a surface between different voxel types
		////if (voxelA != voxelB)
		////{
		////	surfaceA = voxelA;
		////	surfaceB = voxelB;
		////	hasSurfaceA = true;
		////	hasSurfaceB = true;
		////}
		////else
		////{
		////	surfaceA = 0;
		////	surfaceB = 0;
		////}
	}
}
