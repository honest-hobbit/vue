﻿namespace HQS.VUE.Neo;

public readonly record struct Voxel16 : IIndexable, IComparable<Voxel16>
{
	public readonly ushort Value;

	public Voxel16(ushort value)
	{
		this.Value = value;
	}

	public static implicit operator Voxel16(ushort value) => new Voxel16(value);

	public static implicit operator ushort(Voxel16 value) => value.Value;

	public static implicit operator Surface16(Voxel16 value) => new Surface16(value.Value);

	/// <inheritdoc />
	public int Index => this.Value;

	/// <inheritdoc />
	public int CompareTo(Voxel16 other) => this.Value - other.Value;

	/// <inheritdoc />
	public override string ToString() => this.Value.ToString();
}
