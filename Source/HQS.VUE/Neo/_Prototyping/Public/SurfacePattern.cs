﻿namespace HQS.VUE.Neo;

// TODO temporary prototyping purposes only
public enum SurfacePattern
{
	Empty,

	Uniform,

	Quadrants,

	Checkered,

	Diamonds,

	Triangles,

	Pentagons,

	Grid,

	SpinesVertical,

	SpinesHorizontal,

	Noise,

	Special_Bug,

	Special_ConcaveQuadsA,

	Special_ConcaveQuadsB,
}
