﻿namespace HQS.VUE.Neo;

public readonly record struct Surface16 : IIndexable, IComparable<Surface16>
{
	public readonly ushort Value;

	public Surface16(ushort value)
	{
		this.Value = value;
	}

	public static implicit operator Surface16(ushort value) => new Surface16(value);

	public static implicit operator ushort(Surface16 value) => value.Value;

	public static implicit operator Voxel16(Surface16 value) => new Voxel16(value.Value);

	/// <inheritdoc />
	public int Index => this.Value;

	/// <inheritdoc />
	public int CompareTo(Surface16 other) => this.Value - other.Value;

	/// <inheritdoc />
	public override string ToString() => this.Value.ToString();
}
