﻿namespace HQS.VUE.Neo;

[Serializable]
public record struct Wedge16
{
	public static readonly Wedge16 Empty = default;

	public byte Shape;

	public ushort Data;

	public Wedge16(byte shape, ushort data)
	{
		this.Shape = shape;
		this.Data = data;
	}

	public static implicit operator Wedge<ushort>(Wedge16 value) => new Wedge<ushort>(value.Shape, value.Data);

	public static implicit operator Wedge16(Wedge<ushort> value) => new Wedge16(value.Shape, value.Data);

	public static implicit operator Wedge<Voxel16>(Wedge16 value) => new Wedge<Voxel16>(value.Shape, value.Data);

	public static implicit operator Wedge16(Wedge<Voxel16> value) => new Wedge16(value.Shape, value.Data);
}
