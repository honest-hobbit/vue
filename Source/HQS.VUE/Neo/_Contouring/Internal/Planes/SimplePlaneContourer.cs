﻿namespace HQS.VUE.Neo;

internal class SimplePlaneContourer<TSurface, TComparer> : IPlaneContourer<TSurface, TComparer>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	private readonly PlaneContouringBounds boundsChecker;

	private TSurface surfaceType;

	private int triangleCount;

	public SimplePlaneContourer(ChunkSize size)
	{
		size.AssertValid();

		this.boundsChecker = new PlaneContouringBounds(size);
	}

	/// <inheritdoc />
	public IPlaneContourerProfiler Profiler { get; } = new NullPlaneContourerProfiler();

	/// <inheritdoc />
	public IContouringBounds<Int2> Bounds => this.boundsChecker;

	/// <inheritdoc />
	public SurfacePlane<TSurface> Surfaces { get; set; }

	/// <inheritdoc />
	public TComparer Comparer { get; set; }

	/// <inheritdoc />
	public bool CheckForTrianglesMissed { get; set; }

	/// <inheritdoc />
	public bool ReverseOutput { get; set; }

	/// <inheritdoc />
	public Int2 OffsetOutput { get; set; }

	/// <inheritdoc />
	public CoplanarChunk<TSurface>.Builder Output { get; set; }

	/// <inheritdoc />
	public void ContourPlane(PlaneIndex plane)
	{
		IPlaneContourerContracts.ContourPlane(this);

		if (this.boundsChecker.IsEmpty) { return; }

		int initialSurfacesCount = this.Output.Surfaces.Count;
		int initialTrianglesCount = this.Output.Triangles.Count;

		var lower = this.boundsChecker.Lower;
		var upper = this.boundsChecker.Upper;

		this.surfaceType = this.Surfaces[lower.X, lower.Y].Top;
		this.triangleCount = 0;

		for (int iY = lower.Y; iY <= upper.Y; iY++)
		{
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				var surface = this.Surfaces[iX, iY];

				if (!this.Comparer.IsEmpty(surface.Top))
				{
					this.CountTriangles(surface.Top);
					this.AddTopTriangle(iX, iY);
				}

				if (!this.Comparer.IsEmpty(surface.Right))
				{
					this.CountTriangles(surface.Right);
					this.AddRightTriangle(iX, iY);
				}

				if (!this.Comparer.IsEmpty(surface.Bottom))
				{
					this.CountTriangles(surface.Bottom);
					this.AddBottomTriangle(iX, iY);
				}

				if (!this.Comparer.IsEmpty(surface.Left))
				{
					this.CountTriangles(surface.Left);
					this.AddLeftTriangle(iX, iY);
				}
			}
		}

		if (this.triangleCount > 0)
		{
			this.Output.AddSurface(new SurfaceGroup<TSurface>(this.surfaceType, this.triangleCount));
		}

		int surfacesAdded = this.Output.Surfaces.Count - initialSurfacesCount;
		if (surfacesAdded > 0)
		{
			int trianglesAdded = this.Output.Triangles.Count - initialTrianglesCount;
			this.Output.AddPlane(new PlaneGroup(plane, surfacesAdded, trianglesAdded));
		}
	}

	private void CountTriangles(TSurface surface)
	{
		if (this.Comparer.IsSame(surface, this.surfaceType))
		{
			this.triangleCount++;
		}
		else
		{
			if (this.triangleCount > 0)
			{
				this.Output.AddSurface(new SurfaceGroup<TSurface>(this.surfaceType, this.triangleCount));
			}

			this.surfaceType = surface;
			this.triangleCount = 1;
		}
	}

	// x, y are the bottom left corner
	private void AddTopTriangle(int x, int y)
	{
		this.SetPosition(ref x, ref y);

		var a = CompactPoint2.From(x, y + 2);
		var b = CompactPoint2.From(x + 2, y + 2);
		var c = CompactPoint2.From(x + 1, y + 1);

		this.OutputTriangle(a, b, c);
	}

	// x, y are the bottom left corner
	private void AddRightTriangle(int x, int y)
	{
		this.SetPosition(ref x, ref y);

		var a = CompactPoint2.From(x + 2, y + 2);
		var b = CompactPoint2.From(x + 2, y);
		var c = CompactPoint2.From(x + 1, y + 1);

		this.OutputTriangle(a, b, c);
	}

	// x, y are the bottom left corner
	private void AddBottomTriangle(int x, int y)
	{
		this.SetPosition(ref x, ref y);

		var a = CompactPoint2.From(x + 2, y);
		var b = CompactPoint2.From(x, y);
		var c = CompactPoint2.From(x + 1, y + 1);

		this.OutputTriangle(a, b, c);
	}

	// x, y are the bottom left corner
	private void AddLeftTriangle(int x, int y)
	{
		this.SetPosition(ref x, ref y);

		var a = CompactPoint2.From(x, y);
		var b = CompactPoint2.From(x, y + 2);
		var c = CompactPoint2.From(x + 1, y + 1);

		this.OutputTriangle(a, b, c);
	}

	private void SetPosition(ref int x, ref int y)
	{
		x = (x + this.OffsetOutput.X) << 1;
		y = (y + this.OffsetOutput.Y) << 1;
	}

	private void OutputTriangle(CompactPoint2 a, CompactPoint2 b, CompactPoint2 c)
	{
		if (this.ReverseOutput) { RefUtility.Swap(ref a, ref b); }
		this.Output.AddTriangle(new CompactTriangle(a, b, c));
	}
}
