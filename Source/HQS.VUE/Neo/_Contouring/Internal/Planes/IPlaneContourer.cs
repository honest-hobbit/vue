﻿namespace HQS.VUE.Neo;

internal interface IPlaneContourer<TSurface, TComparer>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	IPlaneContourerProfiler Profiler { get; }

	IContouringBounds<Int2> Bounds { get; }

	SurfacePlane<TSurface> Surfaces { get; set; }

	TComparer Comparer { get; set; }

	bool CheckForTrianglesMissed { get; set; }

	bool ReverseOutput { get; set; }

	Int2 OffsetOutput { get; set; }

	CoplanarChunk<TSurface>.Builder Output { get; set; }

	void ContourPlane(PlaneIndex plane);
}
