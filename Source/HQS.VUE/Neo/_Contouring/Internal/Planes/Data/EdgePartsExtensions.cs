﻿namespace HQS.VUE.Neo;

internal static class EdgePartsExtensions
{
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool Has(this EdgeParts current, EdgeParts flag) => (current & flag) == flag;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool HasAny(this EdgeParts current, EdgeParts flags) => (current & flags) != 0;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static EdgeParts Select(this EdgeParts current, EdgeParts flag) => current & flag;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static EdgeParts SelectAll(this EdgeParts current) => current & EdgeParts.All;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static EdgeParts Add(this EdgeParts current, EdgeParts flags) => current | flags;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static EdgeParts Remove(this EdgeParts current, EdgeParts flags) => current & ~flags;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static EdgeParts Set(this EdgeParts current, EdgeParts flags, bool value) =>
		value ? current.Add(flags) : current.Remove(flags);
}
