﻿namespace HQS.VUE.Neo;

internal record struct EdgeState<T>
	where T : unmanaged
{
	public ushort PolygonID;

	public ushort NextEdgeIndex;

	public EdgeSpan Edge;

	public T SurfaceType;
}

internal static class EdgeState
{
	public const ushort UnassignedID = 0;

	public static EdgeState<TVoxel> EmptyPolygon<TVoxel>()
		where TVoxel : unmanaged => default;
}
