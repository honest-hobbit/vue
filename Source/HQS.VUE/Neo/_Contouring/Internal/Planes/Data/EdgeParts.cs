﻿namespace HQS.VUE.Neo;

[Flags]
internal enum EdgeParts : ushort
{
	None = 0,

	TopOut = 1,

	TopRight = 1 << 1,

	TopLeft = 1 << 2,

	RightOut = 1 << 3,

	RightBottom = 1 << 4,

	RightTop = 1 << 5,

	BottomOut = 1 << 6,

	BottomLeft = 1 << 7,

	BottomRight = 1 << 8,

	LeftOut = 1 << 9,

	LeftTop = 1 << 10,

	LeftBottom = 1 << 11,

	TopAll = TopOut | TopRight | TopLeft,

	RightAll = RightOut | RightBottom | RightTop,

	BottomAll = BottomOut | BottomLeft | BottomRight,

	LeftAll = LeftOut | LeftTop | LeftBottom,

	All = TopAll | RightAll | BottomAll | LeftAll,
}
