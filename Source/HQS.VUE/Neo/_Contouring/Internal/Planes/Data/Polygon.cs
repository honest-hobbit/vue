﻿namespace HQS.VUE.Neo;

internal readonly record struct Polygon<T> : IEnumerable<EdgeSpan>
	where T : unmanaged
{
	private readonly ReadOnlyArray<EdgeState<T>> edgeStates;

	private readonly int polygonID;

	public Polygon(ReadOnlyArray<EdgeState<T>> edgeStates, int polygonID)
	{
		Debug.Assert(!edgeStates.IsDefault);
		Debug.Assert(polygonID >= 0);

		this.SurfaceType = edgeStates[polygonID].SurfaceType;
		this.edgeStates = edgeStates;
		this.polygonID = polygonID;
	}

	public T SurfaceType { get; }

	public bool HasHoles => this.edgeStates[this.polygonID].NextEdgeIndex != this.polygonID;

	public EdgeSpan FirstEdge => this.edgeStates[this.polygonID].Edge;

	public EdgeEnumerator<T> GetEnumerator() =>
		new EdgeEnumerator<T>(this.edgeStates, this.polygonID);

	/// <inheritdoc />
	IEnumerator<EdgeSpan> IEnumerable<EdgeSpan>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
