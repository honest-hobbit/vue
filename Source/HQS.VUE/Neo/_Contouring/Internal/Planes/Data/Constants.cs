﻿namespace HQS.VUE.Neo;

internal static class Constants
{
	public const int EdgesPerTriangle = 3;

	public const int VerticesPerTriangle = 3;

	// 6 because tessellating using Tess ElementType.ConnectedPolygons
	public const int ElementsPerTriangle = 6;

	public const int TrianglesPerSurfaceQuad = 4;

	public const int VerticesPerQuad = 4;
}
