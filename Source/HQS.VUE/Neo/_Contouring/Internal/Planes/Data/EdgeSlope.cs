﻿namespace HQS.VUE.Neo;

internal enum EdgeSlope
{
	None,

	Horizontal,

	Ascending,

	Vertical,

	Descending,
}
