﻿namespace HQS.VUE.Neo;

internal readonly record struct Point2
{
	public readonly ushort X;

	public readonly ushort Y;

	public Point2(ushort x, ushort y)
	{
		this.X = x;
		this.Y = y;
	}
}
