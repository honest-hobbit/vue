﻿namespace HQS.VUE.Neo;

internal record struct ConnectedTriangle
{
	private static readonly Point2 DeletedTriangle = new Point2(ushort.MaxValue, ushort.MaxValue);

	public const int None = -1;

	public Point2 PointA;

	public Point2 PointB;

	public Point2 PointC;

	public int EdgeAB;

	public int EdgeBC;

	public int EdgeCA;

	public float SlopeAB
	{
		get
		{
			Debug.Assert(!this.IsDeleted);

			return PointMathUtility.Slope(this.PointA, this.PointB);
		}
	}

	public float SlopeBC
	{
		get
		{
			Debug.Assert(!this.IsDeleted);

			return PointMathUtility.Slope(this.PointB, this.PointC);
		}
	}

	public float SlopeCA
	{
		get
		{
			Debug.Assert(!this.IsDeleted);

			return PointMathUtility.Slope(this.PointC, this.PointA);
		}
	}

	public bool IsDeleted => this.PointA == DeletedTriangle;

	public void Delete()
	{
		Debug.Assert(!this.IsDeleted);

		this.PointA = DeletedTriangle;
#if DEBUG
		this.PointB = DeletedTriangle;
		this.PointC = DeletedTriangle;
		this.EdgeAB = None;
		this.EdgeBC = None;
		this.EdgeCA = None;
#endif
	}

	public void ChangeEdge(int from, int to)
	{
		if (this.EdgeAB == from)
		{
			this.EdgeAB = to;
		}
		else if (this.EdgeBC == from)
		{
			this.EdgeBC = to;
		}
		else if (this.EdgeCA == from)
		{
			this.EdgeCA = to;
		}
	}
}
