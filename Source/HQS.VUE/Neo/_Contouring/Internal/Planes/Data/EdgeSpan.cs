﻿namespace HQS.VUE.Neo;

internal readonly record struct EdgeSpan
{
	public readonly int StartIndex;

	public readonly int Count;

	public EdgeSpan(int startIndex, int count)
	{
		this.StartIndex = startIndex;
		this.Count = count;
	}
}
