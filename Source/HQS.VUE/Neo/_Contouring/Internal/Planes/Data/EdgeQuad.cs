﻿namespace HQS.VUE.Neo;

internal record struct EdgeQuad
{
	public static EdgeQuad Empty => new EdgeQuad()
	{
		Parts = EdgeParts.None,
		TopID = EdgeState.UnassignedID,
		RightID = EdgeState.UnassignedID,
		BottomID = EdgeState.UnassignedID,
		LeftID = EdgeState.UnassignedID,
	};

	public EdgeParts Parts;

	public ushort TopID;

	public ushort RightID;

	public ushort BottomID;

	public ushort LeftID;
}
