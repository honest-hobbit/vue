﻿namespace HQS.VUE.Neo;

internal record struct SurfaceQuad<T>
	where T : unmanaged
{
	public T Top;

	public T Right;

	public T Bottom;

	public T Left;

	public SurfaceQuad(T top, T right, T bottom, T left)
	{
		this.Top = top;
		this.Right = right;
		this.Bottom = bottom;
		this.Left = left;
	}

	public SurfaceQuad(T value)
	{
		this.Top = value;
		this.Right = value;
		this.Bottom = value;
		this.Left = value;
	}
}
