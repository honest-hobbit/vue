﻿namespace HQS.VUE.Neo;

internal class SurfacePlane<T>
	where T : unmanaged
{
	private readonly SquareArrayIndexer indexer;

	private readonly SurfaceQuad<T>[] quads;

	internal SurfacePlane(ChunkSize size)
	{
		size.AssertValid();

		this.indexer = size.AsSquare;
		this.quads = new SurfaceQuad<T>[this.indexer.Length];
	}

	public ChunkSize Size => ChunkSize.CreateAssertOnly(this.indexer.Exponent);

	public SquareArrayIndexer Indexer => this.indexer;

	public SurfaceQuad<T>[] Quads => this.quads;

	public SurfaceQuad<T> this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.quads[index];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.quads[index] = value;
	}

	public SurfaceQuad<T> this[int x, int y]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.quads[this.indexer[x, y]];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this.quads[this.indexer[x, y]] = value;
	}

	public T this[int x, int y, QuadParts parts]
	{
		get => parts switch
		{
			QuadParts.Top => this.quads[this.indexer[x, y]].Top,
			QuadParts.Right => this.quads[this.indexer[x, y]].Right,
			QuadParts.Bottom => this.quads[this.indexer[x, y]].Bottom,
			QuadParts.Left => this.quads[this.indexer[x, y]].Left,
			_ => throw InvalidEnumArgument.CreateException(nameof(parts), parts),
		};

		set
		{
			int i = this.indexer[x, y];
			if (parts.Has(QuadParts.Top)) { this.quads[i].Top = value; }
			if (parts.Has(QuadParts.Right)) { this.quads[i].Right = value; }
			if (parts.Has(QuadParts.Bottom)) { this.quads[i].Bottom = value; }
			if (parts.Has(QuadParts.Left)) { this.quads[i].Left = value; }
		}
	}

	public void SetAllTo(T surface) => this.SetAllTo(new SurfaceQuad<T>(surface));

	public void SetAllTo(SurfaceQuad<T> quad)
	{
		int length = this.quads.Length;
		for (int i = 0; i < length; i++)
		{
			this.quads[i] = quad;
		}
	}
}
