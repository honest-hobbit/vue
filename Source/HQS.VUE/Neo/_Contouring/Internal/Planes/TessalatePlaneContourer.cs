﻿namespace HQS.VUE.Neo;

internal class TessalatePlaneContourer<TSurface, TComparer> : IPlaneContourer<TSurface, TComparer>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
#if VUE_PROFILE
	private readonly PlaneContourerProfiler profiler = new PlaneContourerProfiler();
#else
	private readonly PlaneContourerProfiler profiler = null;
#endif

	private readonly PlaneContouringBounds boundsChecker;

	private readonly EdgePlane<TSurface, TComparer> edgePlane;

	private readonly EdgeMarker<TSurface, TComparer> edgeMarker;

	private readonly EdgePointsLinker edgeLinker;

	private readonly EdgePoints edgePoints;

	private readonly Tessellator<TSurface> tessellator;

	public TessalatePlaneContourer(ChunkSize size, LibTessDotNet.Pools tessPools, int trianglesCapacity)
	{
		size.AssertValid();
		Debug.Assert(tessPools != null);
		Debug.Assert(trianglesCapacity >= 0);

		this.boundsChecker = new PlaneContouringBounds(size);
		this.edgePlane = new EdgePlane<TSurface, TComparer>(size);
		this.edgeMarker = new EdgeMarker<TSurface, TComparer>(this.edgePlane);

		var pointsArray = new CompactPoint2[
			size.AsSquare.Length * Constants.VerticesPerTriangle * Constants.TrianglesPerSurfaceQuad];

		this.edgeLinker = new EdgePointsLinker(pointsArray);
		this.edgePoints = new EdgePoints(pointsArray.AsReadOnlyArray());

		this.tessellator = new Tessellator<TSurface>(
			this.profiler, this.edgePoints, tessPools, trianglesCapacity);
	}

#if VUE_PROFILE
	/// <inheritdoc />
	public IPlaneContourerProfiler Profiler => this.profiler;
#else
	/// <inheritdoc />
	public IPlaneContourerProfiler Profiler { get; } = new NullPlaneContourerProfiler();
#endif

	/// <inheritdoc />
	public SurfacePlane<TSurface> Surfaces { get; set; }

	/// <inheritdoc />
	public IContouringBounds<Int2> Bounds => this.boundsChecker;

	/// <inheritdoc />
	public TComparer Comparer { get; set; }

	/// <inheritdoc />
	public bool CheckForTrianglesMissed { get; set; }

	/// <inheritdoc />
	public bool ReverseOutput { get; set; }

	/// <inheritdoc />
	public Int2 OffsetOutput { get; set; }

	/// <inheritdoc />
	public CoplanarChunk<TSurface>.Builder Output { get; set; }

	/// <inheritdoc />
	public void ContourPlane(PlaneIndex plane)
	{
		IPlaneContourerContracts.ContourPlane(this);

		if (this.boundsChecker.IsEmpty) { return; }

		this.profiler.AssertNotRunning();
		this.profiler.StartContourPlaneTotal();
		this.profiler.Initialize(this.CheckForTrianglesMissed);

		this.Output.StartPlane();

		this.profiler.StartFindEdgesTotal();

		this.FindAllPolygons();

		this.profiler.StopFindEdgesTotal();
		this.profiler.StartTriangulateTotal();

		this.ContourAllPolygons();

		this.profiler.StopTriangulateTotal();

		this.Output.EndPlane(plane);

		this.profiler.StopContourPlaneTotal();
		this.profiler.AddToOutputPlanes(1);
		this.tessellator.ProfileObjectAllocations();
		this.profiler.AssertNotRunning();
	}

	private void FindAllPolygons()
	{
		this.profiler.StartInitializePlane();

		this.edgePlane.Initialize(this.Comparer);
		this.edgeMarker.Initialize(this.Surfaces, this.Comparer);

		this.edgeLinker.Initialize(this.OffsetOutput << 1);
		this.tessellator.Initialize(this.ReverseOutput, this.Output);

		var start = this.boundsChecker.Lower;
		var dimensions = this.boundsChecker.Dimensions;

		this.profiler.StopInitializePlane();
		this.profiler.StartMarkEdges();

		this.edgeMarker.InvokeLoop(start, dimensions);

		this.profiler.StopMarkEdges();
		this.profiler.StartTraceEdges();

		var tracer = new EdgeTracer<TSurface, TComparer>(this.Surfaces, this.edgePlane, this.edgeLinker);
		tracer.FindHoles(start, dimensions);

		this.profiler.StopTraceEdges();
		this.profiler.StartFindHoles();

		this.edgePlane.FindHoles(start, dimensions);

		this.profiler.StopFindHoles();
		this.profiler.StartSortEdges();

		this.edgePlane.SortEdges();

		this.profiler.StopSortEdges();
	}

	private void ContourAllPolygons()
	{
		Debug.Assert(this.Output != null);

		using var polygons = this.edgePlane.GetEnumerator();

		if (!polygons.MoveNext())
		{
			return;
		}

		var surfaceType = polygons.Current.SurfaceType;
		int triangleCount = this.ContourPolygon(polygons.Current);

		while (polygons.MoveNext())
		{
			if (!this.Comparer.IsSame(polygons.Current.SurfaceType, surfaceType))
			{
				this.profiler.AddToOutputSurfaces(1);
				this.profiler.AddToOutputTriangles(triangleCount);

				this.Output.AddSurface(new SurfaceGroup<TSurface>(surfaceType, triangleCount));
				surfaceType = polygons.Current.SurfaceType;
				triangleCount = 0;
			}

			triangleCount += this.ContourPolygon(polygons.Current);
		}

		this.profiler.AddToOutputSurfaces(1);
		this.profiler.AddToOutputTriangles(triangleCount);

		this.Output.AddSurface(new SurfaceGroup<TSurface>(surfaceType, triangleCount));
	}

	// returns the number of triangles added to output
	private int ContourPolygon(Polygon<TSurface> polygon)
	{
		Debug.Assert(this.Output != null);

		if (polygon.HasHoles)
		{
			// the polygon is complex (has holes) so use the tessellation library
			return this.tessellator.TessellateComplexPolygon(polygon);
		}

		// the polygon is simple (has only 1 edge and no holes)
		var singleEdge = polygon.FirstEdge;

		if (singleEdge.Count > Constants.VerticesPerQuad)
		{
			// the polygon has many vertices so use the tessellation library
			return this.tessellator.TessellateSimplePolygon(singleEdge);
		}

		// the polygon is either a triangle or a quad and can be
		// easily and efficiently handled without the tessellation library
		this.profiler.AddToPolygonsSpecial(1);
		this.profiler.StartTriangulateSpecial();

		this.edgePoints.SetToEdgeSpan(singleEdge);

		if (singleEdge.Count == Constants.VerticesPerTriangle)
		{
			// the polygon is a single triangle
			if (this.ReverseOutput)
			{
				this.Output.AddTriangle(new CompactTriangle(
					this.edgePoints[0], this.edgePoints[2], this.edgePoints[1]));
			}
			else
			{
				this.Output.AddTriangle(new CompactTriangle(
					this.edgePoints[0], this.edgePoints[1], this.edgePoints[2]));
			}

			this.profiler.StopTriangulateSpecial();

			return 1;
		}

		// the polygon is a quad (2 triangles)
		Debug.Assert(singleEdge.Count == Constants.VerticesPerQuad);

		var pointA = this.edgePoints[0];
		var pointB = this.edgePoints[1];
		var pointC = this.edgePoints[2];
		var pointD = this.edgePoints[3];

		var a = pointA.GetPosition();
		var b = pointB.GetPosition();
		var c = pointC.GetPosition();
		var d = pointD.GetPosition();

		if (PointMathUtility.Cross(b - a, d - a) > 0 || PointMathUtility.Cross(d - c, b - c) > 0)
		{
			// either A or C has an angle greater than 180 thus the quad is concave
			// split the quad into 2 triangles along A to C
			if (this.ReverseOutput)
			{
				this.Output.AddTriangle(new CompactTriangle(pointA, pointC, pointB));
				this.Output.AddTriangle(new CompactTriangle(pointD, pointC, pointA));
			}
			else
			{
				this.Output.AddTriangle(new CompactTriangle(pointA, pointB, pointC));
				this.Output.AddTriangle(new CompactTriangle(pointD, pointA, pointC));
			}
		}
		else
		{
			// either B or D has an angle greater than 180 thus the quad is concave
			// or all 4 vertices have angles less than 180 thus the quad is convex
			// either way split the quad into 2 triangles along B to D
			if (this.ReverseOutput)
			{
				this.Output.AddTriangle(new CompactTriangle(pointA, pointD, pointB));
				this.Output.AddTriangle(new CompactTriangle(pointD, pointC, pointB));
			}
			else
			{
				this.Output.AddTriangle(new CompactTriangle(pointA, pointB, pointD));
				this.Output.AddTriangle(new CompactTriangle(pointD, pointB, pointC));
			}
		}

		this.profiler.StopTriangulateSpecial();

		return 2;
	}
}
