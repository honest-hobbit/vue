﻿namespace HQS.VUE.Neo;

internal static class IPlaneContourerContracts
{
	public static void ContourPlane<TSurface, TComparer>(
		IPlaneContourer<TSurface, TComparer> contourer)
		where TSurface : unmanaged
		where TComparer : ISurfaceComparer<TSurface>
	{
		Debug.Assert(contourer != null);

		// TODO could this be changed to allow the Surfaces plane
		// to be a smaller ChunkSize than the plane contourer?
		Ensure.That(contourer.Surfaces, nameof(contourer.Surfaces)).IsNotNull();
		Ensure.That(
			contourer.Surfaces.Size.Exponent,
			$"{nameof(contourer.Surfaces)}.{nameof(contourer.Surfaces.Size)}")
			.Is(contourer.Bounds.Size.Exponent);

		Ensure.That(contourer.Output, nameof(contourer.Output)).IsNotNull();
	}
}
