﻿namespace HQS.VUE.Neo;

internal class EdgePointsLinker
{
	private readonly CompactPoint2[] points;

	private Int2 offsetOutput;

	private int pointsCount;

	private int startIndex;

#if DEBUG
	private CompactPoint2 firstPoint;
#endif

	private EdgeSlope firstSlope = EdgeSlope.None;

	// previous point tracks the last point added to the edge which
	// hasn't actually been added to the vertices list yet because
	// it might be extended by additional points with the same slope
	private CompactPoint2 previousPoint;

	private EdgeSlope previousSlope;

	public EdgePointsLinker(CompactPoint2[] points)
	{
		Debug.Assert(points != null);

		this.points = points;
	}

	public bool IsEdgeOpen => this.firstSlope != EdgeSlope.None;

	public bool IsEdgeClosed => this.firstSlope == EdgeSlope.None;

	public void Initialize(Int2 offsetOutput)
	{
		this.offsetOutput = offsetOutput;
		this.pointsCount = 0;
		this.firstSlope = EdgeSlope.None;
	}

	[Conditional(CompilationSymbol.Debug)]
	public void SetFirstPoint(int x, int y)
	{
#if DEBUG
		Debug.Assert(this.IsEdgeClosed);

		this.firstPoint = CompactPoint2.From(x + this.offsetOutput.X, y + this.offsetOutput.Y);
#endif
	}

	public void StartEdge(int x, int y, EdgeSlope slope)
	{
		var nextPoint = CompactPoint2.From(x + this.offsetOutput.X, y + this.offsetOutput.Y);

		Debug.Assert(this.IsEdgeClosed);
		Debug.Assert(slope != EdgeSlope.None);
#if DEBUG
		Debug.Assert(this.firstPoint != nextPoint);
#endif

		this.startIndex = this.pointsCount;
		this.previousPoint = nextPoint;
		this.firstSlope = slope;
		this.previousSlope = slope;
	}

	public void AddPoint(int x, int y, EdgeSlope nextSlope)
	{
		var nextPoint = CompactPoint2.From(x + this.offsetOutput.X, y + this.offsetOutput.Y);

		Debug.Assert(this.IsEdgeOpen);
		Debug.Assert(nextPoint != this.previousPoint);
		Debug.Assert(nextSlope != EdgeSlope.None);

		if (nextSlope == this.previousSlope)
		{
			// the slopes match so replace previous point with next point
			this.previousPoint = nextPoint;
		}
		else
		{
			// the slopes don't match so add previous as a new point
			this.points[this.pointsCount] = this.previousPoint;
			this.pointsCount++;
			this.previousPoint = nextPoint;
			this.previousSlope = nextSlope;
		}
	}

	public EdgeSpan EndEdge()
	{
		Debug.Assert(this.IsEdgeOpen);
#if DEBUG
		Debug.Assert(this.previousPoint == this.firstPoint);
#endif

		if (this.previousSlope != this.firstSlope)
		{
			// the slopes don't match so add previous as a new point
			this.points[this.pointsCount] = this.previousPoint;
			this.pointsCount++;
		}

		this.firstSlope = EdgeSlope.None;
		int count = this.pointsCount - this.startIndex;

		Debug.Assert(count >= Constants.VerticesPerTriangle);

		return new EdgeSpan(this.startIndex, count);
	}
}
