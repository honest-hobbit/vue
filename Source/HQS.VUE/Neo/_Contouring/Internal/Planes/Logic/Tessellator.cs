﻿using LibTessDotNet;

namespace HQS.VUE.Neo;

internal class Tessellator<TSurface>
	where TSurface : unmanaged
{
	private readonly PlaneContourerProfiler profiler;

	private readonly EdgePoints edgePoints;

	private readonly Tess tessellator;

	private readonly TriangleCombiner<TSurface> combiner;

	public Tessellator(
		PlaneContourerProfiler profiler, EdgePoints edgePoints, Pools tessPools, int trianglesCapacity)
	{
		Debug.Assert(edgePoints != null);
		Debug.Assert(tessPools != null);
		Debug.Assert(trianglesCapacity >= 0);

		this.profiler = profiler;
		this.edgePoints = edgePoints;
		this.tessellator = new Tess(
			tessPools,
			trianglesCapacity * Constants.VerticesPerTriangle,
			trianglesCapacity * Constants.ElementsPerTriangle)
		{
			NoEmptyPolygons = true,
		};

		this.combiner = new TriangleCombiner<TSurface>(this.tessellator, trianglesCapacity);
	}

	public void Initialize(bool reverseOutput, CoplanarChunk<TSurface>.Builder output) =>
		this.combiner.Initialize(reverseOutput, output);

	[Conditional(VUECompilationSymbol.Profile)]
	public void ProfileObjectAllocations()
	{
		this.profiler.SetLibTessPooledMeshes(this.tessellator.Pools.MeshPool.Created);
		this.profiler.SetLibTessPooledVertices(this.tessellator.Pools.VertexPool.Created);
		this.profiler.SetLibTessPooledFaces(this.tessellator.Pools.FacePool.Created);
		this.profiler.SetLibTessPooledEdges(this.tessellator.Pools.EdgePool.Created);
		this.profiler.SetLibTessPooledRegions(this.tessellator.Pools.RegionPool.Created);
		this.profiler.SetLibTessPooledNodes(this.tessellator.Pools.NodePool.Created);
		this.profiler.SetLibTessVerticesLength(this.tessellator.Vertices.Length);
		this.profiler.SetLibTessElementsLength(this.tessellator.Elements.Length);
		this.profiler.SetTriangleCombinerLength(this.combiner.TriangleCapacity);
	}

	// returns the number of triangles added to output
	public int TessellateComplexPolygon(Polygon<TSurface> polygon)
	{
		Debug.Assert(polygon.HasHoles);

		this.profiler.StartTriangulateComplex();
		this.profiler.StartLibTessAddContours();

		foreach (var edge in polygon)
		{
			this.edgePoints.SetToEdgeSpan(edge);
			this.tessellator.AddContour(this.edgePoints, ContourOrientation.Original);
		}

		this.profiler.StopLibTessAddContours();

		int triagleCount = this.Tessellate();

		this.profiler.StopTriangulateComplex();
		this.ProfileMissedTriangles();
		this.profiler.AddToPolygonsComplex(1);

		return triagleCount;
	}

	// returns the number of triangles added to output
	public int TessellateSimplePolygon(EdgeSpan edge)
	{
		Debug.Assert(edge.Count > Constants.VerticesPerQuad);

		this.profiler.StartTriangulateSimple();
		this.profiler.StartLibTessAddContours();

		this.edgePoints.SetToEdgeSpan(edge);
		this.tessellator.AddContour(this.edgePoints, ContourOrientation.Original);

		this.profiler.StopLibTessAddContours();

		int triagleCount = this.Tessellate();

		this.profiler.StopTriangulateSimple();
		this.ProfileMissedTriangles();
		this.profiler.AddToPolygonsSimple(1);

		return triagleCount;
	}

	// returns the number of triangles added to output
	private int Tessellate()
	{
		this.profiler.StartLibTessTessellate();

		this.tessellator.Tessellate(
			WindingRule.EvenOdd,
			ElementType.ConnectedPolygons,
			Constants.VerticesPerTriangle,
			null,
			new Vec3(0, 0, -1));

		this.profiler.StopLibTessTessellate();

		Debug.Assert(this.tessellator.Pools.AllObjectsAvailable);

		this.profiler.StartPrepareTriangles();

		this.combiner.PrepareTriangles();

		this.profiler.StopPrepareTriangles();
		this.profiler.StartCombineTriangles();

		int combined = this.combiner.CombineTrianglesEfficient();

		this.profiler.StopCombineTriangles();
		this.profiler.AddToTrianglesCombined(combined);
		this.profiler.StartRecordTriangles();

		this.combiner.RecordTriangles();

		this.profiler.StopRecordTriangles();

		return this.combiner.TriangleCount;
	}

	[Conditional(VUECompilationSymbol.Profile)]
	private void ProfileMissedTriangles()
	{
		if (!this.profiler.CheckForTrianglesMissed) { return; }

		this.profiler.StopTriangulateTotal();
		this.profiler.StopContourPlaneTotal();

		int missed = this.combiner.CombineTrianglesThorough();

		this.profiler.AddToTrianglesMissed(missed);
		this.profiler.StartContourPlaneTotal();
		this.profiler.StartTriangulateTotal();
	}
}
