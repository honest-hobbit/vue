﻿namespace HQS.VUE.Neo;

internal static class PointMathUtility
{
	public static float Slope(Point2 a, Point2 b)
	{
		Debug.Assert(a != b);

		return (float)(a.Y - b.Y) / (a.X - b.X);
	}

	// This is the 2D equivalent of the 3D cross product when Z of both inputs is 0.
	// The X and Y of the output will both always be 0 so just return Z as a scalar.
	public static int Cross(Int2 a, Int2 b)
	{
		Debug.Assert(a != b);

		return (a.X * b.Y) - (a.Y * b.X);
	}
}
