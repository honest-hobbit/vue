﻿namespace HQS.VUE.Neo;

internal struct EdgeTracer<TSurface, TComparer>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	private readonly SurfacePlane<TSurface> surfaces;

	private readonly EdgePlane<TSurface, TComparer> edges;

	private readonly EdgePointsLinker edgeLinker;

	private readonly SquareArrayIndexer indexer;

	private TSurface surface;

	private int startIndex;

	private EdgeParts startEdgePart;

	private int index;

	private EdgeParts previousEdgePart;

	private EdgeParts edgePartsAtIndex;

	private int x;

	private int y;

	private int pX;

	private int pY;

	private bool isEdgeOpen;

	public EdgeTracer(
		SurfacePlane<TSurface> surfaces,
		EdgePlane<TSurface, TComparer> edges,
		EdgePointsLinker edgeLinker)
		: this()
	{
		Debug.Assert(surfaces != null);
		Debug.Assert(edges != null);
		Debug.Assert(surfaces.Size == edges.Size);
		Debug.Assert(edgeLinker != null);

		this.surfaces = surfaces;
		this.edges = edges;
		this.edgeLinker = edgeLinker;
		this.indexer = surfaces.Indexer;
	}

	public void FindHoles(Int2 start, Int2 dimensions)
	{
		Debug.Assert(dimensions.X >= 0);
		Debug.Assert(dimensions.Y >= 0);

		int maxX = start.X + dimensions.X;
		int maxY = start.Y + dimensions.Y;

		// CheckForEdges will change this.x and this.y when it calls LoopAroundEdge
		// but that loop will continue until it gets back to where it began
		// thus always restoring this.x and this.y to their original values
		// so there's no need to use separate iX and iY values here
		for (this.y = start.Y; this.y < maxY; this.y++)
		{
			for (this.x = start.X; this.x < maxX; this.x++)
			{
				this.CheckForEdges();
			}
		}
	}

	private void CheckForEdges()
	{
		this.index = this.indexer[this.x, this.y];
		var edges = this.edges.GetEdgeQuad(this.index);
		this.edgePartsAtIndex = edges.Parts;

		// check if an unassigned edge exists in the Top voxel to trace
		if (this.edgePartsAtIndex.HasAny(EdgeParts.TopAll) &&
			edges.TopID == EdgeState.UnassignedID)
		{
			this.InitializeEdgeStart();
			this.edges.SetTopEdgeID(this.index);
			this.surface = this.surfaces[this.index].Top;

			if (this.edgePartsAtIndex.Has(EdgeParts.TopOut))
			{
				this.previousEdgePart = EdgeParts.TopOut;
				this.edgeLinker.SetFirstPoint(this.pX, this.pY + 2);
				this.edgeLinker.StartEdge(this.pX + 2, this.pY + 2, EdgeSlope.Horizontal);
			}
			else if (this.edgePartsAtIndex.Has(EdgeParts.TopRight))
			{
				this.previousEdgePart = EdgeParts.TopRight;
				this.edgeLinker.SetFirstPoint(this.pX + 2, this.pY + 2);
				this.edgeLinker.StartEdge(this.pX + 1, this.pY + 1, EdgeSlope.Ascending);
			}
			else
			{
				Debug.Assert(this.edgePartsAtIndex.Has(EdgeParts.TopLeft));

				this.previousEdgePart = EdgeParts.TopLeft;
				this.edgeLinker.SetFirstPoint(this.pX + 1, this.pY + 1);
				this.edgeLinker.StartEdge(this.pX, this.pY + 2, EdgeSlope.Descending);
			}

			this.LoopAroundEdge();
			edges = this.edges.GetEdgeQuad(this.index);
		}

		// check if an unassigned edge exists in the Right voxel to trace
		if (this.edgePartsAtIndex.HasAny(EdgeParts.RightAll) &&
			edges.RightID == EdgeState.UnassignedID)
		{
			this.InitializeEdgeStart();
			this.edges.SetRightEdgeID(this.index);
			this.surface = this.surfaces[this.index].Right;

			if (this.edgePartsAtIndex.Has(EdgeParts.RightOut))
			{
				this.previousEdgePart = EdgeParts.RightOut;
				this.edgeLinker.SetFirstPoint(this.pX + 2, this.pY + 2);
				this.edgeLinker.StartEdge(this.pX + 2, this.pY, EdgeSlope.Vertical);
			}
			else if (this.edgePartsAtIndex.Has(EdgeParts.RightBottom))
			{
				this.previousEdgePart = EdgeParts.RightBottom;
				this.edgeLinker.SetFirstPoint(this.pX + 2, this.pY);
				this.edgeLinker.StartEdge(this.pX + 1, this.pY + 1, EdgeSlope.Descending);
			}
			else
			{
				Debug.Assert(this.edgePartsAtIndex.Has(EdgeParts.RightTop));

				this.previousEdgePart = EdgeParts.RightTop;
				this.edgeLinker.SetFirstPoint(this.pX + 1, this.pY + 1);
				this.edgeLinker.StartEdge(this.pX + 2, this.pY + 2, EdgeSlope.Ascending);
			}

			this.LoopAroundEdge();
			edges = this.edges.GetEdgeQuad(this.index);
		}

		// check if an unassigned edge exists in the Bottom voxel to trace
		if (this.edgePartsAtIndex.HasAny(EdgeParts.BottomAll) &&
			edges.BottomID == EdgeState.UnassignedID)
		{
			this.InitializeEdgeStart();
			this.edges.SetBottomEdgeID(this.index);
			this.surface = this.surfaces[this.index].Bottom;

			if (this.edgePartsAtIndex.Has(EdgeParts.BottomOut))
			{
				this.previousEdgePart = EdgeParts.BottomOut;
				this.edgeLinker.SetFirstPoint(this.pX + 2, this.pY);
				this.edgeLinker.StartEdge(this.pX, this.pY, EdgeSlope.Horizontal);
			}
			else if (this.edgePartsAtIndex.Has(EdgeParts.BottomLeft))
			{
				this.previousEdgePart = EdgeParts.BottomLeft;
				this.edgeLinker.SetFirstPoint(this.pX, this.pY);
				this.edgeLinker.StartEdge(this.pX + 1, this.pY + 1, EdgeSlope.Ascending);
			}
			else
			{
				Debug.Assert(this.edgePartsAtIndex.Has(EdgeParts.BottomRight));

				this.previousEdgePart = EdgeParts.BottomRight;
				this.edgeLinker.SetFirstPoint(this.pX + 1, this.pY + 1);
				this.edgeLinker.StartEdge(this.pX + 2, this.pY, EdgeSlope.Descending);
			}

			this.LoopAroundEdge();
			edges = this.edges.GetEdgeQuad(this.index);
		}

		// check if an unassigned edge exists in the Left voxel to trace
		if (this.edgePartsAtIndex.HasAny(EdgeParts.LeftAll) &&
			edges.LeftID == EdgeState.UnassignedID)
		{
			this.InitializeEdgeStart();
			this.edges.SetLeftEdgeID(this.index);
			this.surface = this.surfaces[this.index].Left;

			if (this.edgePartsAtIndex.Has(EdgeParts.LeftOut))
			{
				this.previousEdgePart = EdgeParts.LeftOut;
				this.edgeLinker.SetFirstPoint(this.pX, this.pY);
				this.edgeLinker.StartEdge(this.pX, this.pY + 2, EdgeSlope.Vertical);
			}
			else if (this.edgePartsAtIndex.Has(EdgeParts.LeftTop))
			{
				this.previousEdgePart = EdgeParts.LeftTop;
				this.edgeLinker.SetFirstPoint(this.pX, this.pY + 2);
				this.edgeLinker.StartEdge(this.pX + 1, this.pY + 1, EdgeSlope.Descending);
			}
			else
			{
				Debug.Assert(this.edgePartsAtIndex.Has(EdgeParts.LeftBottom));

				this.previousEdgePart = EdgeParts.LeftBottom;
				this.edgeLinker.SetFirstPoint(this.pX + 1, this.pY + 1);
				this.edgeLinker.StartEdge(this.pX, this.pY, EdgeSlope.Ascending);
			}

			// don't bother reassigning 'edges' because it will be reassigned when the loop repeats
			this.LoopAroundEdge();
		}
	}

	private void InitializeEdgeStart()
	{
		this.pX = this.x << 1;
		this.pY = this.y << 1;
		this.edges.StartEdgeID();
	}

	[SuppressMessage("StyleCop", "SA1107", Justification = "Compacting massive but simple switch statement.")]
	private void LoopAroundEdge()
	{
		// store the starting edge so the loop can end when the same edge is reached again
		this.startIndex = this.index;
		this.startEdgePart = this.previousEdgePart;

		// trace all the remaining segments of the edge loop
		this.isEdgeOpen = true;
		while (this.isEdgeOpen)
		{
			switch (this.previousEdgePart)
			{
				case EdgeParts.TopOut:
					if (this.edgePartsAtIndex.Has(EdgeParts.TopRight)) { this.AddTopRightEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.RightOut)) { this.AddRightOutEdge(); break; }
					this.MoveRight();
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftTop)) { this.AddLeftTopEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.TopOut)) { this.AddTopOutEdge(); break; }
					this.MoveUp();
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomLeft)) { this.AddBottomLeftEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftOut)) { this.AddLeftOutEdge(); break; }
					this.MoveLeft();
					this.AddRightBottomEdge();
					break;

				case EdgeParts.TopRight:
					if (this.edgePartsAtIndex.Has(EdgeParts.TopLeft)) { this.AddTopLeftEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftBottom)) { this.AddLeftBottomEdge(); break; }
					this.AddBottomRightEdge();
					break;

				case EdgeParts.TopLeft:
					if (this.edgePartsAtIndex.Has(EdgeParts.TopOut)) { this.AddTopOutEdge(); break; }
					this.MoveUp();
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomLeft)) { this.AddBottomLeftEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftOut)) { this.AddLeftOutEdge(); break; }
					this.MoveLeft();
					if (this.edgePartsAtIndex.Has(EdgeParts.RightBottom)) { this.AddRightBottomEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomOut)) { this.AddBottomOutEdge(); break; }
					this.MoveDown();
					if (this.edgePartsAtIndex.Has(EdgeParts.TopRight)) { this.AddTopRightEdge(); break; }
					this.AddRightOutEdge();
					break;

				case EdgeParts.RightOut:
					if (this.edgePartsAtIndex.Has(EdgeParts.RightBottom)) { this.AddRightBottomEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomOut)) { this.AddBottomOutEdge(); break; }
					this.MoveDown();
					if (this.edgePartsAtIndex.Has(EdgeParts.TopRight)) { this.AddTopRightEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.RightOut)) { this.AddRightOutEdge(); break; }
					this.MoveRight();
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftTop)) { this.AddLeftTopEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.TopOut)) { this.AddTopOutEdge(); break; }
					this.MoveUp();
					this.AddBottomLeftEdge();
					break;

				case EdgeParts.RightBottom:
					if (this.edgePartsAtIndex.Has(EdgeParts.RightTop)) { this.AddRightTopEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.TopLeft)) { this.AddTopLeftEdge(); break; }
					this.AddLeftBottomEdge();
					break;

				case EdgeParts.RightTop:
					if (this.edgePartsAtIndex.Has(EdgeParts.RightOut)) { this.AddRightOutEdge(); break; }
					this.MoveRight();
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftTop)) { this.AddLeftTopEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.TopOut)) { this.AddTopOutEdge(); break; }
					this.MoveUp();
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomLeft)) { this.AddBottomLeftEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftOut)) { this.AddLeftOutEdge(); break; }
					this.MoveLeft();
					if (this.edgePartsAtIndex.Has(EdgeParts.RightBottom)) { this.AddRightBottomEdge(); break; }
					this.AddBottomOutEdge();
					break;

				case EdgeParts.BottomOut:
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomLeft)) { this.AddBottomLeftEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftOut)) { this.AddLeftOutEdge(); break; }
					this.MoveLeft();
					if (this.edgePartsAtIndex.Has(EdgeParts.RightBottom)) { this.AddRightBottomEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomOut)) { this.AddBottomOutEdge(); break; }
					this.MoveDown();
					if (this.edgePartsAtIndex.Has(EdgeParts.TopRight)) { this.AddTopRightEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.RightOut)) { this.AddRightOutEdge(); break; }
					this.MoveRight();
					this.AddLeftTopEdge();
					break;

				case EdgeParts.BottomLeft:
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomRight)) { this.AddBottomRightEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.RightTop)) { this.AddRightTopEdge(); break; }
					this.AddTopLeftEdge();
					break;

				case EdgeParts.BottomRight:
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomOut)) { this.AddBottomOutEdge(); break; }
					this.MoveDown();
					if (this.edgePartsAtIndex.Has(EdgeParts.TopRight)) { this.AddTopRightEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.RightOut)) { this.AddRightOutEdge(); break; }
					this.MoveRight();
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftTop)) { this.AddLeftTopEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.TopOut)) { this.AddTopOutEdge(); break; }
					this.MoveUp();
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomLeft)) { this.AddBottomLeftEdge(); break; }
					this.AddLeftOutEdge();
					break;

				case EdgeParts.LeftOut:
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftTop)) { this.AddLeftTopEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.TopOut)) { this.AddTopOutEdge(); break; }
					this.MoveUp();
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomLeft)) { this.AddBottomLeftEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftOut)) { this.AddLeftOutEdge(); break; }
					this.MoveLeft();
					if (this.edgePartsAtIndex.Has(EdgeParts.RightBottom)) { this.AddRightBottomEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomOut)) { this.AddBottomOutEdge(); break; }
					this.MoveDown();
					this.AddTopRightEdge();
					break;

				case EdgeParts.LeftTop:
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftBottom)) { this.AddLeftBottomEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomRight)) { this.AddBottomRightEdge(); break; }
					this.AddRightTopEdge();
					break;

				case EdgeParts.LeftBottom:
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftOut)) { this.AddLeftOutEdge(); break; }
					this.MoveLeft();
					if (this.edgePartsAtIndex.Has(EdgeParts.RightBottom)) { this.AddRightBottomEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.BottomOut)) { this.AddBottomOutEdge(); break; }
					this.MoveDown();
					if (this.edgePartsAtIndex.Has(EdgeParts.TopRight)) { this.AddTopRightEdge(); break; }
					if (this.edgePartsAtIndex.Has(EdgeParts.RightOut)) { this.AddRightOutEdge(); break; }
					this.MoveRight();
					if (this.edgePartsAtIndex.Has(EdgeParts.LeftTop)) { this.AddLeftTopEdge(); break; }
					this.AddTopOutEdge();
					break;

				default: throw InvalidEnumArgument.CreateException(nameof(this.previousEdgePart), this.previousEdgePart);
			}
		}
	}

	private void MoveRight()
	{
		this.x++;
		this.pX = this.x << 1;
		this.index = this.indexer[this.x, this.y];
		this.edgePartsAtIndex = this.edges.GetEdgeParts(this.index);
	}

	private void MoveLeft()
	{
		this.x--;
		this.pX = this.x << 1;
		this.index = this.indexer[this.x, this.y];
		this.edgePartsAtIndex = this.edges.GetEdgeParts(this.index);
	}

	private void MoveUp()
	{
		this.y++;
		this.pY = this.y << 1;
		this.index = this.indexer[this.x, this.y];
		this.edgePartsAtIndex = this.edges.GetEdgeParts(this.index);
	}

	private void MoveDown()
	{
		this.y--;
		this.pY = this.y << 1;
		this.index = this.indexer[this.x, this.y];
		this.edgePartsAtIndex = this.edges.GetEdgeParts(this.index);
	}

	private void AddEdge(EdgeParts edge, int x, int y, EdgeSlope slope)
	{
		Debug.Assert(edge != EdgeParts.None);
		Debug.Assert(this.edgePartsAtIndex.Has(edge));
		Debug.Assert(slope != EdgeSlope.None);

		if (this.index == this.startIndex && this.startEdgePart == edge)
		{
			this.edges.EndEdgeID(this.surface, this.edgeLinker.EndEdge());
			this.isEdgeOpen = false;
			return;
		}

		this.edgeLinker.AddPoint(this.pX + x, this.pY + y, slope);
		this.previousEdgePart = edge;
	}

	private void AddTopOutEdge()
	{
		this.AddEdge(EdgeParts.TopOut, 2, 2, EdgeSlope.Horizontal);
		this.edges.SetTopEdgeID(this.index);
	}

	private void AddTopRightEdge()
	{
		this.AddEdge(EdgeParts.TopRight, 1, 1, EdgeSlope.Ascending);
		this.edges.SetTopEdgeID(this.index);
	}

	private void AddTopLeftEdge()
	{
		this.AddEdge(EdgeParts.TopLeft, 0, 2, EdgeSlope.Descending);
		this.edges.SetTopEdgeID(this.index);
	}

	private void AddRightOutEdge()
	{
		this.AddEdge(EdgeParts.RightOut, 2, 0, EdgeSlope.Vertical);
		this.edges.SetRightEdgeID(this.index);
	}

	private void AddRightBottomEdge()
	{
		this.AddEdge(EdgeParts.RightBottom, 1, 1, EdgeSlope.Descending);
		this.edges.SetRightEdgeID(this.index);
	}

	private void AddRightTopEdge()
	{
		this.AddEdge(EdgeParts.RightTop, 2, 2, EdgeSlope.Ascending);
		this.edges.SetRightEdgeID(this.index);
	}

	private void AddBottomOutEdge()
	{
		this.AddEdge(EdgeParts.BottomOut, 0, 0, EdgeSlope.Horizontal);
		this.edges.SetBottomEdgeID(this.index);
	}

	private void AddBottomLeftEdge()
	{
		this.AddEdge(EdgeParts.BottomLeft, 1, 1, EdgeSlope.Ascending);
		this.edges.SetBottomEdgeID(this.index);
	}

	private void AddBottomRightEdge()
	{
		this.AddEdge(EdgeParts.BottomRight, 2, 0, EdgeSlope.Descending);
		this.edges.SetBottomEdgeID(this.index);
	}

	private void AddLeftOutEdge()
	{
		this.AddEdge(EdgeParts.LeftOut, 0, 2, EdgeSlope.Vertical);
		this.edges.SetLeftEdgeID(this.index);
	}

	private void AddLeftTopEdge()
	{
		this.AddEdge(EdgeParts.LeftTop, 1, 1, EdgeSlope.Descending);
		this.edges.SetLeftEdgeID(this.index);
	}

	private void AddLeftBottomEdge()
	{
		this.AddEdge(EdgeParts.LeftBottom, 0, 0, EdgeSlope.Ascending);
		this.edges.SetLeftEdgeID(this.index);
	}
}
