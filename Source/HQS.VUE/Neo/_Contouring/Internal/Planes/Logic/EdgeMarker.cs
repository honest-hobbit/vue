﻿namespace HQS.VUE.Neo;

internal class EdgeMarker<TSurface, TComparer> : ForLoopIterator2
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	private readonly EdgePlane<TSurface, TComparer> edges;

	private readonly SquareArrayIndexer indexer;

	private SurfacePlane<TSurface> surfaces;

	private TComparer comparer;

	public EdgeMarker(EdgePlane<TSurface, TComparer> edges)
	{
		Debug.Assert(edges != null);

		this.edges = edges;
		this.indexer = edges.Indexer;
	}

	public void Initialize(SurfacePlane<TSurface> surfaces, TComparer comparer)
	{
		Debug.Assert(surfaces != null);
		Debug.Assert(surfaces.Size == this.edges.Size);

		this.surfaces = surfaces;
		this.comparer = comparer;
	}

	/// <inheritdoc />
	protected override void None()
	{
	}

	/// <inheritdoc />
	protected override void Single(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: true, isHighX: true, isLowY: true, isHighY: true);
	}

	/// <inheritdoc />
	protected override void LineHorizontalStartLow(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: true, isHighX: false, isLowY: true, isHighY: true);
	}

	/// <inheritdoc />
	protected override void LineHorizontalEndHigh(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: false, isHighX: true, isLowY: true, isHighY: true);
	}

	/// <inheritdoc />
	protected override void LineHorizontal(Int2 start, int lengthX)
	{
		int maxX = start.X + lengthX;
		int iY = start.Y;

		for (int iX = start.X; iX < maxX; iX++)
		{
			this.CheckCell(iX, iY, isLowX: false, isHighX: false, isLowY: true, isHighY: true);
		}
	}

	/// <inheritdoc />
	protected override void LineVerticalStartLow(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: true, isHighX: true, isLowY: true, isHighY: false);
	}

	/// <inheritdoc />
	protected override void LineVerticalEndHigh(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: true, isHighX: true, isLowY: false, isHighY: true);
	}

	/// <inheritdoc />
	protected override void LineVertical(Int2 start, int lengthY)
	{
		int maxY = start.Y + lengthY;
		int iX = start.X;

		for (int iY = start.Y; iY < maxY; iY++)
		{
			this.CheckCell(iX, iY, isLowX: true, isHighX: true, isLowY: false, isHighY: false);
		}
	}

	/// <inheritdoc />
	protected override void CornerLowXLowY(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: true, isHighX: false, isLowY: true, isHighY: false);
	}

	/// <inheritdoc />
	protected override void CornerLowXHighY(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: true, isHighX: false, isLowY: false, isHighY: true);
	}

	/// <inheritdoc />
	protected override void CornerHighXLowY(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: false, isHighX: true, isLowY: true, isHighY: false);
	}

	/// <inheritdoc />
	protected override void CornerHighXHighY(Int2 index)
	{
		this.CheckCell(index.X, index.Y, isLowX: false, isHighX: true, isLowY: false, isHighY: true);
	}

	/// <inheritdoc />
	protected override void EdgeHorizontalLowY(Int2 start, int lengthX)
	{
		int maxX = start.X + lengthX;
		int iY = start.Y;

		for (int iX = start.X; iX < maxX; iX++)
		{
			this.CheckCell(iX, iY, isLowX: false, isHighX: false, isLowY: true, isHighY: false);
		}
	}

	/// <inheritdoc />
	protected override void EdgeHorizontalHighY(Int2 start, int lengthX)
	{
		int maxX = start.X + lengthX;
		int iY = start.Y;

		for (int iX = start.X; iX < maxX; iX++)
		{
			this.CheckCell(iX, iY, isLowX: false, isHighX: false, isLowY: false, isHighY: true);
		}
	}

	/// <inheritdoc />
	protected override void EdgeVerticalLowX(Int2 start, int lengthY)
	{
		int maxY = start.Y + lengthY;
		int iX = start.X;

		for (int iY = start.Y; iY < maxY; iY++)
		{
			this.CheckCell(iX, iY, isLowX: true, isHighX: false, isLowY: false, isHighY: false);
		}
	}

	/// <inheritdoc />
	protected override void EdgeVerticalHighX(Int2 start, int lengthY)
	{
		int maxY = start.Y + lengthY;
		int iX = start.X;

		for (int iY = start.Y; iY < maxY; iY++)
		{
			this.CheckCell(iX, iY, isLowX: false, isHighX: true, isLowY: false, isHighY: false);
		}
	}

	/// <inheritdoc />
	protected override void Inside(Int2 start, Int2 dimensions)
	{
		int maxX = start.X + dimensions.X;
		int maxY = start.Y + dimensions.Y;

		for (int iY = start.Y; iY < maxY; iY++)
		{
			for (int iX = start.X; iX < maxX; iX++)
			{
				int index = this.indexer[iX, iY];
				var surface = this.surfaces[index];

				bool isTopFilled = !this.comparer.IsEmpty(surface.Top);
				bool isRightFilled = !this.comparer.IsEmpty(surface.Right);
				bool isBottomFilled = !this.comparer.IsEmpty(surface.Bottom);
				bool isLeftFilled = !this.comparer.IsEmpty(surface.Left);

				if (!(isTopFilled || isRightFilled || isBottomFilled || isLeftFilled))
				{
					// this is just an optimization
					continue;
				}

				var edges = EdgeParts.None;

				if (!this.comparer.IsSame(surface.Bottom, surface.Left))
				{
					if (isBottomFilled) { edges |= EdgeParts.BottomLeft; }
					if (isLeftFilled) { edges |= EdgeParts.LeftBottom; }
				}

				if (!this.comparer.IsSame(surface.Left, surface.Top))
				{
					if (isLeftFilled) { edges |= EdgeParts.LeftTop; }
					if (isTopFilled) { edges |= EdgeParts.TopLeft; }
				}

				if (!this.comparer.IsSame(surface.Top, surface.Right))
				{
					if (isTopFilled) { edges |= EdgeParts.TopRight; }
					if (isRightFilled) { edges |= EdgeParts.RightTop; }
				}

				if (!this.comparer.IsSame(surface.Right, surface.Bottom))
				{
					if (isRightFilled) { edges |= EdgeParts.RightBottom; }
					if (isBottomFilled) { edges |= EdgeParts.BottomRight; }
				}

				if (isLeftFilled && !this.comparer.IsSame(surface.Left, this.surfaces[iX - 1, iY].Right))
				{
					edges |= EdgeParts.LeftOut;
				}

				if (isRightFilled && !this.comparer.IsSame(surface.Right, this.surfaces[iX + 1, iY].Left))
				{
					edges |= EdgeParts.RightOut;
				}

				if (isBottomFilled && !this.comparer.IsSame(surface.Bottom, this.surfaces[iX, iY - 1].Top))
				{
					edges |= EdgeParts.BottomOut;
				}

				if (isTopFilled && !this.comparer.IsSame(surface.Top, this.surfaces[iX, iY + 1].Bottom))
				{
					edges |= EdgeParts.TopOut;
				}

				this.edges.SetEdgeParts(index, edges);
			}
		}
	}

	private void CheckCell(int iX, int iY, bool isLowX, bool isHighX, bool isLowY, bool isHighY)
	{
		int index = this.indexer[iX, iY];
		var surface = this.surfaces[index];

		bool isTopFilled = !this.comparer.IsEmpty(surface.Top);
		bool isRightFilled = !this.comparer.IsEmpty(surface.Right);
		bool isBottomFilled = !this.comparer.IsEmpty(surface.Bottom);
		bool isLeftFilled = !this.comparer.IsEmpty(surface.Left);

		if (!(isTopFilled || isRightFilled || isBottomFilled || isLeftFilled))
		{
			// this is just an optimization
			return;
		}

		var edges = EdgeParts.None;

		if (!this.comparer.IsSame(surface.Bottom, surface.Left))
		{
			if (isBottomFilled) { edges |= EdgeParts.BottomLeft; }
			if (isLeftFilled) { edges |= EdgeParts.LeftBottom; }
		}

		if (!this.comparer.IsSame(surface.Left, surface.Top))
		{
			if (isLeftFilled) { edges |= EdgeParts.LeftTop; }
			if (isTopFilled) { edges |= EdgeParts.TopLeft; }
		}

		if (!this.comparer.IsSame(surface.Top, surface.Right))
		{
			if (isTopFilled) { edges |= EdgeParts.TopRight; }
			if (isRightFilled) { edges |= EdgeParts.RightTop; }
		}

		if (!this.comparer.IsSame(surface.Right, surface.Bottom))
		{
			if (isRightFilled) { edges |= EdgeParts.RightBottom; }
			if (isBottomFilled) { edges |= EdgeParts.BottomRight; }
		}

		if (isLeftFilled && (isLowX || !this.comparer.IsSame(surface.Left, this.surfaces[iX - 1, iY].Right)))
		{
			edges |= EdgeParts.LeftOut;
		}

		if (isRightFilled && (isHighX || !this.comparer.IsSame(surface.Right, this.surfaces[iX + 1, iY].Left)))
		{
			edges |= EdgeParts.RightOut;
		}

		if (isBottomFilled && (isLowY || !this.comparer.IsSame(surface.Bottom, this.surfaces[iX, iY - 1].Top)))
		{
			edges |= EdgeParts.BottomOut;
		}

		if (isTopFilled && (isHighY || !this.comparer.IsSame(surface.Top, this.surfaces[iX, iY + 1].Bottom)))
		{
			edges |= EdgeParts.TopOut;
		}

		this.edges.SetEdgeParts(index, edges);
	}
}
