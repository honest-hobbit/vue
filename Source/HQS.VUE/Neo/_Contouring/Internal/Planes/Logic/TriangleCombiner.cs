﻿using LibTessDotNet;

namespace HQS.VUE.Neo;

internal class TriangleCombiner<TSurface>
	where TSurface : unmanaged
{
	private readonly Tess tessellator;

	private readonly FastList<ConnectedTriangle> trianglesList;

	private ConnectedTriangle[] triangles;

	private bool reverseOutput;

	private CoplanarChunk<TSurface>.Builder output;

	public TriangleCombiner(Tess tessellator, int trianglesCapacity)
	{
		Debug.Assert(tessellator != null);
		Debug.Assert(trianglesCapacity >= 0);

		this.tessellator = tessellator;
		this.trianglesList = new FastList<ConnectedTriangle>(trianglesCapacity);
	}

	public int TriangleCapacity => this.trianglesList.Array.Length;

	public int TriangleCount { get; private set; }

	public void Initialize(bool reverseOutput, CoplanarChunk<TSurface>.Builder output)
	{
		Debug.Assert(output != null);

		this.reverseOutput = reverseOutput;
		this.output = output;
	}

	public void RecordTriangles()
	{
		Debug.Assert(this.output != null);

		int length = this.trianglesList.Count;

		if (this.reverseOutput)
		{
			for (int i = 0; i < length; i++)
			{
				if (!this.triangles[i].IsDeleted)
				{
					this.output.AddTriangle(new CompactTriangle(
						CompactPoint2.From(this.triangles[i].PointA),
						CompactPoint2.From(this.triangles[i].PointC),
						CompactPoint2.From(this.triangles[i].PointB)));
				}
			}
		}
		else
		{
			for (int i = 0; i < length; i++)
			{
				if (!this.triangles[i].IsDeleted)
				{
					this.output.AddTriangle(new CompactTriangle(
						CompactPoint2.From(this.triangles[i].PointA),
						CompactPoint2.From(this.triangles[i].PointB),
						CompactPoint2.From(this.triangles[i].PointC)));
				}
			}
		}
	}

	public void PrepareTriangles()
	{
		int triangleCount = this.tessellator.ElementCount;
		var inputTriangles = this.tessellator.Elements;
		var vertices = this.tessellator.Vertices;

		Point2 GetPoint(int i)
		{
			var vertex = vertices[inputTriangles[i]].Position;
			return new Point2((ushort)vertex.X, (ushort)vertex.Y);
		}

		this.trianglesList.FastClear();
		this.trianglesList.EnsureCapacity(triangleCount);

		// copy the data from LibTess into a form that's easier to work with
		int length = triangleCount * Constants.ElementsPerTriangle;
		for (int i = 0; i < length;)
		{
			this.trianglesList.AddFixedCapacity(new ConnectedTriangle()
			{
				PointA = GetPoint(i++),
				PointB = GetPoint(i++),
				PointC = GetPoint(i++),
				EdgeAB = inputTriangles[i++],
				EdgeBC = inputTriangles[i++],
				EdgeCA = inputTriangles[i++],
			});
		}

		this.TriangleCount = this.trianglesList.Count;
		this.triangles = this.trianglesList.Array;
	}

	// return the number of triangles combined
	public int CombineTrianglesEfficient()
	{
		int initialTriangles = this.TriangleCount;

		this.CombineTrianglesClockwise();
		////this.CombineTrianglesBothDirections();

		return initialTriangles - this.TriangleCount;
	}

	// return the number of triangles combined
	public int CombineTrianglesThorough()
	{
		int initialTriangles = this.TriangleCount;
		int previousTriangleCount;

		do
		{
			previousTriangleCount = this.TriangleCount;
			this.CombineTrianglesBothDirections();
		}
		while (this.TriangleCount < previousTriangleCount);

		return initialTriangles - this.TriangleCount;
	}

	private void CombineTrianglesClockwise()
	{
		int length = this.trianglesList.Count;
		for (int i = 0; i < length; i++)
		{
			if (this.triangles[i].IsDeleted) { continue; }

			// Keep trying to combine along EdgeAB, then EdgeBC, and then EdgeCA.
			// Repeat this process in that order until 3 edges in a row don't combine any triangles.
			int tryCombineAttemptsRemaining = 3;
			while (true)
			{
				if (this.TryCombineClockwise(
					i, this.triangles[i].SlopeAB, ref this.triangles[i].EdgeBC, ref this.triangles[i].PointB))
				{
					// expanded EdgeAB by moving PointB to combine triangles sharing EdgeBC
					this.UnlinkEdge(ref this.triangles[i].EdgeAB, i);
					tryCombineAttemptsRemaining = 2;
				}
				else
				{
					tryCombineAttemptsRemaining--;
					if (tryCombineAttemptsRemaining == 0) { break; }
				}

				if (this.TryCombineClockwise(
					i, this.triangles[i].SlopeBC, ref this.triangles[i].EdgeCA, ref this.triangles[i].PointC))
				{
					// expanded EdgeBC by moving PointC to combine triangles sharing EdgeCA
					this.UnlinkEdge(ref this.triangles[i].EdgeBC, i);
					tryCombineAttemptsRemaining = 2;
				}
				else
				{
					tryCombineAttemptsRemaining--;
					if (tryCombineAttemptsRemaining == 0) { break; }
				}

				if (this.TryCombineClockwise(
					i, this.triangles[i].SlopeCA, ref this.triangles[i].EdgeAB, ref this.triangles[i].PointA))
				{
					// expanded EdgeCA by moving PointA to combine triangles sharing EdgeAB
					this.UnlinkEdge(ref this.triangles[i].EdgeCA, i);
					tryCombineAttemptsRemaining = 2;
				}
				else
				{
					tryCombineAttemptsRemaining--;
					if (tryCombineAttemptsRemaining == 0) { break; }
				}
			}
		}
	}

	// TODO not using this because CombineTrianglesClockwise is faster and appears to always
	// produce the same result. More testing to cofirm this would be nice before deleting this.
	private void CombineTrianglesBothDirections()
	{
		int length = this.trianglesList.Count;
		for (int i = 0; i < length; i++)
		{
			if (this.triangles[i].IsDeleted) { continue; }

			// Keep trying to combine along EdgeAB, then EdgeBC, and then EdgeCA.
			// Repeat this process in that order until 3 edges in a row don't combine any triangles.
			int tryCombineAttemptsRemaining = 3;
			while (true)
			{
				if (this.TryCombineBoth(
					i,
					this.triangles[i].SlopeAB,
					ref this.triangles[i].EdgeBC,
					ref this.triangles[i].PointB,
					ref this.triangles[i].EdgeCA,
					ref this.triangles[i].PointA))
				{
					// expanded EdgeAB by moving PointB or Point A
					// to combine triangles sharing EdgeBC or EdgeCA
					this.UnlinkEdge(ref this.triangles[i].EdgeAB, i);
					tryCombineAttemptsRemaining = 2;
				}
				else
				{
					tryCombineAttemptsRemaining--;
					if (tryCombineAttemptsRemaining == 0) { break; }
				}

				if (this.TryCombineBoth(
					i,
					this.triangles[i].SlopeBC,
					ref this.triangles[i].EdgeCA,
					ref this.triangles[i].PointC,
					ref this.triangles[i].EdgeAB,
					ref this.triangles[i].PointB))
				{
					// expanded EdgeBC by moving PointC or PointB
					// to combine triangles sharing EdgeCA or EdgeAB
					this.UnlinkEdge(ref this.triangles[i].EdgeBC, i);
					tryCombineAttemptsRemaining = 2;
				}
				else
				{
					tryCombineAttemptsRemaining--;
					if (tryCombineAttemptsRemaining == 0) { break; }
				}

				if (this.TryCombineBoth(
					i,
					this.triangles[i].SlopeCA,
					ref this.triangles[i].EdgeAB,
					ref this.triangles[i].PointA,
					ref this.triangles[i].EdgeBC,
					ref this.triangles[i].PointC))
				{
					// expanded EdgeCA by moving PointA or PointC
					// to combine triangles sharing EdgeAB or EdgeBC
					this.UnlinkEdge(ref this.triangles[i].EdgeCA, i);
					tryCombineAttemptsRemaining = 2;
				}
				else
				{
					tryCombineAttemptsRemaining--;
					if (tryCombineAttemptsRemaining == 0) { break; }
				}
			}
		}
	}

	private void UnlinkEdge(ref int edge, int self)
	{
		Debug.Assert(self != ConnectedTriangle.None);

		if (edge != ConnectedTriangle.None)
		{
			this.triangles[edge].ChangeEdge(self, ConnectedTriangle.None);
			edge = ConnectedTriangle.None;
		}
	}

	private void DeleteTriangle(int index)
	{
		Debug.Assert(index != ConnectedTriangle.None);

		this.triangles[index].Delete();
		this.TriangleCount--;
	}

	// Returns true if any triangles were combined, otherwise false.
	private bool TryCombineClockwise(int self, float slope, ref int edge, ref Point2 point)
	{
		Debug.Assert(self != ConnectedTriangle.None);
		Debug.Assert(!this.triangles[self].IsDeleted);
		Debug.Assert(self != edge);

		int current = self;
		while (true)
		{
			// check if there is an adjacent triangle
			if (edge == ConnectedTriangle.None || this.triangles[edge].IsDeleted) { break; }

			if (this.triangles[edge].EdgeAB == current)
			{
				if (!MathUtility.AreSimiliar(slope, this.triangles[edge].SlopeBC)) { break; }

				// combine with the adjacent triangle
				current = edge;
				edge = this.triangles[current].EdgeCA;
				point = this.triangles[current].PointC;
				this.DeleteTriangle(current);
			}
			else if (this.triangles[edge].EdgeBC == current)
			{
				if (!MathUtility.AreSimiliar(slope, this.triangles[edge].SlopeCA)) { break; }

				// combine with the adjacent triangle
				current = edge;
				edge = this.triangles[current].EdgeAB;
				point = this.triangles[current].PointA;
				this.DeleteTriangle(current);
			}
			else if (this.triangles[edge].EdgeCA == current)
			{
				if (!MathUtility.AreSimiliar(slope, this.triangles[edge].SlopeAB)) { break; }

				// combine with the adjacent triangle
				current = edge;
				edge = this.triangles[current].EdgeBC;
				point = this.triangles[current].PointB;
				this.DeleteTriangle(current);
			}
			else
			{
				break;
			}
		}

		// if triangles have been combined, check if there is an adjacent triangle
		// and if so update the adjacent triangle to point to the combined triangle
		if (current != self)
		{
			if (edge != ConnectedTriangle.None)
			{
				this.triangles[edge].ChangeEdge(current, self);
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	// TODO not using this because CombineTrianglesClockwise is faster and appears to always
	// produce the same result. More testing to cofirm this would be nice before deleting this.
	// Returns true if any triangles were combined, otherwise false.
	private bool TryCombineCounterClockwise(int self, float slope, ref int edge, ref Point2 point)
	{
		Debug.Assert(self != ConnectedTriangle.None);
		Debug.Assert(!this.triangles[self].IsDeleted);
		Debug.Assert(self != edge);

		int current = self;
		while (true)
		{
			// check if there is an adjacent triangle
			if (edge == ConnectedTriangle.None || this.triangles[edge].IsDeleted) { break; }

			if (this.triangles[edge].EdgeAB == current)
			{
				if (!MathUtility.AreSimiliar(slope, this.triangles[edge].SlopeCA)) { break; }

				// combine with the adjacent triangle
				current = edge;
				edge = this.triangles[current].EdgeBC;
				point = this.triangles[current].PointC;
				this.DeleteTriangle(current);
			}
			else if (this.triangles[edge].EdgeBC == current)
			{
				if (!MathUtility.AreSimiliar(slope, this.triangles[edge].SlopeAB)) { break; }

				// combine with the adjacent triangle
				current = edge;
				edge = this.triangles[current].EdgeCA;
				point = this.triangles[current].PointA;
				this.DeleteTriangle(current);
			}
			else if (this.triangles[edge].EdgeCA == current)
			{
				if (!MathUtility.AreSimiliar(slope, this.triangles[edge].SlopeBC)) { break; }

				// combine with the adjacent triangle
				current = edge;
				edge = this.triangles[current].EdgeAB;
				point = this.triangles[current].PointB;
				this.DeleteTriangle(current);
			}
			else
			{
				break;
			}
		}

		// if triangles have been combined, check if there is an adjacent triangle
		// and if so update the adjacent triangle to point to the combined triangle
		if (current != self)
		{
			if (edge != ConnectedTriangle.None)
			{
				this.triangles[edge].ChangeEdge(current, self);
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	// TODO not using this because CombineTrianglesClockwise is faster and appears to always
	// produce the same result. More testing to cofirm this would be nice before deleting this.
	// Returns true if any triangles were combined, otherwise false.
	private bool TryCombineBoth(
		int self,
		float slope,
		ref int clockwiseEdge,
		ref Point2 clockwisePoint,
		ref int counterCwEdge,
		ref Point2 counterCwPoint)
	{
		Debug.Assert(self != ConnectedTriangle.None);
		Debug.Assert(!this.triangles[self].IsDeleted);
		Debug.Assert(self != clockwiseEdge);
		Debug.Assert(self != counterCwEdge);
		Debug.Assert(clockwiseEdge != counterCwEdge || clockwiseEdge == ConnectedTriangle.None);
		Debug.Assert(clockwisePoint != counterCwPoint);

		// deliberately done as 2 separate lines in order to avoid short circuiting of || (or)
		bool clockwise = this.TryCombineClockwise(self, slope, ref clockwiseEdge, ref clockwisePoint);
		bool counterClockwise = this.TryCombineCounterClockwise(self, slope, ref counterCwEdge, ref counterCwPoint);
		return clockwise || counterClockwise;
	}
}
