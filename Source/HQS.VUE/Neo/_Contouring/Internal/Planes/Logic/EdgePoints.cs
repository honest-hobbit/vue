﻿using LibTessDotNet;

namespace HQS.VUE.Neo;

internal class EdgePoints : IReadOnlyList<ContourVertex>
{
	private ReadOnlyArray<CompactPoint2> points;

	private int startIndex;

	public EdgePoints(ReadOnlyArray<CompactPoint2> points)
	{
		Debug.Assert(!points.IsDefault);

		this.points = points;
	}

	public CompactPoint2 this[int index] => this.points[this.startIndex + index];

	/// <inheritdoc />
	ContourVertex IReadOnlyList<ContourVertex>.this[int index]
	{
		get
		{
			Debug.Assert(index >= 0 && index < this.Count);

			this.points[this.startIndex + index].GetPosition(out int x, out int y);
			return new ContourVertex(new Vec3(x, y, 0));
		}
	}

	/// <inheritdoc />
	public int Count { get; private set; }

	public void SetToEdgeSpan(EdgeSpan edge)
	{
		Debug.Assert(edge.StartIndex >= 0);
		Debug.Assert(edge.Count > 0);

		this.startIndex = edge.StartIndex;
		this.Count = edge.Count;
	}

	/// <inheritdoc />
	IEnumerator<ContourVertex> IEnumerable<ContourVertex>.GetEnumerator() =>
		throw new NotSupportedException();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() =>
		throw new NotSupportedException();
}
