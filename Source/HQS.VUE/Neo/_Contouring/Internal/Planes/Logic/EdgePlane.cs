﻿namespace HQS.VUE.Neo;

// TODO this class does too much, but not sure how better to split it up yet
internal class EdgePlane<TSurface, TComparer> : IEnumerable<Polygon<TSurface>>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	private readonly SquareArrayIndexer indexer;

	private readonly EdgeQuad[] surfaceEdges;

	private readonly EdgeState<TSurface>[] edgeStates;

	private readonly ushort[] polygons;

	// sorts the polygons by voxel type
	private readonly PolygonComparer polygonComparer;

	private int polygonCount;

	private int edgeCount;

	private ushort markingEdgeID;

	public EdgePlane(ChunkSize size)
	{
		size.AssertValid();

		this.indexer = size.AsSquare;

		int length = this.indexer.Length;
		this.surfaceEdges = new EdgeQuad[length];

		length *= Constants.TrianglesPerSurfaceQuad;
		this.edgeStates = new EdgeState<TSurface>[length];
		this.polygons = new ushort[length];

		this.polygonComparer = new PolygonComparer(this.edgeStates);
	}

	public ChunkSize Size => ChunkSize.CreateAssertOnly(this.indexer.Exponent);

	public SquareArrayIndexer Indexer => this.indexer;

	public void Initialize(TComparer comparer)
	{
		this.polygonComparer.Initialize(comparer);
		this.Clear();
	}

	public void SetEdgeParts(int index, EdgeParts parts) => this.surfaceEdges[index].Parts = parts;

	public EdgeParts GetEdgeParts(int index) => this.surfaceEdges[index].Parts;

	public EdgeQuad GetEdgeQuad(int index) => this.surfaceEdges[index];

	public void StartEdgeID()
	{
		this.edgeCount++;
		this.markingEdgeID = (ushort)(this.edgeCount % this.edgeStates.Length);

		this.edgeStates[this.markingEdgeID] = new EdgeState<TSurface>()
		{
			PolygonID = this.markingEdgeID,
			NextEdgeIndex = this.markingEdgeID,
		};
	}

	public void EndEdgeID(TSurface surfaceType, EdgeSpan edge)
	{
		Debug.Assert(edge.StartIndex >= 0);
		Debug.Assert(edge.Count >= Constants.EdgesPerTriangle);

		this.edgeStates[this.markingEdgeID].SurfaceType = surfaceType;
		this.edgeStates[this.markingEdgeID].Edge = edge;
	}

	public void SetTopEdgeID(int index)
	{
		Debug.Assert(this.surfaceEdges[index].TopID == EdgeState.UnassignedID
			|| this.surfaceEdges[index].TopID == this.markingEdgeID);

		this.surfaceEdges[index].TopID = this.markingEdgeID;
	}

	public void SetRightEdgeID(int index)
	{
		Debug.Assert(this.surfaceEdges[index].RightID == EdgeState.UnassignedID
			|| this.surfaceEdges[index].RightID == this.markingEdgeID);

		this.surfaceEdges[index].RightID = this.markingEdgeID;
	}

	public void SetBottomEdgeID(int index)
	{
		Debug.Assert(this.surfaceEdges[index].BottomID == EdgeState.UnassignedID
			|| this.surfaceEdges[index].BottomID == this.markingEdgeID);

		this.surfaceEdges[index].BottomID = this.markingEdgeID;
	}

	public void SetLeftEdgeID(int index)
	{
		Debug.Assert(this.surfaceEdges[index].LeftID == EdgeState.UnassignedID
			|| this.surfaceEdges[index].LeftID == this.markingEdgeID);

		this.surfaceEdges[index].LeftID = this.markingEdgeID;
	}

	// TODO it seems wrong to have this functionality in this class
	// but I haven't found a better way to split this class up yet
	public void FindHoles(Int2 start, Int2 dimensions)
	{
		Debug.Assert(dimensions.X >= 0);
		Debug.Assert(dimensions.Y >= 0);

		if (this.edgeCount <= 1 || this.edgeCount ==
			dimensions.X * dimensions.Y * Constants.TrianglesPerSurfaceQuad)
		{
			// Either fewer than 2 edges have been marked or every single voxel triangle
			// has been assigned a unique ID. This means there are no holes because
			// there are no edges or every edge loop is a single tiny triangle.
			return;
		}

		int maxX = start.X + dimensions.X;
		int maxY = start.Y + dimensions.Y;

		for (int iY = start.Y; iY < maxY; iY++)
		{
			ushort previousRightEdgeID = EdgeState.UnassignedID;

			for (int iX = start.X; iX < maxX; iX++)
			{
				int index = this.indexer[iX, iY];
				var edges = this.surfaceEdges[index];

				edges.LeftID = this.edgeStates[edges.LeftID].PolygonID;

				// check if the previous Right flood fills into Left
				if (previousRightEdgeID != EdgeState.UnassignedID)
				{
					// flood fill previous Right into Left
					if (edges.LeftID != EdgeState.UnassignedID && edges.LeftID != previousRightEdgeID)
					{
						this.ChangeEdgeGroupID(edges.LeftID, previousRightEdgeID);
					}

					edges.LeftID = previousRightEdgeID;
				}

				// only assign these after the above check in case
				// previous Right flood fills into Left and changes Left edgeState
				edges.TopID = this.edgeStates[edges.TopID].PolygonID;
				edges.BottomID = this.edgeStates[edges.BottomID].PolygonID;
				edges.RightID = this.edgeStates[edges.RightID].PolygonID;

				// check if Left flood fills into Top and/or Bottom
				if (edges.LeftID != EdgeState.UnassignedID)
				{
					if (!edges.Parts.Has(EdgeParts.LeftTop))
					{
						// flood fill from Left into Top
						if (edges.TopID != EdgeState.UnassignedID && edges.TopID != edges.LeftID)
						{
							this.ChangeEdgeGroupID(edges.TopID, edges.LeftID);

							// reassign edge IDs that will be checked after this in case they
							// were the same as Top and thus have now been changed
							// (always reassigning is faster than extra if checking)
							edges.BottomID = this.edgeStates[edges.BottomID].PolygonID;
							edges.RightID = this.edgeStates[edges.RightID].PolygonID;
						}

						edges.TopID = edges.LeftID;
					}

					if (!edges.Parts.Has(EdgeParts.LeftBottom))
					{
						// flood fill from Left into Bottom
						if (edges.BottomID != EdgeState.UnassignedID && edges.BottomID != edges.LeftID)
						{
							this.ChangeEdgeGroupID(edges.BottomID, edges.LeftID);

							// reassign edge IDs that will be checked after this in case they
							// were the same as Bottom and thus have now been changed
							// (always reassigning is faster than extra if checking)
							edges.TopID = this.edgeStates[edges.TopID].PolygonID;
							edges.RightID = this.edgeStates[edges.RightID].PolygonID;
						}

						edges.BottomID = edges.LeftID;
					}
				}

				// check if Top or Bottom flood fills into Right
				// (if both Top and Bottom would flood fill into Right then
				// only flood fill Top into Right because Bottom must be the same)
				if (edges.TopID != EdgeState.UnassignedID && !edges.Parts.Has(EdgeParts.TopRight))
				{
					Debug.Assert(edges.BottomID == EdgeState.UnassignedID ||
						(edges.BottomID != edges.TopID && edges.Parts.Has(EdgeParts.BottomRight)) ||
						(edges.BottomID == edges.TopID && !edges.Parts.Has(EdgeParts.BottomRight)));

					// flood fill from Top into Right
					if (edges.RightID != EdgeState.UnassignedID && edges.RightID != edges.TopID)
					{
						this.ChangeEdgeGroupID(edges.RightID, edges.TopID);
					}

					edges.RightID = edges.TopID;
				}
				else if (edges.BottomID != EdgeState.UnassignedID && !edges.Parts.Has(EdgeParts.BottomRight))
				{
					// flood fill from Bottom into Right
					if (edges.RightID != EdgeState.UnassignedID && edges.RightID != edges.BottomID)
					{
						this.ChangeEdgeGroupID(edges.RightID, edges.BottomID);
					}

					edges.RightID = edges.BottomID;
				}

				// check if Right flood fills into the next index Left
				if (edges.RightID != EdgeState.UnassignedID && !edges.Parts.Has(EdgeParts.RightOut))
				{
					// flood fill from Right into previousRightEdgeID
					// (this will flood fill into next index Left)
					previousRightEdgeID = edges.RightID;
				}
				else
				{
					previousRightEdgeID = EdgeState.UnassignedID;
				}

				// don't assign 'edges' back to this.voxelEdges[i] even though it has changed
				// because we never revisit previous indices of this.voxelEdges
			}
		}

		// assert that the empty voxel polygon state is still either empty or a single triangle
		// (which only happens when every possible triangle is a separate polygon)
		Debug.Assert(this.edgeStates[EdgeState.UnassignedID].PolygonID == EdgeState.UnassignedID);
		Debug.Assert(this.edgeStates[EdgeState.UnassignedID].NextEdgeIndex == EdgeState.UnassignedID);
		Debug.Assert(
			this.edgeStates[EdgeState.UnassignedID].Edge.Count == 0 || (this.markingEdgeID == 0 &&
			this.edgeStates[EdgeState.UnassignedID].Edge.Count == Constants.EdgesPerTriangle));
	}

	public void SortEdges()
	{
		if (this.edgeCount == 0)
		{
			return;
		}

		// check if markingEdgeID has not overflowed back to 0
		if (this.markingEdgeID > 0)
		{
			// the first edge state is still the empty voxel polygon state
			// and there could be holes in some polygons
			for (int i = 1; i <= this.markingEdgeID; i++)
			{
				// check if the edge is a hole inside a polygon
				if (this.edgeStates[i].PolygonID == i)
				{
					// the edge is not a hole, it is the outside edge of a polygon
					this.polygons[this.polygonCount] = this.edgeStates[i].PolygonID;
					this.polygonCount++;
				}
			}
		}
		else
		{
			// markingEdgeID has overflowed to 0 because
			// every possible triangle is a separate polygon
			// and there is guaranteed to be no holes in any polygons
			for (int i = 0; i < this.edgeStates.Length; i++)
			{
				this.polygons[i] = this.edgeStates[i].PolygonID;
			}

			// every edge is a separate polygon
			this.polygonCount = this.edgeStates.Length;
		}

		// sort the polygons by voxel type
		Array.Sort(this.polygons, 0, this.polygonCount, this.polygonComparer);
	}

	public PolygonEnumerator<TSurface> GetEnumerator() => new PolygonEnumerator<TSurface>(
		this.edgeStates.AsReadOnlyArray(), this.polygons.AsReadOnlyArray(), this.polygonCount);

	/// <inheritdoc />
	IEnumerator<Polygon<TSurface>> IEnumerable<Polygon<TSurface>>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private void ChangeEdgeGroupID(ushort oldEdgeID, ushort newEdgeID)
	{
		Debug.Assert(oldEdgeID != EdgeState.UnassignedID);
		Debug.Assert(newEdgeID != EdgeState.UnassignedID);
		Debug.Assert(newEdgeID != oldEdgeID);
		Debug.Assert(this.edgeStates[oldEdgeID].PolygonID == oldEdgeID);
		Debug.Assert(this.edgeStates[oldEdgeID].NextEdgeIndex == oldEdgeID);

		// set the old edge to be part of the new polygon
		this.edgeStates[oldEdgeID].PolygonID = newEdgeID;

		// set the old edge to point to what the new polygon points to as the next edge
		// if it doesn't point at ifself (pointing at self indicates no more edges)
		ushort nextEdgeIndex = this.edgeStates[newEdgeID].NextEdgeIndex;
		if (nextEdgeIndex != newEdgeID)
		{
			this.edgeStates[oldEdgeID].NextEdgeIndex = nextEdgeIndex;
		}

		// set the new polygon to point to the old edge
		this.edgeStates[newEdgeID].NextEdgeIndex = oldEdgeID;
	}

	private void Clear()
	{
		// reset the empty voxel polygon state in case it was overwritten
		this.edgeStates[EdgeState.UnassignedID] = EdgeState.EmptyPolygon<TSurface>();

		this.polygonCount = 0;
		this.edgeCount = 0;
		this.markingEdgeID = 0;

		var emptyEdges = EdgeQuad.Empty;
		int length = this.surfaceEdges.Length;

		for (int i = 0; i < length; i++)
		{
			this.surfaceEdges[i] = emptyEdges;
		}
	}

	private class PolygonComparer : IComparer<ushort>
	{
		private readonly EdgeState<TSurface>[] edgeStates;

		private TComparer comparer;

		public PolygonComparer(EdgeState<TSurface>[] edgeStates)
		{
			Debug.Assert(edgeStates != null);

			this.edgeStates = edgeStates;
		}

		public void Initialize(TComparer comparer) => this.comparer = comparer;

		/// <inheritdoc />
		public int Compare(ushort x, ushort y)
		{
			return this.comparer.Compare(
				this.edgeStates[x].SurfaceType, this.edgeStates[y].SurfaceType);
		}
	}
}
