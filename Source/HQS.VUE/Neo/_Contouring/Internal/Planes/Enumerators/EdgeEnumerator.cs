﻿namespace HQS.VUE.Neo;

internal struct EdgeEnumerator<T> : IEnumerator<EdgeSpan>
	where T : unmanaged
{
	private readonly ReadOnlyArray<EdgeState<T>> edgeStates;

	private int previousIndex;

	private int nextIndex;

	private EdgeSpan current;

	public EdgeEnumerator(ReadOnlyArray<EdgeState<T>> edgeStates, int polygonID)
	{
		Debug.Assert(!edgeStates.IsDefault);
		Debug.Assert(polygonID >= 0);

		this.edgeStates = edgeStates;
		this.previousIndex = -1;
		this.nextIndex = polygonID;
		this.current = default;
	}

	/// <inheritdoc />
	public EdgeSpan Current => this.current;

	/// <inheritdoc />
	object IEnumerator.Current => this.current;

	/// <inheritdoc />
	public bool MoveNext()
	{
		if (this.nextIndex != this.previousIndex)
		{
			this.previousIndex = this.nextIndex;
			this.current = this.edgeStates[this.nextIndex].Edge;
			this.nextIndex = this.edgeStates[this.nextIndex].NextEdgeIndex;
			return true;
		}
		else
		{
			this.current = default;
			return false;
		}
	}

	/// <inheritdoc />
	public void Dispose()
	{
	}

	/// <inheritdoc />
	void IEnumerator.Reset() => throw new NotSupportedException();
}
