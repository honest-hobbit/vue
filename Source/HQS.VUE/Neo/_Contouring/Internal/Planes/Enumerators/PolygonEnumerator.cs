﻿namespace HQS.VUE.Neo;

internal struct PolygonEnumerator<T> : IEnumerator<Polygon<T>>
	where T : unmanaged
{
	private readonly ReadOnlyArray<EdgeState<T>> edgeStates;

	private readonly ReadOnlyArray<ushort> polygons;

	private readonly int polygonCount;

	private int nextIndex;

	private Polygon<T> current;

	public PolygonEnumerator(
		ReadOnlyArray<EdgeState<T>> edgeStates,
		ReadOnlyArray<ushort> polygons,
		int polygonCount)
	{
		Debug.Assert(!edgeStates.IsDefault);
		Debug.Assert(!polygons.IsDefault);
		Debug.Assert(polygonCount >= 0);

		this.edgeStates = edgeStates;
		this.polygons = polygons;
		this.polygonCount = polygonCount;
		this.nextIndex = 0;
		this.current = default;
	}

	/// <inheritdoc />
	public Polygon<T> Current => this.current;

	/// <inheritdoc />
	object IEnumerator.Current => this.current;

	/// <inheritdoc />
	public bool MoveNext()
	{
		if (this.nextIndex < this.polygonCount)
		{
			int polygonID = this.polygons[this.nextIndex];
			this.current = new Polygon<T>(this.edgeStates, polygonID);
			this.nextIndex++;
			return true;
		}
		else
		{
			this.current = default;
			return false;
		}
	}

	/// <inheritdoc />
	public void Dispose()
	{
	}

	/// <inheritdoc />
	void IEnumerator.Reset() => throw new NotSupportedException();
}
