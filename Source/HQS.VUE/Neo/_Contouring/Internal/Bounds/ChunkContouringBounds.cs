﻿namespace HQS.VUE.Neo;

internal class ChunkContouringBounds : IContouringBounds<Int3>
{
	private readonly int sideLength;

	public ChunkContouringBounds(ChunkSize size)
	{
		size.AssertValid();

		this.Size = size;
		this.sideLength = size.SideLength;

		this.Dimensions = new Int3(this.sideLength);
		this.Lower = new Int3(0);
		this.Upper = new Int3(this.sideLength - 1);
	}

	/// <inheritdoc />
	public ChunkSize Size { get; }

	/// <inheritdoc />
	public Int3 MaxDimensions => new Int3(this.sideLength);

	/// <inheritdoc />
	public Int3 Dimensions { get; private set; }

	/// <inheritdoc />
	public Int3 Lower { get; private set; }

	/// <inheritdoc />
	public Int3 Upper { get; private set; }

	/// <inheritdoc />
	public bool IsEmpty => this.Dimensions.X == 0 || this.Dimensions.Y == 0 || this.Dimensions.Z == 0;

	/// <inheritdoc />
	public void SetBounds(Int3 origin, Int3 dimensions)
	{
		ContouringBoundsUtility.ValidateSetBounds(
			this.sideLength, origin.X, dimensions.X, nameof(Int3.X));
		ContouringBoundsUtility.ValidateSetBounds(
			this.sideLength, origin.Y, dimensions.Y, nameof(Int3.Y));
		ContouringBoundsUtility.ValidateSetBounds(
			this.sideLength, origin.Z, dimensions.Z, nameof(Int3.Z));

		this.Dimensions = dimensions;
		this.Lower = origin;
		this.Upper = origin + dimensions - Int3.One;
	}
}
