﻿namespace HQS.VUE.Neo;

internal class PlaneContouringBounds : IContouringBounds<Int2>
{
	private readonly int sideLength;

	public PlaneContouringBounds(ChunkSize size)
	{
		size.AssertValid();

		this.Size = size;
		this.sideLength = size.SideLength;

		this.Dimensions = new Int2(this.sideLength);
		this.Lower = new Int2(0);
		this.Upper = new Int2(this.sideLength - 1);
	}

	/// <inheritdoc />
	public ChunkSize Size { get; }

	/// <inheritdoc />
	public Int2 MaxDimensions => new Int2(this.sideLength);

	/// <inheritdoc />
	public Int2 Dimensions { get; private set; }

	/// <inheritdoc />
	public Int2 Lower { get; private set; }

	/// <inheritdoc />
	public Int2 Upper { get; private set; }

	/// <inheritdoc />
	public bool IsEmpty => this.Dimensions.X == 0 || this.Dimensions.Y == 0;

	/// <inheritdoc />
	public void SetBounds(Int2 origin, Int2 dimensions)
	{
		ContouringBoundsUtility.AssertSetBounds(
			this.sideLength, origin.X, dimensions.X, nameof(Int2.X));
		ContouringBoundsUtility.AssertSetBounds(
			this.sideLength, origin.Y, dimensions.Y, nameof(Int2.Y));

		this.Dimensions = dimensions;
		this.Lower = origin;
		this.Upper = origin + dimensions - Int2.One;
	}
}
