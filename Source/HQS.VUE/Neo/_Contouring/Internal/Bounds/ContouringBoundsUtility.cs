﻿namespace HQS.VUE.Neo;

internal static class ContouringBoundsUtility
{
	[Conditional(CompilationSymbol.Debug)]
	public static void AssertSetBounds(int maxDimensions, int origin, int dimensions, string nameOfAxis)
	{
		Debug.Assert(maxDimensions > 0);
		Debug.Assert(!nameOfAxis.IsNullOrWhiteSpace());
		Debug.Assert(nameOfAxis.Length == 1);

		Debug.Assert(origin.IsIn(0, maxDimensions - 1));
		Debug.Assert(dimensions.IsIn(0, maxDimensions - origin));
	}

	public static void ValidateSetBounds(int maxDimensions, int origin, int dimensions, string nameOfAxis)
	{
		Debug.Assert(maxDimensions > 0);
		Debug.Assert(!nameOfAxis.IsNullOrWhiteSpace());
		Debug.Assert(nameOfAxis.Length == 1);

		Ensure.That(origin, $"{nameof(origin)}.{nameOfAxis}").IsInRange(0, maxDimensions - 1);
		Ensure.That(dimensions, $"{nameof(dimensions)}.{nameOfAxis}").IsInRange(0, maxDimensions - origin);
	}
}
