﻿namespace HQS.VUE.Neo;

internal abstract class AbstractChunkContourer<TVoxel, TSurface, TExtractor> :
	AbstractDisposable, IChunkContourer<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
#if VUE_PROFILE
	private readonly ChunkContourerProfiler profiler = new ChunkContourerProfiler();
#else
	private readonly ChunkContourerProfiler profiler = null;
#endif

	private readonly ChunkContouringBounds boundsChecker;

	private readonly IPool<DualPlanes<TSurface, TExtractor>> contourerPool;

	private readonly CoplanarChunkMerger<TSurface, TExtractor> chunkMerger;

	private readonly MergeRangeProcessor<
		ContourChunkThreadLocalState<TSurface, TExtractor>, CoplanarChunk<TSurface>> processor;

	private int offsetOutput;

	public AbstractChunkContourer(
		ChunkSize size,
		IPool<DualPlanes<TSurface, TExtractor>> contourerPool,
		CoplanarChunk<TSurface>.Builder chunkBuilder)
	{
		size.AssertValid();
		Debug.Assert(contourerPool != null);
		Debug.Assert(chunkBuilder != null);

		this.boundsChecker = new ChunkContouringBounds(size);
		this.contourerPool = contourerPool;

		this.chunkMerger = new CoplanarChunkMerger<TSurface, TExtractor>(
			this.profiler, contourerPool, chunkBuilder);

		this.processor = new MergeRangeProcessor<
			ContourChunkThreadLocalState<TSurface, TExtractor>, CoplanarChunk<TSurface>>(
			this.ProcessDualPlanes,
			this.MergeStates,
			this.InitializeState,
			this.FinalizeState);
	}

#if VUE_PROFILE
	/// <inheritdoc />
	public IChunkContourerProfiler Profiler => this.profiler;
#else
	/// <inheritdoc />
	public IChunkContourerProfiler Profiler { get; } = new NullChunkContourerProfiler();
#endif

	/// <inheritdoc />
	public IContouringBounds<Int3> Bounds => this.boundsChecker;

	/// <inheritdoc />
	public TExtractor Extractor { get; set; }

	/// <inheritdoc />
	public int MaxDegreeOfParallelism
	{
		get => this.processor.MaxDegreeOfParallelism;
		set => this.processor.MaxDegreeOfParallelism = value;
	}

	/// <inheritdoc />
	public bool CheckForTrianglesMissed { get; set; }

	/// <inheritdoc />
	public bool SortAndCompactOutput { get; set; }

	protected static void ContourFront(
		DualPlanes<TSurface, TExtractor> plane, bool hasFront, PlaneNormal normal, int depth)
	{
		Debug.Assert(plane != null);

		if (hasFront)
		{
			plane.Contourer.Surfaces = plane.FrontSurfaces;
			plane.Contourer.Output = plane.FrontOutput;
			plane.Contourer.ReverseOutput = plane.IsFrontReversed;
			plane.Contourer.ContourPlane(PlaneIndex.From(normal, depth));

			plane.Profiler.AddToPlanesProcessed(1);
		}
		else
		{
			plane.Profiler.AddToPlanesSkipped(1);
		}
	}

	protected static void ContourBack(
		DualPlanes<TSurface, TExtractor> plane, bool hasBack, PlaneNormal normal, int depth)
	{
		Debug.Assert(plane != null);

		if (hasBack)
		{
			plane.Contourer.Surfaces = plane.BackSurfaces;
			plane.Contourer.Output = plane.BackOutput;
			plane.Contourer.ReverseOutput = !plane.IsFrontReversed;
			plane.Contourer.ContourPlane(PlaneIndex.From(normal, depth));

			plane.Profiler.AddToPlanesProcessed(1);
		}
		else
		{
			plane.Profiler.AddToPlanesSkipped(1);
		}
	}

	protected static void ContourBoth(
		DualPlanes<TSurface, TExtractor> plane, bool hasFront, bool hasBack, PlaneNormal frontNormal, int depth)
	{
		Debug.Assert(plane != null);

		if (hasFront)
		{
			plane.Contourer.Surfaces = plane.FrontSurfaces;
			plane.Contourer.Output = plane.FrontOutput;
			plane.Contourer.ReverseOutput = plane.IsFrontReversed;
			plane.Contourer.ContourPlane(PlaneIndex.From(frontNormal, depth));

			plane.Profiler.AddToPlanesProcessed(1);
		}
		else
		{
			plane.Profiler.AddToPlanesSkipped(1);
		}

		if (hasBack)
		{
			plane.Contourer.Surfaces = plane.BackSurfaces;
			plane.Contourer.Output = plane.BackOutput;
			plane.Contourer.ReverseOutput = !plane.IsFrontReversed;
			plane.Contourer.ContourPlane(PlaneIndex.From(frontNormal.Negate(), depth));

			plane.Profiler.AddToPlanesProcessed(1);
		}
		else
		{
			plane.Profiler.AddToPlanesSkipped(1);
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.processor.Dispose();

	protected abstract void ProcessDualPlanes(
		ref ContourChunkThreadLocalState<TSurface, TExtractor> state, int index);

	protected void InitializeContourChunk(ChunkSize voxelChunkSize)
	{
		voxelChunkSize.AssertValid();

		this.profiler.AssertNotRunning();
		this.profiler.StartContourChunkTotal();
		this.profiler.StartSerializedTotal();
		this.profiler.StartInitializeChunk();
		this.profiler.Initialize(
			this.MaxDegreeOfParallelism,
			this.CheckForTrianglesMissed,
			this.SortAndCompactOutput);

		this.offsetOutput = (ChunkSize.Max.SideLength - voxelChunkSize.SideLength) >> 1;
	}

	protected CoplanarChunk<TSurface> ContourChunk(int contourPlanesCount)
	{
		Debug.Assert(contourPlanesCount >= 0);

		// TODO if processor Run is split into Start and Stop then move this to between Start and Stop
		this.profiler.StopInitializeChunk();
		this.profiler.StopSerializedTotal();
		this.profiler.StartParallelizedTotal();

		return this.processor.Run(contourPlanesCount);
	}

	protected void InitializeFaceX(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.IsFrontReversed = PlaneNormal.FacePosX.GetReverseOutput();
		plane.Contourer.Bounds.SetBounds(
			Project(this.Bounds.Lower), Project(this.Bounds.Dimensions));

		static Int2 Project(Int3 value) => new Int2(value.Z, value.Y);
	}

	protected void InitializeFaceY(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.IsFrontReversed = PlaneNormal.FacePosY.GetReverseOutput();
		plane.Contourer.Bounds.SetBounds(
			Project(this.Bounds.Lower), Project(this.Bounds.Dimensions));

		static Int2 Project(Int3 value) => new Int2(value.X, value.Z);
	}

	protected void InitializeFaceZ(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.IsFrontReversed = PlaneNormal.FacePosZ.GetReverseOutput();
		plane.Contourer.Bounds.SetBounds(
			Project(this.Bounds.Lower), Project(this.Bounds.Dimensions));

		static Int2 Project(Int3 value) => new Int2(value.X, value.Y);
	}

	private void InitializeState(ref ContourChunkThreadLocalState<TSurface, TExtractor> state)
	{
		var plane = this.contourerPool.Rent();

		plane.Profiler.Reset();
		plane.Profiler.StartProcessPlanesTotal();

		plane.Contourer.OffsetOutput = new Int2(this.offsetOutput);
		plane.Contourer.Comparer = this.Extractor;
		plane.Contourer.CheckForTrianglesMissed = this.CheckForTrianglesMissed;
		plane.Contourer.Profiler.Reset();

		this.InitializeFaceX(plane);
		state.Initialize(plane, this.Bounds.Dimensions.X + 1);
	}

	private void FinalizeState(ref ContourChunkThreadLocalState<TSurface, TExtractor> state)
	{
		state.Plane.Contourer.Comparer = default;

		CoplanarChunkUtility.AssertValid(state.Plane.FrontOutput);
		CoplanarChunkUtility.AssertValid(state.Plane.BackOutput);

		CoplanarChunkUtility.AssertPlanesAreSorted(state.Plane.FrontOutput);
		CoplanarChunkUtility.AssertPlanesAreSorted(state.Plane.BackOutput);

		state.Plane.Profiler.StopProcessPlanesTotal();
		state.Plane.Profiler.AssertNotRunning();
	}

	private CoplanarChunk<TSurface> MergeStates(
		List<ContourChunkThreadLocalState<TSurface, TExtractor>> states)
	{
		Debug.Assert(states != null);

		return this.chunkMerger.MergeStates(states, this.SortAndCompactOutput);
	}
}
