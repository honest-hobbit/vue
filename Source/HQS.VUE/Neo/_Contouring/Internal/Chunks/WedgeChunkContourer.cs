﻿namespace HQS.VUE.Neo;

internal class WedgeChunkContourer<TVoxel, TSurface, TExtractor> :
	AbstractChunkContourer<TVoxel, TSurface, TExtractor>, IWedgeChunkContourer<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	private const QuadParts Mask = (QuadParts)WedgePlane.Mask;

	private const int Shift = WedgePlane.Shift;

	private WedgeChunkContourerSharedData<TVoxel, TSurface, TExtractor> data;

	public WedgeChunkContourer(
		ChunkSize size,
		IPool<DualPlanes<TSurface, TExtractor>> contourerPool,
		CoplanarChunk<TSurface>.Builder chunkBuilder)
		: base(size, contourerPool, chunkBuilder)
	{
	}

	/// <inheritdoc />
	public WedgeTable Wedges { get; set; }

	/// <inheritdoc />
	public CoplanarChunk<TSurface> ContourChunk(WedgeChunk<TVoxel> chunk)
	{
		IWedgeChunkContourerContracts.ContourChunk(this, chunk);

		if (this.Bounds.IsEmpty) { return CoplanarChunk<TSurface>.Empty; }

		if (chunk.IsUniform)
		{
			// TODO need to handle uniform chunk properly
			Console.WriteLine("Skipping uniform chunk");
			return default;
		}

		// TODO check that chunk size matches Bounds?
		this.InitializeContourChunk(chunk.Size);

		this.data = new WedgeChunkContourerSharedData<TVoxel, TSurface, TExtractor>(this, chunk);

		return this.ContourChunk(this.data.TotalPlanesCount);
	}

	/// <inheritdoc />
	[SuppressMessage("StyleCop", "SA1123", Justification = "Switch cases should be collapsible.")]
	protected sealed override void ProcessDualPlanes(
		ref ContourChunkThreadLocalState<TSurface, TExtractor> state, int index)
	{
		state.Plane.Profiler.StartIndexPlane();

		while (true)
		{
			Debug.Assert(index >= state.LowerBound);

			// TODO this could be optimized by doing a pre-pass in ContourChunk
			// to determine which PlaneAxis could possibly contain any surfaces
			// and then only process those PlaneAxis. However, this pre-pass
			// has a cost and would be single threaded so it may not be faster.
			switch (state.CurrentAxis)
			{
				#region FaceX
				case PlaneAxis.FaceX:
					if (index < state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						if (index == state.LowerBound)
						{
							this.ContourFaceXBack(state.Plane);
							return;
						}

						this.ContourFaceXInterior(state.Plane, index - state.LowerBound - 1);
						return;
					}

					if (index == state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourFaceXFront(state.Plane);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.Dimensions.Y + 1);
					this.InitializeFaceY(state.Plane);
					break;
				#endregion
				#region FaceY
				case PlaneAxis.FaceY:
					if (index < state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						if (index == state.LowerBound)
						{
							this.ContourFaceYBack(state.Plane);
							return;
						}

						this.ContourFaceYInterior(state.Plane, index - state.LowerBound - 1);
						return;
					}

					if (index == state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourFaceYFront(state.Plane);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.Dimensions.Z + 1);
					this.InitializeFaceZ(state.Plane);
					break;
				#endregion
				#region FaceZ
				case PlaneAxis.FaceZ:
					if (index < state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						if (index == state.LowerBound)
						{
							this.ContourFaceZBack(state.Plane);
							return;
						}

						this.ContourFaceZInterior(state.Plane, index - state.LowerBound - 1);
						return;
					}

					if (index == state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourFaceZFront(state.Plane);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.EdgePlanesCount.X);
					state.Plane.IsFrontReversed = !PlaneNormal.EdgeXNegYNegZ.GetReverseOutput();
					break;
				#endregion
				#region EdgeXAscending
				case PlaneAxis.EdgeXAscending:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourEdgeXAscending(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.EdgePlanesCount.X);
					state.Plane.IsFrontReversed = !PlaneNormal.EdgeXNegYPosZ.GetReverseOutput();
					break;
				#endregion
				#region EdgeXDescending
				case PlaneAxis.EdgeXDescending:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourEdgeXDescending(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.EdgePlanesCount.Y);
					state.Plane.IsFrontReversed = !PlaneNormal.EdgeYNegXNegZ.GetReverseOutput();
					break;
				#endregion
				#region EdgeYAscending
				case PlaneAxis.EdgeYAscending:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourEdgeYAscending(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.EdgePlanesCount.Y);
					state.Plane.IsFrontReversed = !PlaneNormal.EdgeYNegXPosZ.GetReverseOutput();
					break;
				#endregion
				#region EdgeYDescending
				case PlaneAxis.EdgeYDescending:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourEdgeYDescending(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.EdgePlanesCount.Z);
					state.Plane.IsFrontReversed = !PlaneNormal.EdgeZNegXNegY.GetReverseOutput();
					break;
				#endregion
				#region EdgeZAscending
				case PlaneAxis.EdgeZAscending:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourEdgeZAscending(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.EdgePlanesCount.Z);
					state.Plane.IsFrontReversed = !PlaneNormal.EdgeZNegXPosY.GetReverseOutput();
					break;
				#endregion
				#region EdgeZDescending
				case PlaneAxis.EdgeZDescending:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourEdgeZDescending(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.CornerPlanesCount);
					state.Plane.IsFrontReversed = !PlaneNormal.CornerNegXNegYNegZ.GetReverseOutput();
					break;
				#endregion
				#region CornerNegXNegYNegZ
				case PlaneAxis.CornerNegXNegYNegZ:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourCornerNegXNegYNegZ(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.CornerPlanesCount);
					state.Plane.IsFrontReversed = !PlaneNormal.CornerNegXNegYPosZ.GetReverseOutput();
					break;
				#endregion
				#region CornerNegXNegYPosZ
				case PlaneAxis.CornerNegXNegYPosZ:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourCornerNegXNegYPosZ(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.CornerPlanesCount);
					state.Plane.IsFrontReversed = !PlaneNormal.CornerNegXPosYNegZ.GetReverseOutput();
					break;
				#endregion
				#region CornerNegXPosYNegZ
				case PlaneAxis.CornerNegXPosYNegZ:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourCornerNegXPosYNegZ(state.Plane, index - state.LowerBound);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.data.CornerPlanesCount);
					state.Plane.IsFrontReversed = !PlaneNormal.CornerNegXPosYPosZ.GetReverseOutput();
					break;
				#endregion
				#region CornerNegXPosYPosZ
				case PlaneAxis.CornerNegXPosYPosZ:
					if (index <= state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourCornerNegXPosYPosZ(state.Plane, index - state.LowerBound);
						return;
					}

					// finished contouring, this should NOT be reached
					state.Plane.Profiler.StopIndexPlane();
					throw new InvalidOperationException($"Range of {nameof(index)} is too large.");
				#endregion
				default:
					state.Plane.Profiler.StopIndexPlane();
					throw InvalidEnumArgument.CreateException(
						$"{nameof(state)}.{nameof(state.CurrentAxis)}", state.CurrentAxis);
			}
		}
	}

	#region Utility Methods

	private static void GetVoxel(TVoxel voxelData, QuadParts parts, ref SurfaceQuad<TVoxel> result)
	{
		switch (parts)
		{
			case QuadParts.All:
				result.Top = voxelData;
				result.Right = voxelData;
				result.Bottom = voxelData;
				result.Left = voxelData;
				return;

			case QuadParts.Top:
				result.Top = voxelData;
				return;

			case QuadParts.Right:
				result.Right = voxelData;
				return;

			case QuadParts.Bottom:
				result.Bottom = voxelData;
				return;

			case QuadParts.Left:
				result.Left = voxelData;
				return;

			case QuadParts.TopLeft:
				result.Top = voxelData;
				result.Left = voxelData;
				return;

			case QuadParts.TopRight:
				result.Top = voxelData;
				result.Right = voxelData;
				return;

			case QuadParts.BottomLeft:
				result.Bottom = voxelData;
				result.Left = voxelData;
				return;

			case QuadParts.BottomRight:
				result.Bottom = voxelData;
				result.Right = voxelData;
				return;

			case QuadParts.TopBottom:
				result.Top = voxelData;
				result.Bottom = voxelData;
				return;

			case QuadParts.LeftRight:
				result.Left = voxelData;
				result.Right = voxelData;
				return;

			case QuadParts.TopThree:
				result.Top = voxelData;
				result.Left = voxelData;
				result.Right = voxelData;
				return;

			case QuadParts.BottomThree:
				result.Bottom = voxelData;
				result.Left = voxelData;
				result.Right = voxelData;
				return;

			case QuadParts.RightThree:
				result.Right = voxelData;
				result.Top = voxelData;
				result.Bottom = voxelData;
				return;

			case QuadParts.LeftThree:
				result.Left = voxelData;
				result.Top = voxelData;
				result.Bottom = voxelData;
				return;
		}
	}

	#endregion
	#region Contour Exterior Faces

	private void ContourFaceXFront(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		var frontSurfaces = plane.FrontSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.Z, iY];

			for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
			{
				var frontWedges = this.data.Voxels[this.data.Upper.X, iY, iZ];
				var frontVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < frontWedges.Length; iW++)
				{
					var frontWedge = frontWedges[iW];
					var frontParts = (QuadParts)((int)this.data.Wedges.GetFaceX(frontWedge.Shape) >> Shift);
					GetVoxel(frontWedge.Data, frontParts, ref frontVoxel);
				}

				var front = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, ref hasFront, ref front.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, ref hasFront, ref front.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, ref hasFront, ref front.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, ref hasFront, ref front.Left);
				frontSurfaces[iP] = front;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourFront(plane, hasFront, PlaneNormal.FacePosX, this.data.Upper.X - this.data.FaceDepthOffset + 1);
	}

	private void ContourFaceXBack(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasBack = false;
		var backSurfaces = plane.BackSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.Z, iY];

			for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
			{
				var backWedges = this.data.Voxels[this.data.Lower.X, iY, iZ];
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < backWedges.Length; iW++)
				{
					var backWedge = backWedges[iW];
					var backParts = this.data.Wedges.GetFaceX(backWedge.Shape) & Mask;
					GetVoxel(backWedge.Data, backParts, ref backVoxel);
				}

				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(backVoxel.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(backVoxel.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(backVoxel.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(backVoxel.Left, ref hasBack, ref back.Left);
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBack(plane, hasBack, PlaneNormal.FaceNegX, this.data.Lower.X - this.data.FaceDepthOffset);
	}

	private void ContourFaceYFront(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		var frontSurfaces = plane.FrontSurfaces;

		for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iZ];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var frontWedges = this.data.Voxels[iX, this.data.Upper.Y, iZ];
				var frontVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < frontWedges.Length; iW++)
				{
					var frontWedge = frontWedges[iW];
					var frontParts = (QuadParts)((int)this.data.Wedges.GetFaceY(frontWedge.Shape) >> Shift);
					GetVoxel(frontWedge.Data, frontParts, ref frontVoxel);
				}

				var front = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, ref hasFront, ref front.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, ref hasFront, ref front.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, ref hasFront, ref front.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, ref hasFront, ref front.Left);
				frontSurfaces[iP] = front;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourFront(plane, hasFront, PlaneNormal.FacePosY, this.data.Upper.Y - this.data.FaceDepthOffset + 1);
	}

	private void ContourFaceYBack(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasBack = false;
		var backSurfaces = plane.BackSurfaces;

		for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iZ];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var backWedges = this.data.Voxels[iX, this.data.Lower.Y, iZ];
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < backWedges.Length; iW++)
				{
					var backWedge = backWedges[iW];
					var backParts = this.data.Wedges.GetFaceY(backWedge.Shape) & Mask;
					GetVoxel(backWedge.Data, backParts, ref backVoxel);
				}

				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(backVoxel.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(backVoxel.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(backVoxel.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(backVoxel.Left, ref hasBack, ref back.Left);
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBack(plane, hasBack, PlaneNormal.FaceNegY, this.data.Lower.Y - this.data.FaceDepthOffset);
	}

	private void ContourFaceZFront(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		var frontSurfaces = plane.FrontSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iY];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var frontWedges = this.data.Voxels[iX, iY, this.data.Upper.Z];
				var frontVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < frontWedges.Length; iW++)
				{
					var frontWedge = frontWedges[iW];
					var frontParts = (QuadParts)((int)this.data.Wedges.GetFaceZ(frontWedge.Shape) >> Shift);
					GetVoxel(frontWedge.Data, frontParts, ref frontVoxel);
				}

				var front = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, ref hasFront, ref front.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, ref hasFront, ref front.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, ref hasFront, ref front.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, ref hasFront, ref front.Left);
				frontSurfaces[iP] = front;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourFront(plane, hasFront, PlaneNormal.FacePosZ, this.data.Upper.Z - this.data.FaceDepthOffset + 1);
	}

	private void ContourFaceZBack(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasBack = false;
		var backSurfaces = plane.BackSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iY];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var backWedges = this.data.Voxels[iX, iY, this.data.Lower.Z];
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < backWedges.Length; iW++)
				{
					var backWedge = backWedges[iW];
					var backParts = this.data.Wedges.GetFaceZ(backWedge.Shape) & Mask;
					GetVoxel(backWedge.Data, backParts, ref backVoxel);
				}

				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(backVoxel.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(backVoxel.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(backVoxel.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(backVoxel.Left, ref hasBack, ref back.Left);
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBack(plane, hasBack, PlaneNormal.FaceNegZ, this.data.Lower.Z - this.data.FaceDepthOffset);
	}

	#endregion
	#region Contour Interior Faces

	private void ContourFaceXInterior(DualPlanes<TSurface, TExtractor> plane, int iX)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		iX += this.data.Lower.X;
		int nextX = iX + 1;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.Z, iY];

			for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
			{
				var frontWedges = this.data.Voxels[iX, iY, iZ];
				var backWedges = this.data.Voxels[nextX, iY, iZ];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < frontWedges.Length; iW++)
				{
					var frontWedge = frontWedges[iW];
					var backWedge = backWedges[iW];
					if (frontWedge.Shape.Index + backWedge.Shape.Index == 0) { break; }

					var frontParts = (QuadParts)((int)this.data.Wedges.GetFaceX(frontWedge.Shape) >> Shift);
					GetVoxel(frontWedge.Data, frontParts, ref frontVoxel);

					var backParts = this.data.Wedges.GetFaceX(backWedge.Shape) & Mask;
					GetVoxel(backWedge.Data, backParts, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.FacePosX, nextX - this.data.FaceDepthOffset);
	}

	private void ContourFaceYInterior(DualPlanes<TSurface, TExtractor> plane, int iY)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		iY += this.data.Lower.Y;
		int nextY = iY + 1;

		for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iZ];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var frontWedges = this.data.Voxels[iX, iY, iZ];
				var backWedges = this.data.Voxels[iX, nextY, iZ];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < frontWedges.Length; iW++)
				{
					var frontWedge = frontWedges[iW];
					var backWedge = backWedges[iW];
					if (frontWedge.Shape.Index + backWedge.Shape.Index == 0) { break; }

					var frontParts = (QuadParts)((int)this.data.Wedges.GetFaceY(frontWedge.Shape) >> Shift);
					GetVoxel(frontWedge.Data, frontParts, ref frontVoxel);

					var backParts = this.data.Wedges.GetFaceY(backWedge.Shape) & Mask;
					GetVoxel(backWedge.Data, backParts, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.FacePosY, nextY - this.data.FaceDepthOffset);
	}

	private void ContourFaceZInterior(DualPlanes<TSurface, TExtractor> plane, int iZ)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		iZ += this.data.Lower.Z;
		int nextZ = iZ + 1;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iY];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var frontWedges = this.data.Voxels[iX, iY, iZ];
				var backWedges = this.data.Voxels[iX, iY, nextZ];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < frontWedges.Length; iW++)
				{
					var frontWedge = frontWedges[iW];
					var backWedge = backWedges[iW];
					if (frontWedge.Shape.Index + backWedge.Shape.Index == 0) { break; }

					var frontParts = (QuadParts)((int)this.data.Wedges.GetFaceZ(frontWedge.Shape) >> Shift);
					GetVoxel(frontWedge.Data, frontParts, ref frontVoxel);

					var backParts = this.data.Wedges.GetFaceZ(backWedge.Shape) & Mask;
					GetVoxel(backWedge.Data, backParts, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.FacePosZ, nextZ - this.data.FaceDepthOffset);
	}

	#endregion
	#region Contour Edges

	private void ContourEdgeXAscending(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForEdgeXAscending(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int cOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		for (int iB = lower.Y; iB <= upper.Y; iB++)
		{
			int iC = cOffset - iB;
			int iP = this.data.PlaneIndexer[lower.X, iB];

			for (int iA = lower.X; iA <= upper.X; iA++)
			{
				var wedges = this.data.Voxels[iA, iB, iC];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < wedges.Length; iW++)
				{
					var wedge = wedges[iW];
					if (wedge.Shape.Index == 0) { break; }

					var parts = this.data.Wedges.GetEdgeXAscending(wedge.Shape);
					GetVoxel(wedge.Data, (QuadParts)((int)parts >> Shift), ref frontVoxel);
					GetVoxel(wedge.Data, parts & Mask, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.EdgeXPosYPosZ, index + this.data.EdgeAscendingDepthOffset.X);
	}

	private void ContourEdgeXDescending(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForEdgeXDescending(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int cOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		for (int iB = lower.Y; iB <= upper.Y; iB++)
		{
			int iC = cOffset + iB;
			int iP = this.data.PlaneIndexer[lower.X, iB];

			for (int iA = lower.X; iA <= upper.X; iA++)
			{
				var wedges = this.data.Voxels[iA, iB, iC];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < wedges.Length; iW++)
				{
					var wedge = wedges[iW];
					if (wedge.Shape.Index == 0) { break; }

					var parts = this.data.Wedges.GetEdgeXDescending(wedge.Shape);
					GetVoxel(wedge.Data, (QuadParts)((int)parts >> Shift), ref frontVoxel);
					GetVoxel(wedge.Data, parts & Mask, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.EdgeXPosYNegZ, index + this.data.EdgeDescendingDepthOffset.X);
	}

	private void ContourEdgeYAscending(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForEdgeYAscending(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int aOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		for (int iB = lower.Y; iB <= upper.Y; iB++)
		{
			int iP = this.data.PlaneIndexer[lower.X, iB];

			for (int iA = lower.X; iA <= upper.X; iA++)
			{
				var wedges = this.data.Voxels[aOffset - iA, iB, iA];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < wedges.Length; iW++)
				{
					var wedge = wedges[iW];
					if (wedge.Shape.Index == 0) { break; }

					var parts = this.data.Wedges.GetEdgeYAscending(wedge.Shape);
					GetVoxel(wedge.Data, (QuadParts)((int)parts >> Shift), ref frontVoxel);
					GetVoxel(wedge.Data, parts & Mask, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.EdgeYPosXPosZ, index + this.data.EdgeAscendingDepthOffset.Y);
	}

	private void ContourEdgeYDescending(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForEdgeYDescending(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int aOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		for (int iB = lower.Y; iB <= upper.Y; iB++)
		{
			int iP = this.data.PlaneIndexer[lower.X, iB];

			for (int iA = lower.X; iA <= upper.X; iA++)
			{
				var wedges = this.data.Voxels[aOffset + iA, iB, iA];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < wedges.Length; iW++)
				{
					var wedge = wedges[iW];
					if (wedge.Shape.Index == 0) { break; }

					var parts = this.data.Wedges.GetEdgeYDescending(wedge.Shape);
					GetVoxel(wedge.Data, (QuadParts)((int)parts >> Shift), ref frontVoxel);
					GetVoxel(wedge.Data, parts & Mask, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.EdgeYPosXNegZ, index + this.data.EdgeDescendingDepthOffset.Y);
	}

	private void ContourEdgeZAscending(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForEdgeZAscending(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int cOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		for (int iB = lower.Y; iB <= upper.Y; iB++)
		{
			int iC = cOffset - iB;
			int iP = this.data.PlaneIndexer[lower.X, iB];

			for (int iA = lower.X; iA <= upper.X; iA++)
			{
				var wedges = this.data.Voxels[iC, iB, iA];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < wedges.Length; iW++)
				{
					var wedge = wedges[iW];
					if (wedge.Shape.Index == 0) { break; }

					var parts = this.data.Wedges.GetEdgeZAscending(wedge.Shape);
					GetVoxel(wedge.Data, (QuadParts)((int)parts >> Shift), ref frontVoxel);
					GetVoxel(wedge.Data, parts & Mask, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.EdgeZPosXPosY, index + this.data.EdgeAscendingDepthOffset.Z);
	}

	private void ContourEdgeZDescending(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForEdgeZDescending(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int cOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		for (int iB = lower.Y; iB <= upper.Y; iB++)
		{
			int iC = cOffset + iB;
			int iP = this.data.PlaneIndexer[lower.X, iB];

			for (int iA = lower.X; iA <= upper.X; iA++)
			{
				var wedges = this.data.Voxels[iC, iB, iA];
				var frontVoxel = this.data.EmptyVoxel;
				var backVoxel = this.data.EmptyVoxel;

				for (int iW = 0; iW < wedges.Length; iW++)
				{
					var wedge = wedges[iW];
					if (wedge.Shape.Index == 0) { break; }

					var parts = this.data.Wedges.GetEdgeZDescending(wedge.Shape);
					GetVoxel(wedge.Data, (QuadParts)((int)parts >> Shift), ref frontVoxel);
					GetVoxel(wedge.Data, parts & Mask, ref backVoxel);
				}

				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.data.Extractor.GetSurfaces(frontVoxel.Top, backVoxel.Top, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
				this.data.Extractor.GetSurfaces(frontVoxel.Right, backVoxel.Right, ref hasFront, ref front.Right, ref hasBack, ref back.Right);
				this.data.Extractor.GetSurfaces(frontVoxel.Bottom, backVoxel.Bottom, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
				this.data.Extractor.GetSurfaces(frontVoxel.Left, backVoxel.Left, ref hasFront, ref front.Left, ref hasBack, ref back.Left);
				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.EdgeZPosXNegY, index + this.data.EdgeDescendingDepthOffset.Z);
	}

	#endregion
	#region Contour Corners

	// TODO corners could possibly be optimized by taking advantage of the fact that each horizontal run
	// of voxels (aka iterating over iX) follows 1 of 3 patterns;
	// Empty voxels -> Filled Voxels -> Empty Voxels
	// Empty voxels -> Filled Voxels
	// Filled Voxels -> Empty Voxels
	// And that while iterating over iX, iY is always decreasing or always increasing (depending on which
	// corner it is). This means that the number of if statements involving iY inside the iX for loop
	// could be reduced by creating separate for loops for the segments of empty and filled voxels.
	private void ContourCornerNegXNegYNegZ(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForCornerNegXNegYNegZ(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int yOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		var emptyVoxel = this.data.EmptyVoxel.Top;
		int maxY = this.data.Upper.Y + 1;

		for (int iZ = lower.Y; iZ <= upper.Y; iZ++)
		{
			int iY = yOffset;
			int iP = this.data.PlaneIndexer[lower.X, iZ];

			// iY is decreasing
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;

				if (iY > this.data.Lower.Y && iY <= maxY)
				{
					// read lower voxel
					var wedges = this.data.Voxels[iX, iY - 1, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideTop(wedge.Shape);
						if ((corners & WedgeCorners.PPP) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NNN) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
					front.Right = front.Top;
					back.Right = back.Top;
				}

				if (iY >= this.data.Lower.Y && iY < maxY)
				{
					// read higher voxel
					var wedges = this.data.Voxels[iX, iY, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideBot(wedge.Shape);
						if ((corners & WedgeCorners.PPP) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NNN) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
					front.Left = front.Bottom;
					back.Left = back.Bottom;
				}

				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
				iY--;
			}

			yOffset--;
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.CornerPosXPosYPosZ, index - this.data.CornerNegXNegYNegZDepthOffset);
	}

	private void ContourCornerNegXNegYPosZ(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForCornerNegXNegYPosZ(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int yOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		var emptyVoxel = this.data.EmptyVoxel.Top;
		int maxY = this.data.Upper.Y + 1;

		for (int iZ = lower.Y; iZ <= upper.Y; iZ++)
		{
			int iY = yOffset;
			int iP = this.data.PlaneIndexer[lower.X, iZ];

			// iY is decreasing
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;

				if (iY > this.data.Lower.Y && iY <= maxY)
				{
					// read lower voxel
					var wedges = this.data.Voxels[iX, iY - 1, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideTop(wedge.Shape);
						if ((corners & WedgeCorners.PPN) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NNP) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
					front.Right = front.Bottom;
					back.Right = back.Bottom;
				}

				if (iY >= this.data.Lower.Y && iY < maxY)
				{
					// read higher voxel
					var wedges = this.data.Voxels[iX, iY, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideBot(wedge.Shape);
						if ((corners & WedgeCorners.PPN) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NNP) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
					front.Left = front.Top;
					back.Left = back.Top;
				}

				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
				iY--;
			}

			yOffset++;
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.CornerPosXPosYNegZ, index - this.data.CornerNegXNegYPosZDepthOffset);
	}

	private void ContourCornerNegXPosYNegZ(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForCornerNegXPosYNegZ(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int yOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		var emptyVoxel = this.data.EmptyVoxel.Top;
		int maxY = this.data.Upper.Y + 1;

		for (int iZ = lower.Y; iZ <= upper.Y; iZ++)
		{
			int iY = yOffset;
			int iP = this.data.PlaneIndexer[lower.X, iZ];

			// iY is increasing
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;

				if (iY > this.data.Lower.Y && iY <= maxY)
				{
					// read lower voxel
					var wedges = this.data.Voxels[iX, iY - 1, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideTop(wedge.Shape);
						if ((corners & WedgeCorners.PNP) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NPN) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
					front.Left = front.Bottom;
					back.Left = back.Bottom;
				}

				if (iY >= this.data.Lower.Y && iY < maxY)
				{
					// read higher voxel
					var wedges = this.data.Voxels[iX, iY, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideBot(wedge.Shape);
						if ((corners & WedgeCorners.PNP) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NPN) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
					front.Right = front.Top;
					back.Right = back.Top;
				}

				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
				iY++;
			}

			yOffset++;
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.CornerNegXPosYNegZ, index - this.data.CornerNegXPosYNegZDepthOffset);
	}

	private void ContourCornerNegXPosYPosZ(DualPlanes<TSurface, TExtractor> plane, int index)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		this.data.SetBoundsForCornerNegXPosYPosZ(
			plane.Contourer.Bounds, index, out Int2 lower, out Int2 upper, out int yOffset);

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		var emptyVoxel = this.data.EmptyVoxel.Top;
		int maxY = this.data.Upper.Y + 1;

		for (int iZ = lower.Y; iZ <= upper.Y; iZ++)
		{
			int iY = yOffset;
			int iP = this.data.PlaneIndexer[lower.X, iZ];

			// iY is increasing
			for (int iX = lower.X; iX <= upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;

				if (iY > this.data.Lower.Y && iY <= maxY)
				{
					// read lower voxel
					var wedges = this.data.Voxels[iX, iY - 1, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideTop(wedge.Shape);
						if ((corners & WedgeCorners.PNN) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NPP) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Top, ref hasBack, ref back.Top);
					front.Left = front.Top;
					back.Left = back.Top;
				}

				if (iY >= this.data.Lower.Y && iY < maxY)
				{
					// read higher voxel
					var wedges = this.data.Voxels[iX, iY, iZ];
					var frontVoxel = emptyVoxel;
					var backVoxel = emptyVoxel;

					for (int iW = 0; iW < wedges.Length; iW++)
					{
						var wedge = wedges[iW];
						if (wedge.Shape.Index == 0) { break; }

						var corners = this.data.Wedges.GetCornerWideBot(wedge.Shape);
						if ((corners & WedgeCorners.PNN) != 0) { frontVoxel = wedge.Data; }
						if ((corners & WedgeCorners.NPP) != 0) { backVoxel = wedge.Data; }
					}

					this.data.Extractor.GetSurfaces(
						frontVoxel, backVoxel, ref hasFront, ref front.Bottom, ref hasBack, ref back.Bottom);
					front.Right = front.Bottom;
					back.Right = back.Bottom;
				}

				frontSurfaces[iP] = front;
				backSurfaces[iP] = back;
				iP++;
				iY++;
			}

			yOffset--;
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.CornerNegXPosYPosZ, index - this.data.CornerNegXPosYPosZDepthOffset);
	}

	#endregion
}
