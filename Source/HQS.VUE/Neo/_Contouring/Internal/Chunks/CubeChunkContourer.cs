﻿namespace HQS.VUE.Neo;

internal class CubeChunkContourer<TVoxel, TSurface, TExtractor> :
	AbstractChunkContourer<TVoxel, TSurface, TExtractor>, ICubeChunkContourer<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	private ChunkContourerSharedData<TVoxel, TSurface, TExtractor> data;

	public CubeChunkContourer(
		ChunkSize size,
		IPool<DualPlanes<TSurface, TExtractor>> contourerPool,
		CoplanarChunk<TSurface>.Builder chunkBuilder)
		: base(size, contourerPool, chunkBuilder)
	{
	}

	/// <inheritdoc />
	public CoplanarChunk<TSurface> ContourChunk(CubeChunk<TVoxel> chunk)
	{
		ICubeChunkContourerContracts.ContourChunk(this, chunk);

		if (this.Bounds.IsEmpty) { return CoplanarChunk<TSurface>.Empty; }

		if (chunk.IsUniform)
		{
			// TODO need to handle uniform chunk properly
			Console.WriteLine("Skipping uniform chunk");
			return default;
		}

		// TODO check that chunk size matches Bounds?
		this.InitializeContourChunk(chunk.Size);

		this.data = new ChunkContourerSharedData<TVoxel, TSurface, TExtractor>(this, chunk);

		return this.ContourChunk(
			this.Bounds.Dimensions.X +
			this.Bounds.Dimensions.Y +
			this.Bounds.Dimensions.Z + 3);
	}

	/// <inheritdoc />
	protected sealed override void ProcessDualPlanes(
		ref ContourChunkThreadLocalState<TSurface, TExtractor> state, int index)
	{
		state.Plane.Profiler.StartIndexPlane();

		while (true)
		{
			Debug.Assert(index >= state.LowerBound);

			switch (state.CurrentAxis)
			{
				case PlaneAxis.FaceX:
					if (index < state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						if (index == state.LowerBound)
						{
							this.ContourFaceXBack(state.Plane);
							return;
						}

						this.ContourFaceXInterior(state.Plane, index - state.LowerBound - 1);
						return;
					}

					if (index == state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourFaceXFront(state.Plane);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.Bounds.Dimensions.Y + 1);
					this.InitializeFaceY(state.Plane);
					break;

				case PlaneAxis.FaceY:
					if (index < state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						if (index == state.LowerBound)
						{
							this.ContourFaceYBack(state.Plane);
							return;
						}

						this.ContourFaceYInterior(state.Plane, index - state.LowerBound - 1);
						return;
					}

					if (index == state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourFaceYFront(state.Plane);
						return;
					}

					// didn't find a plane to process so move the CurrentAxis and loop
					state.AdvanceToNextAxis(this.Bounds.Dimensions.Z + 1);
					this.InitializeFaceZ(state.Plane);
					break;

				case PlaneAxis.FaceZ:
					if (index < state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						if (index == state.LowerBound)
						{
							this.ContourFaceZBack(state.Plane);
							return;
						}

						this.ContourFaceZInterior(state.Plane, index - state.LowerBound - 1);
						return;
					}

					if (index == state.UpperBound)
					{
						state.Plane.Profiler.StopIndexPlane();

						this.ContourFaceZFront(state.Plane);
						return;
					}

					// finished contouring, this should NOT be reached
					state.Plane.Profiler.StopIndexPlane();
					Debug.Assert(false);
					return;

				default:
					state.Plane.Profiler.StopIndexPlane();
					throw InvalidEnumArgument.CreateException(
						$"{nameof(state)}.{nameof(state.CurrentAxis)}", state.CurrentAxis);
			}
		}
	}

	private void ContourFaceXFront(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		var frontSurfaces = plane.FrontSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.Z, iY];

			for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
			{
				var front = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[this.data.Upper.X, iY, iZ], ref hasFront, ref front);
				frontSurfaces[iP] = new SurfaceQuad<TSurface>(front);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourFront(plane, hasFront, PlaneNormal.FacePosX, this.data.Upper.X - this.data.DepthOffset + 1);
	}

	private void ContourFaceXBack(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasBack = false;
		var backSurfaces = plane.BackSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.Z, iY];

			for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
			{
				var back = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[this.data.Lower.X, iY, iZ], ref hasBack, ref back);
				backSurfaces[iP] = new SurfaceQuad<TSurface>(back);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBack(plane, hasBack, PlaneNormal.FaceNegX, this.data.Lower.X - this.data.DepthOffset);
	}

	private void ContourFaceYFront(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		var frontSurfaces = plane.FrontSurfaces;

		for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iZ];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[iX, this.data.Upper.Y, iZ], ref hasFront, ref front);
				frontSurfaces[iP] = new SurfaceQuad<TSurface>(front);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourFront(plane, hasFront, PlaneNormal.FacePosY, this.data.Upper.Y - this.data.DepthOffset + 1);
	}

	private void ContourFaceYBack(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasBack = false;
		var backSurfaces = plane.BackSurfaces;

		for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iZ];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var back = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[iX, this.data.Lower.Y, iZ], ref hasBack, ref back);
				backSurfaces[iP] = new SurfaceQuad<TSurface>(back);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBack(plane, hasBack, PlaneNormal.FaceNegY, this.data.Lower.Y - this.data.DepthOffset);
	}

	private void ContourFaceZFront(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		var frontSurfaces = plane.FrontSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iY];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[iX, iY, this.data.Upper.Z], ref hasFront, ref front);
				frontSurfaces[iP] = new SurfaceQuad<TSurface>(front);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourFront(plane, hasFront, PlaneNormal.FacePosZ, this.data.Upper.Z - this.data.DepthOffset + 1);
	}

	private void ContourFaceZBack(DualPlanes<TSurface, TExtractor> plane)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasBack = false;
		var backSurfaces = plane.BackSurfaces;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iY];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var back = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[iX, iY, this.data.Lower.Z], ref hasBack, ref back);
				backSurfaces[iP] = new SurfaceQuad<TSurface>(back);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBack(plane, hasBack, PlaneNormal.FaceNegZ, this.data.Lower.Z - this.data.DepthOffset);
	}

	private void ContourFaceXInterior(DualPlanes<TSurface, TExtractor> plane, int iX)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		iX += this.data.Lower.X;
		int nextX = iX + 1;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.Z, iY];

			for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
			{
				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[iX, iY, iZ],
					this.data.Voxels[nextX, iY, iZ],
					ref hasFront,
					ref front,
					ref hasBack,
					ref back);
				frontSurfaces[iP] = new SurfaceQuad<TSurface>(front);
				backSurfaces[iP] = new SurfaceQuad<TSurface>(back);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.FacePosX, nextX - this.data.DepthOffset);
	}

	private void ContourFaceYInterior(DualPlanes<TSurface, TExtractor> plane, int iY)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		iY += this.data.Lower.Y;
		int nextY = iY + 1;

		for (int iZ = this.data.Lower.Z; iZ <= this.data.Upper.Z; iZ++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iZ];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[iX, iY, iZ],
					this.data.Voxels[iX, nextY, iZ],
					ref hasFront,
					ref front,
					ref hasBack,
					ref back);
				frontSurfaces[iP] = new SurfaceQuad<TSurface>(front);
				backSurfaces[iP] = new SurfaceQuad<TSurface>(back);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.FacePosY, nextY - this.data.DepthOffset);
	}

	private void ContourFaceZInterior(DualPlanes<TSurface, TExtractor> plane, int iZ)
	{
		Debug.Assert(plane != null);

		plane.Profiler.StartFillSurfaces();

		bool hasFront = false;
		bool hasBack = false;
		var frontSurfaces = plane.FrontSurfaces;
		var backSurfaces = plane.BackSurfaces;

		iZ += this.data.Lower.Z;
		int nextZ = iZ + 1;

		for (int iY = this.data.Lower.Y; iY <= this.data.Upper.Y; iY++)
		{
			int iP = this.data.PlaneIndexer[this.data.Lower.X, iY];

			for (int iX = this.data.Lower.X; iX <= this.data.Upper.X; iX++)
			{
				var front = this.data.EmptySurface;
				var back = this.data.EmptySurface;
				this.Extractor.GetSurfaces(
					this.data.Voxels[iX, iY, iZ],
					this.data.Voxels[iX, iY, nextZ],
					ref hasFront,
					ref front,
					ref hasBack,
					ref back);
				frontSurfaces[iP] = new SurfaceQuad<TSurface>(front);
				backSurfaces[iP] = new SurfaceQuad<TSurface>(back);
				iP++;
			}
		}

		plane.Profiler.StopFillSurfaces();

		ContourBoth(plane, hasFront, hasBack, PlaneNormal.FacePosZ, nextZ - this.data.DepthOffset);
	}
}
