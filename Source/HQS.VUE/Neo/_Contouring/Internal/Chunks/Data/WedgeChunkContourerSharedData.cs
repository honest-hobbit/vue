﻿namespace HQS.VUE.Neo;

internal readonly record struct WedgeChunkContourerSharedData<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	public readonly WedgeTable Wedges;

	public readonly TExtractor Extractor;

	public readonly SurfaceQuad<TVoxel> EmptyVoxel;

	public readonly SurfaceQuad<TSurface> EmptySurface;

	public readonly ReadOnlyWedgeArray<TVoxel> Voxels;

	public readonly SquareArrayIndexer PlaneIndexer;

	public readonly Int3 Lower;

	public readonly Int3 Upper;

	public readonly Int3 Dimensions;

	public readonly int FaceDepthOffset;

	public readonly Int3 EdgePlanesCount;

	public readonly Int3 EdgeMaxLength;

	public readonly Int3 EdgeAscendingDepthOffset;

	public readonly Int3 EdgeDescendingDepthOffset;

	public readonly int CornerPlanesCount;

	public readonly int CornerNegXNegYNegZDepthOffset;

	public readonly int CornerNegXNegYPosZDepthOffset;

	public readonly int CornerNegXPosYNegZDepthOffset;

	public readonly int CornerNegXPosYPosZDepthOffset;

	public WedgeChunkContourerSharedData(
		WedgeChunkContourer<TVoxel, TSurface, TExtractor> contourer, WedgeChunk<TVoxel> chunk)
	{
		Debug.Assert(contourer != null);
		Debug.Assert(contourer.Wedges != null);
		Debug.Assert(!chunk.IsDefault);

		this.Wedges = contourer.Wedges;
		this.Extractor = contourer.Extractor;
		this.EmptyVoxel = new SurfaceQuad<TVoxel>(contourer.Extractor.EmptyVoxel);
		this.EmptySurface = new SurfaceQuad<TSurface>(contourer.Extractor.EmptySurface);
		this.Voxels = chunk.Voxels;
		this.PlaneIndexer = chunk.Size.AsSquare;

		this.Lower = contourer.Bounds.Lower;
		this.Upper = contourer.Bounds.Upper;
		this.Dimensions = contourer.Bounds.Dimensions;

		int sideLength = chunk.Size.SideLength;
		this.FaceDepthOffset = sideLength >> 1;

		this.EdgePlanesCount = new Int3(
			this.Dimensions.Y + this.Dimensions.Z - 1,
			this.Dimensions.X + this.Dimensions.Z - 1,
			this.Dimensions.X + this.Dimensions.Y - 1);
		this.EdgeMaxLength = new Int3(
			Math.Min(this.Dimensions.Y, this.Dimensions.Z),
			Math.Min(this.Dimensions.X, this.Dimensions.Z),
			Math.Min(this.Dimensions.X, this.Dimensions.Y));
		this.EdgeAscendingDepthOffset = new Int3(
			1 + this.Lower.Y + this.Lower.Z - sideLength,
			1 + this.Lower.X + this.Lower.Z - sideLength,
			1 + this.Lower.X + this.Lower.Y - sideLength);
		this.EdgeDescendingDepthOffset = new Int3(
			1 + this.Lower.Z - this.Lower.Y - this.Dimensions.Y,
			1 + this.Lower.X - this.Lower.Z - this.Dimensions.Z,
			1 + this.Lower.X - this.Lower.Y - this.Dimensions.Y);

		this.CornerPlanesCount = this.Dimensions.X + this.Dimensions.Y + this.Dimensions.Z - 1;

		this.CornerNegXNegYNegZDepthOffset =
			(this.CornerPlanesCount >> 1) - this.Lower.X - this.Lower.Y - this.Lower.Z +
			(((sideLength * 3) - this.Dimensions.X - this.Dimensions.Y - this.Dimensions.Z) >> 1);

		this.CornerNegXNegYPosZDepthOffset =
			(this.CornerPlanesCount >> 1) - this.Lower.X - this.Lower.Y + this.Lower.Z +
			((sideLength - this.Dimensions.X - this.Dimensions.Y + this.Dimensions.Z) >> 1);

		this.CornerNegXPosYNegZDepthOffset =
			(this.CornerPlanesCount >> 1) + this.Lower.X - this.Lower.Y + this.Lower.Z -
			((sideLength - this.Dimensions.X + this.Dimensions.Y - this.Dimensions.Z + 1) >> 1);

		this.CornerNegXPosYPosZDepthOffset =
			(this.CornerPlanesCount >> 1) + this.Lower.X - this.Lower.Y - this.Lower.Z +
			((sideLength + this.Dimensions.X - this.Dimensions.Y - this.Dimensions.Z) >> 1);
	}

	public int TotalPlanesCount =>
		this.Dimensions.X + this.Dimensions.Y + this.Dimensions.Z + 3 +
		((this.EdgePlanesCount.X + this.EdgePlanesCount.Y + this.EdgePlanesCount.Z) << 1) +
		(this.CornerPlanesCount << 2);

	public void SetBoundsForEdgeXAscending(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		var dimensions = new Int2(
			this.Dimensions.X,
			Math.Min(index + 1, this.EdgePlanesCount.X - index).ClampUpper(this.EdgeMaxLength.X));
		lower = new Int2(
			this.Lower.X,
			this.Lower.Y + (index + 1 - this.Dimensions.Z).ClampLower(0));

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.Lower.Y + this.Lower.Z;
	}

	public void SetBoundsForEdgeXDescending(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		var dimensions = new Int2(
			this.Dimensions.X,
			Math.Min(index + 1, this.EdgePlanesCount.X - index).ClampUpper(this.EdgeMaxLength.X));
		lower = new Int2(
			this.Lower.X,
			this.Lower.Y + (this.Dimensions.Y - index - 1).ClampLower(0));

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.EdgeDescendingDepthOffset.X;
	}

	public void SetBoundsForEdgeYAscending(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		var dimensions = new Int2(
			Math.Min(index + 1, this.EdgePlanesCount.Y - index).ClampUpper(this.EdgeMaxLength.Y),
			this.Dimensions.Y);
		lower = new Int2(
			this.Lower.Z + (index + 1 - this.Dimensions.X).ClampLower(0),
			this.Lower.Y);

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.Lower.Z + this.Lower.X;
	}

	public void SetBoundsForEdgeYDescending(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		var dimensions = new Int2(
			Math.Min(index + 1, this.EdgePlanesCount.Y - index).ClampUpper(this.EdgeMaxLength.Y),
			this.Dimensions.Y);
		lower = new Int2(
			this.Lower.Z + (this.Dimensions.Z - index - 1).ClampLower(0),
			this.Lower.Y);

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.EdgeDescendingDepthOffset.Y;
	}

	public void SetBoundsForEdgeZAscending(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		var dimensions = new Int2(
			this.Dimensions.Z,
			Math.Min(index + 1, this.EdgePlanesCount.Z - index).ClampUpper(this.EdgeMaxLength.Z));
		lower = new Int2(
			this.Lower.Z,
			this.Lower.Y + (index + 1 - this.Dimensions.X).ClampLower(0));

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.Lower.Y + this.Lower.X;
	}

	public void SetBoundsForEdgeZDescending(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		var dimensions = new Int2(
			this.Dimensions.Z,
			Math.Min(index + 1, this.EdgePlanesCount.Z - index).ClampUpper(this.EdgeMaxLength.Z));
		lower = new Int2(
			this.Lower.Z,
			this.Lower.Y + (this.Dimensions.Y - index - 1).ClampLower(0));

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.EdgeDescendingDepthOffset.Z;
	}

	public void SetBoundsForCornerNegXNegYNegZ(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		lower = new Int2(this.Lower.X, this.Lower.Z);
		var dimensions = new Int2(this.Dimensions.X, this.Dimensions.Z);
		int distanceFromRangeBounds = this.CornerPlanesCount - index;

		if (index <= distanceFromRangeBounds)
		{
			distanceFromRangeBounds = index + 1;

			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			var unclampedDimensions = dimensions;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
			lower += unclampedDimensions - dimensions;
		}
		else
		{
			var unclampedDimensions = dimensions;
			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			lower += unclampedDimensions - dimensions;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
		}

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.Lower.Y + this.Lower.X + this.Lower.Z - lower.X - lower.Y;
	}

	public void SetBoundsForCornerNegXNegYPosZ(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		lower = new Int2(this.Lower.X, this.Lower.Z);
		var dimensions = new Int2(this.Dimensions.X, this.Dimensions.Z);
		int distanceFromRangeBounds = this.CornerPlanesCount - index;

		if (index <= distanceFromRangeBounds)
		{
			distanceFromRangeBounds = index + 1;

			int unclampedY = dimensions.Y;
			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			lower.Y += unclampedY - dimensions.Y;
			int unclampedX = dimensions.X;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
			lower.X += unclampedX - dimensions.X;
		}
		else
		{
			int unclampedX = dimensions.X;
			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			lower.X += unclampedX - dimensions.X;
			int unclampedY = dimensions.Y;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
			lower.Y += unclampedY - dimensions.Y;
		}

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = (index - this.Dimensions.Z + this.Lower.Y + 1).Clamp(this.Lower.Y, this.Upper.Y + 1);
	}

	public void SetBoundsForCornerNegXPosYNegZ(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		lower = new Int2(this.Lower.X, this.Lower.Z);
		var dimensions = new Int2(this.Dimensions.X, this.Dimensions.Z);
		int distanceFromRangeBounds = this.CornerPlanesCount - index;

		if (index <= distanceFromRangeBounds)
		{
			distanceFromRangeBounds = index + 1;

			var unclampedDimensions = dimensions;
			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			lower += unclampedDimensions - dimensions;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
		}
		else
		{
			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			var unclampedDimensions = dimensions;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
			lower += unclampedDimensions - dimensions;
		}

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = index + this.Lower.Y - this.Lower.X - this.Lower.Z
			- this.Dimensions.X - this.Dimensions.Z + lower.X + lower.Y + 2;
	}

	public void SetBoundsForCornerNegXPosYPosZ(
		IContouringBounds<Int2> bounds, int index, out Int2 lower, out Int2 upper, out int offset)
	{
		Debug.Assert(bounds != null);

		lower = new Int2(this.Lower.X, this.Lower.Z);
		var dimensions = new Int2(this.Dimensions.X, this.Dimensions.Z);
		int distanceFromRangeBounds = this.CornerPlanesCount - index;

		if (index <= distanceFromRangeBounds)
		{
			distanceFromRangeBounds = index + 1;

			int unclampedX = dimensions.X;
			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			lower.X += unclampedX - dimensions.X;
			int unclampedY = dimensions.Y;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
			lower.Y += unclampedY - dimensions.Y;
		}
		else
		{
			int unclampedY = dimensions.Y;
			this.ClampDimensionsByDistance(ref dimensions, distanceFromRangeBounds);
			lower.Y += unclampedY - dimensions.Y;
			int unclampedX = dimensions.X;
			this.SwapAndClampDimensionsByHeight(ref dimensions);
			lower.X += unclampedX - dimensions.X;
		}

		bounds.SetBounds(lower, dimensions);
		upper = bounds.Upper;
		offset = (index - this.Dimensions.X + this.Lower.Y + 1).Clamp(this.Lower.Y, this.Upper.Y + 1);
	}

	private void ClampDimensionsByDistance(ref Int2 dimensions, int distanceFromRangeBounds)
	{
		dimensions.X = dimensions.X.ClampUpper(distanceFromRangeBounds);
		dimensions.Y = dimensions.Y.ClampUpper(distanceFromRangeBounds);
	}

	private void SwapAndClampDimensionsByHeight(ref Int2 dimensions)
	{
		int tempX = dimensions.X;
		dimensions.X = dimensions.X.ClampUpper(dimensions.Y + this.Dimensions.Y);
		dimensions.Y = dimensions.Y.ClampUpper(tempX + this.Dimensions.Y);
	}
}
