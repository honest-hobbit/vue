﻿namespace HQS.VUE.Neo;

internal readonly record struct ChunkContourerSharedData<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	public readonly SquareArrayIndexer PlaneIndexer;

	public readonly TExtractor Extractor;

	public readonly TSurface EmptySurface;

	public readonly ReadOnlyCubeArray<TVoxel> Voxels;

	public readonly Int3 Lower;

	public readonly Int3 Upper;

	public readonly int DepthOffset;

	public ChunkContourerSharedData(
		CubeChunkContourer<TVoxel, TSurface, TExtractor> contourer, CubeChunk<TVoxel> chunk)
	{
		Debug.Assert(contourer != null);
		Debug.Assert(!chunk.IsDefault);

		this.PlaneIndexer = chunk.Size.AsSquare;
		this.Extractor = contourer.Extractor;
		this.EmptySurface = contourer.Extractor.EmptySurface;
		this.Voxels = chunk.Voxels;
		this.Lower = contourer.Bounds.Lower;
		this.Upper = contourer.Bounds.Upper;
		this.DepthOffset = chunk.Size.SideLength >> 1;
	}
}
