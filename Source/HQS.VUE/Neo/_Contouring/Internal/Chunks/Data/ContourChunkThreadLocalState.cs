﻿namespace HQS.VUE.Neo;

internal struct ContourChunkThreadLocalState<TSurface, TComparer>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	public DualPlanes<TSurface, TComparer> Plane { get; private set; }

	public PlaneAxis CurrentAxis { get; private set; }

	public int LowerBound { get; private set; }

	public int UpperBound { get; private set; }

	public void Initialize(DualPlanes<TSurface, TComparer> plane, int rangeLength)
	{
		Debug.Assert(plane != null);
		Debug.Assert(rangeLength >= 1);

		this.Plane = plane;
		this.CurrentAxis = PlaneAxis.FaceX;
		this.LowerBound = 0;
		this.UpperBound = rangeLength - 1;
	}

	public void AdvanceToNextAxis(int rangeLength)
	{
		Debug.Assert(rangeLength >= 1);

		this.CurrentAxis++;
		this.LowerBound = this.UpperBound + 1;
		this.UpperBound = this.LowerBound + rangeLength - 1;
	}
}
