﻿namespace HQS.VUE.Neo;

internal enum PlaneAxis : byte
{
	FaceX = 0,

	FaceY,

	FaceZ,

	EdgeXAscending,

	EdgeXDescending,

	EdgeYAscending,

	EdgeYDescending,

	EdgeZAscending,

	EdgeZDescending,

	CornerNegXNegYNegZ,

	CornerNegXNegYPosZ,

	CornerNegXPosYNegZ,

	CornerNegXPosYPosZ,
}
