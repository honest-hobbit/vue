﻿namespace HQS.VUE.Neo;

internal class DualPlanes<TSurface, TComparer>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	public DualPlanes(
		IPlaneContourer<TSurface, TComparer> contourer,
		SurfacePlane<TSurface> frontSurfaces,
		SurfacePlane<TSurface> backSurfaces,
		CoplanarChunk<TSurface>.Builder frontOutput,
		CoplanarChunk<TSurface>.Builder backOutput)
	{
		Debug.Assert(contourer != null);
		Debug.Assert(frontSurfaces != null);
		Debug.Assert(backSurfaces != null);
		Debug.Assert(frontOutput != null);
		Debug.Assert(backOutput != null);

		this.Contourer = contourer;
		this.FrontSurfaces = frontSurfaces;
		this.BackSurfaces = backSurfaces;
		this.FrontOutput = frontOutput;
		this.BackOutput = backOutput;
	}

	public DualPlanesProfiler Profiler { get; } = new DualPlanesProfiler();

	public IPlaneContourer<TSurface, TComparer> Contourer { get; }

	public SurfacePlane<TSurface> FrontSurfaces { get; }

	public SurfacePlane<TSurface> BackSurfaces { get; }

	public CoplanarChunk<TSurface>.Builder FrontOutput { get; }

	public CoplanarChunk<TSurface>.Builder BackOutput { get; }

	public bool IsFrontReversed { get; set; }
}
