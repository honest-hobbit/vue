﻿namespace HQS.VUE.Neo;

internal class CoplanarChunkMerger<TSurface, TComparer>
	where TSurface : unmanaged
	where TComparer : ISurfaceComparer<TSurface>
{
	private readonly CoplanarChunkEnumerator<TSurface>[] enumerators =
		new CoplanarChunkEnumerator<TSurface>[DegreeOfParallelism.ProcessorCount];

	private readonly ChunkContourerProfiler profiler;

	private readonly IPool<DualPlanes<TSurface, TComparer>> contourerPool;

	private readonly CoplanarChunk<TSurface>.Builder chunkBuilder;

	public CoplanarChunkMerger(
		ChunkContourerProfiler profiler,
		IPool<DualPlanes<TSurface, TComparer>> contourerPool,
		CoplanarChunk<TSurface>.Builder chunkBuilder)
	{
		Debug.Assert(contourerPool != null);
		Debug.Assert(chunkBuilder != null);

		this.profiler = profiler;
		this.contourerPool = contourerPool;
		this.chunkBuilder = chunkBuilder;
	}

	public CoplanarChunk<TSurface> MergeStates(
		List<ContourChunkThreadLocalState<TSurface, TComparer>> states, bool sortAndCompactOutput)
	{
		Debug.Assert(states != null);

		this.profiler.StopParallelizedTotal();
		this.profiler.StartSerializedTotal();

		if (sortAndCompactOutput)
		{
			this.SortAndCompactOutputStates(states);
		}
		else
		{
			this.FastConcatOutputStates(states);
		}

		this.profiler.StartFinalizeOutput();

		// merge the thread local profiler data into the shared profiler
		// and return the thread local state to the pool
		foreach (var state in states)
		{
			this.profiler.CopyIn(state.Plane.Profiler);
			this.profiler.CopyIn(state.Plane.Contourer.Profiler);

			state.Plane.FrontOutput.ClearPages();
			state.Plane.BackOutput.ClearPages();

			this.contourerPool.Return(state.Plane);
		}

		// build the merged chunk to return
		var mergedChunk = this.chunkBuilder.Build();

		this.profiler.StopFinalizeOutput();
		this.profiler.StopSerializedTotal();
		this.profiler.StopContourChunkTotal();
		this.profiler.AddToOutputChunks(1);
		this.profiler.AssertNotRunning();

		return mergedChunk;
	}

	private void FastConcatOutputStates(List<ContourChunkThreadLocalState<TSurface, TComparer>> states)
	{
		Debug.Assert(states != null);

		this.profiler.StartMergeOutput();

		foreach (var state in states)
		{
			ConcatOutput(state.Plane.BackOutput);
			ConcatOutput(state.Plane.FrontOutput);
		}

		this.profiler.StopMergeOutput();

		void ConcatOutput(CoplanarChunk<TSurface>.Builder builder)
		{
			Debug.Assert(builder != null);

			if (builder.Planes.Count >= 1)
			{
				this.chunkBuilder.Concat(builder);
			}
		}
	}

	private void SortAndCompactOutputStates(List<ContourChunkThreadLocalState<TSurface, TComparer>> states)
	{
		Debug.Assert(states != null);

		this.profiler.StartSortOutput();

		// create enumerators from the back facing surfaces and combine them
		int length = 0;
		foreach (var state in states) { AddEnumerator(state.Plane.BackOutput); }
		this.SortAndCombineEnumerators(length);

		// create enumerators from the front facing surfaces and combine them
		length = 0;
		foreach (var state in states) { AddEnumerator(state.Plane.FrontOutput); }
		this.SortAndCombineEnumerators(length);

		CoplanarChunkUtility.AssertPlanesAreSorted(this.chunkBuilder);

		void AddEnumerator(CoplanarChunk<TSurface>.Builder builder)
		{
			Debug.Assert(builder != null);

			if (builder.Planes.Count >= 1)
			{
				this.enumerators[length] = new CoplanarChunkEnumerator<TSurface>(builder);
				length++;
			}
		}
	}

	private void SortAndCombineEnumerators(int length)
	{
		Debug.Assert(length >= 0);

		// while there are 2 or more enumerators left we need to find the enumerator
		// with the lowest plane index to copy its next plane into the merged chunk
		while (length >= 2)
		{
			// find the enumerator with the lowest plane index
			int lowestIndex = 0;
			ushort lowestPlane = this.enumerators[lowestIndex].NextPlane.Index;

			for (int index = 1; index < length; index++)
			{
				ushort plane = this.enumerators[index].NextPlane.Index;
				if (plane < lowestPlane)
				{
					lowestIndex = index;
					lowestPlane = plane;
				}
			}

			this.profiler.StopSortOutput();
			this.profiler.StartMergeOutput();

			// copy the plane found into the merged chunk
			this.enumerators[lowestIndex].AddNextPlaneTo(this.chunkBuilder);

			this.profiler.StopMergeOutput();
			this.profiler.StartSortOutput();

			// check if enumerator has ran out and if so
			// replace it with the last enumerator and shrink the array length
			if (!this.enumerators[lowestIndex].HasNextPlane)
			{
				length--;
				this.enumerators[lowestIndex] = this.enumerators[length];
				this.enumerators[length] = default;
			}
		}

		this.profiler.StopSortOutput();
		this.profiler.StartMergeOutput();

		if (length == 1)
		{
			// only 1 enumerator left so copy all its contents into the merged chunk
			this.enumerators[0].AddAllRemainingPlanesTo(this.chunkBuilder);
			this.enumerators[0] = default;
		}

		this.profiler.StopMergeOutput();
	}
}
