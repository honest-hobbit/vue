﻿namespace HQS.VUE.Neo;

internal static class IWedgeChunkContourerContracts
{
	public static void ContourChunk<TVoxel, TSurface, TExtractor>(
		IWedgeChunkContourer<TVoxel, TSurface, TExtractor> contourer, WedgeChunk<TVoxel> chunk)
		where TVoxel : unmanaged
		where TSurface : unmanaged
		where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
	{
		Debug.Assert(contourer != null);

		Ensure.That(contourer.Wedges, nameof(contourer.Wedges)).IsNotNull();

		// TODO could this be changed to allow the chunk
		// to be a smaller ChunkSize than the chunk contourer?
		Ensure.That(chunk, nameof(chunk)).IsAssigned();
		Ensure.That(chunk.Size.Exponent, $"{nameof(chunk)}.{nameof(chunk.Size)}")
			.Is(contourer.Bounds.Size.Exponent);
	}
}
