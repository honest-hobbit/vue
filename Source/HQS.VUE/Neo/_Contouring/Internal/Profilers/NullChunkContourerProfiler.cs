﻿namespace HQS.VUE.Neo;

internal class NullChunkContourerProfiler : NullPlaneContourerProfiler, IChunkContourerProfiler
{
	/// <inheritdoc />
	public int MaxDegreeOfParallelism => 1;

	/// <inheritdoc />
	public bool SortAndCompactOutput => false;

	/// <inheritdoc />
	public TimeSpan InitializeChunk => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan IndexPlane => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan FillSurfaces => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan ProcessPlanesTotal => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan SortOutput => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan MergeOutput => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan FinalizeOutput => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan SerializedTotal => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan ParallelizedTotal => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan ContourChunkTotal => TimeSpan.Zero;

	/// <inheritdoc />
	public int PlanesProcessed => 0;

	/// <inheritdoc />
	public int PlanesSkipped => 0;

	/// <inheritdoc />
	public int OutputChunks => 0;
}
