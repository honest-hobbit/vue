﻿namespace HQS.VUE.Neo;

internal class NullPlaneContourerProfiler : IPlaneContourerProfiler
{
	/// <inheritdoc />
	public bool IsProfilerEnabled => false;

	/// <inheritdoc />
	public bool CheckForTrianglesMissed => false;

	/// <inheritdoc />
	public TimeSpan InitializePlane => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan MarkEdges => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan TraceEdges => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan FindHoles => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan SortEdges => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan FindEdgesTotal => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan TriangulateSpecial => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan TriangulateSimple => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan TriangulateComplex => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan LibTessAddContours => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan LibTessTessellate => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan PrepareTriangles => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan CombineTriangles => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan RecordTriangles => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan TriangulateTotal => TimeSpan.Zero;

	/// <inheritdoc />
	public TimeSpan ContourPlaneTotal => TimeSpan.Zero;

	/// <inheritdoc />
	public int OutputPlanes => 0;

	/// <inheritdoc />
	public int OutputSurfaces => 0;

	/// <inheritdoc />
	public int OutputTriangles => 0;

	/// <inheritdoc />
	public int TrianglesCombined => 0;

	/// <inheritdoc />
	public int TrianglesMissed => 0;

	/// <inheritdoc />
	public int PolygonsSimple => 0;

	/// <inheritdoc />
	public int PolygonsComplex => 0;

	/// <inheritdoc />
	public int PolygonsSpecial => 0;

	/// <inheritdoc />
	public int LibTessPooledMeshes => 0;

	/// <inheritdoc />
	public int LibTessPooledVertices => 0;

	/// <inheritdoc />
	public int LibTessPooledFaces => 0;

	/// <inheritdoc />
	public int LibTessPooledEdges => 0;

	/// <inheritdoc />
	public int LibTessPooledRegions => 0;

	/// <inheritdoc />
	public int LibTessPooledNodes => 0;

	/// <inheritdoc />
	public int LibTessVerticesLength => 0;

	/// <inheritdoc />
	public int LibTessElementsLength => 0;

	/// <inheritdoc />
	public int TriangleCombinerLength => 0;

	/// <inheritdoc />
	public void Reset()
	{
	}
}
