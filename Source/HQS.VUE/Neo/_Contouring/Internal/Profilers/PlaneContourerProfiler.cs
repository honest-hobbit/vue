﻿namespace HQS.VUE.Neo;

internal class PlaneContourerProfiler : IPlaneContourerProfiler
{
	private readonly Stopwatch initializePlane = new Stopwatch();

	private readonly Stopwatch markEdges = new Stopwatch();

	private readonly Stopwatch traceEdges = new Stopwatch();

	private readonly Stopwatch findHoles = new Stopwatch();

	private readonly Stopwatch sortEdges = new Stopwatch();

	private readonly Stopwatch libTessAddContours = new Stopwatch();

	private readonly Stopwatch libTessTessellate = new Stopwatch();

	private readonly Stopwatch prepareTriangles = new Stopwatch();

	private readonly Stopwatch combineTriangles = new Stopwatch();

	private readonly Stopwatch recordTriangles = new Stopwatch();

	private readonly Stopwatch triangulateSimple = new Stopwatch();

	private readonly Stopwatch triangulateComplex = new Stopwatch();

	private readonly Stopwatch triangulateSpecial = new Stopwatch();

	private readonly Stopwatch findEdgesTotal = new Stopwatch();

	private readonly Stopwatch triangulateTotal = new Stopwatch();

	private readonly Stopwatch contourPlaneTotal = new Stopwatch();

	/// <inheritdoc />
	public bool IsProfilerEnabled => true;

	/// <inheritdoc />
	public bool CheckForTrianglesMissed { get; private set; }

	/// <inheritdoc />
	public TimeSpan InitializePlane => this.initializePlane.Elapsed;

	/// <inheritdoc />
	public TimeSpan MarkEdges => this.markEdges.Elapsed;

	/// <inheritdoc />
	public TimeSpan TraceEdges => this.traceEdges.Elapsed;

	/// <inheritdoc />
	public TimeSpan FindHoles => this.findHoles.Elapsed;

	/// <inheritdoc />
	public TimeSpan SortEdges => this.sortEdges.Elapsed;

	/// <inheritdoc />
	public TimeSpan LibTessAddContours => this.libTessAddContours.Elapsed;

	/// <inheritdoc />
	public TimeSpan LibTessTessellate => this.libTessTessellate.Elapsed;

	/// <inheritdoc />
	public TimeSpan PrepareTriangles => this.prepareTriangles.Elapsed;

	/// <inheritdoc />
	public TimeSpan CombineTriangles => this.combineTriangles.Elapsed;

	/// <inheritdoc />
	public TimeSpan RecordTriangles => this.recordTriangles.Elapsed;

	/// <inheritdoc />
	public TimeSpan TriangulateSimple => this.triangulateSimple.Elapsed;

	/// <inheritdoc />
	public TimeSpan TriangulateComplex => this.triangulateComplex.Elapsed;

	/// <inheritdoc />
	public TimeSpan TriangulateSpecial => this.triangulateSpecial.Elapsed;

	/// <inheritdoc />
	public TimeSpan FindEdgesTotal => this.findEdgesTotal.Elapsed;

	/// <inheritdoc />
	public TimeSpan TriangulateTotal => this.triangulateTotal.Elapsed;

	/// <inheritdoc />
	public TimeSpan ContourPlaneTotal => this.contourPlaneTotal.Elapsed;

	/// <inheritdoc />
	public int OutputPlanes { get; private set; }

	/// <inheritdoc />
	public int OutputSurfaces { get; private set; }

	/// <inheritdoc />
	public int OutputTriangles { get; private set; }

	/// <inheritdoc />
	public int TrianglesCombined { get; private set; }

	/// <inheritdoc />
	public int TrianglesMissed { get; private set; }

	/// <inheritdoc />
	public int PolygonsSimple { get; private set; }

	/// <inheritdoc />
	public int PolygonsComplex { get; private set; }

	/// <inheritdoc />
	public int PolygonsSpecial { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledMeshes { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledVertices { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledFaces { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledEdges { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledRegions { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledNodes { get; private set; }

	/// <inheritdoc />
	public int LibTessVerticesLength { get; private set; }

	/// <inheritdoc />
	public int LibTessElementsLength { get; private set; }

	/// <inheritdoc />
	public int TriangleCombinerLength { get; private set; }

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartInitializePlane() => this.initializePlane.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopInitializePlane() => this.initializePlane.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartMarkEdges() => this.markEdges.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopMarkEdges() => this.markEdges.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartTraceEdges() => this.traceEdges.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopTraceEdges() => this.traceEdges.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartFindHoles() => this.findHoles.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopFindHoles() => this.findHoles.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartSortEdges() => this.sortEdges.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopSortEdges() => this.sortEdges.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartLibTessAddContours() => this.libTessAddContours.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopLibTessAddContours() => this.libTessAddContours.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartLibTessTessellate() => this.libTessTessellate.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopLibTessTessellate() => this.libTessTessellate.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartPrepareTriangles() => this.prepareTriangles.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopPrepareTriangles() => this.prepareTriangles.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartCombineTriangles() => this.combineTriangles.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopCombineTriangles() => this.combineTriangles.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartRecordTriangles() => this.recordTriangles.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopRecordTriangles() => this.recordTriangles.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartTriangulateSimple() => this.triangulateSimple.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopTriangulateSimple() => this.triangulateSimple.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartTriangulateComplex() => this.triangulateComplex.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopTriangulateComplex() => this.triangulateComplex.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartTriangulateSpecial() => this.triangulateSpecial.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopTriangulateSpecial() => this.triangulateSpecial.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartFindEdgesTotal() => this.findEdgesTotal.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopFindEdgesTotal() => this.findEdgesTotal.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartTriangulateTotal() => this.triangulateTotal.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopTriangulateTotal() => this.triangulateTotal.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartContourPlaneTotal() => this.contourPlaneTotal.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopContourPlaneTotal() => this.contourPlaneTotal.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToOutputPlanes(int count) => this.OutputPlanes += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToOutputSurfaces(int count) => this.OutputSurfaces += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToOutputTriangles(int count) => this.OutputTriangles += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToTrianglesCombined(int count) => this.TrianglesCombined += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToTrianglesMissed(int count) => this.TrianglesMissed += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToPolygonsSimple(int count) => this.PolygonsSimple += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToPolygonsComplex(int count) => this.PolygonsComplex += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToPolygonsSpecial(int count) => this.PolygonsSpecial += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessPooledMeshes(int count) => this.LibTessPooledMeshes = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessPooledVertices(int count) => this.LibTessPooledVertices = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessPooledFaces(int count) => this.LibTessPooledFaces = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessPooledEdges(int count) => this.LibTessPooledEdges = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessPooledRegions(int count) => this.LibTessPooledRegions = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessPooledNodes(int count) => this.LibTessPooledNodes = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessVerticesLength(int count) => this.LibTessVerticesLength = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetLibTessElementsLength(int count) => this.LibTessElementsLength = count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void SetTriangleCombinerLength(int count) => this.TriangleCombinerLength = count;

	/// <inheritdoc />
	public void Reset()
	{
#if VUE_PROFILE
		this.AssertNotRunning();

		this.initializePlane.Reset();
		this.markEdges.Reset();
		this.traceEdges.Reset();
		this.findHoles.Reset();
		this.sortEdges.Reset();
		this.libTessAddContours.Reset();
		this.libTessTessellate.Reset();
		this.prepareTriangles.Reset();
		this.combineTriangles.Reset();
		this.recordTriangles.Reset();
		this.triangulateSimple.Reset();
		this.triangulateComplex.Reset();
		this.triangulateSpecial.Reset();
		this.findEdgesTotal.Reset();
		this.triangulateTotal.Reset();
		this.contourPlaneTotal.Reset();
		this.OutputPlanes = 0;
		this.OutputSurfaces = 0;
		this.OutputTriangles = 0;
		this.TrianglesCombined = 0;
		this.TrianglesMissed = 0;
		this.PolygonsSimple = 0;
		this.PolygonsComplex = 0;
		this.PolygonsSpecial = 0;
		this.LibTessPooledMeshes = 0;
		this.LibTessPooledVertices = 0;
		this.LibTessPooledFaces = 0;
		this.LibTessPooledEdges = 0;
		this.LibTessPooledRegions = 0;
		this.LibTessPooledNodes = 0;
		this.LibTessVerticesLength = 0;
		this.LibTessElementsLength = 0;
		this.TriangleCombinerLength = 0;
#endif
	}

	[Conditional(VUECompilationSymbol.Profile)]
	public void Initialize(bool checkForTrianglesMissed)
	{
		this.CheckForTrianglesMissed = checkForTrianglesMissed;
	}

	[Conditional(VUECompilationSymbol.Profile)]
	public void AssertNotRunning()
	{
		Debug.Assert(!this.initializePlane.IsRunning);
		Debug.Assert(!this.markEdges.IsRunning);
		Debug.Assert(!this.traceEdges.IsRunning);
		Debug.Assert(!this.findHoles.IsRunning);
		Debug.Assert(!this.sortEdges.IsRunning);
		Debug.Assert(!this.libTessAddContours.IsRunning);
		Debug.Assert(!this.libTessTessellate.IsRunning);
		Debug.Assert(!this.prepareTriangles.IsRunning);
		Debug.Assert(!this.combineTriangles.IsRunning);
		Debug.Assert(!this.recordTriangles.IsRunning);
		Debug.Assert(!this.triangulateSimple.IsRunning);
		Debug.Assert(!this.triangulateComplex.IsRunning);
		Debug.Assert(!this.triangulateSpecial.IsRunning);
		Debug.Assert(!this.findEdgesTotal.IsRunning);
		Debug.Assert(!this.triangulateTotal.IsRunning);
		Debug.Assert(!this.contourPlaneTotal.IsRunning);
	}
}
