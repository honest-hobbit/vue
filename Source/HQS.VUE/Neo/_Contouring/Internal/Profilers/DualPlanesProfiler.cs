﻿namespace HQS.VUE.Neo;

internal class DualPlanesProfiler
{
	private readonly Stopwatch indexPlane = new Stopwatch();

	private readonly Stopwatch fillSurfaces = new Stopwatch();

	private readonly Stopwatch processPlanesTotal = new Stopwatch();

	public TimeSpan IndexPlane => this.indexPlane.Elapsed;

	public TimeSpan FillSurfaces => this.fillSurfaces.Elapsed;

	public TimeSpan ProcessPlanesTotal => this.processPlanesTotal.Elapsed;

	public int PlanesProcessed { get; private set; }

	public int PlanesSkipped { get; private set; }

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartIndexPlane() => this.indexPlane.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopIndexPlane() => this.indexPlane.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartFillSurfaces() => this.fillSurfaces.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopFillSurfaces() => this.fillSurfaces.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartProcessPlanesTotal() => this.processPlanesTotal.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopProcessPlanesTotal() => this.processPlanesTotal.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToPlanesProcessed(int count) => this.PlanesProcessed += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToPlanesSkipped(int count) => this.PlanesSkipped += count;

	[Conditional(VUECompilationSymbol.Profile)]
	public void Reset()
	{
		this.AssertNotRunning();

		this.indexPlane.Reset();
		this.fillSurfaces.Reset();
		this.processPlanesTotal.Reset();
		this.PlanesProcessed = 0;
		this.PlanesSkipped = 0;
	}

	[Conditional(VUECompilationSymbol.Profile)]
	public void AssertNotRunning()
	{
		Debug.Assert(!this.indexPlane.IsRunning);
		Debug.Assert(!this.fillSurfaces.IsRunning);
		Debug.Assert(!this.processPlanesTotal.IsRunning);
	}
}
