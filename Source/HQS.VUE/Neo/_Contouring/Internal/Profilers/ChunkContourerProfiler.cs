﻿namespace HQS.VUE.Neo;

internal class ChunkContourerProfiler : IChunkContourerProfiler
{
	private readonly Stopwatch initializeChunk = new Stopwatch();

	private readonly Stopwatch sortOutput = new Stopwatch();

	private readonly Stopwatch mergeOutput = new Stopwatch();

	private readonly Stopwatch finalizeOutput = new Stopwatch();

	private readonly Stopwatch serializedTotal = new Stopwatch();

	private readonly Stopwatch parallelizedTotal = new Stopwatch();

	private readonly Stopwatch contourChunkTotal = new Stopwatch();

	/// <inheritdoc />
	public bool IsProfilerEnabled => true;

	/// <inheritdoc />
	public int MaxDegreeOfParallelism { get; private set; }

	/// <inheritdoc />
	public bool CheckForTrianglesMissed { get; private set; }

	/// <inheritdoc />
	public bool SortAndCompactOutput { get; private set; }

	/// <inheritdoc />
	public TimeSpan InitializePlane { get; private set; }

	/// <inheritdoc />
	public TimeSpan MarkEdges { get; private set; }

	/// <inheritdoc />
	public TimeSpan TraceEdges { get; private set; }

	/// <inheritdoc />
	public TimeSpan FindHoles { get; private set; }

	/// <inheritdoc />
	public TimeSpan SortEdges { get; private set; }

	/// <inheritdoc />
	public TimeSpan LibTessAddContours { get; private set; }

	/// <inheritdoc />
	public TimeSpan LibTessTessellate { get; private set; }

	/// <inheritdoc />
	public TimeSpan PrepareTriangles { get; private set; }

	/// <inheritdoc />
	public TimeSpan CombineTriangles { get; private set; }

	/// <inheritdoc />
	public TimeSpan RecordTriangles { get; private set; }

	/// <inheritdoc />
	public TimeSpan TriangulateSimple { get; private set; }

	/// <inheritdoc />
	public TimeSpan TriangulateComplex { get; private set; }

	/// <inheritdoc />
	public TimeSpan TriangulateSpecial { get; private set; }

	/// <inheritdoc />
	public TimeSpan FindEdgesTotal { get; private set; }

	/// <inheritdoc />
	public TimeSpan TriangulateTotal { get; private set; }

	/// <inheritdoc />
	public TimeSpan ContourPlaneTotal { get; private set; }

	/// <inheritdoc />
	public TimeSpan InitializeChunk => this.initializeChunk.Elapsed;

	/// <inheritdoc />
	public TimeSpan IndexPlane { get; private set; }

	/// <inheritdoc />
	public TimeSpan FillSurfaces { get; private set; }

	/// <inheritdoc />
	public TimeSpan ProcessPlanesTotal { get; private set; }

	/// <inheritdoc />
	public TimeSpan SortOutput => this.sortOutput.Elapsed;

	/// <inheritdoc />
	public TimeSpan MergeOutput => this.mergeOutput.Elapsed;

	/// <inheritdoc />
	public TimeSpan FinalizeOutput => this.finalizeOutput.Elapsed;

	/// <inheritdoc />
	public TimeSpan SerializedTotal => this.serializedTotal.Elapsed;

	/// <inheritdoc />
	public TimeSpan ParallelizedTotal => this.parallelizedTotal.Elapsed;

	/// <inheritdoc />
	public TimeSpan ContourChunkTotal => this.contourChunkTotal.Elapsed;

	/// <inheritdoc />
	public int OutputPlanes { get; private set; }

	/// <inheritdoc />
	public int OutputSurfaces { get; private set; }

	/// <inheritdoc />
	public int OutputTriangles { get; private set; }

	/// <inheritdoc />
	public int TrianglesCombined { get; private set; }

	/// <inheritdoc />
	public int TrianglesMissed { get; private set; }

	/// <inheritdoc />
	public int PolygonsSimple { get; private set; }

	/// <inheritdoc />
	public int PolygonsComplex { get; private set; }

	/// <inheritdoc />
	public int PolygonsSpecial { get; private set; }

	/// <inheritdoc />
	public int PlanesProcessed { get; private set; }

	/// <inheritdoc />
	public int PlanesSkipped { get; private set; }

	/// <inheritdoc />
	public int OutputChunks { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledMeshes { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledVertices { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledFaces { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledEdges { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledRegions { get; private set; }

	/// <inheritdoc />
	public int LibTessPooledNodes { get; private set; }

	/// <inheritdoc />
	public int LibTessVerticesLength { get; private set; }

	/// <inheritdoc />
	public int LibTessElementsLength { get; private set; }

	/// <inheritdoc />
	public int TriangleCombinerLength { get; private set; }

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartInitializeChunk() => this.initializeChunk.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopInitializeChunk() => this.initializeChunk.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartSortOutput() => this.sortOutput.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopSortOutput() => this.sortOutput.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartMergeOutput() => this.mergeOutput.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopMergeOutput() => this.mergeOutput.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartFinalizeOutput() => this.finalizeOutput.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopFinalizeOutput() => this.finalizeOutput.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartSerializedTotal() => this.serializedTotal.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopSerializedTotal() => this.serializedTotal.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartParallelizedTotal() => this.parallelizedTotal.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopParallelizedTotal() => this.parallelizedTotal.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StartContourChunkTotal() => this.contourChunkTotal.Start();

	[Conditional(VUECompilationSymbol.Profile)]
	public void StopContourChunkTotal() => this.contourChunkTotal.Stop();

	[Conditional(VUECompilationSymbol.Profile)]
	public void AddToOutputChunks(int count) => this.OutputChunks += count;

	/// <inheritdoc />
	public void Reset()
	{
#if VUE_PROFILE
		this.AssertNotRunning();

		this.InitializePlane = TimeSpan.Zero;
		this.MarkEdges = TimeSpan.Zero;
		this.TraceEdges = TimeSpan.Zero;
		this.FindHoles = TimeSpan.Zero;
		this.SortEdges = TimeSpan.Zero;
		this.LibTessAddContours = TimeSpan.Zero;
		this.LibTessTessellate = TimeSpan.Zero;
		this.PrepareTriangles = TimeSpan.Zero;
		this.CombineTriangles = TimeSpan.Zero;
		this.RecordTriangles = TimeSpan.Zero;
		this.TriangulateSimple = TimeSpan.Zero;
		this.TriangulateComplex = TimeSpan.Zero;
		this.TriangulateSpecial = TimeSpan.Zero;
		this.FindEdgesTotal = TimeSpan.Zero;
		this.TriangulateTotal = TimeSpan.Zero;
		this.ContourPlaneTotal = TimeSpan.Zero;

		this.initializeChunk.Reset();
		this.sortOutput.Reset();
		this.mergeOutput.Reset();
		this.finalizeOutput.Reset();
		this.IndexPlane = TimeSpan.Zero;
		this.FillSurfaces = TimeSpan.Zero;
		this.ProcessPlanesTotal = TimeSpan.Zero;
		this.serializedTotal.Reset();
		this.parallelizedTotal.Reset();
		this.contourChunkTotal.Reset();

		this.OutputPlanes = 0;
		this.OutputSurfaces = 0;
		this.OutputTriangles = 0;
		this.TrianglesCombined = 0;
		this.TrianglesMissed = 0;
		this.PolygonsSimple = 0;
		this.PolygonsComplex = 0;
		this.PolygonsSpecial = 0;

		this.PlanesProcessed = 0;
		this.PlanesSkipped = 0;
		this.OutputChunks = 0;

		this.LibTessPooledMeshes = 0;
		this.LibTessPooledVertices = 0;
		this.LibTessPooledFaces = 0;
		this.LibTessPooledEdges = 0;
		this.LibTessPooledRegions = 0;
		this.LibTessPooledNodes = 0;
		this.LibTessVerticesLength = 0;
		this.LibTessElementsLength = 0;
		this.TriangleCombinerLength = 0;
#endif
	}

	[Conditional(VUECompilationSymbol.Profile)]
	public void Initialize(
		int maxDegreeOfParallelism, bool checkForTrianglesMissed, bool sortAndCompactOutput)
	{
		this.MaxDegreeOfParallelism = maxDegreeOfParallelism;
		this.CheckForTrianglesMissed = checkForTrianglesMissed;
		this.SortAndCompactOutput = sortAndCompactOutput;

		// reset just the object allocation counts, otherwise they will keep increasing
		this.LibTessPooledMeshes = 0;
		this.LibTessPooledVertices = 0;
		this.LibTessPooledFaces = 0;
		this.LibTessPooledEdges = 0;
		this.LibTessPooledRegions = 0;
		this.LibTessPooledNodes = 0;
		this.LibTessVerticesLength = 0;
		this.LibTessElementsLength = 0;
		this.TriangleCombinerLength = 0;
	}

	[Conditional(VUECompilationSymbol.Profile)]
	public void AssertNotRunning()
	{
		Debug.Assert(!this.initializeChunk.IsRunning);
		Debug.Assert(!this.sortOutput.IsRunning);
		Debug.Assert(!this.mergeOutput.IsRunning);
		Debug.Assert(!this.finalizeOutput.IsRunning);
		Debug.Assert(!this.serializedTotal.IsRunning);
		Debug.Assert(!this.parallelizedTotal.IsRunning);
		Debug.Assert(!this.contourChunkTotal.IsRunning);
	}

	[Conditional(VUECompilationSymbol.Profile)]
	public void CopyIn(IPlaneContourerProfiler profiler)
	{
		Debug.Assert(profiler != null);
		if (profiler is PlaneContourerProfiler planeProfiler)
		{
			planeProfiler.AssertNotRunning();
		}

		this.InitializePlane += profiler.InitializePlane;
		this.MarkEdges += profiler.MarkEdges;
		this.TraceEdges += profiler.TraceEdges;
		this.FindHoles += profiler.FindHoles;
		this.SortEdges += profiler.SortEdges;
		this.LibTessAddContours += profiler.LibTessAddContours;
		this.LibTessTessellate += profiler.LibTessTessellate;
		this.PrepareTriangles += profiler.PrepareTriangles;
		this.CombineTriangles += profiler.CombineTriangles;
		this.RecordTriangles += profiler.RecordTriangles;
		this.TriangulateSimple += profiler.TriangulateSimple;
		this.TriangulateComplex += profiler.TriangulateComplex;
		this.TriangulateSpecial += profiler.TriangulateSpecial;
		this.FindEdgesTotal += profiler.FindEdgesTotal;
		this.TriangulateTotal += profiler.TriangulateTotal;
		this.ContourPlaneTotal += profiler.ContourPlaneTotal;
		this.OutputPlanes += profiler.OutputPlanes;
		this.OutputSurfaces += profiler.OutputSurfaces;
		this.OutputTriangles += profiler.OutputTriangles;
		this.TrianglesCombined += profiler.TrianglesCombined;
		this.TrianglesMissed += profiler.TrianglesMissed;
		this.PolygonsSimple += profiler.PolygonsSimple;
		this.PolygonsComplex += profiler.PolygonsComplex;
		this.PolygonsSpecial += profiler.PolygonsSpecial;
		this.LibTessPooledMeshes += profiler.LibTessPooledMeshes;
		this.LibTessPooledVertices += profiler.LibTessPooledVertices;
		this.LibTessPooledFaces += profiler.LibTessPooledFaces;
		this.LibTessPooledEdges += profiler.LibTessPooledEdges;
		this.LibTessPooledRegions += profiler.LibTessPooledRegions;
		this.LibTessPooledNodes += profiler.LibTessPooledNodes;
		this.LibTessVerticesLength += profiler.LibTessVerticesLength;
		this.LibTessElementsLength += profiler.LibTessElementsLength;
		this.TriangleCombinerLength += profiler.TriangleCombinerLength;
	}

	[Conditional(VUECompilationSymbol.Profile)]
	public void CopyIn(DualPlanesProfiler profiler)
	{
		Debug.Assert(profiler != null);
		profiler.AssertNotRunning();

		this.IndexPlane += profiler.IndexPlane;
		this.FillSurfaces += profiler.FillSurfaces;
		this.ProcessPlanesTotal += profiler.ProcessPlanesTotal;
		this.PlanesProcessed += profiler.PlanesProcessed;
		this.PlanesSkipped += profiler.PlanesSkipped;
	}
}
