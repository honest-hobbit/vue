﻿namespace HQS.VUE.Neo;

// everything here is measured in number of voxels
public interface IContouringBounds<T>
{
	ChunkSize Size { get; }

	T MaxDimensions { get; }

	T Dimensions { get; }

	T Lower { get; }

	T Upper { get; }

	bool IsEmpty { get; }

	void SetBounds(T origin, T dimensions);
}
