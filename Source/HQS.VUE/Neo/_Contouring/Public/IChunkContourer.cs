﻿namespace HQS.VUE.Neo;

public interface IChunkContourer<TVoxel, TSurface, TExtractor> : IVisiblyDisposable
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	IChunkContourerProfiler Profiler { get; }

	IContouringBounds<Int3> Bounds { get; }

	TExtractor Extractor { get; set; }

	int MaxDegreeOfParallelism { get; set; }

	bool CheckForTrianglesMissed { get; set; }

	bool SortAndCompactOutput { get; set; }
}
