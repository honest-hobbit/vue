﻿namespace HQS.VUE.Neo;

public static class IContouringBoundsExtensions
{
	public static void ClampToBounds(
		this IContouringBounds<Int2> bounds, ref Int2 origin, ref Int2 dimensions)
	{
		Ensure.That(bounds, nameof(bounds)).IsNotNull();

		ClampOriginAndDimensions(bounds.MaxDimensions.X, ref origin.X, ref dimensions.X);
		ClampOriginAndDimensions(bounds.MaxDimensions.Y, ref origin.Y, ref dimensions.Y);
	}

	public static void ClampToBounds(
		this IContouringBounds<Int3> bounds, ref Int3 origin, ref Int3 dimensions)
	{
		Ensure.That(bounds, nameof(bounds)).IsNotNull();

		ClampOriginAndDimensions(bounds.MaxDimensions.X, ref origin.X, ref dimensions.X);
		ClampOriginAndDimensions(bounds.MaxDimensions.Y, ref origin.Y, ref dimensions.Y);
		ClampOriginAndDimensions(bounds.MaxDimensions.Z, ref origin.Z, ref dimensions.Z);
	}

	private static void ClampOriginAndDimensions(int maxDimensions, ref int origin, ref int dimensions)
	{
		Debug.Assert(maxDimensions > 0);

		dimensions = dimensions.Clamp(0, (maxDimensions - origin).Clamp(0, maxDimensions));
		origin = origin.Clamp(0, maxDimensions - 1);
	}
}
