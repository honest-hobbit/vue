﻿namespace HQS.VUE.Neo;

public interface ICubeChunkContourer<TVoxel, TSurface, TExtractor> : IChunkContourer<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	CoplanarChunk<TSurface> ContourChunk(CubeChunk<TVoxel> chunk);
}
