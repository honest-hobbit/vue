﻿namespace HQS.VUE.Neo;

public interface IPlaneContourerProfiler
{
	bool IsProfilerEnabled { get; }

	bool CheckForTrianglesMissed { get; }

	TimeSpan InitializePlane { get; }

	TimeSpan MarkEdges { get; }

	TimeSpan TraceEdges { get; }

	TimeSpan FindHoles { get; }

	TimeSpan SortEdges { get; }

	TimeSpan LibTessAddContours { get; }

	TimeSpan LibTessTessellate { get; }

	TimeSpan PrepareTriangles { get; }

	TimeSpan CombineTriangles { get; }

	TimeSpan RecordTriangles { get; }

	TimeSpan TriangulateSimple { get; }

	TimeSpan TriangulateComplex { get; }

	TimeSpan TriangulateSpecial { get; }

	TimeSpan FindEdgesTotal { get; }

	TimeSpan TriangulateTotal { get; }

	TimeSpan ContourPlaneTotal { get; }

	int OutputPlanes { get; }

	int OutputSurfaces { get; }

	int OutputTriangles { get; }

	int TrianglesCombined { get; }

	int TrianglesMissed { get; }

	int PolygonsSimple { get; }

	int PolygonsComplex { get; }

	int PolygonsSpecial { get; }

	int LibTessPooledMeshes { get; }

	int LibTessPooledVertices { get; }

	int LibTessPooledFaces { get; }

	int LibTessPooledEdges { get; }

	int LibTessPooledRegions { get; }

	int LibTessPooledNodes { get; }

	int LibTessVerticesLength { get; }

	int LibTessElementsLength { get; }

	int TriangleCombinerLength { get; }

	void Reset();
}
