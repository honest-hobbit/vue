﻿namespace HQS.VUE.Neo;

// TODO maybe override IChunkContourerProfiler Profiler { get; } to create a more specific profiler
// that splits profiling data into 3 groups by plane type; Face, Edge, and Corner
public interface IWedgeChunkContourer<TVoxel, TSurface, TExtractor> : IChunkContourer<TVoxel, TSurface, TExtractor>
	where TVoxel : unmanaged
	where TSurface : unmanaged
	where TExtractor : ISurfaceExtractor<TVoxel, TSurface>
{
	WedgeTable Wedges { get; set; }

	CoplanarChunk<TSurface> ContourChunk(WedgeChunk<TVoxel> chunk);
}
