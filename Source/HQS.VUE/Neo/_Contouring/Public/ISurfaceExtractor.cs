﻿namespace HQS.VUE.Neo;

public interface ISurfaceExtractor<TVoxel, TSurface> : ISurfaceComparer<TSurface>
	where TVoxel : unmanaged
	where TSurface : unmanaged
{
	TVoxel EmptyVoxel { get; }

	void GetSurfaces(TVoxel voxel, ref bool hasSurface, ref TSurface surface);

	void GetSurfaces(
		TVoxel voxelA,
		TVoxel voxelB,
		ref bool hasSurfaceA,
		ref TSurface surfaceA,
		ref bool hasSurfaceB,
		ref TSurface surfaceB);
}
