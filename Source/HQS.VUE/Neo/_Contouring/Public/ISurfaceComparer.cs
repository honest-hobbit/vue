﻿namespace HQS.VUE.Neo;

public interface ISurfaceComparer<TSurface>
	where TSurface : unmanaged
{
	TSurface EmptySurface { get; }

	bool IsEmpty(TSurface surface);

	bool IsSame(TSurface a, TSurface b);

	// see https://docs.microsoft.com/en-us/dotnet/api/system.comparison-1?view=net-6.0
	// returns less than 0 if 'a' is less than 'b'
	// returns 0 if 'a' equals 'b'
	// returns greater than 0 if 'a' is greater than 'b'
	int Compare(TSurface a, TSurface b);
}
