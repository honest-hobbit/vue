﻿namespace HQS.VUE.Neo;

public static class ContourerProfilerExtensions
{
	private const string DisabledMessage = "Detailed profiling is disabled.";

	public static string GetPlaneTotals(this IPlaneContourerProfiler profiler)
	{
		var builder = new StringBuilder();
		profiler.AppendPlaneTotalsTo(builder);
		return builder.ToString();
	}

	public static string GetPlaneAverages(this IPlaneContourerProfiler profiler)
	{
		var builder = new StringBuilder();
		profiler.AppendPlaneAveragesTo(builder);
		return builder.ToString();
	}

	public static string GetChunkTotals(this IChunkContourerProfiler profiler)
	{
		var builder = new StringBuilder();
		profiler.AppendChunkTotalsTo(builder);
		return builder.ToString();
	}

	public static string GetChunkAverages(this IChunkContourerProfiler profiler)
	{
		var builder = new StringBuilder();
		profiler.AppendChunkAveragesTo(builder);
		return builder.ToString();
	}

	public static void AppendPlaneTotalsTo(this IPlaneContourerProfiler profiler, StringBuilder builder)
	{
		Ensure.That(profiler, nameof(profiler)).IsNotNull();
		Ensure.That(builder, nameof(builder)).IsNotNull();

		if (!profiler.IsProfilerEnabled)
		{
			builder.AppendLine(DisabledMessage);
			return;
		}

		AppendLinesToBuilder(profiler, null, builder, 1);
	}

	public static void AppendPlaneAveragesTo(this IPlaneContourerProfiler profiler, StringBuilder builder)
	{
		Ensure.That(profiler, nameof(profiler)).IsNotNull();
		Ensure.That(builder, nameof(builder)).IsNotNull();

		if (!profiler.IsProfilerEnabled)
		{
			builder.AppendLine(DisabledMessage);
			return;
		}

		AppendLinesToBuilder(profiler, null, builder, profiler.OutputPlanes.ClampLower(1));
	}

	public static void AppendChunkTotalsTo(this IChunkContourerProfiler profiler, StringBuilder builder)
	{
		Ensure.That(profiler, nameof(profiler)).IsNotNull();
		Ensure.That(builder, nameof(builder)).IsNotNull();

		if (!profiler.IsProfilerEnabled)
		{
			builder.AppendLine(DisabledMessage);
			return;
		}

		AppendLinesToBuilder(profiler, profiler, builder, 1);
	}

	public static void AppendChunkAveragesTo(this IChunkContourerProfiler profiler, StringBuilder builder)
	{
		Ensure.That(profiler, nameof(profiler)).IsNotNull();
		Ensure.That(builder, nameof(builder)).IsNotNull();

		if (!profiler.IsProfilerEnabled)
		{
			builder.AppendLine(DisabledMessage);
			return;
		}

		AppendLinesToBuilder(profiler, profiler, builder, profiler.OutputChunks.ClampLower(1));
	}

	internal static void AppendLineHeader(this StringBuilder builder, string name, string column)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		name = $" - {name} - ";
		if (!column.IsNullOrWhiteSpace())
		{
			column = $" - {column} - ";
		}

		builder.AppendLine($"{name,-44}{column,24}");
	}

	internal static void AppendLineHeader(this StringBuilder builder, string name)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		builder.AppendLine($" - {name} - ");
	}

	// TODO update to use CallerArgumentExpression when switching over to .Net 5 or 6
	internal static void AppendLineSkipped(this StringBuilder builder, string name)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		builder.AppendLine($"{name,-24}:{"Skipped",12}");
	}

	// TODO update to use CallerArgumentExpression when switching over to .Net 5 or 6
	// divisor of 0 can be safely ignored, only divisors greater than 1 are used
	internal static void AppendLineTimeSpanWithPerColumn(
		this StringBuilder builder, TimeSpan totalTime, int divisor, TimeSpan? time, string name)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		if (time.HasValue)
		{
			double value = time.Value.TotalMilliseconds;
			string perColumn = divisor > 1 ? $"{value / divisor,12:N3} ms" : null;
			builder.AppendLine($"{name,-24}:{value,12:N3} ms  ({time / totalTime,8:P}){perColumn}");
		}
		else
		{
			builder.AppendLineSkipped(name);
		}
	}

	// TODO update to use CallerArgumentExpression when switching over to .Net 5 or 6
	internal static void AppendLineTimeSpan(
		this StringBuilder builder, TimeSpan totalTime, TimeSpan? time, string name)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		if (time.HasValue)
		{
			builder.AppendLine($"{name,-24}:{time.Value.TotalMilliseconds,12:N3} ms  ({time / totalTime,8:P})");
		}
		else
		{
			builder.AppendLineSkipped(name);
		}
	}

	// TODO update to use CallerArgumentExpression when switching over to .Net 5 or 6
	internal static void AppendLineTimeSpan(
		this StringBuilder builder, TimeSpan? time, string name)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		if (time.HasValue)
		{
			builder.AppendLine($"{name,-24}:{time.Value.TotalMilliseconds,12:N3} ms");
		}
		else
		{
			builder.AppendLineSkipped(name);
		}
	}

	// TODO update to use CallerArgumentExpression when switching over to .Net 5 or 6
	// divisor of 0 can be safely ignored, only divisors greater than 1 are used
	internal static void AppendLineCountWithPerColumn(
		this StringBuilder builder, int divisor, double? count, string name)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		if (count.HasValue)
		{
			string perColumn = divisor > 1 ? $"{count.Value / divisor,27:N0}" : null;
			builder.AppendLine($"{name,-24}:{count.Value,12:N0}{perColumn}");
		}
		else
		{
			builder.AppendLineSkipped(name);
		}
	}

	// TODO update to use CallerArgumentExpression when switching over to .Net 5 or 6
	internal static void AppendLineCount(
		this StringBuilder builder, double? count, string name)
	{
		Debug.Assert(builder != null);
		Debug.Assert(!name.IsNullOrWhiteSpace());

		if (count.HasValue)
		{
			builder.AppendLine($"{name,-24}:{count,12:N0}");
		}
		else
		{
			builder.AppendLineSkipped(name);
		}
	}

	private static void AppendLinesToBuilder(
		IPlaneContourerProfiler planeProfiler,
		IChunkContourerProfiler chunkProfiler,
		StringBuilder builder,
		double divisor)
	{
		Debug.Assert(planeProfiler != null);
		Debug.Assert(builder != null);
		Debug.Assert(divisor >= 1);

		var pp = planeProfiler;
		var cp = chunkProfiler;
		double d = divisor;
		var pt = (cp?.ProcessPlanesTotal ?? pp.ContourPlaneTotal) / d;
		int t = cp?.MaxDegreeOfParallelism ?? 1;
		string perThread = t > 1 ? "Per Thread" : null;

		if (cp == null)
		{
			builder.AppendLineHeader("Overview");
			builder.AppendLineTimeSpan(pt, pp.ContourPlaneTotal / d, nameof(pp.ContourPlaneTotal));
			builder.AppendLineTimeSpan(pt, pp.FindEdgesTotal / d, nameof(pp.FindEdgesTotal));
			builder.AppendLineTimeSpan(pt, pp.TriangulateTotal / d, nameof(pp.TriangulateTotal));
		}
		else
		{
			var ct = cp.ContourChunkTotal / d;

			builder.AppendLineHeader("Overview");
			builder.AppendLineCount(cp.MaxDegreeOfParallelism, nameof(cp.MaxDegreeOfParallelism));
			builder.AppendLineTimeSpan(ct, cp.ContourChunkTotal / d, nameof(cp.ContourChunkTotal));
			builder.AppendLineTimeSpan(ct, cp.SerializedTotal / d, nameof(cp.SerializedTotal));
			builder.AppendLineTimeSpan(ct, cp.ParallelizedTotal / d, nameof(cp.ParallelizedTotal));

			builder.AppendLineHeader("Serialized Parts");
			builder.AppendLineTimeSpan(ct, cp.InitializeChunk / d, nameof(cp.InitializeChunk));
			builder.AppendLineTimeSpan(ct, cp.SortAndCompactOutput ? cp.SortOutput / d : null, nameof(cp.SortOutput));
			builder.AppendLineTimeSpan(ct, cp.MergeOutput / d, nameof(cp.MergeOutput));
			builder.AppendLineTimeSpan(ct, cp.FinalizeOutput / d, nameof(cp.FinalizeOutput));

			builder.AppendLineHeader("Parallelized Parts", perThread);
			builder.AppendLineTimeSpanWithPerColumn(pt, t, cp.ProcessPlanesTotal / d, nameof(cp.ProcessPlanesTotal));
			builder.AppendLineTimeSpanWithPerColumn(pt, t, cp.IndexPlane / d, nameof(cp.IndexPlane));
			builder.AppendLineTimeSpanWithPerColumn(pt, t, cp.FillSurfaces / d, nameof(cp.FillSurfaces));
			builder.AppendLineTimeSpanWithPerColumn(pt, t, cp.ContourPlaneTotal / d, nameof(cp.ContourPlaneTotal));
		}

		builder.AppendLineHeader("Find Edges", perThread);
		if (cp != null)
		{
			builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.FindEdgesTotal / d, nameof(pp.FindEdgesTotal));
		}

		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.InitializePlane / d, nameof(pp.InitializePlane));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.MarkEdges / d, nameof(pp.MarkEdges));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.TraceEdges / d, nameof(pp.TraceEdges));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.FindHoles / d, nameof(pp.FindHoles));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.SortEdges / d, nameof(pp.SortEdges));

		builder.AppendLineHeader("Triangulate Edges", perThread);
		if (cp != null)
		{
			builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.TriangulateTotal / d, nameof(pp.TriangulateTotal));
		}

		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.LibTessAddContours / d, nameof(pp.LibTessAddContours));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.LibTessTessellate / d, nameof(pp.LibTessTessellate));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.PrepareTriangles / d, nameof(pp.PrepareTriangles));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.CombineTriangles / d, nameof(pp.CombineTriangles));
		builder.AppendLineTimeSpanWithPerColumn(pt, t, pp.RecordTriangles / d, nameof(pp.RecordTriangles));

		builder.AppendLineHeader("Polygons", "Per Polygon");
		builder.AppendLineTimeSpanWithPerColumn(pt, pp.PolygonsSimple, pp.TriangulateSimple / d, nameof(pp.TriangulateSimple));
		builder.AppendLineTimeSpanWithPerColumn(pt, pp.PolygonsComplex, pp.TriangulateComplex / d, nameof(pp.TriangulateComplex));
		builder.AppendLineTimeSpanWithPerColumn(pt, pp.PolygonsSpecial, pp.TriangulateSpecial / d, nameof(pp.TriangulateSpecial));
		builder.AppendLineCount(pp.PolygonsSimple / d, nameof(pp.PolygonsSimple));
		builder.AppendLineCount(pp.PolygonsComplex / d, nameof(pp.PolygonsComplex));
		builder.AppendLineCount(pp.PolygonsSpecial / d, nameof(pp.PolygonsSpecial));

		builder.AppendLineHeader("Statistics");
		if (cp != null)
		{
			builder.AppendLineCount(cp.PlanesProcessed / d, nameof(cp.PlanesProcessed));
			builder.AppendLineCount(cp.PlanesSkipped / d, nameof(cp.PlanesSkipped));
			builder.AppendLineCount(cp.OutputChunks / d, nameof(cp.OutputChunks));
		}

		builder.AppendLineCount(pp.OutputPlanes / d, nameof(pp.OutputPlanes));
		builder.AppendLineCount(pp.OutputSurfaces / d, nameof(pp.OutputSurfaces));
		builder.AppendLineCount(pp.OutputTriangles / d, nameof(pp.OutputTriangles));
		builder.AppendLineCount(pp.TrianglesCombined / d, nameof(pp.TrianglesCombined));
		builder.AppendLineCount(pp.CheckForTrianglesMissed ? pp.TrianglesMissed / d : null, nameof(pp.TrianglesMissed));

		builder.AppendLineHeader("Object Allocations", perThread);
		builder.AppendLineCountWithPerColumn(t, pp.LibTessPooledMeshes, nameof(pp.LibTessPooledMeshes));
		builder.AppendLineCountWithPerColumn(t, pp.LibTessPooledVertices, nameof(pp.LibTessPooledVertices));
		builder.AppendLineCountWithPerColumn(t, pp.LibTessPooledFaces, nameof(pp.LibTessPooledFaces));
		builder.AppendLineCountWithPerColumn(t, pp.LibTessPooledEdges, nameof(pp.LibTessPooledEdges));
		builder.AppendLineCountWithPerColumn(t, pp.LibTessPooledRegions, nameof(pp.LibTessPooledRegions));
		builder.AppendLineCountWithPerColumn(t, pp.LibTessPooledNodes, nameof(pp.LibTessPooledNodes));
		builder.AppendLineCount(pp.LibTessVerticesLength, nameof(pp.LibTessVerticesLength));
		builder.AppendLineCount(pp.LibTessElementsLength, nameof(pp.LibTessElementsLength));
		builder.AppendLineCount(pp.TriangleCombinerLength, nameof(pp.TriangleCombinerLength));
	}
}
