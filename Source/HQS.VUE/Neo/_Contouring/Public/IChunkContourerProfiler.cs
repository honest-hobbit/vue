﻿namespace HQS.VUE.Neo;

public interface IChunkContourerProfiler : IPlaneContourerProfiler
{
	int MaxDegreeOfParallelism { get; }

	bool SortAndCompactOutput { get; }

	TimeSpan InitializeChunk { get; }

	TimeSpan IndexPlane { get; }

	TimeSpan FillSurfaces { get; }

	TimeSpan ProcessPlanesTotal { get; }

	TimeSpan SortOutput { get; }

	TimeSpan MergeOutput { get; }

	TimeSpan FinalizeOutput { get; }

	TimeSpan SerializedTotal { get; }

	TimeSpan ParallelizedTotal { get; }

	TimeSpan ContourChunkTotal { get; }

	int PlanesProcessed { get; }

	int PlanesSkipped { get; }

	int OutputChunks { get; }
}
