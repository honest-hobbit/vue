﻿namespace HQS.VUE.Neo;

internal static class CoplanarChunkUtility
{
	[Conditional(CompilationSymbol.Debug)]
	public static void AssertPlanesAreSorted<TSurface>(CoplanarChunk<TSurface> chunk)
		where TSurface : unmanaged
	{
		foreach (var pair in chunk.Planes.Where(x => x.Plane.Normal.IsBack()).AsPairs())
		{
			Debug.Assert(pair.Previous.Plane.Index < pair.Next.Plane.Index);
		}

		foreach (var pair in chunk.Planes.Where(x => x.Plane.Normal.IsFront()).AsPairs())
		{
			Debug.Assert(pair.Previous.Plane.Index < pair.Next.Plane.Index);
		}
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AssertPlanesAreSorted<TSurface>(CoplanarChunk<TSurface>.Builder chunk)
		where TSurface : unmanaged
	{
		Debug.Assert(chunk != null);

		foreach (var pair in chunk.Planes.Where(x => x.Plane.Normal.IsBack()).AsPairs())
		{
			Debug.Assert(pair.Previous.Plane.Index < pair.Next.Plane.Index);
		}

		foreach (var pair in chunk.Planes.Where(x => x.Plane.Normal.IsFront()).AsPairs())
		{
			Debug.Assert(pair.Previous.Plane.Index < pair.Next.Plane.Index);
		}
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AssertValid(PlaneGroup group)
	{
		Debug.Assert(group.SurfaceCount > 0);
		Debug.Assert(group.TriangleCount > 0);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AssertValid<TSurface>(SurfaceGroup<TSurface> group)
		where TSurface : unmanaged
	{
		Debug.Assert(group.TriangleCount > 0);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AssertValid(CompactTriangle triangle)
	{
		Debug.Assert(triangle.A != triangle.B);
		Debug.Assert(triangle.A != triangle.C);
		Debug.Assert(triangle.B != triangle.C);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AssertValid<TSurface>(CoplanarChunk<TSurface> chunk)
		where TSurface : unmanaged
	{
		AssertValid(
			chunk.Planes.GetEnumerator(),
			chunk.Surfaces.GetEnumerator(),
			chunk.Triangles.GetEnumerator(),
			chunk.Planes.Count,
			chunk.Surfaces.Count,
			chunk.Triangles.Count);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void AssertValid<TSurface>(CoplanarChunk<TSurface>.Builder chunk)
		where TSurface : unmanaged
	{
		Debug.Assert(chunk != null);

		AssertValid(
			chunk.Planes.GetEnumerator(),
			chunk.Surfaces.GetEnumerator(),
			chunk.Triangles.GetEnumerator(),
			chunk.Planes.Count,
			chunk.Surfaces.Count,
			chunk.Triangles.Count);
	}

	private static void AssertValid<TSurface>(
		PagedValuesEnumerator<PlaneGroup> planes,
		PagedValuesEnumerator<SurfaceGroup<TSurface>> surfaces,
		PagedValuesEnumerator<CompactTriangle> triangles,
		int planesCount,
		int surfacesCount,
		int trianglesCount)
		where TSurface : unmanaged
	{
		Debug.Assert(planesCount >= 0);
		Debug.Assert(surfacesCount >= 0);
		Debug.Assert(trianglesCount >= 0);

		int totalPlanesCount = 0;
		int totalSurfacesCount = 0;
		int totalTrianglesByPlanesCount = 0;
		int totalTrianglesBySurfacesCount = 0;

		while (planes.MoveNext())
		{
			var plane = planes.Current;
			AssertValid(plane);

			totalPlanesCount++;
			totalSurfacesCount += plane.SurfaceCount;
			totalTrianglesByPlanesCount += plane.TriangleCount;
			int totalTrianglesInThisPlaneCount = 0;

			for (int count = 0; count < plane.SurfaceCount; count++)
			{
				bool surfacesMoved = surfaces.MoveNext();
				Debug.Assert(surfacesMoved);

				var surface = surfaces.Current;
				AssertValid(surface);

				totalTrianglesBySurfacesCount += surface.TriangleCount;
				totalTrianglesInThisPlaneCount += surface.TriangleCount;
			}

			Debug.Assert(plane.TriangleCount == totalTrianglesInThisPlaneCount);
		}

		while (triangles.MoveNext())
		{
			var triangle = triangles.Current;
			AssertValid(triangle);
		}

		Debug.Assert(!planes.MoveNext());
		Debug.Assert(!surfaces.MoveNext());
		Debug.Assert(!triangles.MoveNext());

		Debug.Assert(planes.IsEmpty);
		Debug.Assert(surfaces.IsEmpty);
		Debug.Assert(triangles.IsEmpty);

		if (totalPlanesCount != 0)
		{
			Debug.Assert(planesCount == totalPlanesCount);
		}

		if (totalSurfacesCount != 0)
		{
			Debug.Assert(surfacesCount == totalSurfacesCount);
		}

		if (totalTrianglesByPlanesCount != 0)
		{
			Debug.Assert(trianglesCount == totalTrianglesByPlanesCount);
		}

		if (totalTrianglesBySurfacesCount != 0)
		{
			Debug.Assert(trianglesCount == totalTrianglesBySurfacesCount);
		}
	}
}
