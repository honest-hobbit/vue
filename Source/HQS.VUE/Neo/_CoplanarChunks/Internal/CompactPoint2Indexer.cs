﻿namespace HQS.VUE.Neo;

/*
 * This encodes 2D points (X, Y) into 1D indices in a checkered pattern where every other
 * point is skipped. The skipped points are on the center of the edges between voxels and
 * will never be used. Depicted below is the resulting pattern for a VoxelPlaneSizeExponent
 * of 1. Indices 1, 3, 6, 8, 11, and 13 are at the center of their voxels whereas indices
 * 0, 2, 4, 5, 7, 9, 10, 12, and 14 are at the corner intersections between voxels.
 *
 *	X  0  1  2  3  4
 *  Y +---------------
 *  0 |  0  -  2  -  4
 *  1 |  -  1  -  3  -
 *  2 |  5  -  7  -  9
 *  3 |  -  6  -  8  -
 *  4 | 10  - 12  - 14
 *  5 |  - 11  - 13  -
 */
internal readonly record struct CompactPoint2Indexer
{
	private readonly int lengthX;

	public CompactPoint2Indexer(ChunkSize size)
	{
		size.Validate(nameof(size));

		this.lengthX = MathUtility.PowerOf2(size.Exponent + 1) + 1;
	}

	public static int GetLength(ChunkSize size)
	{
		int exponent = size.Exponent;
		int lengthX = MathUtility.PowerOf2(exponent + 1) + 1;
		int lengthY = MathUtility.PowerOf2(exponent) + 1;
		return lengthX * lengthY;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public CompactPoint2 GetPoint(int x, int y)
	{
		// even points are centered on the edges between voxels and are not supported
		Debug.Assert((x + y).IsEven());

		return new CompactPoint2((ushort)(((y >> 1) * this.lengthX) + x));
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void GetPosition(CompactPoint2 point, out int x, out int y)
	{
		y = (Math.DivRem(point.Index, this.lengthX, out x) << 1) + (x & 0b1);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	internal Int2 GetPosition(CompactPoint2 point)
	{
		this.GetPosition(point, out int x, out int y);
		return new Int2(x, y);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	internal CompactPoint2 GetPoint(Point2 point) => this.GetPoint(point.X, point.Y);
}
