﻿namespace HQS.VUE.Neo;

internal struct CoplanarChunkEnumerator<TSurface>
	where TSurface : unmanaged
{
	private PagedValuesEnumerator<PlaneGroup> planes;

	private PagedValuesEnumerator<SurfaceGroup<TSurface>> surfaces;

	private PagedValuesEnumerator<CompactTriangle> triangles;

	public CoplanarChunkEnumerator(CoplanarChunk<TSurface> chunk)
	{
		CoplanarChunkUtility.AssertValid(chunk);

		this.planes = chunk.Planes.GetEnumerator();
		this.surfaces = chunk.Surfaces.GetEnumerator();
		this.triangles = chunk.Triangles.GetEnumerator();
		this.MoveNextPlane();
	}

	public CoplanarChunkEnumerator(CoplanarChunk<TSurface>.Builder chunk)
	{
		Debug.Assert(chunk != null);

		CoplanarChunkUtility.AssertValid(chunk);

		this.planes = chunk.Planes.GetEnumerator();
		this.surfaces = chunk.Surfaces.GetEnumerator();
		this.triangles = chunk.Triangles.GetEnumerator();
		this.MoveNextPlane();
	}

	public bool HasNextPlane { get; private set; } = false;

	public PlaneIndex NextPlane { get; private set; } = default;

	/// <inheritdoc />
	public override string ToString() => this.HasNextPlane ? this.NextPlane.ToString() : "None";

	public void AddAllRemainingPlanesTo(CoplanarChunk<TSurface>.Builder builder)
	{
		Debug.Assert(builder != null);

		while (this.HasNextPlane)
		{
			this.AddNextPlaneTo(builder);
		}
	}

	public void AddNextPlaneTo(CoplanarChunk<TSurface>.Builder builder)
	{
		Debug.Assert(this.HasNextPlane);
		Debug.Assert(builder != null);

		builder.AddPlane(this.planes.Current);
		builder.AddSurfaces(ref this.surfaces, this.planes.Current.SurfaceCount);
		builder.AddTriangles(ref this.triangles, this.planes.Current.TriangleCount);

		this.MoveNextPlane();
	}

	private void MoveNextPlane()
	{
		this.HasNextPlane = this.planes.MoveNext();
		this.NextPlane = this.HasNextPlane ? this.planes.Current.Plane : default;
	}
}
