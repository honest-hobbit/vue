﻿namespace HQS.VUE.Neo;

public static class PlaneNormalExtensions
{
	private static readonly Vector3[] Vectors;

	// Do not modify this field outside of the constructor. Not marked readonly only
	// because BitField32 is mutable and would result in more copying if marked readonly.
	private static Bit32 reverseOutput;

	static PlaneNormalExtensions()
	{
		Vectors = new Vector3[PlaneNormalUtility.CountOfNormals];

		Vectors[(int)PlaneNormal.FaceNegX] = Vector3.Normalize(new Vector3(-1, 0, 0));
		Vectors[(int)PlaneNormal.FacePosX] = Vector3.Normalize(new Vector3(1, 0, 0));
		Vectors[(int)PlaneNormal.FaceNegY] = Vector3.Normalize(new Vector3(0, -1, 0));
		Vectors[(int)PlaneNormal.FacePosY] = Vector3.Normalize(new Vector3(0, 1, 0));
		Vectors[(int)PlaneNormal.FaceNegZ] = Vector3.Normalize(new Vector3(0, 0, -1));
		Vectors[(int)PlaneNormal.FacePosZ] = Vector3.Normalize(new Vector3(0, 0, 1));

		Vectors[(int)PlaneNormal.EdgeXNegYNegZ] = Vector3.Normalize(new Vector3(0, -1, -1));
		Vectors[(int)PlaneNormal.EdgeXPosYPosZ] = Vector3.Normalize(new Vector3(0, 1, 1));
		Vectors[(int)PlaneNormal.EdgeXNegYPosZ] = Vector3.Normalize(new Vector3(0, -1, 1));
		Vectors[(int)PlaneNormal.EdgeXPosYNegZ] = Vector3.Normalize(new Vector3(0, 1, -1));

		Vectors[(int)PlaneNormal.EdgeYNegXNegZ] = Vector3.Normalize(new Vector3(-1, 0, -1));
		Vectors[(int)PlaneNormal.EdgeYPosXPosZ] = Vector3.Normalize(new Vector3(1, 0, 1));
		Vectors[(int)PlaneNormal.EdgeYNegXPosZ] = Vector3.Normalize(new Vector3(-1, 0, 1));
		Vectors[(int)PlaneNormal.EdgeYPosXNegZ] = Vector3.Normalize(new Vector3(1, 0, -1));

		Vectors[(int)PlaneNormal.EdgeZNegXNegY] = Vector3.Normalize(new Vector3(-1, -1, 0));
		Vectors[(int)PlaneNormal.EdgeZPosXPosY] = Vector3.Normalize(new Vector3(1, 1, 0));
		Vectors[(int)PlaneNormal.EdgeZNegXPosY] = Vector3.Normalize(new Vector3(-1, 1, 0));
		Vectors[(int)PlaneNormal.EdgeZPosXNegY] = Vector3.Normalize(new Vector3(1, -1, 0));

		Vectors[(int)PlaneNormal.CornerNegXNegYNegZ] = Vector3.Normalize(new Vector3(-1, -1, -1));
		Vectors[(int)PlaneNormal.CornerPosXPosYPosZ] = Vector3.Normalize(new Vector3(1, 1, 1));
		Vectors[(int)PlaneNormal.CornerNegXNegYPosZ] = Vector3.Normalize(new Vector3(-1, -1, 1));
		Vectors[(int)PlaneNormal.CornerPosXPosYNegZ] = Vector3.Normalize(new Vector3(1, 1, -1));

		Vectors[(int)PlaneNormal.CornerNegXPosYNegZ] = Vector3.Normalize(new Vector3(-1, 1, -1));
		Vectors[(int)PlaneNormal.CornerPosXNegYPosZ] = Vector3.Normalize(new Vector3(1, -1, 1));
		Vectors[(int)PlaneNormal.CornerNegXPosYPosZ] = Vector3.Normalize(new Vector3(-1, 1, 1));
		Vectors[(int)PlaneNormal.CornerPosXNegYNegZ] = Vector3.Normalize(new Vector3(1, -1, -1));

		// faces
		reverseOutput[(int)PlaneNormal.FaceNegX] = true;
		reverseOutput[(int)PlaneNormal.FaceNegY] = true;
		reverseOutput[(int)PlaneNormal.FacePosZ] = true;

		// edges
		reverseOutput[(int)PlaneNormal.EdgeXPosYPosZ] = true;
		reverseOutput[(int)PlaneNormal.EdgeXNegYPosZ] = true;
		reverseOutput[(int)PlaneNormal.EdgeYNegXNegZ] = true;
		reverseOutput[(int)PlaneNormal.EdgeYNegXPosZ] = true;
		reverseOutput[(int)PlaneNormal.EdgeZNegXNegY] = true;
		reverseOutput[(int)PlaneNormal.EdgeZNegXPosY] = true;

		// corners
		reverseOutput[(int)PlaneNormal.CornerNegXNegYNegZ] = true;
		reverseOutput[(int)PlaneNormal.CornerNegXNegYPosZ] = true;
		reverseOutput[(int)PlaneNormal.CornerPosXNegYPosZ] = true;
		reverseOutput[(int)PlaneNormal.CornerPosXNegYNegZ] = true;

		// TODO delete this if not switching PlaneTo3DTransformer corners back
		////reverseOutput[(int)PlaneNormal.CornerNegXPosYNegZ] = true;
		////reverseOutput[(int)PlaneNormal.CornerNegXPosYPosZ] = true;
	}

	public static bool IsDefined(this PlaneNormal normal) =>
		normal >= PlaneNormal.FaceNegX && normal <= PlaneNormal.CornerNegXPosYPosZ;

	public static void ValidateIsDefined(this PlaneNormal normal) => normal.ValidateIsDefined(nameof(normal));

	public static void ValidateIsDefined(this PlaneNormal normal, string name)
	{
		if (!normal.IsDefined())
		{
			throw InvalidEnumArgument.CreateException(name, normal);
		}
	}

	public static Vector3 ToVector(this PlaneNormal normal) => Vectors[(int)normal];

	// toggling the first bit using XOR switches between pairs of PlaneNormals
	public static PlaneNormal Negate(this PlaneNormal normal) => (PlaneNormal)((int)normal ^ 1);

	public static bool GetReverseOutput(this PlaneNormal normal) => reverseOutput[(int)normal];

	public static bool IsFace(this PlaneNormal normal) =>
		normal >= PlaneNormal.FaceNegX && normal <= PlaneNormal.FacePosZ;

	public static bool IsEdge(this PlaneNormal normal) =>
		normal >= PlaneNormal.EdgeXNegYNegZ && normal <= PlaneNormal.EdgeZPosXNegY;

	public static bool IsCorner(this PlaneNormal normal) =>
		normal >= PlaneNormal.CornerNegXNegYNegZ && normal <= PlaneNormal.CornerNegXPosYPosZ;

	public static bool IsBack(this PlaneNormal normal) => ((int)normal).IsEven();

	public static bool IsFront(this PlaneNormal normal) => ((int)normal).IsOdd();
}
