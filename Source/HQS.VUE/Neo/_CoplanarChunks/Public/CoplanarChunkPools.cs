﻿namespace HQS.VUE.Neo;

public class CoplanarChunkPools<TSurface>
	where TSurface : unmanaged
{
	private const bool DontClearPages = false;

	private readonly IPool<Page<PlaneGroup>> planePages;

	private readonly IPool<Page<SurfaceGroup<TSurface>>> surfacePages;

	private readonly IPool<Page<CompactTriangle>> trianglePages;

	public CoplanarChunkPools(
		IPool<CoplanarChunk<TSurface>.Builder> builders,
		IPool<Page<PlaneGroup>> planePages,
		IPool<Page<SurfaceGroup<TSurface>>> surfacePages,
		IPool<Page<CompactTriangle>> trianglePages)
	{
		Debug.Assert(builders != null);
		Debug.Assert(planePages != null);
		Debug.Assert(surfacePages != null);
		Debug.Assert(trianglePages != null);

		this.Builders = builders;
		this.planePages = planePages;
		this.surfacePages = surfacePages;
		this.trianglePages = trianglePages;
	}

	public IPool<CoplanarChunk<TSurface>.Builder> Builders { get; }

	public IPoolControls PlanePages => this.planePages;

	public IPoolControls SurfacePages => this.surfacePages;

	public IPoolControls TrianglePages => this.trianglePages;

	public void ReturnChunk(CoplanarChunk<TSurface> chunk)
	{
		chunk.Planes.ReturnToPool(this.planePages, DontClearPages);
		chunk.Surfaces.ReturnToPool(this.surfacePages, DontClearPages);
		chunk.Triangles.ReturnToPool(this.trianglePages, DontClearPages);
	}
}
