﻿namespace HQS.VUE.Neo;

public readonly record struct PlaneIndex
{
	private const int DepthMask = 0b_00000111_11111111;

	private const int NormalShift = 11;

	public const int MinDepth = -1024;

	public const int MaxDepth = 1023;

	public readonly ushort Index;

	public PlaneIndex(ushort index)
	{
		this.Index = index;
	}

	public PlaneNormal Normal => (PlaneNormal)(this.Index >> NormalShift);

	public int Depth => (this.Index & DepthMask) + MinDepth;

	public static PlaneIndex From(PlaneNormal normal, int depth)
	{
		normal.ValidateIsDefined();
		ValidateDepth(depth);

		return new PlaneIndex((ushort)(((int)normal << NormalShift) | (depth - MinDepth)));
	}

	public static bool IsValidDepth(int depth) => depth >= MinDepth && depth <= MaxDepth;

	public static void ValidateDepth(int depth) => ValidateDepth(depth, nameof(depth));

	public static void ValidateDepth(int depth, string name) =>
		Ensure.That(depth, name).IsInRange(MinDepth, MaxDepth);

	/// <inheritdoc />
	public override string ToString() => this.Index.ToString();
}
