﻿namespace HQS.VUE.Neo;

// the ordering of this enum is extremely important, do no change it
// there is deliberately no ZZZ
public enum PlaneNormal : byte
{
	// NZZ,
	FaceNegX = 0,

	// PZZ,
	FacePosX,

	// ZNZ,
	FaceNegY,

	// ZPZ,
	FacePosY,

	// ZZN,
	FaceNegZ,

	// ZZP,
	FacePosZ,

	// ZNN,
	EdgeXNegYNegZ,

	// ZPP,
	EdgeXPosYPosZ,

	// ZNP,
	EdgeXNegYPosZ,

	// ZPN,
	EdgeXPosYNegZ,

	// NZN,
	EdgeYNegXNegZ,

	// PZP,
	EdgeYPosXPosZ,

	// NZP,
	EdgeYNegXPosZ,

	// PZN,
	EdgeYPosXNegZ,

	// NNZ,
	EdgeZNegXNegY,

	// PPZ,
	EdgeZPosXPosY,

	// NPZ,
	EdgeZNegXPosY,

	// PNZ,
	EdgeZPosXNegY,

	// NNN,
	CornerNegXNegYNegZ,

	// PPP,
	CornerPosXPosYPosZ,

	// NNP,
	CornerNegXNegYPosZ,

	// PPN,
	CornerPosXPosYNegZ,

	// PNP,
	CornerPosXNegYPosZ,

	// NPN,
	CornerNegXPosYNegZ,

	// PNN,
	CornerPosXNegYNegZ,

	// NPP,
	CornerNegXPosYPosZ,
}
