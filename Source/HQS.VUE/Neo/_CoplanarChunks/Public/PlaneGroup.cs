﻿namespace HQS.VUE.Neo;

public readonly record struct PlaneGroup
{
	public readonly PlaneIndex Plane;

	public readonly int SurfaceCount;

	public readonly int TriangleCount;

	public PlaneGroup(PlaneIndex plane, int surfaceCount, int triangleCount)
	{
		this.Plane = plane;
		this.SurfaceCount = surfaceCount;
		this.TriangleCount = triangleCount;
	}
}
