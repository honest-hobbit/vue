﻿namespace HQS.VUE.Neo;

// TODO should this be internal?
public class PlaneTo3DTransformer
{
	private static readonly Func<PlaneTo3DTransformer, int, int, Int3>[] Transforms;

	private Func<PlaneTo3DTransformer, int, int, Int3> transform;

	private ChunkSize size;

	private PlaneNormal normal;

	private int doubleDepth;

	private int length;

	private int faceOffset;

	private int edgeOffset;

	// TODO delete this if not switching PlaneTo3DTransformer corners back
	////private int cornerOffset1;

	////private int cornerOffsetAlt1;

	////private int cornerOffset2;

	////private int cornerOffsetAlt2;

	private int diamondOffset1;

	private int diamondOffset2;

	private int diamondOffset3;

	static PlaneTo3DTransformer()
	{
		Transforms = new Func<PlaneTo3DTransformer, int, int, Int3>[PlaneNormalUtility.CountOfNormals];

		SetTransform(PlaneNormal.FaceNegX, (self, a, b) => new Int3(self.faceOffset, b, a));
		SetTransform(PlaneNormal.FaceNegY, (self, a, b) => new Int3(a, self.faceOffset, b));
		SetTransform(PlaneNormal.FaceNegZ, (self, a, b) => new Int3(a, b, self.faceOffset));

		SetTransform(PlaneNormal.EdgeXNegYNegZ, (self, a, b) => new Int3(a, b, self.edgeOffset - b));
		SetTransform(PlaneNormal.EdgeXNegYPosZ, (self, a, b) => new Int3(a, b, self.doubleDepth + b));

		SetTransform(PlaneNormal.EdgeYNegXNegZ, (self, a, b) => new Int3(self.edgeOffset - a, b, a));
		SetTransform(PlaneNormal.EdgeYNegXPosZ, (self, a, b) => new Int3(self.doubleDepth + a, b, a));

		SetTransform(PlaneNormal.EdgeZNegXNegY, (self, a, b) => new Int3(self.edgeOffset - b, b, a));
		SetTransform(PlaneNormal.EdgeZNegXPosY, (self, a, b) => new Int3(self.doubleDepth + b, b, a));

		// TODO delete this if not switching PlaneTo3DTransformer corners back
		// and update PlaneNormalExtensions reverseOutput
		/*
		SetTransform(PlaneNormal.CornerNegXNegYNegZ, (self, a, b) =>
		{
			int c = b >> 1;
			return new Int3(self.cornerOffsetAlt1 - a - c, b, self.cornerOffset1 + a - c);
		});

		SetTransform(PlaneNormal.CornerNegXNegYPosZ, (self, a, b) =>
		{
			int c = b >> 1;
			return new Int3(self.cornerOffset1 + a - c, b, -self.cornerOffset1 + a + c);
		});

		SetTransform(PlaneNormal.CornerPosXNegYPosZ, (self, a, b) =>
		{
			int c = b >> 1;
			return new Int3(-self.cornerOffsetAlt2 - a + c, b, -self.cornerOffset2 + a + c);
		});

		SetTransform(PlaneNormal.CornerPosXNegYNegZ, (self, a, b) =>
		{
			int c = b >> 1;
			return new Int3(-self.cornerOffset2 + a + c, b, self.cornerOffset2 + a - c);
		});
		*/

		SetTransform(PlaneNormal.CornerNegXNegYNegZ, (self, a, b) => new Int3(a, self.diamondOffset2 - a - b, b));
		SetTransform(PlaneNormal.CornerNegXNegYPosZ, (self, a, b) => new Int3(a, self.diamondOffset1 - a + b, b));
		SetTransform(PlaneNormal.CornerPosXNegYPosZ, (self, a, b) => new Int3(a, a + b - self.diamondOffset3, b));
		SetTransform(PlaneNormal.CornerPosXNegYNegZ, (self, a, b) => new Int3(a, self.diamondOffset1 + a - b, b));

		void SetTransform(PlaneNormal normal, Func<PlaneTo3DTransformer, int, int, Int3> transform)
		{
			Debug.Assert(transform != null);

			Transforms[(int)normal] = transform;
			Transforms[(int)normal.Negate()] = transform;
		}
	}

	public PlaneTo3DTransformer()
	{
		this.Size = ChunkSize.Min;
		this.Normal = PlaneNormal.CornerNegXNegYNegZ;
	}

	public ChunkSize Size
	{
		get => this.size;
		set
		{
			value.Validate(nameof(this.Size));

			this.size = value;
			this.length = this.size.SideLength;

			this.CalculateDerived();
		}
	}

	public PlaneNormal Normal
	{
		get => this.normal;
		set
		{
			value.ValidateIsDefined(nameof(this.Normal));

			this.normal = value;
			this.transform = Transforms[(int)value];
		}
	}

	public int Depth
	{
		get => this.doubleDepth >> 1;
		set
		{
			// depth value is doubled because each voxel is 2 vertices wide
			this.doubleDepth = value << 1;

			this.CalculateDerived();
		}
	}

	public PlaneIndex Plane
	{
		get => PlaneIndex.From(this.Normal, this.Depth);
		set
		{
			this.Normal = value.Normal;
			this.Depth = value.Depth;
		}
	}

	public Int3 Transform(int a, int b) => this.transform(this, a, b);

	private void CalculateDerived()
	{
		// ChunkSize.SideLength is measured in voxels, but each voxel is 2 vertices wide
		// that is why SideLength must be doubled
		int doubleLength = this.length << 1;

		this.faceOffset = this.length + this.doubleDepth;
		this.edgeOffset = doubleLength + this.doubleDepth;

		// TODO delete this if not switching PlaneTo3DTransformer corners back
		////this.cornerOffset1 = (this.doubleDepth >> 1) + (this.length >> 1);
		////this.cornerOffsetAlt1 = this.cornerOffset1 + doubleLength;
		////this.cornerOffset2 = -(this.doubleDepth >> 1) + (this.length >> 1);
		////this.cornerOffsetAlt2 = this.cornerOffset2 - doubleLength;

		this.diamondOffset1 = this.length + this.doubleDepth;
		this.diamondOffset2 = this.diamondOffset1 + doubleLength;
		this.diamondOffset3 = this.length - this.doubleDepth;
	}
}
