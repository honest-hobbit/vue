﻿namespace HQS.VUE.Neo;

// TODO should this be internal?
// this class maps CompactPoint2 to 1D vertex indices,
// allowing vertices to be reused in the final mesh if they share the same position
public class PointToVertexIndexMap
{
	private const int NoVertex = -1;

	private readonly int[] pointToVertexIndex;

	private readonly int clearThreshold;

	private int vertexThreshold;

	private int nextVertexIndex;

	public PointToVertexIndexMap(ChunkSize size)
	{
		size.Validate(nameof(size));

		this.Size = size;
		this.pointToVertexIndex = Factory.CreateArray(CompactPoint2Indexer.GetLength(size), NoVertex);
		this.clearThreshold = int.MaxValue - this.pointToVertexIndex.Length - 1;
	}

	public ChunkSize Size { get; }

	public int Length => this.pointToVertexIndex.Length;

	public int VertexThreshold => this.vertexThreshold;

	public int VertexCount => this.nextVertexIndex;

	// returns true if an already assigned vertex is found
	// returns false if a new vertex index is assigned
	public bool TryGetOrCreateVertexIndex(CompactPoint2 point, out int index)
	{
		index = this.pointToVertexIndex[point.Index] - this.vertexThreshold;
		if (index >= 0)
		{
			return true;
		}
		else
		{
			Debug.Assert(this.nextVertexIndex < this.pointToVertexIndex.Length);

			index = this.nextVertexIndex;
			this.pointToVertexIndex[point.Index] = index + this.vertexThreshold;
			this.nextVertexIndex++;
			return false;
		}
	}

	public void Clear()
	{
		// Do a fast clear by increasing the vertex threshold by the last used max vertex index.
		// This will cause all vertex indices currently stored to become negative and be treated
		// as not created yet.
		this.vertexThreshold += this.nextVertexIndex;
		this.nextVertexIndex = 0;

		if (this.vertexThreshold >= this.clearThreshold)
		{
			// If vertex threshold gets too close to int.MaxValue an arithmetic overflow exception
			// can occur when assigning vertex indices so actually clear the array.
			this.vertexThreshold = 0;

			var length = this.pointToVertexIndex.Length;
			for (int i = 0; i < length; i++)
			{
				this.pointToVertexIndex[i] = NoVertex;
			}
		}
	}
}
