﻿namespace HQS.VUE.Neo;

// TODO need to add in support for multiple different types of mesh materials
// Maybe multiple different types of mesh materials will be handled by a different system entirely.
public readonly record struct CoplanarChunk<TSurface>
	where TSurface : unmanaged
{
	private CoplanarChunk(
		PagedValuesSequence<PlaneGroup> planes,
		PagedValuesSequence<SurfaceGroup<TSurface>> surfaces,
		PagedValuesSequence<CompactTriangle> triangles)
	{
		this.Planes = planes;
		this.Surfaces = surfaces;
		this.Triangles = triangles;
	}

	public static CoplanarChunk<TSurface> Empty => default;

	public PagedValuesSequence<PlaneGroup> Planes { get; }

	public PagedValuesSequence<SurfaceGroup<TSurface>> Surfaces { get; }

	public PagedValuesSequence<CompactTriangle> Triangles { get; }

	public class Builder : IBuilder<CoplanarChunk<TSurface>>
	{
		private readonly PagedList<PlaneGroup> planes;

		private readonly PagedList<SurfaceGroup<TSurface>> surfaces;

		private readonly PagedList<CompactTriangle> triangles;

		private int startSurfaceCount;

		private int startTriangleCount;

		internal Builder(
			IPool<Page<PlaneGroup>> planePages,
			IPool<Page<SurfaceGroup<TSurface>>> surfacePages,
			IPool<Page<CompactTriangle>> trianglePages)
		{
			Debug.Assert(planePages != null);
			Debug.Assert(surfacePages != null);
			Debug.Assert(trianglePages != null);

			this.planes = new PagedList<PlaneGroup>(planePages);
			this.surfaces = new PagedList<SurfaceGroup<TSurface>>(surfacePages);
			this.triangles = new PagedList<CompactTriangle>(trianglePages);
		}

		public PagedValues<PlaneGroup> Planes => this.planes.Values;

		public PagedValues<SurfaceGroup<TSurface>> Surfaces => this.surfaces.Values;

		public PagedValues<CompactTriangle> Triangles => this.triangles.Values;

		public void ClearPages()
		{
			this.planes.Clear(false);
			this.surfaces.Clear(false);
			this.triangles.Clear(false);
		}

		public void Concat(Builder other)
		{
			Ensure.That(other, nameof(other)).IsNotNull();

			this.planes.Concat(other.planes);
			this.surfaces.Concat(other.surfaces);
			this.triangles.Concat(other.triangles);
		}

		public void StartPlane()
		{
			this.startSurfaceCount = this.surfaces.Values.Count;
			this.startTriangleCount = this.triangles.Values.Count;
		}

		public void EndPlane(PlaneIndex plane)
		{
			int surfacesAdded = this.surfaces.Values.Count - this.startSurfaceCount;
			if (surfacesAdded > 0)
			{
				this.AddPlane(new PlaneGroup(
					plane, surfacesAdded, this.triangles.Values.Count - this.startTriangleCount));
			}
		}

		public void AddPlane(PlaneGroup group)
		{
			CoplanarChunkUtility.AssertValid(group);

			this.planes.Add(group);
		}

		public void AddSurface(SurfaceGroup<TSurface> group)
		{
			CoplanarChunkUtility.AssertValid(group);

			this.surfaces.Add(group);
		}

		public void AddTriangle(CompactTriangle triangle)
		{
			CoplanarChunkUtility.AssertValid(triangle);

			this.triangles.Add(triangle);
		}

		// TODO maybe this should be internal?
		public void AddSurfaces(
			ref PagedValuesEnumerator<SurfaceGroup<TSurface>> surfaces, int count)
		{
			Debug.Assert(!surfaces.IsEmpty);
			Debug.Assert(count > 0);

			this.surfaces.Add(ref surfaces, count);
		}

		// TODO maybe this should be internal?
		public void AddTriangles(
			ref PagedValuesEnumerator<CompactTriangle> triangles, int count)
		{
			Debug.Assert(!triangles.IsEmpty);
			Debug.Assert(count > 0);

			this.triangles.Add(ref triangles, count);
		}

		/// <inheritdoc />
		public CoplanarChunk<TSurface> Build()
		{
			var result = new CoplanarChunk<TSurface>(
				this.planes.DetachValuesSequence(),
				this.surfaces.DetachValuesSequence(),
				this.triangles.DetachValuesSequence());

			CoplanarChunkUtility.AssertValid(result);

			return result;
		}
	}
}
