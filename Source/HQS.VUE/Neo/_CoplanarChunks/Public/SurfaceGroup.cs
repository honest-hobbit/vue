﻿namespace HQS.VUE.Neo;

public readonly record struct SurfaceGroup<TSurface>
	where TSurface : unmanaged
{
	public readonly TSurface SurfaceType;

	public readonly int TriangleCount;

	public SurfaceGroup(TSurface surfaceType, int triangleCount)
	{
		this.SurfaceType = surfaceType;
		this.TriangleCount = triangleCount;
	}
}
