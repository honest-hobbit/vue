﻿namespace HQS.VUE.Neo;

public readonly record struct CompactPoint2
{
	private static readonly CompactPoint2Indexer Indexer = new CompactPoint2Indexer(ChunkSize.Max);

	public readonly ushort Index;

	public CompactPoint2(ushort index)
	{
		this.Index = index;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static CompactPoint2 From(int x, int y) => Indexer.GetPoint(x, y);

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static CompactPoint2 From(Int2 index) => Indexer.GetPoint(index.X, index.Y);

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	internal static CompactPoint2 From(Point2 point) => Indexer.GetPoint(point.X, point.Y);

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void GetPosition(out int x, out int y) => Indexer.GetPosition(this, out x, out y);

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public Int2 GetPosition() => Indexer.GetPosition(this);

	/// <inheritdoc />
	public override string ToString() => this.Index.ToString();
}
