﻿namespace HQS.VUE.Neo;

public readonly record struct CompactTriangle
{
	public readonly CompactPoint2 A;

	public readonly CompactPoint2 B;

	public readonly CompactPoint2 C;

	public CompactTriangle(CompactPoint2 a, CompactPoint2 b, CompactPoint2 c)
	{
		this.A = a;
		this.B = b;
		this.C = c;
	}
}
