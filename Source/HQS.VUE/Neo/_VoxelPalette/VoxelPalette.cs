﻿namespace HQS.VUE.Neo;

// TODO this does not support optionally not optimizing a surface
public readonly record struct VoxelPalette<TVoxel, TSurface> : ISurfaceExtractor<TVoxel, TSurface>
	where TVoxel : unmanaged, IEquatable<TVoxel>, IIndexable
	where TSurface : unmanaged, IEquatable<TSurface>, IComparable<TSurface>
{
	private readonly string[] surfaceCategoryNames;

	private readonly string[] voxelTypeNames;

	private readonly VoxelType<TSurface>[] voxelTypes;

	private VoxelPalette(
		string[] surfaceCategoryNames, string[] voxelTypeNames, VoxelType<TSurface>[] voxelTypes)
	{
		Debug.Assert(surfaceCategoryNames != null);
		Debug.Assert(surfaceCategoryNames.Length == Length.OfULong.InBits);
		Debug.Assert(voxelTypeNames != null);
		Debug.Assert(voxelTypes != null);
		Debug.Assert(voxelTypeNames.Length == voxelTypes.Length);

		this.surfaceCategoryNames = surfaceCategoryNames;
		this.voxelTypeNames = voxelTypeNames;
		this.voxelTypes = voxelTypes;
	}

	public bool IsDefault => this.voxelTypes == null;

	public ReadOnlyArray<string> SurfaceCategoryNames => new ReadOnlyArray<string>(this.surfaceCategoryNames);

	public ReadOnlyArray<string> VoxelTypeNames => new ReadOnlyArray<string>(this.voxelTypeNames);

	public ReadOnlyArray<VoxelType<TSurface>> VoxelTypes => new ReadOnlyArray<VoxelType<TSurface>>(this.voxelTypes);

	/// <inheritdoc />
	public TVoxel EmptyVoxel => default;

	/// <inheritdoc />
	public TSurface EmptySurface => default;

	/// <inheritdoc />
	public bool IsEmpty(TSurface surface) => surface.Equals(this.EmptySurface);

	/// <inheritdoc />
	public bool IsSame(TSurface a, TSurface b) => a.Equals(b);

	/// <inheritdoc />
	public int Compare(TSurface a, TSurface b) => a.CompareTo(b);

	/// <inheritdoc />
	public void GetSurfaces(TVoxel voxel, ref bool hasSurface, ref TSurface surface)
	{
		if (!voxel.Equals(this.EmptyVoxel))
		{
			surface = this.voxelTypes[voxel.Index].SurfaceType;
			hasSurface = true;
		}
	}

	/// <inheritdoc />
	public void GetSurfaces(
		TVoxel voxelA,
		TVoxel voxelB,
		ref bool hasSurfaceA,
		ref TSurface surfaceA,
		ref bool hasSurfaceB,
		ref TSurface surfaceB)
	{
		if (voxelA.Equals(voxelB))
		{
			var voxelType = this.voxelTypes[voxelA.Index];
			if (voxelType.HasInternalSurfaces)
			{
				surfaceA = voxelType.SurfaceType;
				surfaceB = voxelType.SurfaceType;
				hasSurfaceA = true;
				hasSurfaceB = true;
			}
		}
		else
		{
			var voxelTypeA = this.voxelTypes[voxelA.Index];
			var voxelTypeB = this.voxelTypes[voxelB.Index];

			if ((voxelTypeA.IsSurfacePresentBits & voxelTypeB.SurfaceCategoryBits) != 0)
			{
				hasSurfaceA = true;
				surfaceA = voxelTypeA.SurfaceType;
			}

			if ((voxelTypeB.IsSurfacePresentBits & voxelTypeA.SurfaceCategoryBits) != 0)
			{
				hasSurfaceB = true;
				surfaceB = voxelTypeB.SurfaceType;
			}
		}
	}

	public class Builder
	{
		public Builder(int initialCapacity, int maxCapacity)
		{
			this.VoxelTypes = new Palette<VoxelTypeBuilder<TSurface>>(initialCapacity, maxCapacity);
		}

		public string[] SurfaceCategoryNames { get; } = new string[Length.OfULong.InBits];

		public Palette<VoxelTypeBuilder<TSurface>> VoxelTypes { get; }

		public void Copy(VoxelPalette<TVoxel, TSurface> palette)
		{
			Ensure.That(palette.IsDefault, $"{nameof(palette)}.{nameof(palette.IsDefault)}").IsFalse();

			palette.surfaceCategoryNames.CopyTo(this.SurfaceCategoryNames, 0);

			int length = palette.voxelTypes.Length;
			this.VoxelTypes.Clear();
			this.VoxelTypes.EnsureCapacity(length);

			for (int i = 0; i < length; i++)
			{
				this.VoxelTypes.Add(palette.voxelTypes[i].ToBuilder(palette.voxelTypeNames[i]));
			}
		}

		public VoxelPalette<TVoxel, TSurface> BuildPalette()
		{
			var result = new VoxelPalette<TVoxel, TSurface>(
				this.SurfaceCategoryNames.Copy(),
				this.VoxelTypes.Select(x => x.Name).ToArrayExtended(),
				this.VoxelTypes.Select(x => x.ToType()).ToArrayExtended());

			this.SurfaceCategoryNames.AsSpan().Clear();
			this.VoxelTypes.Clear();

			return result;
		}
	}
}
