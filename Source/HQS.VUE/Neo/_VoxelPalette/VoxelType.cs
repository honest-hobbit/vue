﻿namespace HQS.VUE.Neo;

public readonly record struct VoxelType<TSurface>
	where TSurface : unmanaged
{
	public readonly TSurface SurfaceType;

	public readonly ulong SurfaceCategoryBits;

	public readonly ulong IsSurfacePresentBits;

	public readonly bool HasInternalSurfaces;

	public VoxelType(
		TSurface surfaceType,
		ulong surfaceCategoryBits,
		ulong isSurfacePresentBits,
		bool hasInternalSurfaces)
	{
		this.SurfaceType = surfaceType;
		this.SurfaceCategoryBits = surfaceCategoryBits;
		this.IsSurfacePresentBits = isSurfacePresentBits;
		this.HasInternalSurfaces = hasInternalSurfaces;
	}

	public VoxelTypeBuilder<TSurface> ToBuilder(string name) => new VoxelTypeBuilder<TSurface>()
	{
		Name = name,
		SurfaceType = this.SurfaceType,
		SurfaceCategoryBits = new Bit64(this.SurfaceCategoryBits),
		IsSurfacePresentBits = new Bit64(this.IsSurfacePresentBits),
	};
}
