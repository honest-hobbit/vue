﻿namespace HQS.VUE.Neo;

public record struct VoxelTypeBuilder<TSurface>
	where TSurface : unmanaged
{
	public string Name;

	public TSurface SurfaceType;

	public Bit64 SurfaceCategoryBits;

	public Bit64 IsSurfacePresentBits;

	public bool HasInternalSurfaces;

	public VoxelType<TSurface> ToType() => new VoxelType<TSurface>(
		this.SurfaceType,
		this.SurfaceCategoryBits.Value,
		this.IsSurfacePresentBits.Value,
		this.HasInternalSurfaces);
}
