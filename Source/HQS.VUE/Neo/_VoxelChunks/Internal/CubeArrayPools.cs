﻿namespace HQS.VUE.Neo;

internal class CubeArrayPools<TVoxel>
	where TVoxel : unmanaged
{
	private readonly ArrayPool<TVoxel>[] pools;

	private readonly int minExponent;

	private readonly int maxExponent;

	public CubeArrayPools(Func<int, IPool<TVoxel[]>> createPool)
		: this(createPool, ChunkSize.Min, ChunkSize.Max)
	{
	}

	public CubeArrayPools(
		Func<int, IPool<TVoxel[]>> createPool, ChunkSize minSize, ChunkSize maxSize)
	{
		Ensure.That(createPool, nameof(createPool)).IsNotNull();
		minSize.Validate(nameof(minSize));
		maxSize.Validate(nameof(maxSize));
		Ensure.That(minSize.Exponent, nameof(minSize)).IsLte(maxSize.Exponent);

		this.minExponent = minSize.Exponent;
		this.maxExponent = maxSize.Exponent;

		this.pools = new ArrayPool<TVoxel>[maxSize.Exponent - minSize.Exponent + 1];

		for (int i = 0; i < this.pools.Length; i++)
		{
			int arrayLength = ChunkSize.CreateAssertOnly(i + this.minExponent).AsCube.Length;
			this.pools[i] = new ArrayPool<TVoxel>(createPool(arrayLength), arrayLength);
		}

		this.Arrays = ReadOnlyList.ConvertReadOnly(this.pools, x => x.Pool);
	}

	public IReadOnlyList<ArrayPoolControls> Arrays { get; }

	public ChunkSize MinSize => new ChunkSize(this.minExponent);

	public ChunkSize MaxSize => new ChunkSize(this.maxExponent);

	public void Validate(ChunkSize size, string name) =>
		Ensure.That(size.Exponent, name).IsInRange(this.minExponent, this.maxExponent);

	public ArrayPool<TVoxel> GetPool(ChunkSize size)
	{
		size.AssertValid();
		Debug.Assert(size.Exponent.IsIn(this.minExponent, this.maxExponent));

		return this.pools[size.Exponent - this.minExponent];
	}
}
