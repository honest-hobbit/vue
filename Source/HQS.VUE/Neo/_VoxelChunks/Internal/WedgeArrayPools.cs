﻿namespace HQS.VUE.Neo;

internal class WedgeArrayPools<TVoxel>
	where TVoxel : unmanaged
{
	private readonly ArrayPool<Wedge<TVoxel>>[,] pools;

	private readonly int minExponent;

	private readonly int maxExponent;

	private readonly int maxChannels;

	public WedgeArrayPools(Func<int, IPool<Wedge<TVoxel>[]>> createPool)
		: this(createPool, ChunkSize.Min, ChunkSize.Max, WedgeVoxel.MaxChannels)
	{
	}

	public WedgeArrayPools(Func<int, IPool<Wedge<TVoxel>[]>> createPool, int maxChannels)
		: this(createPool, ChunkSize.Min, ChunkSize.Max, maxChannels)
	{
	}

	public WedgeArrayPools(
		Func<int, IPool<Wedge<TVoxel>[]>> createPool, ChunkSize minSize, ChunkSize maxSize, int maxChannels)
	{
		Ensure.That(createPool, nameof(createPool)).IsNotNull();
		minSize.Validate(nameof(minSize));
		maxSize.Validate(nameof(maxSize));
		Ensure.That(minSize.Exponent, nameof(minSize)).IsLte(maxSize.Exponent);
		WedgeVoxel.ValidateChannels(maxChannels, nameof(maxChannels));

		this.minExponent = minSize.Exponent;
		this.maxExponent = maxSize.Exponent;
		this.maxChannels = maxChannels;

		this.pools = new ArrayPool<Wedge<TVoxel>>[maxSize.Exponent - minSize.Exponent + 1, maxChannels];

		var poolsByLengeth = new Dictionary<int, IPool<Wedge<TVoxel>[]>>(
			this.pools.Length, EqualityComparer.ForStruct<int>());

		int maxExponent = maxSize.Exponent;
		for (int exponent = minSize.Exponent; exponent <= maxExponent; exponent++)
		{
			var size = ChunkSize.CreateAssertOnly(exponent);
			int chunkLength = size.AsCube.Length;

			for (int channels = WedgeVoxel.MinChannels; channels <= maxChannels; channels++)
			{
				int arrayLength = chunkLength * channels;

				if (!poolsByLengeth.TryGetValue(arrayLength, out var pool))
				{
					pool = createPool(arrayLength);
					poolsByLengeth[arrayLength] = pool;
				}

				var index = this.GetPoolIndex(size, channels);
				this.pools[index.X, index.Y] = new ArrayPool<Wedge<TVoxel>>(pool, arrayLength);
			}
		}

		this.Arrays = ArrayPoolUtility.CreateReadOnlyListFrom(poolsByLengeth);
	}

	public IReadOnlyList<ArrayPoolControls> Arrays { get; }

	public ChunkSize MinSize => new ChunkSize(this.minExponent);

	public ChunkSize MaxSize => new ChunkSize(this.maxExponent);

	public int MaxChannels => this.maxChannels;

	public void Validate(ChunkSize size, string name) =>
		Ensure.That(size.Exponent, name).IsInRange(this.minExponent, this.maxExponent);

	public void Validate(int channels, string name) =>
		Ensure.That(channels, name).IsInRange(WedgeVoxel.MinChannels, this.maxChannels);

	public ArrayPool<Wedge<TVoxel>> GetPool(ChunkSize size, int channels)
	{
		Int2 index = this.GetPoolIndex(size, channels);
		return this.pools[index.X, index.Y];
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private Int2 GetPoolIndex(ChunkSize size, int channels)
	{
		size.AssertValid();
		Debug.Assert(size.Exponent.IsIn(this.minExponent, this.maxExponent));
		Debug.Assert(channels.IsIn(WedgeVoxel.MinChannels, this.maxChannels));

		return new Int2(size.Exponent - this.minExponent, channels - WedgeVoxel.MinChannels);
	}
}
