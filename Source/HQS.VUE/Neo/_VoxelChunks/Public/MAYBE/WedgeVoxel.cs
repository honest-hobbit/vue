﻿namespace HQS.VUE.Neo;

// TODO maybe delete this?
// TODO maybe this should be a class?
public record struct WedgeVoxel<TVoxel>
	where TVoxel : unmanaged
{
	public int Channels;

	public Wedge<TVoxel> Channel0;

	public Wedge<TVoxel> Channel1;

	public Wedge<TVoxel> Channel2;

	public Wedge<TVoxel> Channel3;

	public Wedge<TVoxel> Channel4;

	public Wedge<TVoxel> Channel5;

	public Wedge<TVoxel> Channel6;

	public Wedge<TVoxel> Channel7;

	public Wedge<TVoxel> Channel8;

	public Wedge<TVoxel> Channel9;

	public Wedge<TVoxel> Channel10;

	public Wedge<TVoxel> Channel11;

	public Wedge<TVoxel> Channel12;

	public Wedge<TVoxel> Channel13;

	public Wedge<TVoxel> Channel14;

	public Wedge<TVoxel> Channel15;
}
