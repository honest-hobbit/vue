﻿namespace HQS.VUE.Neo;

public readonly record struct WedgeChunk<TVoxel>
	where TVoxel : unmanaged
{
	private readonly Wedge<TVoxel> uniformVoxel;

	private readonly Wedge<TVoxel>[] voxels;

	public WedgeChunk(ChunkSize size, Wedge<TVoxel> uniformVoxel)
	{
		size.Validate(nameof(size));

		this.Size = size;
		this.Channels = 1;
		this.uniformVoxel = uniformVoxel;
		this.voxels = null;
	}

	private WedgeChunk(ChunkSize size, byte channels, Wedge<TVoxel>[] voxels)
	{
		size.AssertValid();
		WedgeVoxel.AssertChannelsValid(channels);
		Debug.Assert(voxels != null);

		this.Size = size;
		this.Channels = channels;
		this.uniformVoxel = default;
		this.voxels = voxels;
	}

	public bool IsDefault => !this.Size.IsValid;

	public ChunkSize Size { get; }

	public byte Channels { get; }

	public bool IsUniform
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.voxels == null;
	}

	public Wedge<TVoxel> UniformVoxel
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			// TODO should this be Ensure?
			Debug.Assert(this.IsUniform);

			return this.uniformVoxel;
		}
	}

	public ReadOnlyWedgeArray<TVoxel> Voxels
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			// TODO should this be Ensure?
			Debug.Assert(!this.IsUniform);

			return new ReadOnlyWedgeArray<TVoxel>(this.Size.AsCube, this.Channels, this.voxels);
		}
	}

	internal void ReturnToPool(WedgeArrayPools<TVoxel> pools)
	{
		Debug.Assert(pools != null);
		this.Size.AssertValid();

		if (this.voxels != null)
		{
			pools.GetPool(this.Size, this.Channels).Return(this.voxels);
		}
	}

	public class Builder : IBuilder<WedgeChunk<TVoxel>>
	{
		private readonly WedgeArrayPools<TVoxel> arrayPools;

		internal Builder(WedgeArrayPools<TVoxel> arrayPools)
		{
			Debug.Assert(arrayPools != null);

			this.arrayPools = arrayPools;
		}

		public ChunkSize Size { get; private set; } = ChunkSize.Invalid;

		public int Channels { get; private set; } = 0;

		public bool IsUniform { get; set; } = false;

		public Wedge<TVoxel> UniformVoxel { get; set; } = Wedge<TVoxel>.Empty;

		public WedgeArray<TVoxel> Voxels { get; private set; }

		public bool IsInitialized => this.Size != ChunkSize.Invalid;

		public void Initialize(ChunkSize size, int channels)
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsFalse();
			this.arrayPools.Validate(size, nameof(size));
			this.arrayPools.Validate(channels, nameof(channels));

			Debug.Assert(this.Voxels.IsDefault);

			this.Size = size;
			this.Channels = channels;
			this.Voxels = new WedgeArray<TVoxel>(
				size.AsCube, channels, this.arrayPools.GetPool(this.Size, this.Channels).Rent());
		}

		// TODO need a version of this that allows a different number of channels
		public void Initialize(WedgeChunk<TVoxel> chunk)
		{
			// Initialize validates chunk.Size which effectively also validates that chunk is not default
			this.Initialize(chunk.Size, chunk.Channels);

			this.IsUniform = chunk.IsUniform;
			if (chunk.IsUniform)
			{
				this.UniformVoxel = chunk.UniformVoxel;
			}
			else
			{
				chunk.Voxels.Array.CopyTo(this.Voxels.Array);
			}
		}

		public void Initialize(WedgeChunk<TVoxel> chunk, int channels)
		{
			// Initialize validates chunk.Size which effectively also validates that chunk is not default
			this.Initialize(chunk.Size, channels);

			this.IsUniform = chunk.IsUniform;
			if (chunk.IsUniform)
			{
				this.UniformVoxel = chunk.UniformVoxel;
			}
			else
			{
				if (chunk.Channels == channels)
				{
					chunk.Voxels.Array.CopyTo(this.Voxels.Array);
					return;
				}

				// TODO this method needs additional testing to verify is copies the data correctly.
				// TODO Initialize and SetChannels have very similar forms of copying
				// that should be refactored into a helper method (with performance testing)
				var result = this.Voxels.Array;
				var source = chunk.Voxels.Array;
				int sourceChannels = chunk.Channels;
				int sourceLength = source.Length;
				int copyLength = Math.Min(sourceChannels, channels);
				int sourceIndex = 0;
				int resultIndex = 0;

				while (sourceIndex < sourceLength)
				{
					source.Slice(sourceIndex, copyLength).CopyTo(new Span<Wedge<TVoxel>>(result, resultIndex, copyLength));
					sourceIndex += sourceChannels;
					resultIndex += channels;
				}
			}
		}

		public void Deinitialize()
		{
			if (!this.Voxels.IsDefault)
			{
				Debug.Assert(this.IsInitialized);

				this.arrayPools.GetPool(this.Size, this.Channels).Return(this.Voxels.Array);
				this.Voxels = default;
			}

			this.Size = ChunkSize.Invalid;
			this.Channels = 0;
			this.IsUniform = false;
			this.UniformVoxel = Wedge<TVoxel>.Empty;
		}

		/// <inheritdoc />
		public WedgeChunk<TVoxel> Build()
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

			Debug.Assert(!this.Voxels.IsDefault);

			WedgeChunk<TVoxel> result;
			if (this.IsUniform)
			{
				result = new WedgeChunk<TVoxel>(this.Size, this.UniformVoxel);
				this.arrayPools.GetPool(this.Size, this.Channels).Return(this.Voxels.Array);
			}
			else
			{
				result = new WedgeChunk<TVoxel>(this.Size, (byte)this.Channels, this.Voxels.Array);
			}

			this.Size = ChunkSize.Invalid;
			this.Channels = 0;
			this.IsUniform = false;
			this.UniformVoxel = Wedge<TVoxel>.Empty;
			this.Voxels = default;

			return result;
		}

		// This automatically copies the data from the original array to the new array.
		// If increasing the number of channels then no data is lost.
		// If decreasing the number of channels then data from the higher number channels is lost.
		public void SetChannels(int channels)
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();
			this.arrayPools.Validate(channels, nameof(channels));

			if (this.Channels == channels)
			{
				return;
			}

			// TODO this method needs additional testing to verify is copies the data correctly.
			// TODO Initialize and SetChannels have very similar forms of copying
			// that should be refactored into a helper method (with performance testing)
			var result = this.arrayPools.GetPool(this.Size, channels).Rent();
			var source = this.Voxels.Array;
			int sourceChannels = this.Channels;
			int sourceLength = source.Length;
			int copyLength = Math.Min(sourceChannels, channels);
			int sourceIndex = 0;
			int resultIndex = 0;

			while (sourceIndex < sourceLength)
			{
				Array.Copy(source, sourceIndex, result, resultIndex, copyLength);
				sourceIndex += sourceChannels;
				resultIndex += channels;
			}

			this.arrayPools.GetPool(this.Size, this.Channels).Return(source);
			this.Channels = channels;
			this.Voxels = new WedgeArray<TVoxel>(this.Size.AsCube, this.Channels, result);
		}
	}
}
