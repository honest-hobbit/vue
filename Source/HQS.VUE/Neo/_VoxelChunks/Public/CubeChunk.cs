﻿namespace HQS.VUE.Neo;

public readonly record struct CubeChunk<TVoxel>
	where TVoxel : unmanaged
{
	private readonly TVoxel uniformVoxel;

	private readonly TVoxel[] voxels;

	public CubeChunk(ChunkSize size, TVoxel uniformVoxel)
	{
		size.Validate(nameof(size));

		this.Size = size;
		this.uniformVoxel = uniformVoxel;
		this.voxels = null;
	}

	private CubeChunk(ChunkSize size, TVoxel[] voxels)
	{
		size.AssertValid();
		Debug.Assert(voxels != null);

		this.Size = size;
		this.uniformVoxel = default;
		this.voxels = voxels;
	}

	public bool IsDefault => !this.Size.IsValid;

	public ChunkSize Size { get; }

	public bool IsUniform
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.voxels == null;
	}

	public TVoxel UniformVoxel
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			// TODO should this be Ensure?
			Debug.Assert(this.IsUniform);

			return this.uniformVoxel;
		}
	}

	public ReadOnlyCubeArray<TVoxel> Voxels
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			// TODO should this be Ensure?
			Debug.Assert(!this.IsUniform);

			return new ReadOnlyCubeArray<TVoxel>(this.Size.AsCube, this.voxels);
		}
	}

	internal void ReturnToPool(CubeArrayPools<TVoxel> pools)
	{
		Debug.Assert(pools != null);
		this.Size.AssertValid();

		if (this.voxels != null)
		{
			pools.GetPool(this.Size).Return(this.voxels);
		}
	}

	public class Builder : IBuilder<CubeChunk<TVoxel>>
	{
		private readonly CubeArrayPools<TVoxel> arrayPools;

		internal Builder(CubeArrayPools<TVoxel> arrayPools)
		{
			Debug.Assert(arrayPools != null);

			this.arrayPools = arrayPools;
		}

		public ChunkSize Size { get; private set; } = ChunkSize.Invalid;

		public bool IsUniform { get; set; } = false;

		public TVoxel UniformVoxel { get; set; } = default;

		public CubeArray<TVoxel> Voxels { get; private set; }

		public bool IsInitialized => this.Size != ChunkSize.Invalid;

		public void Initialize(ChunkSize size)
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsFalse();
			this.arrayPools.Validate(size, nameof(size));

			Debug.Assert(this.Voxels.IsDefault);

			this.Size = size;
			this.Voxels = new CubeArray<TVoxel>(
				size.AsCube, this.arrayPools.GetPool(this.Size).Rent());
		}

		public void Initialize(CubeChunk<TVoxel> chunk)
		{
			// Initialize validates chunk.Size which effectively also validates that chunk is not default
			this.Initialize(chunk.Size);

			this.IsUniform = chunk.IsUniform;
			if (chunk.IsUniform)
			{
				this.UniformVoxel = chunk.UniformVoxel;
			}
			else
			{
				chunk.Voxels.Array.CopyTo(this.Voxels.Array);
			}
		}

		public void Deinitialize()
		{
			if (!this.Voxels.IsDefault)
			{
				Debug.Assert(this.IsInitialized);

				this.arrayPools.GetPool(this.Size).Return(this.Voxels.Array);
				this.Voxels = default;
			}

			this.Size = ChunkSize.Invalid;
			this.IsUniform = false;
			this.UniformVoxel = default;
		}

		/// <inheritdoc />
		public CubeChunk<TVoxel> Build()
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

			Debug.Assert(!this.Voxels.IsDefault);

			CubeChunk<TVoxel> result;
			if (this.IsUniform)
			{
				result = new CubeChunk<TVoxel>(this.Size, this.UniformVoxel);
				this.arrayPools.GetPool(this.Size).Return(this.Voxels.Array);
			}
			else
			{
				result = new CubeChunk<TVoxel>(this.Size, this.Voxels.Array);
			}

			this.Size = ChunkSize.Invalid;
			this.IsUniform = false;
			this.UniformVoxel = default;
			this.Voxels = default;

			return result;
		}
	}
}
