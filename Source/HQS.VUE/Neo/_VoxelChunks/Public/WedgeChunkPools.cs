﻿namespace HQS.VUE.Neo;

public class WedgeChunkPools<TVoxel>
	where TVoxel : unmanaged
{
	private readonly WedgeArrayPools<TVoxel> arrayPools;

	internal WedgeChunkPools(
		IPool<WedgeChunk<TVoxel>.Builder> builders, WedgeArrayPools<TVoxel> arrayPools)
	{
		Debug.Assert(builders != null);
		Debug.Assert(arrayPools != null);

		this.Builders = builders;
		this.arrayPools = arrayPools;
	}

	public IPool<WedgeChunk<TVoxel>.Builder> Builders { get; }

	public IReadOnlyList<ArrayPoolControls> Arrays => this.arrayPools.Arrays;

	public ChunkSize MinSize => this.arrayPools.MinSize;

	public ChunkSize MaxSize => this.arrayPools.MaxSize;

	public int MaxChannels => this.arrayPools.MaxChannels;

	public ArrayPoolControls GetArrayPool(ChunkSize size, int channels)
	{
		this.arrayPools.Validate(size, nameof(size));
		this.arrayPools.Validate(channels, nameof(channels));

		return this.arrayPools.GetPool(size, channels).Pool;
	}

	public void ReturnChunk(WedgeChunk<TVoxel> chunk)
	{
		// validating chunk.Size effectively also validates that chunk is not default
		this.arrayPools.Validate(chunk.Size, $"{nameof(chunk)}.{nameof(chunk.Size)}");
		this.arrayPools.Validate(chunk.Channels, $"{nameof(chunk)}.{nameof(chunk.Channels)}");

		chunk.ReturnToPool(this.arrayPools);
	}
}
