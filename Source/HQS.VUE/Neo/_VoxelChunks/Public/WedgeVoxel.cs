﻿namespace HQS.VUE.Neo;

public static class WedgeVoxel
{
	public const int MinChannels = 1;

	public const int MaxChannels = 16;

	public static void ValidateChannels(int channels, string name) =>
		Ensure.That(channels, name).IsInRange(MinChannels, MaxChannels);

	[Conditional(CompilationSymbol.Debug)]
	internal static void AssertChannelsValid(int channels) =>
		Debug.Assert(channels.IsIn(MinChannels, MaxChannels));
}
