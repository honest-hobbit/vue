﻿namespace HQS.VUE.Neo;

public readonly record struct ReadOnlyWedgeArray<T> : IEnumerable<Wedge<T>>
	where T : unmanaged
{
	private readonly Wedge<T>[] array;

	public ReadOnlyWedgeArray(WedgeArray<T> array)
	{
		this.Indexer = array.Indexer;
		this.Channels = array.Channels;
		this.array = array.Array;
	}

	public ReadOnlyWedgeArray(CubeArrayIndexer indexer, int channels, Wedge<T>[] array)
	{
		// TODO Assert or Ensure?
		WedgeVoxel.AssertChannelsValid(channels);

		if (array != null)
		{
			Ensure.That(array.Length, $"{nameof(array)}.Length").Is(indexer.Length * channels);
		}

		this.Indexer = indexer;
		this.Channels = (byte)channels;
		this.array = array;
	}

	public bool IsDefault => this.array == null;

	public CubeArrayIndexer Indexer { get; }

	public byte Channels { get; }

	public ReadOnlyArray<Wedge<T>> Array => new ReadOnlyArray<Wedge<T>>(this.array);

	public int Count => this.Indexer.Length;

	public ReadOnlySpan<Wedge<T>> this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => new ReadOnlySpan<Wedge<T>>(
			this.array, index * this.Channels, this.Channels);
	}

	public ReadOnlySpan<Wedge<T>> this[int x, int y, int z]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => new ReadOnlySpan<Wedge<T>>(
			this.array, this.Indexer[x, y, z] * this.Channels, this.Channels);
	}

	public ReadOnlySpan<Wedge<T>> this[Int3 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => new ReadOnlySpan<Wedge<T>>(
			this.array, this.Indexer[index] * this.Channels, this.Channels);
	}

	public ArraySegment<Wedge<T>>.Enumerator GetEnumerator() => this.array.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator<Wedge<T>> IEnumerable<Wedge<T>>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
