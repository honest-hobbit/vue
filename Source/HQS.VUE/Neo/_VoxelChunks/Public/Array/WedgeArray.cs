﻿namespace HQS.VUE.Neo;

public readonly record struct WedgeArray<T> : IEnumerable<Wedge<T>>
	where T : unmanaged
{
	public WedgeArray(CubeArrayIndexer indexer, int channels)
		: this(indexer, channels, new Wedge<T>[indexer.Length * channels])
	{
	}

	public WedgeArray(CubeArrayIndexer indexer, int channels, Wedge<T>[] array)
	{
		// TODO Assert or Ensure?
		WedgeVoxel.AssertChannelsValid(channels);

		if (array != null)
		{
			Ensure.That(array.Length, $"{nameof(array)}.Length").Is(indexer.Length * channels);
		}

		this.Indexer = indexer;
		this.Channels = (byte)channels;
		this.Array = array;
	}

	public bool IsDefault => this.Array == null;

	public ReadOnlyWedgeArray<T> AsReadOnly => new ReadOnlyWedgeArray<T>(this);

	public CubeArrayIndexer Indexer { get; }

	public byte Channels { get; }

	public Wedge<T>[] Array { get; }

	public int Count => this.Indexer.Length;

	public Span<Wedge<T>> this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => new Span<Wedge<T>>(this.Array, index * this.Channels, this.Channels);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => value.CopyTo(this[index]);
	}

	public Span<Wedge<T>> this[int x, int y, int z]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => new Span<Wedge<T>>(this.Array, this.Indexer[x, y, z] * this.Channels, this.Channels);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => value.CopyTo(this[x, y, z]);
	}

	public Span<Wedge<T>> this[Int3 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => new Span<Wedge<T>>(this.Array, this.Indexer[index] * this.Channels, this.Channels);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => value.CopyTo(this[index]);
	}

	public ArraySegment<Wedge<T>>.Enumerator GetEnumerator() => this.Array.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator<Wedge<T>> IEnumerable<Wedge<T>>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
