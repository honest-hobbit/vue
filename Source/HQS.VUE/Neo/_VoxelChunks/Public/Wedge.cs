﻿namespace HQS.VUE.Neo;

public readonly record struct Wedge<TVoxel>
	where TVoxel : unmanaged
{
	public static readonly Wedge<TVoxel> Empty = default;

	public readonly WedgeIndex Shape;

	public readonly TVoxel Data;

	public Wedge(WedgeIndex shape, TVoxel data)
	{
		this.Shape = shape;
		this.Data = data;
	}
}
