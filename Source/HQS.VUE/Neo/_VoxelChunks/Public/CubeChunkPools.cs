﻿namespace HQS.VUE.Neo;

public class CubeChunkPools<TVoxel>
	where TVoxel : unmanaged
{
	private readonly CubeArrayPools<TVoxel> arrayPools;

	internal CubeChunkPools(
		IPool<CubeChunk<TVoxel>.Builder> builders, CubeArrayPools<TVoxel> arrayPools)
	{
		Debug.Assert(builders != null);
		Debug.Assert(arrayPools != null);

		this.Builders = builders;
		this.arrayPools = arrayPools;
	}

	public IPool<CubeChunk<TVoxel>.Builder> Builders { get; }

	public IReadOnlyList<ArrayPoolControls> Arrays => this.arrayPools.Arrays;

	public ChunkSize MinSize => this.arrayPools.MinSize;

	public ChunkSize MaxSize => this.arrayPools.MaxSize;

	public ArrayPoolControls GetArrayPool(ChunkSize size)
	{
		this.arrayPools.Validate(size, nameof(size));

		return this.arrayPools.GetPool(size).Pool;
	}

	public void ReturnChunk(CubeChunk<TVoxel> chunk)
	{
		// validating chunk.Size effectively also validates that chunk is not default
		this.arrayPools.Validate(chunk.Size, $"{nameof(chunk)}.{nameof(chunk.Size)}");

		chunk.ReturnToPool(this.arrayPools);
	}
}
