﻿namespace HQS.VUE.Neo;

internal readonly struct ArrayPool<T>
{
	private readonly IPool<T[]> pool;

	private readonly int length;

	public ArrayPool(IPool<T[]> pool, int length)
	{
		Debug.Assert(pool != null);
		Debug.Assert(length > 0);

		this.pool = pool;
		this.length = length;
	}

	public ArrayPoolControls Pool => new ArrayPoolControls(this.pool, this.length);

	public T[] Rent()
	{
		var array = this.pool.Rent();

		Debug.Assert(array != null);
		Debug.Assert(array.Length == this.length);

		return array;
	}

	public void Return(T[] array)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.Length == this.length);

		this.pool.Return(array);
	}
}
