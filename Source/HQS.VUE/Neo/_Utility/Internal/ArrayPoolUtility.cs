﻿namespace HQS.VUE.Neo;

internal static class ArrayPoolUtility
{
	public static IReadOnlyList<ArrayPoolControls> CreateReadOnlyListFrom<T>(
		Dictionary<int, IPool<T[]>> poolsByLengeth)
	{
		Debug.Assert(poolsByLengeth != null);

		var sortedPools = poolsByLengeth.Select(x => new ArrayPoolControls(x.Value, x.Key)).ToArray();
		Array.Sort(sortedPools, (x, y) => x.Length - y.Length);
		return ReadOnlyList.FromReadOnly(sortedPools);
	}

	public static IReadOnlyList<ArrayPoolControls> CreateReadOnlyListFrom<T>(
		Dictionary<int, ArrayPool<T>> poolsByLengeth)
	{
		Debug.Assert(poolsByLengeth != null);

		var sortedPools = poolsByLengeth.Select(x => x.Value.Pool).ToArray();
		Array.Sort(sortedPools, (x, y) => x.Length - y.Length);
		return ReadOnlyList.FromReadOnly(sortedPools);
	}
}
