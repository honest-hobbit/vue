﻿namespace HQS.VUE.Neo;

internal class ChannelArrayPool<TBuilder, TResult>
{
	// TODO these could be updated to FrozenDictionary
	private readonly Dictionary<int, ArrayPool<TBuilder>> builderPools;

	private readonly Dictionary<int, ArrayPool<TResult>> resultPools;

	// TODO maybe add result pools to this?
	public ChannelArrayPool(
		Func<int, IPool<TBuilder[]>> createBuilderPool,
		Func<int, IPool<TResult[]>> createResultPool,
		IEnumerable<int> channelCounts)
	{
		Debug.Assert(createBuilderPool != null);
		Debug.Assert(createResultPool != null);
		Debug.Assert(channelCounts != null);

		this.builderPools = new Dictionary<int, ArrayPool<TBuilder>>(EqualityComparer.ForStruct<int>());
		this.resultPools = new Dictionary<int, ArrayPool<TResult>>(EqualityComparer.ForStruct<int>());

		foreach (int channels in  channelCounts)
		{
			this.builderPools.Add(channels, new ArrayPool<TBuilder>(createBuilderPool(channels), channels));
			this.resultPools.Add(channels, new ArrayPool<TResult>(createResultPool(channels), channels));
		}

		this.BuilderArrays = ArrayPoolUtility.CreateReadOnlyListFrom(this.builderPools);
		this.ResultArrays = ArrayPoolUtility.CreateReadOnlyListFrom(this.resultPools);
	}

	public IReadOnlyList<ArrayPoolControls> BuilderArrays { get; }

	public IReadOnlyList<ArrayPoolControls> ResultArrays { get; }

	public ArrayPool<TBuilder> GetBuilderPool(int channels) => this.builderPools[channels];

	public ArrayPool<TResult> GetResultPool(int channels) => this.resultPools[channels];
}
