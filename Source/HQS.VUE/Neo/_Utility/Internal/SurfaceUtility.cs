﻿namespace HQS.VUE.Neo;

internal static class SurfaceUtility
{
	public static QuadParts FlipLeftRight(QuadParts parts)
	{
		var result = parts & (QuadParts.Top | QuadParts.Bottom);
		////result |= (QuadParts)((int)(parts & QuadParts.Left) >> 2);
		////result |= (QuadParts)((int)(parts & QuadParts.Right) << 2);
		if (parts.Has(QuadParts.Left)) { result |= QuadParts.Right; }
		if (parts.Has(QuadParts.Right)) { result |= QuadParts.Left; }
		return result;
	}

	public static QuadParts FlipTopBottom(QuadParts parts)
	{
		var result = parts & (QuadParts.Left | QuadParts.Right);
		if (parts.Has(QuadParts.Top)) { result |= QuadParts.Bottom; }
		if (parts.Has(QuadParts.Bottom)) { result |= QuadParts.Top; }
		return result;
	}

	public static QuadParts FlipAll(QuadParts parts)
	{
		var result = QuadParts.None;
		if (parts.Has(QuadParts.Left)) { result |= QuadParts.Right; }
		if (parts.Has(QuadParts.Right)) { result |= QuadParts.Left; }
		if (parts.Has(QuadParts.Top)) { result |= QuadParts.Bottom; }
		if (parts.Has(QuadParts.Bottom)) { result |= QuadParts.Top; }
		return result;
	}

	public static WedgeCorners Negate(WedgeCorners corners)
	{
		var result = WedgeCorners.None;
		if (corners.Has(WedgeCorners.NNN)) { result |= WedgeCorners.PPP; }
		if (corners.Has(WedgeCorners.NNP)) { result |= WedgeCorners.PPN; }
		if (corners.Has(WedgeCorners.NPN)) { result |= WedgeCorners.PNP; }
		if (corners.Has(WedgeCorners.NPP)) { result |= WedgeCorners.PNN; }
		if (corners.Has(WedgeCorners.PNN)) { result |= WedgeCorners.NPP; }
		if (corners.Has(WedgeCorners.PNP)) { result |= WedgeCorners.NPN; }
		if (corners.Has(WedgeCorners.PPN)) { result |= WedgeCorners.NNP; }
		if (corners.Has(WedgeCorners.PPP)) { result |= WedgeCorners.NNN; }
		return result;
	}
}
