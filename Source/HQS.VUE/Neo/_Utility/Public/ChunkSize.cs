﻿namespace HQS.VUE.Neo;

public readonly record struct ChunkSize
{
	private readonly byte exponent;

	public ChunkSize(int exponent)
	{
		Ensure.That(exponent, nameof(exponent)).IsInRange(MinExponent, MaxExponent);

		this.exponent = (byte)exponent;
	}

	private ChunkSize(byte exponent)
	{
		Debug.Assert(exponent.IsIn((byte)MinExponent, (byte)MaxExponent));

		this.exponent = exponent;
	}

	public const int MinExponent = 1;

	// Max of 7 is dictated by ushort.MaxValue used by CompactPoint2 and CompactPoint2Indexer.
	//  Length X   *  Length Y  (which is half of Length X)
	// ((2^8) + 1) * ((2^7) + 1)  <= 2^16
	// ushort is also used by EdgeQuad and EdgeState to identify individual edges, the maximum
	// number of which is ((2^7)^2) * 4 = 65536
	public const int MaxExponent = 7;

	public static ChunkSize Invalid => default;

	public static ChunkSize Min => new ChunkSize((byte)MinExponent);

	public static ChunkSize Max => new ChunkSize((byte)MaxExponent);

	public int Exponent
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.exponent;
	}

	public int SideLength
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => MathUtility.PowerOf2(this.exponent);
	}

	public SquareArrayIndexer AsSquare => new SquareArrayIndexer(this.exponent);

	public CubeArrayIndexer AsCube => new CubeArrayIndexer(this.exponent);

	public bool IsValid => this.exponent != 0;

	internal static ChunkSize CreateAssertOnly(int exponent) => new ChunkSize((byte)exponent);

	public void Validate(string name) =>
		Ensure.That(this.Exponent, name).IsInRange(MinExponent, MaxExponent);

	[Conditional(CompilationSymbol.Debug)]
	internal void AssertValid() =>
		Debug.Assert(this.Exponent.IsIn(MinExponent, MaxExponent));

	/// <inheritdoc />
	public override string ToString() => this.exponent.ToString();
}
