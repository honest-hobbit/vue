﻿namespace HQS.VUE.Neo;

public interface IBuilder<T>
{
	T Build();
}
