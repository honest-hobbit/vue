﻿namespace HQS.VUE.Neo;

public static class RotatableExtensions
{
	public static IEnumerable<T> EnumerateMainRotations<T>(this T parts)
		where T : IRotatable<T>
	{
		yield return parts;								 // PosY on top
		yield return parts.RotateZ().RotateZ();			 // NegY on top

		yield return parts.RotateZ();					   // PosX on top
		yield return parts.RotateZ().RotateZ().RotateZ();   // NegX on top

		yield return parts.RotateX().RotateX().RotateX();   // PosZ on top
		yield return parts.RotateX();					   // NegZ on top
	}

	public static IEnumerable<T> EnumerateRotations<T>(this T parts)
		where T : IRotatable<T>
	{
		foreach (var rotation in EnumerateMainRotations(parts))
		{
			var temp = rotation;
			yield return temp;
			temp = temp.RotateY();
			yield return temp;
			temp = temp.RotateY();
			yield return temp;
			temp = temp.RotateY();
			yield return temp;
		}
	}
}
