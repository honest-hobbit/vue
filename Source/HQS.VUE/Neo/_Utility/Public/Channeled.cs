﻿namespace HQS.VUE.Neo;

public readonly record struct Channeled<TResult>
{
	private readonly TResult[] channels;

	private Channeled(TResult[] channels)
	{
		Debug.Assert(channels != null);

		this.channels = channels;
	}

	public ReadOnlyArray<TResult> Channels => new ReadOnlyArray<TResult>(this.channels);

	internal void ReturnToPool<TBuilder>(ChannelArrayPool<TBuilder, TResult> pools)
	{
		Debug.Assert(pools != null);

		// TODO this doesn't return the individual result objects to their pools
		pools.GetResultPool(this.channels.Length).Return(this.channels);
	}

	public class Builder<TBuilder> : IBuilder<Channeled<TResult>>
		where TBuilder : IBuilder<TResult>
	{
		private readonly ChannelArrayPool<TBuilder, TResult> arrayPools;

		private TBuilder[] builders;

		internal Builder(ChannelArrayPool<TBuilder, TResult> arrayPools)
		{
			Debug.Assert(arrayPools != null);

			this.arrayPools = arrayPools;
		}

		public bool IsInitialized => this.builders != null;

		public void Initialize(int channels)
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsFalse();
			Ensure.That(channels, nameof(channels)).IsGt(0);

			this.builders = this.arrayPools.GetBuilderPool(channels).Rent();
		}

		public void Deinitialize()
		{
			this.arrayPools.GetBuilderPool(this.builders.Length).Return(this.builders);
			this.builders = null;
		}

		/// <inheritdoc />
		public Channeled<TResult> Build()
		{
			Ensure.That(this.IsInitialized, nameof(this.IsInitialized)).IsTrue();

			int channels = this.builders.Length;
			var results = this.arrayPools.GetResultPool(channels).Rent();

			for (int i = 0; i < channels; i++)
			{
				results[i] = this.builders[i].Build();
			}

			this.Deinitialize();
			return new Channeled<TResult>(results);
		}
	}
}
