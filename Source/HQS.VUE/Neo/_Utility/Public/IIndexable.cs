﻿namespace HQS.VUE.Neo;

public interface IIndexable
{
	int Index { get; }
}
