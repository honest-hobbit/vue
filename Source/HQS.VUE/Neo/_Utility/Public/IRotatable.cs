﻿namespace HQS.VUE.Neo;

public interface IRotatable<T>
{
	// rotates 90 degrees clockwise around the X axis when looking from positive X towards negative X
	T RotateX();

	// rotates 90 degrees clockwise around the Y axis when looking from positive Y towards negative Y
	T RotateY();

	// rotates 90 degrees clockwise around the Z axis when looking from positive Z towards negative Z
	T RotateZ();
}
