﻿namespace HQS.VUE.Neo;

public readonly struct ArrayPoolControls
{
	internal ArrayPoolControls(IPoolControls pool, int length)
	{
		Debug.Assert(pool != null);
		Debug.Assert(length > 0);

		this.Pool = pool;
		this.Length = length;
	}

	public IPoolControls Pool { get; }

	public int Length { get; }
}
