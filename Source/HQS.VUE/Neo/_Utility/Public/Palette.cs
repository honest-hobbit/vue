﻿namespace HQS.VUE.Neo;

public class Palette<T> : IReadOnlyList<T>
{
	private readonly FastList<T> values;

	public Palette(int initialCapacity, int maxCapacity)
	{
		Ensure.That(maxCapacity, nameof(maxCapacity)).IsGte(0);
		Ensure.That(initialCapacity, nameof(initialCapacity)).IsInRange(0, maxCapacity);

		this.values = new FastList<T>(initialCapacity, maxCapacity);
	}

	public int MaxCapacity => this.values.MaxCapacity;

	public int Capacity => this.values.Array.Length;

	/// <inheritdoc />
	public int Count => this.values.Count;

	/// <inheritdoc />
	public T this[int index]
	{
		get => this.values[index];
		set => this.values[index] = value;
	}

	public void Add(T value) => this.values.Add(value);

	public void RemoveLast(int count)
	{
		Ensure.That(count, nameof(count)).IsInRange(0, count);

		if (count == 0) { return; }

		var span = new Span<T>(this.values.Array, this.values.Count - count, count);
		span.Clear();
		this.values.SetCount(this.values.Count - count);
	}

	public void Clear() => this.RemoveLast(this.values.Count);

	public void EnsureCapacity(int capacity) => this.values.EnsureCapacity(capacity);

	public ArraySegment<T>.Enumerator GetEnumerator() => this.values.GetEnumerator();

	/// <inheritdoc />
	IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
