﻿namespace HQS.VUE.Neo;

public class WedgeDefinitionSerDes : CompositeSerDes<
	WedgeType, WedgeSurfaces, WedgeInteriorSurfaces, WedgeCorners, WedgeCorners, WedgeCorners, WedgeDefinition>
{
	private WedgeDefinitionSerDes()
		: base(
			WedgeTypeSerDes.Instance,
			WedgeSurfacesSerDes.Instance,
			WedgeInteriorSurfacesSerDes.Instance,
			WedgeCornersSerDes.Instance,
			WedgeCornersSerDes.Instance,
			WedgeCornersSerDes.Instance)
	{
	}

	public static ISerDes<WedgeDefinition> Instance { get; } = new WedgeDefinitionSerDes();

	/// <inheritdoc />
	protected override WedgeDefinition ComposeValue(
		WedgeType type,
		WedgeSurfaces surfaces,
		WedgeInteriorSurfaces enclosedSurfaces,
		WedgeCorners filledCorners,
		WedgeCorners part1Corners,
		WedgeCorners part2Corners) => new WedgeDefinition()
		{
			Type = type,
			Surfaces = surfaces,
			EnclosedSurfaces = enclosedSurfaces,
			FilledCorners = filledCorners,
			Part1Corners = part1Corners,
			Part2Corners = part2Corners,
		};

	/// <inheritdoc />
	protected override void DecomposeValue(
		WedgeDefinition definition,
		out WedgeType type,
		out WedgeSurfaces surfaces,
		out WedgeInteriorSurfaces enclosedSurfaces,
		out WedgeCorners filledCorners,
		out WedgeCorners part1Corners,
		out WedgeCorners part2Corners)
	{
		type = definition.Type;
		surfaces = definition.Surfaces;
		enclosedSurfaces = definition.EnclosedSurfaces;
		filledCorners = definition.FilledCorners;
		part1Corners = definition.Part1Corners;
		part2Corners = definition.Part2Corners;
	}
}
