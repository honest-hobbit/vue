﻿namespace HQS.VUE.Neo;

public class WedgePlaneSerDes : CompositeSerDes<byte, WedgePlane>
{
	private WedgePlaneSerDes()
		: base(SerDes.OfByte)
	{
	}

	public static ISerDes<WedgePlane> Instance { get; } = new WedgePlaneSerDes();

	/// <inheritdoc />
	protected override WedgePlane ComposeValue(byte value) => new WedgePlane(value);

	/// <inheritdoc />
	protected override void DecomposeValue(WedgePlane plane, out byte part) => part = plane.Value;
}
