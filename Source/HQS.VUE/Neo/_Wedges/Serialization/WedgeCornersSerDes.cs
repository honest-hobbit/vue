﻿namespace HQS.VUE.Neo;

public class WedgeCornersSerDes : CompositeSerDes<byte, WedgeCorners>
{
	private WedgeCornersSerDes()
		: base(SerDes.OfByte)
	{
	}

	public static ISerDes<WedgeCorners> Instance { get; } = new WedgeCornersSerDes();

	/// <inheritdoc />
	protected override WedgeCorners ComposeValue(byte part) => (WedgeCorners)part;

	/// <inheritdoc />
	protected override void DecomposeValue(WedgeCorners value, out byte part) => part = (byte)value;
}
