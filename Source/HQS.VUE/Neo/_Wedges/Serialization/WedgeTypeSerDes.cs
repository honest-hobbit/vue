﻿namespace HQS.VUE.Neo;

public class WedgeTypeSerDes : CompositeSerDes<byte, WedgeType>
{
	private WedgeTypeSerDes()
		: base(SerDes.OfByte)
	{
	}

	public static ISerDes<WedgeType> Instance { get; } = new WedgeTypeSerDes();

	/// <inheritdoc />
	protected override WedgeType ComposeValue(byte part) => (WedgeType)part;

	/// <inheritdoc />
	protected override void DecomposeValue(WedgeType value, out byte part) => part = (byte)value;
}
