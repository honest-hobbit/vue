﻿namespace HQS.VUE.Neo;

public class WedgeSurfacesSerDes : CompositeSerDes<
	WedgePlane, WedgePlane, WedgePlane, WedgeInteriorSurfaces, WedgeSurfaces>
{
	public WedgeSurfacesSerDes()
		: base(
			WedgePlaneSerDes.Instance,
			WedgePlaneSerDes.Instance,
			WedgePlaneSerDes.Instance,
			WedgeInteriorSurfacesSerDes.Instance)
	{
	}

	public static ISerDes<WedgeSurfaces> Instance { get; } = new WedgeSurfacesSerDes();

	protected override WedgeSurfaces ComposeValue(
		WedgePlane faceX,
		WedgePlane faceY,
		WedgePlane faceZ,
		WedgeInteriorSurfaces interior) => new WedgeSurfaces
		{
			FaceX = faceX,
			FaceY = faceY,
			FaceZ = faceZ,
			Interior = interior,
		};

	protected override void DecomposeValue(
		WedgeSurfaces surfaces,
		out WedgePlane faceX,
		out WedgePlane faceY,
		out WedgePlane faceZ,
		out WedgeInteriorSurfaces interior)
	{
		faceX = surfaces.FaceX;
		faceY = surfaces.FaceY;
		faceZ = surfaces.FaceZ;
		interior = surfaces.Interior;
	}
}
