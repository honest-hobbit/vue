﻿namespace HQS.VUE.Neo;

public class WedgeInteriorSurfacesSerDes : CompositeSerDes<
	WedgePlane, WedgePlane, WedgePlane, WedgePlane, WedgePlane, WedgePlane, WedgeCorners, WedgeCorners,
	WedgeInteriorSurfaces>
{
	public WedgeInteriorSurfacesSerDes()
		: base(
			WedgePlaneSerDes.Instance,
			WedgePlaneSerDes.Instance,
			WedgePlaneSerDes.Instance,
			WedgePlaneSerDes.Instance,
			WedgePlaneSerDes.Instance,
			WedgePlaneSerDes.Instance,
			WedgeCornersSerDes.Instance,
			WedgeCornersSerDes.Instance)
	{
	}

	public static ISerDes<WedgeInteriorSurfaces> Instance { get; } = new WedgeInteriorSurfacesSerDes();

	protected override WedgeInteriorSurfaces ComposeValue(
		WedgePlane edgeXAscending,
		WedgePlane edgeXDescending,
		WedgePlane edgeYAscending,
		WedgePlane edgeYDescending,
		WedgePlane edgeZAscending,
		WedgePlane edgeZDescending,
		WedgeCorners cornerWideBot,
		WedgeCorners cornerWideTop) => new WedgeInteriorSurfaces
		{
			EdgeXAscending = edgeXAscending,
			EdgeXDescending = edgeXDescending,
			EdgeYAscending = edgeYAscending,
			EdgeYDescending = edgeYDescending,
			EdgeZAscending = edgeZAscending,
			EdgeZDescending = edgeZDescending,
			CornerWideBot = cornerWideBot,
			CornerWideTop = cornerWideTop,
		};

	protected override void DecomposeValue(
		WedgeInteriorSurfaces surfaces,
		out WedgePlane edgeXAscending,
		out WedgePlane edgeXDescending,
		out WedgePlane edgeYAscending,
		out WedgePlane edgeYDescending,
		out WedgePlane edgeZAscending,
		out WedgePlane edgeZDescending,
		out WedgeCorners cornerWideBot,
		out WedgeCorners cornerWideTop)
	{
		edgeXAscending = surfaces.EdgeXAscending;
		edgeXDescending = surfaces.EdgeXDescending;
		edgeYAscending = surfaces.EdgeYAscending;
		edgeYDescending = surfaces.EdgeYDescending;
		edgeZAscending = surfaces.EdgeZAscending;
		edgeZDescending = surfaces.EdgeZDescending;
		cornerWideBot = surfaces.CornerWideBot;
		cornerWideTop = surfaces.CornerWideTop;
	}
}
