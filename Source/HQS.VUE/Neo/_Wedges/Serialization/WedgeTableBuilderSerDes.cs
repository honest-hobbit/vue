﻿namespace HQS.VUE.Neo;

public class WedgeTableBuilderSerDes : ISerDes<WedgeTableBuilder>, IInlineSerDes<WedgeTableBuilder>
{
	private static readonly ArraySerDes<WedgeDefinition> DefinitionsSerDes =
		new ArraySerDes<WedgeDefinition>(WedgeDefinitionSerDes.Instance, SerializeCount.AsUnsignedVarint);

	private static readonly NullableObjectSerDes<Bit256[]> CollidesWithSerDes =
		new NullableObjectSerDes<Bit256[]>(
			new ArraySerDes<Bit256>(Bit256SerDes.Instance, SerializeCount.AsUnsignedVarint));

	private static readonly NullableObjectSerDes<WedgeIndex[]> IndicesSerDes =
		new NullableObjectSerDes<WedgeIndex[]>(
			new ArraySerDes<WedgeIndex>(WedgeIndexSerDes.Instance, SerializeCount.AsUnsignedVarint));

	private WedgeTableBuilderSerDes()
	{
	}

	public static WedgeTableBuilderSerDes Instance { get; } = new WedgeTableBuilderSerDes();

	public void Serialize(WedgeTableBuilder builder, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(builder, writeByte);
		Ensure.That(builder.Definition, $"{nameof(builder)}.{nameof(builder.Definition)}").IsNotNull();

		DefinitionsSerDes.Serialize(builder.Definition, writeByte);
		CollidesWithSerDes.Serialize(builder.CollidesWith, writeByte);
		IndicesSerDes.Serialize(builder.WedgeByFilledCorners, writeByte);
		IndicesSerDes.Serialize(builder.Part1, writeByte);
		IndicesSerDes.Serialize(builder.Part2, writeByte);
		IndicesSerDes.Serialize(builder.MirrorX, writeByte);
		IndicesSerDes.Serialize(builder.MirrorY, writeByte);
		IndicesSerDes.Serialize(builder.MirrorZ, writeByte);
		IndicesSerDes.Serialize(builder.RotateX90, writeByte);
		IndicesSerDes.Serialize(builder.RotateY90, writeByte);
		IndicesSerDes.Serialize(builder.RotateZ90, writeByte);
		IndicesSerDes.Serialize(builder.RotateX180, writeByte);
		IndicesSerDes.Serialize(builder.RotateY180, writeByte);
		IndicesSerDes.Serialize(builder.RotateZ180, writeByte);
		IndicesSerDes.Serialize(builder.RotateX270, writeByte);
		IndicesSerDes.Serialize(builder.RotateY270, writeByte);
		IndicesSerDes.Serialize(builder.RotateZ270, writeByte);
		IndicesSerDes.Serialize(builder.CombineWedges, writeByte);
	}

	public WedgeTableBuilder Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		var result = new WedgeTableBuilder();
		this.DeserializeInline(readByte, result);
		return result;
	}

	public void DeserializeInline(Func<byte> readByte, WedgeTableBuilder result)
	{
		IInlineDeserializerContracts.DeserializeInline(readByte, result);

		result.Definition = DefinitionsSerDes.Deserialize(readByte);
		result.CollidesWith = CollidesWithSerDes.Deserialize(readByte);
		result.WedgeByFilledCorners = IndicesSerDes.Deserialize(readByte);
		result.Part1 = IndicesSerDes.Deserialize(readByte);
		result.Part2 = IndicesSerDes.Deserialize(readByte);
		result.MirrorX = IndicesSerDes.Deserialize(readByte);
		result.MirrorY = IndicesSerDes.Deserialize(readByte);
		result.MirrorZ = IndicesSerDes.Deserialize(readByte);
		result.RotateX90 = IndicesSerDes.Deserialize(readByte);
		result.RotateY90 = IndicesSerDes.Deserialize(readByte);
		result.RotateZ90 = IndicesSerDes.Deserialize(readByte);
		result.RotateX180 = IndicesSerDes.Deserialize(readByte);
		result.RotateY180 = IndicesSerDes.Deserialize(readByte);
		result.RotateZ180 = IndicesSerDes.Deserialize(readByte);
		result.RotateX270 = IndicesSerDes.Deserialize(readByte);
		result.RotateY270 = IndicesSerDes.Deserialize(readByte);
		result.RotateZ270 = IndicesSerDes.Deserialize(readByte);
		result.CombineWedges = IndicesSerDes.Deserialize(readByte);
	}
}
