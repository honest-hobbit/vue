﻿namespace HQS.VUE.Neo;

public class WedgeIndexSerDes : CompositeSerDes<byte, WedgeIndex>
{
	private WedgeIndexSerDes()
		: base(SerDes.OfByte)
	{
	}

	public static ISerDes<WedgeIndex> Instance { get; } = new WedgeIndexSerDes();

	/// <inheritdoc />
	protected override WedgeIndex ComposeValue(byte part) => new WedgeIndex(part);

	/// <inheritdoc />
	protected override void DecomposeValue(WedgeIndex value, out byte part) => part = value.Index;
}
