﻿namespace HQS.VUE.Neo;

public class FaceTrianglesSerDes : CompositeSerDes<byte, QuadParts>
{
	private FaceTrianglesSerDes()
		: base(SerDes.OfByte)
	{
	}

	public static ISerDes<QuadParts> Instance { get; } = new FaceTrianglesSerDes();

	/// <inheritdoc />
	protected override QuadParts ComposeValue(byte part) => (QuadParts)part;

	/// <inheritdoc />
	protected override void DecomposeValue(QuadParts value, out byte part) => part = (byte)value;
}
