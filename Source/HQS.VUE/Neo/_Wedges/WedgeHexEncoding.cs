﻿namespace HQS.VUE.Neo;

public static class WedgeHexEncoding
{
	public static int FaceTrianglesStringLength => 1;

	public static int WedgeCornersStringLength => 2;

	public static int WedgePartsStringLength => (FaceTrianglesStringLength * 18) + (WedgeCornersStringLength * 5);

	public static char ToChar(QuadParts value)
	{
		value &= QuadParts.All;
		return HexUtility.ToCharSingle((byte)value);
	}

	public static void ToChar(WedgeCorners value, Span<char> result)
	{
		value &= WedgeCorners.All;
		HexUtility.ToChar((byte)value, result);
	}

	public static string ToString(WedgeDefinition value)
	{
		return string.Create(WedgePartsStringLength, value, (span, parts) =>
		{
			span[0] = ToChar(parts.Surfaces.FaceNegX);
			span[1] = ToChar(parts.Surfaces.FacePosX);
			span[2] = ToChar(parts.Surfaces.FaceNegY);
			span[3] = ToChar(parts.Surfaces.FacePosY);
			span[4] = ToChar(parts.Surfaces.FaceNegZ);
			span[5] = ToChar(parts.Surfaces.FacePosZ);

			span[6] = ToChar(parts.Surfaces.EdgeXNegYNegZ);
			span[7] = ToChar(parts.Surfaces.EdgeXNegYPosZ);
			span[8] = ToChar(parts.Surfaces.EdgeXPosYNegZ);
			span[9] = ToChar(parts.Surfaces.EdgeXPosYPosZ);

			span[10] = ToChar(parts.Surfaces.EdgeYNegXNegZ);
			span[11] = ToChar(parts.Surfaces.EdgeYNegXPosZ);
			span[12] = ToChar(parts.Surfaces.EdgeYPosXNegZ);
			span[13] = ToChar(parts.Surfaces.EdgeYPosXPosZ);

			span[14] = ToChar(parts.Surfaces.EdgeZNegXNegY);
			span[15] = ToChar(parts.Surfaces.EdgeZNegXPosY);
			span[16] = ToChar(parts.Surfaces.EdgeZPosXNegY);
			span[17] = ToChar(parts.Surfaces.EdgeZPosXPosY);

			ToChar(parts.Surfaces.CornerWideBot, span.Slice(18, 2));
			ToChar(parts.Surfaces.CornerWideTop, span.Slice(20, 2));

			ToChar(parts.FilledCorners, span.Slice(22, 2));

			ToChar(parts.Part1Corners, span.Slice(24, 2));
			ToChar(parts.Part2Corners, span.Slice(26, 2));
		});
	}

	public static QuadParts ToFaceTriangles(char value)
	{
		return (QuadParts)HexUtility.ToByte(value);
	}

	public static WedgeCorners ToWedgeCorners(ReadOnlySpan<char> value)
	{
		return (WedgeCorners)HexUtility.ToByte(value);
	}

	public static WedgeDefinition ToWedgeParts(string value)
	{
		if (value.IsNullOrWhiteSpace())
		{
			return default;
		}

		var span = value.AsSpan();
		return new WedgeDefinition()
		{
			Surfaces = new WedgeSurfaces()
			{
				FaceNegX = ToFaceTriangles(value[0]),
				FacePosX = ToFaceTriangles(value[1]),
				FaceNegY = ToFaceTriangles(value[2]),
				FacePosY = ToFaceTriangles(value[3]),
				FaceNegZ = ToFaceTriangles(value[4]),
				FacePosZ = ToFaceTriangles(value[5]),

				EdgeXNegYNegZ = ToFaceTriangles(value[6]),
				EdgeXNegYPosZ = ToFaceTriangles(value[7]),
				EdgeXPosYNegZ = ToFaceTriangles(value[8]),
				EdgeXPosYPosZ = ToFaceTriangles(value[9]),

				EdgeYNegXNegZ = ToFaceTriangles(value[10]),
				EdgeYNegXPosZ = ToFaceTriangles(value[11]),
				EdgeYPosXNegZ = ToFaceTriangles(value[12]),
				EdgeYPosXPosZ = ToFaceTriangles(value[13]),

				EdgeZNegXNegY = ToFaceTriangles(value[14]),
				EdgeZNegXPosY = ToFaceTriangles(value[15]),
				EdgeZPosXNegY = ToFaceTriangles(value[16]),
				EdgeZPosXPosY = ToFaceTriangles(value[17]),

				CornerWideBot = ToWedgeCorners(span.Slice(18, 2)),
				CornerWideTop = ToWedgeCorners(span.Slice(20, 2)),
			},

			FilledCorners = ToWedgeCorners(span.Slice(22, 2)),

			Part1Corners = ToWedgeCorners(span.Slice(24, 2)),
			Part2Corners = ToWedgeCorners(span.Slice(26, 2)),
		};
	}
}
