﻿using static HQS.VUE.Neo.WedgeCorners;

namespace HQS.VUE.Neo;

public static class WedgePrototypes
{
	public static WedgeDefinition Empty => WedgeDefinition.Empty;

	public static WedgeDefinition Cube => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.Cube,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.All,
			FacePosX = QuadParts.All,
			FaceNegY = QuadParts.All,
			FacePosY = QuadParts.All,
			FaceNegZ = QuadParts.All,
			FacePosZ = QuadParts.All,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.All,
			EdgeXNegYPosZ = QuadParts.All,
			EdgeXPosYNegZ = QuadParts.All,
			EdgeXPosYPosZ = QuadParts.All,

			EdgeYNegXNegZ = QuadParts.All,
			EdgeYNegXPosZ = QuadParts.All,
			EdgeYPosXNegZ = QuadParts.All,
			EdgeYPosXPosZ = QuadParts.All,

			EdgeZNegXNegY = QuadParts.All,
			EdgeZNegXPosY = QuadParts.All,
			EdgeZPosXNegY = QuadParts.All,
			EdgeZPosXPosY = QuadParts.All,

			CornerWideBot = All,
			CornerWideTop = All,
		},

		FilledCorners = All,
	});

	public static WedgeDefinition Edge => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.Edge,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.BottomRight,
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.All,
			FaceNegZ = QuadParts.All,

			EdgeXPosYPosZ = QuadParts.All,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.All,
			EdgeXNegYPosZ = QuadParts.Bottom,
			EdgeXPosYNegZ = QuadParts.Bottom,

			EdgeYNegXNegZ = QuadParts.BottomRight,
			EdgeYNegXPosZ = QuadParts.BottomRight,
			EdgeYPosXNegZ = QuadParts.BottomLeft,
			EdgeYPosXPosZ = QuadParts.BottomLeft,

			EdgeZNegXNegY = QuadParts.BottomRight,
			EdgeZNegXPosY = QuadParts.BottomRight,
			EdgeZPosXNegY = QuadParts.BottomLeft,
			EdgeZPosXPosY = QuadParts.BottomLeft,

			CornerWideBot = NNN.Add(NPP).Add(PNN).Add(PPP),
		},

		FilledCorners = All.Remove(PPP).Remove(NPP),
	});

	public static WedgeDefinition Triangle => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.Triangle,

		Surfaces = new WedgeSurfaces
		{
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.BottomLeft,
			FaceNegZ = QuadParts.BottomRight,

			CornerWideBot = NPP,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			CornerWideBot = PNN,
		},

		FilledCorners = PNN.Add(PPN).Add(NNN).Add(PNP),
	});

	public static WedgeDefinition TriangleInverse => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.TriangleInverse,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.BottomRight,
			FacePosX = QuadParts.All,
			FaceNegY = QuadParts.All,
			FacePosY = QuadParts.TopLeft,
			FaceNegZ = QuadParts.All,
			FacePosZ = QuadParts.BottomLeft,

			CornerWideTop = NPP,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.All,
			EdgeXNegYPosZ = QuadParts.BottomLeft,
			EdgeXPosYNegZ = QuadParts.BottomRight,
			EdgeXPosYPosZ = QuadParts.All,

			EdgeYNegXNegZ = QuadParts.BottomRight,
			EdgeYNegXPosZ = QuadParts.All,
			EdgeYPosXNegZ = QuadParts.All,
			EdgeYPosXPosZ = QuadParts.BottomLeft,

			EdgeZNegXNegY = QuadParts.BottomRight,
			EdgeZNegXPosY = QuadParts.All,
			EdgeZPosXNegY = QuadParts.All,
			EdgeZPosXPosY = QuadParts.BottomLeft,

			CornerWideBot = NNN.Add(NPN).Add(NPP).Add(PNN).Add(PNP).Add(PPP),
			CornerWideTop = NNP.Add(PNN).Add(PPN),
		},

		FilledCorners = All.Remove(NPP),
	});

	public static WedgeDefinition TriangleInverseClipped => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.TriangleInverseClipped,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.BottomRight,
			FacePosX = QuadParts.TopRight,
			FaceNegY = QuadParts.TopRight,
			FacePosY = QuadParts.TopLeft,
			FaceNegZ = QuadParts.TopLeft,
			FacePosZ = QuadParts.BottomLeft,

			CornerWideBot = PNN,
			CornerWideTop = NPP,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.All,
			EdgeXPosYPosZ = QuadParts.All,

			EdgeYNegXPosZ = QuadParts.All,
			EdgeYPosXNegZ = QuadParts.All,

			EdgeZNegXPosY = QuadParts.All,
			EdgeZPosXNegY = QuadParts.All,

			CornerWideBot = NPP,
			CornerWideTop = PNN,
		},

		FilledCorners = All.Remove(PNN).Remove(NPP),
	});

	public static WedgeDefinition Square => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.Square,

		Surfaces = new WedgeSurfaces
		{
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.All,
			FaceNegZ = QuadParts.BottomRight,

			EdgeZNegXPosY = QuadParts.BottomRight,
			EdgeXPosYPosZ = QuadParts.BottomLeft,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.BottomRight,
			EdgeXNegYPosZ = QuadParts.Bottom,
			EdgeXPosYNegZ = QuadParts.Bottom,

			EdgeYNegXNegZ = QuadParts.BottomRight,
			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeYPosXPosZ = QuadParts.BottomLeft,

			EdgeZNegXNegY = QuadParts.Bottom,
			EdgeZPosXNegY = QuadParts.BottomLeft,
			EdgeZPosXPosY = QuadParts.Bottom,

			CornerWideBot = NPP.Add(PNN),
		},

		FilledCorners = All.Remove(NPN).Remove(NPP).Remove(PPP),
	});

	public static WedgeDefinition SquareClipped => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.SquareClipped,

		Surfaces = new WedgeSurfaces
		{
			FaceNegY = QuadParts.TopRight,

			EdgeZNegXPosY = QuadParts.BottomRight,
			EdgeXPosYPosZ = QuadParts.BottomLeft,

			CornerWideBot = PNN,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.BottomRight,

			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,

			EdgeZPosXNegY = QuadParts.BottomLeft,

			CornerWideBot = NPP,
		},

		FilledCorners = All.Remove(NPN).Remove(NPP).Remove(PPP).Remove(PNN),
	});

	public static WedgeDefinition SquareSplit => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.SquareSplit,

		Surfaces = new WedgeSurfaces
		{
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.TopLeft,

			EdgeXPosYPosZ = QuadParts.BottomLeft,
			EdgeYNegXNegZ = QuadParts.BottomRight,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.BottomRight,

			EdgeYPosXPosZ = QuadParts.BottomLeft,

			EdgeZNegXNegY = QuadParts.Bottom,
			EdgeZPosXPosY = QuadParts.Bottom,
		},

		FilledCorners = PPN.Add(PNN).Add(PNP).Add(NNP),
	});

	public static WedgeDefinition SquareSplitAlt => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.SquareSplitAlt,

		Surfaces = new WedgeSurfaces
		{
			FaceNegY = QuadParts.BottomRight,
			FaceNegZ = QuadParts.BottomRight,

			EdgeYPosXPosZ = QuadParts.BottomLeft,
			EdgeZNegXPosY = QuadParts.BottomRight,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYPosZ = QuadParts.Bottom,
			EdgeXPosYNegZ = QuadParts.Bottom,

			EdgeYNegXNegZ = QuadParts.BottomRight,

			EdgeZPosXNegY = QuadParts.BottomLeft,
		},

		FilledCorners = PPN.Add(PNN).Add(NNN).Add(NNP),
	});

	public static WedgeDefinition Diagonal => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.Diagonal,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.BottomLeft,
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.All,
			FaceNegZ = QuadParts.BottomRight,
			FacePosZ = QuadParts.BottomRight,

			CornerWideTop = PPP.Add(NPN),
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.BottomRight,
			EdgeXNegYPosZ = QuadParts.BottomRight,
			EdgeXPosYNegZ = QuadParts.BottomLeft,
			EdgeXPosYPosZ = QuadParts.BottomLeft,

			EdgeYNegXNegZ = QuadParts.All,
			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeYPosXPosZ = QuadParts.All,

			EdgeZNegXNegY = QuadParts.BottomLeft,
			EdgeZNegXPosY = QuadParts.BottomRight,
			EdgeZPosXNegY = QuadParts.BottomLeft,
			EdgeZPosXPosY = QuadParts.BottomRight,

			CornerWideBot = NNP.Add(NPP).Add(PNN).Add(PPN),
			CornerWideTop = NNN.Add(PNP),
		},

		FilledCorners = All.Remove(PPP).Remove(NPN),
	});

	public static WedgeDefinition DiagonalClipped => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.DiagonalClipped,

		Surfaces = new WedgeSurfaces
		{
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.BottomLeft,
			FaceNegZ = QuadParts.BottomRight,

			CornerWideBot = NNP,
			CornerWideTop = NPN.Add(PPP),
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.Right,
			EdgeXNegYPosZ = QuadParts.BottomRight,
			EdgeXPosYNegZ = QuadParts.BottomLeft,
			EdgeXPosYPosZ = QuadParts.Left,

			EdgeYNegXNegZ = QuadParts.TopRight,
			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeYPosXPosZ = QuadParts.TopLeft,

			EdgeZNegXNegY = QuadParts.BottomLeft,
			EdgeZNegXPosY = QuadParts.Right,
			EdgeZPosXNegY = QuadParts.Left,
			EdgeZPosXPosY = QuadParts.BottomRight,

			CornerWideBot = NPP.Add(PNN).Add(PPN),
			CornerWideTop = NNN.Add(PNP),
		},

		FilledCorners = All.Remove(PPP).Remove(NPN).Remove(NNP),
	});

	public static WedgeDefinition DiagonalSplit => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.DiagonalSplit,

		Surfaces = new WedgeSurfaces
		{
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.TopLeft,
			FacePosZ = QuadParts.BottomRight,

			EdgeYNegXNegZ = QuadParts.All,
			CornerWideTop = PPP,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.BottomRight,
			EdgeXPosYPosZ = QuadParts.BottomLeft,

			EdgeYPosXPosZ = QuadParts.All,

			EdgeZNegXNegY = QuadParts.BottomLeft,
			EdgeZPosXPosY = QuadParts.BottomRight,

			CornerWideTop = NNN,
		},

		FilledCorners = All.Remove(PPP).Remove(NPN).Remove(NNN),
	});

	public static WedgeDefinition Diamond => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.Diamond,

		Surfaces = new WedgeSurfaces
		{
			CornerWideBot = NNP.Add(PNN),
			CornerWideTop = NPN.Add(PPP),
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.Right,
			EdgeXNegYPosZ = QuadParts.Right,
			EdgeXPosYNegZ = QuadParts.Left,
			EdgeXPosYPosZ = QuadParts.Left,

			EdgeYNegXNegZ = QuadParts.Top,
			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeYPosXPosZ = QuadParts.Top,

			EdgeZNegXNegY = QuadParts.Left,
			EdgeZNegXPosY = QuadParts.Right,
			EdgeZPosXNegY = QuadParts.Left,
			EdgeZPosXPosY = QuadParts.Right,

			CornerWideBot = NPP.Add(PPN),
			CornerWideTop = NNN.Add(PNP),
		},

		FilledCorners = NNN.Add(NPP).Add(PNP).Add(PPN),
	});

	public static WedgeDefinition Face => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.Face,

		Surfaces = new WedgeSurfaces
		{
			FaceNegY = QuadParts.All,

			EdgeXPosYNegZ = QuadParts.Bottom,
			EdgeXPosYPosZ = QuadParts.Bottom,

			EdgeZNegXPosY = QuadParts.Bottom,
			EdgeZPosXPosY = QuadParts.Bottom,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.Bottom,
			EdgeXNegYPosZ = QuadParts.Bottom,

			EdgeYNegXNegZ = QuadParts.Bottom,
			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeYPosXPosZ = QuadParts.Bottom,

			EdgeZNegXNegY = QuadParts.Bottom,
			EdgeZPosXNegY = QuadParts.Bottom,
		},

		FilledCorners = NNN.Add(NNP).Add(PNN).Add(PNP),
	});

	public static WedgeDefinition FaceSplit => CopyFilledCornersToPart1Corners(new()
	{
		Type = WedgeType.FaceSplit,

		Surfaces = new WedgeSurfaces
		{
			FaceNegY = QuadParts.BottomLeft,

			EdgeXPosYNegZ = QuadParts.Bottom,
			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeZPosXPosY = QuadParts.Bottom,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeZNegXNegY = QuadParts.Bottom,
		},

		FilledCorners = NNN.Add(PNN).Add(PNP),
	});

	// this is an optimization case (composite of 2 cases)
	public static WedgeDefinition SquareInverse => new()
	{
		Type = WedgeType.SquareInverse,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.BottomRight,
			FacePosX = QuadParts.All,
			FaceNegY = QuadParts.All,
			FaceNegZ = QuadParts.All,
			FacePosZ = QuadParts.BottomLeft,

			EdgeZNegXPosY = QuadParts.TopLeft,
			EdgeXPosYPosZ = QuadParts.TopRight,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.All,
			EdgeXNegYPosZ = QuadParts.BottomLeft,
			EdgeXPosYNegZ = QuadParts.BottomRight,
			EdgeXPosYPosZ = QuadParts.BottomLeft,

			EdgeYNegXNegZ = QuadParts.BottomRight,
			EdgeYNegXPosZ = QuadParts.All.Remove(QuadParts.Top),
			EdgeYPosXNegZ = QuadParts.All.Remove(QuadParts.Top),
			EdgeYPosXPosZ = QuadParts.BottomLeft,

			EdgeZNegXNegY = QuadParts.BottomRight,
			EdgeZNegXPosY = QuadParts.BottomRight,
			EdgeZPosXNegY = QuadParts.All,
			EdgeZPosXPosY = QuadParts.BottomLeft,

			CornerWideBot = All.Remove(NNP).Remove(PPN),
		},

		////FilledCorners = NNN.Add(PNN).Add(PNP),
		Part1Corners = All.Remove(NNN).Remove(NPN).Remove(NPP),
		Part2Corners = All.Remove(PNP).Remove(PPP).Remove(NPP),
	};

	// this is an optimization case (composite of 2 cases)
	public static WedgeDefinition EdgeSplit => new()
	{
		Type = WedgeType.EdgeSplit,

		Surfaces = new WedgeSurfaces
		{
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.All,

			EdgeXPosYNegZ = QuadParts.Bottom,
			EdgeXPosYPosZ = QuadParts.BottomLeft,

			EdgeYNegXNegZ = QuadParts.Right,

			EdgeZNegXPosY = QuadParts.Bottom,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.BottomRight,
			EdgeXNegYPosZ = QuadParts.Bottom,

			EdgeYNegXNegZ = QuadParts.Bottom,
			EdgeYNegXPosZ = QuadParts.Bottom,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeYPosXPosZ = QuadParts.BottomLeft,

			EdgeZNegXNegY = QuadParts.Bottom,
			EdgeZPosXNegY = QuadParts.Bottom,
			EdgeZPosXPosY = QuadParts.Bottom,
		},

		////FilledCorners = All.Remove(PPP).Remove(NPP).Remove(NPN),
		Part1Corners = NNN.Add(NNP).Add(PNN).Add(PNP),
		Part2Corners = PNN.Add(PNP).Add(PPN),
	};

	// this is an optimization case (composite of 2 cases)
	public static WedgeDefinition EdgeSplitAlt => new()
	{
		Type = WedgeType.EdgeSplitAlt,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.BottomRight,
			FaceNegY = QuadParts.All,

			EdgeXPosYNegZ = QuadParts.Bottom,
			EdgeXPosYPosZ = QuadParts.BottomRight,

			EdgeYPosXNegZ = QuadParts.Left,

			EdgeZPosXPosY = QuadParts.Bottom,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			EdgeXNegYNegZ = QuadParts.BottomLeft,
			EdgeXNegYPosZ = QuadParts.Bottom,

			EdgeYNegXNegZ = QuadParts.Bottom,
			EdgeYNegXPosZ = QuadParts.BottomRight,
			EdgeYPosXNegZ = QuadParts.Bottom,
			EdgeYPosXPosZ = QuadParts.Bottom,

			EdgeZNegXNegY = QuadParts.Bottom,
			EdgeZNegXPosY = QuadParts.Bottom,
			EdgeZPosXNegY = QuadParts.Bottom,
		},

		////FilledCorners = All.Remove(PPP).Remove(NPP).Remove(PPN),
		Part1Corners = NNN.Add(NNP).Add(PNN).Add(PNP),
		Part2Corners = NNN.Add(NNP).Add(NPN),
	};

	// this is an optimization case (composite of 2 cases)
	public static WedgeDefinition TriangleDoubleAdjacent => new()
	{
		Type = WedgeType.TriangleDoubleAdjacent,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.BottomLeft,
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.All,
			FaceNegZ = QuadParts.BottomRight,
			FacePosZ = QuadParts.BottomRight,

			CornerWideBot = NPP.Add(PPN),
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			CornerWideBot = NNP.Add(PNN),
		},

		////FilledCorners = NNP.Add(PNN),
		Part1Corners = PNN.Add(PPN).Add(NNN).Add(PNP),
		Part2Corners = NNP.Add(NPP).Add(NNN).Add(PNP),
	};

	// this is an optimization case (composite of 2 cases)
	public static WedgeDefinition TriangleDoubleAcross => new()
	{
		Type = WedgeType.TriangleDoubleAcross,

		Surfaces = new WedgeSurfaces
		{
			FaceNegX = QuadParts.TopLeft,
			FacePosX = QuadParts.BottomLeft,
			FaceNegY = QuadParts.BottomLeft,
			FacePosY = QuadParts.BottomRight,
			FaceNegZ = QuadParts.BottomRight,
			FacePosZ = QuadParts.TopRight,

			CornerWideBot = NPP,
			CornerWideTop = PNN,
		},

		EnclosedSurfaces = new WedgeInteriorSurfaces
		{
			CornerWideBot = PNN,
			CornerWideTop = NPP,
		},

		////FilledCorners = NPP.Add(PNN),
		Part1Corners = PNN.Add(PPN).Add(NNN).Add(PNP),
		Part2Corners = NPP.Add(PPP).Add(NPN).Add(NNP),
	};

	public static IEnumerable<WedgeDefinition> EnumeratePrototypes()
	{
		yield return Empty;
		yield return Cube;
		yield return Edge;

		yield return Triangle;
		yield return TriangleInverse;
		yield return TriangleInverseClipped;

		yield return Square;
		yield return SquareClipped;
		yield return SquareSplit;
		yield return SquareSplitAlt;

		yield return Diagonal;
		yield return DiagonalClipped;
		yield return DiagonalSplit;

		yield return Diamond;

		yield return Face;
		yield return FaceSplit;

		yield return SquareInverse;
		////yield return EdgeSplit;
		////yield return EdgeSplitAlt;
		////yield return TriangleDoubleAdjacent;
		////yield return TriangleDoubleAcross;
	}

	public static IEnumerable<WedgeDefinition> EnumerateDistinctRotations() => EnumeratePrototypes()
		.SelectMany(parts => parts.EnumerateRotations()
		.Distinct(WedgeDefinition.SurfacesComparer));

	private static WedgeDefinition CopyFilledCornersToPart1Corners(WedgeDefinition wedge)
	{
		wedge.Part1Corners = wedge.FilledCorners;
		return wedge;
	}
}
