﻿namespace HQS.VUE.Neo;

public enum WedgeType : byte
{
	Empty,

	Cube,

	Edge,

	Triangle,

	TriangleInverse,

	TriangleInverseClipped,

	Square,

	SquareClipped,

	SquareSplit,

	SquareSplitAlt,

	Diagonal,

	DiagonalClipped,

	DiagonalSplit,

	Diamond,

	Face,

	FaceSplit,

	SquareInverse,

	EdgeSplit,

	EdgeSplitAlt,

	TriangleDoubleAdjacent,

	TriangleDoubleAcross,
}
