﻿namespace HQS.VUE.Neo;

public static class WedgeCornersExtensions
{
	private const int TableLength = 256;

	private static readonly WedgeCorners[] TableRotateX = new WedgeCorners[TableLength];

	private static readonly WedgeCorners[] TableRotateY = new WedgeCorners[TableLength];

	private static readonly WedgeCorners[] TableRotateZ = new WedgeCorners[TableLength];

	private static readonly WedgeCorners[] TableMirrorX = new WedgeCorners[TableLength];

	private static readonly WedgeCorners[] TableMirrorY = new WedgeCorners[TableLength];

	private static readonly WedgeCorners[] TableMirrorZ = new WedgeCorners[TableLength];

	static WedgeCornersExtensions()
	{
		for (int i = 0; i < TableLength; i++)
		{
			var corners = (WedgeCorners)i;
			TableRotateX[i] = GetRotateX(corners);
			TableRotateY[i] = GetRotateY(corners);
			TableRotateZ[i] = GetRotateZ(corners);
			TableMirrorX[i] = GetMirrorX(corners);
			TableMirrorY[i] = GetMirrorY(corners);
			TableMirrorZ[i] = GetMirrorZ(corners);
		}
	}

	public static bool Has(this WedgeCorners current, WedgeCorners flag) => (current & flag) == flag;

	public static bool HasAny(this WedgeCorners current, WedgeCorners flags) => (current & flags) != 0;

	public static WedgeCorners Select(this WedgeCorners current, WedgeCorners flag) => current & flag;

	public static WedgeCorners SelectAll(this WedgeCorners current) => current & WedgeCorners.All;

	public static WedgeCorners Add(this WedgeCorners current, WedgeCorners flags) => current | flags;

	public static WedgeCorners Remove(this WedgeCorners current, WedgeCorners flags) => current & ~flags;

	public static WedgeCorners Set(this WedgeCorners current, WedgeCorners flags, bool value) =>
		value ? current.Add(flags) : current.Remove(flags);

	// rotates 90 degrees clockwise around the X axis when looking from positive X towards negative X
	public static WedgeCorners RotateX(this WedgeCorners current) => TableRotateX[(int)current];

	// rotates 90 degrees clockwise around the Y axis when looking from positive Y towards negative Y
	public static WedgeCorners RotateY(this WedgeCorners current) => TableRotateY[(int)current];

	// rotates 90 degrees clockwise around the Z axis when looking from positive Z towards negative Z
	public static WedgeCorners RotateZ(this WedgeCorners current) => TableRotateZ[(int)current];

	public static WedgeCorners MirrorX(this WedgeCorners current) => TableMirrorX[(int)current];

	public static WedgeCorners MirrorY(this WedgeCorners current) => TableMirrorY[(int)current];

	public static WedgeCorners MirrorZ(this WedgeCorners current) => TableMirrorZ[(int)current];

	private static WedgeCorners GetRotateX(WedgeCorners current)
	{
		WedgeCorners result = default;
		if (current.Has(WedgeCorners.NNN)) { result |= WedgeCorners.NPN; }
		if (current.Has(WedgeCorners.NPN)) { result |= WedgeCorners.NPP; }
		if (current.Has(WedgeCorners.NPP)) { result |= WedgeCorners.NNP; }
		if (current.Has(WedgeCorners.NNP)) { result |= WedgeCorners.NNN; }

		if (current.Has(WedgeCorners.PNN)) { result |= WedgeCorners.PPN; }
		if (current.Has(WedgeCorners.PPN)) { result |= WedgeCorners.PPP; }
		if (current.Has(WedgeCorners.PPP)) { result |= WedgeCorners.PNP; }
		if (current.Has(WedgeCorners.PNP)) { result |= WedgeCorners.PNN; }
		return result;
	}

	private static WedgeCorners GetRotateY(WedgeCorners current)
	{
		WedgeCorners result = default;
		if (current.Has(WedgeCorners.NNN)) { result |= WedgeCorners.NNP; }
		if (current.Has(WedgeCorners.NNP)) { result |= WedgeCorners.PNP; }
		if (current.Has(WedgeCorners.PNP)) { result |= WedgeCorners.PNN; }
		if (current.Has(WedgeCorners.PNN)) { result |= WedgeCorners.NNN; }

		if (current.Has(WedgeCorners.NPN)) { result |= WedgeCorners.NPP; }
		if (current.Has(WedgeCorners.NPP)) { result |= WedgeCorners.PPP; }
		if (current.Has(WedgeCorners.PPP)) { result |= WedgeCorners.PPN; }
		if (current.Has(WedgeCorners.PPN)) { result |= WedgeCorners.NPN; }
		return result;
	}

	private static WedgeCorners GetRotateZ(WedgeCorners current)
	{
		WedgeCorners result = default;
		if (current.Has(WedgeCorners.NNN)) { result |= WedgeCorners.PNN; }
		if (current.Has(WedgeCorners.PNN)) { result |= WedgeCorners.PPN; }
		if (current.Has(WedgeCorners.PPN)) { result |= WedgeCorners.NPN; }
		if (current.Has(WedgeCorners.NPN)) { result |= WedgeCorners.NNN; }

		if (current.Has(WedgeCorners.NNP)) { result |= WedgeCorners.PNP; }
		if (current.Has(WedgeCorners.PNP)) { result |= WedgeCorners.PPP; }
		if (current.Has(WedgeCorners.PPP)) { result |= WedgeCorners.NPP; }
		if (current.Has(WedgeCorners.NPP)) { result |= WedgeCorners.NNP; }
		return result;
	}

	private static WedgeCorners GetMirrorX(WedgeCorners current)
	{
		WedgeCorners result = default;
		if (current.Has(WedgeCorners.NNN)) { result |= WedgeCorners.PNN; }
		if (current.Has(WedgeCorners.NNP)) { result |= WedgeCorners.PNP; }
		if (current.Has(WedgeCorners.NPN)) { result |= WedgeCorners.PPN; }
		if (current.Has(WedgeCorners.NPP)) { result |= WedgeCorners.PPP; }

		if (current.Has(WedgeCorners.PNN)) { result |= WedgeCorners.NNN; }
		if (current.Has(WedgeCorners.PNP)) { result |= WedgeCorners.NNP; }
		if (current.Has(WedgeCorners.PPN)) { result |= WedgeCorners.NPN; }
		if (current.Has(WedgeCorners.PPP)) { result |= WedgeCorners.NPP; }
		return result;
	}

	private static WedgeCorners GetMirrorY(WedgeCorners current)
	{
		WedgeCorners result = default;
		if (current.Has(WedgeCorners.NNN)) { result |= WedgeCorners.NPN; }
		if (current.Has(WedgeCorners.NNP)) { result |= WedgeCorners.NPP; }
		if (current.Has(WedgeCorners.PNN)) { result |= WedgeCorners.PPN; }
		if (current.Has(WedgeCorners.PNP)) { result |= WedgeCorners.PPP; }

		if (current.Has(WedgeCorners.NPN)) { result |= WedgeCorners.NNN; }
		if (current.Has(WedgeCorners.NPP)) { result |= WedgeCorners.NNP; }
		if (current.Has(WedgeCorners.PPN)) { result |= WedgeCorners.PNN; }
		if (current.Has(WedgeCorners.PPP)) { result |= WedgeCorners.PNP; }
		return result;
	}

	private static WedgeCorners GetMirrorZ(WedgeCorners current)
	{
		WedgeCorners result = default;
		if (current.Has(WedgeCorners.NNN)) { result |= WedgeCorners.NNP; }
		if (current.Has(WedgeCorners.NPN)) { result |= WedgeCorners.NPP; }
		if (current.Has(WedgeCorners.PNN)) { result |= WedgeCorners.PNP; }
		if (current.Has(WedgeCorners.PPN)) { result |= WedgeCorners.PPP; }

		if (current.Has(WedgeCorners.NNP)) { result |= WedgeCorners.NNN; }
		if (current.Has(WedgeCorners.NPP)) { result |= WedgeCorners.NPN; }
		if (current.Has(WedgeCorners.PNP)) { result |= WedgeCorners.PNN; }
		if (current.Has(WedgeCorners.PPP)) { result |= WedgeCorners.PPN; }
		return result;
	}
}
