﻿namespace HQS.VUE.Neo;

public static class QuadPartsExtensions
{
	public static bool Has(this QuadParts current, QuadParts flag) => (current & flag) == flag;

	public static bool HasAny(this QuadParts current, QuadParts flags) => (current & flags) != 0;

	public static QuadParts Select(this QuadParts current, QuadParts flag) => current & flag;

	public static QuadParts SelectAll(this QuadParts current) => current & QuadParts.All;

	public static QuadParts Add(this QuadParts current, QuadParts flags) => current | flags;

	public static QuadParts Remove(this QuadParts current, QuadParts flags) => current & ~flags;

	public static QuadParts Set(this QuadParts current, QuadParts flags, bool value) =>
		value ? current.Add(flags) : current.Remove(flags);

	public static QuadParts Next(this QuadParts current) => current switch
	{
		QuadParts.None => QuadParts.Top,
		QuadParts.Top => QuadParts.Right,
		QuadParts.Right => QuadParts.Bottom,
		QuadParts.Bottom => QuadParts.Left,
		QuadParts.Left => QuadParts.None,
		_ => throw InvalidEnumArgument.CreateException(nameof(current), current),
	};

	// rotates 90 degrees clockwise
	public static QuadParts Rotate(this QuadParts current)
	{
		QuadParts result = default;
		result = current.Has(QuadParts.Top) ? result.Add(QuadParts.Right) : result;
		result = current.Has(QuadParts.Right) ? result.Add(QuadParts.Bottom) : result;
		result = current.Has(QuadParts.Bottom) ? result.Add(QuadParts.Left) : result;
		result = current.Has(QuadParts.Left) ? result.Add(QuadParts.Top) : result;
		return result;
	}
}
