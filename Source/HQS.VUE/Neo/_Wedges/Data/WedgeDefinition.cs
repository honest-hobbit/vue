﻿namespace HQS.VUE.Neo;

// For all of these FaceTriangles.Top is positive Y and QuadEdges.Ab
// and then goes clockwise to FaceTriangles.Right and QuadEdges.BC.
// The only exceptions are FaceNegY and FacePosY.
[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
public record struct WedgeDefinition : IRotatable<WedgeDefinition>
{
	public static IEqualityComparer<WedgeDefinition> FilledCornersComparer { get; } = new FilledCornersComparerImpl();

	public static IEqualityComparer<WedgeDefinition> SurfacesComparer { get; } = new SurfacesComparerImpl();

	public static WedgeDefinition Empty => default;

	public WedgeType Type;

	public WedgeSurfaces Surfaces;

	public WedgeInteriorSurfaces EnclosedSurfaces;

	public WedgeCorners FilledCorners;

	public WedgeCorners Part1Corners;

	public WedgeCorners Part2Corners;

	/// <inheritdoc />
	public WedgeDefinition RotateX() => new()
	{
		Type = this.Type,
		Surfaces = this.Surfaces.RotateX(),
		EnclosedSurfaces = this.EnclosedSurfaces.RotateX(),
		FilledCorners = this.FilledCorners.RotateX(),
		Part1Corners = this.Part1Corners.RotateX(),
		Part2Corners = this.Part2Corners.RotateX(),
	};

	/// <inheritdoc />
	public WedgeDefinition RotateY() => new()
	{
		Type = this.Type,
		Surfaces = this.Surfaces.RotateY(),
		EnclosedSurfaces = this.EnclosedSurfaces.RotateY(),
		FilledCorners = this.FilledCorners.RotateY(),
		Part1Corners = this.Part1Corners.RotateY(),
		Part2Corners = this.Part2Corners.RotateY(),
	};

	/// <inheritdoc />
	public WedgeDefinition RotateZ() => new()
	{
		Type = this.Type,
		Surfaces = this.Surfaces.RotateZ(),
		EnclosedSurfaces = this.EnclosedSurfaces.RotateZ(),
		FilledCorners = this.FilledCorners.RotateZ(),
		Part1Corners = this.Part1Corners.RotateZ(),
		Part2Corners = this.Part2Corners.RotateZ(),
	};

	private class FilledCornersComparerImpl : EqualityComparer<WedgeDefinition>
	{
		public override bool Equals(WedgeDefinition x, WedgeDefinition y) => x.FilledCorners == y.FilledCorners;

		public override int GetHashCode(WedgeDefinition value) => value.FilledCorners.GetHashCode();
	}

	private class SurfacesComparerImpl : EqualityComparer<WedgeDefinition>
	{
		public override bool Equals(WedgeDefinition x, WedgeDefinition y) => x.Surfaces == y.Surfaces;

		public override int GetHashCode(WedgeDefinition value) => value.Surfaces.GetHashCode();
	}
}
