﻿namespace HQS.VUE.Neo;

public record struct WedgeSurfaces : IRotatable<WedgeSurfaces>
{
	public static WedgeSurfaces Empty => default;

	public WedgePlane FaceX;

	public WedgePlane FaceY;

	public WedgePlane FaceZ;

	public WedgeInteriorSurfaces Interior;

	public QuadParts FaceNegX
	{
		get => this.FaceX.Lower;
		set => this.FaceX.Lower = value;
	}

	public QuadParts FacePosX
	{
		get => this.FaceX.Upper;
		set => this.FaceX.Upper = value;
	}

	// FaceTriangles.Top is EdgeXNegYPosZ
	public QuadParts FaceNegY
	{
		get => this.FaceY.Lower;
		set => this.FaceY.Lower = value;
	}

	// FaceTriangles.Top is EdgeXPosYNegZ
	public QuadParts FacePosY
	{
		get => this.FaceY.Upper;
		set => this.FaceY.Upper = value;
	}

	public QuadParts FaceNegZ
	{
		get => this.FaceZ.Lower;
		set => this.FaceZ.Lower = value;
	}

	public QuadParts FacePosZ
	{
		get => this.FaceZ.Upper;
		set => this.FaceZ.Upper = value;
	}

	public QuadParts EdgeXNegYNegZ
	{
		get => this.Interior.EdgeXNegYNegZ;
		set => this.Interior.EdgeXNegYNegZ = value;
	}

	public QuadParts EdgeXNegYPosZ
	{
		get => this.Interior.EdgeXNegYPosZ;
		set => this.Interior.EdgeXNegYPosZ = value;
	}

	public QuadParts EdgeXPosYNegZ
	{
		get => this.Interior.EdgeXPosYNegZ;
		set => this.Interior.EdgeXPosYNegZ = value;
	}

	public QuadParts EdgeXPosYPosZ
	{
		get => this.Interior.EdgeXPosYPosZ;
		set => this.Interior.EdgeXPosYPosZ = value;
	}

	public QuadParts EdgeYNegXNegZ
	{
		get => this.Interior.EdgeYNegXNegZ;
		set => this.Interior.EdgeYNegXNegZ = value;
	}

	public QuadParts EdgeYNegXPosZ
	{
		get => this.Interior.EdgeYNegXPosZ;
		set => this.Interior.EdgeYNegXPosZ = value;
	}

	public QuadParts EdgeYPosXNegZ
	{
		get => this.Interior.EdgeYPosXNegZ;
		set => this.Interior.EdgeYPosXNegZ = value;
	}

	public QuadParts EdgeYPosXPosZ
	{
		get => this.Interior.EdgeYPosXPosZ;
		set => this.Interior.EdgeYPosXPosZ = value;
	}

	public QuadParts EdgeZNegXNegY
	{
		get => this.Interior.EdgeZNegXNegY;
		set => this.Interior.EdgeZNegXNegY = value;
	}

	public QuadParts EdgeZNegXPosY
	{
		get => this.Interior.EdgeZNegXPosY;
		set => this.Interior.EdgeZNegXPosY = value;
	}

	public QuadParts EdgeZPosXNegY
	{
		get => this.Interior.EdgeZPosXNegY;
		set => this.Interior.EdgeZPosXNegY = value;
	}

	public QuadParts EdgeZPosXPosY
	{
		get => this.Interior.EdgeZPosXPosY;
		set => this.Interior.EdgeZPosXPosY = value;
	}

	public WedgeCorners CornerWideBot
	{
		get => this.Interior.CornerWideBot;
		set => this.Interior.CornerWideBot = value;
	}

	public WedgeCorners CornerWideTop
	{
		get => this.Interior.CornerWideTop;
		set => this.Interior.CornerWideTop = value;
	}

	public WedgeSurfaces Add(WedgeSurfaces other) => new WedgeSurfaces
	{
		FaceX = this.FaceX.Add(other.FaceX),
		FaceY = this.FaceY.Add(other.FaceY),
		FaceZ = this.FaceZ.Add(other.FaceZ),
		Interior = this.Interior.Add(other.Interior),
	};

	public WedgeSurfaces Remove(WedgeSurfaces other) => new WedgeSurfaces
	{
		FaceX = this.FaceX.Remove(other.FaceX),
		FaceY = this.FaceY.Remove(other.FaceY),
		FaceZ = this.FaceZ.Remove(other.FaceZ),
		Interior = this.Interior.Remove(other.Interior),
	};

	/// <inheritdoc />
	public WedgeSurfaces RotateX()
	{
		return new WedgeSurfaces()
		{
			FaceNegX = this.FaceNegX.Rotate().Rotate().Rotate(),
			FacePosX = this.FacePosX.Rotate(),
			FaceNegY = this.FacePosZ,
			FacePosY = this.FaceNegZ.Rotate().Rotate(),
			FaceNegZ = this.FaceNegY.Rotate().Rotate(),
			FacePosZ = this.FacePosY,
			Interior = this.Interior.RotateX(),
		};
	}

	/// <inheritdoc />
	public WedgeSurfaces RotateY()
	{
		return new WedgeSurfaces()
		{
			FaceNegX = this.FaceNegZ,
			FacePosX = this.FacePosZ,
			FaceNegY = this.FaceNegY.Rotate().Rotate().Rotate(),
			FacePosY = this.FacePosY.Rotate(),
			FaceNegZ = this.FacePosX,
			FacePosZ = this.FaceNegX,
			Interior = this.Interior.RotateY(),
		};
	}

	/// <inheritdoc />
	public WedgeSurfaces RotateZ()
	{
		return new WedgeSurfaces()
		{
			FaceNegX = this.FacePosY.Rotate(),
			FacePosX = this.FaceNegY.Rotate(),
			FaceNegY = this.FaceNegX.Rotate(),
			FacePosY = this.FacePosX.Rotate(),
			FaceNegZ = this.FaceNegZ.Rotate().Rotate().Rotate(),
			FacePosZ = this.FacePosZ.Rotate(),
			Interior = this.Interior.RotateZ(),
		};
	}
}
