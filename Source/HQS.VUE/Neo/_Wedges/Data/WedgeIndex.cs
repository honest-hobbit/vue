﻿namespace HQS.VUE.Neo;

public readonly record struct WedgeIndex
{
	public static readonly WedgeIndex Empty = default;

	public readonly byte Index;

	public WedgeIndex(byte index)
	{
		this.Index = index;
	}

	public static implicit operator byte(WedgeIndex value) => value.Index;

	public static implicit operator WedgeIndex(byte value) => new WedgeIndex(value);

	/// <inheritdoc />
	public override string ToString() => this.Index.ToString();
}
