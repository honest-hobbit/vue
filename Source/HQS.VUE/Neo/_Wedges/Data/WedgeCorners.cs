﻿namespace HQS.VUE.Neo;

[Flags]
public enum WedgeCorners : byte
{
	None = 0,

	NNN = 1,

	NNP = 1 << 1,

	NPN = 1 << 2,

	NPP = 1 << 3,

	PNN = 1 << 4,

	PNP = 1 << 5,

	PPN = 1 << 6,

	PPP = 1 << 7,

	All = 0b11111111,
}
