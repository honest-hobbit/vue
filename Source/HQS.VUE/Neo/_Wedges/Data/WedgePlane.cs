﻿namespace HQS.VUE.Neo;

public record struct WedgePlane
{
	internal const byte Mask = 0b_00001111;

	internal const int Shift = 4;

	public WedgePlane(QuadParts upper, QuadParts lower)
	{
		this.Value = (byte)((uint)upper << Shift | (uint)lower);
	}

	public WedgePlane(byte value)
	{
		this.Value = value;
	}

	public static WedgePlane Empty => default;

	public byte Value { get; private set; }

	public QuadParts Upper
	{
		get => (QuadParts)((uint)this.Value >> Shift);
		set => this.Value |= (byte)((uint)value << Shift);
	}

	public QuadParts Lower
	{
		get => (QuadParts)(this.Value & Mask);
		set => this.Value |= (byte)value;
	}

	public WedgePlane Add(WedgePlane other) => new WedgePlane((byte)(this.Value | other.Value));

	public WedgePlane Remove(WedgePlane other) => new WedgePlane((byte)(this.Value & ~other.Value));
}
