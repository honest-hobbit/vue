﻿namespace HQS.VUE.Neo;

[Flags]
public enum QuadParts : byte
{
	None = 0,

	Top = 1,

	Right = 1 << 1,

	Bottom = 1 << 2,

	Left = 1 << 3,

	TopLeft = Top | Left,

	TopRight = Top | Right,

	BottomRight = Bottom | Right,

	BottomLeft = Bottom | Left,

	TopBottom = Top | Bottom,

	LeftRight = Left | Right,

	TopThree = Top | Left | Right,

	RightThree = Right | Top | Bottom,

	BottomThree = Bottom | Left | Right,

	LeftThree = Left | Top | Bottom,

	All = Top | Right | Bottom | Left,
}
