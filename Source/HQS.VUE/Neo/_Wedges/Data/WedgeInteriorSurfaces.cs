﻿namespace HQS.VUE.Neo;

public record struct WedgeInteriorSurfaces : IRotatable<WedgeInteriorSurfaces>
{
	public static WedgeInteriorSurfaces Empty => default;

	public WedgePlane EdgeXAscending;

	public WedgePlane EdgeXDescending;

	public WedgePlane EdgeYAscending;

	public WedgePlane EdgeYDescending;

	public WedgePlane EdgeZAscending;

	public WedgePlane EdgeZDescending;

	public WedgeCorners CornerWideBot;

	public WedgeCorners CornerWideTop;

	public QuadParts EdgeXNegYNegZ
	{
		get => this.EdgeXAscending.Lower;
		set => this.EdgeXAscending.Lower = value;
	}

	public QuadParts EdgeXNegYPosZ
	{
		get => this.EdgeXDescending.Lower;
		set => this.EdgeXDescending.Lower = value;
	}

	public QuadParts EdgeXPosYNegZ
	{
		get => this.EdgeXDescending.Upper;
		set => this.EdgeXDescending.Upper = value;
	}

	public QuadParts EdgeXPosYPosZ
	{
		get => this.EdgeXAscending.Upper;
		set => this.EdgeXAscending.Upper = value;
	}

	public QuadParts EdgeYNegXNegZ
	{
		get => this.EdgeYAscending.Lower;
		set => this.EdgeYAscending.Lower = value;
	}

	public QuadParts EdgeYNegXPosZ
	{
		get => this.EdgeYDescending.Lower;
		set => this.EdgeYDescending.Lower = value;
	}

	public QuadParts EdgeYPosXNegZ
	{
		get => this.EdgeYDescending.Upper;
		set => this.EdgeYDescending.Upper = value;
	}

	public QuadParts EdgeYPosXPosZ
	{
		get => this.EdgeYAscending.Upper;
		set => this.EdgeYAscending.Upper = value;
	}

	public QuadParts EdgeZNegXNegY
	{
		get => this.EdgeZAscending.Lower;
		set => this.EdgeZAscending.Lower = value;
	}

	public QuadParts EdgeZNegXPosY
	{
		get => this.EdgeZDescending.Lower;
		set => this.EdgeZDescending.Lower = value;
	}

	public QuadParts EdgeZPosXNegY
	{
		get => this.EdgeZDescending.Upper;
		set => this.EdgeZDescending.Upper = value;
	}

	public QuadParts EdgeZPosXPosY
	{
		get => this.EdgeZAscending.Upper;
		set => this.EdgeZAscending.Upper = value;
	}

	public WedgeInteriorSurfaces Add(WedgeInteriorSurfaces other) => new WedgeInteriorSurfaces
	{
		EdgeXAscending = this.EdgeXAscending.Add(other.EdgeXAscending),
		EdgeXDescending = this.EdgeXDescending.Add(other.EdgeXDescending),

		EdgeYAscending = this.EdgeYAscending.Add(other.EdgeYAscending),
		EdgeYDescending = this.EdgeYDescending.Add(other.EdgeYDescending),

		EdgeZAscending = this.EdgeZAscending.Add(other.EdgeZAscending),
		EdgeZDescending = this.EdgeZDescending.Add(other.EdgeZDescending),

		CornerWideBot = this.CornerWideBot.Add(other.CornerWideBot),
		CornerWideTop = this.CornerWideTop.Add(other.CornerWideTop),
	};

	public WedgeInteriorSurfaces Remove(WedgeInteriorSurfaces other) => new WedgeInteriorSurfaces
	{
		EdgeXAscending = this.EdgeXAscending.Remove(other.EdgeXAscending),
		EdgeXDescending = this.EdgeXDescending.Remove(other.EdgeXDescending),

		EdgeYAscending = this.EdgeYAscending.Remove(other.EdgeYAscending),
		EdgeYDescending = this.EdgeYDescending.Remove(other.EdgeYDescending),

		EdgeZAscending = this.EdgeZAscending.Remove(other.EdgeZAscending),
		EdgeZDescending = this.EdgeZDescending.Remove(other.EdgeZDescending),

		CornerWideBot = this.CornerWideBot.Remove(other.CornerWideBot),
		CornerWideTop = this.CornerWideTop.Remove(other.CornerWideTop),
	};

	/// <inheritdoc />
	public WedgeInteriorSurfaces RotateX()
	{
		WedgeCorners wideBot = default;
		WedgeCorners wideTop = default;

		if (this.CornerWideBot.Has(WedgeCorners.NNN)) { wideTop |= WedgeCorners.NPN; }
		if (this.CornerWideBot.Has(WedgeCorners.NNP)) { wideBot |= WedgeCorners.NNN; }
		if (this.CornerWideBot.Has(WedgeCorners.NPN)) { wideBot |= WedgeCorners.NPP; }
		if (this.CornerWideBot.Has(WedgeCorners.NPP)) { wideTop |= WedgeCorners.NNP; }

		if (this.CornerWideBot.Has(WedgeCorners.PNN)) { wideTop |= WedgeCorners.PPN; }
		if (this.CornerWideBot.Has(WedgeCorners.PNP)) { wideBot |= WedgeCorners.PNN; }
		if (this.CornerWideBot.Has(WedgeCorners.PPN)) { wideBot |= WedgeCorners.PPP; }
		if (this.CornerWideBot.Has(WedgeCorners.PPP)) { wideTop |= WedgeCorners.PNP; }

		if (this.CornerWideTop.Has(WedgeCorners.NNN)) { wideBot |= WedgeCorners.NPN; }
		if (this.CornerWideTop.Has(WedgeCorners.NNP)) { wideTop |= WedgeCorners.NNN; }
		if (this.CornerWideTop.Has(WedgeCorners.NPN)) { wideTop |= WedgeCorners.NPP; }
		if (this.CornerWideTop.Has(WedgeCorners.NPP)) { wideBot |= WedgeCorners.NNP; }

		if (this.CornerWideTop.Has(WedgeCorners.PNN)) { wideBot |= WedgeCorners.PPN; }
		if (this.CornerWideTop.Has(WedgeCorners.PNP)) { wideTop |= WedgeCorners.PNN; }
		if (this.CornerWideTop.Has(WedgeCorners.PPN)) { wideTop |= WedgeCorners.PPP; }
		if (this.CornerWideTop.Has(WedgeCorners.PPP)) { wideBot |= WedgeCorners.PNP; }

		return new WedgeInteriorSurfaces()
		{
			EdgeXNegYNegZ = this.EdgeXNegYPosZ.Rotate().Rotate(),
			EdgeXNegYPosZ = this.EdgeXPosYPosZ,
			EdgeXPosYNegZ = this.EdgeXNegYNegZ,
			EdgeXPosYPosZ = this.EdgeXPosYNegZ.Rotate().Rotate(),

			EdgeYNegXNegZ = this.EdgeZNegXNegY.Rotate().Rotate().Rotate(),
			EdgeYNegXPosZ = this.EdgeZNegXPosY.Rotate().Rotate().Rotate(),
			EdgeYPosXNegZ = this.EdgeZPosXNegY.Rotate(),
			EdgeYPosXPosZ = this.EdgeZPosXPosY.Rotate(),

			EdgeZNegXNegY = this.EdgeYNegXPosZ.Rotate().Rotate().Rotate(),
			EdgeZNegXPosY = this.EdgeYNegXNegZ.Rotate().Rotate().Rotate(),
			EdgeZPosXNegY = this.EdgeYPosXPosZ.Rotate(),
			EdgeZPosXPosY = this.EdgeYPosXNegZ.Rotate(),

			CornerWideBot = wideBot,
			CornerWideTop = wideTop,
		};
	}

	/// <inheritdoc />
	public WedgeInteriorSurfaces RotateY()
	{
		return new WedgeInteriorSurfaces()
		{
			EdgeXNegYNegZ = this.EdgeZPosXNegY,
			EdgeXNegYPosZ = this.EdgeZNegXNegY,
			EdgeXPosYNegZ = this.EdgeZPosXPosY,
			EdgeXPosYPosZ = this.EdgeZNegXPosY,

			EdgeYNegXNegZ = this.EdgeYPosXNegZ,
			EdgeYNegXPosZ = this.EdgeYNegXNegZ,
			EdgeYPosXNegZ = this.EdgeYPosXPosZ,
			EdgeYPosXPosZ = this.EdgeYNegXPosZ,

			EdgeZNegXNegY = this.EdgeXNegYNegZ,
			EdgeZNegXPosY = this.EdgeXPosYNegZ,
			EdgeZPosXNegY = this.EdgeXNegYPosZ,
			EdgeZPosXPosY = this.EdgeXPosYPosZ,

			CornerWideBot = Rotate(this.CornerWideBot),
			CornerWideTop = Rotate(this.CornerWideTop),
		};

		static WedgeCorners Rotate(WedgeCorners current)
		{
			WedgeCorners result = default;
			if (current.Has(WedgeCorners.NNN)) { result |= WedgeCorners.NNP; }
			if (current.Has(WedgeCorners.NNP)) { result |= WedgeCorners.PNP; }
			if (current.Has(WedgeCorners.PNN)) { result |= WedgeCorners.NNN; }
			if (current.Has(WedgeCorners.PNP)) { result |= WedgeCorners.PNN; }

			if (current.Has(WedgeCorners.NPN)) { result |= WedgeCorners.NPP; }
			if (current.Has(WedgeCorners.NPP)) { result |= WedgeCorners.PPP; }
			if (current.Has(WedgeCorners.PPN)) { result |= WedgeCorners.NPN; }
			if (current.Has(WedgeCorners.PPP)) { result |= WedgeCorners.PPN; }
			return result;
		}
	}

	/// <inheritdoc />
	public WedgeInteriorSurfaces RotateZ()
	{
		WedgeCorners wideBot = default;
		WedgeCorners wideTop = default;

		if (this.CornerWideBot.Has(WedgeCorners.NNN)) { wideBot |= WedgeCorners.PNN; }
		if (this.CornerWideBot.Has(WedgeCorners.NPN)) { wideTop |= WedgeCorners.NNN; }
		if (this.CornerWideBot.Has(WedgeCorners.PNN)) { wideTop |= WedgeCorners.PPN; }
		if (this.CornerWideBot.Has(WedgeCorners.PPN)) { wideBot |= WedgeCorners.NPN; }

		if (this.CornerWideBot.Has(WedgeCorners.NNP)) { wideBot |= WedgeCorners.PNP; }
		if (this.CornerWideBot.Has(WedgeCorners.NPP)) { wideTop |= WedgeCorners.NNP; }
		if (this.CornerWideBot.Has(WedgeCorners.PNP)) { wideTop |= WedgeCorners.PPP; }
		if (this.CornerWideBot.Has(WedgeCorners.PPP)) { wideBot |= WedgeCorners.NPP; }

		if (this.CornerWideTop.Has(WedgeCorners.NNN)) { wideTop |= WedgeCorners.PNN; }
		if (this.CornerWideTop.Has(WedgeCorners.NPN)) { wideBot |= WedgeCorners.NNN; }
		if (this.CornerWideTop.Has(WedgeCorners.PNN)) { wideBot |= WedgeCorners.PPN; }
		if (this.CornerWideTop.Has(WedgeCorners.PPN)) { wideTop |= WedgeCorners.NPN; }

		if (this.CornerWideTop.Has(WedgeCorners.NNP)) { wideTop |= WedgeCorners.PNP; }
		if (this.CornerWideTop.Has(WedgeCorners.NPP)) { wideBot |= WedgeCorners.NNP; }
		if (this.CornerWideTop.Has(WedgeCorners.PNP)) { wideBot |= WedgeCorners.PPP; }
		if (this.CornerWideTop.Has(WedgeCorners.PPP)) { wideTop |= WedgeCorners.NPP; }

		return new WedgeInteriorSurfaces()
		{
			EdgeXNegYNegZ = this.EdgeYNegXNegZ.Rotate().Rotate().Rotate(),
			EdgeXNegYPosZ = this.EdgeYNegXPosZ.Rotate(),
			EdgeXPosYNegZ = this.EdgeYPosXNegZ.Rotate().Rotate().Rotate(),
			EdgeXPosYPosZ = this.EdgeYPosXPosZ.Rotate(),

			EdgeYNegXNegZ = this.EdgeXPosYNegZ.Rotate().Rotate().Rotate(),
			EdgeYNegXPosZ = this.EdgeXPosYPosZ.Rotate(),
			EdgeYPosXNegZ = this.EdgeXNegYNegZ.Rotate().Rotate().Rotate(),
			EdgeYPosXPosZ = this.EdgeXNegYPosZ.Rotate(),

			EdgeZNegXNegY = this.EdgeZNegXPosY,
			EdgeZNegXPosY = this.EdgeZPosXPosY.Rotate().Rotate(),
			EdgeZPosXNegY = this.EdgeZNegXNegY.Rotate().Rotate(),
			EdgeZPosXPosY = this.EdgeZPosXNegY,

			CornerWideBot = wideBot,
			CornerWideTop = wideTop,
		};
	}
}
