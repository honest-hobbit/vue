﻿namespace HQS.VUE.Neo;

public class WedgeTableBuilder
{
	private readonly Stopwatch timer = new Stopwatch();

	public WedgeDefinition[] Definition { get; set; }

	public Bit256[] CollidesWith { get; set; }

	public WedgeIndex[] WedgeByFilledCorners { get; set; }

	public WedgeIndex[] Part1 { get; set; }

	public WedgeIndex[] Part2 { get; set; }

	public WedgeIndex[] MirrorX { get; set; }

	public WedgeIndex[] MirrorY { get; set; }

	public WedgeIndex[] MirrorZ { get; set; }

	public WedgeIndex[] RotateX90 { get; set; }

	public WedgeIndex[] RotateY90 { get; set; }

	public WedgeIndex[] RotateZ90 { get; set; }

	public WedgeIndex[] RotateX180 { get; set; }

	public WedgeIndex[] RotateY180 { get; set; }

	public WedgeIndex[] RotateZ180 { get; set; }

	public WedgeIndex[] RotateX270 { get; set; }

	public WedgeIndex[] RotateY270 { get; set; }

	public WedgeIndex[] RotateZ270 { get; set; }

	public WedgeIndex[] CombineWedges { get; set; }

	// Upper 4 bits: FacePosX - Lower 4 bits: FaceNegX
	public QuadParts[] FaceX { get; set; }

	// Upper 4 bits: FacePosY - Lower 4 bits: FaceNegY
	public QuadParts[] FaceY { get; set; }

	// Upper 4 bits: FacePosZ - Lower 4 bits: FaceNegZ
	public QuadParts[] FaceZ { get; set; }

	// Upper 4 bits: EdgeXPosYPosZ - Lower 4 bits: EdgeXNegYNegZ
	public QuadParts[] EdgeXAscending { get; set; }

	// Upper 4 bits: EdgeXPosYNegZ - Lower 4 bits: EdgeXNegYPosZ
	public QuadParts[] EdgeXDescending { get; set; }

	// Upper 4 bits: EdgeYPosXPosZ - Lower 4 bits: EdgeYNegXNegZ
	public QuadParts[] EdgeYAscending { get; set; }

	// Upper 4 bits: EdgeYPosXNegZ - Lower 4 bits: EdgeYNegXPosZ
	public QuadParts[] EdgeYDescending { get; set; }

	// Upper 4 bits: EdgeZPosXPosY - Lower 4 bits: EdgeZNegXNegY
	public QuadParts[] EdgeZAscending { get; set; }

	// Upper 4 bits: EdgeZPosXNegY - Lower 4 bits: EdgeZNegXPosY
	public QuadParts[] EdgeZDescending { get; set; }

	public WedgeCorners[] CornerWideTop { get; set; }

	public WedgeCorners[] CornerWideBot { get; set; }

	public TimeSpan WedgeByFilledCornersDuration { get; private set; }

	public TimeSpan PartsDuration { get; private set; }

	public TimeSpan MirrorDuration { get; private set; }

	public TimeSpan Rotate90Duration { get; private set; }

	public TimeSpan Rotate180Duration { get; private set; }

	public TimeSpan Rotate270Duration { get; private set; }

	public TimeSpan CombineWedgesDuration { get; private set; }

	public TimeSpan SurfacesDuration { get; private set; }

	public TimeSpan ValidateDefinitionDuration { get; private set; }

	public TimeSpan ValidateTablesQuickDuration { get; private set; }

	public TimeSpan ValidateTablesIntermediateDuration { get; private set; }

	public TimeSpan ValidateTablesFullDuration { get; private set; }

	public WedgeTable BuildWedgeTable() => new WedgeTable(this);

	public void GenerateMissingTables()
	{
		this.ValidateDefinitions();

		this.CollidesWith ??= new Bit256[this.Definition.Length];

		this.WedgeByFilledCornersDuration = this.TimeMethod(() =>
		{
			if (this.WedgeByFilledCorners != null) { return; }

			// find all the wedge cases by what corners are filled in
			// do this before anything else so that corners can be used for faster lookups
			int length = 256;
			this.WedgeByFilledCorners = new WedgeIndex[length];
			for (int i = 0; i < length; i++)
			{
				this.WedgeByFilledCorners[i] = this.FindWedgeWithCorners((WedgeCorners)i);
			}
		});

		this.PartsDuration = this.TimeMethod(() =>
		{
			this.Part1 = this.GenerateCornersTable(this.Part1, def => def.Part1Corners);
			this.Part2 = this.GenerateCornersTable(this.Part2, def => def.Part2Corners);
		});

		this.MirrorDuration = this.TimeMethod(() =>
		{
			this.MirrorX = this.GenerateCornersTable(this.MirrorX, def => def.FilledCorners.MirrorX());
			this.MirrorY = this.GenerateCornersTable(this.MirrorY, def => def.FilledCorners.MirrorY());
			this.MirrorZ = this.GenerateCornersTable(this.MirrorZ, def => def.FilledCorners.MirrorZ());
		});

		this.Rotate90Duration = this.TimeMethod(() =>
		{
			this.RotateX90 = this.GenerateCornersTable(this.RotateX90, def => def.FilledCorners.RotateX());
			this.RotateY90 = this.GenerateCornersTable(this.RotateY90, def => def.FilledCorners.RotateY());
			this.RotateZ90 = this.GenerateCornersTable(this.RotateZ90, def => def.FilledCorners.RotateZ());
		});

		this.Rotate180Duration = this.TimeMethod(() =>
		{
			this.RotateX180 = this.GenerateCornersTable(this.RotateX180, def => def.FilledCorners.RotateX().RotateX());
			this.RotateY180 = this.GenerateCornersTable(this.RotateY180, def => def.FilledCorners.RotateY().RotateY());
			this.RotateZ180 = this.GenerateCornersTable(this.RotateZ180, def => def.FilledCorners.RotateZ().RotateZ());
		});

		this.Rotate270Duration = this.TimeMethod(() =>
		{
			this.RotateX270 = this.GenerateCornersTable(this.RotateX270, def => def.FilledCorners.RotateX().RotateX().RotateX());
			this.RotateY270 = this.GenerateCornersTable(this.RotateY270, def => def.FilledCorners.RotateY().RotateY().RotateY());
			this.RotateZ270 = this.GenerateCornersTable(this.RotateZ270, def => def.FilledCorners.RotateZ().RotateZ().RotateZ());
		});

		this.CombineWedgesDuration = this.TimeMethod(() =>
		{
			if (this.CombineWedges != null) { return; }

			this.CombineWedges = new WedgeIndex[SymmetricalArray2D.GetArrayLength(this.Definition.Length)];
			var combineWedges = this.CreateCombineWedgesTable();
			foreach (var i in combineWedges.EnumerateUniqueIndices())
			{
				combineWedges[i.X, i.Y] = this.FindCombinedWedge(i.X, i.Y);
			}
		});

		// TODO these array are not being validated or serialized
		// and maybe they should have a name to imply the bits have been fixed and compacted
		this.SurfacesDuration = this.TimeMethod(() =>
		{
			this.FaceX = this.GenerateSurfaceTable(
				this.FaceX,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.FaceNegX),
				def => def.Surfaces.FacePosX);
			this.FaceY = this.GenerateSurfaceTable(
				this.FaceY,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.FaceNegY),
				def => SurfaceUtility.FlipAll(def.Surfaces.FacePosY));
			this.FaceZ = this.GenerateSurfaceTable(
				this.FaceZ,
				def => def.Surfaces.FaceNegZ,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.FacePosZ));

			this.EdgeXAscending = this.GenerateSurfaceTable(
				this.EdgeXAscending,
				def => def.Surfaces.EdgeXNegYNegZ,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.EdgeXPosYPosZ));
			this.EdgeXDescending = this.GenerateSurfaceTable(
				this.EdgeXDescending,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.EdgeXNegYPosZ),
				def => def.Surfaces.EdgeXPosYNegZ);

			this.EdgeYAscending = this.GenerateSurfaceTable(
				this.EdgeYAscending,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.EdgeYNegXNegZ),
				def => def.Surfaces.EdgeYPosXPosZ);
			this.EdgeYDescending = this.GenerateSurfaceTable(
				this.EdgeYDescending,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.EdgeYNegXPosZ),
				def => def.Surfaces.EdgeYPosXNegZ);

			this.EdgeZAscending = this.GenerateSurfaceTable(
				this.EdgeZAscending,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.EdgeZNegXNegY),
				def => def.Surfaces.EdgeZPosXPosY);
			this.EdgeZDescending = this.GenerateSurfaceTable(
				this.EdgeZDescending,
				def => SurfaceUtility.FlipLeftRight(def.Surfaces.EdgeZNegXPosY),
				def => def.Surfaces.EdgeZPosXNegY);

			this.CornerWideTop = this.GenerateSurfaceTable(this.CornerWideTop, def => def.Surfaces.CornerWideTop);
			this.CornerWideBot = this.GenerateSurfaceTable(this.CornerWideBot, def => def.Surfaces.CornerWideBot);
		});
	}

	public void ValidateDefinitions()
	{
		this.ValidateDefinitionDuration = this.TimeMethod(() =>
		{
			Ensure.That(this.Definition, nameof(this.Definition)).IsNotNull();

			Ensure.That(this.Definition.Length, $"{nameof(this.Definition)}.Length").IsInRange(1, 256);
			Ensure.That(this.Definition[0] == WedgeDefinition.Empty, $"{nameof(this.Definition)}[0]").IsTrue();

			var usedFilledCorners = Bit256.AllFalse;

			for (int i = 0; i < this.Definition.Length; i++)
			{
				// each combination of filled corners must be unique
				var filledCorners = this.Definition[i].FilledCorners;
				if (filledCorners != WedgeCorners.None)
				{
					Ensure.That(
						usedFilledCorners[(int)filledCorners],
						$"{nameof(this.Definition)}[{i}].{nameof(WedgeDefinition.FilledCorners)}").IsFalse();
					usedFilledCorners[(int)filledCorners] = true;
				}
			}
		});
	}

	[SuppressMessage("StyleCop", "SA1101", Justification = "StyleCop rule is broken.")]
	public void ValidateTablesQuick()
	{
		this.ValidateDefinitions();

		this.ValidateTablesQuickDuration = this.TimeMethod(() =>
		{
			Ensure.That(this.WedgeByFilledCorners, nameof(this.WedgeByFilledCorners)).IsNotNull();
			Ensure.That(this.WedgeByFilledCorners.Length, $"{nameof(this.WedgeByFilledCorners)}.Length").Is(256);

			ValidateTable(this.CollidesWith, nameof(this.CollidesWith));
			ValidateTable(this.Part1, nameof(this.Part1));
			ValidateTable(this.Part2, nameof(this.Part2));
			ValidateTable(this.MirrorX, nameof(this.MirrorX));
			ValidateTable(this.MirrorY, nameof(this.MirrorY));
			ValidateTable(this.MirrorZ, nameof(this.MirrorZ));
			ValidateTable(this.RotateX90, nameof(this.RotateX90));
			ValidateTable(this.RotateY90, nameof(this.RotateY90));
			ValidateTable(this.RotateZ90, nameof(this.RotateZ90));
			ValidateTable(this.RotateX180, nameof(this.RotateX180));
			ValidateTable(this.RotateY180, nameof(this.RotateY180));
			ValidateTable(this.RotateZ180, nameof(this.RotateZ180));
			ValidateTable(this.RotateX270, nameof(this.RotateX270));
			ValidateTable(this.RotateY270, nameof(this.RotateY270));
			ValidateTable(this.RotateZ270, nameof(this.RotateZ270));

			Ensure.That(this.CombineWedges, nameof(this.CombineWedges)).IsNotNull();
			Ensure.That(this.CombineWedges.Length, $"{nameof(this.CombineWedges)}.Length")
				.Is(SymmetricalArray2D.GetArrayLength(this.Definition.Length));
		});

		void ValidateTable<T>(T[] table, string name)
		{
			Debug.Assert(this.Definition != null);
			Debug.Assert(!name.IsNullOrWhiteSpace());

			Ensure.That(table, name).IsNotNull();
			Ensure.That(table.Length, $"{name}.Length").Is(this.Definition.Length);
		}
	}

	public void ValidateTablesIntermediate()
	{
		this.ValidateTablesQuick();

		this.ValidateTablesIntermediateDuration = this.TimeMethod(() =>
		{
			for (int i = 0; i < this.WedgeByFilledCorners.Length; i++)
			{
				this.ValidateWedgeIndexIsInBounds(this.WedgeByFilledCorners[i], nameof(this.WedgeByFilledCorners), i);
			}

			ValidateTable(this.Part1, nameof(this.Part1));
			ValidateTable(this.Part2, nameof(this.Part2));
			ValidateTable(this.MirrorX, nameof(this.MirrorX));
			ValidateTable(this.MirrorY, nameof(this.MirrorY));
			ValidateTable(this.MirrorZ, nameof(this.MirrorZ));
			ValidateTable(this.RotateX90, nameof(this.RotateX90));
			ValidateTable(this.RotateY90, nameof(this.RotateY90));
			ValidateTable(this.RotateZ90, nameof(this.RotateZ90));
			ValidateTable(this.RotateX180, nameof(this.RotateX180));
			ValidateTable(this.RotateY180, nameof(this.RotateY180));
			ValidateTable(this.RotateZ180, nameof(this.RotateZ180));
			ValidateTable(this.RotateX270, nameof(this.RotateX270));
			ValidateTable(this.RotateY270, nameof(this.RotateY270));
			ValidateTable(this.RotateZ270, nameof(this.RotateZ270));

			for (int i = 0; i < this.CombineWedges.Length; i++)
			{
				this.ValidateWedgeIndexIsInBounds(this.CombineWedges[i], nameof(this.CombineWedges), i);
			}
		});

		void ValidateTable(WedgeIndex[] table, string name)
		{
			Debug.Assert(this.Definition != null);
			Debug.Assert(!name.IsNullOrWhiteSpace());

			for (int i = 0; i < table.Length; i++)
			{
				this.ValidateWedgeIndexIsInBounds(table[i], name, i);
			}
		}
	}

	public void ValidateTablesFull()
	{
		this.ValidateTablesIntermediate();

		this.ValidateTablesFullDuration = this.TimeMethod(() =>
		{
			var combineWedges = this.CreateCombineWedgesTable();
			int length = combineWedges.SideLength;

			for (int x = 0; x < length; x++)
			{
				for (int y = 0; y < length; y++)
				{
					// wedge index must be the correct combined wedge shape
					Ensure.That(
						combineWedges[x, y] == this.FindCombinedWedge(x, y),
						$"{nameof(this.CombineWedges)}[{x}, {y}]").IsTrue();
				}
			}
		});
	}

	internal SymmetricalArray2D<WedgeIndex> CreateCombineWedgesTable()
	{
		Ensure.That(this.Definition, nameof(this.Definition)).IsNotNull();
		Ensure.That(this.CombineWedges, nameof(this.CombineWedges)).IsNotNull();

		return new SymmetricalArray2D<WedgeIndex>(this.Definition.Length, this.CombineWedges);
	}

	private void ValidateWedgeIndexIsInBounds(WedgeIndex wedge, string name, int i)
	{
		Debug.Assert(!name.IsNullOrWhiteSpace());
		Debug.Assert(i >= 0);

		Ensure.That<int>(wedge.Index, $"{name}[{i}]").IsLt(this.Definition.Length);
	}

	private TimeSpan TimeMethod(Action method)
	{
		Debug.Assert(method != null);

		this.timer.Restart();
		method();
		this.timer.Stop();
		return this.timer.Elapsed;
	}

	private QuadParts[] GenerateSurfaceTable(
		QuadParts[] table,
		Func<WedgeDefinition, QuadParts> getLowerParts,
		Func<WedgeDefinition, QuadParts> getUpperParts)
	{
		Ensure.That(getLowerParts, nameof(getLowerParts)).IsNotNull();
		Ensure.That(getUpperParts, nameof(getUpperParts)).IsNotNull();

		if (table != null) { return table; }

		int length = this.Definition.Length;
		table = new QuadParts[length];

		for (int i = 0; i < length; i++)
		{
			table[i] = (QuadParts)(
				((int)getUpperParts(this.Definition[i]) << WedgePlane.Shift) |
				(int)getLowerParts(this.Definition[i]));
		}

		return table;
	}

	private WedgeCorners[] GenerateSurfaceTable(
		WedgeCorners[] table, Func<WedgeDefinition, WedgeCorners> getCorners)
	{
		Ensure.That(getCorners, nameof(getCorners)).IsNotNull();

		if (table != null) { return table; }

		int length = this.Definition.Length;
		table = new WedgeCorners[length];

		for (int i = 0; i < length; i++)
		{
			table[i] = getCorners(this.Definition[i]);
		}

		return table;
	}

	private WedgeIndex[] GenerateCornersTable(
		WedgeIndex[] table, Func<WedgeDefinition, WedgeCorners> getCornersToFind)
	{
		Ensure.That(getCornersToFind, nameof(getCornersToFind)).IsNotNull();

		if (table != null) { return table; }

		int length = this.Definition.Length;
		table = new WedgeIndex[length];

		for (int i = 0; i < length; i++)
		{
			table[i] = this.WedgeByFilledCorners[(int)getCornersToFind(this.Definition[i])];
		}

		return table;
	}

	private WedgeIndex FindWedgeWithCorners(WedgeCorners filledCorners)
	{
		for (int i = 0; i < this.Definition.Length; i++)
		{
			if (this.Definition[i].FilledCorners == filledCorners)
			{
				return new WedgeIndex((byte)i);
			}
		}

		return WedgeIndex.Empty;
	}

	private WedgeIndex FindCombinedWedge(int sourceIndex, int otherIndex)
	{
		var sourceDefinition = this.Definition[sourceIndex];
		var otherDefinition = this.Definition[otherIndex];

		var combinedSurfaces = sourceDefinition.Surfaces.Add(otherDefinition.Surfaces);
		combinedSurfaces.Interior = combinedSurfaces.Interior
			.Remove(sourceDefinition.EnclosedSurfaces).Remove(otherDefinition.EnclosedSurfaces);

		for (int i = 0; i < this.Definition.Length; i++)
		{
			if (this.Definition[i].Surfaces == combinedSurfaces)
			{
				return new WedgeIndex((byte)i);
			}
		}

		return WedgeIndex.Empty;
	}
}
