﻿namespace HQS.VUE.Neo;

public class WedgeTable : IEnumerable<KeyValuePair<WedgeIndex, WedgeDefinition>>
{
	private readonly WedgeDefinition[] definition;

	private readonly Bit256[] collidesWith;

	private readonly WedgeIndex[] wedgeByFilledCorners;

	private readonly WedgeIndex[] part1;

	private readonly WedgeIndex[] part2;

	private readonly WedgeIndex[] mirrorX;

	private readonly WedgeIndex[] mirrorY;

	private readonly WedgeIndex[] mirrorZ;

	private readonly WedgeIndex[] rotateX90;

	private readonly WedgeIndex[] rotateY90;

	private readonly WedgeIndex[] rotateZ90;

	private readonly WedgeIndex[] rotateX180;

	private readonly WedgeIndex[] rotateY180;

	private readonly WedgeIndex[] rotateZ180;

	private readonly WedgeIndex[] rotateX270;

	private readonly WedgeIndex[] rotateY270;

	private readonly WedgeIndex[] rotateZ270;

	private readonly SymmetricalArray2D<WedgeIndex> combineWedges;

	private readonly QuadParts[] faceX;

	private readonly QuadParts[] faceY;

	private readonly QuadParts[] faceZ;

	private readonly QuadParts[] edgeXAscending;

	private readonly QuadParts[] edgeXDescending;

	private readonly QuadParts[] edgeYAscending;

	private readonly QuadParts[] edgeYDescending;

	private readonly QuadParts[] edgeZAscending;

	private readonly QuadParts[] edgeZDescending;

	private readonly WedgeCorners[] cornerWideTop;

	private readonly WedgeCorners[] cornerWideBot;

	internal WedgeTable(WedgeTableBuilder builder)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();
		builder.ValidateTablesQuick();

		this.definition = builder.Definition;
		this.collidesWith = builder.CollidesWith;
		this.wedgeByFilledCorners = builder.WedgeByFilledCorners;
		this.part1 = builder.Part1;
		this.part2 = builder.Part2;
		this.mirrorX = builder.MirrorX;
		this.mirrorY = builder.MirrorY;
		this.mirrorZ = builder.MirrorZ;
		this.rotateX90 = builder.RotateX90;
		this.rotateY90 = builder.RotateY90;
		this.rotateZ90 = builder.RotateZ90;
		this.rotateX180 = builder.RotateX180;
		this.rotateY180 = builder.RotateY180;
		this.rotateZ180 = builder.RotateZ180;
		this.rotateX270 = builder.RotateX270;
		this.rotateY270 = builder.RotateY270;
		this.rotateZ270 = builder.RotateZ270;
		this.combineWedges = builder.CreateCombineWedgesTable();
		this.faceX = builder.FaceX;
		this.faceY = builder.FaceY;
		this.faceZ = builder.FaceZ;
		this.edgeXAscending = builder.EdgeXAscending;
		this.edgeXDescending = builder.EdgeXDescending;
		this.edgeYAscending = builder.EdgeYAscending;
		this.edgeYDescending = builder.EdgeYDescending;
		this.edgeZAscending = builder.EdgeZAscending;
		this.edgeZDescending = builder.EdgeZDescending;
		this.cornerWideTop = builder.CornerWideTop;
		this.cornerWideBot = builder.CornerWideBot;
	}

	public int Length => this.definition.Length;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public bool GetCollidesWith(WedgeIndex source, WedgeIndex other) => this.collidesWith[source.Index][other.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex GetWedgeByFilledCorners(WedgeCorners corners) => this.wedgeByFilledCorners[(int)corners];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex GetPart1(WedgeIndex source) => this.part1[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex GetPart2(WedgeIndex source) => this.part2[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeDefinition GetDefinition(WedgeIndex source) => this.definition[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeDefinition GetPart1Definition(WedgeIndex source) => this.definition[this.part1[source.Index].Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeDefinition GetPart2Definition(WedgeIndex source) => this.definition[this.part2[source.Index].Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex MirrorX(WedgeIndex source) => this.mirrorX[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex MirrorY(WedgeIndex source) => this.mirrorY[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex MirrorZ(WedgeIndex source) => this.mirrorZ[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateX90(WedgeIndex source) => this.rotateX90[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateY90(WedgeIndex source) => this.rotateY90[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateZ90(WedgeIndex source) => this.rotateZ90[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateX180(WedgeIndex source) => this.rotateX180[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateY180(WedgeIndex source) => this.rotateY180[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateZ180(WedgeIndex source) => this.rotateZ180[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateX270(WedgeIndex source) => this.rotateX270[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateY270(WedgeIndex source) => this.rotateY270[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeIndex RotateZ270(WedgeIndex source) => this.rotateZ270[source.Index];

	public WedgeIndex AddCornerNNN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.NNN)];

	public WedgeIndex AddCornerNNP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.NNP)];

	public WedgeIndex AddCornerNPN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.NPN)];

	public WedgeIndex AddCornerNPP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.NPP)];

	public WedgeIndex AddCornerPNN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.PNN)];

	public WedgeIndex AddCornerPNP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.PNP)];

	public WedgeIndex AddCornerPPN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.PPN)];

	public WedgeIndex AddCornerPPP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Add(WedgeCorners.PPP)];

	public WedgeIndex RemoveCornerNNN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.NNN)];

	public WedgeIndex RemoveCornerNNP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.NNP)];

	public WedgeIndex RemoveCornerNPN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.NPN)];

	public WedgeIndex RemoveCornerNPP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.NPP)];

	public WedgeIndex RemoveCornerPNN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.PNN)];

	public WedgeIndex RemoveCornerPNP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.PNP)];

	public WedgeIndex RemoveCornerPPN(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.PPN)];

	public WedgeIndex RemoveCornerPPP(WedgeIndex source) =>
		this.wedgeByFilledCorners[(int)this.definition[source.Index].FilledCorners.Remove(WedgeCorners.PPP)];

	public WedgeIndex Combine(WedgeIndex source, WedgeIndex other) => this.combineWedges[source.Index, other.Index];

	// Upper 4 bits: FacePosX - Lower 4 bits: FaceNegX
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetFaceX(WedgeIndex source) => this.faceX[source.Index];

	// Upper 4 bits: FacePosY - Lower 4 bits: FaceNegY
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetFaceY(WedgeIndex source) => this.faceY[source.Index];

	// Upper 4 bits: FacePosZ - Lower 4 bits: FaceNegZ
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetFaceZ(WedgeIndex source) => this.faceZ[source.Index];

	// Upper 4 bits: EdgeXPosYPosZ - Lower 4 bits: EdgeXNegYNegZ
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetEdgeXAscending(WedgeIndex source) => this.edgeXAscending[source.Index];

	// Upper 4 bits: EdgeXPosYNegZ - Lower 4 bits: EdgeXNegYPosZ
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetEdgeXDescending(WedgeIndex source) => this.edgeXDescending[source.Index];

	// Upper 4 bits: EdgeYPosXPosZ - Lower 4 bits: EdgeYNegXNegZ
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetEdgeYAscending(WedgeIndex source) => this.edgeYAscending[source.Index];

	// Upper 4 bits: EdgeYPosXNegZ - Lower 4 bits: EdgeYNegXPosZ
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetEdgeYDescending(WedgeIndex source) => this.edgeYDescending[source.Index];

	// Upper 4 bits: EdgeZPosXPosY - Lower 4 bits: EdgeZNegXNegY
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetEdgeZAscending(WedgeIndex source) => this.edgeZAscending[source.Index];

	// Upper 4 bits: EdgeZPosXNegY - Lower 4 bits: EdgeZNegXPosY
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public QuadParts GetEdgeZDescending(WedgeIndex source) => this.edgeZDescending[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeCorners GetCornerWideTop(WedgeIndex source) => this.cornerWideTop[source.Index];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public WedgeCorners GetCornerWideBot(WedgeIndex source) => this.cornerWideBot[source.Index];

	/// <inheritdoc />
	public IEnumerator<KeyValuePair<WedgeIndex, WedgeDefinition>> GetEnumerator()
	{
		for (int i = 0; i < this.definition.Length; i++)
		{
			yield return KeyValuePair.Create(new WedgeIndex((byte)i), this.definition[i]);
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
