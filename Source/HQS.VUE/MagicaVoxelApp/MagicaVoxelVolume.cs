﻿namespace HQS.VUE.MagicaVoxelApp;

public class MagicaVoxelVolume
{
	private readonly List<Frame> frames = new List<Frame>();

	private IReadOnlyList<ColorRGB> colors = MagicaColorPalette.DefaultColors;

	public MagicaVoxelVolume(Int3 dimensions)
	{
		Debug.Assert(dimensions.X >= 0);
		Debug.Assert(dimensions.Y >= 0);
		Debug.Assert(dimensions.Z >= 0);

		this.Dimensions = dimensions;
		this.Frames = this.frames.AsReadOnlyList();
	}

	public Int3 Dimensions { get; }

	public IReadOnlyList<ColorRGB> Colors
	{
		get => this.colors;
		set
		{
			Debug.Assert(MagicaColorPalette.IsValid(value));

			this.colors = value;
		}
	}

	public IReadOnlyList<Frame> Frames { get; }

	public ColorRGB GetColor(MagicaVoxel voxel)
	{
		Debug.Assert(this.Colors != null);

		return voxel.GetColor(this.Colors);
	}

	public Frame AddFrame()
	{
		var frame = new Frame(this);
		this.frames.Add(frame);
		return frame;
	}

	public Frame GetOrAddFrame(int index)
	{
		Debug.Assert(index >= 0);

		while (this.frames.Count <= index)
		{
			this.frames.Add(new Frame(this));
		}

		return this.frames[index];
	}

	public void ClearAllFrames() => this.frames.ForEach(x => x.Clear());

	public class Frame
	{
		private readonly MagicaVoxel[] voxels;

		public Frame(MagicaVoxelVolume volume)
		{
			Debug.Assert(volume != null);

			this.Volume = volume;
			this.voxels = new MagicaVoxel[volume.Dimensions.X * volume.Dimensions.Y * volume.Dimensions.Z];
		}

		public MagicaVoxelVolume Volume { get; }

		public MagicaVoxel this[int x, int y, int z]
		{
			get => this.voxels[this.GetIndex(x, y, z)];
			set => this.voxels[this.GetIndex(x, y, z)] = value;
		}

		public MagicaVoxel this[Int3 index]
		{
			get => this.voxels[this.GetIndex(index.X, index.Y, index.Z)];
			set => this.voxels[this.GetIndex(index.X, index.Y, index.Z)] = value;
		}

		public ColorRGB GetColor(MagicaVoxel voxel) => this.Volume.GetColor(voxel);

		public void Clear() => this.voxels.SetAllTo(MagicaVoxel.Empty);

		private int GetIndex(int x, int y, int z) => x + (this.Volume.Dimensions.X * (z + (this.Volume.Dimensions.Y * y)));
	}
}
