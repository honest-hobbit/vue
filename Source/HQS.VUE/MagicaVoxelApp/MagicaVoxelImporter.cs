﻿namespace HQS.VUE.MagicaVoxelApp;

public static class MagicaVoxelImporter
{
	public static MagicaVoxelVolume CreateVolume(string filePath)
	{
		Debug.Assert(PathUtility.IsPathValid(filePath));

		using var stream = new BinaryReader(new FileStream(filePath, FileMode.Open));
		return CreateVolume(stream);
	}

	public static MagicaVoxelVolume CreateVolume(BinaryReader stream)
	{
		Debug.Assert(stream != null);

		var dataFrames = new List<MagicaVoxelData[]>();
		var size = ReadMagicaVoxelData(stream, dataFrames, out var colors);
		var volume = new MagicaVoxelVolume(size)
		{
			Colors = colors,
		};

		FillVolume(volume, dataFrames);
		return volume;
	}

	public static void FillVolume(string filePath, MagicaVoxelVolume volume)
	{
		Debug.Assert(PathUtility.IsPathValid(filePath));
		Debug.Assert(volume != null);

		using var stream = new BinaryReader(new FileStream(filePath, FileMode.Open));
		FillVolume(stream, volume);
	}

	[SuppressMessage("Usage", "CA2208", Justification = "Exception is not a method precondition.")]
	public static void FillVolume(BinaryReader stream, MagicaVoxelVolume volume)
	{
		Debug.Assert(stream != null);
		Debug.Assert(volume != null);

		var dataFrames = new List<MagicaVoxelData[]>();
		var size = ReadMagicaVoxelData(stream, dataFrames, out var colors);

		if (volume.Dimensions != size)
		{
			throw new ArgumentException("Volume size does not match MagicaVoxel shape dimensions.", nameof(volume.Dimensions));
		}

		volume.Colors = colors;
		FillVolume(volume, dataFrames);
	}

	private static void FillVolume(MagicaVoxelVolume volume, List<MagicaVoxelData[]> dataFrames)
	{
		Debug.Assert(volume != null);
		Debug.Assert(dataFrames != null);

		volume.ClearAllFrames();
		for (int frameIndex = 0; frameIndex < dataFrames.Count; frameIndex++)
		{
			var dataFrame = dataFrames[frameIndex];
			var frame = volume.GetOrAddFrame(frameIndex);

			for (int voxelIndex = 0; voxelIndex < dataFrame.Length; voxelIndex++)
			{
				var voxel = dataFrame[voxelIndex];
				frame[voxel.X, voxel.Y, voxel.Z] = new MagicaVoxel(voxel.ColorIndex);
			}
		}
	}

	private static Int3 ReadMagicaVoxelData(
		BinaryReader stream, List<MagicaVoxelData[]> dataFrames, out IReadOnlyList<ColorRGB> colors)
	{
		Debug.Assert(stream != null);
		Debug.Assert(dataFrames != null);
		Debug.Assert(dataFrames.Count == 0);

		// Magica Voxel file format
		// http://voxel.codeplex.com/wikipage?title=VOX%20Format&referringTitle=Home
		string magic = new string(stream.ReadChars(4));

		// int version (not used)
		stream.ReadInt32();

		// a MagicaVoxel .vox file starts with a 'magic' 4 character 'VOX ' identifier
		if (magic != "VOX ")
		{
			throw new IOException($"{nameof(stream)} is not Magica Voxel data.");
		}

		int sizeX = 0;
		int sizeY = 0;
		int sizeZ = 0;
		colors = null;

		while (stream.BaseStream.Position < stream.BaseStream.Length)
		{
			// each chunk has an ID, size and child chunks
			string chunkName = new string(stream.ReadChars(4));
			int chunkSize = stream.ReadInt32();

			// int childChunks (not used)
			stream.ReadInt32();

			if (chunkName == "SIZE")
			{
				sizeX = stream.ReadInt32();
				sizeZ = stream.ReadInt32();
				sizeY = stream.ReadInt32();
			}
			else if (chunkName == "XYZI")
			{
				var voxelData = new MagicaVoxelData[stream.ReadInt32()];
				for (int i = 0; i < voxelData.Length; i++)
				{
					voxelData[i] = ReadVoxel(stream);
				}

				dataFrames.Add(voxelData);
			}
			else if (chunkName == "RGBA")
			{
				var colorsArray = new ColorRGB[256];
				for (int i = 1; i < colorsArray.Length; i++)
				{
					colorsArray[i] = ReadColor(stream);
				}

				// slot 0 is reserved for the empty voxel / transparent color
				// but is saved as the last color in the color block and should be ignored
				ReadColor(stream);
				colorsArray[0] = new ColorRGB(0, 0, 0, 0);
			}
			else
			{
				// skip over any unrecognized chunk by reading any excess bytes
				stream.BaseStream.Seek(chunkSize, SeekOrigin.Current);
			}
		}

		if (colors == null)
		{
			colors = MagicaColorPalette.DefaultColors;
		}

		return new Int3(sizeX, sizeY, sizeZ);
	}

	private static ColorRGB ReadColor(BinaryReader stream)
	{
		Debug.Assert(stream != null);

		byte r = stream.ReadByte();
		byte g = stream.ReadByte();
		byte b = stream.ReadByte();
		byte a = stream.ReadByte();
		return new ColorRGB(r, g, b, a);
	}

	private static MagicaVoxelData ReadVoxel(BinaryReader stream)
	{
		Debug.Assert(stream != null);

		// each voxel has x, y, z and color index values
		var x = stream.ReadByte();
		var y = stream.ReadByte();
		var z = stream.ReadByte();
		var color = stream.ReadByte();
		return new MagicaVoxelData(x, y, z, color);
	}

	private readonly struct MagicaVoxelData
	{
		public MagicaVoxelData(byte x, byte y, byte z, byte colorIndex)
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
			this.ColorIndex = colorIndex;
		}

		public byte X { get; }

		public byte Y { get; }

		public byte Z { get; }

		public byte ColorIndex { get; }
	}
}
