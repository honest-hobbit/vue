﻿namespace HQS.VUE.MagicaVoxelApp;

public readonly struct MagicaVoxel : IEquatable<MagicaVoxel>
{
	public MagicaVoxel(byte colorIndex)
	{
		this.ColorIndex = colorIndex;
	}

	public static MagicaVoxel Empty => new MagicaVoxel(0);

	public byte ColorIndex { get; }

	public bool IsEmpty => this.ColorIndex == 0;

	public static bool operator ==(MagicaVoxel lhs, MagicaVoxel rhs) => lhs.Equals(rhs);

	public static bool operator !=(MagicaVoxel lhs, MagicaVoxel rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(MagicaVoxel other) => this.ColorIndex == other.ColorIndex;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.ColorIndex.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.ColorIndex.ToString();

	public ColorRGB GetColor(IReadOnlyList<ColorRGB> colors)
	{
		Debug.Assert(colors != null);
		Debug.Assert(colors.Count == MagicaColorPalette.RequirredLength);

		return colors[this.ColorIndex];
	}
}
