﻿namespace HQS.VUE.OLD;

internal class ChangesetRecorder
{
	private readonly CommandWaiter recording = new CommandWaiter();

	private readonly Stopwatch timer = new Stopwatch();

	private readonly ICommandSubmitter<RecordChangesCommand.Args> recordProcessor;

	private int changeRecords;

	public ChangesetRecorder(ICommandSubmitter<RecordChangesCommand.Args> recordProcessor)
	{
		Debug.Assert(recordProcessor != null);

		this.recordProcessor = recordProcessor;
	}

	public void StartRecordingChanges(IReadOnlyList<IVolumeView> volumes, Pinned<IVoxelJobInfo> jobCompleted)
	{
		Debug.Assert(volumes != null);
		Debug.Assert(volumes.Count > 0);
		Debug.Assert(jobCompleted.IsPinned);

		this.timer.Restart();
		this.changeRecords = 0;

		int volumesCount = volumes.Count;
		for (int volumeIndex = 0; volumeIndex < volumesCount; volumeIndex++)
		{
			var volume = volumes[volumeIndex];

			int recordsCount = volume.ChangeRecords.Count;
			for (int recordIndex = 0; recordIndex < recordsCount; recordIndex++)
			{
				this.recording.Add(this.recordProcessor.SubmitAndGetWaiter(new RecordChangesCommand.Args()
				{
					Volume = volume,
					Record = volume.ChangeRecords[recordIndex].Record.Value,
					JobCompleted = jobCompleted,
				}));

				this.changeRecords++;
			}
		}

		if (this.changeRecords == 0)
		{
			this.timer.Reset();
		}
	}

	public void WaitUntilRecordingFinished(VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(diagnostics != null);

		this.recording.Wait();
		this.recording.Clear();
		this.timer.Stop();
		diagnostics.SetRecordChangesDuration(this.timer.Elapsed);
		diagnostics.ChangeRecordsRecorded = this.changeRecords;
	}
}
