﻿namespace HQS.VUE.OLD;

internal class AdjacentVoxelsBlock
{
	private readonly Voxel[][] arrays;

	public AdjacentVoxelsBlock(VoxelSizeConfig config)
	{
		Debug.Assert(config != null);

		int length = config.VoxelIndexer.AsSquare.Length;
		this.arrays = new Voxel[7][];
		this.arrays[(int)BlockPart.Center] = new Voxel[config.VoxelIndexer.Length];
		this.arrays[(int)BlockPart.NegX] = new Voxel[length];
		this.arrays[(int)BlockPart.PosX] = new Voxel[length];
		this.arrays[(int)BlockPart.NegY] = new Voxel[length];
		this.arrays[(int)BlockPart.PosY] = new Voxel[length];
		this.arrays[(int)BlockPart.NegZ] = new Voxel[length];
		this.arrays[(int)BlockPart.PosZ] = new Voxel[length];
	}

	public Voxel[] this[int index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this.arrays[index];
	}

	public Voxel[] Center => this.arrays[(int)BlockPart.Center];

	public Voxel[] NegX => this.arrays[(int)BlockPart.NegX];

	public Voxel[] PosX => this.arrays[(int)BlockPart.PosX];

	public Voxel[] NegY => this.arrays[(int)BlockPart.NegY];

	public Voxel[] PosY => this.arrays[(int)BlockPart.PosY];

	public Voxel[] NegZ => this.arrays[(int)BlockPart.NegZ];

	public Voxel[] PosZ => this.arrays[(int)BlockPart.PosZ];

	public void SetAllTo(Voxel voxel)
	{
		for (int i = 0; i < 7; i++)
		{
			this.arrays[i].SetAllTo(voxel);
		}
	}
}
