﻿namespace HQS.VUE.OLD;

internal class VoxelChangesBlock : IVoxelChangesBlock
{
	private readonly VoxelAdjacencyTable table;

	private readonly AdjacentVoxelsBlock oldVoxels;

	private readonly AdjacentVoxelsBlock newVoxels;

	public VoxelChangesBlock(VoxelAdjacencyTable table, IVoxelDefinitions definitions)
	{
		Debug.Assert(table != null);
		Debug.Assert(table != null);

		this.table = table;
		this.Definitions = definitions;
		this.VoxelIndexer = table.Config.VoxelIndexer;

		this.oldVoxels = new AdjacentVoxelsBlock(table.Config);
		this.newVoxels = new AdjacentVoxelsBlock(table.Config);

		this.OldVoxels = new ReadOnlyCubeArray<Voxel>(this.VoxelIndexer, this.oldVoxels.Center);
		this.NewVoxels = new ReadOnlyCubeArray<Voxel>(this.VoxelIndexer, this.newVoxels.Center);
	}

	/// <inheritdoc />
	public IVoxelDefinitions Definitions { get; }

	/// <inheritdoc />
	public Guid Key { get; set; }

	/// <inheritdoc />
	public CubeArrayIndexer VoxelIndexer { get; }

	/// <inheritdoc />
	public IReadOnlyCubeArray<Voxel> OldVoxels { get; }

	/// <inheritdoc />
	public IReadOnlyCubeArray<Voxel> NewVoxels { get; }

	/// <inheritdoc />
	public Int3 VoxelIndexOffset { get; set; }

	/// <inheritdoc />
	public bool HasAdjacentVoxels { get; set; }

	public void SetOldVoxelsCenter(Voxel uniformVoxel) => this.oldVoxels.Center.SetAllTo(uniformVoxel);

	public void SetOldVoxelsCenter(ReadOnlyArray<Voxel> voxels) => voxels.CopyTo(this.oldVoxels.Center);

	public void SetOldVoxelsAdjacent(BlockPart part, ReadOnlyVoxelSubchunk subchunk) =>
		this.SetVoxelsAdjacent(this.oldVoxels, part, subchunk);

	public void SetNewVoxelsCenter(Voxel uniformVoxel) => this.newVoxels.Center.SetAllTo(uniformVoxel);

	public void SetNewVoxelsCenter(ReadOnlyArray<Voxel> voxels) => voxels.CopyTo(this.newVoxels.Center);

	public void SetNewVoxelsAdjacent(BlockPart part, ReadOnlyVoxelSubchunk subchunk) =>
		this.SetVoxelsAdjacent(this.newVoxels, part, subchunk);

	public void Clear()
	{
		this.Key = Guid.Empty;
		this.VoxelIndexOffset = Int3.Zero;
		this.HasAdjacentVoxels = false;
		this.oldVoxels.SetAllTo(Voxel.Empty);
		this.newVoxels.SetAllTo(Voxel.Empty);
	}

	/// <inheritdoc />
	public Int3 GetVoxelIndex(int index) => this.VoxelIndexer[index] + this.VoxelIndexOffset;

	/// <inheritdoc />
	public Cross<Voxel> GetOldAdjacentVoxels(int x, int y, int z)
	{
		this.ValidateHasAdjacentVoxels(nameof(this.GetOldAdjacentVoxels));
		return this.GetAdjacentVoxels(this.oldVoxels, this.VoxelIndexer[x, y, z]);
	}

	/// <inheritdoc />
	public Cross<Voxel> GetOldAdjacentVoxels(Int3 index)
	{
		this.ValidateHasAdjacentVoxels(nameof(this.GetOldAdjacentVoxels));
		return this.GetAdjacentVoxels(this.oldVoxels, this.VoxelIndexer[index]);
	}

	/// <inheritdoc />
	public Cross<Voxel> GetOldAdjacentVoxels(int index)
	{
		this.ValidateHasAdjacentVoxels(nameof(this.GetOldAdjacentVoxels));
		return this.GetAdjacentVoxels(this.oldVoxels, index);
	}

	/// <inheritdoc />
	public Cross<Voxel> GetNewAdjacentVoxels(int x, int y, int z)
	{
		this.ValidateHasAdjacentVoxels(nameof(this.GetNewAdjacentVoxels));
		return this.GetAdjacentVoxels(this.newVoxels, this.VoxelIndexer[x, y, z]);
	}

	/// <inheritdoc />
	public Cross<Voxel> GetNewAdjacentVoxels(Int3 index)
	{
		this.ValidateHasAdjacentVoxels(nameof(this.GetNewAdjacentVoxels));
		return this.GetAdjacentVoxels(this.newVoxels, this.VoxelIndexer[index]);
	}

	/// <inheritdoc />
	public Cross<Voxel> GetNewAdjacentVoxels(int index)
	{
		this.ValidateHasAdjacentVoxels(nameof(this.GetNewAdjacentVoxels));
		return this.GetAdjacentVoxels(this.newVoxels, index);
	}

	private void ValidateHasAdjacentVoxels(string methodName)
	{
		Debug.Assert(methodName != null);

		if (!this.HasAdjacentVoxels)
		{
			throw new InvalidOperationException($"Can't call {methodName} because {nameof(this.HasAdjacentVoxels)} is false.");
		}
	}

	private Cross<Voxel> GetAdjacentVoxels(AdjacentVoxelsBlock voxels, int index)
	{
		Debug.Assert(voxels != null);

		this.table.GetAdjacentIndices(index, out var part, out var voxelIndex);
		return new Cross<Voxel>(
			voxels.Center[index],
			voxels[(int)part.NegX][voxelIndex.NegX],
			voxels[(int)part.PosX][voxelIndex.PosX],
			voxels[(int)part.NegY][voxelIndex.NegY],
			voxels[(int)part.PosY][voxelIndex.PosY],
			voxels[(int)part.NegZ][voxelIndex.NegZ],
			voxels[(int)part.PosZ][voxelIndex.PosZ]);
	}

	private void SetVoxelsAdjacent(AdjacentVoxelsBlock voxelsBlock, BlockPart part, ReadOnlyVoxelSubchunk subchunk)
	{
		Debug.Assert(voxelsBlock != null);
		Debug.Assert(part != BlockPart.Center);

		var adjacent = voxelsBlock[(int)part];
		if (subchunk.IsUniform)
		{
			adjacent.SetAllTo(subchunk.UniformVoxel);
		}
		else
		{
			var fromSubchunkTo = this.table.GetFromSubchunkTo(part);
			Debug.Assert(adjacent.Length == fromSubchunkTo.Length);

			var voxels = subchunk.Voxels;
			int max = adjacent.Length;
			for (int i = 0; i < max; i++)
			{
				adjacent[i] = voxels[fromSubchunkTo[i]];
			}
		}
	}
}
