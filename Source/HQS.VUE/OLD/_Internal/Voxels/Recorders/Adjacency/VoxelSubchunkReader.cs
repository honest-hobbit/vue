﻿namespace HQS.VUE.OLD;

internal class VoxelSubchunkReader
{
	private const bool WaitUntilLoaded = true;

	private readonly VoxelChunkCache cache;

	private readonly CubeArrayIndexer subchunkIndexer;

	private readonly int sideMax;

	private IVolumeView volume;

	private IVoxelGridStore store;

	private Cross<VoxelChunkChangeset> chunk;

	private Adjacent<Pin<VoxelChunk>> pin;

	public VoxelSubchunkReader(VoxelSizeConfig config, VoxelChunkCache cache)
	{
		Debug.Assert(config != null);
		Debug.Assert(cache != null);

		this.cache = cache;
		this.subchunkIndexer = config.SubchunkIndexer;
		this.sideMax = this.subchunkIndexer.SideLength - 1;
	}

	public bool IsVolumeSet { get; private set; }

	public bool IsChunkSet { get; private set; }

	public void SetVolume(IVolumeView volume)
	{
		Debug.Assert(!this.IsVolumeSet);
		Debug.Assert(!this.IsChunkSet);
		Debug.Assert(volume != null);
		Debug.Assert(volume.IsInitialized);

		this.IsVolumeSet = true;
		this.volume = volume;
		var config = volume.RunJobInfo;
		this.store = config.VoxelGridStore;
	}

	public void ClearVolume()
	{
		Debug.Assert(this.IsVolumeSet);
		Debug.Assert(!this.IsChunkSet);

		this.IsVolumeSet = false;
		this.volume = null;
		this.store = null;
	}

	public void SetChunk(VoxelChunkChangeset chunk)
	{
		Debug.Assert(this.IsVolumeSet);
		Debug.Assert(!this.IsChunkSet);
		Debug.Assert(!chunk.IsNull);

		this.IsChunkSet = true;
		this.chunk.Center = chunk;
	}

	public void ClearChunk()
	{
		Debug.Assert(this.IsVolumeSet);
		Debug.Assert(this.IsChunkSet);

		this.IsChunkSet = false;
		this.chunk = default;

		this.pin.NegX.Dispose();
		this.pin.PosX.Dispose();
		this.pin.NegY.Dispose();
		this.pin.PosY.Dispose();
		this.pin.NegZ.Dispose();
		this.pin.PosZ.Dispose();
		this.pin = default;
	}

	public void GetAdjacentSubchunks(
		int subchunkIndex,
		out Adjacent<ReadOnlyVoxelSubchunk> oldSubchunks,
		out Adjacent<ReadOnlyVoxelSubchunk> newSubchunks)
	{
		Debug.Assert(this.IsVolumeSet);
		Debug.Assert(this.IsChunkSet);

		var centerSubchunkIndex = this.subchunkIndexer[subchunkIndex];
		oldSubchunks = default;
		newSubchunks = default;

		// handle assigning X axis
		if (centerSubchunkIndex.X == 0)
		{
			this.AssignAdjacentSubchunk(
				new Int3(this.sideMax, centerSubchunkIndex.Y, centerSubchunkIndex.Z),
				ref oldSubchunks.NegX,
				ref newSubchunks.NegX,
				-Int3.UnitX,
				ref this.chunk.NegX,
				ref this.pin.NegX);
		}
		else
		{
			this.AssignSubchunk(
				centerSubchunkIndex - Int3.UnitX, ref oldSubchunks.NegX, ref newSubchunks.NegX);
		}

		if (centerSubchunkIndex.X == this.sideMax)
		{
			this.AssignAdjacentSubchunk(
				new Int3(0, centerSubchunkIndex.Y, centerSubchunkIndex.Z),
				ref oldSubchunks.PosX,
				ref newSubchunks.PosX,
				Int3.UnitX,
				ref this.chunk.PosX,
				ref this.pin.PosX);
		}
		else
		{
			this.AssignSubchunk(
				centerSubchunkIndex + Int3.UnitX, ref oldSubchunks.PosX, ref newSubchunks.PosX);
		}

		// handle assigning Y axis
		if (centerSubchunkIndex.Y == 0)
		{
			this.AssignAdjacentSubchunk(
				new Int3(centerSubchunkIndex.X, this.sideMax, centerSubchunkIndex.Z),
				ref oldSubchunks.NegY,
				ref newSubchunks.NegY,
				-Int3.UnitY,
				ref this.chunk.NegY,
				ref this.pin.NegY);
		}
		else
		{
			this.AssignSubchunk(
				centerSubchunkIndex - Int3.UnitY, ref oldSubchunks.NegY, ref newSubchunks.NegY);
		}

		if (centerSubchunkIndex.Y == this.sideMax)
		{
			this.AssignAdjacentSubchunk(
				new Int3(centerSubchunkIndex.X, 0, centerSubchunkIndex.Z),
				ref oldSubchunks.PosY,
				ref newSubchunks.PosY,
				Int3.UnitY,
				ref this.chunk.PosY,
				ref this.pin.PosY);
		}
		else
		{
			this.AssignSubchunk(
				centerSubchunkIndex + Int3.UnitY, ref oldSubchunks.PosY, ref newSubchunks.PosY);
		}

		// handle assigning Z axis
		if (centerSubchunkIndex.Z == 0)
		{
			this.AssignAdjacentSubchunk(
				new Int3(centerSubchunkIndex.X, centerSubchunkIndex.Y, this.sideMax),
				ref oldSubchunks.NegZ,
				ref newSubchunks.NegZ,
				-Int3.UnitZ,
				ref this.chunk.NegZ,
				ref this.pin.NegZ);
		}
		else
		{
			this.AssignSubchunk(
				centerSubchunkIndex - Int3.UnitZ, ref oldSubchunks.NegZ, ref newSubchunks.NegZ);
		}

		if (centerSubchunkIndex.Z == this.sideMax)
		{
			this.AssignAdjacentSubchunk(
				new Int3(centerSubchunkIndex.X, centerSubchunkIndex.Y, 0),
				ref oldSubchunks.PosZ,
				ref newSubchunks.PosZ,
				Int3.UnitZ,
				ref this.chunk.PosZ,
				ref this.pin.PosZ);
		}
		else
		{
			this.AssignSubchunk(
				centerSubchunkIndex + Int3.UnitZ, ref oldSubchunks.PosZ, ref newSubchunks.PosZ);
		}
	}

	private void AssignSubchunk(
		Int3 subchunkIndex,
		ref ReadOnlyVoxelSubchunk oldSubchunk,
		ref ReadOnlyVoxelSubchunk newSubchunk)
	{
		var adjacentIndex = this.subchunkIndexer[subchunkIndex];
		oldSubchunk = this.chunk.Center.Previous.GetSubchunkOrUniformVoxelAsSubchunk(adjacentIndex);
		newSubchunk = this.chunk.Center.Current.GetSubchunkOrUniformVoxelAsSubchunk(adjacentIndex);
	}

	private void AssignAdjacentSubchunk(
		Int3 subchunkIndex,
		ref ReadOnlyVoxelSubchunk oldSubchunk,
		ref ReadOnlyVoxelSubchunk newSubchunk,
		Int3 offset,
		ref VoxelChunkChangeset adjacentChangeset,
		ref Pin<VoxelChunk> adjacentPin)
	{
		if (adjacentChangeset.IsNull && adjacentPin.IsUnassigned)
		{
			offset = this.chunk.Center.Key.Location + offset;
			if (this.volume.TryGetVoxelChunkChangeset(offset, out var temp))
			{
				adjacentChangeset = temp;
			}
			else
			{
				adjacentPin = this.cache.GetChunkPin(
					new ChunkKey(this.volume.Key, offset), this.store, WaitUntilLoaded);
			}
		}

		var adjacentIndex = this.subchunkIndexer[subchunkIndex];
		if (!adjacentChangeset.IsNull)
		{
			oldSubchunk = adjacentChangeset.Previous.GetSubchunkOrUniformVoxelAsSubchunk(adjacentIndex);
			newSubchunk = adjacentChangeset.Current.GetSubchunkOrUniformVoxelAsSubchunk(adjacentIndex);
		}
		else
		{
			oldSubchunk = adjacentPin.Value.AsReadOnly.GetSubchunkOrUniformVoxelAsSubchunk(adjacentIndex);
			newSubchunk = oldSubchunk;
		}
	}
}
