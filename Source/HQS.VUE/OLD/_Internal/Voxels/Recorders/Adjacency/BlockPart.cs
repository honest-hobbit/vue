﻿namespace HQS.VUE.OLD;

internal enum BlockPart : byte
{
	Center,
	NegX,
	PosX,
	NegY,
	PosY,
	NegZ,
	PosZ,
}
