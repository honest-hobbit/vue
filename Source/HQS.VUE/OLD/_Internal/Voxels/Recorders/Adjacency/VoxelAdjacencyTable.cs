﻿namespace HQS.VUE.OLD;

internal class VoxelAdjacencyTable
{
	private readonly BlockPart[] partNegX;

	private readonly BlockPart[] partPosX;

	private readonly BlockPart[] partNegY;

	private readonly BlockPart[] partPosY;

	private readonly BlockPart[] partNegZ;

	private readonly BlockPart[] partPosZ;

	private readonly ushort[] voxelNegX;

	private readonly ushort[] voxelPosX;

	private readonly ushort[] voxelNegY;

	private readonly ushort[] voxelPosY;

	private readonly ushort[] voxelNegZ;

	private readonly ushort[] voxelPosZ;

	private readonly ushort[][] fromSubchunk;

	public VoxelAdjacencyTable(VoxelSizeConfig config)
	{
		Debug.Assert(config != null);

		this.Config = config;
		var voxelIndexer = config.VoxelIndexer;

		// generate the get adjacent indices tables
		int length = voxelIndexer.Length;
		this.partNegX = new BlockPart[length];
		this.partPosX = new BlockPart[length];
		this.partNegY = new BlockPart[length];
		this.partPosY = new BlockPart[length];
		this.partNegZ = new BlockPart[length];
		this.partPosZ = new BlockPart[length];
		this.voxelNegX = new ushort[length];
		this.voxelPosX = new ushort[length];
		this.voxelNegY = new ushort[length];
		this.voxelPosY = new ushort[length];
		this.voxelNegZ = new ushort[length];
		this.voxelPosZ = new ushort[length];

		FillCenterIndices(voxelIndexer, -Int3.UnitX, this.partNegX, this.voxelNegX);
		FillCenterIndices(voxelIndexer, Int3.UnitX, this.partPosX, this.voxelPosX);
		FillCenterIndices(voxelIndexer, -Int3.UnitY, this.partNegY, this.voxelNegY);
		FillCenterIndices(voxelIndexer, Int3.UnitY, this.partPosY, this.voxelPosY);
		FillCenterIndices(voxelIndexer, -Int3.UnitZ, this.partNegZ, this.voxelNegZ);
		FillCenterIndices(voxelIndexer, Int3.UnitZ, this.partPosZ, this.voxelPosZ);

		// generate the subchunk to adjacent part arrays
		var adjacentIndexer = voxelIndexer.AsSquare;
		int sideMax = voxelIndexer.SideLength - 1;

		length = adjacentIndexer.Length;
		var toNegX = new ushort[length];
		var toPosX = new ushort[length];
		var toNegY = new ushort[length];
		var toPosY = new ushort[length];
		var toNegZ = new ushort[length];
		var toPosZ = new ushort[length];

		for (int i = 0; i < length; i++)
		{
			var index = adjacentIndexer[i];
			toNegX[i] = (ushort)voxelIndexer[sideMax, index.X, index.Y];
			toPosX[i] = (ushort)voxelIndexer[0, index.X, index.Y];
			toNegY[i] = (ushort)voxelIndexer[index.X, sideMax, index.Y];
			toPosY[i] = (ushort)voxelIndexer[index.X, 0, index.Y];
			toNegZ[i] = (ushort)voxelIndexer[index.X, index.Y, sideMax];
			toPosZ[i] = (ushort)voxelIndexer[index.X, index.Y, 0];
		}

		this.fromSubchunk = new ushort[6][];
		this.fromSubchunk[(int)BlockPart.NegX - 1] = toNegX;
		this.fromSubchunk[(int)BlockPart.PosX - 1] = toPosX;
		this.fromSubchunk[(int)BlockPart.NegY - 1] = toNegY;
		this.fromSubchunk[(int)BlockPart.PosY - 1] = toPosY;
		this.fromSubchunk[(int)BlockPart.NegZ - 1] = toNegZ;
		this.fromSubchunk[(int)BlockPart.PosZ - 1] = toPosZ;
	}

	public VoxelSizeConfig Config { get; }

	public ReadOnlyArray<ushort> GetFromSubchunkTo(BlockPart part)
	{
		Debug.Assert(part != BlockPart.Center);

		return new ReadOnlyArray<ushort>(this.fromSubchunk[(int)part - 1]);
	}

	public void GetAdjacentIndices(int index, out Cross<BlockPart> part, out Cross<int> voxelIndex)
	{
		part.Center = BlockPart.Center;
		part.NegX = this.partNegX[index];
		part.PosX = this.partPosX[index];
		part.NegY = this.partNegY[index];
		part.PosY = this.partPosY[index];
		part.NegZ = this.partNegZ[index];
		part.PosZ = this.partPosZ[index];

		voxelIndex.Center = index;
		voxelIndex.NegX = this.voxelNegX[index];
		voxelIndex.PosX = this.voxelPosX[index];
		voxelIndex.NegY = this.voxelNegY[index];
		voxelIndex.PosY = this.voxelPosY[index];
		voxelIndex.NegZ = this.voxelNegZ[index];
		voxelIndex.PosZ = this.voxelPosZ[index];
	}

	private static void FillCenterIndices(
		CubeArrayIndexer voxelIndexer,
		Int3 voxelOffset,
		BlockPart[] arrayIndices,
		ushort[] voxelIndices)
	{
		int length = voxelIndexer.Length;

		Debug.Assert(arrayIndices != null);
		Debug.Assert(voxelIndices != null);
		Debug.Assert(arrayIndices.Length == length);
		Debug.Assert(voxelIndices.Length == length);

		var adjacentIndexer = voxelIndexer.AsSquare;
		int max = voxelIndexer.SideLength;

		for (int i = 0; i < length; i++)
		{
			var voxelIndex = voxelIndexer[i] + voxelOffset;

			if (voxelIndex.X == -1)
			{
				arrayIndices[i] = BlockPart.NegX;
				voxelIndices[i] = (ushort)adjacentIndexer[voxelIndex.Y, voxelIndex.Z];
			}
			else if (voxelIndex.X == max)
			{
				arrayIndices[i] = BlockPart.PosX;
				voxelIndices[i] = (ushort)adjacentIndexer[voxelIndex.Y, voxelIndex.Z];
			}
			else if (voxelIndex.Y == -1)
			{
				arrayIndices[i] = BlockPart.NegY;
				voxelIndices[i] = (ushort)adjacentIndexer[voxelIndex.X, voxelIndex.Z];
			}
			else if (voxelIndex.Y == max)
			{
				arrayIndices[i] = BlockPart.PosY;
				voxelIndices[i] = (ushort)adjacentIndexer[voxelIndex.X, voxelIndex.Z];
			}
			else if (voxelIndex.Z == -1)
			{
				arrayIndices[i] = BlockPart.NegZ;
				voxelIndices[i] = (ushort)adjacentIndexer[voxelIndex.X, voxelIndex.Y];
			}
			else if (voxelIndex.Z == max)
			{
				arrayIndices[i] = BlockPart.PosZ;
				voxelIndices[i] = (ushort)adjacentIndexer[voxelIndex.X, voxelIndex.Y];
			}
			else
			{
				arrayIndices[i] = BlockPart.Center;
				voxelIndices[i] = (ushort)voxelIndexer[voxelIndex];
			}
		}
	}
}
