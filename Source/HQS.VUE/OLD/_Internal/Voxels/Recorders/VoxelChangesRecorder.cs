﻿namespace HQS.VUE.OLD;

internal class VoxelChangesRecorder
{
	private readonly GranularChangeRecorder granularRecorder;

	private readonly ChunkedChangeRecorder chunkedRecorder;

	private readonly HierarchicalChangeRecorder hierarchicalRecorder;

	public VoxelChangesRecorder(
		VoxelChunkCache cache,
		VoxelAdjacencyTable table,
		VoxelSizeConfig config,
		VoxelDefinitions definitions)
	{
		Debug.Assert(cache != null);
		Debug.Assert(table != null);
		Debug.Assert(config != null);
		Debug.Assert(definitions != null);

		this.granularRecorder = new GranularChangeRecorder(cache, config, definitions);
		this.chunkedRecorder = new ChunkedChangeRecorder(cache, table, config, definitions);
		this.hierarchicalRecorder = new HierarchicalChangeRecorder(config, definitions);
	}

	public void RecordVoxelChanges(IVolumeView volume, IVoxelChangesRecord record, Pinned<IVoxelJobInfo> jobCompleted)
	{
		Debug.Assert(volume != null);
		Debug.Assert(record != null);
		Debug.Assert(jobCompleted.IsPinned);

		if (volume.VoxelsChanged == 0)
		{
			return;
		}

		bool recordPass = record.StartRecording(jobCompleted, volume.VoxelsChanged);
		while (recordPass)
		{
			switch (record.RecordMode)
			{
				case RecordChangesMode.Granular:
					if (record is IGranularChangeRecord granular)
					{
						recordPass = this.granularRecorder.RecordPass(volume, granular);
					}
					else
					{
						ThrowCastException(record, RecordChangesMode.Granular, nameof(IGranularChangeRecord));
					}

					break;

				case RecordChangesMode.Chunked:
					if (record is IChunkedChangeRecord chunked)
					{
						recordPass = this.chunkedRecorder.RecordPass(volume, chunked);
					}
					else
					{
						ThrowCastException(record, RecordChangesMode.Chunked, nameof(IChunkedChangeRecord));
					}

					break;

				case RecordChangesMode.Hierarchical:
					if (record is IHierarchicalChangeRecord hierarchical)
					{
						recordPass = this.hierarchicalRecorder.RecordPass(volume, hierarchical);
					}
					else
					{
						ThrowCastException(record, RecordChangesMode.Hierarchical, nameof(IHierarchicalChangeRecord));
					}

					break;

				default: throw InvalidEnumArgument.CreateException(nameof(record.RecordMode), record.RecordMode);
			}
		}

		record.FinishedRecording();
	}

	private static void ThrowCastException(IVoxelChangesRecord record, RecordChangesMode mode, string interfaceName)
	{
		Debug.Assert(record != null);
		Debug.Assert(Enumeration.IsDefined(mode));
		Debug.Assert(!interfaceName.IsNullOrWhiteSpace());

		throw new InvalidCastException(
			$"{nameof(record.RecordMode)} returned {mode} but {nameof(record)} does not implement {interfaceName}.");
	}
}
