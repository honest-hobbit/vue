﻿namespace HQS.VUE.OLD;

internal class ChunkedChangeRecorder
{
	private readonly ChangeEnumerator enumerator;

	public ChunkedChangeRecorder(
		VoxelChunkCache cache, VoxelAdjacencyTable table, VoxelSizeConfig config, VoxelDefinitions definitions)
	{
		Debug.Assert(cache != null);
		Debug.Assert(table != null);
		Debug.Assert(config != null);
		Debug.Assert(definitions != null);

		this.enumerator = new ChangeEnumerator(cache, table, config, definitions);
	}

	public bool RecordPass(IVolumeView volume, IChunkedChangeRecord record)
	{
		Debug.Assert(volume != null);
		Debug.Assert(record != null);

		this.enumerator.Initialize(volume, record);

		try
		{
			this.enumerator.PassStarted();

			var chunks = volume.ChangedVoxelChunks;
			int max = chunks.Count;

			for (int i = 0; i < max; i++)
			{
				if (!this.enumerator.EnumerateVoxelChanges(chunks[i]))
				{
					break;
				}
			}

			return record.PassFinished();
		}
		finally
		{
			this.enumerator.Deinitialize();
		}
	}

	private class ChangeEnumerator : VoxelChunkChangesetEnumerator
	{
		private readonly VoxelSubchunkReader reader;

		private readonly VoxelChangesBlock block;

		private IChunkedChangeRecord record;

		public ChangeEnumerator(
			VoxelChunkCache cache,
			VoxelAdjacencyTable table,
			VoxelSizeConfig config,
			VoxelDefinitions definitions)
			: base(config)
		{
			Debug.Assert(cache != null);
			Debug.Assert(table != null);
			Debug.Assert(definitions != null);

			this.reader = new VoxelSubchunkReader(config, cache);
			this.block = new VoxelChangesBlock(table, definitions);
		}

		public void Initialize(IVolumeView volume, IChunkedChangeRecord record)
		{
			Debug.Assert(volume != null);
			Debug.Assert(record != null);

			this.reader.SetVolume(volume);
			this.block.Key = volume.Key;
			this.record = record;
		}

		public void Deinitialize()
		{
			this.reader.ClearVolume();
			this.block.Clear();
			this.record = null;
		}

		public void PassStarted()
		{
			this.record.PassStarted(this.block);
			this.block.HasAdjacentVoxels = this.record.IncludeAdjacentVoxels;
		}

		/// <inheritdoc />
		protected override void ChunkStarted() => this.reader.SetChunk(this.Chunk);

		/// <inheritdoc />
		protected override void ChunkFinished() => this.reader.ClearChunk();

		/// <inheritdoc />
		protected override bool NextSubchunk(Voxel oldUniformVoxel, Voxel newUniformVoxel)
		{
			this.block.VoxelIndexOffset = this.SubchunkVoxelOffset;
			this.block.SetOldVoxelsCenter(oldUniformVoxel);
			this.block.SetNewVoxelsCenter(newUniformVoxel);
			this.HandleSettingAdjacentVoxels();
			return this.record.NextBlock();
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(Voxel oldUniformVoxel, ReadOnlyArray<Voxel> newVoxels)
		{
			this.block.VoxelIndexOffset = this.SubchunkVoxelOffset;
			this.block.SetOldVoxelsCenter(oldUniformVoxel);
			this.block.SetNewVoxelsCenter(newVoxels);
			this.HandleSettingAdjacentVoxels();
			return this.record.NextBlock();
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, Voxel newUniformVoxel)
		{
			this.block.VoxelIndexOffset = this.SubchunkVoxelOffset;
			this.block.SetOldVoxelsCenter(oldVoxels);
			this.block.SetNewVoxelsCenter(newUniformVoxel);
			this.HandleSettingAdjacentVoxels();
			return this.record.NextBlock();
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, ReadOnlyArray<Voxel> newVoxels)
		{
			this.block.VoxelIndexOffset = this.SubchunkVoxelOffset;
			this.block.SetOldVoxelsCenter(oldVoxels);
			this.block.SetNewVoxelsCenter(newVoxels);
			this.HandleSettingAdjacentVoxels();
			return this.record.NextBlock();
		}

		private void HandleSettingAdjacentVoxels()
		{
			if (!this.block.HasAdjacentVoxels)
			{
				return;
			}

			this.reader.GetAdjacentSubchunks(this.SubchunkIndex, out var oldSubchunks, out var newSubchunks);

			this.block.SetOldVoxelsAdjacent(BlockPart.NegX, oldSubchunks.NegX);
			this.block.SetOldVoxelsAdjacent(BlockPart.PosX, oldSubchunks.PosX);
			this.block.SetOldVoxelsAdjacent(BlockPart.NegY, oldSubchunks.NegY);
			this.block.SetOldVoxelsAdjacent(BlockPart.PosY, oldSubchunks.PosY);
			this.block.SetOldVoxelsAdjacent(BlockPart.NegZ, oldSubchunks.NegZ);
			this.block.SetOldVoxelsAdjacent(BlockPart.PosZ, oldSubchunks.PosZ);

			this.block.SetNewVoxelsAdjacent(BlockPart.NegX, newSubchunks.NegX);
			this.block.SetNewVoxelsAdjacent(BlockPart.PosX, newSubchunks.PosX);
			this.block.SetNewVoxelsAdjacent(BlockPart.NegY, newSubchunks.NegY);
			this.block.SetNewVoxelsAdjacent(BlockPart.PosY, newSubchunks.PosY);
			this.block.SetNewVoxelsAdjacent(BlockPart.NegZ, newSubchunks.NegZ);
			this.block.SetNewVoxelsAdjacent(BlockPart.PosZ, newSubchunks.PosZ);
		}
	}
}
