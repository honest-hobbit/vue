﻿namespace HQS.VUE.OLD;

internal class HierarchicalChangeRecorder
{
	private readonly ChangeEnumerator enumerator;

	private readonly VoxelDefinitions definitions;

	public HierarchicalChangeRecorder(VoxelSizeConfig config, VoxelDefinitions definitions)
	{
		Debug.Assert(config != null);
		Debug.Assert(definitions != null);

		this.enumerator = new ChangeEnumerator(config);
		this.definitions = definitions;
	}

	public bool RecordPass(IVolumeView volume, IHierarchicalChangeRecord record)
	{
		Debug.Assert(volume != null);
		Debug.Assert(record != null);

		this.enumerator.Record = record;

		try
		{
			record.PassStarted(this.definitions);
			var chunks = volume.ChangedVoxelChunks;
			int max = chunks.Count;

			for (int i = 0; i < max; i++)
			{
				var chunk = chunks[i];
				record.NextChunk(chunk.Key.Location);
				if (!this.enumerator.EnumerateVoxelChanges(chunk))
				{
					break;
				}
			}

			return record.PassFinished();
		}
		finally
		{
			this.enumerator.Record = null;
		}
	}

	private class ChangeEnumerator : VoxelChunkChangesetEnumerator
	{
		public ChangeEnumerator(VoxelSizeConfig config)
			: base(config)
		{
		}

		public IHierarchicalChangeRecord Record { get; set; }

		/// <inheritdoc />
		protected override bool NextSubchunk(Voxel oldUniformVoxel, Voxel newUniformVoxel)
		{
			this.Record.NextSubchunk(this.SubchunkIndex);
			int max = this.VoxelsTotalLength;

			for (ushort i = 0; i < max; i++)
			{
				if (!this.Record.RecordChange(i, oldUniformVoxel, newUniformVoxel))
				{
					return false;
				}
			}

			return true;
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(Voxel oldUniformVoxel, ReadOnlyArray<Voxel> newVoxels)
		{
			this.Record.NextSubchunk(this.SubchunkIndex);
			int max = this.VoxelsTotalLength;

			for (ushort i = 0; i < max; i++)
			{
				var newVoxel = newVoxels[i];
				if (oldUniformVoxel != newVoxel)
				{
					if (!this.Record.RecordChange(i, oldUniformVoxel, newVoxel))
					{
						return false;
					}
				}
			}

			return true;
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, Voxel newUniformVoxel)
		{
			this.Record.NextSubchunk(this.SubchunkIndex);
			int max = this.VoxelsTotalLength;

			for (ushort i = 0; i < max; i++)
			{
				var oldVoxel = oldVoxels[i];
				if (oldVoxel != newUniformVoxel)
				{
					if (!this.Record.RecordChange(i, oldVoxel, newUniformVoxel))
					{
						return false;
					}
				}
			}

			return true;
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, ReadOnlyArray<Voxel> newVoxels)
		{
			this.Record.NextSubchunk(this.SubchunkIndex);
			int max = this.VoxelsTotalLength;

			for (ushort i = 0; i < max; i++)
			{
				var oldVoxel = oldVoxels[i];
				var newVoxel = newVoxels[i];
				if (oldVoxel != newVoxel)
				{
					if (!this.Record.RecordChange(i, oldVoxel, newVoxel))
					{
						return false;
					}
				}
			}

			return true;
		}
	}
}
