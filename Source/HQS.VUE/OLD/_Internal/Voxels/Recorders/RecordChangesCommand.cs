﻿namespace HQS.VUE.OLD;

internal class RecordChangesCommand :
	AbstractCommand, IParallelCommand, IInitializable<RecordChangesCommand.Args>, INamespaceOLD
{
	private readonly VoxelChangesRecorder changesRecorder;

	private IVolumeView volume;

	private IVoxelChangesRecord record;

	private Pin<IVoxelJobInfo> jobCompleted;

	public RecordChangesCommand(VoxelChangesRecorder changesRecorder)
	{
		Debug.Assert(changesRecorder != null);

		this.changesRecorder = changesRecorder;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.Volume != null);
		Debug.Assert(args.Volume.IsInitialized);
		Debug.Assert(args.Record != null);
		Debug.Assert(args.JobCompleted.IsPinned);
		this.ValidateAndSetInitialized();

		this.volume = args.Volume;
		this.record = args.Record;
		this.jobCompleted = args.JobCompleted.CreatePin();
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.volume = null;
		this.record = null;
		this.jobCompleted.Dispose();
		this.jobCompleted = default;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		this.changesRecorder.RecordVoxelChanges(this.volume, this.record, this.jobCompleted.AsPinned);
	}

	public struct Args
	{
		public IVolumeView Volume;

		public IVoxelChangesRecord Record;

		public Pinned<IVoxelJobInfo> JobCompleted;
	}
}
