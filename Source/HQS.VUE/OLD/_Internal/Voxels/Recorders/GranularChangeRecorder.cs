﻿namespace HQS.VUE.OLD;

internal class GranularChangeRecorder
{
	private readonly ChangeEnumerator enumerator;

	private readonly VoxelChangesView changesView;

	public GranularChangeRecorder(VoxelChunkCache cache, VoxelSizeConfig config, VoxelDefinitions definitions)
	{
		Debug.Assert(cache != null);
		Debug.Assert(config != null);
		Debug.Assert(definitions != null);

		this.enumerator = new ChangeEnumerator(config);
		this.changesView = new VoxelChangesView(cache, config, definitions);
	}

	public bool RecordPass(IVolumeView volume, IGranularChangeRecord record)
	{
		Debug.Assert(volume != null);
		Debug.Assert(record != null);

		this.changesView.Initialize(volume);
		this.enumerator.Record = record;

		try
		{
			record.PassStarted(this.changesView);
			var chunks = volume.ChangedVoxelChunks;
			int max = chunks.Count;

			for (int i = 0; i < max; i++)
			{
				if (!this.enumerator.EnumerateVoxelChanges(chunks[i]))
				{
					break;
				}
			}

			return record.PassFinished();
		}
		finally
		{
			this.enumerator.Record = null;
			this.changesView.Deinitialize();
		}
	}

	private class ChangeEnumerator : VoxelChunkChangesetEnumerator
	{
		public ChangeEnumerator(VoxelSizeConfig config)
			: base(config)
		{
		}

		public IGranularChangeRecord Record { get; set; }

		/// <inheritdoc />
		protected override bool NextSubchunk(Voxel oldUniformVoxel, Voxel newUniformVoxel)
		{
			var offset = this.SubchunkVoxelOffset;
			int max = this.VoxelsSideLength;

			for (int iX = 0; iX < max; iX++)
			{
				for (int iY = 0; iY < max; iY++)
				{
					for (int iZ = 0; iZ < max; iZ++)
					{
						if (!this.Record.RecordChange(
							new Int3(offset.X + iX, offset.Y + iY, offset.Z + iZ), oldUniformVoxel, newUniformVoxel))
						{
							return false;
						}
					}
				}
			}

			return true;
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(Voxel oldUniformVoxel, ReadOnlyArray<Voxel> newVoxels)
		{
			var offset = this.SubchunkVoxelOffset;
			var indexer = this.Config.VoxelIndexer;
			int max = this.VoxelsTotalLength;

			for (int i = 0; i < max; i++)
			{
				var newVoxel = newVoxels[i];
				if (oldUniformVoxel != newVoxel)
				{
					if (!this.Record.RecordChange(offset + indexer[i], oldUniformVoxel, newVoxel))
					{
						return false;
					}
				}
			}

			return true;
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, Voxel newUniformVoxel)
		{
			var offset = this.SubchunkVoxelOffset;
			var indexer = this.Config.VoxelIndexer;
			int max = this.VoxelsTotalLength;

			for (int i = 0; i < max; i++)
			{
				var oldVoxel = oldVoxels[i];
				if (oldVoxel != newUniformVoxel)
				{
					if (!this.Record.RecordChange(offset + indexer[i], oldVoxel, newUniformVoxel))
					{
						return false;
					}
				}
			}

			return true;
		}

		/// <inheritdoc />
		protected override bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, ReadOnlyArray<Voxel> newVoxels)
		{
			var offset = this.SubchunkVoxelOffset;
			var indexer = this.Config.VoxelIndexer;
			int max = this.VoxelsTotalLength;

			for (int i = 0; i < max; i++)
			{
				var oldVoxel = oldVoxels[i];
				var newVoxel = newVoxels[i];
				if (oldVoxel != newVoxel)
				{
					if (!this.Record.RecordChange(offset + indexer[i], oldVoxel, newVoxel))
					{
						return false;
					}
				}
			}

			return true;
		}
	}
}
