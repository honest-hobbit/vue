﻿namespace HQS.VUE.OLD;

internal abstract class VoxelChunkChangesetEnumerator
{
	private readonly int voxelSizeShift;

	private readonly int subchunksLength;

	public VoxelChunkChangesetEnumerator(VoxelSizeConfig config)
	{
		Debug.Assert(config != null);

		this.Config = config;
		this.voxelSizeShift = config.VoxelIndexer.Exponent;
		this.subchunksLength = config.SubchunkIndexer.Length;
		this.VoxelsTotalLength = config.VoxelIndexer.Length;
		this.VoxelsSideLength = config.VoxelIndexer.SideLength;
	}

	protected VoxelSizeConfig Config { get; }

	protected VoxelChunkChangeset Chunk { get; private set; }

	protected Int3 ChunkVoxelOffset { get; private set; }

	protected ushort SubchunkIndex { get; private set; }

	protected Int3 SubchunkVoxelOffset
	{
		get
		{
			var index = this.Config.SubchunkIndexer[this.SubchunkIndex];
			return new Int3(
				this.ChunkVoxelOffset.X + (index.X << this.voxelSizeShift),
				this.ChunkVoxelOffset.Y + (index.Y << this.voxelSizeShift),
				this.ChunkVoxelOffset.Z + (index.Z << this.voxelSizeShift));
		}
	}

	protected int VoxelsTotalLength { get; }

	protected int VoxelsSideLength { get; }

	public bool EnumerateVoxelChanges(VoxelChunkChangeset chunk)
	{
		Debug.Assert(!chunk.IsNull);

		this.Chunk = chunk;
		this.ChunkVoxelOffset = this.Config.ConvertChunkToVoxelIndex(chunk.Key.Location);
		this.SubchunkIndex = 0;
		this.ChunkStarted();

		var oldChunk = chunk.Previous;
		var newChunk = chunk.Current;

		try
		{
			if (oldChunk.IsUniform)
			{
				if (newChunk.IsUniform)
				{
					// both are uniform
					var oldVoxel = oldChunk.UniformVoxel;
					var newVoxel = newChunk.UniformVoxel;
					Debug.Assert(oldVoxel != newVoxel, "Previous and current chunks should not be identical because this only enumerates changed chunks.");

					for (this.SubchunkIndex = 0; this.SubchunkIndex < this.subchunksLength; this.SubchunkIndex++)
					{
						if (!this.NextSubchunk(oldVoxel, newVoxel))
						{
							return false;
						}
					}
				}
				else
				{
					// old is uniform, new is not uniform
					var oldVoxel = oldChunk.UniformVoxel;
					for (this.SubchunkIndex = 0; this.SubchunkIndex < this.subchunksLength; this.SubchunkIndex++)
					{
						var newSubchunk = newChunk[this.SubchunkIndex];
						if (newSubchunk.IsUniform)
						{
							if (newSubchunk.UniformVoxel != oldVoxel)
							{
								if (!this.NextSubchunk(oldVoxel, newSubchunk.UniformVoxel))
								{
									return false;
								}
							}
						}
						else
						{
							if (!this.NextSubchunk(oldVoxel, newSubchunk.Voxels))
							{
								return false;
							}
						}
					}
				}
			}
			else
			{
				if (newChunk.IsUniform)
				{
					// old is not uniform, new is uniform
					var newVoxel = newChunk.UniformVoxel;
					for (this.SubchunkIndex = 0; this.SubchunkIndex < this.subchunksLength; this.SubchunkIndex++)
					{
						var oldSubchunk = oldChunk[this.SubchunkIndex];
						if (oldSubchunk.IsUniform)
						{
							if (oldSubchunk.UniformVoxel != newVoxel)
							{
								if (!this.NextSubchunk(oldSubchunk.UniformVoxel, newVoxel))
								{
									return false;
								}
							}
						}
						else
						{
							if (!this.NextSubchunk(oldSubchunk.Voxels, newVoxel))
							{
								return false;
							}
						}
					}
				}
				else
				{
					// both are not uniform
					for (this.SubchunkIndex = 0; this.SubchunkIndex < this.subchunksLength; this.SubchunkIndex++)
					{
						var oldSubchunk = oldChunk[this.SubchunkIndex];
						var newSubchunk = newChunk[this.SubchunkIndex];

						if (oldSubchunk.IsUniform)
						{
							if (newSubchunk.IsUniform)
							{
								if (oldSubchunk.UniformVoxel != newSubchunk.UniformVoxel)
								{
									if (!this.NextSubchunk(oldSubchunk.UniformVoxel, newSubchunk.UniformVoxel))
									{
										return false;
									}
								}
							}
							else
							{
								if (!this.NextSubchunk(oldSubchunk.UniformVoxel, newSubchunk.Voxels))
								{
									return false;
								}
							}
						}
						else
						{
							if (newSubchunk.IsUniform)
							{
								if (!this.NextSubchunk(oldSubchunk.Voxels, newSubchunk.UniformVoxel))
								{
									return false;
								}
							}
							else
							{
								if (oldSubchunk.Voxels != newSubchunk.Voxels)
								{
									if (!this.NextSubchunk(oldSubchunk.Voxels, newSubchunk.Voxels))
									{
										return false;
									}
								}
							}
						}
					}
				}
			}
		}
		finally
		{
			this.ChunkFinished();
		}

		return true;
	}

	protected abstract bool NextSubchunk(Voxel oldUniformVoxel, Voxel newUniformVoxel);

	protected abstract bool NextSubchunk(Voxel oldUniformVoxel, ReadOnlyArray<Voxel> newVoxels);

	protected abstract bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, Voxel newUniformVoxel);

	protected abstract bool NextSubchunk(ReadOnlyArray<Voxel> oldVoxels, ReadOnlyArray<Voxel> newVoxels);

	protected virtual void ChunkStarted()
	{
	}

	protected virtual void ChunkFinished()
	{
	}
}
