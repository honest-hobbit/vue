﻿namespace HQS.VUE.OLD;

internal class VoxelChunkPools : AbstractDisposable,
	ITypeResolver<IPool<ModifiedVoxelChunk>>,
	ITypeResolver<IPool<VoxelChunk>>,
	ITypeResolver<IPool<VoxelSubchunk[]>>,
	ITypeResolver<IPool<Voxel[]>>
{
	private readonly ExpiryPool<VoxelChunk> chunks;

	private readonly ThreadSafeCompactingQueue<CacheExpiration> subchunksExpiryQueue;

	public VoxelChunkPools(VoxelSizeConfig sizes, UniverseConfig config)
	{
		Debug.Assert(sizes != null);
		Debug.Assert(config != null);

		this.chunks = new ExpiryPool<VoxelChunk>(new ThreadSafePool<VoxelChunk>(() => new VoxelChunk()));
		this.ModifiedChunks = new ThreadSafePool<ModifiedVoxelChunk>(CreateModifiedChunks(sizes));

		this.subchunksExpiryQueue = new ThreadSafeCompactingQueue<CacheExpiration>(x => x.IsStale);

		this.SubchunkArrays = new ExpiryPool<VoxelSubchunk[]>(
			new ThreadSafePool<VoxelSubchunk[]>(
				PopulateUtility.GetCreateArraysFunc<VoxelSubchunk>(sizes.SubchunkIndexer.Length)),
			this.subchunksExpiryQueue);

		this.VoxelArrays = new ExpiryPool<Voxel[]>(
			new ThreadSafePool<Voxel[]>(
				PopulateUtility.GetCreateArraysFunc<Voxel>(sizes.VoxelIndexer.Length)),
			this.subchunksExpiryQueue);
	}

	public IPool<ModifiedVoxelChunk> ModifiedChunks { get; }

	public IPool<VoxelChunk> Chunks => this.chunks;

	public IPool<VoxelSubchunk[]> SubchunkArrays { get; }

	public IPool<Voxel[]> VoxelArrays { get; }

	/// <inheritdoc />
	IPool<ModifiedVoxelChunk> ITypeResolver<IPool<ModifiedVoxelChunk>>.GetInstance() => this.ModifiedChunks;

	/// <inheritdoc />
	IPool<VoxelChunk> ITypeResolver<IPool<VoxelChunk>>.GetInstance() => this.Chunks;

	/// <inheritdoc />
	IPool<VoxelSubchunk[]> ITypeResolver<IPool<VoxelSubchunk[]>>.GetInstance() => this.SubchunkArrays;

	/// <inheritdoc />
	IPool<Voxel[]> ITypeResolver<IPool<Voxel[]>>.GetInstance() => this.VoxelArrays;

	public void EnqueueChunkExpiration(VoxelChunk chunk, CacheExpiration token)
	{
		Debug.Assert(chunk != null);

		this.chunks.AddExpiration(token);

		if (chunk.Subchunks != null)
		{
			// expiry queue is shared for both subchunk arrays and voxel arrays because
			// if the chunk is nonuniform then likely at least 1 subchunk is also nonuniform
			this.subchunksExpiryQueue.Enqueue(token);
		}
	}

	public void ResetAndGive(VoxelChunk readChunk)
	{
		Debug.Assert(readChunk != null);

		var subchunks = readChunk.Subchunks;
		if (subchunks != null)
		{
			this.ResetAndGive(subchunks);
			readChunk.Subchunks = null;
		}

		readChunk.ClearAndReset();
		this.chunks.Return(readChunk);
	}

	public void ResetAndGive(ModifiedVoxelChunk chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Read != null);
		Debug.Assert(chunk.Write != null);

		var writeSubchunks = chunk.Write.Subchunks;
		if (writeSubchunks != null)
		{
			var readSubchunks = chunk.Read.Subchunks;
			if (readSubchunks != null)
			{
				this.ResetAndGiveIfNotShared(writeSubchunks, readSubchunks);
			}
			else
			{
				this.ResetAndGive(writeSubchunks);
			}

			chunk.Write.Subchunks = null;
		}

		chunk.Write.ClearAndReset();
		this.Chunks.Return(chunk.Write);
		chunk.Clear();
		this.ModifiedChunks.Return(chunk);
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.ModifiedChunks.ReleaseAll();
		this.Chunks.ReleaseAll();
		this.SubchunkArrays.ReleaseAll();
		this.VoxelArrays.ReleaseAll();
	}

	private static Func<ModifiedVoxelChunk> CreateModifiedChunks(VoxelSizeConfig config) => () => new ModifiedVoxelChunk(config);

	private void ResetAndGive(VoxelSubchunk[] subchunks)
	{
		Debug.Assert(subchunks != null);

		for (int i = 0; i < subchunks.Length; i++)
		{
			var voxels = subchunks[i].Voxels;
			if (voxels != null)
			{
				// voxel arrays are always copied over when taken from the pool so no need to clear it here
				this.VoxelArrays.Return(voxels);
			}

			// subchunk arrays are never shared and they hold onto references to voxel arrays, thus need to be cleared here
			subchunks[i] = VoxelSubchunk.Empty;
		}

		this.SubchunkArrays.Return(subchunks);
	}

	// writeSubchunks is what is being returned, while readSubchunks is what is kept
	private void ResetAndGiveIfNotShared(VoxelSubchunk[] writeSubchunks, VoxelSubchunk[] readSubchunks)
	{
		Debug.Assert(writeSubchunks != null);
		Debug.Assert(readSubchunks != null);

		for (int i = 0; i < writeSubchunks.Length; i++)
		{
			var writeVoxels = writeSubchunks[i].Voxels;
			var readVoxels = readSubchunks[i].Voxels;

			if (writeVoxels != null && writeVoxels != readVoxels)
			{
				// voxel arrays are always copied over when taken from the pool so no need to clear it here
				this.VoxelArrays.Return(writeVoxels);
			}

			// subchunk arrays are never shared and they hold onto references to voxel arrays, thus need to be cleared here
			writeSubchunks[i] = VoxelSubchunk.Empty;
		}

		this.SubchunkArrays.Return(writeSubchunks);
	}
}
