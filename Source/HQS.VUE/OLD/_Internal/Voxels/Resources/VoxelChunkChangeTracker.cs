﻿namespace HQS.VUE.OLD;

internal class VoxelChunkChangeTracker : IKeyed<Guid>
{
	private readonly VoxelChunkPools pools;

	private readonly Dictionary<Int3, ModifiedVoxelChunk> chunksByKey;

	private readonly List<ModifiedVoxelChunk> chunks;

	private readonly List<ReadOnlyPin<ReadOnlyVoxelChunk>> pins;

	private readonly Predicate<ModifiedVoxelChunk> tryCleanupAndRemoveChunk;

	public VoxelChunkChangeTracker(VoxelChunkPools pools, int chunkCapacity)
	{
		Debug.Assert(pools != null);
		Debug.Assert(chunkCapacity >= 0);

		this.pools = pools;
		this.chunksByKey = new Dictionary<Int3, ModifiedVoxelChunk>(chunkCapacity, MortonCurve.EqualityComparer.ForInt3);
		this.chunks = new List<ModifiedVoxelChunk>(chunkCapacity);
		this.pins = new List<ReadOnlyPin<ReadOnlyVoxelChunk>>(chunkCapacity);

		this.Chunks = ReadOnlyList.ConvertReadOnly(this.chunks, x => x.AsChangeset);
		this.Pins = ReadOnlyList.ConvertReadOnly(this.pins, x => x.AsView);
		this.tryCleanupAndRemoveChunk = this.TryCleanupAndRemoveChunk;
	}

	/// <inheritdoc />
	public Guid Key { get; set; }

	// this will not contain any duplicates,
	// call CleanupChanges to remove any uneeded chunks before accessing this
	public IReadOnlyList<VoxelChunkChangeset> Chunks { get; }

	// this will not contain any duplicates,
	public IReadOnlyList<ReadOnlyPinView<ReadOnlyVoxelChunk>> Pins { get; }

	public bool TryGetChunk(Int3 key, out ModifiedVoxelChunk chunk) => this.chunksByKey.TryGetValue(key, out chunk);

	public void AddChunk(ModifiedVoxelChunk chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Read != null);
		Debug.Assert(chunk.Read.Key.ChunkGrid == this.Key);
		Debug.Assert(chunk.Write != null);
		Debug.Assert(chunk.Read.Key == chunk.Write.Key);
		Debug.Assert(!this.chunksByKey.ContainsKey(chunk.Read.Key.Location));

		this.chunks.Add(chunk);
		this.chunksByKey.Add(chunk.Read.Key.Location, chunk);
	}

	public void CleanupChanges()
	{
		this.chunks.RemoveAll(this.tryCleanupAndRemoveChunk);
	}

	public void CommitChanges()
	{
		Debug.Assert(this.pins.Count == 0);

		// swap the data within the chunks
		int max = this.chunks.Count;
		for (int i = 0; i < max; i++)
		{
			var chunk = this.chunks[i];
			this.pins.Add(new ReadOnlyPin<ReadOnlyVoxelChunk>(
				chunk.ReadPin.CreatePin().CastTo<IChunk<ReadOnlyVoxelChunk>>()));

			var writeChunk = chunk.Write;
			var readChunk = chunk.Read;

			// TODO just swap the writeChunk and readChunk references (once switched over to Chunk<T> system)
			var tempUniform = readChunk.UniformVoxel;
			var tempSubchunks = readChunk.Subchunks;

			readChunk.UniformVoxel = writeChunk.UniformVoxel;
			readChunk.Subchunks = writeChunk.Subchunks;

			writeChunk.UniformVoxel = tempUniform;
			writeChunk.Subchunks = tempSubchunks;
		}
	}

	public void PostCommitCleanup()
	{
		// this methods works for returning the correct subchunks to the pool
		// regardless of whether or not CommitChanges was called
		int max = this.chunks.Count;
		for (int i = 0; i < max; i++)
		{
			this.pools.ResetAndGive(this.chunks[i]);
		}

		this.chunks.Clear();
		this.chunksByKey.Clear();
	}

	public void Clear()
	{
		if (this.chunks.Count > 0)
		{
			Debug.Assert(this.pins.Count == 0);

			// an exception was thrown while running the voxel job or Unity rejected the changes
			// so CommitChanges and PostCommitCleanup were never called
			this.PostCommitCleanup();
		}
		else
		{
			int max = this.pins.Count;
			for (int i = 0; i < max; i++)
			{
				this.pins[i].Unpin();
			}

			this.pins.Clear();
		}
	}

	#region Cleanup Chunks

	// return false to keep the chunk, true to remove it because it matches the read chunk
	private bool TryCleanupAndRemoveChunk(ModifiedVoxelChunk chunk)
	{
		if (this.TryCleanupAndKeepChunk(chunk))
		{
			chunk.SetChangedSubchunks();
			return false;
		}
		else
		{
			this.chunksByKey.Remove(chunk.Read.Key.Location);

			// this handles unpinning the chunk to match the pin when added to tracker
			this.pools.ResetAndGive(chunk);
			return true;
		}
	}

	// This returns the opposite of TryCleanupAndRemoveChunk.
	// Returns true to keep the chunk, false to remove it because it matches the read chunk.
	private bool TryCleanupAndKeepChunk(ModifiedVoxelChunk chunk)
	{
		Debug.Assert(chunk != null);

		var writeChunk = chunk.Write;
		var readChunk = chunk.Read;

		if (writeChunk.Subchunks == null)
		{
			// write is uniform
			if (readChunk.Subchunks == null && writeChunk.UniformVoxel == readChunk.UniformVoxel)
			{
				return false;
			}

			return true;
		}

		if (readChunk.Subchunks == null)
		{
			return this.TryCleanupNonuniformWriteAndUniformRead(writeChunk, readChunk.UniformVoxel);
		}
		else
		{
			return this.TryCleanupBothNonuniform(writeChunk, readChunk.Subchunks);
		}
	}

	private bool TryCleanupNonuniformWriteAndUniformRead(VoxelChunk writeChunk, Voxel readUniformVoxel)
	{
		Debug.Assert(writeChunk != null);
		Debug.Assert(writeChunk.Subchunks != null);

		var subchunks = writeChunk.Subchunks;

		for (int i = 0; i < subchunks.Length; i++)
		{
			// try to unify each subchunk
			var voxels = subchunks[i].Voxels;
			if (voxels != null && voxels.IsUniform(out var uniformVoxel))
			{
				subchunks[i] = new VoxelSubchunk(uniformVoxel);
				this.pools.VoxelArrays.Return(voxels);
			}
		}

		if (this.TryUnifySubchunksTogether(writeChunk))
		{
			if (writeChunk.UniformVoxel == readUniformVoxel)
			{
				// read and write chunks are identical now
				return false;
			}
		}

		return true;
	}

	private bool TryCleanupBothNonuniform(VoxelChunk writeChunk, VoxelSubchunk[] readSubchunks)
	{
		Debug.Assert(writeChunk != null);
		Debug.Assert(writeChunk.Subchunks != null);
		Debug.Assert(readSubchunks != null);

		var writeSubchunks = writeChunk.Subchunks;

		for (int i = 0; i < writeSubchunks.Length; i++)
		{
			var writeSubchunk = writeSubchunks[i];
			var readSubchunk = readSubchunks[i];

			if (writeSubchunk.Voxels == readSubchunk.Voxels || writeSubchunk.Voxels == null)
			{
				// either
				//  1) both subchunks are uniform
				//  2) both point to the same voxels array object
				//  3) write subchunk is already uniform
				// either way no need to try to unify anything
				continue;
			}

			// check if write voxels are equal to read voxels
			if (readSubchunk.Voxels != null && writeSubchunk.Voxels.VoxelsEqual(readSubchunk.Voxels))
			{
				// voxel arrays have equivalent voxels so set write to point to the same read array
				writeSubchunks[i] = new VoxelSubchunk(readSubchunk.Voxels);
				this.pools.VoxelArrays.Return(writeSubchunk.Voxels);
				continue;
			}

			// try to unify the write voxels
			if (writeSubchunk.Voxels.IsUniform(out var uniformSubchunkVoxel))
			{
				writeSubchunks[i] = new VoxelSubchunk(uniformSubchunkVoxel);
				this.pools.VoxelArrays.Return(writeSubchunk.Voxels);
			}
		}

		if (writeSubchunks.SubchunksExactMatch(readSubchunks))
		{
			return false;
		}

		this.TryUnifySubchunksTogether(writeChunk);
		return true;
	}

	private bool TryUnifySubchunksTogether(VoxelChunk writeChunk)
	{
		Debug.Assert(writeChunk != null);
		Debug.Assert(writeChunk.Subchunks != null);

		var subchunks = writeChunk.Subchunks;
		if (subchunks.IsUniform(out var uniformVoxel))
		{
			writeChunk.UniformVoxel = uniformVoxel;
			writeChunk.Subchunks = null;
			this.pools.SubchunkArrays.Return(subchunks);
			return true;
		}

		return false;
	}

	#endregion
}
