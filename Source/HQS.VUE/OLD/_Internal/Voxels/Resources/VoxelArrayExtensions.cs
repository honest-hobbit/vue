﻿namespace HQS.VUE.OLD;

internal static class VoxelArrayExtensions
{
	public static bool VoxelsEqual(this Voxel[] voxels, Voxel[] other)
	{
		Debug.Assert(voxels != null);
		Debug.Assert(other != null);
		Debug.Assert(voxels.Length == other.Length);

		for (int i = 0; i < voxels.Length; i++)
		{
			if (voxels[i] != other[i])
			{
				return false;
			}
		}

		return true;
	}

	// checks if the voxel arrays are the exact same object in memory,
	// it does not check if 2 different arrays contain the same voxels
	public static bool SubchunksExactMatch(this VoxelSubchunk[] subchunks, VoxelSubchunk[] other)
	{
		Debug.Assert(subchunks != null);
		Debug.Assert(other != null);
		Debug.Assert(subchunks.Length == other.Length);

		for (int i = 0; i < subchunks.Length; i++)
		{
			if (!subchunks[i].IsExactMatch(other[i]))
			{
				return false;
			}
		}

		return true;
	}

	public static bool IsUniform(this Voxel[] voxels, out Voxel uniformVoxel)
	{
		Debug.Assert(voxels != null);

		uniformVoxel = voxels[0];
		for (int i = 1; i < voxels.Length; i++)
		{
			if (voxels[i] != uniformVoxel)
			{
				uniformVoxel = Voxel.Empty;
				return false;
			}
		}

		return true;
	}

	public static bool IsUniform(this VoxelSubchunk[] subchunks, out Voxel uniformVoxel)
	{
		Debug.Assert(subchunks != null);

		var subchunk = subchunks[0];
		if (subchunk.Voxels != null)
		{
			uniformVoxel = Voxel.Empty;
			return false;
		}

		uniformVoxel = subchunk.UniformVoxel;
		for (int i = 1; i < subchunks.Length; i++)
		{
			subchunk = subchunks[i];
			if (subchunk.Voxels != null || subchunk.UniformVoxel != uniformVoxel)
			{
				uniformVoxel = Voxel.Empty;
				return false;
			}
		}

		return true;
	}
}
