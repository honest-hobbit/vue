﻿namespace HQS.VUE.OLD;

internal class VoxelChunkCache : AbstractDisposable, IChunkCache
{
	private readonly MemoryCache<ChunkKey, VoxelChunk> cache;

	private readonly ChunkLoaderOLD<IVoxelGridStore, VoxelChunk> loader;

	public VoxelChunkCache(
		VoxelChunkPools pools, ICommandSubmitter<LoadChunkCommandOLD<IVoxelGridStore, VoxelChunk>.Args> loadProcessor)
	{
		Debug.Assert(pools != null);
		Debug.Assert(loadProcessor != null);

		this.cache = new MemoryCache<ChunkKey, VoxelChunk>(new CachingFactory(pools), ChunkKey.Comparer);
		this.loader = new ChunkLoaderOLD<IVoxelGridStore, VoxelChunk>(loadProcessor);
	}

	/// <inheritdoc />
	public int Count => this.cache.Count;

	/// <inheritdoc />
	public int TryClear() => this.cache.GetPinsSnapshot().TryExpireAll();

	/// <inheritdoc />
	public IEnumerable<ChunkCacheEntry> GetChunksSnapshot()
	{
		foreach (var entry in this.cache.GetPinsSnapshot())
		{
			yield return new ChunkCacheEntry(entry.Key, entry.PinCount);
		}
	}

	public Pin<VoxelChunk> GetChunkPin(ChunkKey key, IVoxelGridStore store, bool waitUntilLoaded)
	{
		Debug.Assert(!this.IsDisposed);

		var cached = this.cache.GetCached(key);
		bool isLoading = this.loader.CheckLoading(cached, store);
		if (isLoading && waitUntilLoaded)
		{
			cached.Pin.Value.Loading.Wait();
		}

		return cached.Pin;
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.TryClear();

	private class CachingFactory : ICacheValueFactory<ChunkKey, VoxelChunk>
	{
		private readonly VoxelChunkPools pools;

		public CachingFactory(VoxelChunkPools pools)
		{
			Debug.Assert(pools != null);

			this.pools = pools;
		}

		/// <inheritdoc />
		public VoxelChunk CreateValue(ChunkKey key) => this.pools.Chunks.Rent();

		/// <inheritdoc />
		public void EnqueueToken(ChunkKey key, VoxelChunk chunk, CacheExpiration token) =>
			this.pools.EnqueueChunkExpiration(chunk, token);

		/// <inheritdoc />
		public void ValueExpired(ChunkKey key, VoxelChunk chunk) => this.pools.ResetAndGive(chunk);
	}
}
