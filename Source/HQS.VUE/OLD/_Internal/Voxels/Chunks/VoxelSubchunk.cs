﻿namespace HQS.VUE.OLD;

internal readonly struct VoxelSubchunk
{
	public VoxelSubchunk(Voxel uniformVoxel)
	{
		this.UniformVoxel = uniformVoxel;
		this.Voxels = null;
	}

	public VoxelSubchunk(Voxel[] voxels)
	{
		Debug.Assert(voxels != null);

		this.UniformVoxel = Voxel.Empty;
		this.Voxels = voxels;
	}

	public static VoxelSubchunk Empty => default;

	public Voxel UniformVoxel { get; }

	public Voxel[] Voxels { get; }

	public ReadOnlyVoxelSubchunk AsReadOnly => new ReadOnlyVoxelSubchunk(this);

	// checks if the voxel arrays are the exact same object in memory,
	// it does not check if 2 different arrays contain the same voxels
	public bool IsExactMatch(VoxelSubchunk other)
	{
		if (this.Voxels != other.Voxels)
		{
			// both voxel arrays aren't null or don't both point to the same array object
			return false;
		}

		if (this.Voxels == null)
		{
			// both are uniform, so check if their uniform voxels are the same
			return this.UniformVoxel == other.UniformVoxel;
		}

		// both are not uniform and both point to the same array object
		return true;
	}
}
