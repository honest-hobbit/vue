﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyVoxelChunk : IKeyed<ChunkKey>
{
	private readonly VoxelChunk chunk;

	public ReadOnlyVoxelChunk(VoxelChunk chunk)
	{
		Debug.Assert(chunk != null);

		this.chunk = chunk;
	}

	public static ReadOnlyVoxelChunk Null => default;

	public bool IsNull => this.chunk == null;

	/// <inheritdoc />
	public ChunkKey Key => this.chunk.Key;

	public Guid EntityKey => this.chunk.RecordKey;

	public bool IsUniform => this.chunk.Subchunks == null;

	public Voxel UniformVoxel
	{
		get
		{
			Debug.Assert(this.IsUniform);
			return this.chunk.UniformVoxel;
		}
	}

	public int Count => this.chunk.Subchunks?.Length ?? 0;

	public IEnumerable<ReadOnlyVoxelSubchunk> Subchunks
	{
		get
		{
			Debug.Assert(!this.IsUniform);
			return this.chunk;
		}
	}

	public ReadOnlyVoxelSubchunk this[int index]
	{
		get
		{
			Debug.Assert(!this.IsUniform);
			return this.chunk.Subchunks[index].AsReadOnly;
		}
	}

	public bool IsUniformly(Voxel voxel) => this.IsUniform && this.UniformVoxel == voxel;

	public ReadOnlyVoxelSubchunk GetUniformVoxelAsSubchunk() => new ReadOnlyVoxelSubchunk(new VoxelSubchunk(this.UniformVoxel));

	public ReadOnlyVoxelSubchunk GetSubchunkOrUniformVoxelAsSubchunk(int index) =>
		new ReadOnlyVoxelSubchunk(this.chunk.Subchunks?[index] ?? new VoxelSubchunk(this.UniformVoxel));
}
