﻿namespace HQS.VUE.OLD;

internal class VoxelChunk : IChunk<ReadOnlyVoxelChunk>, IEnumerable<ReadOnlyVoxelSubchunk>
{
	public Voxel UniformVoxel { get; set; }

	public VoxelSubchunk[] Subchunks { get; set; }

	public ReadOnlyVoxelChunk AsReadOnly => new ReadOnlyVoxelChunk(this);

	/// <inheritdoc />
	public ChunkKey Key { get; set; }

	/// <inheritdoc />
	public Guid RecordKey { get; set; }

	/// <inheritdoc />
	public bool HasData { get; set; }

	/// <inheritdoc />
	public ChunkLoadingSignal Loading { get; } = new ChunkLoadingSignal();

	/// <inheritdoc />
	public void SetToEmptyChunk()
	{
		Debug.Assert(this.Subchunks == null);

		////this.Key = default;
		this.RecordKey = Guid.Empty;
		this.HasData = false;
		this.UniformVoxel = Voxel.Empty;
	}

	/// <inheritdoc />
	IEnumerator<ReadOnlyVoxelSubchunk> IEnumerable<ReadOnlyVoxelSubchunk>.GetEnumerator()
	{
		Debug.Assert(this.Subchunks != null);

		var subchunks = this.Subchunks;
		for (int index = 0; index < subchunks.Length; index++)
		{
			yield return subchunks[index].AsReadOnly;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => throw new NotSupportedException();
}
