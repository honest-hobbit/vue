﻿namespace HQS.VUE.OLD;

internal class ModifiedVoxelChunk
{
	private readonly ushort[] changedSubchunks;

	private Pin<VoxelChunk> read;

	private VoxelChunk write;

	public ModifiedVoxelChunk(VoxelSizeConfig config)
	{
		Debug.Assert(config != null);

		this.changedSubchunks = new ushort[config.SubchunkIndexer.Length];
	}

	public Pinned<VoxelChunk> ReadPin => this.read.AsPinned;

	public VoxelChunk Read => this.read.Value;

	public VoxelChunk Write
	{
		get
		{
			Debug.Assert(this.write != null);

			return this.write;
		}
	}

	public int ChangedSubchunksCount { get; private set; }

	// all changed subchunks appear at the beginning, unchanged subchunks appear after
	// each subchunk appears only once
	public ReadOnlyArray<ushort> ChangedSubchunks => new ReadOnlyArray<ushort>(this.changedSubchunks);

	public VoxelChunkChangeset AsChangeset => new VoxelChunkChangeset(this);

	public void SetChunks(Pinned<VoxelChunk> readChunk, VoxelChunk writeChunk)
	{
		Debug.Assert(this.read.IsUnassigned);
		Debug.Assert(this.write == null);
		Debug.Assert(readChunk.IsPinned);
		Debug.Assert(readChunk.Value.IsKeyAssigned());
		Debug.Assert(writeChunk != null);
		Debug.Assert(!writeChunk.IsKeyAssigned());

		writeChunk.Key = readChunk.Value.Key;
		this.read = readChunk.CreatePin();
		this.write = writeChunk;
	}

	public void Clear()
	{
		Debug.Assert(this.read.IsPinned);
		Debug.Assert(this.write != null);

		this.read.Dispose();
		this.read = default;
		this.write = null;
		this.ChangedSubchunksCount = 0;
	}

	public void SetChangedSubchunks()
	{
		Debug.Assert(this.read.IsPinned);
		Debug.Assert(this.write != null);

		var length = this.changedSubchunks.Length;
		var previous = this.Read;
		var previousSubchunks = previous.Subchunks;
		var previousUniform = previous.UniformVoxel;
		var currentSubchunks = this.Write.Subchunks;
		var currentUniform = this.Write.UniformVoxel;
		int assignToIndex = 0;

		if (previousSubchunks == null)
		{
			if (currentSubchunks == null)
			{
				// both uniform
				Debug.Assert(previousUniform != currentUniform);

				// all subchunks have changed
				for (int i = 0; i < length; i++)
				{
					this.changedSubchunks[assignToIndex] = (ushort)i;
					assignToIndex++;
				}

				this.ChangedSubchunksCount = assignToIndex;
			}
			else
			{
				// previous uniform, current nonuniform
				// set all changed subchunks first
				for (int i = 0; i < length; i++)
				{
					var subchunk = currentSubchunks[i];
					if (subchunk.Voxels != null || subchunk.UniformVoxel != previousUniform)
					{
						this.changedSubchunks[assignToIndex] = (ushort)i;
						assignToIndex++;
					}
				}

				this.ChangedSubchunksCount = assignToIndex;

				// set all unchanged subchunks after
				for (int i = 0; i < length; i++)
				{
					var subchunk = currentSubchunks[i];
					if (!(subchunk.Voxels != null || subchunk.UniformVoxel != previousUniform))
					{
						this.changedSubchunks[assignToIndex] = (ushort)i;
						assignToIndex++;
					}
				}
			}
		}
		else
		{
			if (currentSubchunks == null)
			{
				// previous nonuniform, current uniform
				// set all changed subchunks first
				for (int i = 0; i < length; i++)
				{
					var subchunk = previousSubchunks[i];
					if (subchunk.Voxels != null || subchunk.UniformVoxel != currentUniform)
					{
						this.changedSubchunks[assignToIndex] = (ushort)i;
						assignToIndex++;
					}
				}

				this.ChangedSubchunksCount = assignToIndex;

				// set all unchanged subchunks after
				for (int i = 0; i < length; i++)
				{
					var subchunk = previousSubchunks[i];
					if (!(subchunk.Voxels != null || subchunk.UniformVoxel != currentUniform))
					{
						this.changedSubchunks[assignToIndex] = (ushort)i;
						assignToIndex++;
					}
				}
			}
			else
			{
				// both not uniform
				// set all changed subchunks first
				for (int i = 0; i < length; i++)
				{
					if (!previousSubchunks[i].IsExactMatch(currentSubchunks[i]))
					{
						this.changedSubchunks[assignToIndex] = (ushort)i;
						assignToIndex++;
					}
				}

				this.ChangedSubchunksCount = assignToIndex;

				// set all unchanged subchunks after
				for (int i = 0; i < length; i++)
				{
					if (previousSubchunks[i].IsExactMatch(currentSubchunks[i]))
					{
						this.changedSubchunks[assignToIndex] = (ushort)i;
						assignToIndex++;
					}
				}
			}
		}
	}
}
