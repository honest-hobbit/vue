﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyVoxelSubchunk
{
	private readonly Voxel uniformVoxel;

	public ReadOnlyVoxelSubchunk(VoxelSubchunk subchunk)
	{
		this.uniformVoxel = subchunk.UniformVoxel;
		this.Voxels = new ReadOnlyArray<Voxel>(subchunk.Voxels);
	}

	public bool IsUniform => this.Voxels.IsDefault;

	public Voxel UniformVoxel
	{
		get
		{
			Debug.Assert(this.IsUniform);
			return this.uniformVoxel;
		}
	}

	public ReadOnlyArray<Voxel> Voxels { get; }
}
