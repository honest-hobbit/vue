﻿namespace HQS.VUE.OLD;

internal readonly struct VoxelChunkChangeset : IKeyed<ChunkKey>
{
	private readonly ModifiedVoxelChunk chunk;

	public VoxelChunkChangeset(ModifiedVoxelChunk chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Read != null);
		Debug.Assert(chunk.Write != null);

		this.chunk = chunk;
	}

	public static VoxelChunkChangeset Null => default;

	public bool IsNull => this.chunk == null;

	/// <inheritdoc />
	public ChunkKey Key => this.chunk.Write.Key;

	public ReadOnlyVoxelChunk Previous => this.chunk.Read.AsReadOnly;

	public ReadOnlyVoxelChunk Current => this.chunk.Write.AsReadOnly;

	public int ChangedSubchunksCount => this.chunk.ChangedSubchunksCount;

	// all changed subchunks appear at the beginning, unchanged subchunks appear after
	// each subchunk appears only once
	public ReadOnlyArray<ushort> ChangedSubchunks => this.chunk.ChangedSubchunks;
}
