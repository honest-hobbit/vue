﻿namespace HQS.VUE.OLD;

internal class VoxelSerDes : CompositeSerDes<ushort, Voxel>
{
	private VoxelSerDes()
		: base(VarintSerDes.ForUnsigned)
	{
	}

	public static ISerDes<Voxel> Instance { get; } = new VoxelSerDes();

	/// <inheritdoc />
	protected override Voxel ComposeValue(ushort part) => new Voxel(part);

	/// <inheritdoc />
	protected override void DecomposeValue(Voxel voxel, out ushort part) => part = voxel.TypeIndex;
}
