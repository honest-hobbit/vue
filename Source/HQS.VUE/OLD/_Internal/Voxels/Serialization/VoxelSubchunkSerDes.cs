﻿namespace HQS.VUE.OLD;

internal class VoxelSubchunkSerDes : ISerializer<ReadOnlyVoxelSubchunk>, IDeserializer<VoxelSubchunk>
{
	private readonly ArraySerDes<Voxel> voxelArraySerDes;

	private readonly VoxelChunkPools pools;

	public VoxelSubchunkSerDes(VoxelSizeConfig config, VoxelChunkPools pools)
	{
		Debug.Assert(config != null);
		Debug.Assert(pools != null);

		this.pools = pools;
		this.voxelArraySerDes = new ArraySerDes<Voxel>(EnumerableSerDesStrategy.RunLengthEncoded(
			VoxelSerDes.Instance,
			new RunLengthEncoderOptions<Voxel>()
			{
				CountSerDes = SerializeCount.AsConstant(config.VoxelIndexer.Length),
				RunLengthSerDes = SerializeCount.AsSignedVarint,
				Comparer = EqualityComparer.ForStruct<Voxel>(),
			}));
	}

	/// <inheritdoc />
	public void Serialize(ReadOnlyVoxelSubchunk subchunk, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(subchunk, writeByte);

		bool isUniform = subchunk.IsUniform;
		SerDes.OfBool.Serialize(isUniform, writeByte);
		if (isUniform)
		{
			VoxelSerDes.Instance.Serialize(subchunk.UniformVoxel, writeByte);
		}
		else
		{
			this.voxelArraySerDes.Serialize(subchunk.Voxels, writeByte);
		}
	}

	/// <inheritdoc />
	public VoxelSubchunk Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		bool isUniform = SerDes.OfBool.Deserialize(readByte);
		if (isUniform)
		{
			var uniformVoxel = VoxelSerDes.Instance.Deserialize(readByte);
			return new VoxelSubchunk(uniformVoxel);
		}
		else
		{
			var voxels = this.pools.VoxelArrays.Rent();
			this.voxelArraySerDes.DeserializeInline(readByte, voxels);
			return new VoxelSubchunk(voxels);
		}
	}
}
