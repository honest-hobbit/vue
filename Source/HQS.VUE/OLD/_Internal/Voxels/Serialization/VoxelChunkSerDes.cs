﻿namespace HQS.VUE.OLD;

internal class VoxelChunkSerDes : ISerializer<ReadOnlyVoxelChunk>, IInlineDeserializer<VoxelChunk>
{
	private readonly ArraySerDes<ReadOnlyVoxelSubchunk, VoxelSubchunk> subchunkArraySerDes;

	private readonly VoxelChunkPools pools;

	public VoxelChunkSerDes(VoxelSizeConfig config, VoxelChunkPools pools)
	{
		Debug.Assert(config != null);
		Debug.Assert(pools != null);

		this.pools = pools;
		var subchunkSerDes = new VoxelSubchunkSerDes(config, pools);
		this.subchunkArraySerDes = new ArraySerDes<ReadOnlyVoxelSubchunk, VoxelSubchunk>(
			EnumerableSerDesStrategy.RunLengthEncoded(
				subchunkSerDes,
				subchunkSerDes,
				new RunLengthEncoderOptions<ReadOnlyVoxelSubchunk>()
				{
					CountSerDes = SerializeCount.AsConstant(config.SubchunkIndexer.Length),
					RunLengthSerDes = SerializeCount.AsSignedVarint,
					Comparer = SubchunkComparer.Instance,
				}));
	}

	/// <inheritdoc />
	public void Serialize(ReadOnlyVoxelChunk chunk, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(chunk, writeByte);

		bool isUniform = chunk.IsUniform;
		SerDes.OfBool.Serialize(isUniform, writeByte);
		if (isUniform)
		{
			VoxelSerDes.Instance.Serialize(chunk.UniformVoxel, writeByte);
		}
		else
		{
			this.subchunkArraySerDes.Serialize(chunk.Subchunks, writeByte);
		}
	}

	/// <inheritdoc />
	public void DeserializeInline(Func<byte> readByte, VoxelChunk chunk)
	{
		IInlineDeserializerContracts.DeserializeInline(readByte, chunk);
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.UniformVoxel == Voxel.Empty);
		Debug.Assert(chunk.Subchunks == null);

		bool isUniform = SerDes.OfBool.Deserialize(readByte);
		if (isUniform)
		{
			chunk.UniformVoxel = VoxelSerDes.Instance.Deserialize(readByte);
		}
		else
		{
			chunk.Subchunks = this.pools.SubchunkArrays.Rent();
			this.subchunkArraySerDes.DeserializeInline(readByte, chunk.Subchunks);
		}
	}

	private class SubchunkComparer : IEqualityComparer<ReadOnlyVoxelSubchunk>
	{
		private SubchunkComparer()
		{
		}

		public static SubchunkComparer Instance { get; } = new SubchunkComparer();

		/// <inheritdoc />
		public bool Equals(ReadOnlyVoxelSubchunk x, ReadOnlyVoxelSubchunk y)
		{
			// this only returns true if both subchunks are of the same uniform voxel
			// this is done both because it is unlikely to find 2 nonuniform subchunks that are identical
			// and because serializing them as a single run would cause them to be deserialized sharing the same array
			// which might break other systems (nonuniform subchunks should never share arrays)
			if (x.IsUniform != y.IsUniform)
			{
				return false;
			}

			if (x.IsUniform)
			{
				return x.UniformVoxel == y.UniformVoxel;
			}

			return false;
		}

		/// <inheritdoc />
		public int GetHashCode(ReadOnlyVoxelSubchunk obj) => throw new NotSupportedException();
	}
}
