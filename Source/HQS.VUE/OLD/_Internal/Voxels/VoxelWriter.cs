﻿namespace HQS.VUE.OLD;

internal class VoxelWriter
{
	private readonly VoxelChunkPools pools;

	private readonly VoxelSizeConfig config;

	private readonly VoxelChangesView changesView;

	private readonly int voxelTypesCount;

	private VolumeView volume;

	private Int3 cachedChunkIndex;

	private VoxelChunk readChunk;

	private VoxelChunk writeChunk;

	public VoxelWriter(
		VoxelChunkPools pools,
		VoxelChunkCache cache,
		VoxelSizeConfig config,
		VoxelDefinitions definitions)
	{
		Debug.Assert(pools != null);
		Debug.Assert(cache != null);
		Debug.Assert(config != null);
		Debug.Assert(definitions != null);

		this.pools = pools;
		this.config = config;
		this.changesView = new VoxelChangesView(cache, config, definitions);
		this.voxelTypesCount = definitions.Voxels.Count;

		this.ClearFields();
	}

	public bool IsInitialized => this.volume != null;

	public void Initialize(VolumeView volume)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(volume != null);
		Debug.Assert(volume.IsInitialized);

		this.volume = volume;
		this.changesView.Initialize(volume);
	}

	public void Deinitialize()
	{
		Debug.Assert(this.IsInitialized);

		this.ClearFields();
		this.changesView.Deinitialize();
	}

	public Voxel GetOldVoxel(Int3 index) => this.changesView.GetOldVoxel(index);

	public Voxel GetNewVoxel(Int3 index) => this.changesView.GetNewVoxel(index);

	public Voxel Replace(Int3 voxelIndex, Voxel voxel)
	{
		if (voxel.TypeIndex >= this.voxelTypesCount)
		{
			throw new ArgumentOutOfRangeException(
				nameof(voxel), $"{nameof(voxel.TypeIndex)} is out of range. Voxel type is not defined.");
		}

		int subchunkInt = this.config.ConvertVolumeVoxelToSubchunkInt(voxelIndex);
		int voxelInt = this.config.ConvertVolumeVoxelToSubchunkVoxelInt(voxelIndex);
		var chunkIndex = this.config.ConvertVolumeVoxelToChunkIndex(voxelIndex);

		bool wasChunkAlreadyModified = chunkIndex == this.cachedChunkIndex;
		if (!wasChunkAlreadyModified && this.volume.TryGetModifiedVoxelChunk(chunkIndex, out var modifiedChunk))
		{
			this.cachedChunkIndex = chunkIndex;
			this.readChunk = modifiedChunk.Read;
			this.writeChunk = modifiedChunk.Write;
			wasChunkAlreadyModified = true;
		}

		if (wasChunkAlreadyModified)
		{
			// this chunk has already been written to before
			Debug.Assert(this.readChunk != null);
			Debug.Assert(this.writeChunk != null);

			var currentVoxel = GetVoxel(this.writeChunk, subchunkInt, voxelInt);
			if (currentVoxel == voxel)
			{
				return currentVoxel;
			}

			// TODO VoxelsChanged could maybe be optimized using VoxelChunkChangeTracker.CleanupChanges to count
			// the changes instead of doing it here on every write.
			// writing a different voxel than what is currently there
			var originalVoxel = GetVoxel(this.readChunk, subchunkInt, voxelInt);
			if (currentVoxel == originalVoxel)
			{
				// this hasn't been written to yet so it's a new change
				this.volume.VoxelsChanged++;
			}
			else if (voxel == originalVoxel)
			{
				// voxel being written is actually being reset to match its read state
				this.volume.VoxelsChanged--;
			}
		}
		else
		{
			// this is the first time writing to this chunk
			var readPin = this.changesView.GetReadChunkPin(chunkIndex);
			var tempReadChunk = readPin.Value;

			var currentVoxel = GetVoxel(tempReadChunk, subchunkInt, voxelInt);
			if (currentVoxel == voxel)
			{
				return currentVoxel;
			}

			// this chunk is being writen a changed value
			this.volume.VoxelsChanged++;

			this.cachedChunkIndex = chunkIndex;
			this.readChunk = tempReadChunk;
			this.writeChunk = this.pools.Chunks.Rent();

			modifiedChunk = this.pools.ModifiedChunks.Rent();
			modifiedChunk.SetChunks(readPin, this.writeChunk);
			this.volume.TrackChunk(modifiedChunk);
			this.changesView.OverrideNewChunk(chunkIndex, this.writeChunk.AsReadOnly);

			this.writeChunk.Subchunks = this.pools.SubchunkArrays.Rent();
			if (this.readChunk.Subchunks == null)
			{
				this.writeChunk.Subchunks.SetAllTo(new VoxelSubchunk(this.readChunk.UniformVoxel));
			}
			else
			{
				// create a copy of the subchunks array, but not a deep copy of every voxels array
				this.readChunk.Subchunks.CopyTo(this.writeChunk.Subchunks, 0);
			}
		}

		// beyond this point writeChunk is guaranteed to not be null, that we are writing a new voxel value,
		// and that writeChunk.Subchunks is guaranteed to not be null and to be a deep copy
		Debug.Assert(this.readChunk != null);
		Debug.Assert(this.writeChunk != null);
		Debug.Assert(this.writeChunk.Subchunks != null);
		Debug.Assert(this.writeChunk.Subchunks != this.readChunk.Subchunks);

		var writeSubchunk = this.writeChunk.Subchunks[subchunkInt];
		if (writeSubchunk.Voxels == null)
		{
			// the voxels were uniform but will be nonuniform so create a voxels array with the uniform voxel
			var voxels = this.pools.VoxelArrays.Rent();
			voxels.SetAllTo(writeSubchunk.UniformVoxel);
			writeSubchunk = new VoxelSubchunk(voxels);
			this.writeChunk.Subchunks[subchunkInt] = writeSubchunk;
		}
		else
		{
			// check if the write voxels array is still pointing to the read voxels array
			// aka the write voxels hasn't been written to yet so need to deep copy the voxels array
			if (this.readChunk.Subchunks != null)
			{
				var readSubchunk = this.readChunk.Subchunks[subchunkInt];
				if (readSubchunk.Voxels == writeSubchunk.Voxels)
				{
					var voxels = this.pools.VoxelArrays.Rent();
					readSubchunk.Voxels.CopyTo(voxels, 0);
					writeSubchunk = new VoxelSubchunk(voxels);
					this.writeChunk.Subchunks[subchunkInt] = writeSubchunk;
				}
			}
		}

		// beyond this point writeSubchunk.Voxels is guaranteed to not be null and to be a deep copy
		var previousVoxel = writeSubchunk.Voxels[voxelInt];
		writeSubchunk.Voxels[voxelInt] = voxel;
		return previousVoxel;
	}

	private static Voxel GetVoxel(VoxelChunk chunk, int subchunkInt, int voxelInt)
	{
		Debug.Assert(chunk != null);

		if (chunk.Subchunks == null)
		{
			return chunk.UniformVoxel;
		}

		var subchunk = chunk.Subchunks[subchunkInt];
		if (subchunk.Voxels == null)
		{
			return subchunk.UniformVoxel;
		}

		return subchunk.Voxels[voxelInt];
	}

	private void ClearFields()
	{
		this.volume = null;

		// by setting to MinValue the chunk index is guaranteed not to match when the first chunk is accessed
		// because the min chunk index is MinValue / 4 (4 is minimum chunk width in voxels)
		this.cachedChunkIndex = Int3.MinValue;
		this.readChunk = null;
		this.writeChunk = null;
	}
}
