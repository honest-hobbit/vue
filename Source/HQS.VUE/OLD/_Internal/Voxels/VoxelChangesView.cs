﻿namespace HQS.VUE.OLD;

// This class caches the chunk it finds when calling GetNewVoxel. This means this class is only safe to use for
// voxel change reporting after a job has finished running and not for viewing new voxels while more changes are being
// made. This is because if GetNewVoxel is used on a chunk that has no changes, the old chunk will be cached, and then
// if changes are made to that chunk and GetNewVoxel is called again it will still be using the cached old chunk.
// However, OverrideNewChunk can be used to replace the cached chunk in such a scenario when a new change is made.
// GetOldVoxel is always safe to use.
internal class VoxelChangesView : IVoxelChangesView
{
	private const bool WaitUntilLoaded = true;

	private readonly Dictionary<Int3, Pin<VoxelChunk>> pins =
		new Dictionary<Int3, Pin<VoxelChunk>>(MortonCurve.EqualityComparer.ForInt3);

	private readonly VoxelChunkCache cache;

	private readonly VoxelSizeConfig config;

	private IVolumeView volume;

	private Pin<VoxelChunk> oldChunkPin;

	private Int3 oldChunkIndex;

	private ReadOnlyVoxelChunk oldChunk;

	private Int3 newChunkIndex;

	private ReadOnlyVoxelChunk newChunk;

	public VoxelChangesView(VoxelChunkCache cache, VoxelSizeConfig config, VoxelDefinitions definitions)
	{
		Debug.Assert(cache != null);
		Debug.Assert(config != null);
		Debug.Assert(definitions != null);

		this.cache = cache;
		this.config = config;
		this.Definitions = definitions;

		this.ClearFields();
	}

	public bool IsInitialized => this.volume != null;

	/// <inheritdoc />
	public IVoxelDefinitions Definitions { get; }

	/// <inheritdoc />
	public Guid Key { get; private set; }

	/// <inheritdoc />
	public Voxel GetOldVoxel(Int3 voxelIndex)
	{
		Debug.Assert(this.IsInitialized);

		var chunkIndex = this.config.ConvertVolumeVoxelToChunkIndex(voxelIndex);
		return this.GetVoxel(this.GetOldChunk(chunkIndex), voxelIndex);
	}

	/// <inheritdoc />
	public Voxel GetOldVoxel(int x, int y, int z) => this.GetOldVoxel(new Int3(x, y, z));

	/// <inheritdoc />
	public Voxel GetNewVoxel(Int3 voxelIndex)
	{
		Debug.Assert(this.IsInitialized);

		var chunkIndex = this.config.ConvertVolumeVoxelToChunkIndex(voxelIndex);
		return this.GetVoxel(this.GetNewChunk(chunkIndex), voxelIndex);
	}

	/// <inheritdoc />
	public Voxel GetNewVoxel(int x, int y, int z) => this.GetNewVoxel(new Int3(x, y, z));

	public Pinned<VoxelChunk> GetReadChunkPin(Int3 chunkIndex)
	{
		this.GetOldChunk(chunkIndex);
		return this.oldChunkPin.AsPinned;
	}

	public void OverrideNewChunk(Int3 chunkIndex, ReadOnlyVoxelChunk chunk)
	{
		if (chunkIndex == this.newChunkIndex)
		{
			this.newChunk = chunk;
		}
	}

	public void Initialize(IVolumeView volume)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(volume != null);

		this.volume = volume;
		this.Key = volume.Key;
	}

	public void Deinitialize()
	{
		Debug.Assert(this.IsInitialized);

		this.ClearFields();

		foreach (var pin in this.pins.Values)
		{
			pin.Dispose();
		}

		this.pins.Clear();
	}

	private void ClearFields()
	{
		this.volume = null;
		this.Key = Guid.Empty;

		// by setting to MinValue the chunk index is guaranteed not to match when the first chunk is accessed
		// because the min chunk index is MinValue / 4 (4 is minimum chunk width in voxels)
		this.oldChunkPin = default;
		this.oldChunkIndex = Int3.MinValue;
		this.oldChunk = ReadOnlyVoxelChunk.Null;
		this.newChunkIndex = Int3.MinValue;
		this.newChunk = ReadOnlyVoxelChunk.Null;
	}

	private ReadOnlyVoxelChunk GetOldChunk(Int3 chunkIndex)
	{
		if (chunkIndex != this.oldChunkIndex) ////|| this.oldChunk.IsNull)
		{
			if (!this.pins.TryGetValue(chunkIndex, out var pin))
			{
				pin = this.cache.GetChunkPin(
					new ChunkKey(this.Key, chunkIndex),
					this.volume.RunJobInfo.VoxelGridStore,
					WaitUntilLoaded);
				this.pins.Add(chunkIndex, pin);
			}

			this.oldChunkPin = pin;
			this.oldChunkIndex = chunkIndex;
			this.oldChunk = pin.Value.AsReadOnly;
		}

		return this.oldChunk;
	}

	private ReadOnlyVoxelChunk GetNewChunk(Int3 chunkIndex)
	{
		if (chunkIndex != this.newChunkIndex) ////|| this.newChunk.IsNull)
		{
			if (this.volume.TryGetVoxelChunkChangeset(chunkIndex, out var changeset))
			{
				this.newChunk = changeset.Current;
			}
			else
			{
				this.newChunk = this.GetOldChunk(chunkIndex);
			}

			this.newChunkIndex = chunkIndex;
		}

		return this.newChunk;
	}

	private Voxel GetVoxel(ReadOnlyVoxelChunk chunk, Int3 voxelIndex)
	{
		Debug.Assert(!chunk.IsNull);

		if (chunk.IsUniform)
		{
			return chunk.UniformVoxel;
		}

		int subchunkInt = this.config.ConvertVolumeVoxelToSubchunkInt(voxelIndex);
		var subchunk = chunk[subchunkInt];

		if (subchunk.IsUniform)
		{
			return subchunk.UniformVoxel;
		}

		int voxelInt = this.config.ConvertVolumeVoxelToSubchunkVoxelInt(voxelIndex);
		return subchunk.Voxels[voxelInt];
	}
}
