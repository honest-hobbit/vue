﻿namespace HQS.VUE.OLD;

internal interface IResyncHostApplicationQueue
{
	// returns true if the host application accepted the resync and false if rejected because of collision problems
	// must call recorder?.WaitUntilRecordingFinished() before returning
	bool SubmitResyncFrame(
		IReadOnlyList<IVolumeView> volumes,
		VoxelJobDiagnosticsRecorder diagnostics,
		bool checkCollision,
		Pinned<IVoxelJobInfo> jobCompleted,
		ChangesetRecorder recorder);
}
