﻿namespace HQS.VUE.OLD;

internal class HostlessResyncQueue : IResyncHostApplicationQueue
{
	/// <inheritdoc />
	public bool SubmitResyncFrame(
		IReadOnlyList<IVolumeView> volumes,
		VoxelJobDiagnosticsRecorder diagnostics,
		bool checkCollision,
		Pinned<IVoxelJobInfo> jobCompleted,
		ChangesetRecorder recorder)
	{
		IResyncHostApplicationQueueContracts.SubmitResyncFrame(volumes, diagnostics, jobCompleted, recorder);

		recorder?.WaitUntilRecordingFinished(diagnostics);
		diagnostics.VerifyResync = false;
		diagnostics.ResyncSuccessful = true;

		return true;
	}
}
