﻿namespace HQS.VUE.OLD;

internal static class IResyncHostApplicationQueueExtensions
{
	private const bool DoNotCheckCollision = false;

	// returns true if host application accepted the resync and false if rejected because of collision problems
	public static bool SubmitResyncFrame(
		this IResyncHostApplicationQueue queue, IReadOnlyList<IVolumeView> volumes, VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(queue != null);
		Debug.Assert(volumes != null);
		Debug.Assert(diagnostics != null);

		return queue.SubmitResyncFrame(volumes, diagnostics, DoNotCheckCollision, default, null);
	}
}
