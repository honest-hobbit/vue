﻿namespace HQS.VUE.OLD;

internal static class IResyncHostApplicationQueueContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void SubmitResyncFrame(
		IReadOnlyList<IVolumeView> volumes,
		VoxelJobDiagnosticsRecorder diagnostics,
		Pinned<IVoxelJobInfo> jobCompleted,
		ChangesetRecorder recorder)
	{
		Debug.Assert(volumes != null);
		Debug.Assert(volumes.Count > 0);
		Debug.Assert(diagnostics != null);
		Debug.Assert(jobCompleted.IsUnassigned || jobCompleted.IsPinned);
	}
}
