﻿namespace HQS.VUE.OLD;

internal class ChangesetPersister
{
	private readonly VoxelGridPersister voxelGridPersister;

	private readonly VoxelShapePersister voxelShapePersister;

	public ChangesetPersister(
		VoxelGridPersister voxelGridPersister, VoxelShapePersister voxelShapePersister)
	{
		Debug.Assert(voxelGridPersister != null);
		Debug.Assert(voxelShapePersister != null);

		this.voxelGridPersister = voxelGridPersister;
		this.voxelShapePersister = voxelShapePersister;
	}

	public void PersistChanges(IReadOnlyList<IVolumeView> volumes, bool overrideAutoSave)
	{
		Debug.Assert(volumes != null);
		Debug.Assert(volumes.Count > 0);

		// setup all save jobs, but don't start any yet (need to wait until serializing is completed)
		int max = volumes.Count;
		for (int i = 0; i < max; i++)
		{
			var view = volumes[i];
			this.voxelGridPersister.HandleVoxelGrid(view, overrideAutoSave);
			this.voxelShapePersister.HandleVoxelShape(view, overrideAutoSave);
		}
	}
}
