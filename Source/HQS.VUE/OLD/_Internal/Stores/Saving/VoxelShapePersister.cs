﻿namespace HQS.VUE.OLD;

internal class VoxelShapePersister : AbstractChunkGridPersister<ReadOnlyContourChunk>
{
	private readonly ICommandSubmitter<SaveVoxelShapeCommand.Args> saveProcessor;

	public VoxelShapePersister(
		ICommandSubmitter<SaveVoxelShapeCommand.Args> saveProcessor, IPinPool<PinSet<ReadOnlyContourChunk>> pinSetPool)
		: base(pinSetPool)
	{
		Debug.Assert(saveProcessor != null);

		this.saveProcessor = saveProcessor;
	}

	public void HandleVoxelShape(IVolumeView view, bool overrideAutoSave)
	{
		Debug.Assert(view != null);
		Debug.Assert(view.IsInitialized);

		var store = view.RunJobInfo.VoxelShapeStore;
		if (store == null)
		{
			// contours are not set to be saved at some point so don't force them to stay in memory
			return;
		}

		// contours are set to be saved at some point
		var data = view.Volume.EngineData;
		var chunkPins = this.MergeChangedChunksIntoPinSet(data.ContourChunkPins, view.ChangedContourChunkPins);

		if (view.RunJobInfo.Autosave || overrideAutoSave)
		{
			// contours should be saved if there are changes
			if (chunkPins.IsAssigned || view.RunJobInfo.HasUnsavedConfig)
			{
				this.saveProcessor.Submit(new SaveVoxelShapeCommand.Args()
				{
					View = view,
					ChunkPins = chunkPins,
				});
			}

			data.ContourChunkPins = default;
		}
		else
		{
			// contours will be saved in future so keep them in memory now
			data.ContourChunkPins = chunkPins;
		}
	}
}
