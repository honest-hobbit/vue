﻿namespace HQS.VUE.OLD;

internal abstract class AbstractSaveChunkGridCommand<TGrid, TChunk> : AbstractCommand, IPersistenceCommand, INamespaceOLD
	where TChunk : IKeyed<ChunkKey>
{
	private readonly CommandWaiter<ChunkRecord> serializing = new CommandWaiter<ChunkRecord>();

	private readonly ICommandSubmitter<SerializeChunkCommand<TChunk>.Args, ChunkRecord> serializeProcessor;

	private readonly ICommandSubmitter<SaveChunkGridSucceededCommand.Args> savedProcessor;

	private readonly ICommandSubmitter<SaveChunkGridFailedCommand.Args> failedProcessor;

	private readonly IReporter<Exception> errorReporter;

	private Volume volume;

	private IChunkGridStore<TGrid> store;

	private TGrid gridRecord;

	private Pin<PinSet<TChunk>> chunkPins;

	public AbstractSaveChunkGridCommand(
		ICommandSubmitter<SerializeChunkCommand<TChunk>.Args, ChunkRecord> serializeProcessor,
		ICommandSubmitter<SaveChunkGridSucceededCommand.Args> savedProcessor,
		ICommandSubmitter<SaveChunkGridFailedCommand.Args> failedProcessor,
		IReporter<Exception> errorReporter)
	{
		Debug.Assert(serializeProcessor != null);
		Debug.Assert(savedProcessor != null);
		Debug.Assert(failedProcessor != null);
		Debug.Assert(errorReporter != null);

		this.serializeProcessor = serializeProcessor;
		this.savedProcessor = savedProcessor;
		this.failedProcessor = failedProcessor;
		this.errorReporter = errorReporter;
	}

	protected void Initialize(
		Volume volume, IChunkGridStore<TGrid> store, TGrid gridRecord, Pin<PinSet<TChunk>> chunkPins)
	{
		Debug.Assert(volume != null);
		Debug.Assert(store != null);
		this.ValidateAndSetInitialized();

		this.volume = volume;
		this.store = store;
		this.gridRecord = gridRecord;
		this.chunkPins = chunkPins;

		if (chunkPins.IsAssigned)
		{
			var pins = chunkPins.Value;
			int max = pins.Count;
			for (int i = 0; i < max; i++)
			{
				this.serializing.Add(this.serializeProcessor.SubmitAndGetWaitableResult(
					new SerializeChunkCommand<TChunk>.Args() { Chunk = pins[i] }));
			}
		}
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.serializing.Clear();
		this.volume = null;
		this.store = null;
		this.gridRecord = default;
		this.chunkPins = default;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		try
		{
			this.store.UpdateChunks(this.gridRecord, this.serializing.Result);
			this.chunkPins.Dispose();

			this.savedProcessor.Submit(this.GetChunkGridSavedCommandArgs(this.volume, this.gridRecord));
		}
		catch (Exception error)
		{
			this.errorReporter.Report(error);
			this.failedProcessor.Submit(this.GetSaveChunkGridFailedCommandArgs(this.volume.EngineData, this.chunkPins));
		}
	}

	protected abstract SaveChunkGridSucceededCommand.Args GetChunkGridSavedCommandArgs(
		Volume volume, TGrid gridRecord);

	protected abstract SaveChunkGridFailedCommand.Args GetSaveChunkGridFailedCommandArgs(
		VoxelGridEngineData engineData, Pin<PinSet<TChunk>> chunkPins);
}
