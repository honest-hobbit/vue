﻿namespace HQS.VUE.OLD;

internal class VoxelGridPersister : AbstractChunkGridPersister<ReadOnlyVoxelChunk>
{
	private readonly ICommandSubmitter<SaveVoxelGridCommand.Args> saveProcessor;

	public VoxelGridPersister(
		ICommandSubmitter<SaveVoxelGridCommand.Args> saveProcessor, IPinPool<PinSet<ReadOnlyVoxelChunk>> pinSetPool)
		: base(pinSetPool)
	{
		Debug.Assert(saveProcessor != null);

		this.saveProcessor = saveProcessor;
	}

	public void HandleVoxelGrid(IVolumeView view, bool overrideAutoSave)
	{
		Debug.Assert(view != null);
		Debug.Assert(view.IsInitialized);

		var store = view.RunJobInfo.VoxelGridStore;
		var data = view.Volume.EngineData;
		var chunkPins = this.MergeChangedChunksIntoPinSet(data.VoxelChunkPins, view.ChangedVoxelChunkPins);

		if ((view.RunJobInfo.Autosave || overrideAutoSave) && store != null)
		{
			// voxels should be saved if there are changes
			if (chunkPins.IsAssigned || view.RunJobInfo.HasUnsavedConfig)
			{
				this.saveProcessor.Submit(new SaveVoxelGridCommand.Args()
				{
					View = view,
					ChunkPins = chunkPins,
				});
			}

			data.VoxelChunkPins = default;
		}
		else
		{
			// voxels aren't being saved, must be kept in memory
			data.VoxelChunkPins = chunkPins;
		}
	}
}
