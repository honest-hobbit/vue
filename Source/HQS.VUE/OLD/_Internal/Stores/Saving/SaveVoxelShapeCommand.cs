﻿namespace HQS.VUE.OLD;

internal class SaveVoxelShapeCommand :
	AbstractSaveChunkGridCommand<VoxelShapeRecord, ReadOnlyContourChunk>, IInitializable<SaveVoxelShapeCommand.Args>
{
	public SaveVoxelShapeCommand(
		ICommandSubmitter<SerializeChunkCommand<ReadOnlyContourChunk>.Args, ChunkRecord> serializeProcessor,
		ICommandSubmitter<SaveChunkGridSucceededCommand.Args> savedProcessor,
		ICommandSubmitter<SaveChunkGridFailedCommand.Args> failedProcessor,
		IReporter<Exception> errorReporter)
		: base(serializeProcessor, savedProcessor, failedProcessor, errorReporter)
	{
	}

	/// <inheritdoc />
	public void Initialize(Args args) => this.Initialize(
		args.View.Volume,
		args.View.RunJobInfo.VoxelShapeStore,
		VoxelShapeRecord.From(args.View),
		args.ChunkPins);

	protected override SaveChunkGridSucceededCommand.Args GetChunkGridSavedCommandArgs(
		Volume volume, VoxelShapeRecord gridRecord) =>
		new SaveChunkGridSucceededCommand.Args()
		{
			Volume = volume,
			SavedInfo = VoxelGridSavedInfo.From(gridRecord),
		};

	protected override SaveChunkGridFailedCommand.Args GetSaveChunkGridFailedCommandArgs(
		VoxelGridEngineData engineData, Pin<PinSet<ReadOnlyContourChunk>> chunkPins) =>
		new SaveChunkGridFailedCommand.Args()
		{
			EngineData = engineData,
			ContourChunkPins = chunkPins,
		};

	public struct Args
	{
		public IVolumeView View;

		public Pin<PinSet<ReadOnlyContourChunk>> ChunkPins;
	}
}
