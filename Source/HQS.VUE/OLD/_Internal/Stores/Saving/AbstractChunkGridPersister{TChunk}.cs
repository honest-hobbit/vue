﻿namespace HQS.VUE.OLD;

internal abstract class AbstractChunkGridPersister<TChunk>
	where TChunk : IKeyed<ChunkKey>
{
	private readonly IPinPool<PinSet<TChunk>> pinSetPool;

	public AbstractChunkGridPersister(IPinPool<PinSet<TChunk>> pinSetPool)
	{
		Debug.Assert(pinSetPool != null);

		this.pinSetPool = pinSetPool;
	}

	protected Pin<PinSet<TChunk>> MergeChangedChunksIntoPinSet(
		Pin<PinSet<TChunk>> currentPins, IReadOnlyList<ReadOnlyPinView<TChunk>> changedChunks)
	{
		Debug.Assert(currentPins.IsUnassigned || currentPins.IsPinned);
		Debug.Assert(changedChunks != null);

		if (changedChunks.Count == 0)
		{
			return currentPins;
		}

		if (currentPins.IsUnassigned)
		{
			currentPins = this.pinSetPool.Rent();
		}

		// An extra pin is added to every chunk before it's queued to be saved out to the store.
		// This is done to force the chunk to remain in memory until it is actually saved,
		// thus preventing a stale version of the chunk from being loaded back into memory from the store.
		currentPins.Value.AddMany(changedChunks);
		return currentPins;
	}
}
