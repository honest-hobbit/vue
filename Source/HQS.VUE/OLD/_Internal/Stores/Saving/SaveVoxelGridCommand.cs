﻿namespace HQS.VUE.OLD;

internal class SaveVoxelGridCommand :
	AbstractSaveChunkGridCommand<VoxelGridRecord, ReadOnlyVoxelChunk>, IInitializable<SaveVoxelGridCommand.Args>
{
	public SaveVoxelGridCommand(
		ICommandSubmitter<SerializeChunkCommand<ReadOnlyVoxelChunk>.Args, ChunkRecord> serializeProcessor,
		ICommandSubmitter<SaveChunkGridSucceededCommand.Args> savedProcessor,
		ICommandSubmitter<SaveChunkGridFailedCommand.Args> failedProcessor,
		IReporter<Exception> errorReporter)
		: base(serializeProcessor, savedProcessor, failedProcessor, errorReporter)
	{
	}

	/// <inheritdoc />
	public void Initialize(Args args) => this.Initialize(
		args.View.Volume,
		args.View.RunJobInfo.VoxelGridStore,
		VoxelGridRecord.From(args.View),
		args.ChunkPins);

	protected override SaveChunkGridSucceededCommand.Args GetChunkGridSavedCommandArgs(
		Volume volume, VoxelGridRecord gridRecord) =>
		new SaveChunkGridSucceededCommand.Args()
		{
			Volume = volume,
			SavedInfo = VoxelGridSavedInfo.From(gridRecord),
		};

	protected override SaveChunkGridFailedCommand.Args GetSaveChunkGridFailedCommandArgs(
		VoxelGridEngineData engineData, Pin<PinSet<ReadOnlyVoxelChunk>> chunkPins) =>
		new SaveChunkGridFailedCommand.Args()
		{
			EngineData = engineData,
			VoxelChunkPins = chunkPins,
		};

	public struct Args
	{
		public IVolumeView View;

		public Pin<PinSet<ReadOnlyVoxelChunk>> ChunkPins;
	}
}
