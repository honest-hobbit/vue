﻿namespace HQS.VUE.OLD;

internal class SaveChunkGridSucceededCommand :
	AbstractCommand, IHostCommand, IInitializable<SaveChunkGridSucceededCommand.Args>, INamespaceOLD
{
	private Volume volume;

	private VoxelGridSavedInfo savedInfo;

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.Volume != null);
		this.ValidateAndSetInitialized();

		this.volume = args.Volume;
		this.savedInfo = args.SavedInfo;
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.volume = null;
		this.savedInfo = default;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		this.volume.SetSavedInfo(this.savedInfo);
	}

	public struct Args
	{
		public Volume Volume;

		public VoxelGridSavedInfo SavedInfo;
	}
}
