﻿namespace HQS.VUE.OLD;

internal class SaveChunkGridFailedCommand :
	AbstractCommand, IMainCommand, IInitializable<SaveChunkGridFailedCommand.Args>, INamespaceOLD
{
	private VoxelGridEngineData engineData;

	private Pin<PinSet<ReadOnlyVoxelChunk>> voxelChunkPins;

	private Pin<PinSet<ReadOnlyContourChunk>> contourChunkPins;

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.EngineData != null);
		Debug.Assert(args.VoxelChunkPins.IsUnassigned || args.VoxelChunkPins.IsPinned);
		Debug.Assert(args.ContourChunkPins.IsUnassigned || args.ContourChunkPins.IsPinned);
		this.ValidateAndSetInitialized();

		this.engineData = args.EngineData;
		this.voxelChunkPins = args.VoxelChunkPins;
		this.contourChunkPins = args.ContourChunkPins;
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.engineData = null;
		this.voxelChunkPins = default;
		this.contourChunkPins = default;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		if (this.voxelChunkPins.IsAssigned)
		{
			if (this.engineData.VoxelChunkPins.IsAssigned)
			{
				this.engineData.VoxelChunkPins.Value.AddMany(this.voxelChunkPins.Value);
				this.voxelChunkPins.Dispose();
			}
			else
			{
				this.engineData.VoxelChunkPins = this.voxelChunkPins;
			}
		}

		if (this.contourChunkPins.IsAssigned)
		{
			if (this.engineData.ContourChunkPins.IsAssigned)
			{
				this.engineData.ContourChunkPins.Value.AddMany(this.contourChunkPins.Value);
				this.contourChunkPins.Dispose();
			}
			else
			{
				this.engineData.ContourChunkPins = this.contourChunkPins;
			}
		}
	}

	public struct Args
	{
		public VoxelGridEngineData EngineData;

		public Pin<PinSet<ReadOnlyVoxelChunk>> VoxelChunkPins;

		public Pin<PinSet<ReadOnlyContourChunk>> ContourChunkPins;
	}
}
