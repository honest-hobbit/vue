﻿namespace HQS.VUE.OLD;

internal class ChunkLoadingSignal
{
	private readonly ConcurrentBool isStarted = new ConcurrentBool(false);

	private readonly PendingSignal isCompleted = new PendingSignal();

	public bool IsDoneOrNotStarted => !(this.IsStarted ^ this.IsCompleted);

	public bool IsStarted => this.isStarted.Value;

	public bool IsCompleted => this.isCompleted.IsSet;

	public bool TryStart() => this.isStarted.ToggleIfFalse();

	public void SetCompleted()
	{
		Debug.Assert(this.IsStarted);
		Debug.Assert(!this.IsCompleted);

		this.isCompleted.Set();
	}

	public void SetFailed(Exception exception)
	{
		Debug.Assert(this.IsStarted);
		Debug.Assert(!this.IsCompleted);
		Debug.Assert(exception != null);

		this.isCompleted.SetException(exception);
	}

	public void Wait() => this.isCompleted.Wait();

	public void Reset()
	{
		Debug.Assert(this.IsDoneOrNotStarted);

		this.isStarted.SetFalse();
		this.isCompleted.Reset();
	}
}
