﻿namespace HQS.VUE.OLD;

internal class LoadContourChunkCommand : LoadChunkCommandOLD<IVoxelShapeStore, ContourChunk>
{
	public LoadContourChunkCommand(IInlineDeserializer<ContourChunk> deserializer, IPinPool<IByteBuffer> buffers)
		: base(deserializer, buffers)
	{
	}
}
