﻿namespace HQS.VUE.OLD;

internal class ChunkLoaderOLD<TStore, TChunk>
	where TStore : class, IChunkGridStore
	where TChunk : class, IChunk
{
	private readonly ICommandSubmitter<LoadChunkCommandOLD<TStore, TChunk>.Args> loadProcessor;

	public ChunkLoaderOLD(ICommandSubmitter<LoadChunkCommandOLD<TStore, TChunk>.Args> loadProcessor)
	{
		Debug.Assert(loadProcessor != null);

		this.loadProcessor = loadProcessor;
	}

	// returns true if the pin might still be loading, returns false if it is already loaded
	public bool CheckLoading(Cached<ChunkKey, TChunk> cached, TStore store)
	{
		Debug.Assert(cached != null);
		Debug.Assert(cached.Pin.IsPinned);

		var chunk = cached.Pin.Value;
		if (chunk.Loading.TryStart())
		{
			chunk.Key = cached.Key;
			if (store == null)
			{
				chunk.SetToEmptyChunk();
				chunk.Loading.SetCompleted();
				return false;
			}
			else
			{
				this.loadProcessor.Submit(new LoadChunkCommandOLD<TStore, TChunk>.Args()
				{
					ChunkPin = cached.Pin.AsPinned,
					Store = store,
				});
			}
		}

		return true;
	}
}
