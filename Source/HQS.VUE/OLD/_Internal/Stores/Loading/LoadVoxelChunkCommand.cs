﻿namespace HQS.VUE.OLD;

internal class LoadVoxelChunkCommand : LoadChunkCommandOLD<IVoxelGridStore, VoxelChunk>
{
	public LoadVoxelChunkCommand(IInlineDeserializer<VoxelChunk> deserializer, IPinPool<IByteBuffer> buffers)
		: base(deserializer, buffers)
	{
	}
}
