﻿namespace HQS.VUE.OLD;

internal class LoadChunkCommandOLD<TStore, TChunk> :
	AbstractCommand, IParallelCommand, IInitializable<LoadChunkCommandOLD<TStore, TChunk>.Args>, INamespaceOLD
	where TStore : class, IChunkGridStore
	where TChunk : class, IChunk
{
	private readonly IInlineDeserializer<TChunk> deserializer;

	private readonly IPinPool<IByteBuffer> buffers;

	private Pin<TChunk> chunkPin;

	private TStore store;

	public LoadChunkCommandOLD(IInlineDeserializer<TChunk> deserializer, IPinPool<IByteBuffer> buffers)
	{
		Debug.Assert(deserializer != null);
		Debug.Assert(buffers != null);

		this.deserializer = deserializer;
		this.buffers = buffers;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.ChunkPin.IsPinned);
		Debug.Assert(args.ChunkPin.Value.IsKeyAssigned());
		Debug.Assert(args.Store != null);
		this.ValidateAndSetInitialized();

		this.chunkPin = args.ChunkPin.CreatePin();
		this.store = args.Store;
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.chunkPin = default;
		this.store = null;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		var chunk = this.chunkPin.Value;
		chunk.SetToEmptyChunk();
		var buffer = this.buffers.Rent();

		try
		{
			if (this.store.TryGetChunk(chunk.Key, out var record))
			{
				buffer.Value.DeserializeInline(this.deserializer, chunk, record.Data);
				chunk.RecordKey = record.RecordKey;
				chunk.HasData = true;
			}

			chunk.Loading.SetCompleted();
		}
		catch (Exception error)
		{
			chunk.Loading.SetFailed(error);
			throw;
		}
		finally
		{
			this.chunkPin.Dispose();
			buffer.Dispose();
		}
	}

	public struct Args
	{
		public Pinned<TChunk> ChunkPin;

		public TStore Store;
	}
}
