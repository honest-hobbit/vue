﻿namespace HQS.VUE.OLD;

internal class SerializeContourChunkCommand : SerializeChunkCommand<ReadOnlyContourChunk>
{
	public SerializeContourChunkCommand(ISerializer<ReadOnlyContourChunk> serializer, IPinPool<IByteBuffer> buffers)
		: base(serializer, buffers)
	{
	}
}
