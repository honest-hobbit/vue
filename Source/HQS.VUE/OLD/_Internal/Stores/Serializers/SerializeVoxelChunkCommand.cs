﻿namespace HQS.VUE.OLD;

internal class SerializeVoxelChunkCommand : SerializeChunkCommand<ReadOnlyVoxelChunk>
{
	public SerializeVoxelChunkCommand(ISerializer<ReadOnlyVoxelChunk> serializer, IPinPool<IByteBuffer> buffers)
		: base(serializer, buffers)
	{
	}
}
