﻿namespace HQS.VUE.OLD;

internal class SerializeChunkCommand<TChunk> :
	AbstractResultCommand<ChunkRecord>, IParallelCommand, IInitializable<SerializeChunkCommand<TChunk>.Args>, INamespaceOLD
	where TChunk : IKeyed<ChunkKey>
{
	private readonly ISerializer<TChunk> serializer;

	private readonly IPinPool<IByteBuffer> buffers;

	private ReadOnlyPin<TChunk> chunkPin;

	public SerializeChunkCommand(ISerializer<TChunk> serializer, IPinPool<IByteBuffer> buffers)
	{
		Debug.Assert(serializer != null);
		Debug.Assert(buffers != null);

		this.serializer = serializer;
		this.buffers = buffers;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(!args.Chunk.IsNull);
		Debug.Assert(args.Chunk.IsPinnedAndLoaded);
		this.ValidateAndSetInitialized();

		this.chunkPin = args.Chunk.CreatePin();
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.chunkPin.Unpin();
		this.chunkPin = default;
	}

	/// <inheritdoc />
	protected override ChunkRecord RunAndGetResult()
	{
		Debug.Assert(!this.chunkPin.IsNull);
		Debug.Assert(this.chunkPin.IsPinnedAndLoaded);

		// TODO this does not actually support version numbers
		using var buffer = this.buffers.Rent();
		return new ChunkRecord(
			this.chunkPin.AssignAndGetRecordKey(),
			this.chunkPin.Value.Key,
			0,
			buffer.Value.Serialize(this.serializer, this.chunkPin.Value));
	}

	public struct Args
	{
		public ReadOnlyPinView<TChunk> Chunk;
	}
}
