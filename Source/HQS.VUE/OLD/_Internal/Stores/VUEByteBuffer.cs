﻿namespace HQS.VUE.OLD;

internal class VUEByteBuffer : IByteBuffer
{
	private readonly IByteBuffer buffer;

	public VUEByteBuffer(UniverseConfig config)
	{
		Debug.Assert(config != null);

		this.buffer = new LZ4ByteBuffer(config.ByteBufferCapacity) { Mode = config.ByteCompressionMode };
	}

	/// <inheritdoc />
	public T Deserialize<T>(IDeserializer<T> deserializer, byte[] array) =>
		this.buffer.Deserialize(deserializer, array);

	/// <inheritdoc />
	public void DeserializeInline<T>(IInlineDeserializer<T> deserializer, T result, byte[] array) =>
		this.buffer.DeserializeInline(deserializer, result, array);

	/// <inheritdoc />
	public byte[] Serialize<T>(ISerializer<T> serializer, T value) =>
		this.buffer.Serialize(serializer, value);
}
