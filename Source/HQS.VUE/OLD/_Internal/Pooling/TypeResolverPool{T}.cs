﻿namespace HQS.VUE.OLD;

internal class TypeResolverPool<T> : AbstractDisposable, ITypePool, IPool<T>
{
	private readonly IPool<T> pool;

	public TypeResolverPool(IPooledTypeResolver keyResolver, ITypeResolver<IPool<T>> poolResolver)
	{
		Debug.Assert(keyResolver != null);
		Debug.Assert(poolResolver != null);

		var key = keyResolver[typeof(T)];
		var pool = poolResolver.GetInstance();

		Debug.Assert(key != null);
		Debug.Assert(pool != null);

		this.Key = key;
		this.pool = pool;
	}

	/// <inheritdoc />
	public PooledType Key { get; }

	/// <inheritdoc />
	public KeyValuePair<PooledType, PoolDiagnostics> Diagnostics =>
		new KeyValuePair<PooledType, PoolDiagnostics>(this.Key, this.pool.GetDiagnostics());

	/// <inheritdoc />
	public int? BoundedCapacity => this.pool.BoundedCapacity;

	/// <inheritdoc />
	public int Available => this.pool.Available;

	/// <inheritdoc />
	public long Active => this.pool.Active;

	/// <inheritdoc />
	public long Created => this.pool.Created;

	/// <inheritdoc />
	public long Released => this.pool.Released;

	/// <inheritdoc />
	public void Return(T value) => this.pool.Return(value);

	/// <inheritdoc />
	public T Rent() => this.pool.Rent();

	/// <inheritdoc />
	public bool TryRent(out T value) => this.pool.TryRent(out value);

	/// <inheritdoc />
	public int Create(int count) => this.pool.Create(count);

	/// <inheritdoc />
	public int Release(int count) => this.pool.Release(count);

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.pool.ReleaseAll();
}
