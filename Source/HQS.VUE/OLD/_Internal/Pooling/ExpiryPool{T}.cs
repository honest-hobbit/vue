﻿namespace HQS.VUE.OLD;

internal class ExpiryPool<T> : AbstractExpiryQueue, IPool<T>
{
	private readonly IPool<T> pool;

	public ExpiryPool(IPool<T> pool, ThreadSafeCompactingQueue<CacheExpiration> queue)
		: base(queue)
	{
		Debug.Assert(pool != null);

		this.pool = pool;
	}

	public ExpiryPool(IPool<T> pool, int expiryCapacity)
		: base(expiryCapacity)
	{
		Debug.Assert(pool != null);

		this.pool = pool;
	}

	public ExpiryPool(IPool<T> pool)
		: base()
	{
		Debug.Assert(pool != null);

		this.pool = pool;
	}

	/// <inheritdoc />
	public int? BoundedCapacity => this.pool.BoundedCapacity;

	/// <inheritdoc />
	public int Available => this.pool.Available;

	/// <inheritdoc />
	public long Active => this.pool.Active;

	/// <inheritdoc />
	public long Created => this.pool.Created;

	/// <inheritdoc />
	public long Released => this.pool.Released;

	/// <inheritdoc />
	public T Rent() => this.TryRent(out var value) ? value : this.pool.Rent();

	/// <inheritdoc />
	public bool TryRent(out T value)
	{
		while (true)
		{
			if (this.pool.TryRent(out value))
			{
				return true;
			}
			else
			{
				// unable to TryTake from the pool so try disposing a token
				// to release a value back to the pool
				if (!this.TryExpireToken())
				{
					// unable to release any more values back to the pool
					// so TryTake can't succeed
					value = default;
					return false;
				}
			}
		}
	}

	/// <inheritdoc />
	public void Return(T value) => this.pool.Return(value);

	/// <inheritdoc />
	public int Create(int count) => this.pool.Create(count);

	/// <inheritdoc />
	public int Release(int count) => this.pool.Release(count);
}
