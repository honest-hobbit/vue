﻿namespace HQS.VUE.OLD;

internal interface IChunk : IKeyed<ChunkKey>
{
	ChunkLoadingSignal Loading { get; }

	new ChunkKey Key { get; set; }

	Guid RecordKey { get; set; }

	bool HasData { get; set; }

	// this is not the same as Clearing
	void SetToEmptyChunk();
}
