﻿namespace HQS.VUE.OLD;

internal class ChunkCacheWrapper : IChunkCache
{
	private readonly EngineValidator validator;

	private readonly IChunkCache cache;

	public ChunkCacheWrapper(EngineValidator validator, IChunkCache cache)
	{
		Debug.Assert(validator != null);
		Debug.Assert(cache != null);

		this.validator = validator;
		this.cache = cache;
	}

	/// <inheritdoc />
	public int Count => this.cache.Count;

	/// <inheritdoc />
	public int TryClear()
	{
		this.validator.ValidateEngine();

		return this.cache.TryClear();
	}

	/// <inheritdoc />
	public IEnumerable<ChunkCacheEntry> GetChunksSnapshot()
	{
		this.validator.ValidateEngine();

		return this.cache.GetChunksSnapshot();
	}
}
