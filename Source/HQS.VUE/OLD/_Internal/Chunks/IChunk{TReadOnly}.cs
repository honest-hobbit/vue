﻿namespace HQS.VUE.OLD;

internal interface IChunk<TReadOnly> : IChunk
	where TReadOnly : IKeyed<ChunkKey>
{
	TReadOnly AsReadOnly { get; }
}
