﻿namespace HQS.VUE.OLD;

internal static class IChunkExtensions
{
	public static bool IsKeyAssigned(this IChunk chunk)
	{
		Debug.Assert(chunk != null);

		return chunk.Key.ChunkGrid != Guid.Empty;
	}

	public static void ClearAndReset(this IChunk chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Loading.IsDoneOrNotStarted);

		chunk.Key = default;
		chunk.SetToEmptyChunk();
		chunk.Loading.Reset();
	}

	public static Guid AssignAndGetRecordKey(this IChunk chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Loading.IsCompleted);

		if (chunk.RecordKey == Guid.Empty)
		{
			chunk.RecordKey = Guid.NewGuid();
		}

		return chunk.RecordKey;
	}
}
