﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyPin<TReadOnly> : IEquatable<ReadOnlyPin<TReadOnly>>
	where TReadOnly : IKeyed<ChunkKey>
{
	private readonly Pin<IChunk<TReadOnly>> pin;

	public ReadOnlyPin(Pin<IChunk<TReadOnly>> pin)
	{
		Debug.Assert(pin != null);

		this.pin = pin;
	}

	public bool IsNull => this.pin == null;

	public bool IsPinnedAndLoaded => this.pin.IsPinned && this.pin.Value.Loading.IsCompleted;

	public bool IsPinned => this.pin.IsPinned;

	public ReadOnlyPinView<TReadOnly> AsView => new ReadOnlyPinView<TReadOnly>(this);

	public int PinCount => this.pin.PinCount;

	public TReadOnly Value => this.pin.Value.AsReadOnly;

	public static bool operator ==(ReadOnlyPin<TReadOnly> lhs, ReadOnlyPin<TReadOnly> rhs) => lhs.Equals(rhs);

	public static bool operator !=(ReadOnlyPin<TReadOnly> lhs, ReadOnlyPin<TReadOnly> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ReadOnlyPin<TReadOnly> other) => this.pin == other.pin;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.pin.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.pin.ToString();

	public ReadOnlyPin<TReadOnly> CreatePin() => new ReadOnlyPin<TReadOnly>(this.pin.CreatePin());

	public void Unpin() => this.pin.Dispose();

	public Guid AssignAndGetRecordKey() => this.pin.Value.AssignAndGetRecordKey();
}
