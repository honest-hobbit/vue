﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyPinView<TReadOnly> : IEquatable<ReadOnlyPinView<TReadOnly>>
	where TReadOnly : IKeyed<ChunkKey>
{
	private readonly ReadOnlyPin<TReadOnly> pin;

	public ReadOnlyPinView(ReadOnlyPin<TReadOnly> pin)
	{
		this.pin = pin;
	}

	public bool IsNull => this.pin.IsNull;

	public bool IsPinnedAndLoaded => this.pin.IsPinnedAndLoaded;

	public bool IsPinned => this.pin.IsPinned;

	public int PinCount => this.pin.PinCount;

	public TReadOnly Value => this.pin.Value;

	public static bool operator ==(ReadOnlyPinView<TReadOnly> lhs, ReadOnlyPinView<TReadOnly> rhs) => lhs.Equals(rhs);

	public static bool operator !=(ReadOnlyPinView<TReadOnly> lhs, ReadOnlyPinView<TReadOnly> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ReadOnlyPinView<TReadOnly> other) => this.pin == other.pin;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.pin.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.pin.ToString();

	public ReadOnlyPin<TReadOnly> CreatePin() => this.pin.CreatePin();

	public Guid AssignAndGetRecordKey() => this.pin.AssignAndGetRecordKey();
}
