﻿namespace HQS.VUE.OLD;

internal class ContourVolumeCommand : MainCommandOLD, IInitializable<ContourVolumeCommand.Args>, IRequireScope, INamespaceOLD
{
	private const bool DoOverrideAutoSave = true;

	private readonly SingleVolumeView volume;

	private readonly VolumeContourer contourer;

	private readonly ChangesetPersister persister;

	private readonly IResyncHostApplicationQueue resyncUnity;

	public ContourVolumeCommand(
		VolumeView volumeView,
		VolumeContourer contourer,
		ChangesetPersister persister,
		IResyncHostApplicationQueue resyncUnity)
	{
		Debug.Assert(volumeView != null);
		Debug.Assert(contourer != null);
		Debug.Assert(persister != null);
		Debug.Assert(resyncUnity != null);

		this.volume = new SingleVolumeView(volumeView);
		this.contourer = contourer;
		this.persister = persister;
		this.resyncUnity = resyncUnity;
	}

	/// <inheritdoc />
	void IRequireScope.SetScope(IDisposable scope) => this.SetScope(scope);

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.Volume != null);
		this.ValidateAndSetInitialized();

		this.volume.View.Initialize(args.Volume);
	}

	/// <inheritdoc />
	protected override void RunCommand()
	{
		var view = this.volume.View;

		try
		{
			// contour the entire volume
			this.contourer.ContourVolume(view, this.Diagnostics);

			bool resyncResult = this.resyncUnity.SubmitResyncFrame(this.volume, this.Diagnostics);
			Debug.Assert(resyncResult == true);

			// commit the changes
			view.CommitChangesAndCleanup(this.Diagnostics.DateRan);

			this.Diagnostics.VolumesChanged = 1;
			this.Diagnostics.ContourChunksChanged = view.ChangedContourChunkPins.Count;

			// save anything that was contoured
			this.Diagnostics.StartTiming();
			this.persister.PersistChanges(this.volume, DoOverrideAutoSave);
			this.Diagnostics.SetPersistenceDuration();
		}
		catch (Exception)
		{
			view.ClearChanges();
			throw;
		}
		finally
		{
			// cleanup
			view.Deinitialize();
		}
	}

	public struct Args
	{
		public Volume Volume;
	}
}
