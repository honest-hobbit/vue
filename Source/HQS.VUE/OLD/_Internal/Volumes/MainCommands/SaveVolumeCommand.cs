﻿namespace HQS.VUE.OLD;

internal class SaveVolumeCommand :
	MainCommandOLD, IInitializable<SaveVolumeCommand.Args>, INamespaceOLD
{
	private const bool DoOverrideAutoSave = true;

	private readonly SingleReadOnlyVolumeView volume = new SingleReadOnlyVolumeView();

	private readonly ChangesetPersister persister;

	public SaveVolumeCommand(ChangesetPersister persister)
	{
		Debug.Assert(persister != null);

		this.persister = persister;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.Volume != null);
		this.ValidateAndSetInitialized();

		this.volume.View.Initialize(args.Volume);
	}

	/// <inheritdoc />
	protected override void RunCommand()
	{
		try
		{
			// save the volume
			this.Diagnostics.StartTiming();
			this.persister.PersistChanges(this.volume, DoOverrideAutoSave);
			this.Diagnostics.SetPersistenceDuration();
		}
		finally
		{
			// cleanup
			this.volume.View.Deinitialize();
		}
	}

	public struct Args
	{
		public Volume Volume;
	}
}
