﻿namespace HQS.VUE.OLD;

internal interface IVolumeServices
{
	EngineValidator Validator { get; }

	ICommandSubmitter<ContourVolumeCommand.Args> Contour { get; }

	ICommandSubmitter<SaveVolumeCommand.Args> Save { get; }

	IVolumeInstances CreateInstances(Volume volume);
}
