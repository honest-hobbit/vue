﻿namespace HQS.VUE.OLD;

internal class VolumeServices : IVolumeServices
{
	public VolumeServices(
		EngineValidator validator,
		ICommandSubmitter<ContourVolumeCommand.Args> contourSubmitter,
		ICommandSubmitter<SaveVolumeCommand.Args> saveSubmitter)
	{
		Debug.Assert(validator != null);
		Debug.Assert(contourSubmitter != null);
		Debug.Assert(saveSubmitter != null);

		this.Validator = validator;
		this.Contour = contourSubmitter;
		this.Save = saveSubmitter;
	}

	/// <inheritdoc />
	public EngineValidator Validator { get; }

	/// <inheritdoc />
	public ICommandSubmitter<ContourVolumeCommand.Args> Contour { get; }

	/// <inheritdoc />
	public ICommandSubmitter<SaveVolumeCommand.Args> Save { get; }

	/// <inheritdoc />
	public virtual IVolumeInstances CreateInstances(Volume volume)
	{
		Debug.Assert(volume != null);

		return new HostlessVolumeInstances(volume);
	}
}
