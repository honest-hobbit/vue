﻿namespace HQS.VUE.OLD;

internal struct VoxelGridRunJobInfo
{
	public string Name;

	public bool Autosave;

	public bool HasUnsavedConfig;

	public IVoxelGridStore VoxelGridStore;

	public IVoxelShapeStore VoxelShapeStore;
}
