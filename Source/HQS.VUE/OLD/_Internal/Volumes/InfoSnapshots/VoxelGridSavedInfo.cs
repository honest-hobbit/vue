﻿namespace HQS.VUE.OLD;

internal struct VoxelGridSavedInfo
{
	public string Name;

	public bool Autosave;

	public long Version;

	public static VoxelGridSavedInfo From(VoxelGridRecord record) => new VoxelGridSavedInfo()
	{
		Name = record.Name,
		Autosave = record.Autosave,
		Version = record.Version,
	};

	public static VoxelGridSavedInfo From(VoxelShapeRecord record) => new VoxelGridSavedInfo()
	{
		Name = record.Name,
		Autosave = record.Autosave,
		Version = record.Version,
	};
}
