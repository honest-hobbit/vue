﻿namespace HQS.VUE.OLD;

internal struct VoxelGridChangedInfo
{
	public DateTime LastModified;

	public long Version;
}
