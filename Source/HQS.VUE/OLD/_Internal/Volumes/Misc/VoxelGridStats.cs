﻿namespace HQS.VUE.OLD;

internal class VoxelGridStats
{
	private readonly EngineValidator validator;

	private DateTime lastModified;

	private long version;

	private long savedVersion;

	public VoxelGridStats(EngineValidator validator, VoxelGridRecord record)
	{
		Debug.Assert(validator != null);
		Debug.Assert(record.IsValid);

		this.validator = validator;
		this.lastModified = record.LastModified;
		this.version = record.Version;
		this.savedVersion = record.Version;
	}

	public DateTime LastModified
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.lastModified;
		}
	}

	public long Version
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.version;
		}
	}

	public bool HasUnsavedChanges
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.version > this.savedVersion;
		}
	}

	public void SetChangedInfo(VoxelGridChangedInfo info)
	{
		Debug.Assert(this.validator.IsValid);

		this.lastModified = info.LastModified;
		this.version = info.Version;
	}

	public void SetSavedInfo(VoxelGridSavedInfo info)
	{
		Debug.Assert(this.validator.IsValid);

		this.savedVersion = info.Version;
	}
}
