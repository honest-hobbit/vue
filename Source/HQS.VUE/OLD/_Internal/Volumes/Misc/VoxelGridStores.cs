﻿namespace HQS.VUE.OLD;

internal class VoxelGridStores
{
	private readonly EngineValidator validator;

	private IVoxelGridStore voxelGridStore;

	private IVoxelShapeStore voxelShapeStore;

	public VoxelGridStores(EngineValidator validator)
	{
		Debug.Assert(validator != null);

		this.validator = validator;
	}

	public bool HasVoxelGridStore
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.voxelGridStore != null;
		}
	}

	public bool HasVoxelShapeStore
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.voxelShapeStore != null;
		}
	}

	public void GetInfo(ref VoxelGridRunJobInfo info)
	{
		Debug.Assert(this.validator.IsValid);

		info.VoxelGridStore = this.voxelGridStore;
		info.VoxelShapeStore = this.voxelShapeStore;
	}

	public void SetVoxelGridStore(IVoxelGridStore store)
	{
		Debug.Assert(this.validator.IsValid);

		Ensure.That(store, nameof(store)).IsNotNull();
		if (this.voxelGridStore != null)
		{
			throw new InvalidOperationException($"{nameof(this.SetVoxelGridStore)} can only be called once.");
		}

		this.voxelGridStore = store;
	}

	public void SetVoxelShapeStore(IVoxelShapeStore store)
	{
		Debug.Assert(this.validator.IsValid);

		Ensure.That(store, nameof(store)).IsNotNull();
		if (this.voxelShapeStore != null)
		{
			throw new InvalidOperationException($"{nameof(this.SetVoxelShapeStore)} can only be called once.");
		}

		this.voxelShapeStore = store;
	}
}
