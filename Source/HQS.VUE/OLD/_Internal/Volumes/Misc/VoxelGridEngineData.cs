﻿namespace HQS.VUE.OLD;

internal class VoxelGridEngineData
{
	public VoxelGridEngineData(VoxelGridRecord record)
	{
		Debug.Assert(record.IsValid);

		this.LastModified = record.LastModified;
		this.Version = record.Version;
	}

	public Pin<PinSet<ReadOnlyVoxelChunk>> VoxelChunkPins { get; set; }

	public Pin<PinSet<ReadOnlyContourChunk>> ContourChunkPins { get; set; }

	public DateTime LastModified { get; private set; }

	public long Version { get; private set; }

	public void UpdateStats(DateTime lastModified)
	{
		Debug.Assert(lastModified >= this.LastModified);

		this.LastModified = lastModified;
		this.Version++;
	}

	public VoxelGridChangedInfo GetUpdateInfo() => new VoxelGridChangedInfo()
	{
		LastModified = this.LastModified,
		Version = this.Version,
	};
}
