﻿namespace HQS.VUE.OLD;

internal class HostlessVolumeInstances : IVolumeInstances
{
	private static readonly IReadOnlyList<IVolumeInstance> EmptyInstances = ReadOnlyList.Empty<IVolumeInstance>();

	public HostlessVolumeInstances(Volume volume)
	{
		Debug.Assert(volume != null);

		this.Volume = volume;
	}

	/// <inheritdoc />
	public Volume Volume { get; }

	/// <inheritdoc />
	public int Count => EmptyInstances.Count;

	/// <inheritdoc />
	public IVolumeInstance this[int index] => EmptyInstances[index];

	/// <inheritdoc />
	public IEnumerator<IVolumeInstance> GetEnumerator() => EmptyInstances.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => EmptyInstances.GetEnumerator();

	/// <inheritdoc />
	public IVolumeInstance CreateInstance()
	{
		throw new InvalidOperationException(
			$"{nameof(this.CreateInstance)} can only be called when running in Unity.");
	}
}
