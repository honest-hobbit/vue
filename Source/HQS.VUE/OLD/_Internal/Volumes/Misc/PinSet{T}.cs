﻿namespace HQS.VUE.OLD;

internal class PinSet<T> : IReadOnlyList<ReadOnlyPinView<T>>, IDeinitializable
	where T : IKeyed<ChunkKey>
{
	private readonly HashSet<Int3> pinSet = new HashSet<Int3>(MortonCurve.EqualityComparer.ForInt3);

	private readonly List<ReadOnlyPin<T>> pinList = new List<ReadOnlyPin<T>>(VUEConstants.ChunksModifiedPerJob);

	public ReadOnlyPinView<T> this[int index] => this.pinList[index].AsView;

	/// <inheritdoc />
	public int Count => this.pinList.Count;

	public void Add(ReadOnlyPinView<T> pin)
	{
		Debug.Assert(!pin.IsNull);
		Debug.Assert(pin.IsPinned);

		if (this.pinSet.Add(pin.Value.Key.Location))
		{
			this.pinList.Add(pin.CreatePin());
		}
	}

	public void AddMany(IReadOnlyList<ReadOnlyPinView<T>> pins)
	{
		Debug.Assert(pins != null);

		int count = pins.Count;
		for (int i = 0; i < count; i++)
		{
			this.Add(pins[i]);
		}
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		int count = this.pinList.Count;
		for (int i = 0; i < count; i++)
		{
			this.pinList[i].Unpin();
		}

		this.pinList.Clear();
		this.pinSet.Clear();
	}

	/// <inheritdoc />
	public IEnumerator<ReadOnlyPinView<T>> GetEnumerator()
	{
		int count = this.pinList.Count;
		for (int i = 0; i < count; i++)
		{
			yield return this.pinList[i].AsView;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
