﻿namespace HQS.VUE.OLD;

internal class VoxelGridConfig
{
	private readonly EngineValidator validator;

	private string name;

	private bool autosave;

	private string savedName;

	private bool savedAutosave;

	public VoxelGridConfig(EngineValidator validator, VoxelGridRecord record)
	{
		Debug.Assert(validator != null);
		Debug.Assert(record.IsValid);

		this.validator = validator;
		this.name = record.Name ?? string.Empty;
		this.autosave = record.Autosave;
		this.savedName = this.name;
		this.savedAutosave = this.autosave;
	}

	public bool HasUnsavedConfig
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.name != this.savedName || this.autosave != this.savedAutosave;
		}
	}

	public string Name
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.name;
		}

		set
		{
			Debug.Assert(this.validator.IsValid);

			this.name = value ?? string.Empty;
		}
	}

	public bool Autosave
	{
		get
		{
			Debug.Assert(this.validator.IsValid);

			return this.autosave;
		}

		set
		{
			Debug.Assert(this.validator.IsValid);

			this.autosave = value;
		}
	}

	public void GetInfo(ref VoxelGridRunJobInfo info)
	{
		Debug.Assert(this.validator.IsValid);

		info.Name = this.name;
		info.Autosave = this.autosave;
		info.HasUnsavedConfig = this.HasUnsavedConfig;
	}

	public void SetSavedInfo(VoxelGridSavedInfo info)
	{
		Debug.Assert(this.validator.IsValid);
		Debug.Assert(info.Name != null);

		this.savedName = info.Name;
		this.savedAutosave = info.Autosave;
	}

	public void SetUnsaved()
	{
		Debug.Assert(this.validator.IsValid);

		this.savedName = null;
	}
}
