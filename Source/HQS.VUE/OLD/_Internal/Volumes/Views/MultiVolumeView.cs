﻿namespace HQS.VUE.OLD;

internal class MultiVolumeView : IVoxelVolumesView
{
	private readonly Dictionary<Guid, Pinned<VoxelView>> readViews;

	private readonly Dictionary<Guid, Pinned<VoxelView>> writeViews;

	private readonly List<Pin<VoxelView>> viewPins;

	private readonly List<Pinned<VoxelView>> changedVolumes;

	private readonly IPinPool<VoxelView> viewsPool;

	public MultiVolumeView(VoxelDefinitions definitions, IPinPool<VoxelView> viewPool)
	{
		Debug.Assert(definitions != null);
		Debug.Assert(viewPool != null);

		this.Definitions = definitions;
		this.viewsPool = viewPool;

		int capacity = VUEConstants.VolumesPerJob;
		var comparer = EqualityComparer.ForStruct<Guid>();
		this.readViews = new Dictionary<Guid, Pinned<VoxelView>>(capacity, comparer);
		this.writeViews = new Dictionary<Guid, Pinned<VoxelView>>(capacity, comparer);
		this.viewPins = new List<Pin<VoxelView>>(capacity);
		this.changedVolumes = new List<Pinned<VoxelView>>(capacity);

		this.ReadVolumes = ReadOnlyDictionary.Convert(this.readViews, x => x.Value.Read);
		this.WriteVolumes = ReadOnlyDictionary.Convert(this.writeViews, x => (IVoxelWriteView)x.Value);
		this.ChangedVolumes = ReadOnlyList.Convert(this.changedVolumes, x => x.Value);
	}

	public IReadOnlyList<VolumeView> ChangedVolumes { get; }

	/// <inheritdoc />
	public IVoxelDefinitions Definitions { get; }

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, IVoxelReadView> ReadVolumes { get; }

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, IVoxelWriteView> WriteVolumes { get; }

	/// <inheritdoc />
	public void DiscardAllChanges()
	{
		Debug.Assert(this.IsInitialized);

		int max = this.viewPins.Count;
		for (int i = 0; i < max; i++)
		{
			this.viewPins[i].Value.ClearChanges();
		}
	}

	public bool IsInitialized { get; private set; } = false;

	public void Initialize(IVoxelJob job)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(job != null);
		Debug.Assert(job.ReadVolumes.Any() || job.WriteVolumes.Any());

		foreach (var volume in job.ReadVolumes)
		{
			if (!this.readViews.ContainsKey(volume.Key))
			{
				var pin = this.InitializeView(volume);
				this.readViews.Add(volume.Key, pin.AsPinned);
			}
		}

		foreach (var volume in job.WriteVolumes)
		{
			if (this.readViews.TryGetValue(volume.Key, out var pin))
			{
				var view = pin.Value;
				if (!view.IsWritingVoxels)
				{
					view.SetToWriteVoxels();
					this.writeViews.Add(volume.Key, pin);
				}
			}
			else
			{
				pin = this.InitializeView(volume).AsPinned;
				pin.Value.SetToWriteVoxels();
				this.writeViews.Add(volume.Key, pin);
				this.readViews.Add(volume.Key, pin);
			}
		}

		this.IsInitialized = true;
	}

	public void CleanupVoxelChanges()
	{
		Debug.Assert(this.IsInitialized);

		int max = this.viewPins.Count;
		for (int i = 0; i < max; i++)
		{
			var pin = this.viewPins[i];
			if (pin.Value.TryCleanupVoxelChanges())
			{
				this.changedVolumes.Add(pin.AsPinned);
			}
		}
	}

	public void CommitChanges(DateTime dateRan)
	{
		Debug.Assert(this.IsInitialized);

		int max = this.changedVolumes.Count;
		for (int i = 0; i < max; i++)
		{
			this.changedVolumes[i].Value.CommitChangesAndCleanup(dateRan);
		}
	}

	public void Deinitialize()
	{
		Debug.Assert(this.IsInitialized);

		int max = this.viewPins.Count;
		for (int i = 0; i < max; i++)
		{
			var view = this.viewPins[i];
			view.Value.Deinitialize();
			view.Dispose();
		}

		this.readViews.Clear();
		this.writeViews.Clear();
		this.viewPins.Clear();
		this.changedVolumes.Clear();
		this.IsInitialized = false;
	}

	private Pin<VoxelView> InitializeView(Volume volume)
	{
		Debug.Assert(volume != null);

		var pin = this.viewsPool.Rent();
		pin.Value.Initialize(volume);
		this.viewPins.Add(pin);
		return pin;
	}
}
