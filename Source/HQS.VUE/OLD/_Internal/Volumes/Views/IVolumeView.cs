﻿namespace HQS.VUE.OLD;

internal interface IVolumeView : IKeyed<Guid>
{
	bool IsInitialized { get; }

	Volume Volume { get; }

	VoxelGridRunJobInfo RunJobInfo { get; }

	// this contains the data before commiting changes
	// after commiting this will always be empty
	IReadOnlyList<VoxelChunkChangeset> ChangedVoxelChunks { get; }

	// this contains the data before commiting changes
	// after commiting this will always be empty
	IReadOnlyList<ReadOnlyContourChunkResult> ChangedContourChunks { get; }

	// this contains the data after commiting changes and is only used for persisting changes
	// before commiting this will always be empty
	IReadOnlyList<ReadOnlyPinView<ReadOnlyVoxelChunk>> ChangedVoxelChunkPins { get; }

	// this contains the data after commiting changes and is only used for persisting changes
	// before commiting this will always be empty
	IReadOnlyList<ReadOnlyPinView<ReadOnlyContourChunk>> ChangedContourChunkPins { get; }

	// TODO this is for changeset records and that whole subsystem should be handled differently
	IReadOnlyList<ChangeRecord> ChangeRecords { get; }

	int VoxelsChanged { get; }

	bool TryGetVoxelChunkChangeset(Int3 key, out VoxelChunkChangeset chunk);
}
