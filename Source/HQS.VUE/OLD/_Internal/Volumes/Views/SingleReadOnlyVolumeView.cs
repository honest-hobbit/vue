﻿namespace HQS.VUE.OLD;

internal class SingleReadOnlyVolumeView : IReadOnlyList<IVolumeView>
{
	public ReadOnlyVolumeView View { get; } = new ReadOnlyVolumeView();

	public int Count => 1;

	public IVolumeView this[int index]
	{
		get
		{
			IReadOnlyListContracts.Indexer(this, index);

			return this.View;
		}
	}

	public IEnumerator<IVolumeView> GetEnumerator()
	{
		yield return this.View;
	}

	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
