﻿namespace HQS.VUE.OLD;

internal class ReadOnlyVolumeView : IVolumeView
{
	/// <inheritdoc />
	public Guid Key { get; private set; }

	/// <inheritdoc />
	public bool IsInitialized => this.Volume != null;

	/// <inheritdoc />
	public Volume Volume { get; private set; }

	/// <inheritdoc />
	public VoxelGridRunJobInfo RunJobInfo { get; private set; }

	/// <inheritdoc />
	public IReadOnlyList<VoxelChunkChangeset> ChangedVoxelChunks { get; } =
		ReadOnlyList.Empty<VoxelChunkChangeset>();

	/// <inheritdoc />
	public IReadOnlyList<ReadOnlyContourChunkResult> ChangedContourChunks { get; } =
		ReadOnlyList.Empty<ReadOnlyContourChunkResult>();

	/// <inheritdoc />
	public IReadOnlyList<ReadOnlyPinView<ReadOnlyVoxelChunk>> ChangedVoxelChunkPins { get; } =
		ReadOnlyList.Empty<ReadOnlyPinView<ReadOnlyVoxelChunk>>();

	/// <inheritdoc />
	public IReadOnlyList<ReadOnlyPinView<ReadOnlyContourChunk>> ChangedContourChunkPins { get; } =
		ReadOnlyList.Empty<ReadOnlyPinView<ReadOnlyContourChunk>>();

	/// <inheritdoc />
	public IReadOnlyList<ChangeRecord> ChangeRecords { get; } = ReadOnlyList.Empty<ChangeRecord>();

	/// <inheritdoc />
	public int VoxelsChanged => 0;

	/// <inheritdoc />
	public bool TryGetVoxelChunkChangeset(Int3 key, out VoxelChunkChangeset chunk)
	{
		chunk = VoxelChunkChangeset.Null;
		return false;
	}

	public void Initialize(Volume volume)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(volume != null);

		this.Key = volume.Key;
		this.Volume = volume;
		this.RunJobInfo = volume.GetRunJobInfo();
	}

	public void Deinitialize()
	{
		Debug.Assert(this.IsInitialized);

		this.Key = Guid.Empty;
		this.Volume = null;
		this.RunJobInfo = default;
	}
}
