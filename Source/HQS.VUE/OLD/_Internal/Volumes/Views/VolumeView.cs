﻿namespace HQS.VUE.OLD;

internal class VolumeView : IVolumeView
{
	private readonly List<ChangeRecord> recorders = new List<ChangeRecord>();

	private readonly VoxelChunkChangeTracker voxelChanges;

	private readonly ContourChunkChangeTracker contourChanges;

	private VoxelGridRunJobInfo configSnapshot;

	public VolumeView(VoxelChunkPools voxelPools, ContourChunkPools contourPools)
	{
		Debug.Assert(voxelPools != null);
		Debug.Assert(contourPools != null);

		this.voxelChanges = new VoxelChunkChangeTracker(voxelPools, VUEConstants.ChunksModifiedPerJob);
		this.contourChanges = new ContourChunkChangeTracker(contourPools, VUEConstants.ChunksModifiedPerJob);
	}

	/// <inheritdoc />
	public Guid Key
	{
		get => this.voxelChanges.Key;
		private set
		{
			this.voxelChanges.Key = value;
			this.contourChanges.Key = value;
		}
	}

	public bool IsWritingVoxels { get; private set; } = false;

	/// <inheritdoc />
	public bool IsInitialized => this.Volume != null;

	/// <inheritdoc />
	public Volume Volume { get; private set; }

	/// <inheritdoc />
	public VoxelGridRunJobInfo RunJobInfo => this.configSnapshot;

	/// <inheritdoc />
	public IReadOnlyList<VoxelChunkChangeset> ChangedVoxelChunks => this.voxelChanges.Chunks;

	/// <inheritdoc />
	public IReadOnlyList<ReadOnlyContourChunkResult> ChangedContourChunks => this.contourChanges.Chunks;

	/// <inheritdoc />
	public IReadOnlyList<ReadOnlyPinView<ReadOnlyVoxelChunk>> ChangedVoxelChunkPins => this.voxelChanges.Pins;

	/// <inheritdoc />
	public IReadOnlyList<ReadOnlyPinView<ReadOnlyContourChunk>> ChangedContourChunkPins => this.contourChanges.Pins;

	/// <inheritdoc />
	public IReadOnlyList<ChangeRecord> ChangeRecords => this.recorders;

	/// <inheritdoc />
	public int VoxelsChanged { get; set; }

	public void Initialize(Volume volume)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(volume != null);

		this.Key = volume.Key;
		this.Volume = volume;
		this.configSnapshot = volume.GetRunJobInfo();
		this.OnConfigSnapshotSet();
	}

	public void Deinitialize()
	{
		Debug.Assert(this.IsInitialized);

		this.IsWritingVoxels = false;
		this.Key = Guid.Empty;
		this.Volume = null;
		this.configSnapshot = default;
		this.VoxelsChanged = 0;
		this.voxelChanges.Clear();
		this.contourChanges.Clear();
		this.recorders.Clear();
		this.OnDeinitialized();
	}

	public void SetToWriteVoxels()
	{
		Debug.Assert(this.IsInitialized);

		if (this.IsWritingVoxels)
		{
			return;
		}

		this.IsWritingVoxels = true;

		// add voxel change recorders paired with the animators they came from
		var instances = this.Volume.Instances;
		int max = instances.Count;
		for (int i = 0; i < max; i++)
		{
			if (instances[i].TryGetChangeRecord(out var record))
			{
				this.recorders.Add(record);
			}
		}
	}

	// returns true if there are changes left after cleanup, false if there are no changes
	public bool TryCleanupVoxelChanges()
	{
		Debug.Assert(this.IsInitialized);

		if (this.IsWritingVoxels)
		{
			this.voxelChanges.CleanupChanges();
			return this.ChangedVoxelChunks.Count > 0;
		}
		else
		{
			return false;
		}
	}

	public void CommitChangesAndCleanup(DateTime dateRan)
	{
		Debug.Assert(this.IsInitialized);

		if (this.IsWritingVoxels)
		{
			this.voxelChanges.CommitChanges();
			this.Volume.EngineData.UpdateStats(dateRan);
			this.IsWritingVoxels = false;
		}

		this.contourChanges.CommitChanges();
		this.voxelChanges.PostCommitCleanup();
		this.contourChanges.PostCommitCleanup();
	}

	public void ClearChanges()
	{
		Debug.Assert(this.IsInitialized);

		this.VoxelsChanged = 0;
		this.voxelChanges.Clear();
		this.contourChanges.Clear();
	}

	public void TrackChunk(ContourChunkResult chunk)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!chunk.IsNull);

		this.contourChanges.AddChunk(chunk);
	}

	public void TrackChunk(ModifiedVoxelChunk chunk)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.IsWritingVoxels);
		Debug.Assert(chunk != null);

		this.voxelChanges.AddChunk(chunk);
	}

	public bool TryGetModifiedVoxelChunk(Int3 key, out ModifiedVoxelChunk chunk)
	{
		Debug.Assert(this.IsInitialized);

		return this.voxelChanges.TryGetChunk(key, out chunk);
	}

	/// <inheritdoc />
	public bool TryGetVoxelChunkChangeset(Int3 key, out VoxelChunkChangeset chunk)
	{
		Debug.Assert(this.IsInitialized);

		if (this.voxelChanges.TryGetChunk(key, out var modifiedChunk))
		{
			chunk = modifiedChunk.AsChangeset;
			return true;
		}
		else
		{
			chunk = VoxelChunkChangeset.Null;
			return false;
		}
	}

	protected virtual void OnConfigSnapshotSet()
	{
	}

	protected virtual void OnDeinitialized()
	{
	}
}
