﻿namespace HQS.VUE.OLD;

internal class VoxelView : VolumeView, IVoxelWriteView
{
	private readonly VoxelWriter writer;

	public VoxelView(
		VoxelSizeConfig config,
		VoxelDefinitions definitions,
		VoxelChunkCache voxelCache,
		VoxelChunkPools voxelPools,
		ContourChunkPools contourPools)
		: base(voxelPools, contourPools)
	{
		Debug.Assert(config != null);
		Debug.Assert(definitions != null);
		Debug.Assert(voxelCache != null);

		this.Definitions = definitions;
		this.writer = new VoxelWriter(voxelPools, voxelCache, config, definitions);
		this.Read = new ReadView(this);
	}

	/// <inheritdoc />
	public IVoxelDefinitions Definitions { get; }

	/// <inheritdoc />
	public IVoxelReadView Read { get; }

	/// <inheritdoc />
	public Voxel this[int x, int y, int z]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => this[new Int3(x, y, z)];

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set => this[new Int3(x, y, z)] = value;
	}

	/// <inheritdoc />
	public Voxel this[Int3 index]
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get
		{
			Debug.Assert(this.IsInitialized);
			Debug.Assert(this.IsWritingVoxels);

			return this.writer.GetNewVoxel(index);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		set
		{
			Debug.Assert(this.IsInitialized);
			Debug.Assert(this.IsWritingVoxels);

			this.writer.Replace(index, value);
		}
	}

	/// <inheritdoc />
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public Voxel Replace(int x, int y, int z, Voxel voxel)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.IsWritingVoxels);

		return this.writer.Replace(new Int3(x, y, z), voxel);
	}

	/// <inheritdoc />
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public Voxel Replace(Int3 index, Voxel voxel)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.IsWritingVoxels);

		return this.writer.Replace(index, voxel);
	}

	/// <inheritdoc />
	public void DiscardChanges()
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.IsWritingVoxels);

		this.ClearChanges();
	}

	/// <inheritdoc />
	protected override void OnConfigSnapshotSet() => this.writer.Initialize(this);

	/// <inheritdoc />
	protected override void OnDeinitialized() => this.writer.Deinitialize();

	private class ReadView : IVoxelReadView
	{
		private readonly VoxelView parent;

		public ReadView(VoxelView parent)
		{
			Debug.Assert(parent != null);

			this.parent = parent;
		}

		/// <inheritdoc />
		public Guid Key => this.parent.Key;

		/// <inheritdoc />
		public IVoxelDefinitions Definitions => this.parent.Definitions;

		/// <inheritdoc />
		public Voxel this[int x, int y, int z]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => this[new Int3(x, y, z)];
		}

		/// <inheritdoc />
		public Voxel this[Int3 index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				Debug.Assert(this.parent.IsInitialized);

				return this.parent.writer.GetOldVoxel(index);
			}
		}
	}
}
