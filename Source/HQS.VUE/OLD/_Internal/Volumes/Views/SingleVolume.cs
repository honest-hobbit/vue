﻿namespace HQS.VUE.OLD;

internal class SingleVolume : IEnumerable<Volume>
{
	public Volume Volume { get; set; }

	/// <inheritdoc />
	public IEnumerator<Volume> GetEnumerator()
	{
		if (this.Volume != null)
		{
			yield return this.Volume;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
