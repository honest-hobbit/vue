﻿namespace HQS.VUE.OLD;

internal class SingleVolumeView : IReadOnlyList<VolumeView>
{
	public SingleVolumeView(VolumeView view)
	{
		Debug.Assert(view != null);

		this.View = view;
	}

	public VolumeView View { get; }

	public int Count => 1;

	public VolumeView this[int index]
	{
		get
		{
			IReadOnlyListContracts.Indexer(this, index);

			return this.View;
		}
	}

	public IEnumerator<VolumeView> GetEnumerator()
	{
		yield return this.View;
	}

	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
