﻿namespace HQS.VUE.OLD;

[SuppressMessage("Design", "CA1001", Justification = "AbstractCompletable handles disposal.")]
internal class MainCommandProcessorOLD : AbstractCompletable, ICommandProcessor<IMainCommand>
{
	private readonly Subject<VoxelJobDiagnostics> diagnostics = new Subject<VoxelJobDiagnostics>();

	private readonly IReporter<Exception> errorReporter;

	private readonly SingleConsumerQueue<Pin<IMainCommand>> commandQueue;

	public MainCommandProcessorOLD(IReporter<Exception> errorReporter)
	{
		Debug.Assert(errorReporter != null);

		this.errorReporter = errorReporter;
		this.Diagnostics = this.diagnostics.Synchronize();

		this.commandQueue = new SingleConsumerQueue<Pin<IMainCommand>>(this.RunCommand);
	}

	public IObservable<VoxelJobDiagnostics> Diagnostics { get; }

	/// <inheritdoc />
	public void Submit<TCommand>(Pinned<TCommand> command)
		where TCommand : class, IMainCommand => this.Submit(command.CreatePin());

	/// <inheritdoc />
	public void Submit<TCommand>(Pin<TCommand> command)
		where TCommand : class, IMainCommand
	{
		Debug.Assert(!this.IsCompleting);
		Debug.Assert(command.IsPinned);

		this.commandQueue.Add(command.CastTo<IMainCommand>());
	}

	/// <inheritdoc />
	protected async override Task CompleteAsync()
	{
		try
		{
			this.commandQueue.Complete();
			await this.commandQueue.Completion.DontMarshallContext();
		}
		finally
		{
			this.diagnostics.OnCompleted();
			this.diagnostics.Dispose();
		}
	}

	private void RunCommand(Pin<IMainCommand> pooled)
	{
		try
		{
			var command = pooled.Value;
			command.Run();

			if (command is IWaitable<VoxelJobDiagnostics> waitable)
			{
				this.diagnostics.OnNext(waitable.Result);
			}
		}
		catch (Exception error)
		{
			this.errorReporter.Report(error);
			throw;
		}
		finally
		{
			pooled.Dispose();
		}
	}
}
