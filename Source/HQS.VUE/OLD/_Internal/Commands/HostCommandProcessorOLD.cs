﻿namespace HQS.VUE.OLD;

internal class HostCommandProcessorOLD : ICommandProcessor<IHostCommand>
{
	private readonly ConcurrentQueue<Pin<IHostCommand>> queue = new ConcurrentQueue<Pin<IHostCommand>>();

	private readonly IReporter<Exception> errorReporter;

	public HostCommandProcessorOLD(IReporter<Exception> errorReporter)
	{
		Debug.Assert(errorReporter != null);

		this.errorReporter = errorReporter;
	}

	/// <inheritdoc />
	public void Submit<TCommand>(Pinned<TCommand> command)
		where TCommand : class, IHostCommand => this.Submit(command.CreatePin());

	/// <inheritdoc />
	public void Submit<TCommand>(Pin<TCommand> command)
		where TCommand : class, IHostCommand
	{
		Debug.Assert(command.IsPinned);

		this.queue.Enqueue(command.CastTo<IHostCommand>());
	}

	public int RunCommands()
	{
		int count = 0;
		while (this.queue.TryDequeue(out var pooled))
		{
			this.RunCommand(pooled);
			count++;
		}

		return count;
	}

	private void RunCommand(Pin<IHostCommand> pooled)
	{
		try
		{
			pooled.Value.Run();
		}
		catch (Exception error)
		{
			this.errorReporter.Report(error);
			throw;
		}
		finally
		{
			pooled.Dispose();
		}
	}
}
