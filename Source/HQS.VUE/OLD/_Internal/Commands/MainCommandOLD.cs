﻿namespace HQS.VUE.OLD;

internal abstract class MainCommandOLD : AbstractResultCommand<VoxelJobDiagnostics>, IMainCommand
{
	protected VoxelJobDiagnosticsRecorder Diagnostics { get; } = new VoxelJobDiagnosticsRecorder();

	/// <inheritdoc />
	protected sealed override VoxelJobDiagnostics RunAndGetResult()
	{
		this.Diagnostics.JobType = this.GetType();
		this.Diagnostics.DateRan = DateTime.UtcNow;
		this.RunCommand();
		return this.Diagnostics.GetSnapshot();
	}

	/// <inheritdoc />
	protected sealed override void OnDeinitialize()
	{
		try
		{
			this.OnDeinitialized();
		}
		finally
		{
			this.Diagnostics.Clear();
		}
	}

	protected abstract void RunCommand();

	protected virtual void OnDeinitialized()
	{
	}
}
