﻿namespace HQS.VUE.OLD;

internal class VoxelSizeConfig : IVoxelSizeConfig
{
	private readonly int subchunkMask;

	private readonly int voxelMask;

	public VoxelSizeConfig(UniverseConfig config)
	{
		Debug.Assert(config != null);
		Debug.Assert(IsExponentValid(config.SubchunkExponent));
		Debug.Assert(IsExponentValid(config.VoxelExponent));

		this.SubchunkIndexer = new CubeArrayIndexer(config.SubchunkExponent);
		this.VoxelIndexer = new CubeArrayIndexer(config.VoxelExponent);

		this.ChunkExponent = this.SubchunkIndexer.Exponent + this.VoxelIndexer.Exponent;
		this.ChunkSideLengthInVoxels = MathUtility.PowerOf2(this.ChunkExponent);

		this.subchunkMask = BitMask.CreateIntWithOnesInLowerBits(this.ChunkExponent);
		this.voxelMask = BitMask.CreateIntWithOnesInLowerBits(this.VoxelIndexer.Exponent);
	}

	// Min of 1 was chosen to ensure some efficiency of batching data together occurs.
	// Min of 0 works but significantly degrades performance.
	public static int MinExponent => 1;

	// TODO maybe this max of 5 should be expanded to 7 by changing shorts to ints?
	// Max of 5 was chosen so that Shorts can be used to index into 3D arrays mapped to 1D ((2^5)^3 - 1 <= 2^15 - 1 aka Short.MaxValue).
	// Otherwise it would have been a max of 7 because MeshQuadSlim and BoxColliderStructSlim use single bytes
	// and need to store a range of 2^x + 1 so therefore 2^7 + 1 <= 2^8 aka Byte.MaxValue.
	public static int MaxExponent => 5;

	public static bool IsExponentValid(int exponent) => exponent.IsIn(MinExponent, MaxExponent);

	public static void ValidateExponent(int exponent, string exponentName)
	{
		Debug.Assert(!exponentName.IsNullOrWhiteSpace());

		if (!IsExponentValid(exponent))
		{
			throw new ArgumentOutOfRangeException(
				exponentName, $"{exponentName} of {exponent} is not in the range of {MinExponent} to {MaxExponent} inclusive.");
		}
	}

	public int ChunkExponent { get; }

	/// <inheritdoc />
	public CubeArrayIndexer SubchunkIndexer { get; }

	/// <inheritdoc />
	public CubeArrayIndexer VoxelIndexer { get; }

	public int ChunkSideLengthInVoxels { get; }

	// gets the chunk index from a volume voxel index
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public Int3 ConvertVolumeVoxelToChunkIndex(Int3 voxelIndex) => voxelIndex >> this.ChunkExponent;

	// gets the subchunk index of a chunk from a volume voxel index
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public Int3 ConvertVolumeVoxelToSubchunkIndex(Int3 voxelIndex) =>
		(voxelIndex & this.subchunkMask) >> this.VoxelIndexer.Exponent;

	// gets the voxel index of a subchunk from a volume voxel index
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public Int3 ConvertVolumeVoxelToSubchunkVoxelIndex(Int3 voxelIndex) => voxelIndex & this.voxelMask;

	// gets the subchunk index of a chunk from a volume voxel index
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public int ConvertVolumeVoxelToSubchunkInt(Int3 voxelIndex) =>
		this.SubchunkIndexer[(voxelIndex & this.subchunkMask) >> this.VoxelIndexer.Exponent];

	// gets the voxel index of a subchunk from a volume voxel index
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public int ConvertVolumeVoxelToSubchunkVoxelInt(Int3 voxelIndex) => this.VoxelIndexer[voxelIndex & this.voxelMask];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public Int3 ConvertChunkToVoxelIndex(Int3 chunkIndex) => chunkIndex << this.ChunkExponent;
}
