﻿namespace HQS.VUE.OLD;

internal class VoxelDefinitions : IVoxelDefinitions
{
	public VoxelDefinitions(
		VoxelTypes voxels, SurfaceTypes surfaces, PolyTypes polys, ColliderTypes colliders)
	{
		Debug.Assert(voxels != null);
		Debug.Assert(surfaces != null);
		Debug.Assert(polys != null);
		Debug.Assert(colliders != null);

		this.Voxels = voxels;
		this.Surfaces = surfaces;
		this.Polys = polys;
		this.Colliders = colliders;
	}

	/// <inheritdoc />
	public ITypeList<Voxel, VoxelTypeData> Voxels { get; }

	/// <inheritdoc />
	public ITypeList<SurfaceTypeIndex, SurfaceVisibility> Surfaces { get; }

	/// <inheritdoc />
	public ITypeList<PolyTypeIndex, PolyType> Polys { get; }

	/// <inheritdoc />
	public ITypeList<ColliderTypeIndex, ColliderType> Colliders { get; }
}
