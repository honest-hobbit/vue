﻿namespace HQS.VUE.OLD;

internal class VoxelTypes : ITypeList<Voxel, VoxelTypeData>
{
	private readonly VoxelTypeData[] types;

	public VoxelTypes(VoxelTypeData[] types)
	{
		Debug.Assert(types != null);

		this.types = types;
	}

	public VoxelTypeData this[Voxel voxel] => this.types[voxel.TypeIndex];

	/// <inheritdoc />
	public VoxelTypeData this[int index] => this.types[index];

	/// <inheritdoc />
	public int Count => this.types.Length;

	/// <inheritdoc />
	public IEnumerator<VoxelTypeData> GetEnumerator() => this.types.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
