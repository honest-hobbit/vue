﻿namespace HQS.VUE.OLD;

internal class PolyTypes : ITypeList<PolyTypeIndex, PolyType>
{
	private readonly PolyType[] types;

	public PolyTypes(PolyType[] types)
	{
		Debug.Assert(types != null);

		this.types = types;
	}

	public PolyType this[PolyTypeIndex index] => this.types[index.Index];

	/// <inheritdoc />
	public PolyType this[int index] => this.types[index];

	/// <inheritdoc />
	public int Count => this.types.Length;

	/// <inheritdoc />
	public IEnumerator<PolyType> GetEnumerator() => this.types.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
