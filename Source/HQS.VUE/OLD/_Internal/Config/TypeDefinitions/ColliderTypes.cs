﻿namespace HQS.VUE.OLD;

internal class ColliderTypes : ITypeList<ColliderTypeIndex, ColliderType>
{
	private readonly ColliderType[] types;

	public ColliderTypes(ColliderType[] types)
	{
		Debug.Assert(types != null);

		this.types = types;
	}

	public ColliderType this[ColliderTypeIndex index] => this.types[index.Index];

	/// <inheritdoc />
	public ColliderType this[int index] => this.types[index];

	/// <inheritdoc />
	public int Count => this.types.Length;

	/// <inheritdoc />
	public IEnumerator<ColliderType> GetEnumerator() => this.types.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
