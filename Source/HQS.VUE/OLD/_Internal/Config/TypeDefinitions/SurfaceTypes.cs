﻿namespace HQS.VUE.OLD;

internal class SurfaceTypes : ITypeList<SurfaceTypeIndex, SurfaceVisibility>
{
	private readonly SurfaceVisibility[] types;

	public SurfaceTypes(SurfaceVisibility[] types)
	{
		Debug.Assert(types != null);

		this.types = types;
	}

	public SurfaceVisibility this[SurfaceTypeIndex index] => this.types[index.Index];

	/// <inheritdoc />
	public SurfaceVisibility this[int index] => this.types[index];

	/// <inheritdoc />
	public int Count => this.types.Length;

	/// <inheritdoc />
	public IEnumerator<SurfaceVisibility> GetEnumerator() => this.types.GetEnumeratorTypeSafe();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
