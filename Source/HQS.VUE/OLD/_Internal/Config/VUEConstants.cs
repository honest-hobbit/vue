﻿namespace HQS.VUE.OLD;

internal static class VUEConstants
{
	public static int VolumesPerJob => 4;

	public static int ChunksModifiedPerJob => 8;

	////public static int MeshesPerChunk => 2;

	////public static float PercentChunksUniform => .5f;

	////public static float PercentSubchunksUniform => .5f;
}
