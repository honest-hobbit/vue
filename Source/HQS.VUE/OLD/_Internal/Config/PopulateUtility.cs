﻿namespace HQS.VUE.OLD;

internal static class PopulateUtility
{
	////public static int GetNonuniformChunks(int totalChunks)
	////{
	////	Debug.Assert(totalChunks >= 0);

	////	return (int)(totalChunks * (1f - VUEConstants.PercentChunksUniform)).ClampLower(0);
	////}

	////public static int GetNonuniformSubchunks(VoxelSizeConfig config, int totalChunks)
	////{
	////	Debug.Assert(config != null);
	////	Debug.Assert(totalChunks >= 0);

	////	return (int)(
	////		GetNonuniformChunks(totalChunks) *
	////		config.SubchunkIndexer.Length *
	////		(1f - VUEConstants.PercentSubchunksUniform)).ClampLower(0);
	////}

	public static Func<T[]> GetCreateArraysFunc<T>(int length)
	{
		Debug.Assert(length >= 0);

		return () => new T[length];
	}
}
