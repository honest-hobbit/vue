﻿namespace HQS.VUE.OLD;

internal static class ContainerUtilityExtensionsOLD
{
	public static void RegisterAllPoolsOLD(this Container container, IEnumerable<PooledType> pooledTypes, params Type[] legacyTypes)
	{
		Debug.Assert(container != null);
		Debug.Assert(pooledTypes.AllAndSelfNotNull());
		Debug.Assert(legacyTypes != null);

		var specialTypes = new HashSet<Type>(legacyTypes);

		foreach (var pooledType in pooledTypes)
		{
			if (specialTypes.Contains(pooledType.ResourceType))
			{
				var poolType = typeof(TypeResolverPool<>).MakeGenericType(pooledType.ResourceType);

				container.RegisterSingleton(typeof(IPool<>), poolType);
				container.Collection.Append(typeof(ITypePool), poolType, Lifestyle.Singleton);
				container.Collection.AppendInstance(pooledType);
			}
			else
			{
				container.RegisterPool(pooledType);
			}
		}
	}
}
