﻿namespace HQS.VUE.OLD;

internal static class VoxelEngineSetupOLD
{
	public static void SetUpVoxelEngineOLD(this Container container, UniverseConfig universeConfig, VoxelTypesConfig voxelTypes)
	{
		Debug.Assert(container != null);
		Debug.Assert(universeConfig != null);
		Debug.Assert(voxelTypes != null);

		container.Options.ResolveUnregisteredConcreteTypes = false;
		container.Options.DefaultLifestyle = Lifestyle.Transient;
		container.Options.DefaultScopedLifestyle = ScopedLifestyle.Flowing;

		container.RegisterInstance(universeConfig);
		container.RegisterInstance(voxelTypes);
		container.RegisterInstance(voxelTypes.Definitions);

		container.AutoResolveFactories(autoResolveServiceTypes: false);
		container.RegisterAllCommandsFromAssembly<INamespaceOLD>(typeof(VoxelEngineSetupOLD).Assembly);
		container.RegisterAllTypeResolversFromAssembly(typeof(VoxelEngineSetupOLD).Assembly);

		container.RegisterSingleton<TypePoolCollection>();
		container.RegisterSingleton<IPooledTypeResolver, PooledTypeResolver>();

		container.RegisterAllPoolsOLD(
			PooledTypesOLD.EnumerateAll(),
			typeof(VoxelChunk),
			typeof(VoxelSubchunk[]),
			typeof(Voxel[]),
			typeof(ModifiedVoxelChunk),
			typeof(ContourChunk),
			typeof(ContourSubchunk[]),
			typeof(ContourSubchunk));

		container.RegisterSingleton<VoxelSizeConfig>();
		container.RegisterSingleton<EngineStatus>();
		container.RegisterSingleton<EngineValidator>();
		container.RegisterSingleton(typeof(Reporter<>), typeof(Reporter<>));
		container.RegisterSingleton(typeof(IReporter<>), typeof(Reporter<>));

		container.RegisterSingleton<HostCommandProcessorOLD>();
		container.RegisterSingleton<ICommandProcessor<IHostCommand>, HostCommandProcessorOLD>();
		container.RegisterSingleton<MainCommandProcessorOLD>();
		container.RegisterSingleton<ICommandProcessor<IMainCommand>, MainCommandProcessorOLD>();
		container.RegisterSingleton<ParallelCommandProcessor>();
		container.RegisterSingleton<ICommandProcessor<IParallelCommand>, ParallelCommandProcessor>();
		container.RegisterSingleton<PersistenceCommandProcessor>();
		container.RegisterSingleton<ICommandProcessor<IPersistenceCommand>, PersistenceCommandProcessor>();

		container.Register<ContourChunkData>();
		container.Register<PinSet<ReadOnlyVoxelChunk>>();
		container.Register<PinSet<ReadOnlyContourChunk>>();
		container.Register<IByteBuffer, VUEByteBuffer>();

		container.RegisterSingleton<VoxelChunkCache>();
		container.RegisterSingleton<VoxelChunkSerDes>();
		container.RegisterSingleton<ISerializer<ReadOnlyVoxelChunk>, VoxelChunkSerDes>();
		container.RegisterSingleton<IInlineDeserializer<VoxelChunk>, VoxelChunkSerDes>();
		container.RegisterSingleton<VoxelChunkPools>();

		container.RegisterSingleton<ContourChunkCache>();
		container.RegisterSingleton<ContourChunkSerDes>();
		container.RegisterSingleton<ISerializer<ReadOnlyContourChunk>, ContourChunkSerDes>();
		container.RegisterSingleton<IInlineDeserializer<ContourChunk>, ContourChunkSerDes>();
		container.RegisterSingleton<ContourChunkPools>();

		container.Register<VoxelView>();
		container.Register<VolumeView>();
		container.Register<MultiVolumeView>();

		container.RegisterSingleton<VoxelAdjacencyTable>();
		container.Register<VoxelChangesRecorder>();

		container.Register<ContouringQueue>(Lifestyle.Scoped);
		container.Register<ContouringQueueManager>(Lifestyle.Scoped);
		container.Register<SubchunkContourer>();

		container.Register<VolumeContourer>(Lifestyle.Scoped);

		container.Register<ChangesetContourer>(Lifestyle.Scoped);
		container.Register<ChangesetRecorder>(Lifestyle.Scoped);

		container.RegisterSingleton<VoxelGridPersister>();
		container.RegisterSingleton<VoxelShapePersister>();
		container.RegisterSingleton<ChangesetPersister>();

		container.RegisterSingleton<VoxelJobProcessor>();
		container.RegisterSingleton<Volumes>();
		container.RegisterSingleton<VoxelEngineOLD>();
	}

	public static void SetUpNoHostApplication(this Container container)
	{
		Debug.Assert(container != null);

		container.RegisterSingleton<EngineDiagnostics>();
		container.RegisterSingleton<IResyncHostApplicationQueue, HostlessResyncQueue>();
		container.RegisterSingleton<IVolumeServices, VolumeServices>();
	}
}
