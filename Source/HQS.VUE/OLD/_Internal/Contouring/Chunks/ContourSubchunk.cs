﻿namespace HQS.VUE.OLD;

internal readonly struct ContourSubchunk : IEquatable<ContourSubchunk>
{
	public ContourSubchunk(List<MeshQuadSlim> mesh, List<BoxColliderStructSlim> colliders)
	{
		Debug.Assert(mesh != null);
		Debug.Assert(colliders != null);

		this.Mesh = mesh;
		this.Colliders = colliders;
	}

	public static ContourSubchunk Empty => default;

	public bool IsEmpty => this.Mesh == null;

	public List<MeshQuadSlim> Mesh { get; }

	public List<BoxColliderStructSlim> Colliders { get; }

	public ReadOnlyContourSubchunk AsReadOnly => new ReadOnlyContourSubchunk(this);

	public bool CheckIsEmpty()
	{
		Debug.Assert(!this.IsEmpty);

		return this.Mesh.Count == 0 && this.Colliders.Count == 0;
	}

	public void Clear()
	{
		Debug.Assert(!this.IsEmpty);

		this.Mesh.Clear();
		this.Colliders.Clear();
	}

	public static bool operator ==(ContourSubchunk lhs, ContourSubchunk rhs) => lhs.Equals(rhs);

	public static bool operator !=(ContourSubchunk lhs, ContourSubchunk rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ContourSubchunk other) => this.Mesh == other.Mesh;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Mesh, this.Colliders);

	/// <inheritdoc />
	public override string ToString() =>
		$"({nameof(this.Mesh)}: {this.Mesh?.Count ?? 0}, {nameof(this.Colliders)}: {this.Colliders?.Count ?? 0})";
}
