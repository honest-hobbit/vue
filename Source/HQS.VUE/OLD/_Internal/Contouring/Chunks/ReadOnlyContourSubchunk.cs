﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyContourSubchunk
{
	public ReadOnlyContourSubchunk(ContourSubchunk subchunk)
	{
		this.Mesh = subchunk.Mesh;
		this.Colliders = subchunk.Colliders;
	}

	public static ReadOnlyContourSubchunk Empty => default;

	public bool IsEmpty => this.Mesh == null;

	public IReadOnlyList<MeshQuadSlim> Mesh { get; }

	public IReadOnlyList<BoxColliderStructSlim> Colliders { get; }
}
