﻿namespace HQS.VUE.OLD;

internal class ContourChunk : IChunk<ReadOnlyContourChunk>, IReadOnlyList<ReadOnlyContourSubchunk>
{
	/// <inheritdoc />
	public ChunkKey Key { get; set; }

	public ContourSubchunk[] Subchunks { get; set; }

	/// <inheritdoc />
	public ReadOnlyContourChunk AsReadOnly => new ReadOnlyContourChunk(this);

	/// <inheritdoc />
	public ChunkLoadingSignal Loading { get; } = new ChunkLoadingSignal();

	/// <inheritdoc />
	public Guid RecordKey { get; set; }

	/// <inheritdoc />
	public bool HasData { get; set; }

	/// <inheritdoc />
	public int Count => this.Subchunks?.Length ?? 0;

	/// <inheritdoc />
	public ReadOnlyContourSubchunk this[int index]
	{
		get
		{
			IReadOnlyListContracts.Indexer(this, index);
			return this.Subchunks[index].AsReadOnly;
		}
	}

	/// <inheritdoc />
	IEnumerator<ReadOnlyContourSubchunk> IEnumerable<ReadOnlyContourSubchunk>.GetEnumerator()
	{
		var subchunks = this.Subchunks;
		if (subchunks == null)
		{
			yield break;
		}

		for (int index = 0; index < subchunks.Length; index++)
		{
			yield return subchunks[index].AsReadOnly;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => throw new NotSupportedException();

	/// <inheritdoc />
	public void SetToEmptyChunk()
	{
		Debug.Assert(this.Subchunks == null);

		////this.Key = default;
		this.RecordKey = Guid.Empty;
		this.HasData = false;
	}
}
