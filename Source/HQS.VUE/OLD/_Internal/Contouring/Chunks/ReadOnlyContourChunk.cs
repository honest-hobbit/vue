﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyContourChunk : IKeyed<ChunkKey>
{
	private readonly ContourChunk chunk;

	public ReadOnlyContourChunk(ContourChunk chunk)
	{
		Debug.Assert(chunk != null);

		this.chunk = chunk;
	}

	public static ReadOnlyContourChunk Null => default;

	public bool IsNull => this.chunk == null;

	/// <inheritdoc />
	public ChunkKey Key => this.chunk.Key;

	public Guid EntityKey => this.chunk.RecordKey;

	public bool IsEmpty => this.chunk.Subchunks == null;

	public IReadOnlyList<ReadOnlyContourSubchunk> Subchunks => this.chunk;
}
