﻿namespace HQS.VUE.OLD;

internal abstract class AbstractMeshGenerator
{
	private readonly Func<Voxel, Surface> getSurface;

	private readonly CubeArrayIndexer indexer;

	private readonly Surface[] surfaces;

	public AbstractMeshGenerator(VoxelSizeConfig config, Func<Voxel, Surface> getSurface)
	{
		Debug.Assert(config != null);
		Debug.Assert(getSurface != null);

		this.Config = config;
		this.getSurface = getSurface;
		this.indexer = config.VoxelIndexer;
		this.surfaces = new Surface[config.VoxelIndexer.Length];
	}

	public VoxelSizeConfig Config { get; }

	protected bool CanSkipCenter { get; private set; } = false;

	protected Surface? CenterUniformSurface { get; private set; } = null;

	public abstract void GenerateAll(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result);

	public abstract void GenerateFaces(AdjacencyCross<ReadOnlyVoxelSubchunk> source, CubeParts parts, List<MeshQuadSlim> result);

	[Conditional(CompilationSymbol.Debug)]
	protected void GenerateAllContracts(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		Debug.Assert(source != null);
		Debug.Assert(result != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	protected void GenerateFacesContracts(AdjacencyCross<ReadOnlyVoxelSubchunk> source, CubeParts parts, List<MeshQuadSlim> result)
	{
		Debug.Assert(source != null);
		Debug.Assert(parts != CubeParts.None);
		Debug.Assert(!parts.Has(CubeParts.Center));
		Debug.Assert(result != null);
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	protected Surface GetCenterSurface(int x, int y, int z) => this.surfaces[this.indexer[x, y, z]];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	protected Surface GetVoxelSurface(Voxel voxel) => this.getSurface(voxel);

	protected void AssignSurfaceData(ReadOnlyVoxelSubchunk subchunk)
	{
		if (subchunk.IsUniform)
		{
			this.CanSkipCenter = true;
			this.CenterUniformSurface = this.GetVoxelSurface(subchunk.UniformVoxel);
			return;
		}

		bool isUniform = true;
		bool canSkipCenter = true;
		var voxels = subchunk.Voxels;

		var firstSurface = this.GetVoxelSurface(voxels[0]);
		this.surfaces[0] = firstSurface;

		int max = this.surfaces.Length;
		for (int i = 1; i < max; i++)
		{
			var nextSurface = this.GetVoxelSurface(voxels[i]);
			this.surfaces[i] = nextSurface;
			isUniform &= nextSurface.PolyType == firstSurface.PolyType;
			canSkipCenter &= nextSurface.SurfaceType == firstSurface.SurfaceType;
		}

		this.CanSkipCenter = canSkipCenter;
		if (isUniform)
		{
			this.CenterUniformSurface = firstSurface;
		}
		else
		{
			this.CenterUniformSurface = null;
		}
	}
}
