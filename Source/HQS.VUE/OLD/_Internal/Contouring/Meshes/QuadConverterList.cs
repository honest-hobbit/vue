﻿namespace HQS.VUE.OLD;

internal class QuadConverterList : IReadOnlyList<MeshQuad>
{
	private readonly Func<PolyTypeIndex, ColorRGB> getColor;

	public QuadConverterList(Func<PolyTypeIndex, ColorRGB> getColor)
	{
		Debug.Assert(getColor != null);

		this.getColor = getColor;
	}

	public IReadOnlyList<MeshQuadSlim> Source { get; set; }

	/// <inheritdoc />
	public int Count
	{
		get
		{
			Debug.Assert(this.Source != null);

			return this.Source.Count;
		}
	}

	/// <inheritdoc />
	public MeshQuad this[int index]
	{
		get
		{
			Debug.Assert(this.Source != null);
			IReadOnlyListContracts.Indexer(this, index);

			return this.Source[index].Expand(this.getColor);
		}
	}

	/// <inheritdoc />
	public IEnumerator<MeshQuad> GetEnumerator()
	{
		Debug.Assert(this.Source != null);

		int max = this.Source.Count;
		for (int i = 0; i < max; i++)
		{
			yield return this.Source[i].Expand(this.getColor);
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
