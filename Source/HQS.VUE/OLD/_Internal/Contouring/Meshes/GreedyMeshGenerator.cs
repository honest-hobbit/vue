﻿namespace HQS.VUE.OLD;

internal class GreedyMeshGenerator : AbstractMeshGenerator
{
	private readonly FaceStack faceStorage;

	private readonly int sideMax;

	private readonly int quadMax;

	private readonly SurfaceFunctions surfaceFunctions;

	private readonly SquareArrayIndexer sliceIndexer;

	private readonly PolyTypeIndex[] frontSlice;

	private readonly PolyTypeIndex[] backSlice;

	public GreedyMeshGenerator(VoxelSizeConfig config, Func<Voxel, Surface> getSurface, int faceQuadsCapacity)
		: base(config, getSurface)
	{
		this.quadMax = config.VoxelIndexer.SideLength;
		this.sideMax = this.quadMax - 1;

		this.sliceIndexer = new SquareArrayIndexer(config.VoxelIndexer.Exponent);
		int length = this.sliceIndexer.Length;
		this.frontSlice = new PolyTypeIndex[length];
		this.backSlice = new PolyTypeIndex[length];

		this.surfaceFunctions = new SurfaceFunctions(this);
		this.faceStorage = new FaceStack(config, faceQuadsCapacity);
	}

	/// <inheritdoc />
	public override void GenerateAll(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		this.GenerateAllContracts(source, result);

		result.Clear();
		this.AssignSurfaceData(source.Center);
		if (!this.CanSkipCenter)
		{
			this.GenerateCenterSlicesXAxis(result);
			this.GenerateCenterSlicesYAxis(result);
			this.GenerateCenterSlicesZAxis(result);
		}

		// it is requirred to generate quads in the order of;
		// center, negX, posX, negY, posY, negZ, posZ
		this.GenerateFaceNegativeX(source, result);
		this.GenerateFacePositiveX(source, result);
		this.GenerateFaceNegativeY(source, result);
		this.GenerateFacePositiveY(source, result);
		this.GenerateFaceNegativeZ(source, result);
		this.GenerateFacePositiveZ(source, result);

		this.surfaceFunctions.Clear();
	}

	/// <inheritdoc />
	public override void GenerateFaces(AdjacencyCross<ReadOnlyVoxelSubchunk> source, CubeParts parts, List<MeshQuadSlim> result)
	{
		this.GenerateFacesContracts(source, parts, result);

		this.AssignSurfaceData(source.Center);
		this.faceStorage.StoreFaces(parts, result);

		// it is requirred to generate quads in the order of;
		// center, negX, posX, negY, posY, negZ, posZ
		if (parts.Has(CubeParts.NegX))
		{
			this.GenerateFaceNegativeX(source, result);
		}

		if (parts.Has(CubeParts.PosX))
		{
			this.GenerateFacePositiveX(source, result);
		}
		else
		{
			this.faceStorage.RestoreFace(Direction.PositiveX, result);
		}

		if (parts.Has(CubeParts.NegY))
		{
			this.GenerateFaceNegativeY(source, result);
		}
		else
		{
			this.faceStorage.RestoreFace(Direction.NegativeY, result);
		}

		if (parts.Has(CubeParts.PosY))
		{
			this.GenerateFacePositiveY(source, result);
		}
		else
		{
			this.faceStorage.RestoreFace(Direction.PositiveY, result);
		}

		if (parts.Has(CubeParts.NegZ))
		{
			this.GenerateFaceNegativeZ(source, result);
		}
		else
		{
			this.faceStorage.RestoreFace(Direction.NegativeZ, result);
		}

		if (parts.Has(CubeParts.PosZ))
		{
			this.GenerateFacePositiveZ(source, result);
		}
		else
		{
			this.faceStorage.RestoreFace(Direction.PositiveZ, result);
		}

		this.surfaceFunctions.Clear();
	}

	#region GenerateFace methods

	private void GenerateFaceNegativeX(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		this.surfaceFunctions.InitializeToNegativeX();
		this.GenerateFace(source.NegX, 0, Direction.NegativeX, result);
	}

	private void GenerateFacePositiveX(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		this.surfaceFunctions.InitializeToPositiveX();
		this.GenerateFace(source.PosX, this.quadMax, Direction.PositiveX, result);
	}

	private void GenerateFaceNegativeY(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		this.surfaceFunctions.InitializeToNegativeY();
		this.GenerateFace(source.NegY, 0, Direction.NegativeY, result);
	}

	private void GenerateFacePositiveY(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		this.surfaceFunctions.InitializeToPositiveY();
		this.GenerateFace(source.PosY, this.quadMax, Direction.PositiveY, result);
	}

	private void GenerateFaceNegativeZ(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		this.surfaceFunctions.InitializeToNegativeZ();
		this.GenerateFace(source.NegZ, 0, Direction.NegativeZ, result);
	}

	private void GenerateFacePositiveZ(AdjacencyCross<ReadOnlyVoxelSubchunk> source, List<MeshQuadSlim> result)
	{
		this.surfaceFunctions.InitializeToPositiveZ();
		this.GenerateFace(source.PosZ, this.quadMax, Direction.PositiveZ, result);
	}

	private void GenerateFace(ReadOnlyVoxelSubchunk adjacentSubchunk, int c, Direction facing, List<MeshQuadSlim> result)
	{
		if (adjacentSubchunk.IsUniform && this.CenterUniformSurface.HasValue)
		{
			this.GenerateFace(this.GetVoxelSurface(adjacentSubchunk.UniformVoxel), this.CenterUniformSurface.Value, c, facing, result);
		}
		else
		{
			this.surfaceFunctions.FinalizeWith(adjacentSubchunk);
			this.GenerateFace(this.surfaceFunctions.GetAdjacent, this.surfaceFunctions.GetCenter, c, facing, result);
		}
	}

	private void GenerateFace(Surface adjacent, Surface center, int c, Direction facing, List<MeshQuadSlim> result)
	{
		adjacent.GetPolyTypeIndices(center, out _, out var centerPoly);
		if (centerPoly != PolyTypeIndex.Empty)
		{
			result.Add(MeshQuadSlim.From(0, this.quadMax, 0, this.quadMax, c, facing, centerPoly));
		}
	}

	private void GenerateFace(Func<int, int, Surface> getAdjacent, Func<int, int, Surface> getCenter, int c, Direction facing, List<MeshQuadSlim> result)
	{
		bool isEmpty = true;
		for (int iA = 0; iA <= this.sideMax; iA++)
		{
			for (int iB = 0; iB <= this.sideMax; iB++)
			{
				var adjacent = getAdjacent(iA, iB);
				var center = getCenter(iA, iB);
				adjacent.GetPolyTypeIndices(center, out _, out var centerPoly);
				this.backSlice[this.sliceIndexer[iA, iB]] = centerPoly;
				isEmpty &= centerPoly == PolyTypeIndex.Empty;
			}
		}

		if (!isEmpty)
		{
			this.GenerateSlice(this.backSlice, c, facing, result);
		}
	}

	#endregion

	#region GenerateCenter methods

	private void GenerateCenterSlicesXAxis(List<MeshQuadSlim> result)
	{
		for (int iX = 0; iX < this.sideMax; iX++)
		{
			bool frontIsEmpty = true;
			bool backIsEmpty = true;
			int nextX = iX + 1;

			for (int iY = 0; iY <= this.sideMax; iY++)
			{
				for (int iZ = 0; iZ <= this.sideMax; iZ++)
				{
					var frontSurface = this.GetCenterSurface(iX, iY, iZ);
					var backSurface = this.GetCenterSurface(nextX, iY, iZ);
					frontSurface.GetPolyTypeIndices(backSurface, out var frontPoly, out var backPoly);
					int sliceIndex = this.sliceIndexer[iY, iZ];
					this.frontSlice[sliceIndex] = frontPoly;
					this.backSlice[sliceIndex] = backPoly;
					frontIsEmpty &= frontPoly == PolyTypeIndex.Empty;
					backIsEmpty &= backPoly == PolyTypeIndex.Empty;
				}
			}

			if (!frontIsEmpty)
			{
				this.GenerateSlice(this.frontSlice, nextX, Direction.PositiveX, result);
			}

			if (!backIsEmpty)
			{
				this.GenerateSlice(this.backSlice, nextX, Direction.NegativeX, result);
			}
		}
	}

	private void GenerateCenterSlicesYAxis(List<MeshQuadSlim> result)
	{
		for (int iY = 0; iY < this.sideMax; iY++)
		{
			bool frontIsEmpty = true;
			bool backIsEmpty = true;
			int nextY = iY + 1;

			for (int iX = 0; iX <= this.sideMax; iX++)
			{
				for (int iZ = 0; iZ <= this.sideMax; iZ++)
				{
					var frontSurface = this.GetCenterSurface(iX, iY, iZ);
					var backSurface = this.GetCenterSurface(iX, nextY, iZ);
					frontSurface.GetPolyTypeIndices(backSurface, out var frontPoly, out var backPoly);
					int sliceIndex = this.sliceIndexer[iX, iZ];
					this.frontSlice[sliceIndex] = frontPoly;
					this.backSlice[sliceIndex] = backPoly;
					frontIsEmpty &= frontPoly == PolyTypeIndex.Empty;
					backIsEmpty &= backPoly == PolyTypeIndex.Empty;
				}
			}

			if (!frontIsEmpty)
			{
				this.GenerateSlice(this.frontSlice, nextY, Direction.PositiveY, result);
			}

			if (!backIsEmpty)
			{
				this.GenerateSlice(this.backSlice, nextY, Direction.NegativeY, result);
			}
		}
	}

	private void GenerateCenterSlicesZAxis(List<MeshQuadSlim> result)
	{
		for (int iZ = 0; iZ < this.sideMax; iZ++)
		{
			bool frontIsEmpty = true;
			bool backIsEmpty = true;
			int nextZ = iZ + 1;

			for (int iX = 0; iX <= this.sideMax; iX++)
			{
				for (int iY = 0; iY <= this.sideMax; iY++)
				{
					var frontSurface = this.GetCenterSurface(iX, iY, iZ);
					var backSurface = this.GetCenterSurface(iX, iY, nextZ);
					frontSurface.GetPolyTypeIndices(backSurface, out var frontPoly, out var backPoly);
					int sliceIndex = this.sliceIndexer[iX, iY];
					this.frontSlice[sliceIndex] = frontPoly;
					this.backSlice[sliceIndex] = backPoly;
					frontIsEmpty &= frontPoly == PolyTypeIndex.Empty;
					backIsEmpty &= backPoly == PolyTypeIndex.Empty;
				}
			}

			if (!frontIsEmpty)
			{
				this.GenerateSlice(this.frontSlice, nextZ, Direction.PositiveZ, result);
			}

			if (!backIsEmpty)
			{
				this.GenerateSlice(this.backSlice, nextZ, Direction.NegativeZ, result);
			}
		}
	}

	#endregion

	#region Shared generate methods

	private void GenerateSlice(PolyTypeIndex[] slice, int c, Direction facing, List<MeshQuadSlim> result)
	{
		for (int iA = 0; iA <= this.sideMax; iA++)
		{
			for (int iB = 0; iB <= this.sideMax;)
			{
				var type = slice[this.sliceIndexer[iA, iB]];
				if (type != PolyTypeIndex.Empty)
				{
					this.CreateQuad(slice, type, iA, iB, out int aMax, out int bMax);
					result.Add(MeshQuadSlim.From(iA, aMax, iB, bMax, c, facing, type));
					iB = bMax;
				}
				else
				{
					iB++;
				}
			}
		}
	}

	private void CreateQuad(PolyTypeIndex[] slice, PolyTypeIndex type, int aMin, int bMin, out int aMax, out int bMax)
	{
		this.ExpandQuad(slice, type, aMin, bMin, out aMax, out bMax);

		// clear out the area covered by this quad
		for (int iA = aMin; iA < aMax; iA++)
		{
			for (int iB = bMin; iB < bMax; iB++)
			{
				slice[this.sliceIndexer[iA, iB]] = PolyTypeIndex.Empty;
			}
		}
	}

	private void ExpandQuad(PolyTypeIndex[] slice, PolyTypeIndex type, int aMin, int bMin, out int aMax, out int bMax)
	{
		int aEnd = aMin;
		int bEnd = bMin;

		bool tryPushA = true;
		bool tryPushB = true;

		while (tryPushA || tryPushB)
		{
			if (tryPushA)
			{
				tryPushA = TryPushA();
			}

			if (tryPushB)
			{
				tryPushB = TryPushB();
			}
		}

		aMax = aEnd + 1;
		bMax = bEnd + 1;

		bool TryPushA()
		{
			if (aEnd == this.sideMax)
			{
				return false;
			}

			int pushA = aEnd + 1;
			for (int iB = bMin; iB <= bEnd; iB++)
			{
				if (slice[this.sliceIndexer[pushA, iB]] != type)
				{
					return false;
				}
			}

			aEnd = pushA;
			return true;
		}

		bool TryPushB()
		{
			if (bEnd == this.sideMax)
			{
				return false;
			}

			int pushB = bEnd + 1;
			for (int iA = aMin; iA <= aEnd; iA++)
			{
				if (slice[this.sliceIndexer[iA, pushB]] != type)
				{
					return false;
				}
			}

			bEnd = pushB;
			return true;
		}
	}

	#endregion

	private class SurfaceFunctions
	{
		private readonly GreedyMeshGenerator parent;

		private readonly Func<int, int, Surface> getAdjacentXAxis;

		private readonly Func<int, int, Surface> getAdjacentYAxis;

		private readonly Func<int, int, Surface> getAdjacentZAxis;

		private readonly Func<int, int, Surface> getAdjacentUniform;

		private readonly Func<int, int, Surface> getCenterXAxis;

		private readonly Func<int, int, Surface> getCenterYAxis;

		private readonly Func<int, int, Surface> getCenterZAxis;

		private readonly Func<int, int, Surface> getCenterUniform;

		private readonly CubeArrayIndexer voxelIndexer;

		private int adjacentC;

		private int centerC;

		private ReadOnlyVoxelSubchunk adjacentSubchunk;

		private Surface adjacentUniformSurface;

		private Surface centerUniformSurface;

		public SurfaceFunctions(GreedyMeshGenerator parent)
		{
			Debug.Assert(parent != null);

			this.parent = parent;
			this.voxelIndexer = parent.Config.VoxelIndexer;
			this.getAdjacentXAxis = this.GetAdjacentXAxis;
			this.getAdjacentYAxis = this.GetAdjacentYAxis;
			this.getAdjacentZAxis = this.GetAdjacentZAxis;
			this.getAdjacentUniform = this.GetAdjacentUniform;
			this.getCenterXAxis = this.GetCenterXAxis;
			this.getCenterYAxis = this.GetCenterYAxis;
			this.getCenterZAxis = this.GetCenterZAxis;
			this.getCenterUniform = this.GetCenterUniform;
			this.Clear();
		}

		public Func<int, int, Surface> GetAdjacent { get; private set; }

		public Func<int, int, Surface> GetCenter { get; private set; }

		public void InitializeToNegativeX()
		{
			this.GetAdjacent = this.getAdjacentXAxis;
			this.GetCenter = this.getCenterXAxis;
			this.adjacentC = this.parent.sideMax;
			this.centerC = 0;
		}

		public void InitializeToPositiveX()
		{
			this.GetAdjacent = this.getAdjacentXAxis;
			this.GetCenter = this.getCenterXAxis;
			this.adjacentC = 0;
			this.centerC = this.parent.sideMax;
		}

		public void InitializeToNegativeY()
		{
			this.GetAdjacent = this.getAdjacentYAxis;
			this.GetCenter = this.getCenterYAxis;
			this.adjacentC = this.parent.sideMax;
			this.centerC = 0;
		}

		public void InitializeToPositiveY()
		{
			this.GetAdjacent = this.getAdjacentYAxis;
			this.GetCenter = this.getCenterYAxis;
			this.adjacentC = 0;
			this.centerC = this.parent.sideMax;
		}

		public void InitializeToNegativeZ()
		{
			this.GetAdjacent = this.getAdjacentZAxis;
			this.GetCenter = this.getCenterZAxis;
			this.adjacentC = this.parent.sideMax;
			this.centerC = 0;
		}

		public void InitializeToPositiveZ()
		{
			this.GetAdjacent = this.getAdjacentZAxis;
			this.GetCenter = this.getCenterZAxis;
			this.adjacentC = 0;
			this.centerC = this.parent.sideMax;
		}

		public void FinalizeWith(ReadOnlyVoxelSubchunk adjacentSubchunk)
		{
			if (adjacentSubchunk.IsUniform)
			{
				this.adjacentUniformSurface = this.parent.GetVoxelSurface(adjacentSubchunk.UniformVoxel);
				this.GetAdjacent = this.getAdjacentUniform;
			}
			else
			{
				this.adjacentSubchunk = adjacentSubchunk;
			}

			if (this.parent.CenterUniformSurface.HasValue)
			{
				this.centerUniformSurface = this.parent.CenterUniformSurface.Value;
				this.GetCenter = this.getCenterUniform;
			}
		}

		public void Clear()
		{
			this.GetAdjacent = null;
			this.GetCenter = null;
			this.adjacentSubchunk = default;
			this.adjacentC = -1;
			this.centerC = -1;
			this.adjacentUniformSurface = Surface.None;
			this.centerUniformSurface = Surface.None;
		}

		private Surface GetAdjacentXAxis(int iA, int iB) =>
			this.parent.GetVoxelSurface(this.adjacentSubchunk.Voxels[this.voxelIndexer[this.adjacentC, iA, iB]]);

		private Surface GetAdjacentYAxis(int iA, int iB) =>
			this.parent.GetVoxelSurface(this.adjacentSubchunk.Voxels[this.voxelIndexer[iA, this.adjacentC, iB]]);

		private Surface GetAdjacentZAxis(int iA, int iB) =>
			this.parent.GetVoxelSurface(this.adjacentSubchunk.Voxels[this.voxelIndexer[iA, iB, this.adjacentC]]);

		private Surface GetAdjacentUniform(int iA, int iB) => this.adjacentUniformSurface;

		private Surface GetCenterXAxis(int iA, int iB) => this.parent.GetCenterSurface(this.centerC, iA, iB);

		private Surface GetCenterYAxis(int iA, int iB) => this.parent.GetCenterSurface(iA, this.centerC, iB);

		private Surface GetCenterZAxis(int iA, int iB) => this.parent.GetCenterSurface(iA, iB, this.centerC);

		private Surface GetCenterUniform(int iA, int iB) => this.centerUniformSurface;
	}
}
