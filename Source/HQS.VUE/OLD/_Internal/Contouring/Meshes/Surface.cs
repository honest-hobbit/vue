﻿namespace HQS.VUE.OLD;

internal readonly struct Surface
{
	private const int SurfaceVisibilities = 5;

	private static readonly PolySides[] SidesTable = CreateTable();

	public Surface(SurfaceTypeIndex surfaceType, SurfaceVisibility visibility, PolyTypeIndex polyType)
	{
		Debug.Assert(
			(visibility != SurfaceVisibility.None && polyType != PolyTypeIndex.Empty) ||
			(visibility == SurfaceVisibility.None && polyType == PolyTypeIndex.Empty));

		this.SurfaceType = surfaceType;
		this.Visibility = visibility;
		this.PolyType = polyType;
	}

	public static Surface None => default;

	public readonly SurfaceTypeIndex SurfaceType;

	public readonly SurfaceVisibility Visibility;

	public readonly PolyTypeIndex PolyType;

	public void GetPolyTypeIndices(Surface backSurface, out PolyTypeIndex frontPoly, out PolyTypeIndex backPoly)
	{
		frontPoly = PolyTypeIndex.Empty;
		backPoly = PolyTypeIndex.Empty;
		if (this.SurfaceType == backSurface.SurfaceType)
		{
			return;
		}

		switch (SidesTable[GetIndex(this.Visibility, backSurface.Visibility)])
		{
			case PolySides.Front:
				frontPoly = this.PolyType;
				break;

			case PolySides.Back:
				backPoly = backSurface.PolyType;
				break;

			case PolySides.Both:
				frontPoly = this.PolyType;
				backPoly = backSurface.PolyType;
				break;
		}
	}

	private static PolySides[] CreateTable()
	{
		if (Enumeration.Values<SurfaceVisibility>().Count != SurfaceVisibilities)
		{
			throw new Exception($"{nameof(SurfaceVisibility)} was updated without also updating {nameof(Surface)}.");
		}

		// This implements the following logic table
		// 							     Back
		// 			  | None  | Trans | Opaque | DomTra | DomOpa
		// ------------------------------------------------------
		// F | None   | None  | Back  |  Back  |  Back  |  Back
		// r | Trans  | Front | Both  |  Back  |  Both  |  Back
		// o | Opaque | Front | Front |  None  |  Both  |  Back
		// n | DomTra | Front | Both  |  Both  |  Both  |  Both
		// t | DomOpa | Front | Front |  Front |  Both  |  Both
		var table = new PolySides[SurfaceVisibilities * SurfaceVisibilities];

		// front is None
		table[GetIndex(SurfaceVisibility.None, SurfaceVisibility.None)] = PolySides.None;
		table[GetIndex(SurfaceVisibility.None, SurfaceVisibility.Transparent)] = PolySides.Back;
		table[GetIndex(SurfaceVisibility.None, SurfaceVisibility.Opaque)] = PolySides.Back;
		table[GetIndex(SurfaceVisibility.None, SurfaceVisibility.DominantTransparent)] = PolySides.Back;
		table[GetIndex(SurfaceVisibility.None, SurfaceVisibility.DominantOpaque)] = PolySides.Back;

		// front is Transparent
		table[GetIndex(SurfaceVisibility.Transparent, SurfaceVisibility.None)] = PolySides.Front;
		table[GetIndex(SurfaceVisibility.Transparent, SurfaceVisibility.Transparent)] = PolySides.Both;
		table[GetIndex(SurfaceVisibility.Transparent, SurfaceVisibility.Opaque)] = PolySides.Back;
		table[GetIndex(SurfaceVisibility.Transparent, SurfaceVisibility.DominantTransparent)] = PolySides.Both;
		table[GetIndex(SurfaceVisibility.Transparent, SurfaceVisibility.DominantOpaque)] = PolySides.Back;

		// front is Opaque
		table[GetIndex(SurfaceVisibility.Opaque, SurfaceVisibility.None)] = PolySides.Front;
		table[GetIndex(SurfaceVisibility.Opaque, SurfaceVisibility.Transparent)] = PolySides.Front;
		table[GetIndex(SurfaceVisibility.Opaque, SurfaceVisibility.Opaque)] = PolySides.None;
		table[GetIndex(SurfaceVisibility.Opaque, SurfaceVisibility.DominantTransparent)] = PolySides.Both;
		table[GetIndex(SurfaceVisibility.Opaque, SurfaceVisibility.DominantOpaque)] = PolySides.Back;

		// front is DominantTransparent
		table[GetIndex(SurfaceVisibility.DominantTransparent, SurfaceVisibility.None)] = PolySides.Front;
		table[GetIndex(SurfaceVisibility.DominantTransparent, SurfaceVisibility.Transparent)] = PolySides.Both;
		table[GetIndex(SurfaceVisibility.DominantTransparent, SurfaceVisibility.Opaque)] = PolySides.Both;
		table[GetIndex(SurfaceVisibility.DominantTransparent, SurfaceVisibility.DominantTransparent)] = PolySides.Both;
		table[GetIndex(SurfaceVisibility.DominantTransparent, SurfaceVisibility.DominantOpaque)] = PolySides.Both;

		// front is DominantOpaque
		table[GetIndex(SurfaceVisibility.DominantOpaque, SurfaceVisibility.None)] = PolySides.Front;
		table[GetIndex(SurfaceVisibility.DominantOpaque, SurfaceVisibility.Transparent)] = PolySides.Front;
		table[GetIndex(SurfaceVisibility.DominantOpaque, SurfaceVisibility.Opaque)] = PolySides.Front;
		table[GetIndex(SurfaceVisibility.DominantOpaque, SurfaceVisibility.DominantTransparent)] = PolySides.Both;
		table[GetIndex(SurfaceVisibility.DominantOpaque, SurfaceVisibility.DominantOpaque)] = PolySides.Both;

		return table;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private static int GetIndex(SurfaceVisibility front, SurfaceVisibility back) => ((int)front * SurfaceVisibilities) + (int)back;

	private enum PolySides
	{
		None,
		Front,
		Back,
		Both,
	}
}
