﻿namespace HQS.VUE.OLD;

internal class FaceStack
{
	private readonly int maxC;

	private readonly Stack<MeshQuadSlim> storedFaces;

	public FaceStack(VoxelSizeConfig config, int faceQuadsCapacity)
	{
		Debug.Assert(config != null);
		Debug.Assert(faceQuadsCapacity >= 0);

		this.maxC = config.VoxelIndexer.SideLength;
		this.storedFaces = new Stack<MeshQuadSlim>(faceQuadsCapacity);
	}

	public void StoreFaces(CubeParts generatingParts, List<MeshQuadSlim> source)
	{
		Debug.Assert(this.storedFaces.Count == 0);
		Debug.Assert(generatingParts != CubeParts.None);
		Debug.Assert(!generatingParts.Has(CubeParts.Center));
		Debug.Assert(source != null);

		int sourceCount = source.Count;
		if (sourceCount == 0)
		{
			return;
		}

		DetermineFaces(generatingParts, out var facesToStore, out var facesToRemove);
		int index = sourceCount - 1;

		// it is requirred that quads are generated in the order of;
		// center, negX, posX, negY, posY, negZ, posZ
		// this method handles them in reverse order so as to avoid iterating over the center quads
		// as they are never stored and usually greatly out number the face quads
		HandleFace(CubeParts.PosZ, Direction.PositiveZ, this.maxC);
		HandleFace(CubeParts.NegZ, Direction.NegativeZ, 0);
		HandleFace(CubeParts.PosY, Direction.PositiveY, this.maxC);
		HandleFace(CubeParts.NegY, Direction.NegativeY, 0);
		HandleFace(CubeParts.PosX, Direction.PositiveX, this.maxC);
		HandleFace(CubeParts.NegX, Direction.NegativeX, 0);

		index++;
		if (index < sourceCount)
		{
			source.RemoveRange(index, sourceCount - index);
		}

		void HandleFace(CubeParts face, Direction faceNormal, int c)
		{
			bool TryNextQuad(out MeshQuadSlim quad)
			{
				quad = source[index];
				return quad.C == c && quad.Normal == faceNormal;
			}

			if (facesToStore.Has(face))
			{
				while (index >= 0 && TryNextQuad(out var quad))
				{
					this.storedFaces.Push(quad);
					index--;
				}
			}
			else if (facesToRemove.Has(face))
			{
				while (index >= 0 && TryNextQuad(out var _))
				{
					index--;
				}
			}
		}
	}

	public void RestoreFace(Direction faceNormal, List<MeshQuadSlim> result)
	{
		Debug.Assert(Enumeration.IsDefined(faceNormal));
		Debug.Assert(result != null);

		while (this.storedFaces.Count > 0)
		{
			var quad = this.storedFaces.Pop();
			if (quad.Normal == faceNormal)
			{
				result.Add(quad);
			}
			else
			{
				this.storedFaces.Push(quad);
				return;
			}
		}
	}

	private static void DetermineFaces(CubeParts generatingParts, out CubeParts facesToStore, out CubeParts facesToRemove)
	{
		Debug.Assert(generatingParts != CubeParts.None);

		if (generatingParts.Has(CubeParts.NegX))
		{
			facesToStore = CubeParts.PosX | CubeParts.NegY | CubeParts.PosY | CubeParts.NegZ | CubeParts.PosZ;
			facesToRemove = facesToStore | CubeParts.NegX;
		}
		else if (generatingParts.Has(CubeParts.PosX))
		{
			facesToStore = CubeParts.NegY | CubeParts.PosY | CubeParts.NegZ | CubeParts.PosZ;
			facesToRemove = facesToStore | CubeParts.PosX;
		}
		else if (generatingParts.Has(CubeParts.NegY))
		{
			facesToStore = CubeParts.PosY | CubeParts.NegZ | CubeParts.PosZ;
			facesToRemove = facesToStore | CubeParts.NegY;
		}
		else if (generatingParts.Has(CubeParts.PosY))
		{
			facesToStore = CubeParts.NegZ | CubeParts.PosZ;
			facesToRemove = facesToStore | CubeParts.PosY;
		}
		else if (generatingParts.Has(CubeParts.NegZ))
		{
			facesToStore = CubeParts.PosZ;
			facesToRemove = facesToStore | CubeParts.NegZ;
		}
		else
		{
			facesToStore = CubeParts.None;
			facesToRemove = CubeParts.PosZ;
		}

		facesToStore = facesToStore.Remove(generatingParts);
	}
}
