﻿namespace HQS.VUE.OLD;

[StructLayout(LayoutKind.Explicit)]
internal readonly struct MeshQuadSlim
{
	public MeshQuadSlim(byte aMin, byte aMax, byte bMin, byte bMax, byte c, Direction normal, PolyTypeIndex type)
	{
		Debug.Assert(aMin < aMax);
		Debug.Assert(bMin < bMax);
		Debug.Assert(Enumeration.IsDefined(normal));

		this.RawValue = 0;
		this.AMin = aMin;
		this.AMax = aMax;
		this.BMin = bMin;
		this.BMax = bMax;
		this.C = c;
		this.Normal = normal;
		this.Type = type;
	}

	public MeshQuadSlim(ulong rawValue)
	{
		this.AMin = 0;
		this.AMax = 0;
		this.BMin = 0;
		this.BMax = 0;
		this.C = 0;
		this.Normal = default;
		this.Type = default;
		this.RawValue = rawValue;
	}

	[FieldOffset(0)]
	public readonly ulong RawValue;

	[FieldOffset(0)]
	public readonly byte AMin;

	[FieldOffset(1)]
	public readonly byte AMax;

	[FieldOffset(2)]
	public readonly byte BMin;

	[FieldOffset(3)]
	public readonly byte BMax;

	[FieldOffset(4)]
	public readonly byte C;

	[FieldOffset(5)]
	public readonly Direction Normal;

	[FieldOffset(6)]
	public readonly PolyTypeIndex Type;

	public static MeshQuadSlim FacingNegativeX(byte yMin, byte yMax, byte zMin, byte zMax, byte x, PolyTypeIndex type) =>
		new MeshQuadSlim(yMin, yMax, zMin, zMax, x, Direction.NegativeX, type);

	public static MeshQuadSlim FacingPositiveX(byte yMin, byte yMax, byte zMin, byte zMax, byte x, PolyTypeIndex type) =>
		new MeshQuadSlim(yMin, yMax, zMin, zMax, x, Direction.PositiveX, type);

	public static MeshQuadSlim FacingNegativeY(byte xMin, byte xMax, byte zMin, byte zMax, byte y, PolyTypeIndex type) =>
		new MeshQuadSlim(xMin, xMax, zMin, zMax, y, Direction.NegativeY, type);

	public static MeshQuadSlim FacingPositiveY(byte xMin, byte xMax, byte zMin, byte zMax, byte y, PolyTypeIndex type) =>
		new MeshQuadSlim(xMin, xMax, zMin, zMax, y, Direction.PositiveY, type);

	public static MeshQuadSlim FacingNegativeZ(byte xMin, byte xMax, byte yMin, byte yMax, byte z, PolyTypeIndex type) =>
		new MeshQuadSlim(xMin, xMax, yMin, yMax, z, Direction.NegativeZ, type);

	public static MeshQuadSlim FacingPositiveZ(byte xMin, byte xMax, byte yMin, byte yMax, byte z, PolyTypeIndex type) =>
		new MeshQuadSlim(xMin, xMax, yMin, yMax, z, Direction.PositiveZ, type);

	public static MeshQuadSlim From(int aMin, int aMax, int bMin, int bMax, int c, Direction normal, PolyTypeIndex type)
	{
		Debug.Assert(aMin.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(aMax.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(bMin.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(bMax.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(c.IsIn(byte.MinValue, byte.MaxValue));

		return new MeshQuadSlim((byte)aMin, (byte)aMax, (byte)bMin, (byte)bMax, (byte)c, normal, type);
	}

	public MeshQuad Expand(Func<byte, float> getLength, Func<PolyTypeIndex, ColorRGB> getColor)
	{
		Debug.Assert(getLength != null);
		Debug.Assert(getColor != null);

		return this.Expand(getLength, getColor(this.Type));
	}

	public MeshQuad Expand(Func<byte, float> getLength, ColorRGB color)
	{
		Debug.Assert(getLength != null);

		return this.Expand(
			getLength(this.AMin),
			getLength(this.AMax),
			getLength(this.BMin),
			getLength(this.BMax),
			getLength(this.C),
			color);
	}

	public MeshQuad Expand(Func<PolyTypeIndex, ColorRGB> getColor)
	{
		Debug.Assert(getColor != null);

		return this.Expand(getColor(this.Type));
	}

	public MeshQuad Expand(ColorRGB color) => this.Expand(this.AMin, this.AMax, this.BMin, this.BMax, this.C, color);

	private MeshQuad Expand(float aMin, float aMax, float bMin, float bMax, float constant, ColorRGB color)
	{
		// corners
		// z | 0   0   1   1  y
		//   |-----------------
		// 1 | 2 - 3   6 - 7		A - B
		//   | |   |   |   |		|   |
		// 0 | 0 - 1   4 - 5		C - D
		//---------------------
		// x   0   1   0   1
		Vector3 a, b, c, d, normal;
		switch (this.Normal)
		{
			case Direction.NegativeX:
				a = new Vector3(constant, aMax, bMax);  // corner 6
				b = new Vector3(constant, aMax, bMin);  // corner 4
				c = new Vector3(constant, aMin, bMax);  // corner 2
				d = new Vector3(constant, aMin, bMin);  // corner 0
				normal = new Vector3(-1, 0, 0);
				break;

			case Direction.PositiveX:
				a = new Vector3(constant, aMax, bMin);  // corner 5
				b = new Vector3(constant, aMax, bMax);  // corner 7
				c = new Vector3(constant, aMin, bMin);  // corner 1
				d = new Vector3(constant, aMin, bMax);  // corner 3
				normal = new Vector3(1, 0, 0);
				break;

			case Direction.NegativeY:
				a = new Vector3(aMin, constant, bMin);  // corner 0
				b = new Vector3(aMax, constant, bMin);  // corner 1
				c = new Vector3(aMin, constant, bMax);  // corner 2
				d = new Vector3(aMax, constant, bMax);  // corner 3
				normal = new Vector3(0, -1, 0);
				break;

			case Direction.PositiveY:
				a = new Vector3(aMin, constant, bMax);  // corner 6
				b = new Vector3(aMax, constant, bMax);  // corner 7
				c = new Vector3(aMin, constant, bMin);  // corner 4
				d = new Vector3(aMax, constant, bMin);  // corner 5
				normal = new Vector3(0, 1, 0);
				break;

			case Direction.NegativeZ:
				a = new Vector3(aMin, bMax, constant);  // corner 4
				b = new Vector3(aMax, bMax, constant);  // corner 5
				c = new Vector3(aMin, bMin, constant);  // corner 0
				d = new Vector3(aMax, bMin, constant);  // corner 1
				normal = new Vector3(0, 0, -1);
				break;

			case Direction.PositiveZ:
				a = new Vector3(aMax, bMax, constant);  // corner 7
				b = new Vector3(aMin, bMax, constant);  // corner 6
				c = new Vector3(aMax, bMin, constant);  // corner 3
				d = new Vector3(aMin, bMin, constant);  // corner 2
				normal = new Vector3(0, 0, 1);
				break;

			default: throw InvalidEnumArgument.CreateException(nameof(this.Normal), this.Normal);
		}

		return new MeshQuad(a, b, c, d, normal, color);
	}
}
