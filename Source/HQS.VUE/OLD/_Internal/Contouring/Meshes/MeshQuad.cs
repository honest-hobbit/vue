﻿namespace HQS.VUE.OLD;

internal readonly struct MeshQuad
{
	// A - B
	// |   |
	// C - D
	public MeshQuad(
		Vector3 a,
		Vector3 b,
		Vector3 c,
		Vector3 d,
		Vector3 normal,
		ColorRGB color)
	{
		this.A = a;
		this.B = b;
		this.C = c;
		this.D = d;
		this.Normal = normal;
		this.Color = color;
	}

	public Vector3 A { get; }

	public Vector3 B { get; }

	public Vector3 C { get; }

	public Vector3 D { get; }

	public Vector3 Normal { get; }

	public ColorRGB Color { get; }
}
