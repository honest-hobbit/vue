﻿namespace HQS.VUE.OLD;

internal class VolumeContourer
{
	private readonly ContouringQueueManager contourer;

	public VolumeContourer(ContouringQueueManager contourer)
	{
		Debug.Assert(contourer != null);

		this.contourer = contourer;
	}

	public void ContourVolume(VolumeView volume, VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(volume != null);
		Debug.Assert(volume.IsInitialized);
		Debug.Assert(volume.ChangedVoxelChunks.Count == 0);
		Debug.Assert(volume.ChangedVoxelChunkPins.Count == 0);
		Debug.Assert(volume.ChangedContourChunks.Count == 0);
		Debug.Assert(volume.ChangedContourChunkPins.Count == 0);
		Debug.Assert(diagnostics != null);

		foreach (var chunkKey in GetAllChunkKeys(volume))
		{
			this.contourer.GetAndAddContourData(chunkKey, volume, WhatToContour.OnlyMissing);
		}

		this.contourer.ContourChunks(diagnostics);
	}

	private static IEnumerable<ChunkKey> GetAllChunkKeys(VolumeView volume)
	{
		Debug.Assert(volume != null);
		Debug.Assert(volume.IsInitialized);

		var pins = volume.Volume.EngineData.VoxelChunkPins;
		if (pins.IsPinned)
		{
			var chunks = pins.Value;
			int max = chunks.Count;
			for (int i = 0; i < max; i++)
			{
				yield return chunks[i].Value.Key;
			}
		}
		else
		{
			var store = volume.RunJobInfo.VoxelGridStore;
			if (store != null)
			{
				foreach (var record in store.GetAllChunkIdsOfGrid(volume.Key))
				{
					yield return record.ChunkKey;
				}
			}
		}
	}
}
