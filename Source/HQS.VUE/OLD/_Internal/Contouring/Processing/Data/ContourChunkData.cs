﻿namespace HQS.VUE.OLD;

internal class ContourChunkData : AdjacencyCross<ReadOnlyVoxelChunk>, IContourChunkDataView
{
	private Pin<ContourChunk> contourPin;

	public ContourChunkData(VoxelSizeConfig sizes)
	{
		Debug.Assert(sizes != null);

		this.SubchunksParts = new CubeParts[sizes.SubchunkIndexer.Length];
	}

	/// <inheritdoc />
	public CubeParts[] SubchunksParts { get; }

	/// <inheritdoc />
	public WhatToContour WhatToContour { get; set; } = WhatToContour.Nothing;

	/// <inheritdoc />
	public ContourSubchunk[] Subchunks { get; set; }

	/// <inheritdoc />
	public Pinned<ContourChunk> ContourPin => this.contourPin.AsPinned;

	/// <inheritdoc />
	public VolumeView VolumeView { get; set; }

	/// <inheritdoc />
	public VoxelChunkChangeset VoxelChangeset { get; set; }

	public void SetContourPin(Pin<ContourChunk> pin)
	{
		Debug.Assert(this.contourPin.IsUnassigned);
		Debug.Assert(pin.IsPinned);

		this.contourPin = pin;
	}

	/// <inheritdoc />
	public override void Clear()
	{
		base.Clear();
		this.SubchunksParts.SetAllTo(CubeParts.None);
		this.WhatToContour = WhatToContour.Nothing;
		this.Subchunks = null;
		this.contourPin.Dispose();
		this.contourPin = default;
		this.VolumeView = null;
		this.VoxelChangeset = VoxelChunkChangeset.Null;
	}
}
