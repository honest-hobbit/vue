﻿namespace HQS.VUE.OLD;

internal enum WhatToContour
{
	Nothing,
	OnlyChanges,
	OnlyMissing,
	Everything,
}
