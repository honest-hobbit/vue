﻿namespace HQS.VUE.OLD;

internal class ContourSubchunkData : AdjacencyCross<ReadOnlyVoxelSubchunk>
{
	public CubeParts Parts { get; set; }

	public ContourSubchunk[] Subchunks { get; set; }

	public int SubchunkIndex { get; set; }

	public override void Clear()
	{
		base.Clear();
		this.Parts = CubeParts.None;
		this.Subchunks = null;
		this.SubchunkIndex = 0;
	}
}
