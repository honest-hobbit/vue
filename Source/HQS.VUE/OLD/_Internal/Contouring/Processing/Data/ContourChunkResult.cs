﻿namespace HQS.VUE.OLD;

internal readonly struct ContourChunkResult
{
	public ContourChunkResult(ContourChunkData data)
	{
		Debug.Assert(data != null);
		Debug.Assert(data.ContourPin.IsPinned);

		this.ContourPin = data.ContourPin.CreatePin();
		this.WhatWasContoured = data.WhatToContour;
		this.Subchunks = data.Subchunks;
		this.VoxelChangeset = data.VoxelChangeset;
	}

	public bool IsNull => this.ContourPin == null;

	// this will be pinned
	public Pin<ContourChunk> ContourPin { get; }

	public WhatToContour WhatWasContoured { get; }

	// this can be null if the new contour chunk is empty
	public ContourSubchunk[] Subchunks { get; }

	// this can be VoxelChunkChangeset.Null, so check VoxelChangeset.IsNull before accessing other members
	public VoxelChunkChangeset VoxelChangeset { get; }

	public ReadOnlyContourChunkResult AsReadOnly => new ReadOnlyContourChunkResult(this);
}
