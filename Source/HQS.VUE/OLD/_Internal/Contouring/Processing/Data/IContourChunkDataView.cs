﻿namespace HQS.VUE.OLD;

internal interface IContourChunkDataView : IReadOnlyAdjacencyCross<ReadOnlyVoxelChunk>
{
	public CubeParts[] SubchunksParts { get; }

	public WhatToContour WhatToContour { get; }

	public ContourSubchunk[] Subchunks { get; }

	public Pinned<ContourChunk> ContourPin { get; }

	public VolumeView VolumeView { get; }

	public VoxelChunkChangeset VoxelChangeset { get; }
}
