﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyContourSubchunkArray
{
	private readonly ContourSubchunk[] array;

	public ReadOnlyContourSubchunkArray(ContourSubchunk[] array)
	{
		this.array = array;
	}

	public bool IsNull => this.array == null;

	public int Length => this.array.Length;

	public ReadOnlyContourSubchunk this[int index] => this.array[index].AsReadOnly;
}
