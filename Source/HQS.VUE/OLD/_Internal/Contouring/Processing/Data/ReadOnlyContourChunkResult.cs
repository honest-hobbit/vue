﻿namespace HQS.VUE.OLD;

internal readonly struct ReadOnlyContourChunkResult
{
	public ReadOnlyContourChunkResult(ContourChunkResult data)
	{
		Debug.Assert(!data.IsNull);
		Debug.Assert(data.ContourPin != null);
		Debug.Assert(data.ContourPin.IsPinned);

		this.ContourPin = new ReadOnlyPin<ReadOnlyContourChunk>(
			data.ContourPin.CastTo<IChunk<ReadOnlyContourChunk>>()).AsView;
		this.WhatWasContoured = data.WhatWasContoured;
		this.Subchunks = new ReadOnlyContourSubchunkArray(data.Subchunks);
		this.VoxelChangeset = data.VoxelChangeset;
	}

	public bool IsNull => this.ContourPin.IsNull;

	// this will be pinned
	public ReadOnlyPinView<ReadOnlyContourChunk> ContourPin { get; }

	public WhatToContour WhatWasContoured { get; }

	// this can be null if the new contour chunk is empty
	public ReadOnlyContourSubchunkArray Subchunks { get; }

	// this can be VoxelChunkChangeset.Null, so check VoxelChangeset.IsNull before accessing other members
	public VoxelChunkChangeset VoxelChangeset { get; }
}
