﻿namespace HQS.VUE.OLD;

internal class SubchunkContourer
{
	private readonly AbstractMeshGenerator meshGenerator;

	private readonly SubchunkColliderGenerator colliderGenerator;

	public SubchunkContourer(VoxelSizeConfig sizes, VoxelTypesConfig types, UniverseConfig config)
	{
		Debug.Assert(sizes != null);
		Debug.Assert(types != null);
		Debug.Assert(config != null);

		this.meshGenerator = new GreedyMeshGenerator(sizes, types.GetSurface, config.ContourSubchunkMeshQuadsCapacity);
		this.colliderGenerator = new SubchunkColliderGenerator(sizes, types.GetColliderTypeIndex);
	}

	public CubeArrayIndexer VoxelIndexer => this.colliderGenerator.VoxelIndexer;

	public void ContourAll(AdjacencyCross<ReadOnlyVoxelSubchunk> source, ContourSubchunk result)
	{
		Debug.Assert(source != null);
		Debug.Assert(!result.IsEmpty);

		this.meshGenerator.GenerateAll(source, result.Mesh);
		this.colliderGenerator.Generate(source.Center, result.Colliders);
	}

	public void ContourParts(AdjacencyCross<ReadOnlyVoxelSubchunk> source, CubeParts parts, ContourSubchunk result)
	{
		Debug.Assert(source != null);
		Debug.Assert(parts != CubeParts.None);
		Debug.Assert(parts == CubeParts.All || !parts.Has(CubeParts.Center));
		Debug.Assert(!result.IsEmpty);

		if (parts == CubeParts.All)
		{
			this.ContourAll(source, result);
		}
		else
		{
			this.meshGenerator.GenerateFaces(source, parts, result.Mesh);
		}
	}
}
