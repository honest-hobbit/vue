﻿namespace HQS.VUE.OLD;

internal class ContouringQueueManager
{
	private const bool DoNotWaitUntilLoaded = false;

	private readonly List<Pin<ContourChunkData>> contourDataPins;

	private readonly List<Pin<VoxelChunk>> voxelChunkPins;

	private readonly Dictionary<ChunkKey, Pinned<ContourChunkData>> contourData;

	private readonly Dictionary<ChunkKey, Pinned<VoxelChunk>> voxelChunks;

	private readonly VoxelChunkCache voxelChunkCache;

	private readonly ContourChunkCache contourChunkCache;

	private readonly ContourChunkPools contourChunkPools;

	private readonly IPinPool<ContourChunkData> chunkDataPool;

	private readonly ContouringQueue contouringQueue;

	public ContouringQueueManager(
		VoxelChunkCache voxelChunkCache,
		ContourChunkCache contourChunkCache,
		ContourChunkPools contourChunkPools,
		IPinPool<ContourChunkData> chunkDataPool,
		ContouringQueue contouringQueue)
	{
		Debug.Assert(voxelChunkCache != null);
		Debug.Assert(contourChunkCache != null);
		Debug.Assert(contourChunkPools != null);
		Debug.Assert(chunkDataPool != null);
		Debug.Assert(contouringQueue != null);

		int capacity = VUEConstants.ChunksModifiedPerJob;
		this.contourDataPins = new List<Pin<ContourChunkData>>(capacity);
		this.voxelChunkPins = new List<Pin<VoxelChunk>>(capacity);

		var comparer = EqualityComparer.ForStruct<ChunkKey>();
		this.contourData = new Dictionary<ChunkKey, Pinned<ContourChunkData>>(capacity, comparer);
		this.voxelChunks = new Dictionary<ChunkKey, Pinned<VoxelChunk>>(capacity * 7, comparer);

		this.voxelChunkCache = voxelChunkCache;
		this.contourChunkCache = contourChunkCache;
		this.contourChunkPools = contourChunkPools;
		this.chunkDataPool = chunkDataPool;
		this.contouringQueue = contouringQueue;
	}

	// this assigns and tracks the contour chunk but does not assign any voxel chunks
	public ContourChunkData GetAndAddContourData(ChunkKey key, VolumeView volume, WhatToContour whatToContour)
	{
		Debug.Assert(volume != null);
		Debug.Assert(Enumeration.IsDefined(whatToContour));

		if (!this.contourData.TryGetValue(key, out var pinView))
		{
			var pin = this.chunkDataPool.Rent(out var data);

			data.SetContourPin(this.contourChunkCache.GetChunkPin(key, volume.RunJobInfo.VoxelShapeStore, DoNotWaitUntilLoaded));
			data.VolumeView = volume;
			data.Subchunks = this.contourChunkPools.SubchunkArrays.Rent();
			data.WhatToContour = whatToContour;

			Debug.Assert(data.SubchunksParts.All(x => x == CubeParts.None));

			pinView = pin.AsPinned;
			this.contourData.Add(key, pinView);
			this.contourDataPins.Add(pin);
		}

		return pinView.Value;
	}

	public ReadOnlyVoxelChunk GetVoxelChunk(ChunkKey key, VolumeView volume)
	{
		Debug.Assert(volume != null);

		if (!this.voxelChunks.TryGetValue(key, out var pinView))
		{
			var pin = this.voxelChunkCache.GetChunkPin(key, volume.RunJobInfo.VoxelGridStore, DoNotWaitUntilLoaded);
			pinView = pin.AsPinned;
			this.voxelChunks.Add(key, pinView);
			this.voxelChunkPins.Add(pin);
		}

		return pinView.Value.AsReadOnly;
	}

	public void ContourChunks(VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(diagnostics != null);

		diagnostics.StartTiming();
		this.LoadContourChunks();
		diagnostics.SetLoadContoursDuration();

		// wait for voxel chunks to load
		foreach (var pin in this.voxelChunks.Values)
		{
			pin.Value.Loading.Wait();
		}

		// start contouring
		diagnostics.StartTiming();
		int max = this.contourDataPins.Count;
		for (int i = 0; i < max; i++)
		{
			this.contouringQueue.AddChunk(this.contourDataPins[i].Value);
		}

		this.contouringQueue.ContourChunks(diagnostics);

		this.CleanupAndSetResults();
		diagnostics.SetContouringDuration();
	}

	private static bool AreAllEmpty(ContourSubchunk[] subchunks)
	{
		Debug.Assert(subchunks != null);

		for (int i = 0; i < subchunks.Length; i++)
		{
			if (!subchunks[i].IsEmpty)
			{
				return false;
			}
		}

		return true;
	}

	private void LoadContourChunks()
	{
		int max = this.contourDataPins.Count;
		for (int i = 0; i < max; i++)
		{
			var data = this.contourDataPins[i].Value;

			// wait for contour chunks to load
			var chunk = data.ContourPin.Value;
			chunk.Loading.Wait();

			if (!chunk.HasData)
			{
				// the chunk doesn't have any previously contoured data
				this.MarkEntireChunkForContouring(data);
			}
			else
			{
				// the chunk has previous data so copy it over
				chunk.Subchunks?.CopyTo(data.Subchunks, 0);
			}
		}
	}

	private void MarkEntireChunkForContouring(ContourChunkData data)
	{
		Debug.Assert(data != null);

		data.WhatToContour = WhatToContour.Everything;
		var key = data.ContourPin.Value.Key;
		var volume = data.VolumeView;

		if (data.Center.IsNull)
		{
			data.Center = this.GetVoxelChunk(key, volume);
		}

		if (data.NegX.IsNull)
		{
			data.NegX = this.GetVoxelChunk(key - Int3.UnitX, volume);
		}

		if (data.PosX.IsNull)
		{
			data.PosX = this.GetVoxelChunk(key + Int3.UnitX, volume);
		}

		if (data.NegY.IsNull)
		{
			data.NegY = this.GetVoxelChunk(key - Int3.UnitY, volume);
		}

		if (data.PosY.IsNull)
		{
			data.PosY = this.GetVoxelChunk(key + Int3.UnitY, volume);
		}

		if (data.NegZ.IsNull)
		{
			data.NegZ = this.GetVoxelChunk(key - Int3.UnitZ, volume);
		}

		if (data.PosZ.IsNull)
		{
			data.PosZ = this.GetVoxelChunk(key + Int3.UnitZ, volume);
		}
	}

	private void CleanupAndSetResults()
	{
		int max = this.contourDataPins.Count;
		for (int i = 0; i < max; i++)
		{
			using var pin = this.contourDataPins[i];
			var data = pin.Value;

			// check if the resulting contour chunk is empty
			if (AreAllEmpty(data.Subchunks))
			{
				this.contourChunkPools.SubchunkArrays.Return(data.Subchunks);
				data.Subchunks = null;
			}

			data.VolumeView.TrackChunk(new ContourChunkResult(data));

			// cleanup and recycle the contouring data
			data.Clear();
		}

		max = this.voxelChunkPins.Count;
		for (int i = 0; i < max; i++)
		{
			this.voxelChunkPins[i].Dispose();
		}

		this.contourDataPins.Clear();
		this.voxelChunkPins.Clear();
		this.contourData.Clear();
		this.voxelChunks.Clear();
	}
}
