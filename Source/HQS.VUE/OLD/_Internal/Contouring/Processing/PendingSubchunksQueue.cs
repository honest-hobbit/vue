﻿namespace HQS.VUE.OLD;

internal class PendingSubchunksQueue
{
	private readonly object padlock = new object();

	private readonly Queue<IContourChunkDataView> pendingChunks = new Queue<IContourChunkDataView>();

	private readonly int maxSubchunkIndex;

	private VoxelJobDiagnosticsRecorder diagnostics;

	private IContourChunkDataView chunk;

	private int subchunkIndex;

	public PendingSubchunksQueue(VoxelSizeConfig sizes)
	{
		Debug.Assert(sizes != null);

		this.maxSubchunkIndex = sizes.SubchunkIndexer.Length;
	}

	public bool IsStarted
	{
		get
		{
			lock (this.padlock)
			{
				return this.IsStartedInLock;
			}
		}
	}

	private bool IsStartedInLock => this.diagnostics != null;

	public void AddChunk(ContourChunkData data)
	{
		Debug.Assert(data != null);
		Debug.Assert(data.Subchunks != null);

		lock (this.padlock)
		{
			Debug.Assert(!this.IsStartedInLock);

			switch (data.WhatToContour)
			{
				case WhatToContour.Nothing:
					break;

				case WhatToContour.OnlyMissing:
					// chunks that aren't found in the cache/store will be marked as WhatToContour.Everything
					// so any chunks still marked as WhatToContour.OnlyMissing can be skipped
					data.WhatToContour = WhatToContour.Nothing;
					break;

				case WhatToContour.OnlyChanges:
				case WhatToContour.Everything:
					this.pendingChunks.Enqueue(data);
					break;

				default: throw InvalidEnumArgument.CreateException(nameof(data.WhatToContour), data.WhatToContour);
			}
		}
	}

	public bool TryStart(VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(diagnostics != null);

		lock (this.padlock)
		{
			Debug.Assert(!this.IsStartedInLock);

			if (!this.pendingChunks.TryDequeue(out this.chunk))
			{
				// nothing to contour
				return false;
			}

			this.diagnostics = diagnostics;
			this.subchunkIndex = 0;
			return true;
		}
	}

	public void Reset()
	{
		lock (this.padlock)
		{
			Debug.Assert(this.IsStartedInLock);

			this.diagnostics = null;
			this.subchunkIndex = 0;
			this.chunk = null;
		}
	}

	public bool TryGetNextSubchunkData(ContourSubchunkData data, out IContourChunkDataView chunk)
	{
		Debug.Assert(data != null);

		lock (this.padlock)
		{
			Debug.Assert(this.IsStartedInLock);

			return this.TryGetNextSubchunkIndex(data, out chunk);
		}
	}

	private bool TryGetNextSubchunkIndex(ContourSubchunkData data, out IContourChunkDataView chunk)
	{
		Debug.Assert(data != null);

		if (this.subchunkIndex >= this.maxSubchunkIndex)
		{
			if (this.pendingChunks.TryDequeue(out this.chunk))
			{
				this.subchunkIndex = 0;
			}
			else
			{
				chunk = null;
				return false;
			}
		}

		chunk = this.chunk;
		if (this.chunk.WhatToContour == WhatToContour.Everything)
		{
			data.Parts = CubeParts.All;
			data.SubchunkIndex = this.subchunkIndex;

			this.diagnostics.SubchunksFullyContoured++;

			this.subchunkIndex++;
			return true;
		}
		else
		{
			Debug.Assert(this.chunk.WhatToContour == WhatToContour.OnlyChanges);

			var subchunks = this.chunk.SubchunksParts;
			while (this.subchunkIndex < this.maxSubchunkIndex)
			{
				var subchunkParts = subchunks[this.subchunkIndex];
				if (subchunkParts != CubeParts.None)
				{
					data.Parts = subchunkParts;
					data.SubchunkIndex = this.subchunkIndex;

					if (subchunkParts == CubeParts.All)
					{
						this.diagnostics.SubchunksFullyContoured++;
					}
					else
					{
						this.diagnostics.SubchunksPartiallyContoured++;
					}

					this.subchunkIndex++;
					return true;
				}

				this.subchunkIndex++;
			}

			return this.TryGetNextSubchunkIndex(data, out chunk);
		}
	}
}
