﻿namespace HQS.VUE.OLD;

internal class ContourSubchunkCommand :
	AbstractCommand, IParallelCommand, IInitializable<ContourSubchunkCommand.Args>, INamespaceOLD
{
	private readonly ContourSubchunkData data = new ContourSubchunkData();

	private readonly SubchunkContourer contourer;

	private readonly IPool<ContourSubchunk> subchunks;

	private ContouringQueue queue;

	public ContourSubchunkCommand(SubchunkContourer contourer, IPool<ContourSubchunk> subchunks)
	{
		Debug.Assert(contourer != null);
		Debug.Assert(subchunks != null);

		this.contourer = contourer;
		this.subchunks = subchunks;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.ContouringQueue != null);
		this.ValidateAndSetInitialized();

		this.queue = args.ContouringQueue;
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.data.Clear();
		this.queue = null;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		while (this.queue.TryGetNextSubchunkData(this.data))
		{
			var subchunk = this.data.Subchunks[this.data.SubchunkIndex];
			if (subchunk.IsEmpty || this.data.Parts == CubeParts.All)
			{
				// if contouring all parts then the subchunk will be cleared anyway
				// so don't both copying previous data to it
				subchunk = this.subchunks.Rent();
			}
			else
			{
				// only part of the subchunk is being contoured and there is previous contour data so copy it
				var tempSubchunk = this.subchunks.Rent();
				tempSubchunk.Colliders.AddRange(subchunk.Colliders);
				tempSubchunk.Mesh.AddRange(subchunk.Mesh);
				subchunk = tempSubchunk;
			}

			// subchunk is guaranteed to not be shared with the previous chunk anymore
			this.contourer.ContourParts(this.data, this.data.Parts, subchunk);

			if (subchunk.CheckIsEmpty())
			{
				this.subchunks.Return(subchunk);
				subchunk = ContourSubchunk.Empty;
			}

			this.data.Subchunks[this.data.SubchunkIndex] = subchunk;
		}
	}

	public struct Args
	{
		public ContouringQueue ContouringQueue;
	}
}
