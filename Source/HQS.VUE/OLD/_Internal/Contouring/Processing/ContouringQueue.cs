﻿namespace HQS.VUE.OLD;

internal class ContouringQueue
{
	private readonly CommandWaiter runningCommands = new CommandWaiter();

	private readonly PendingSubchunksQueue pendingSubchunks;

	private readonly CubeArrayIndexer subchunkIndexer;

	private readonly int maxSubchunkIndex;

	private readonly int subchunksPerSide;

	private readonly int maxComputeParallelism;

	private readonly ICommandSubmitter<ContourSubchunkCommand.Args> contourProcessor;

	public ContouringQueue(
		UniverseConfig config, VoxelSizeConfig sizes, ICommandSubmitter<ContourSubchunkCommand.Args> contourProcessor)
	{
		Debug.Assert(config != null);
		Debug.Assert(sizes != null);
		Debug.Assert(contourProcessor != null);

		this.pendingSubchunks = new PendingSubchunksQueue(sizes);

		this.subchunkIndexer = sizes.SubchunkIndexer;
		this.maxSubchunkIndex = this.subchunkIndexer.Length;
		this.subchunksPerSide = this.subchunkIndexer.SideLength;

		this.maxComputeParallelism = DegreeOfParallelism.ProcessorCount;

		this.contourProcessor = contourProcessor;
	}

	public void AddChunk(ContourChunkData data) => this.pendingSubchunks.AddChunk(data);

	public void ContourChunks(VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(diagnostics != null);

		if (!this.pendingSubchunks.TryStart(diagnostics))
		{
			return;
		}

		// submit contouring commands equal to MaxComputeParallelism
		for (int count = 0; count < this.maxComputeParallelism; count++)
		{
			this.runningCommands.Add(this.contourProcessor.SubmitAndGetWaiter(
				new ContourSubchunkCommand.Args() { ContouringQueue = this }));
		}

		this.runningCommands.Wait();
		this.runningCommands.Clear();

		this.pendingSubchunks.Reset();
	}

	public bool TryGetNextSubchunkData(ContourSubchunkData data)
	{
		Debug.Assert(data != null);

		if (this.pendingSubchunks.TryGetNextSubchunkData(data, out var chunk))
		{
			this.FillSubchunkData(chunk, data);
			return true;
		}
		else
		{
			return false;
		}
	}

	private void FillSubchunkData(IContourChunkDataView chunk, ContourSubchunkData data)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(data != null);
		Debug.Assert(data.Parts != CubeParts.None);
		Debug.Assert(data.SubchunkIndex.IsIn(0, this.maxSubchunkIndex));

		data.Subchunks = chunk.Subchunks;
		data.Center = chunk.Center.GetSubchunkOrUniformVoxelAsSubchunk(data.SubchunkIndex);

		var index = this.subchunkIndexer[data.SubchunkIndex];
		var parts = data.Parts;

		if (parts.Has(CubeParts.NegX))
		{
			var source = chunk.Center;
			int x = index.X - 1;
			if (x == -1)
			{
				x += this.subchunksPerSide;
				source = chunk.NegX;
			}

			data.NegX = source.IsUniform ?
				source.GetUniformVoxelAsSubchunk() :
				source[this.subchunkIndexer[x, index.Y, index.Z]];
		}

		if (parts.Has(CubeParts.PosX))
		{
			var source = chunk.Center;
			int x = index.X + 1;
			if (x == this.subchunksPerSide)
			{
				x -= this.subchunksPerSide;
				source = chunk.PosX;
			}

			data.PosX = source.IsUniform ?
				source.GetUniformVoxelAsSubchunk() :
				source[this.subchunkIndexer[x, index.Y, index.Z]];
		}

		if (parts.Has(CubeParts.NegY))
		{
			var source = chunk.Center;
			int y = index.Y - 1;
			if (y == -1)
			{
				y += this.subchunksPerSide;
				source = chunk.NegY;
			}

			data.NegY = source.IsUniform ?
				source.GetUniformVoxelAsSubchunk() :
				source[this.subchunkIndexer[index.X, y, index.Z]];
		}

		if (parts.Has(CubeParts.PosY))
		{
			var source = chunk.Center;
			int y = index.Y + 1;
			if (y == this.subchunksPerSide)
			{
				y -= this.subchunksPerSide;
				source = chunk.PosY;
			}

			data.PosY = source.IsUniform ?
				source.GetUniformVoxelAsSubchunk() :
				source[this.subchunkIndexer[index.X, y, index.Z]];
		}

		if (parts.Has(CubeParts.NegZ))
		{
			var source = chunk.Center;
			int z = index.Z - 1;
			if (z == -1)
			{
				z += this.subchunksPerSide;
				source = chunk.NegZ;
			}

			data.NegZ = source.IsUniform ?
				source.GetUniformVoxelAsSubchunk() :
				source[this.subchunkIndexer[index.X, index.Y, z]];
		}

		if (parts.Has(CubeParts.PosZ))
		{
			var source = chunk.Center;
			int z = index.Z + 1;
			if (z == this.subchunksPerSide)
			{
				z -= this.subchunksPerSide;
				source = chunk.PosZ;
			}

			data.PosZ = source.IsUniform ?
				source.GetUniformVoxelAsSubchunk() :
				source[this.subchunkIndexer[index.X, index.Y, z]];
		}
	}
}
