﻿namespace HQS.VUE.OLD;

internal class ChangesetContourer
{
	private readonly ContouringQueueManager contourer;

	private readonly CubeArrayIndexer subchunkIndexer;

	private readonly int sideLength;

	public ChangesetContourer(VoxelSizeConfig config, ContouringQueueManager contourer)
	{
		Debug.Assert(config != null);
		Debug.Assert(contourer != null);

		this.contourer = contourer;
		this.subchunkIndexer = config.SubchunkIndexer;
		this.sideLength = this.subchunkIndexer.SideLength;
	}

	public void ContourChanges(IReadOnlyList<VolumeView> volumes, VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(volumes != null);
		Debug.Assert(volumes.Count > 0);
		Debug.Assert(diagnostics != null);

		// prepare contouring data for all changed chunks
		int volumesCount = volumes.Count;
		for (int volumeIndex = 0; volumeIndex < volumesCount; volumeIndex++)
		{
			var volume = volumes[volumeIndex];
			int chunksCount = volume.ChangedVoxelChunks.Count;
			for (int chunkIndex = 0; chunkIndex < chunksCount; chunkIndex++)
			{
				this.PrepareChunkForContouring(volume, volume.ChangedVoxelChunks[chunkIndex]);
			}
		}

		this.contourer.ContourChunks(diagnostics);
	}

	private void PrepareChunkForContouring(VolumeView volume, VoxelChunkChangeset chunk)
	{
		Debug.Assert(volume != null);
		Debug.Assert(!chunk.IsNull);

		var data = this.contourer.GetAndAddContourData(chunk.Key, volume, WhatToContour.OnlyChanges);
		data.Center = chunk.Current;
		data.VoxelChangeset = chunk;
		this.DetermineSubchunksParts(data, chunk);
	}

	private void DetermineSubchunksParts(ContourChunkData data, VoxelChunkChangeset chunk)
	{
		Debug.Assert(data != null);
		Debug.Assert(!chunk.IsNull);

		// The face parts start null and will be assigned while iterating the subchunks and only as needed.
		// By placing the variables here, once assigned they will be reused later in the subchunks loop.
		CubeParts[] partsCenter = data.SubchunksParts;
		CubeParts[] partsNegX = null;
		CubeParts[] partsPosX = null;
		CubeParts[] partsNegY = null;
		CubeParts[] partsPosY = null;
		CubeParts[] partsNegZ = null;
		CubeParts[] partsPosZ = null;

		int max = chunk.ChangedSubchunksCount;
		var changedSubchunks = chunk.ChangedSubchunks;
		for (int i = 0; i < max; i++)
		{
			int subchunkIndex = changedSubchunks[i];
			partsCenter[subchunkIndex] = CubeParts.All;
			var index3D = this.subchunkIndexer[subchunkIndex];
			HandleNegX(index3D);
			HandlePosX(index3D);
			HandleNegY(index3D);
			HandlePosY(index3D);
			HandleNegZ(index3D);
			HandlePosZ(index3D);
		}

		void HandleNegX(Int3 index)
		{
			var parts = partsCenter;
			int x = index.X - 1;
			if (x == -1)
			{
				x += this.sideLength;
				if (partsNegX == null)
				{
					var adjacentData = GetAdjacentData(-Int3.UnitX);
					data.NegX = adjacentData.Center;
					adjacentData.PosX = data.Center;
					partsNegX = adjacentData.SubchunksParts;
				}

				parts = partsNegX;
			}

			var adjacentIndex = this.subchunkIndexer[x, index.Y, index.Z];
			parts[adjacentIndex] = parts[adjacentIndex].Add(CubeParts.PosX);
		}

		void HandlePosX(Int3 index)
		{
			var parts = partsCenter;
			int x = index.X + 1;
			if (x == this.sideLength)
			{
				x -= this.sideLength;
				if (partsPosX == null)
				{
					var adjacentData = GetAdjacentData(Int3.UnitX);
					data.PosX = adjacentData.Center;
					adjacentData.NegX = data.Center;
					partsPosX = adjacentData.SubchunksParts;
				}

				parts = partsPosX;
			}

			var adjacentIndex = this.subchunkIndexer[x, index.Y, index.Z];
			parts[adjacentIndex] = parts[adjacentIndex].Add(CubeParts.NegX);
		}

		void HandleNegY(Int3 index)
		{
			var parts = partsCenter;
			int y = index.Y - 1;
			if (y == -1)
			{
				y += this.sideLength;
				if (partsNegY == null)
				{
					var adjacentData = GetAdjacentData(-Int3.UnitY);
					data.NegY = adjacentData.Center;
					adjacentData.PosY = data.Center;
					partsNegY = adjacentData.SubchunksParts;
				}

				parts = partsNegY;
			}

			var adjacentIndex = this.subchunkIndexer[index.X, y, index.Z];
			parts[adjacentIndex] = parts[adjacentIndex].Add(CubeParts.PosY);
		}

		void HandlePosY(Int3 index)
		{
			var parts = partsCenter;
			int y = index.Y + 1;
			if (y == this.sideLength)
			{
				y -= this.sideLength;
				if (partsPosY == null)
				{
					var adjacentData = GetAdjacentData(Int3.UnitY);
					data.PosY = adjacentData.Center;
					adjacentData.NegY = data.Center;
					partsPosY = adjacentData.SubchunksParts;
				}

				parts = partsPosY;
			}

			var adjacentIndex = this.subchunkIndexer[index.X, y, index.Z];
			parts[adjacentIndex] = parts[adjacentIndex].Add(CubeParts.NegY);
		}

		void HandleNegZ(Int3 index)
		{
			var parts = partsCenter;
			int z = index.Z - 1;
			if (z == -1)
			{
				z += this.sideLength;
				if (partsNegZ == null)
				{
					var adjacentData = GetAdjacentData(-Int3.UnitZ);
					data.NegZ = adjacentData.Center;
					adjacentData.PosZ = data.Center;
					partsNegZ = adjacentData.SubchunksParts;
				}

				parts = partsNegZ;
			}

			var adjacentIndex = this.subchunkIndexer[index.X, index.Y, z];
			parts[adjacentIndex] = parts[adjacentIndex].Add(CubeParts.PosZ);
		}

		void HandlePosZ(Int3 index)
		{
			var parts = partsCenter;
			int z = index.Z + 1;
			if (z == this.sideLength)
			{
				z -= this.sideLength;
				if (partsPosZ == null)
				{
					var adjacentData = GetAdjacentData(Int3.UnitZ);
					data.PosZ = adjacentData.Center;
					adjacentData.NegZ = data.Center;
					partsPosZ = adjacentData.SubchunksParts;
				}

				parts = partsPosZ;
			}

			var adjacentIndex = this.subchunkIndexer[index.X, index.Y, z];
			parts[adjacentIndex] = parts[adjacentIndex].Add(CubeParts.NegZ);
		}

		ContourChunkData GetAdjacentData(Int3 index)
		{
			var key = chunk.Key + index;
			var adjacentData = this.contourer.GetAndAddContourData(key, data.VolumeView, WhatToContour.OnlyChanges);
			if (adjacentData.Center.IsNull)
			{
				adjacentData.Center = this.contourer.GetVoxelChunk(key, data.VolumeView);
			}

			return adjacentData;
		}
	}
}
