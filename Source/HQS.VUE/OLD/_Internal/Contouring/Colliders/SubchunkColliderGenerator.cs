﻿namespace HQS.VUE.OLD;

internal class SubchunkColliderGenerator
{
	private readonly Func<Voxel, ColliderTypeIndex> getColliderType;

	private readonly ColliderGenerator generator;

	private readonly Func<int, ColliderTypeIndex> indexToColliderType;

	private readonly Action<BoxColliderStructSlim> addCollider;

	private ReadOnlyArray<Voxel> voxels;

	private List<BoxColliderStructSlim> colliders;

	public SubchunkColliderGenerator(VoxelSizeConfig config, Func<Voxel, ColliderTypeIndex> getColliderType)
	{
		Debug.Assert(config != null);
		Debug.Assert(getColliderType != null);

		this.getColliderType = getColliderType;
		this.generator = new ColliderGenerator(config.VoxelIndexer);
		this.indexToColliderType = this.GetColliderType;
		this.addCollider = this.AddCollider;
	}

	public CubeArrayIndexer VoxelIndexer => this.generator.Indexer;

	public void Generate(ReadOnlyVoxelSubchunk subchunk, List<BoxColliderStructSlim> result)
	{
		Debug.Assert(result != null);

		result.Clear();
		if (subchunk.IsUniform)
		{
			var colliderType = this.getColliderType(subchunk.UniformVoxel);
			if (colliderType != ColliderTypeIndex.Empty)
			{
				result.Add(this.generator.CreateMaxSizedBox(colliderType));
			}
		}
		else
		{
			this.voxels = subchunk.Voxels;
			this.colliders = result;
			this.generator.GenerateColliders(this.indexToColliderType, this.addCollider);
			this.voxels = default;
			this.colliders = null;
		}
	}

	private ColliderTypeIndex GetColliderType(int index) => this.getColliderType(this.voxels[index]);

	private void AddCollider(BoxColliderStructSlim box) => this.colliders.Add(box);
}
