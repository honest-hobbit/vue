﻿namespace HQS.VUE.OLD;

internal readonly struct BoxColliderStruct
{
	public BoxColliderStruct(Vector3 center, Vector3 size, ColliderTypeIndex type)
	{
		this.Center = center;
		this.Size = size;
		this.Type = type;
	}

	public Vector3 Center { get; }

	public Vector3 Size { get; }

	public ColliderTypeIndex Type { get; }
}
