﻿namespace HQS.VUE.OLD;

internal class ColliderGenerator
{
	private readonly int length;

	private readonly int boxMax;

	private readonly int sideMax;

	private readonly ColliderTypeIndex[] colliderType;

	public ColliderGenerator(CubeArrayIndexer indexer)
	{
		Debug.Assert(VoxelSizeConfig.IsExponentValid(indexer.Exponent));

		this.Indexer = indexer;
		this.length = indexer.Length;
		this.boxMax = indexer.SideLength;
		this.sideMax = this.boxMax - 1;
		this.colliderType = new ColliderTypeIndex[this.length];
	}

	public CubeArrayIndexer Indexer { get; }

	public bool IsMaxSizedBox(BoxColliderStructSlim box) =>
		box.XMin == 0 && box.XMax == this.boxMax &&
		box.YMin == 0 && box.YMax == this.boxMax &&
		box.ZMin == 0 && box.ZMax == this.boxMax;

	public BoxColliderStructSlim CreateMaxSizedBox(ColliderTypeIndex type) =>
		BoxColliderStructSlim.From(Int3.Zero, new Int3(this.boxMax), type);

	public void GenerateColliders(Func<int, ColliderTypeIndex> getColliderType, Action<BoxColliderStructSlim> addCollider)
	{
		Debug.Assert(getColliderType != null);
		Debug.Assert(addCollider != null);

		bool isUniform = true;
		var firstType = getColliderType(0);
		this.colliderType[0] = firstType;

		for (int i = 1; i < this.length; i++)
		{
			var nextType = getColliderType(i);
			this.colliderType[i] = nextType;
			isUniform &= nextType == firstType;
		}

		if (isUniform && firstType != ColliderTypeIndex.Empty)
		{
			addCollider(this.CreateMaxSizedBox(firstType));
		}
		else
		{
			for (int i = 0; i < this.length; i++)
			{
				this.TryCreateCollider(i, addCollider);
			}
		}
	}

	private void TryCreateCollider(int index, Action<BoxColliderStructSlim> addCollider)
	{
		var type = this.colliderType[index];
		if (type == ColliderTypeIndex.Empty)
		{
			return;
		}

		var min = this.Indexer[index];
		var max = this.ExpandCollider(type, min);

		// clear out the area covered by this collider
		for (int iX = min.X; iX < max.X; iX++)
		{
			for (int iY = min.Y; iY < max.Y; iY++)
			{
				for (int iZ = min.Z; iZ < max.Z; iZ++)
				{
					this.colliderType[this.Indexer[iX, iY, iZ]] = ColliderTypeIndex.Empty;
				}
			}
		}

		addCollider(BoxColliderStructSlim.From(min, max, type));
	}

	private Int3 ExpandCollider(ColliderTypeIndex type, Int3 min)
	{
		int maxX = min.X;
		int maxY = min.Y;
		int maxZ = min.Z;

		bool tryPushX = true;
		bool tryPushY = true;
		bool tryPushZ = true;

		while (tryPushX || tryPushY || tryPushZ)
		{
			if (tryPushX)
			{
				tryPushX = TryPushX();
			}

			if (tryPushZ)
			{
				tryPushZ = TryPushZ();
			}

			// purposely biasing towards horizontal expansion first, then vertical
			// because game worls tend to be more horizontally orientated
			if (tryPushY)
			{
				tryPushY = TryPushY();
			}
		}

		return new Int3(maxX + 1, maxY + 1, maxZ + 1);

		bool TryPushX()
		{
			if (maxX == this.sideMax)
			{
				return false;
			}

			int pushX = maxX + 1;
			for (int iY = min.Y; iY <= maxY; iY++)
			{
				for (int iZ = min.Z; iZ <= maxZ; iZ++)
				{
					if (this.colliderType[this.Indexer[pushX, iY, iZ]] != type)
					{
						return false;
					}
				}
			}

			maxX = pushX;
			return true;
		}

		bool TryPushY()
		{
			if (maxY == this.sideMax)
			{
				return false;
			}

			int pushY = maxY + 1;
			for (int iX = min.X; iX <= maxX; iX++)
			{
				for (int iZ = min.Z; iZ <= maxZ; iZ++)
				{
					if (this.colliderType[this.Indexer[iX, pushY, iZ]] != type)
					{
						return false;
					}
				}
			}

			maxY = pushY;
			return true;
		}

		bool TryPushZ()
		{
			if (maxZ == this.sideMax)
			{
				return false;
			}

			int pushZ = maxZ + 1;
			for (int iX = min.X; iX <= maxX; iX++)
			{
				for (int iY = min.Y; iY <= maxY; iY++)
				{
					if (this.colliderType[this.Indexer[iX, iY, pushZ]] != type)
					{
						return false;
					}
				}
			}

			maxZ = pushZ;
			return true;
		}
	}
}
