﻿namespace HQS.VUE.OLD;

[StructLayout(LayoutKind.Explicit)]
internal readonly struct BoxColliderStructSlim
{
	public BoxColliderStructSlim(byte xMin, byte xMax, byte yMin, byte yMax, byte zMin, byte zMax, ColliderTypeIndex type)
	{
		Debug.Assert(xMin < xMax);
		Debug.Assert(yMin < yMax);
		Debug.Assert(zMin < zMax);

		this.RawValue = 0;
		this.XMin = xMin;
		this.XMax = xMax;
		this.YMin = yMin;
		this.YMax = yMax;
		this.ZMin = zMin;
		this.ZMax = zMax;
		this.Type = type;
	}

	public BoxColliderStructSlim(ulong rawValue)
	{
		this.XMin = 0;
		this.XMax = 0;
		this.YMin = 0;
		this.YMax = 0;
		this.ZMin = 0;
		this.ZMax = 0;
		this.Type = default;
		this.RawValue = rawValue;
	}

	public static BoxColliderStructSlim From(int xMin, int xMax, int yMin, int yMax, int zMin, int zMax, ColliderTypeIndex type)
	{
		Debug.Assert(xMin.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(xMax.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(yMin.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(yMax.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(zMin.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(zMax.IsIn(byte.MinValue, byte.MaxValue));

		return new BoxColliderStructSlim((byte)xMin, (byte)xMax, (byte)yMin, (byte)yMax, (byte)zMin, (byte)zMax, type);
	}

	public static BoxColliderStructSlim From(Int3 min, Int3 max, ColliderTypeIndex type)
	{
		Debug.Assert(min.X.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(max.X.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(min.Y.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(max.Y.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(min.Z.IsIn(byte.MinValue, byte.MaxValue));
		Debug.Assert(max.Z.IsIn(byte.MinValue, byte.MaxValue));

		return new BoxColliderStructSlim((byte)min.X, (byte)max.X, (byte)min.Y, (byte)max.Y, (byte)min.Z, (byte)max.Z, type);
	}

	[FieldOffset(0)]
	public readonly ulong RawValue;

	[FieldOffset(0)]
	public readonly byte XMin;

	[FieldOffset(1)]
	public readonly byte XMax;

	[FieldOffset(2)]
	public readonly byte YMin;

	[FieldOffset(3)]
	public readonly byte YMax;

	[FieldOffset(4)]
	public readonly byte ZMin;

	[FieldOffset(5)]
	public readonly byte ZMax;

	[FieldOffset(6)]
	public readonly ColliderTypeIndex Type;

	public BoxColliderStruct Expand(Func<byte, float> getLength)
	{
		Debug.Assert(getLength != null);

		float xMin = getLength(this.XMin);
		float xMax = getLength(this.XMax);
		float yMin = getLength(this.YMin);
		float yMax = getLength(this.YMax);
		float zMin = getLength(this.ZMin);
		float zMax = getLength(this.ZMax);

		var size = new Vector3(xMax - xMin, yMax - yMin, zMax - zMin);
		var center = new Vector3((size.X * .5f) + xMin, (size.Y * .5f) + yMin, (size.Z * .5f) + zMin);
		return new BoxColliderStruct(center, size, this.Type);
	}

	public BoxColliderStruct Expand()
	{
		var size = new Vector3(this.XMax - this.XMin, this.YMax - this.YMin, this.ZMax - this.ZMin);
		var center = new Vector3((size.X * .5f) + this.XMin, (size.Y * .5f) + this.YMin, (size.Z * .5f) + this.ZMin);
		return new BoxColliderStruct(center, size, this.Type);
	}

	public BoxColliderStruct Expand(Vector3 offset)
	{
		var size = new Vector3(this.XMax - this.XMin, this.YMax - this.YMin, this.ZMax - this.ZMin);
		var center = new Vector3(
			(size.X * .5f) + this.XMin + offset.X,
			(size.Y * .5f) + this.YMin + offset.Y,
			(size.Z * .5f) + this.ZMin + offset.Z);
		return new BoxColliderStruct(center, size, this.Type);
	}
}
