﻿namespace HQS.VUE.OLD;

internal class ContourChunkSerDes : ISerializer<ReadOnlyContourChunk>, IInlineDeserializer<ContourChunk>
{
	private readonly ArraySerDes<ReadOnlyContourSubchunk, ContourSubchunk> subchunkArraySerDes;

	private readonly ContourChunkPools pools;

	public ContourChunkSerDes(VoxelSizeConfig config, ContourChunkPools pools)
	{
		Debug.Assert(config != null);
		Debug.Assert(pools != null);

		this.pools = pools;
		var subchunkSerDes = new ContourSubchunkSerDes(pools);
		this.subchunkArraySerDes = new ArraySerDes<ReadOnlyContourSubchunk, ContourSubchunk>(
			EnumerableSerDesStrategy.RunLengthEncoded(
				subchunkSerDes,
				subchunkSerDes,
				new RunLengthEncoderOptions<ReadOnlyContourSubchunk>()
				{
					CountSerDes = SerializeCount.AsConstant(config.SubchunkIndexer.Length),
					RunLengthSerDes = SerializeCount.AsSignedVarint,
					Comparer = SubchunkComparer.Instance,
				}));
	}

	/// <inheritdoc />
	public void Serialize(ReadOnlyContourChunk chunk, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(chunk, writeByte);

		bool isEmpty = chunk.IsEmpty;
		SerDes.OfBool.Serialize(isEmpty, writeByte);
		if (!isEmpty)
		{
			this.subchunkArraySerDes.Serialize(chunk.Subchunks, writeByte);
		}
	}

	/// <inheritdoc />
	public void DeserializeInline(Func<byte> readByte, ContourChunk chunk)
	{
		IInlineDeserializerContracts.DeserializeInline(readByte, chunk);
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.Subchunks == null);

		bool isEmpty = SerDes.OfBool.Deserialize(readByte);
		if (!isEmpty)
		{
			chunk.Subchunks = this.pools.SubchunkArrays.Rent();
			this.subchunkArraySerDes.DeserializeInline(readByte, chunk.Subchunks);
		}
	}

	private class SubchunkComparer : IEqualityComparer<ReadOnlyContourSubchunk>
	{
		private SubchunkComparer()
		{
		}

		public static SubchunkComparer Instance { get; } = new SubchunkComparer();

		/// <inheritdoc />
		public bool Equals(ReadOnlyContourSubchunk x, ReadOnlyContourSubchunk y) => x.IsEmpty && y.IsEmpty;

		/// <inheritdoc />
		public int GetHashCode(ReadOnlyContourSubchunk obj) => throw new NotSupportedException();
	}
}
