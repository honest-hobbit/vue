﻿namespace HQS.VUE.OLD;

internal static class ColliderTypeIndexSerDes
{
	public static ISerDes<ColliderTypeIndex> Instance { get; } =
		ConverterSerDes.Create(x => x.Index, x => new ColliderTypeIndex(x), SerDes.OfUShort);
}
