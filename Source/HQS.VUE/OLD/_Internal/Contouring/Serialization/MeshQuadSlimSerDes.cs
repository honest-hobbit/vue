﻿namespace HQS.VUE.OLD;

internal static class MeshQuadSlimSerDes
{
	public static ISerDes<MeshQuadSlim> Instance { get; } =
		ConverterSerDes.Create(x => x.RawValue, x => new MeshQuadSlim(x), SerDes.OfULong);
}
