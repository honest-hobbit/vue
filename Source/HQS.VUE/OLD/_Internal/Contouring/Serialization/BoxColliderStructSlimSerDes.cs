﻿namespace HQS.VUE.OLD;

internal static class BoxColliderStructSlimSerDes
{
	public static ISerDes<BoxColliderStructSlim> Instance { get; } =
		ConverterSerDes.Create(x => x.RawValue, x => new BoxColliderStructSlim(x), SerDes.OfULong);
}
