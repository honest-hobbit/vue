﻿namespace HQS.VUE.OLD;

internal class ContourSubchunkSerDes : ISerializer<ReadOnlyContourSubchunk>, IDeserializer<ContourSubchunk>
{
	private static readonly ListSerDes<MeshQuadSlim> MeshSerDes =
		new ListSerDes<MeshQuadSlim>(MeshQuadSlimSerDes.Instance, SerializeCount.AsUnsignedVarint);

	private static readonly ListSerDes<BoxColliderStructSlim> CollidersSerDes =
		new ListSerDes<BoxColliderStructSlim>(BoxColliderStructSlimSerDes.Instance, SerializeCount.AsUnsignedVarint);

	private readonly ContourChunkPools pools;

	public ContourSubchunkSerDes(ContourChunkPools pools)
	{
		Debug.Assert(pools != null);

		this.pools = pools;
	}

	/// <inheritdoc />
	public void Serialize(ReadOnlyContourSubchunk subchunk, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(subchunk, writeByte);

		bool isEmpty = subchunk.IsEmpty;
		SerDes.OfBool.Serialize(isEmpty, writeByte);
		if (!isEmpty)
		{
			MeshSerDes.Serialize(subchunk.Mesh, writeByte);
			CollidersSerDes.Serialize(subchunk.Colliders, writeByte);
		}
	}

	/// <inheritdoc />
	public ContourSubchunk Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		bool isEmpty = SerDes.OfBool.Deserialize(readByte);
		if (isEmpty)
		{
			return ContourSubchunk.Empty;
		}
		else
		{
			var subchunk = this.pools.Subchunks.Rent();
			MeshSerDes.DeserializeInline(readByte, subchunk.Mesh);
			CollidersSerDes.DeserializeInline(readByte, subchunk.Colliders);
			return subchunk;
		}
	}
}
