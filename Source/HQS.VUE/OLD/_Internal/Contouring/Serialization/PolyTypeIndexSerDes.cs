﻿namespace HQS.VUE.OLD;

internal static class PolyTypeIndexSerDes
{
	public static ISerDes<PolyTypeIndex> Instance { get; } =
		ConverterSerDes.Create(x => x.Index, x => new PolyTypeIndex(x), SerDes.OfUShort);
}
