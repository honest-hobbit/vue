﻿namespace HQS.VUE.OLD;

internal class ContourChunkChangeTracker : IKeyed<Guid>
{
	private readonly ContourChunkPools pools;

	private readonly List<ContourChunkResult> chunks;

	private readonly List<ReadOnlyPin<ReadOnlyContourChunk>> pins;

	private readonly List<SequentialPair<ContourSubchunk[]>> cleanup;

	public ContourChunkChangeTracker(ContourChunkPools pools, int chunkCapacity)
	{
		Debug.Assert(pools != null);
		Debug.Assert(chunkCapacity >= 0);

		this.pools = pools;
		this.chunks = new List<ContourChunkResult>(chunkCapacity);
		this.pins = new List<ReadOnlyPin<ReadOnlyContourChunk>>(chunkCapacity);
		this.cleanup = new List<SequentialPair<ContourSubchunk[]>>(chunkCapacity);

		this.Chunks = ReadOnlyList.ConvertReadOnly(this.chunks, x => x.AsReadOnly);
		this.Pins = ReadOnlyList.ConvertReadOnly(this.pins, x => x.AsView);
	}

	/// <inheritdoc />
	public Guid Key { get; set; }

	// this will not contain any duplicates,
	public IReadOnlyList<ReadOnlyContourChunkResult> Chunks { get; }

	// this will not contain any duplicates,
	public IReadOnlyList<ReadOnlyPinView<ReadOnlyContourChunk>> Pins { get; }

	public void AddChunk(ContourChunkResult chunk)
	{
		Debug.Assert(!chunk.IsNull);
		Debug.Assert(chunk.ContourPin.IsPinned);
		Debug.Assert(chunk.ContourPin.Value.Key.ChunkGrid == this.Key);
		Debug.Assert(!this.chunks.Select(x => x.ContourPin.Value.Key).Contains(chunk.ContourPin.Value.Key, EqualityComparer.ForStruct<ChunkKey>()));
		Debug.Assert(this.cleanup.Count == 0);
		Debug.Assert(this.pins.Count == 0);

		this.chunks.Add(chunk);
	}

	public void CommitChanges()
	{
		Debug.Assert(this.cleanup.Count == 0);
		Debug.Assert(this.pins.Count == 0);

		int max = this.chunks.Count;
		for (int i = 0; i < max; i++)
		{
			var contourResult = this.chunks[i];
			var chunk = contourResult.ContourPin.Value;

			this.cleanup.Add(new SequentialPair<ContourSubchunk[]>(chunk.Subchunks, contourResult.Subchunks));
			////this.pins.Add(new CachePin<ReadOnlyContourChunk>(contourResult.ContourPin));

			switch (contourResult.WhatWasContoured)
			{
				case WhatToContour.Nothing:
				case WhatToContour.OnlyMissing:
					// the contour chunk was loaded in order to resync the host application but nothing was changed
					contourResult.ContourPin.Dispose();
					break;

				case WhatToContour.OnlyChanges:
				case WhatToContour.Everything:
					// some part of the chunk was contoured so the entire chunk needs to be persisted
					this.pins.Add(new ReadOnlyPin<ReadOnlyContourChunk>(
						contourResult.ContourPin.CastTo<IChunk<ReadOnlyContourChunk>>()));
					break;

				default:
					throw InvalidEnumArgument.CreateException(
						nameof(contourResult.WhatWasContoured), contourResult.WhatWasContoured);
			}

			// swap the subchunks
			chunk.Subchunks = contourResult.Subchunks;
			chunk.HasData = true;
		}
	}

	public void PostCommitCleanup()
	{
		int max = this.cleanup.Count;
		for (int i = 0; i < max; i++)
		{
			this.pools.ResetAndGiveIfNotShared(this.cleanup[i]);
		}

		this.cleanup.Clear();
		this.chunks.Clear();
	}

	public void Clear()
	{
		if (this.chunks.Count > 0)
		{
			Debug.Assert(this.cleanup.Count == 0);
			Debug.Assert(this.pins.Count == 0);

			// an exception was thrown while running the voxel job or Unity rejected the changes
			// so CommitChanges and PostCommitCleanup were never called
			int max = this.chunks.Count;
			for (int i = 0; i < max; i++)
			{
				var chunk = this.chunks[i];
				this.pools.ResetAndGiveIfNotShared(chunk.Subchunks, chunk.ContourPin.Value.Subchunks);
				chunk.ContourPin.Dispose();
			}

			this.chunks.Clear();
		}
		else
		{
			int max = this.pins.Count;
			for (int i = 0; i < max; i++)
			{
				this.pins[i].Unpin();
			}

			this.pins.Clear();
		}
	}
}
