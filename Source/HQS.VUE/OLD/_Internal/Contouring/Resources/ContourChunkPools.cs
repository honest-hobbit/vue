﻿namespace HQS.VUE.OLD;

internal class ContourChunkPools : AbstractDisposable,
	ITypeResolver<IPool<ContourChunk>>,
	ITypeResolver<IPool<ContourSubchunk[]>>,
	ITypeResolver<IPool<ContourSubchunk>>
{
	private readonly ExpiryPool<ContourChunk> chunks;

	private readonly ThreadSafeCompactingQueue<CacheExpiration> subchunksExpiryQueue;

	public ContourChunkPools(VoxelSizeConfig sizes, UniverseConfig config)
	{
		Debug.Assert(sizes != null);
		Debug.Assert(config != null);

		this.chunks = new ExpiryPool<ContourChunk>(new ThreadSafePool<ContourChunk>(() => new ContourChunk()));

		this.subchunksExpiryQueue = new ThreadSafeCompactingQueue<CacheExpiration>(x => x.IsStale);

		this.SubchunkArrays = new ExpiryPool<ContourSubchunk[]>(
			new ThreadSafePool<ContourSubchunk[]>(
				PopulateUtility.GetCreateArraysFunc<ContourSubchunk>(sizes.SubchunkIndexer.Length)),
			this.subchunksExpiryQueue);

		int meshSize = config.ContourSubchunkMeshQuadsCapacity;
		int collidersSize = config.ContourSubchunkCollidersCapacity;
		this.Subchunks = new ExpiryPool<ContourSubchunk>(
			new ThreadSafePool<ContourSubchunk>(() => new ContourSubchunk(
				new List<MeshQuadSlim>(meshSize), new List<BoxColliderStructSlim>(collidersSize))),
			this.subchunksExpiryQueue);
	}

	public IPool<ContourChunk> Chunks => this.chunks;

	public IPool<ContourSubchunk[]> SubchunkArrays { get; }

	public IPool<ContourSubchunk> Subchunks { get; }

	/// <inheritdoc />
	IPool<ContourChunk> ITypeResolver<IPool<ContourChunk>>.GetInstance() => this.Chunks;

	/// <inheritdoc />
	IPool<ContourSubchunk[]> ITypeResolver<IPool<ContourSubchunk[]>>.GetInstance() => this.SubchunkArrays;

	/// <inheritdoc />
	IPool<ContourSubchunk> ITypeResolver<IPool<ContourSubchunk>>.GetInstance() => this.Subchunks;

	public void EnqueueChunkExpiration(ContourChunk chunk, CacheExpiration token)
	{
		Debug.Assert(chunk != null);

		this.chunks.AddExpiration(token);

		if (chunk.Subchunks != null)
		{
			// expiry queue is shared for both subchunk arrays and subchunks because
			// if the chunk is not empty then at least 1 subchunk is also not empty
			this.subchunksExpiryQueue.Enqueue(token);
		}
	}

	public void ResetAndGive(ContourChunk chunk)
	{
		Debug.Assert(chunk != null);

		var subchunks = chunk.Subchunks;
		if (subchunks != null)
		{
			this.ResetAndGive(subchunks);
			chunk.Subchunks = null;
		}

		chunk.ClearAndReset();
		this.chunks.Return(chunk);
	}

	public void ResetAndGive(ContourSubchunk[] subchunks)
	{
		Debug.Assert(subchunks != null);

		for (int i = 0; i < subchunks.Length; i++)
		{
			var subchunk = subchunks[i];
			if (!subchunk.IsEmpty)
			{
				subchunk.Mesh.Clear();
				subchunk.Colliders.Clear();
				this.Subchunks.Return(subchunk);
			}

			// subchunks hold onto references to lists and thus need to be cleared here
			subchunks[i] = ContourSubchunk.Empty;
		}

		this.SubchunkArrays.Return(subchunks);
	}

	public void ResetAndGiveIfNotShared(SequentialPair<ContourSubchunk[]> pair) =>
		this.ResetAndGiveIfNotShared(pair.Previous, pair.Next);

	public void ResetAndGiveIfNotShared(
		ContourSubchunk[] previousSubchunks, ContourSubchunk[] nextSubchunks)
	{
		if (previousSubchunks == null)
		{
			return;
		}

		if (nextSubchunks == null)
		{
			this.ResetAndGive(previousSubchunks);
			return;
		}

		Debug.Assert(previousSubchunks != nextSubchunks);

		int max = previousSubchunks.Length;
		for (int i = 0; i < max; i++)
		{
			var previous = previousSubchunks[i];
			var next = nextSubchunks[i];

			if (!previous.IsEmpty && previous != next)
			{
				previous.Clear();
				this.Subchunks.Return(previous);
			}

			previousSubchunks[i] = ContourSubchunk.Empty;
		}

		this.SubchunkArrays.Return(previousSubchunks);
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.Chunks.ReleaseAll();
		this.SubchunkArrays.ReleaseAll();
		this.Subchunks.ReleaseAll();
	}
}
