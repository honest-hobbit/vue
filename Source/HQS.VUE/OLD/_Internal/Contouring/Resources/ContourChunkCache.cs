﻿namespace HQS.VUE.OLD;

internal class ContourChunkCache : AbstractDisposable, IChunkCache
{
	private readonly MemoryCache<ChunkKey, ContourChunk> cache;

	private readonly ChunkLoaderOLD<IVoxelShapeStore, ContourChunk> loader;

	public ContourChunkCache(
		ContourChunkPools pools, ICommandSubmitter<LoadChunkCommandOLD<IVoxelShapeStore, ContourChunk>.Args> loadProcessor)
	{
		Debug.Assert(pools != null);
		Debug.Assert(loadProcessor != null);

		this.cache = new MemoryCache<ChunkKey, ContourChunk>(new CachingFactory(pools), ChunkKey.Comparer);
		this.loader = new ChunkLoaderOLD<IVoxelShapeStore, ContourChunk>(loadProcessor);
	}

	/// <inheritdoc />
	public int Count => this.cache.Count;

	/// <inheritdoc />
	public int TryClear() => this.cache.GetPinsSnapshot().TryExpireAll();

	/// <inheritdoc />
	public IEnumerable<ChunkCacheEntry> GetChunksSnapshot()
	{
		foreach (var entry in this.cache.GetPinsSnapshot())
		{
			yield return new ChunkCacheEntry(entry.Key, entry.PinCount);
		}
	}

	public Pin<ContourChunk> GetChunkPin(ChunkKey key, IVoxelShapeStore store, bool waitUntilLoaded)
	{
		Debug.Assert(!this.IsDisposed);

		var cached = this.cache.GetCached(key);
		bool isLoading = this.loader.CheckLoading(cached, store);
		if (isLoading && waitUntilLoaded)
		{
			cached.Pin.Value.Loading.Wait();
		}

		return cached.Pin;
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.TryClear();

	private class CachingFactory : ICacheValueFactory<ChunkKey, ContourChunk>
	{
		private readonly ContourChunkPools pools;

		public CachingFactory(ContourChunkPools pools)
		{
			Debug.Assert(pools != null);

			this.pools = pools;
		}

		/// <inheritdoc />
		public ContourChunk CreateValue(ChunkKey key) => this.pools.Chunks.Rent();

		/// <inheritdoc />
		public void EnqueueToken(ChunkKey key, ContourChunk chunk, CacheExpiration token) =>
			this.pools.EnqueueChunkExpiration(chunk, token);

		/// <inheritdoc />
		public void ValueExpired(ChunkKey key, ContourChunk chunk) => this.pools.ResetAndGive(chunk);
	}
}
