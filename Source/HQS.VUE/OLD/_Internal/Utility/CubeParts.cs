﻿namespace HQS.VUE.OLD;

[Flags]
internal enum CubeParts : byte
{
	None = 0,

	Center = 1,

	NegX = 1 << 1,

	PosX = 1 << 2,

	NegY = 1 << 3,

	PosY = 1 << 4,

	NegZ = 1 << 5,

	PosZ = 1 << 6,

	AllFaces = NegX | PosX | NegY | PosY | NegZ | PosZ,

	All = Center | AllFaces,
}
