﻿namespace HQS.VUE.OLD;

internal interface IReadOnlyAdjacencyCross<T>
{
	public T Center { get; }

	public T NegX { get; }

	public T PosX { get; }

	public T NegY { get; }

	public T PosY { get; }

	public T NegZ { get; }

	public T PosZ { get; }
}
