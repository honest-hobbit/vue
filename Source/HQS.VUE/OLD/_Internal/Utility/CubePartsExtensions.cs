﻿namespace HQS.VUE.OLD;

internal static class CubePartsExtensions
{
	public static bool Has(this CubeParts current, CubeParts flag) => (current & flag) == flag;

	public static bool HasAny(this CubeParts current, CubeParts flags) => (current & flags) != 0;

	public static CubeParts Add(this CubeParts current, CubeParts flags) => current | flags;

	public static CubeParts Remove(this CubeParts current, CubeParts flags) => current & ~flags;

	public static CubeParts Set(this CubeParts current, CubeParts flags, bool value) =>
		value ? current.Add(flags) : current.Remove(flags);
}
