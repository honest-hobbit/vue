﻿namespace HQS.VUE.OLD;

internal class AdjacencyCross<T> : IReadOnlyAdjacencyCross<T>
{
	public AdjacencyCross()
	{
	}

	public AdjacencyCross(T value)
	{
		this.SetAllTo(value);
	}

	/// <inheritdoc />
	public T Center { get; set; }

	/// <inheritdoc />
	public T NegX { get; set; }

	/// <inheritdoc />
	public T PosX { get; set; }

	/// <inheritdoc />
	public T NegY { get; set; }

	/// <inheritdoc />
	public T PosY { get; set; }

	/// <inheritdoc />
	public T NegZ { get; set; }

	/// <inheritdoc />
	public T PosZ { get; set; }

	public void SetAllTo(T value)
	{
		this.Center = value;
		this.NegX = value;
		this.PosX = value;
		this.NegY = value;
		this.PosY = value;
		this.NegZ = value;
		this.PosZ = value;
	}

	public virtual void Clear() => this.SetAllTo(default);
}
