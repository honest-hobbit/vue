﻿namespace HQS.VUE.OLD;

internal class VoxelEngineOLD : AbstractCompletable
{
	private readonly EngineValidator validator;

	private readonly Reporter<Exception> errorReporter;

	private readonly HostCommandProcessorOLD hostProcessor;

	private readonly AggregateCompletable completables;

	public VoxelEngineOLD(
		VoxelSizeConfig sizeConfig,
		VoxelDefinitions definitions,
		Volumes volumes,
		VoxelJobProcessor jobQueue,
		EngineValidator validator,
		Reporter<Exception> errorReporter,
		HostCommandProcessorOLD hostProcessor,
		MainCommandProcessorOLD mainProcessor,
		ParallelCommandProcessor parallelProcessor,
		PersistenceCommandProcessor persistenceProcessor)
	{
		Debug.Assert(sizeConfig != null);
		Debug.Assert(definitions != null);
		Debug.Assert(volumes != null);
		Debug.Assert(jobQueue != null);
		Debug.Assert(validator != null);
		Debug.Assert(errorReporter != null);
		Debug.Assert(hostProcessor != null);
		Debug.Assert(mainProcessor != null);
		Debug.Assert(parallelProcessor != null);
		Debug.Assert(persistenceProcessor != null);

		this.SizeConfig = sizeConfig;
		this.Definitions = definitions;

		this.Volumes = volumes;
		this.Jobs = jobQueue;

		this.validator = validator;
		this.errorReporter = errorReporter;
		this.hostProcessor = hostProcessor;

		this.completables = new AggregateCompletable(mainProcessor, parallelProcessor, persistenceProcessor);
	}

	public IVoxelSizeConfig SizeConfig { get; }

	public IVoxelDefinitions Definitions { get; }

	public IVolumes Volumes { get; }

	public IVoxelJobProcessorOLD Jobs { get; }

	// returns how many commands were ran
	public int Update()
	{
		this.validator.ValidateEngine();

		return this.hostProcessor.RunCommands();
	}

	/// <inheritdoc />
	protected override async Task CompleteAsync()
	{
		try
		{
			this.validator.Complete();
			this.completables.Complete();
			await this.completables.Completion.DontMarshallContext();
		}
		catch (Exception error)
		{
			this.errorReporter.Report(error);
			throw;
		}
		finally
		{
			this.errorReporter.Complete();
		}
	}
}
