﻿namespace HQS.VUE.OLD;

internal class EngineDiagnostics : IDiagnostics
{
	public EngineDiagnostics(
		MainCommandProcessorOLD jobManager,
		Reporter<Exception> errorReporter,
		TypePoolCollection pools,
		EngineValidator validator,
		VoxelChunkCache voxelChunkCache,
		ContourChunkCache contourChunkCache)
	{
		Debug.Assert(jobManager != null);
		Debug.Assert(errorReporter != null);
		Debug.Assert(pools != null);
		Debug.Assert(validator != null);
		Debug.Assert(voxelChunkCache != null);
		Debug.Assert(contourChunkCache != null);

		this.JobCompleted = jobManager.Diagnostics;
		this.ErrorOccurred = errorReporter.Values;
		this.Pools = pools;
		this.VoxelChunkCache = new ChunkCacheWrapper(validator, voxelChunkCache);
		this.ContourChunkCache = new ChunkCacheWrapper(validator, contourChunkCache);
	}

	/// <inheritdoc />
	public IObservable<VoxelJobDiagnostics> JobCompleted { get; }

	/// <inheritdoc />
	public IObservable<Exception> ErrorOccurred { get; }

	/// <inheritdoc />
	public ITypePoolCollection Pools { get; }

	/// <inheritdoc />
	public IChunkCache VoxelChunkCache { get; }

	/// <inheritdoc />
	public IChunkCache ContourChunkCache { get; }
}
