﻿namespace HQS.VUE.OLD;

internal class EngineValidator
{
	private readonly EngineStatus status;

	private readonly int managedThreadId = Thread.CurrentThread.ManagedThreadId;

	public EngineValidator(EngineStatus status)
	{
		Debug.Assert(status != null);

		this.status = status;
	}

	public bool IsRunning => this.status.IsRunning;

	public bool IsValid => Thread.CurrentThread.ManagedThreadId == this.managedThreadId && this.IsRunning;

	public void Complete() => this.status.Complete();

	public void ValidateEngine()
	{
		if (Thread.CurrentThread.ManagedThreadId != this.managedThreadId)
		{
			throw new InvalidOperationException(
				$"{nameof(VoxelUniverseOLD)} accessed from a thread other than the thread it was created on.");
		}

		if (!this.IsRunning)
		{
			throw new InvalidOperationException($"{nameof(VoxelUniverseOLD.Complete)} was already called.");
		}
	}
}
