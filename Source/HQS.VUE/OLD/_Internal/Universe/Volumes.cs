﻿namespace HQS.VUE.OLD;

internal class Volumes : IVolumes
{
	private readonly Dictionary<Guid, Volume> volumes = new Dictionary<Guid, Volume>(EqualityComparer.ForStruct<Guid>());

	private readonly IVolumeServices services;

	public Volumes(IVolumeServices services)
	{
		Debug.Assert(services != null);

		this.services = services;
		this.Collection = this.volumes.AsReadOnlyDictionary();
	}

	/// <inheritdoc />
	public IReadOnlyDictionary<Guid, Volume> Collection { get; }

	/// <inheritdoc />
	public Volume CreateVolume()
	{
		this.services.Validator.ValidateEngine();

		var key = Guid.NewGuid();
		while (this.volumes.ContainsKey(key))
		{
			key = Guid.NewGuid();
		}

		return this.AddNewVolume(key);
	}

	/// <inheritdoc />
	public Volume CreateVolume(Guid key)
	{
		this.services.Validator.ValidateEngine();
		this.ValidateNotDefault(key);
		this.ValidateUnique(key);

		return this.AddNewVolume(key);
	}

	/// <inheritdoc />
	public Volume GetOrCreateVolume(Guid key)
	{
		this.services.Validator.ValidateEngine();
		this.ValidateNotDefault(key);

		return this.volumes.TryGetValue(key, out var volume) ? volume : this.AddNewVolume(key);
	}

	/// <inheritdoc />
	public Volume CreateVolume(VoxelGridRecord record)
	{
		this.services.Validator.ValidateEngine();
		this.ValidateNotDefault(record.Key);
		this.ValidateUnique(record.Key);
		record.Validate();

		var volume = new Volume(this.services, record);
		this.volumes[volume.Key] = volume;
		return volume;
	}

	private void ValidateNotDefault(Guid key)
	{
		if (key == Guid.Empty)
		{
			throw new ArgumentException("Volume key can't be an empty Guid.", nameof(key));
		}
	}

	private void ValidateUnique(Guid key)
	{
		if (this.volumes.ContainsKey(key))
		{
			throw new ArgumentException("Volume key is not unique.", nameof(key));
		}
	}

	private Volume AddNewVolume(Guid key)
	{
		var volume = new Volume(this.services, key);
		this.volumes[volume.Key] = volume;
		return volume;
	}
}
