﻿namespace HQS.VUE.OLD;

internal class CompleteVoxelJobCommand :
	AbstractCommand, IHostCommand, IInitializable<CompleteVoxelJobCommand.Args>, INamespaceOLD
{
	private readonly List<(Volume, VoxelGridChangedInfo)> volumes = new List<(Volume, VoxelGridChangedInfo)>();

	private readonly VoxelJobDiagnosticsRecorder diagnostics = new VoxelJobDiagnosticsRecorder();

	private readonly IReporter<Exception> errorReporter;

	private Pin<IVoxelJob> job;

	private TaskCompletionSource<VoxelJobDiagnostics> completion;

	public CompleteVoxelJobCommand(IReporter<Exception> errorReporter)
	{
		Debug.Assert(errorReporter != null);

		this.errorReporter = errorReporter;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.Volumes != null);
		Debug.Assert(args.Job.IsPinned);
		Debug.Assert(args.Diagnostics != null);
		this.ValidateAndSetInitialized();

		this.job = args.Job.CreatePin();
		args.Diagnostics.CopyTo(this.diagnostics);
		this.completion = args.Completion;

		int max = args.Volumes.Count;
		for (int i = 0; i < max; i++)
		{
			var volume = args.Volumes[i].Volume;
			this.volumes.Add((volume, volume.EngineData.GetUpdateInfo()));
		}
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.volumes.Clear();
		this.diagnostics.Clear();
		this.job.Dispose();
		this.job = default;
		this.completion = null;
	}

	/// <inheritdoc />
	protected override void OnRun()
	{
		// update the stats of all modified volumes
		int max = this.volumes.Count;
		for (int i = 0; i < max; i++)
		{
			var (volume, info) = this.volumes[i];
			volume.SetChangedInfo(info);
		}

		if (this.job.Value is IVoxelJobCompletion completable)
		{
			try
			{
				this.diagnostics.StartTiming();
				completable.Complete(this.diagnostics.GetSnapshot());
			}
			catch (Exception error)
			{
				this.diagnostics.CompleteJobException = error;
				this.errorReporter.Report(error);
			}
			finally
			{
				this.diagnostics.SetCompleteJobDuration();
			}
		}

		if (this.completion != null)
		{
			this.completion.TrySetResult(this.diagnostics.GetSnapshot());
		}
	}

	public struct Args
	{
		public IReadOnlyList<VolumeView> Volumes;

		public Pin<IVoxelJob> Job;

		public VoxelJobDiagnosticsRecorder Diagnostics;

		public TaskCompletionSource<VoxelJobDiagnostics> Completion;
	}
}
