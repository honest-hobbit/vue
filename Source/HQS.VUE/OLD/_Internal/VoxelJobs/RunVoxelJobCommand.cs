﻿namespace HQS.VUE.OLD;

internal class RunVoxelJobCommand : MainCommandOLD, IInitializable<RunVoxelJobCommand.Args>, IRequireScope, INamespaceOLD
{
	private const bool DoNotOverrideAutoSave = false;

	private readonly MultiVolumeView view;

	private readonly ChangesetContourer contourer;

	private readonly ChangesetRecorder recorder;

	private readonly ChangesetPersister persister;

	private readonly IResyncHostApplicationQueue resyncHost;

	private readonly ICommandSubmitter<CompleteVoxelJobCommand.Args> completeProcessor;

	private readonly IReporter<Exception> errorReporter;

	private Pin<IVoxelJob> job;

	private TaskCompletionSource<VoxelJobDiagnostics> completion;

	public RunVoxelJobCommand(
		MultiVolumeView volumesView,
		ChangesetContourer contourer,
		ChangesetRecorder recorder,
		ChangesetPersister persister,
		IResyncHostApplicationQueue resyncHost,
		ICommandSubmitter<CompleteVoxelJobCommand.Args> completeProcessor,
		IReporter<Exception> errorReporter)
	{
		Debug.Assert(volumesView != null);
		Debug.Assert(contourer != null);
		Debug.Assert(recorder != null);
		Debug.Assert(persister != null);
		Debug.Assert(resyncHost != null);
		Debug.Assert(completeProcessor != null);
		Debug.Assert(errorReporter != null);

		this.view = volumesView;
		this.contourer = contourer;
		this.recorder = recorder;
		this.persister = persister;
		this.resyncHost = resyncHost;
		this.completeProcessor = completeProcessor;
		this.errorReporter = errorReporter;
	}

	/// <inheritdoc />
	void IRequireScope.SetScope(IDisposable scope) => this.SetScope(scope);

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(args.Job.IsPinned);
		this.ValidateAndSetInitialized();

		this.completion = args.Completion;
		this.job = args.Job.CreatePin();
		this.view.Initialize(this.job.Value);
	}

	/// <inheritdoc />
	protected override void RunCommand()
	{
		this.Diagnostics.JobType = this.job.Value.GetType();
		try
		{
			this.HandleRunningJob();
			this.HandleChanges();
		}
		catch (Exception error)
		{
			// exception is rethrown because it happened somewhere in the engine outside of the voxel job
			this.view.DiscardAllChanges();
			this.Diagnostics.EngineException = error;
			throw;
		}
		finally
		{
			this.completeProcessor.Submit(new CompleteVoxelJobCommand.Args()
			{
				Volumes = this.view.ChangedVolumes,
				Job = this.job,
				Diagnostics = this.Diagnostics,
				Completion = this.completion,
			});

			this.view.Deinitialize();
			this.job.Dispose();
			this.job = default;
		}
	}

	private void HandleRunningJob()
	{
		this.Diagnostics.StartTiming();
		try
		{
			this.job.Value.RunOn(this.view);
		}
		catch (Exception error)
		{
			this.view.DiscardAllChanges();
			this.Diagnostics.RunJobException = error;
			this.errorReporter.Report(error);

			// exception is not rethrown because voxel jobs are fault tolerant
			return;
		}
		finally
		{
			this.Diagnostics.SetRunJobDuration();
		}

		this.Diagnostics.StartTiming();
		this.view.CleanupVoxelChanges();
		this.Diagnostics.SetCleanupChangesDuration();
	}

	private void HandleChanges()
	{
		if (this.view.ChangedVolumes.Count == 0)
		{
			// return early if nothing changed
			return;
		}

		// start recording voxel changes and contouring in parallel
		// recording changes will be waited on for completion before sending resync frame to Unity main thread
		this.recorder.StartRecordingChanges(
			this.view.ChangedVolumes, this.job.CastTo<IVoxelJobInfo>().AsPinned);

		// contour changes blocks until it finishes so it must be done after starting recording changes
		this.contourer.ContourChanges(this.view.ChangedVolumes, this.Diagnostics);

		// record diagnostics (this must be done before changes are possibly discarded in order to record what was attempted)
		int volumes = this.view.ChangedVolumes.Count;
		this.Diagnostics.VolumesChanged = volumes;
		for (int i = 0; i < volumes; i++)
		{
			var volume = this.view.ChangedVolumes[i];
			this.Diagnostics.VoxelsChanged += volume.VoxelsChanged;
			this.Diagnostics.VoxelChunksChanged += volume.ChangedVoxelChunks.Count;
			this.Diagnostics.ContourChunksChanged += volume.ChangedContourChunks.Count;
		}

		// verify changes with the host application
		if (!this.resyncHost.SubmitResyncFrame(
			this.view.ChangedVolumes,
			this.Diagnostics,
			this.job.Value.CheckCollision,
			this.job.CastTo<IVoxelJobInfo>().AsPinned,
			this.recorder))
		{
			// host application rejected the changes
			this.view.DiscardAllChanges();
			return;
		}

		// host application accepted the changes so commit permanently
		this.view.CommitChanges(this.Diagnostics.DateRan);

		// handle persistence
		this.Diagnostics.StartTiming();
		this.persister.PersistChanges(this.view.ChangedVolumes, DoNotOverrideAutoSave);
		this.Diagnostics.SetPersistenceDuration();
	}

	public struct Args
	{
		public Pinned<IVoxelJob> Job;

		public TaskCompletionSource<VoxelJobDiagnostics> Completion;
	}
}
