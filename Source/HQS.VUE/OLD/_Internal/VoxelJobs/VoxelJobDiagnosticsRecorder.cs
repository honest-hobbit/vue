﻿namespace HQS.VUE.OLD;

internal class VoxelJobDiagnosticsRecorder
{
	private readonly Stopwatch timer = new Stopwatch();

	public VoxelJobDiagnosticsRecorder()
	{
		this.Clear();
	}

	public Type JobType { get; set; }

	public DateTime DateRan { get; set; }

	public TimeSpan RunJobDuration { get; private set; }

	public TimeSpan CleanupChangesDuration { get; private set; }

	public TimeSpan LoadContoursDuration { get; private set; }

	public TimeSpan ContouringDuration { get; private set; }

	public TimeSpan RecordChangesDuration { get; private set; }

	public TimeSpan CopyResyncDuration { get; private set; }

	public TimeSpan VerifyResyncDuration { get; private set; }

	public TimeSpan PersistenceDuration { get; private set; }

	public TimeSpan CompleteJobDuration { get; private set; }

	public bool VerifyResync { get; set; }

	public bool ResyncSuccessful { get; set; }

	public int VolumesChanged { get; set; }

	public int VoxelsChanged { get; set; }

	public int VoxelChunksChanged { get; set; }

	public int ContourChunksChanged { get; set; }

	public int SubchunksFullyContoured { get; set; }

	public int SubchunksPartiallyContoured { get; set; }

	public int ChangeRecordsRecorded { get; set; }

	public Exception RunJobException { get; set; }

	public Exception EngineException { get; set; }

	public Exception CompleteJobException { get; set; }

	public void StartTiming()
	{
		Debug.Assert(!this.timer.IsRunning);
		this.timer.Restart();
	}

	public void SetRunJobDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.RunJobDuration = this.timer.Elapsed;
	}

	public void SetCleanupChangesDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.CleanupChangesDuration = this.timer.Elapsed;
	}

	public void SetLoadContoursDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.LoadContoursDuration = this.timer.Elapsed;
	}

	public void SetContouringDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.ContouringDuration = this.timer.Elapsed;
	}

	public void SetRecordChangesDuration(TimeSpan duration) => this.RecordChangesDuration = duration;

	public void SetCopyResyncDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.CopyResyncDuration = this.timer.Elapsed;
	}

	public void SetVerifyResyncDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.VerifyResyncDuration = this.timer.Elapsed;
	}

	public void SetPersistenceDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.PersistenceDuration = this.timer.Elapsed;
	}

	public void SetCompleteJobDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.CompleteJobDuration = this.timer.Elapsed;
	}

	public void CopyTo(VoxelJobDiagnosticsRecorder recorder)
	{
		Debug.Assert(recorder != null);

		recorder.JobType = this.JobType;
		recorder.DateRan = this.DateRan;
		recorder.RunJobDuration = this.RunJobDuration;
		recorder.CleanupChangesDuration = this.CleanupChangesDuration;
		recorder.LoadContoursDuration = this.LoadContoursDuration;
		recorder.ContouringDuration = this.ContouringDuration;
		recorder.RecordChangesDuration = this.RecordChangesDuration;
		recorder.CopyResyncDuration = this.CopyResyncDuration;
		recorder.VerifyResyncDuration = this.VerifyResyncDuration;
		recorder.PersistenceDuration = this.PersistenceDuration;
		recorder.CompleteJobDuration = this.CompleteJobDuration;
		recorder.VerifyResync = this.VerifyResync;
		recorder.ResyncSuccessful = this.ResyncSuccessful;
		recorder.VolumesChanged = this.VolumesChanged;
		recorder.VoxelsChanged = this.VoxelsChanged;
		recorder.VoxelChunksChanged = this.VoxelChunksChanged;
		recorder.ContourChunksChanged = this.ContourChunksChanged;
		recorder.SubchunksFullyContoured = this.SubchunksFullyContoured;
		recorder.SubchunksPartiallyContoured = this.SubchunksPartiallyContoured;
		recorder.ChangeRecordsRecorded = this.ChangeRecordsRecorded;
		recorder.RunJobException = this.RunJobException;
		recorder.EngineException = this.EngineException;
		recorder.CompleteJobException = this.CompleteJobException;
	}

	public void Clear()
	{
		this.timer.Reset();
		this.JobType = null;
		this.DateRan = default;
		this.RunJobDuration = TimeSpan.Zero;
		this.CleanupChangesDuration = TimeSpan.Zero;
		this.LoadContoursDuration = TimeSpan.Zero;
		this.ContouringDuration = TimeSpan.Zero;
		this.RecordChangesDuration = TimeSpan.Zero;
		this.CopyResyncDuration = TimeSpan.Zero;
		this.VerifyResyncDuration = TimeSpan.Zero;
		this.PersistenceDuration = TimeSpan.Zero;
		this.CompleteJobDuration = TimeSpan.Zero;
		this.VerifyResync = false;
		this.ResyncSuccessful = false;
		this.VolumesChanged = 0;
		this.VoxelsChanged = 0;
		this.VoxelChunksChanged = 0;
		this.ContourChunksChanged = 0;
		this.SubchunksFullyContoured = 0;
		this.SubchunksPartiallyContoured = 0;
		this.ChangeRecordsRecorded = 0;
		this.RunJobException = null;
		this.EngineException = null;
		this.CompleteJobException = null;
	}

	public VoxelJobDiagnostics GetSnapshot() => new VoxelJobDiagnostics(this);
}
