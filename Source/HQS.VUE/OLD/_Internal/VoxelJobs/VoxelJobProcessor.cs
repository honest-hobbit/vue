﻿namespace HQS.VUE.OLD;

internal class VoxelJobProcessor : IVoxelJobProcessorOLD
{
	private readonly EngineValidator validator;

	private readonly ICommandSubmitter<RunVoxelJobCommand.Args> submitter;

	public VoxelJobProcessor(EngineValidator validator, ICommandSubmitter<RunVoxelJobCommand.Args> submitter)
	{
		Debug.Assert(validator != null);
		Debug.Assert(submitter != null);

		this.validator = validator;
		this.submitter = submitter;
	}

	/// <inheritdoc />
	public void Submit(IVoxelJob job) => this.Submit(Pin.WithoutPool(job));

	/// <inheritdoc />
	public void Submit<TJob>(Pinned<TJob> job)
		where TJob : class, IVoxelJob => this.SubmitJobNoCompletion(job.CastTo<IVoxelJob>());

	/// <inheritdoc />
	public void Submit<TJob>(Pin<TJob> job)
		where TJob : class, IVoxelJob
	{
		using (job)
		{
			this.SubmitJobNoCompletion(job.CastTo<IVoxelJob>().AsPinned);
		}
	}

	/// <inheritdoc />
	public async Task<VoxelJobResults<IVoxelJob>> SubmitAsync(IVoxelJob job)
	{
		using var pooled = Pin.WithoutPool(job);
		var results = await this.SubmitJobWithCompletion(pooled.AsPinned).DontMarshallContext();
		return new VoxelJobResults<IVoxelJob>(job, results);
	}

	/// <inheritdoc />
	public async Task<VoxelJobResults<Pin<TJob>>> SubmitAsync<TJob>(Pinned<TJob> job)
		where TJob : class, IVoxelJob
	{
		var resultJob = job.CreatePin();
		try
		{
			var results = await this.SubmitJobWithCompletion(job.CastTo<IVoxelJob>()).DontMarshallContext();
			return new VoxelJobResults<Pin<TJob>>(resultJob, results);
		}
		catch (Exception)
		{
			resultJob.Dispose();
			throw;
		}
	}

	/// <inheritdoc />
	public async Task<VoxelJobResults<Pin<TJob>>> SubmitAsync<TJob>(Pin<TJob> job)
		where TJob : class, IVoxelJob
	{
		using (job)
		{
			return await this.SubmitAsync(job.AsPinned).DontMarshallContext();
		}
	}

	private void SubmitJobNoCompletion(Pinned<IVoxelJob> job)
	{
		this.ValidateJob(job);

		this.submitter.Submit(new RunVoxelJobCommand.Args()
		{
			Job = job,
		});
	}

	private Task<VoxelJobDiagnostics> SubmitJobWithCompletion(Pinned<IVoxelJob> job)
	{
		this.ValidateJob(job);

		// TaskCreationOptions.RunContinuationsAsynchronously
		var completion = new TaskCompletionSource<VoxelJobDiagnostics>();

		this.submitter.Submit(new RunVoxelJobCommand.Args()
		{
			Job = job,
			Completion = completion,
		});

		return completion.Task;
	}

	private void ValidateJob(Pinned<IVoxelJob> job)
	{
		this.validator.ValidateEngine();

		if (!job.IsPinned)
		{
			throw new ArgumentException(
				$"Pooled voxel job must be assigned and pinned.", nameof(job));
		}

		if (job.Value == null)
		{
			throw new ArgumentNullException(nameof(job));
		}

		if (job.Value.ReadVolumes.IsEmpty() && job.Value.WriteVolumes.IsEmpty())
		{
			throw new ArgumentException(
				$"Voxel job must contain at least one {nameof(Volume)} to read or write.", nameof(job));
		}
	}
}
