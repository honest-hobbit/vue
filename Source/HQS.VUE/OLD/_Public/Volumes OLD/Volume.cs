﻿namespace HQS.VUE.OLD;

// Volume is split into 3 parts. Key and Created are the immutable part.
// Name, Autosave, and Store are the part changed by the Unity main thread.
// LastModified and Version are the last part and is updated by the voxel engine as jobs complete.
public class Volume : IKeyed<Guid>
{
	private readonly IVolumeServices services;

	private readonly VoxelGridConfig config;

	private readonly VoxelGridStores stores;

	private readonly VoxelGridStats stats;

	internal Volume(IVolumeServices services, Guid key)
		: this(services, new VoxelGridRecord(key, DateTime.UtcNow))
	{
		this.config.SetUnsaved();
	}

	internal Volume(IVolumeServices services, VoxelGridRecord record)
	{
		Debug.Assert(services != null);
		Debug.Assert(record.IsValid);

		this.services = services;
		this.config = new VoxelGridConfig(services.Validator, record);
		this.stores = new VoxelGridStores(services.Validator);
		this.stats = new VoxelGridStats(services.Validator, record);
		this.EngineData = new VoxelGridEngineData(record);
		this.Key = record.Key;
		this.Created = record.Created;
		this.Instances = services.CreateInstances(this);
	}

	/// <inheritdoc />
	public Guid Key { get; }

	public DateTime Created { get; }

	public DateTime LastModified
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return this.stats.LastModified;
		}
	}

	public long Version
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return this.stats.Version;
		}
	}

	public string Name
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return this.config.Name;
		}

		set
		{
			this.services.Validator.ValidateEngine();

			this.config.Name = value;
		}
	}

	public bool Autosave
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return this.config.Autosave;
		}

		set
		{
			this.services.Validator.ValidateEngine();

			this.config.Autosave = value;
		}
	}

	public bool HasUnsavedChanges
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return this.stats.HasUnsavedChanges || this.config.HasUnsavedConfig;
		}
	}

	public bool HasVoxelGridStore
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return this.stores.HasVoxelGridStore;
		}
	}

	public bool HasVoxelShapeStore
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return this.stores.HasVoxelShapeStore;
		}
	}

	public bool HasSavePending
	{
		get
		{
			this.services.Validator.ValidateEngine();

			return (this.stats.HasUnsavedChanges || this.config.HasUnsavedConfig)
				&& (this.stores.HasVoxelGridStore || this.stores.HasVoxelShapeStore);
		}
	}

	public IVolumeInstances Instances { get; }

	internal VoxelGridEngineData EngineData { get; }

	public void SetVoxelGridStore(IVoxelGridStore store)
	{
		this.services.Validator.ValidateEngine();

		this.stores.SetVoxelGridStore(store);
	}

	public void SetVoxelShapeStore(IVoxelShapeStore store)
	{
		this.services.Validator.ValidateEngine();

		this.stores.SetVoxelShapeStore(store);
	}

	public void ContourEntireVolume()
	{
		this.services.Validator.ValidateEngine();

		this.services.Contour.Submit(new ContourVolumeCommand.Args() { Volume = this });
	}

	public void SaveChanges()
	{
		// this will call ValidateEngine already
		if (!this.HasSavePending)
		{
			return;
		}

		this.services.Save.Submit(new SaveVolumeCommand.Args() { Volume = this });
	}

	////public void DiscardChanges()
	////{
	////	this.parent.ValidateUniverse();

	////	// TODO need to implement this, might be able to just change the version number?
	////}

	internal VoxelGridRunJobInfo GetRunJobInfo()
	{
		Debug.Assert(this.services.Validator.IsValid);

		VoxelGridRunJobInfo info = default;
		this.config.GetInfo(ref info);
		this.stores.GetInfo(ref info);
		return info;
	}

	internal void SetChangedInfo(VoxelGridChangedInfo info)
	{
		Debug.Assert(this.services.Validator.IsValid);

		this.stats.SetChangedInfo(info);
	}

	internal void SetSavedInfo(VoxelGridSavedInfo info)
	{
		Debug.Assert(this.services.Validator.IsValid);

		this.config.SetSavedInfo(info);
		this.stats.SetSavedInfo(info);
	}
}
