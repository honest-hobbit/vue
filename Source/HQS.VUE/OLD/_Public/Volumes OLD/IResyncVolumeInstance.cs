﻿namespace HQS.VUE.OLD;

public interface IResyncVolumeInstance
{
	bool IsResyncApplied { get; }

	bool IsFinished { get; }

	Pinned<IVoxelJobInfo> JobCompleted { get; }

	void ApplyResync();

	// it is ok to call Finished without ever having called ApplyResync
	// for example, when deinitializing an AnimateVoxelChanges during its animation
	// Finished must be called eventually.
	void Finished();
}
