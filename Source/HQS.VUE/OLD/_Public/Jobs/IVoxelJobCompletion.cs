﻿namespace HQS.VUE.OLD;

public interface IVoxelJobCompletion : IVoxelJob
{
	// this method will only ever be called on the host application main thread
	// and is guaranteed to be called regardless of whether the job threw an exception
	// or if the changes were rejected by the host application
	void Complete(VoxelJobDiagnostics results);
}
