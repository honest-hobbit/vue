﻿namespace HQS.VUE.OLD;

public readonly struct Voxel : IEquatable<Voxel>
{
	public Voxel(ushort typeIndex)
	{
		this.TypeIndex = typeIndex;
	}

	public static Voxel Empty => default;

	public ushort TypeIndex { get; }

	// this is slower :(
	////public bool IsEmpty => this.TypeIndex == 0;

	public static bool operator ==(Voxel lhs, Voxel rhs) => lhs.Equals(rhs);

	public static bool operator !=(Voxel lhs, Voxel rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(Voxel other) => this.TypeIndex == other.TypeIndex;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.TypeIndex.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.TypeIndex.ToString();
}
