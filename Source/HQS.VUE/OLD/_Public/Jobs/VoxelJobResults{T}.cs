﻿namespace HQS.VUE.OLD;

public readonly struct VoxelJobResults<T>
{
	internal VoxelJobResults(T job, VoxelJobDiagnostics diagnostics)
	{
		this.Job = job;
		this.Diagnostics = diagnostics;
	}

	public T Job { get; }

	public VoxelJobDiagnostics Diagnostics { get; }
}
