﻿namespace HQS.VUE.OLD;

public readonly struct VoxelJobDiagnostics
{
	internal VoxelJobDiagnostics(VoxelJobDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(diagnostics != null);

		this.JobType = diagnostics.JobType;
		this.DateRan = diagnostics.DateRan;
		this.RunJobDuration = diagnostics.RunJobDuration;
		this.CleanupChangesDuration = diagnostics.CleanupChangesDuration;
		this.LoadContoursDuration = diagnostics.LoadContoursDuration;
		this.ContouringDuration = diagnostics.ContouringDuration;
		this.RecordChangesDuration = diagnostics.RecordChangesDuration;
		this.CopyResyncDuration = diagnostics.CopyResyncDuration;
		this.VerifyResyncDuration = diagnostics.VerifyResyncDuration;
		this.PersistenceDuration = diagnostics.PersistenceDuration;
		this.CompleteJobDuration = diagnostics.CompleteJobDuration;
		this.VerifyResync = diagnostics.VerifyResync;
		this.ResyncSuccessful = diagnostics.ResyncSuccessful;
		this.VolumesChanged = diagnostics.VolumesChanged;
		this.VoxelsChanged = diagnostics.VoxelsChanged;
		this.VoxelChunksChanged = diagnostics.VoxelChunksChanged;
		this.ContourChunksChanged = diagnostics.ContourChunksChanged;
		this.SubchunksFullyContoured = diagnostics.SubchunksFullyContoured;
		this.SubchunksPartiallyContoured = diagnostics.SubchunksPartiallyContoured;
		this.ChangeRecordsRecorded = diagnostics.ChangeRecordsRecorded;
		this.RunJobException = diagnostics.RunJobException;
		this.EngineException = diagnostics.EngineException;
		this.CompleteJobException = diagnostics.CompleteJobException;
	}

	public static int AverageStringLength => 800;

	public Type JobType { get; }

	public DateTime DateRan { get; }

	public TimeSpan RunJobDuration { get; }

	public TimeSpan CleanupChangesDuration { get; }

	public TimeSpan LoadContoursDuration { get; }

	public TimeSpan ContouringDuration { get; }

	public TimeSpan RecordChangesDuration { get; }

	public TimeSpan CopyResyncDuration { get; }

	public TimeSpan VerifyResyncDuration { get; }

	public TimeSpan PersistenceDuration { get; }

	public TimeSpan CompleteJobDuration { get; }

	public bool VerifyResync { get; }

	public bool ResyncSuccessful { get; }

	public int VolumesChanged { get; }

	public int VoxelsChanged { get; }

	public int VoxelChunksChanged { get; }

	public int ContourChunksChanged { get; }

	public int SubchunksFullyContoured { get; }

	public int SubchunksPartiallyContoured { get; }

	public int ChangeRecordsRecorded { get; }

	public Exception RunJobException { get; }

	public Exception EngineException { get; }

	public Exception CompleteJobException { get; }

	public override string ToString()
	{
		var builder = new StringBuilder(AverageStringLength);
		this.AddToBuilder(builder);
		return builder.ToString();
	}

	public void AddToBuilder(StringBuilder builder)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		builder.AppendLine($"{nameof(this.JobType)}: {this.JobType}");
		builder.AppendLine($"{nameof(this.DateRan)}: {this.DateRan}");
		builder.AppendLine($"{nameof(this.RunJobDuration)}: {this.RunJobDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.CleanupChangesDuration)}: {this.CleanupChangesDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.LoadContoursDuration)}: {this.LoadContoursDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.ContouringDuration)}: {this.ContouringDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.RecordChangesDuration)}: {this.RecordChangesDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.CopyResyncDuration)}: {this.CopyResyncDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.VerifyResyncDuration)}: {this.VerifyResyncDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.PersistenceDuration)}: {this.PersistenceDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.CompleteJobDuration)}: {this.CompleteJobDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.VerifyResync)}: {this.VerifyResync}");
		builder.AppendLine($"{nameof(this.ResyncSuccessful)}: {this.ResyncSuccessful}");
		builder.AppendLine($"{nameof(this.VolumesChanged)}: {this.VolumesChanged:n0}");
		builder.AppendLine($"{nameof(this.VoxelsChanged)}: {this.VoxelsChanged:n0}");
		builder.AppendLine($"{nameof(this.VoxelChunksChanged)}: {this.VoxelChunksChanged:n0}");
		builder.AppendLine($"{nameof(this.ContourChunksChanged)}: {this.ContourChunksChanged:n0}");
		builder.AppendLine($"{nameof(this.SubchunksFullyContoured)}: {this.SubchunksFullyContoured:n0}");
		builder.AppendLine($"{nameof(this.SubchunksPartiallyContoured)}: {this.SubchunksPartiallyContoured:n0}");
		builder.AppendLine($"{nameof(this.ChangeRecordsRecorded)}: {this.ChangeRecordsRecorded:n0}");
		builder.AppendLine($"{nameof(this.RunJobException)}: {this.RunJobException?.ToString() ?? "null"}");
		builder.AppendLine($"{nameof(this.EngineException)}: {this.EngineException?.ToString() ?? "null"}");
		builder.AppendLine($"{nameof(this.CompleteJobException)}: {this.CompleteJobException?.ToString() ?? "null"}");
	}
}
