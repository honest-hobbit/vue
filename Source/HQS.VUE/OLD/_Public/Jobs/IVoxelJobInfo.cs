﻿namespace HQS.VUE.OLD;

public interface IVoxelJobInfo
{
	IEnumerable<Volume> ReadVolumes { get; }

	IEnumerable<Volume> WriteVolumes { get; }

	bool CheckCollision { get; }
}
