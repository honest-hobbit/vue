﻿namespace HQS.VUE.OLD;

public interface IVoxelReadView : IVoxelView
{
	Voxel this[Int3 index] { get; }

	Voxel this[int x, int y, int z] { get; }
}
