﻿namespace HQS.VUE.OLD;

public interface IVoxelVolumesView
{
	IVoxelDefinitions Definitions { get; }

	IReadOnlyDictionary<Guid, IVoxelReadView> ReadVolumes { get; }

	IReadOnlyDictionary<Guid, IVoxelWriteView> WriteVolumes { get; }

	void DiscardAllChanges();
}
