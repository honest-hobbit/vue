﻿namespace HQS.VUE.OLD;

public interface IVoxelView : IKeyed<Guid>
{
	IVoxelDefinitions Definitions { get; }
}
