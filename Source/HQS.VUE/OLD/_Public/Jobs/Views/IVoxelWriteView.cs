﻿namespace HQS.VUE.OLD;

public interface IVoxelWriteView : IVoxelView
{
	IVoxelReadView Read { get; }

	Voxel this[Int3 index] { get; set; }

	Voxel this[int x, int y, int z] { get; set; }

	Voxel Replace(Int3 index, Voxel voxel);

	Voxel Replace(int x, int y, int z, Voxel voxel);

	void DiscardChanges();
}
