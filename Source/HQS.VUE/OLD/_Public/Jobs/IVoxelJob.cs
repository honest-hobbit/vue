﻿namespace HQS.VUE.OLD;

public interface IVoxelJob : IVoxelJobInfo
{
	void RunOn(IVoxelVolumesView volumesView);
}
