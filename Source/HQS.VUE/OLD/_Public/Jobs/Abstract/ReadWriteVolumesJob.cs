﻿namespace HQS.VUE.OLD;

public abstract class ReadWriteVolumesJob : IVoxelJob
{
	private readonly SingleVolume readVolume = new SingleVolume();

	private readonly SingleVolume writeVolume = new SingleVolume();

	public Volume ReadVolume
	{
		get => this.readVolume.Volume;
		set => this.readVolume.Volume = value;
	}

	public Volume WriteVolume
	{
		get => this.writeVolume.Volume;
		set => this.writeVolume.Volume = value;
	}

	/// <inheritdoc />
	public IEnumerable<Volume> ReadVolumes => this.readVolume;

	/// <inheritdoc />
	public IEnumerable<Volume> WriteVolumes => this.writeVolume;

	/// <inheritdoc />
	public bool CheckCollision { get; set; } = true;

	public void Clear()
	{
		this.OnCleared();
		this.ReadVolume = null;
		this.WriteVolume = null;
	}

	/// <inheritdoc />
	void IVoxelJob.RunOn(IVoxelVolumesView volumesView)
	{
		Ensure.That(volumesView, nameof(volumesView)).IsNotNull();
		Ensure.That(this.ReadVolume, nameof(this.ReadVolume)).IsNotNull();
		Ensure.That(this.WriteVolume, nameof(this.WriteVolume)).IsNotNull();

		var readVolume = volumesView.ReadVolumes[this.ReadVolume.Key];
		var writeVolume = volumesView.WriteVolumes[this.WriteVolume.Key];
		this.Run(readVolume, writeVolume);
	}

	protected abstract void Run(IVoxelReadView readVolume, IVoxelWriteView writeVolume);

	protected virtual void OnCleared()
	{
	}
}
