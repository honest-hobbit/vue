﻿namespace HQS.VUE.OLD;

public abstract class ReadSingleVolumeJob : SingleVolumeJob
{
	/// <inheritdoc />
	public sealed override IEnumerable<Volume> ReadVolumes => this.GetVolumes();

	/// <inheritdoc />
	public sealed override IEnumerable<Volume> WriteVolumes { get; } = Enumerable.Empty<Volume>();

	/// <inheritdoc />
	protected sealed override void Run(IVoxelVolumesView volumesView)
	{
		var volume = volumesView.ReadVolumes[this.Volume.Key];
		this.Run(volume);
	}

	protected abstract void Run(IVoxelReadView volume);
}
