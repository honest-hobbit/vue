﻿namespace HQS.VUE.OLD;

public abstract class SingleVolumeJob : IVoxelJob
{
	private readonly SingleVolume volume = new SingleVolume();

	public Volume Volume
	{
		get => this.volume.Volume;
		set => this.volume.Volume = value;
	}

	/// <inheritdoc />
	public abstract IEnumerable<Volume> ReadVolumes { get; }

	/// <inheritdoc />
	public abstract IEnumerable<Volume> WriteVolumes { get; }

	/// <inheritdoc />
	public bool CheckCollision { get; set; } = true;

	/// <inheritdoc />
	void IVoxelJob.RunOn(IVoxelVolumesView volumesView)
	{
		Ensure.That(volumesView, nameof(volumesView)).IsNotNull();
		Ensure.That(this.Volume, nameof(this.Volume)).IsNotNull();

		this.Run(volumesView);
	}

	public void Clear()
	{
		this.OnCleared();
		this.Volume = null;
	}

	protected abstract void Run(IVoxelVolumesView volumesView);

	protected virtual void OnCleared()
	{
	}

	protected IEnumerable<Volume> GetVolumes() => this.volume;
}
