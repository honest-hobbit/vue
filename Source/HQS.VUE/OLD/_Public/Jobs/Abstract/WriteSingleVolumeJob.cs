﻿namespace HQS.VUE.OLD;

public abstract class WriteSingleVolumeJob : SingleVolumeJob
{
	/// <inheritdoc />
	public sealed override IEnumerable<Volume> ReadVolumes { get; } = Enumerable.Empty<Volume>();

	/// <inheritdoc />
	public sealed override IEnumerable<Volume> WriteVolumes => this.GetVolumes();

	/// <inheritdoc />
	protected sealed override void Run(IVoxelVolumesView volumesView)
	{
		var volume = volumesView.WriteVolumes[this.Volume.Key];
		this.Run(volume);
	}

	protected abstract void Run(IVoxelWriteView volume);
}
