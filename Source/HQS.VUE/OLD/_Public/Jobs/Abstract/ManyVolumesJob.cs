﻿namespace HQS.VUE.OLD;

public abstract class ManyVolumesJob : IVoxelJob
{
	private readonly List<Volume> readVolumes = new List<Volume>();

	private readonly List<Volume> writeVolumes = new List<Volume>();

	/// <inheritdoc />
	public IEnumerable<Volume> ReadVolumes => this.readVolumes;

	/// <inheritdoc />
	public IEnumerable<Volume> WriteVolumes => this.writeVolumes;

	/// <inheritdoc />
	public bool CheckCollision { get; set; } = true;

	public void AddReadVolume(Volume volume)
	{
		Ensure.That(volume, nameof(volume)).IsNotNull();

		this.readVolumes.Add(volume);
	}

	public void AddWriteVolume(Volume volume)
	{
		Ensure.That(volume, nameof(volume)).IsNotNull();

		this.writeVolumes.Add(volume);
	}

	/// <inheritdoc />
	void IVoxelJob.RunOn(IVoxelVolumesView volumesView)
	{
		Ensure.That(volumesView, nameof(volumesView)).IsNotNull();

		this.Run(volumesView);
	}

	public void Clear()
	{
		this.OnCleared();
		this.readVolumes.Clear();
		this.writeVolumes.Clear();
	}

	protected abstract void Run(IVoxelVolumesView volumesView);

	protected virtual void OnCleared()
	{
	}
}
