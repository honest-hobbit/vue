﻿namespace HQS.VUE.OLD;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
public struct Adjacent<T> : IEquatable<Adjacent<T>>
{
	public Adjacent(T value)
		: this(value, value, value, value, value, value)
	{
	}

	public Adjacent(T negX, T posX, T negY, T posY, T negZ, T posZ)
	{
		this.NegX = negX;
		this.PosX = posX;
		this.NegY = negY;
		this.PosY = posY;
		this.NegZ = negZ;
		this.PosZ = posZ;
	}

	public T NegX;

	public T PosX;

	public T NegY;

	public T PosY;

	public T NegZ;

	public T PosZ;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(Adjacent<T> other) =>
		this.NegX.Equals(other.NegX) &&
		this.PosX.Equals(other.PosX) &&
		this.NegY.Equals(other.NegY) &&
		this.PosY.Equals(other.PosY) &&
		this.NegZ.Equals(other.NegZ) &&
		this.PosZ.Equals(other.PosZ);

	/// <inheritdoc />
	public override int GetHashCode() =>
		HashCode.Combine(this.NegX, this.PosX, this.NegY, this.PosY, this.NegZ, this.PosZ);

	public static bool operator ==(Adjacent<T> left, Adjacent<T> right) => left.Equals(right);

	public static bool operator !=(Adjacent<T> left, Adjacent<T> right) => !(left == right);
}
