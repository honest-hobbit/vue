﻿namespace HQS.VUE.OLD;

/// <summary>
/// This is a test summary of IVoxelUniverseOLD.
/// </summary>
public interface IVoxelUniverseOLD : IAsyncCompletable
{
	IDiagnostics Diagnostics { get; }

	IVoxelSizeConfig SizeConfig { get; }

	IVoxelDefinitions Definitions { get; }

	IVolumes Volumes { get; }

	IVoxelJobProcessorOLD Jobs { get; }
}
