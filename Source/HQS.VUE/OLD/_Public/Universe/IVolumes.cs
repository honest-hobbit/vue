﻿namespace HQS.VUE.OLD;

public interface IVolumes
{
	IReadOnlyDictionary<Guid, Volume> Collection { get; }

	Volume CreateVolume();

	// this should only be used to create a VoxelGrid from a saved file
	// do not create a brand new  empty VoxelGrid using this method
	Volume CreateVolume(VoxelGridRecord record);

	Volume CreateVolume(Guid key);

	Volume GetOrCreateVolume(Guid key);
}
