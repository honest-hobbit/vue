﻿namespace HQS.VUE.OLD;

public static class PoolingExtensions
{
	public static void PopulateVoxelChunks(this IVoxelUniverseOLD universe, int chunks, double percentUniform = .5)
	{
		CalculatePopulateCounts(universe, chunks, percentUniform, out int subchunkArrays, out int subchunks);

		var pools = universe.Diagnostics.Pools;
		pools[PooledTypesOLD.Chunks.VoxelChunk].Create(chunks);
		pools[PooledTypesOLD.Chunks.VoxelSubchunkArray].Create(subchunkArrays);
		pools[PooledTypesOLD.Chunks.VoxelSubchunk].Create(subchunks);
	}

	public static void PopulateContourChunks(this IVoxelUniverseOLD universe, int chunks, double percentUniform = .5)
	{
		CalculatePopulateCounts(universe, chunks, percentUniform, out int subchunkArrays, out int subchunks);

		var pools = universe.Diagnostics.Pools;
		pools[PooledTypesOLD.Chunks.ShapeChunk].Create(chunks);
		pools[PooledTypesOLD.Chunks.ShapeSubchunkArray].Create(subchunkArrays);
		pools[PooledTypesOLD.Chunks.ShapeSubchunk].Create(subchunks);
	}

	private static void CalculatePopulateCounts(
		IVoxelUniverseOLD universe, int chunks, double percentUniform, out int subchunkArrays, out int subchunks)
	{
		Ensure.That(universe, nameof(universe)).IsNotNull();
		Ensure.That(chunks, nameof(chunks)).IsGte(0);
		Ensure.That(percentUniform, nameof(percentUniform)).IsInRange(0, 1);

		double percentNonuniform = (1 - percentUniform).ClampLower(0);

		subchunkArrays = (chunks * percentNonuniform)
			.Round(MidpointRounding.AwayFromZero).ClampLower(0);

		subchunks = (subchunkArrays * universe.SizeConfig.SubchunkIndexer.Length * percentNonuniform)
			.Round(MidpointRounding.AwayFromZero).ClampLower(0);
	}
}
