﻿namespace HQS.VUE.OLD;

public static class PooledTypesOLD
{
	public static IEnumerable<PooledType> EnumerateAll() =>
		Chunks.Enumerate()
		.Concat(Volumes.Enumerate())
		.Concat(VoxelJobs.Enumerate())
		.Concat(Contouring.Enumerate())
		.Concat(Persistence.Enumerate());

	public static class Chunks
	{
		public static PooledType VoxelChunk { get; } = new PooledType(
			new Guid("37582b18-abb7-4eae-bacd-9fa53516c1e3"),
			CreateName(nameof(VoxelChunk)),
			typeof(VoxelChunk));

		public static PooledType VoxelSubchunkArray { get; } = new PooledType(
			new Guid("8aac6696-5a1a-43f3-9403-9c8171e4f8b9"),
			CreateName(nameof(VoxelSubchunkArray)),
			typeof(VoxelSubchunk[]));

		public static PooledType VoxelSubchunk { get; } = new PooledType(
			new Guid("6ed91b3e-9f7b-4240-9014-ad581653f4a7"),
			CreateName(nameof(VoxelSubchunk)),
			typeof(Voxel[]));

		public static PooledType ShapeChunk { get; } = new PooledType(
			new Guid("1dd38f98-cbf2-4906-b2ef-4acccec92f0b"),
			CreateName(nameof(ShapeChunk)),
			typeof(ContourChunk));

		public static PooledType ShapeSubchunkArray { get; } = new PooledType(
			new Guid("491875e0-7039-461c-a069-f3eeb3ffb3ef"),
			CreateName(nameof(ShapeSubchunkArray)),
			typeof(ContourSubchunk[]));

		public static PooledType ShapeSubchunk { get; } = new PooledType(
			new Guid("0b62ee25-7472-4b79-8a06-0b317b83d2c2"),
			CreateName(nameof(ShapeSubchunk)),
			typeof(ContourSubchunk));

		public static IEnumerable<PooledType> Enumerate()
		{
			yield return VoxelChunk;
			yield return VoxelSubchunkArray;
			yield return VoxelSubchunk;

			yield return ShapeChunk;
			yield return ShapeSubchunkArray;
			yield return ShapeSubchunk;
		}

		private static string CreateName(string name) => $"{nameof(PooledType)}.{nameof(Chunks)}.{name}";
	}

	public static class Volumes
	{
		public static PooledType ContourVolumeCommand { get; } = new PooledType(
			new Guid("e72283b1-8b42-4878-8989-322b4b662b9a"),
			CreateName(nameof(ContourVolumeCommand)),
			typeof(ContourVolumeCommand));

		public static PooledType SaveVolumeCommand { get; } = new PooledType(
			new Guid("e6b356ed-1058-4ba6-b222-b4baeca9f540"),
			CreateName(nameof(SaveVolumeCommand)),
			typeof(SaveVolumeCommand));

		public static PooledType VoxelChunkPinSet { get; } = new PooledType(
			new Guid("8da7c1bf-5f09-420e-91f5-2721c97f698d"),
			CreateName(nameof(VoxelChunkPinSet)),
			typeof(PinSet<ReadOnlyVoxelChunk>));

		public static PooledType ContourChunkPinSet { get; } = new PooledType(
			new Guid("06239d90-d141-4626-8cb6-c29cc1c5f993"),
			CreateName(nameof(ContourChunkPinSet)),
			typeof(PinSet<ReadOnlyContourChunk>));

		public static IEnumerable<PooledType> Enumerate()
		{
			yield return ContourVolumeCommand;
			yield return SaveVolumeCommand;
			yield return VoxelChunkPinSet;
			yield return ContourChunkPinSet;
		}

		private static string CreateName(string name) => $"{nameof(PooledType)}.{nameof(Volumes)}.{name}";
	}

	public static class VoxelJobs
	{
		public static PooledType RunVoxelJobCommand { get; } = new PooledType(
			new Guid("a5186aa9-f4eb-4d2f-970c-880a8fa9e17b"),
			CreateName(nameof(RunVoxelJobCommand)),
			typeof(RunVoxelJobCommand));

		public static PooledType CompleteVoxelJobCommand { get; } = new PooledType(
			new Guid("f2407bfd-83ca-4010-a009-17ff4256c17d"),
			CreateName(nameof(CompleteVoxelJobCommand)),
			typeof(CompleteVoxelJobCommand));

		public static PooledType RecordChangesCommand { get; } = new PooledType(
			new Guid("710c26ca-c22a-4816-9e5b-823a4908c7ff"),
			CreateName(nameof(RecordChangesCommand)),
			typeof(RecordChangesCommand));

		public static PooledType VoxelView { get; } = new PooledType(
			new Guid("4fd9018d-2236-465c-82e8-d73d3107833b"),
			CreateName(nameof(VoxelView)),
			typeof(VoxelView));

		public static PooledType ModifiedVoxelChunk { get; } = new PooledType(
			new Guid("72467fe3-d658-497a-87b0-3a0ad2a42447"),
			CreateName(nameof(ModifiedVoxelChunk)),
			typeof(ModifiedVoxelChunk));

		public static IEnumerable<PooledType> Enumerate()
		{
			yield return RunVoxelJobCommand;
			yield return CompleteVoxelJobCommand;
			yield return RecordChangesCommand;
			yield return VoxelView;
			yield return ModifiedVoxelChunk;
		}

		private static string CreateName(string name) => $"{nameof(PooledType)}.{nameof(VoxelJobs)}.{name}";
	}

	public static class Contouring
	{
		public static PooledType ContouringChunkData { get; } = new PooledType(
			new Guid("a0d64430-8c75-43d2-9dd3-01a51dbf741a"),
			CreateName(nameof(ContouringChunkData)),
			typeof(ContourChunkData));

		public static PooledType ContourSubchunkCommand { get; } = new PooledType(
			new Guid("cad9cac4-aee2-4ab7-8b17-8ac90b01d5be"),
			CreateName(nameof(ContourSubchunkCommand)),
			typeof(ContourSubchunkCommand));

		public static IEnumerable<PooledType> Enumerate()
		{
			yield return ContouringChunkData;
			yield return ContourSubchunkCommand;
		}

		private static string CreateName(string name) => $"{nameof(PooledType)}.{nameof(Contouring)}.{name}";
	}

	public static class Persistence
	{
		public static PooledType ByteBuffer { get; } = new PooledType(
			new Guid("7769e2d8-d34e-495b-8e3f-6557949c79f7"),
			CreateName(nameof(ByteBuffer)),
			typeof(IByteBuffer));

		public static PooledType LoadVoxelChunkCommand { get; } = new PooledType(
			new Guid("1f1cc649-2303-4a97-ab97-a54f30447d1d"),
			CreateName(nameof(LoadVoxelChunkCommand)),
			typeof(LoadVoxelChunkCommand));

		public static PooledType LoadContourChunkCommand { get; } = new PooledType(
			new Guid("9fd1a443-a50f-47bb-a272-d8765827e38f"),
			CreateName(nameof(LoadContourChunkCommand)),
			typeof(LoadContourChunkCommand));

		public static PooledType SerializeVoxelChunkCommand { get; } = new PooledType(
			new Guid("be5287f6-8cfc-4a87-bca9-4ca925c1e03e"),
			CreateName(nameof(SerializeVoxelChunkCommand)),
			typeof(SerializeVoxelChunkCommand));

		public static PooledType SerializeContourChunkCommand { get; } = new PooledType(
			new Guid("b43ff029-d577-4390-a5b8-cbab3fcb0fe5"),
			CreateName(nameof(SerializeContourChunkCommand)),
			typeof(SerializeContourChunkCommand));

		public static PooledType SaveVoxelGridCommand { get; } = new PooledType(
			new Guid("4580a96e-b81d-47a8-be41-2fa0d4deb874"),
			CreateName(nameof(SaveVoxelGridCommand)),
			typeof(SaveVoxelGridCommand));

		public static PooledType SaveVoxelShapeCommand { get; } = new PooledType(
			new Guid("db78fb96-0124-4901-be79-c2b6c72a0d5f"),
			CreateName(nameof(SaveVoxelShapeCommand)),
			typeof(SaveVoxelShapeCommand));

		public static PooledType SaveChunkGridSucceededCommand { get; } = new PooledType(
			new Guid("a2570d2b-8ec4-4195-a4c7-e2921f921dd6"),
			CreateName(nameof(SaveChunkGridSucceededCommand)),
			typeof(SaveChunkGridSucceededCommand));

		public static PooledType SaveChunkGridFailedCommand { get; } = new PooledType(
			new Guid("0983d5fa-1175-4135-ad9e-fdb92d29f4b4"),
			CreateName(nameof(SaveChunkGridFailedCommand)),
			typeof(SaveChunkGridFailedCommand));

		public static IEnumerable<PooledType> Enumerate()
		{
			yield return ByteBuffer;

			yield return LoadVoxelChunkCommand;
			yield return LoadContourChunkCommand;

			yield return SerializeVoxelChunkCommand;
			yield return SerializeContourChunkCommand;

			yield return SaveVoxelGridCommand;
			yield return SaveVoxelShapeCommand;
			yield return SaveChunkGridSucceededCommand;
			yield return SaveChunkGridFailedCommand;
		}

		private static string CreateName(string name) => $"{nameof(PooledType)}.{nameof(Persistence)}.{name}";
	}
}
