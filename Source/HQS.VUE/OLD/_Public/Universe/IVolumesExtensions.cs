﻿namespace HQS.VUE.OLD;

public static class IVolumesExtensions
{
	public static void LoadVolumesFrom(this IVolumes volumes, IVoxelGridStore store)
	{
		Ensure.That(volumes, nameof(volumes)).IsNotNull();
		Ensure.That(store, nameof(store)).IsNotNull();

		foreach (var record in store.GetAllChunkGrids())
		{
			if (!volumes.Collection.TryGetValue(record.Key, out var volume))
			{
				volume = volumes.CreateVolume(record);
			}

			if (!volume.HasVoxelGridStore)
			{
				volume.SetVoxelGridStore(store);
			}
		}
	}

	public static void AttachStoreToLoadedVolumes(this IVolumes volumes, IVoxelShapeStore store)
	{
		Ensure.That(volumes, nameof(volumes)).IsNotNull();
		Ensure.That(store, nameof(store)).IsNotNull();

		foreach (var record in store.GetAllChunkGrids())
		{
			if (volumes.Collection.TryGetValue(record.Key, out var volume) && !volume.HasVoxelShapeStore)
			{
				volume.SetVoxelShapeStore(store);
			}
		}
	}
}
