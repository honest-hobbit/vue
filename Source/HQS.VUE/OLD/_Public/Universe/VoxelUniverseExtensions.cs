﻿namespace HQS.VUE.OLD;

public static class VoxelUniverseExtensions
{
	public static Task<VoxelJobResults<IVoxelJob>> SubmitAndUpdateAsync(
		this VoxelUniverseOLD universe, IVoxelJob job)
	{
		Ensure.That(universe, nameof(universe)).IsNotNull();

		return UpdateUntilCompleteAsync(universe, universe.Jobs.SubmitAsync(job));
	}

	public static Task<VoxelJobResults<Pin<TJob>>> SubmitAndUpdateAsync<TJob>(
		this VoxelUniverseOLD universe, Pinned<TJob> job)
		where TJob : class, IVoxelJob
	{
		Ensure.That(universe, nameof(universe)).IsNotNull();

		return UpdateUntilCompleteAsync(universe, universe.Jobs.SubmitAsync(job));
	}

	public static Task<VoxelJobResults<Pin<TJob>>> SubmitAndUpdateAsync<TJob>(
		this VoxelUniverseOLD universe, Pin<TJob> job)
		where TJob : class, IVoxelJob
	{
		Ensure.That(universe, nameof(universe)).IsNotNull();

		return UpdateUntilCompleteAsync(universe, universe.Jobs.SubmitAsync(job));
	}

	private static async Task<T> UpdateUntilCompleteAsync<T>(VoxelUniverseOLD universe, Task<T> task)
	{
		Debug.Assert(universe != null);
		Debug.Assert(task != null);

		universe.Update();
		while (!task.IsCompleted)
		{
			await Task.Delay(1).MarshallContext();
			universe.Update();
		}

		return await task.MarshallContext();
	}
}
