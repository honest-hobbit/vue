﻿namespace HQS.VUE.OLD;

public interface IVoxelJobProcessorOLD
{
	void Submit(IVoxelJob job);

	void Submit<TJob>(Pinned<TJob> job)
		where TJob : class, IVoxelJob;

	// this will dispose the pin passed in
	void Submit<TJob>(Pin<TJob> job)
		where TJob : class, IVoxelJob;

	Task<VoxelJobResults<IVoxelJob>> SubmitAsync(IVoxelJob job);

	// the pin contained in the VoxelJobResults must be disposed by the caller after awaiting
	Task<VoxelJobResults<Pin<TJob>>> SubmitAsync<TJob>(Pinned<TJob> job)
		where TJob : class, IVoxelJob;

	// this will dispose the pin passed in
	// the pin contained in the VoxelJobResults must be disposed by the caller after awaiting
	Task<VoxelJobResults<Pin<TJob>>> SubmitAsync<TJob>(Pin<TJob> job)
		where TJob : class, IVoxelJob;
}
