﻿namespace HQS.VUE.OLD;

[SuppressMessage("Design", "CA1001", Justification = "AbstractCompletable handles disposal.")]
public class VoxelUniverseOLD : AbstractCompletable, IVoxelUniverseOLD
{
	private readonly Container container;

	private readonly VoxelEngineOLD engine;

	public VoxelUniverseOLD(UniverseConfig universeConfig, VoxelTypesConfig voxelTypes)
	{
		Ensure.That(universeConfig, nameof(universeConfig)).IsNotNull();
		Ensure.That(voxelTypes, nameof(voxelTypes)).IsNotNull();

		this.container = new Container();
		this.container.SetUpVoxelEngineOLD(universeConfig, voxelTypes);
		this.container.SetUpNoHostApplication();

		this.container.VerifyInDebugOnly();

		this.engine = this.container.GetInstance<VoxelEngineOLD>();
		this.Diagnostics = this.container.GetInstance<EngineDiagnostics>();
	}

	/// <inheritdoc />
	public IDiagnostics Diagnostics { get; }

	/// <inheritdoc />
	public IVoxelSizeConfig SizeConfig => this.engine.SizeConfig;

	/// <inheritdoc />
	public IVoxelDefinitions Definitions => this.engine.Definitions;

	/// <inheritdoc />
	public IVolumes Volumes => this.engine.Volumes;

	/// <inheritdoc />
	public IVoxelJobProcessorOLD Jobs => this.engine.Jobs;

	// returns how many commands were ran
	public int Update() => this.engine.Update();

	/// <inheritdoc />
	protected override async Task CompleteAsync()
	{
		try
		{
			this.engine.Complete();
			await this.engine.Completion.DontMarshallContext();
		}
		finally
		{
			this.container.Dispose();
		}
	}
}
