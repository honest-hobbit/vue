﻿namespace HQS.VUE.OLD;

public interface IVoxelSizeConfig
{
	CubeArrayIndexer SubchunkIndexer { get; }

	CubeArrayIndexer VoxelIndexer { get; }
}
