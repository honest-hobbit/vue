﻿namespace HQS.VUE.OLD;

public interface IDiagnostics
{
	IObservable<VoxelJobDiagnostics> JobCompleted { get; }

	IObservable<Exception> ErrorOccurred { get; }

	ITypePoolCollection Pools { get; }

	IChunkCache VoxelChunkCache { get; }

	IChunkCache ContourChunkCache { get; }
}
