﻿namespace HQS.VUE.OLD;

public interface IVoxelChangesRecord
{
	// checked after calling StartRecording and each PassFinished when either return true
	RecordChangesMode RecordMode { get; }

	// return true to start recording a pass, false to skip
	bool StartRecording(Pinned<IVoxelJobInfo> jobCompleted, int voxelsChanged);

	// return true to start another pass, false to stop
	bool PassFinished();

	void FinishedRecording();
}
