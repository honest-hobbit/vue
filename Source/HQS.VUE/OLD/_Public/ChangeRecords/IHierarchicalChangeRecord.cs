﻿namespace HQS.VUE.OLD;

public interface IHierarchicalChangeRecord : IVoxelChangesRecord
{
	void PassStarted(IVoxelDefinitions definitions);

	void NextChunk(Int3 index);

	void NextSubchunk(ushort index);

	// return true to keep recording, false to stop
	bool RecordChange(ushort index, Voxel oldVoxel, Voxel newVoxel);
}
