﻿namespace HQS.VUE.OLD;

public enum RecordChangesMode
{
	Granular,
	Chunked,
	Hierarchical,
}
