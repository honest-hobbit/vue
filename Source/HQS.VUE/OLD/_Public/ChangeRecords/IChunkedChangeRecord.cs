﻿namespace HQS.VUE.OLD;

public interface IChunkedChangeRecord : IVoxelChangesRecord
{
	// this is checked after calling PassStarted
	bool IncludeAdjacentVoxels { get; }

	void PassStarted(IVoxelChangesBlock block);

	// return true to keep recording, false to stop
	bool NextBlock();
}
