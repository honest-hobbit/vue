﻿namespace HQS.VUE.OLD;

public interface IVoxelChangesView : IKeyed<Guid>
{
	IVoxelDefinitions Definitions { get; }

	Voxel GetOldVoxel(Int3 index);

	Voxel GetOldVoxel(int x, int y, int z);

	Voxel GetNewVoxel(Int3 index);

	Voxel GetNewVoxel(int x, int y, int z);
}
