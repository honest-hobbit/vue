﻿namespace HQS.VUE.OLD;

public static class IVoxelChangesBlockExtensions
{
	public static bool GetOldVoxelIsSurface(this IVoxelChangesBlock block, int x, int y, int z)
	{
		Debug.Assert(block != null);

		return block.GetOldVoxelIsSurface(block.VoxelIndexer[x, y, z]);
	}

	public static bool GetOldVoxelIsSurface(this IVoxelChangesBlock block, Int3 index)
	{
		Debug.Assert(block != null);

		return block.GetOldVoxelIsSurface(block.VoxelIndexer[index]);
	}

	public static bool GetOldVoxelIsSurface(this IVoxelChangesBlock block, int index)
	{
		Debug.Assert(block != null);

		var voxelTypes = block.Definitions.Voxels;
		var adjacent = block.GetOldAdjacentVoxels(index);
		var surfaceType = voxelTypes[adjacent.Center].SurfaceType;
		return surfaceType != voxelTypes[adjacent.NegX].SurfaceType
			|| surfaceType != voxelTypes[adjacent.PosX].SurfaceType
			|| surfaceType != voxelTypes[adjacent.NegY].SurfaceType
			|| surfaceType != voxelTypes[adjacent.PosY].SurfaceType
			|| surfaceType != voxelTypes[adjacent.NegZ].SurfaceType
			|| surfaceType != voxelTypes[adjacent.PosZ].SurfaceType;
	}

	public static bool GetNewVoxelIsSurface(this IVoxelChangesBlock block, int x, int y, int z)
	{
		Debug.Assert(block != null);

		return block.GetNewVoxelIsSurface(block.VoxelIndexer[x, y, z]);
	}

	public static bool GetNewVoxelIsSurface(this IVoxelChangesBlock block, Int3 index)
	{
		Debug.Assert(block != null);

		return block.GetNewVoxelIsSurface(block.VoxelIndexer[index]);
	}

	public static bool GetNewVoxelIsSurface(this IVoxelChangesBlock block, int index)
	{
		Debug.Assert(block != null);

		var voxelTypes = block.Definitions.Voxels;
		var adjacent = block.GetNewAdjacentVoxels(index);
		var surfaceType = voxelTypes[adjacent.Center].SurfaceType;
		return surfaceType != voxelTypes[adjacent.NegX].SurfaceType
			|| surfaceType != voxelTypes[adjacent.PosX].SurfaceType
			|| surfaceType != voxelTypes[adjacent.NegY].SurfaceType
			|| surfaceType != voxelTypes[adjacent.PosY].SurfaceType
			|| surfaceType != voxelTypes[adjacent.NegZ].SurfaceType
			|| surfaceType != voxelTypes[adjacent.PosZ].SurfaceType;
	}
}
