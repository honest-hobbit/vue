﻿namespace HQS.VUE.OLD;

public interface IGranularChangeRecord : IVoxelChangesRecord
{
	void PassStarted(IVoxelChangesView view);

	// return true to keep recording, false to stop
	bool RecordChange(Int3 index, Voxel oldVoxel, Voxel newVoxel);
}
