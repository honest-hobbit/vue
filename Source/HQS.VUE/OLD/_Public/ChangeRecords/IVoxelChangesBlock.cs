﻿namespace HQS.VUE.OLD;

public interface IVoxelChangesBlock : IKeyed<Guid>
{
	IVoxelDefinitions Definitions { get; }

	CubeArrayIndexer VoxelIndexer { get; }

	IReadOnlyCubeArray<Voxel> OldVoxels { get; }

	IReadOnlyCubeArray<Voxel> NewVoxels { get; }

	Int3 VoxelIndexOffset { get; }

	bool HasAdjacentVoxels { get; }

	Int3 GetVoxelIndex(int index);

	Cross<Voxel> GetOldAdjacentVoxels(int x, int y, int z);

	Cross<Voxel> GetOldAdjacentVoxels(Int3 index);

	Cross<Voxel> GetOldAdjacentVoxels(int index);

	Cross<Voxel> GetNewAdjacentVoxels(int x, int y, int z);

	Cross<Voxel> GetNewAdjacentVoxels(Int3 index);

	Cross<Voxel> GetNewAdjacentVoxels(int index);
}
