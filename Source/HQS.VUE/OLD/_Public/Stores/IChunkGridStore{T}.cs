﻿namespace HQS.VUE.OLD;

public interface IChunkGridStore<T> : IChunkGridStore
{
	IEnumerable<T> GetAllChunkGrids();

	bool TryGetChunkGrid(Guid chunkGridKey, out T gridRecord);

	void UpdateChunks(T gridRecord, IReadOnlyList<ChunkRecord> chunkRecords);
}
