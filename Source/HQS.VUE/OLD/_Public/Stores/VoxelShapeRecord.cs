﻿namespace HQS.VUE.OLD;

public readonly struct VoxelShapeRecord : IKeyed<Guid>, IEquatable<VoxelShapeRecord>
{
	public VoxelShapeRecord(Guid key, DateTime created)
	{
		this.Key = key;
		this.Created = created;
		this.Name = string.Empty;
		this.Autosave = false;
		this.LastModified = created;
		this.Version = 0;
	}

	public VoxelShapeRecord(
		Guid key,
		DateTime created,
		string name,
		bool autosave,
		DateTime lastModified,
		long version)
	{
		this.Key = key;
		this.Created = created;
		this.Name = name;
		this.Autosave = autosave;
		this.LastModified = lastModified;
		this.Version = version;
	}

	/// <inheritdoc />
	public Guid Key { get; }

	public DateTime Created { get; }

	public string Name { get; }

	public bool Autosave { get; }

	public DateTime LastModified { get; }

	public long Version { get; }

	public bool IsValid => this.Key != Guid.Empty && this.LastModified >= this.Created && this.Version >= 0;

	public void Validate()
	{
		Ensure.That(this.Key, nameof(this.Key)).IsNotEmpty();
		Ensure.That(this.LastModified, nameof(this.LastModified)).IsGte(this.Created);
		Ensure.That(this.Version, nameof(this.Version)).IsGte(0);
	}

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(VoxelShapeRecord other) =>
		this.Key == other.Key &&
		this.Created == other.Created &&
		this.Name == other.Name &&
		this.Autosave == other.Autosave &&
		this.LastModified == other.LastModified &&
		this.Version == other.Version;

	/// <inheritdoc />
	public override int GetHashCode() =>
		HashCode.Combine(this.Key, this.Created, this.Name, this.Autosave, this.LastModified, this.Version);

	public static bool operator ==(VoxelShapeRecord left, VoxelShapeRecord right) => left.Equals(right);

	public static bool operator !=(VoxelShapeRecord left, VoxelShapeRecord right) => !(left == right);

	internal static VoxelShapeRecord From(IVolumeView view)
	{
		Debug.Assert(view != null);
		Debug.Assert(view.IsInitialized);

		return new VoxelShapeRecord(
			view.Key,
			view.Volume.Created,
			view.RunJobInfo.Name,
			view.RunJobInfo.Autosave,
			view.Volume.EngineData.LastModified,
			view.Volume.EngineData.Version);
	}
}
