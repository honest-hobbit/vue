﻿namespace HQS.VUE.OLD;

public readonly struct ChunkRecord : IEquatable<ChunkRecord>
{
	public ChunkRecord(Guid recordKey, ChunkKey chunkKey, long version, byte[] data)
	{
		this.RecordKey = recordKey;
		this.ChunkKey = chunkKey;
		this.Version = version;
		this.Data = data;
	}

	public Guid RecordKey { get; }

	public ChunkKey ChunkKey { get; }

	public long Version { get; }

	public byte[] Data { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(ChunkRecord other) =>
		this.Data == other.Data &&
		this.RecordKey == other.RecordKey &&
		this.ChunkKey == other.ChunkKey &&
		this.Version == other.Version;

	/// <inheritdoc />
	public override int GetHashCode() => this.RecordKey.GetHashCode();

	public static bool operator ==(ChunkRecord left, ChunkRecord right) => left.Equals(right);

	public static bool operator !=(ChunkRecord left, ChunkRecord right) => !(left == right);
}
