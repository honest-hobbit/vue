﻿namespace HQS.VUE.OLD;

public readonly struct ChunkIdRecord : IEquatable<ChunkIdRecord>
{
	public ChunkIdRecord(Guid recordKey, ChunkKey chunkKey)
	{
		this.RecordKey = recordKey;
		this.ChunkKey = chunkKey;
	}

	public Guid RecordKey { get; }

	public ChunkKey ChunkKey { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(ChunkIdRecord other) => this.RecordKey == other.RecordKey && this.ChunkKey == other.ChunkKey;

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.RecordKey, this.ChunkKey);

	public static bool operator ==(ChunkIdRecord left, ChunkIdRecord right) => left.Equals(right);

	public static bool operator !=(ChunkIdRecord left, ChunkIdRecord right) => !(left == right);
}
