﻿namespace HQS.VUE.OLD;

public interface IChunkGridStore : IDisposed
{
	// not originally part of interface
	string FilePath { get; }

	// not originally part of interface
	Guid ChunkGridKey { get; }

	////IEnumerable<Guid> GetAllChunkGridKeys();

	IEnumerable<ChunkIdRecord> GetAllChunkIdsOfGrid(Guid chunkGridKey);

	bool TryGetChunk(ChunkKey key, out ChunkRecord record);

	void DeleteChunkGrid(Guid chunkGridKey);
}
