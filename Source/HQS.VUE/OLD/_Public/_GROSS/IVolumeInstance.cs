﻿namespace HQS.VUE.OLD;

public interface IVolumeInstance
{
	bool TryGetChangeRecord(out ChangeRecord record);
}
