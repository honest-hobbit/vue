﻿namespace HQS.VUE.OLD;

public interface IVolumeInstances : IReadOnlyList<IVolumeInstance>
{
	Volume Volume { get; }

	IVolumeInstance CreateInstance();
}
