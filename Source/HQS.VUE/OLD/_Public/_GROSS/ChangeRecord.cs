﻿namespace HQS.VUE.OLD;

public readonly struct ChangeRecord : IEquatable<ChangeRecord>
{
	// TODO was Pooled<AnimateVoxelChangesComponent>
	public ChangeRecord(Pin<object> animator, Pin<IVoxelChangesRecord> record)
	{
		Debug.Assert(animator.IsPinned);
		Debug.Assert(record.IsPinned);

		this.Animator = animator;
		this.Record = record;
	}

	public bool IsAssigned => this.Animator.IsAssigned;

	public Pin<object> Animator { get; }

	public Pin<IVoxelChangesRecord> Record { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(ChangeRecord other) => this.Animator == other.Animator && this.Record == other.Record;

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Animator, this.Record);

	public static bool operator ==(ChangeRecord left, ChangeRecord right) => left.Equals(right);

	public static bool operator !=(ChangeRecord left, ChangeRecord right) => !(left == right);
}
