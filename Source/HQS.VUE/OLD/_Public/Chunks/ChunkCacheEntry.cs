﻿namespace HQS.VUE.OLD;

public readonly struct ChunkCacheEntry : IKeyed<ChunkKey>, IEquatable<ChunkCacheEntry>
{
	public ChunkCacheEntry(ChunkKey key, int pinCount)
	{
		this.Key = key;
		this.PinCount = pinCount;
	}

	public ChunkKey Key { get; }

	public bool IsPinned => this.PinCount > 0;

	public int PinCount { get; }

	public static bool operator ==(ChunkCacheEntry lhs, ChunkCacheEntry rhs) => lhs.Equals(rhs);

	public static bool operator !=(ChunkCacheEntry lhs, ChunkCacheEntry rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ChunkCacheEntry other) => this.Key == other.Key && this.PinCount == other.PinCount;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.Key.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => $"<{this.Key} - {this.PinCount}>";
}
