﻿namespace HQS.VUE.OLD;

public interface IChunkCache
{
	int Count { get; }

	// returns how many entries were successfully expired
	int TryClear();

	IEnumerable<ChunkCacheEntry> GetChunksSnapshot();
}
