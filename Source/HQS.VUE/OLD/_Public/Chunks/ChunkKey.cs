﻿namespace HQS.VUE.OLD;

public readonly struct ChunkKey : IEquatable<ChunkKey>
{
	public ChunkKey(Guid chunkGrid, Int3 location)
	{
		Debug.Assert(chunkGrid != Guid.Empty);

		this.ChunkGrid = chunkGrid;
		this.Location = location;
	}

	public static EqualityComparer<ChunkKey> Comparer => KeyEqualityComparer.Instance;

	public Guid ChunkGrid { get; }

	public Int3 Location { get; }

	public bool IsAssigned => this.ChunkGrid != Guid.Empty;

	#region Operators

	public static ChunkKey operator +(ChunkKey key) => key;

	public static ChunkKey operator -(ChunkKey key) => new ChunkKey(key.ChunkGrid, -key.Location);

	public static ChunkKey operator +(ChunkKey key, Int3 index) => new ChunkKey(key.ChunkGrid, key.Location + index);

	public static ChunkKey operator -(ChunkKey key, Int3 index) => new ChunkKey(key.ChunkGrid, key.Location - index);

	public static ChunkKey operator *(ChunkKey key, Int3 index) => new ChunkKey(key.ChunkGrid, key.Location * index);

	public static ChunkKey operator *(ChunkKey key, int scalar) => new ChunkKey(key.ChunkGrid, key.Location * scalar);

	public static ChunkKey operator *(int scalar, ChunkKey key) => new ChunkKey(key.ChunkGrid, key.Location * scalar);

	public static ChunkKey operator /(ChunkKey key, Int3 index) => new ChunkKey(key.ChunkGrid, key.Location / index);

	public static ChunkKey operator /(ChunkKey key, int scalar) => new ChunkKey(key.ChunkGrid, key.Location / scalar);

	public static ChunkKey operator %(ChunkKey key, Int3 index) => new ChunkKey(key.ChunkGrid, key.Location % index);

	public static ChunkKey operator %(ChunkKey key, int scalar) => new ChunkKey(key.ChunkGrid, key.Location % scalar);

	public static bool operator ==(ChunkKey lhs, ChunkKey rhs) => lhs.Equals(rhs);

	public static bool operator !=(ChunkKey lhs, ChunkKey rhs) => !lhs.Equals(rhs);

	#endregion

	/// <inheritdoc />
	public bool Equals(ChunkKey other) => this.ChunkGrid == other.ChunkGrid && this.Location == other.Location;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.ChunkGrid, this.Location);

	/// <inheritdoc />
	public override string ToString() => $"[{this.ChunkGrid}: {this.Location}]";

	private class KeyEqualityComparer : EqualityComparer<ChunkKey>
	{
		private KeyEqualityComparer()
		{
		}

		public static readonly KeyEqualityComparer Instance = new KeyEqualityComparer();

		/// <inheritdoc />
		public override bool Equals(ChunkKey lhs, ChunkKey rhs) => lhs.Equals(rhs);

		/// <inheritdoc />
		public override int GetHashCode(ChunkKey key) =>
			HashCode.Combine(key.ChunkGrid, MortonCurve.ForInt3.Encode(key.Location));
	}
}
