﻿namespace HQS.VUE.OLD;

public class UniverseConfig
{
	internal UniverseConfig(UniverseConfigBuilder config)
	{
		Debug.Assert(config != null);
		config.Validate();

		this.SubchunkExponent = config.SubchunkExponent;
		this.VoxelExponent = config.VoxelExponent;
		this.ContourSubchunkMeshQuadsCapacity = config.ContourSubchunkMeshQuadsCapacity;
		this.ContourSubchunkCollidersCapacity = config.ContourSubchunkCollidersCapacity;
		this.ByteBufferCapacity = config.ByteBufferCapacity;
		this.ByteCompressionMode = config.ByteCompressionMode;
	}

	public int SubchunkExponent { get; }

	public int VoxelExponent { get; }

	public int ContourSubchunkMeshQuadsCapacity { get; }

	public int ContourSubchunkCollidersCapacity { get; }

	public int ByteBufferCapacity { get; }

	public LZ4Mode ByteCompressionMode { get; }
}
