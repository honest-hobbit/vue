﻿namespace HQS.VUE.OLD;

public abstract class AbstractConfigBuilder
{
	private readonly List<Exception> exceptions = new List<Exception>(16);

	public void Validate()
	{
		try
		{
			this.exceptions.Clear();
			this.PerformValidation();

			if (this.exceptions.Count != 0)
			{
				throw new AggregateException(
					$"{this.exceptions.Count} exception(s) while validating {this.GetType().Name}.", this.exceptions);
			}
		}
		finally
		{
			this.exceptions.Clear();
		}
	}

	protected abstract void PerformValidation();

	protected void Validate<T>(T value, string name, Action<T, string> validate)
	{
		try
		{
			validate(value, name);
		}
		catch (Exception e)
		{
			this.exceptions.Add(e);
		}
	}

	protected void AddException(Exception exception)
	{
		if (exception != null)
		{
			this.exceptions.Add(exception);
		}
	}
}
