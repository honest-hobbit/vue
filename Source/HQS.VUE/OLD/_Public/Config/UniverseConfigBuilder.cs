﻿namespace HQS.VUE.OLD;

public class UniverseConfigBuilder : AbstractConfigBuilder
{
	public int SubchunkExponent { get; set; } = 4;

	public int VoxelExponent { get; set; } = 4;

	public int ContourSubchunkMeshQuadsCapacity { get; set; } = 64;

	public int ContourSubchunkCollidersCapacity { get; set; } = 32;

	public int ByteBufferCapacity { get; set; } = 100_000;

	public LZ4Mode ByteCompressionMode { get; set; } = LZ4Mode.L03HC;

	public UniverseConfig BuildConfig() => new UniverseConfig(this);

	/// <inheritdoc />
	protected override void PerformValidation()
	{
		this.Validate(
			this.SubchunkExponent,
			nameof(this.SubchunkExponent),
			(value, name) => VoxelSizeConfig.ValidateExponent(value, name));

		this.Validate(
			this.VoxelExponent,
			nameof(this.VoxelExponent),
			(value, name) => VoxelSizeConfig.ValidateExponent(value, name));

		this.Validate(
			this.ContourSubchunkMeshQuadsCapacity,
			nameof(this.ContourSubchunkMeshQuadsCapacity),
			(value, name) => Ensure.That(value, name).IsGte(0));

		this.Validate(
			this.ContourSubchunkCollidersCapacity,
			nameof(this.ContourSubchunkCollidersCapacity),
			(value, name) => Ensure.That(value, name).IsGte(0));

		this.Validate(
			this.ByteBufferCapacity,
			nameof(this.ByteBufferCapacity),
			(value, name) => Ensure.That(value, name).IsGte(0));

		this.Validate(
			this.ByteCompressionMode,
			nameof(this.ByteCompressionMode),
			(value, name) => Enumeration.ValidateIsDefined(value, name));
	}
}
