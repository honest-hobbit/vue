﻿namespace HQS.VUE.OLD;

public readonly struct VoxelType
{
	public VoxelType(PolyTypeIndex polyType, ColliderTypeIndex colliderType)
	{
		this.PolyType = polyType;
		this.ColliderType = colliderType;
	}

	public PolyTypeIndex PolyType { get; }

	public ColliderTypeIndex ColliderType { get; }
}
