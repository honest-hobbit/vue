﻿namespace HQS.VUE.OLD;

public readonly struct ColliderTypeIndex : IEquatable<ColliderTypeIndex>
{
	public ColliderTypeIndex(ushort index)
	{
		this.Index = index;
	}

	public static ColliderTypeIndex Empty => default;

	public ushort Index { get; }

	public static bool operator ==(ColliderTypeIndex lhs, ColliderTypeIndex rhs) => lhs.Equals(rhs);

	public static bool operator !=(ColliderTypeIndex lhs, ColliderTypeIndex rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(ColliderTypeIndex other) => this.Index == other.Index;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.Index.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.Index.ToString();
}
