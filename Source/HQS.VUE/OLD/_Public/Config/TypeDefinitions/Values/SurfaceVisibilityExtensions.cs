﻿namespace HQS.VUE.OLD;

public static class SurfaceVisibilityExtensions
{
	private static readonly Tables Table = new Tables();

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool IsVisible(this SurfaceVisibility type) => Table.IsVisible[(int)type];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool IsTransparent(this SurfaceVisibility type) => Table.IsTransparent[(int)type];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool IsOpaque(this SurfaceVisibility type) => Table.IsOpaque[(int)type];

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool IsDominant(this SurfaceVisibility type) => Table.IsDominant[(int)type];

	private class Tables
	{
		public Tables()
		{
			int length = 5;
			if (Enumeration.Values<SurfaceVisibility>().Count != length)
			{
				throw new Exception(
					$"{nameof(SurfaceVisibility)} was updated without also updating {nameof(SurfaceVisibilityExtensions)}.");
			}

			this.IsVisible = new bool[length];
			this.IsVisible[(int)SurfaceVisibility.None] = false;
			this.IsVisible[(int)SurfaceVisibility.Transparent] = true;
			this.IsVisible[(int)SurfaceVisibility.Opaque] = true;
			this.IsVisible[(int)SurfaceVisibility.DominantTransparent] = true;
			this.IsVisible[(int)SurfaceVisibility.DominantOpaque] = true;

			this.IsTransparent = new bool[length];
			this.IsTransparent[(int)SurfaceVisibility.None] = false;
			this.IsTransparent[(int)SurfaceVisibility.Transparent] = true;
			this.IsTransparent[(int)SurfaceVisibility.Opaque] = false;
			this.IsTransparent[(int)SurfaceVisibility.DominantTransparent] = true;
			this.IsTransparent[(int)SurfaceVisibility.DominantOpaque] = false;

			this.IsOpaque = new bool[length];
			this.IsOpaque[(int)SurfaceVisibility.None] = false;
			this.IsOpaque[(int)SurfaceVisibility.Transparent] = false;
			this.IsOpaque[(int)SurfaceVisibility.Opaque] = true;
			this.IsOpaque[(int)SurfaceVisibility.DominantTransparent] = false;
			this.IsOpaque[(int)SurfaceVisibility.DominantOpaque] = true;

			this.IsDominant = new bool[length];
			this.IsDominant[(int)SurfaceVisibility.None] = false;
			this.IsDominant[(int)SurfaceVisibility.Transparent] = false;
			this.IsDominant[(int)SurfaceVisibility.Opaque] = false;
			this.IsDominant[(int)SurfaceVisibility.DominantTransparent] = true;
			this.IsDominant[(int)SurfaceVisibility.DominantOpaque] = true;
		}

		public bool[] IsVisible { get; }

		public bool[] IsTransparent { get; }

		public bool[] IsOpaque { get; }

		public bool[] IsDominant { get; }
	}
}
