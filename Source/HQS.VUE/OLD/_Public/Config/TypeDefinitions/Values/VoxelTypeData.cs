﻿namespace HQS.VUE.OLD;

public readonly struct VoxelTypeData
{
	internal VoxelTypeData(
		ushort voxelTypeIndex,
		PolyTypeIndex polyType,
		SurfaceTypeIndex surfaceType,
		SurfaceVisibility visibility,
		ColorRGB color,
		ColliderTypeIndex colliderType)
	{
		this.VoxelTypeIndex = voxelTypeIndex;
		this.PolyType = polyType;
		this.SurfaceType = surfaceType;
		this.Visibility = visibility;
		this.Color = color;
		this.ColliderType = colliderType;
	}

	public ushort VoxelTypeIndex { get; }

	public PolyTypeIndex PolyType { get; }

	public SurfaceTypeIndex SurfaceType { get; }

	public SurfaceVisibility Visibility { get; }

	public ColorRGB Color { get; }

	public ColliderTypeIndex ColliderType { get; }

	internal Surface Surface => new Surface(this.SurfaceType, this.Visibility, this.PolyType);

	/// <inheritdoc />
	public override string ToString() =>
		$"VoxelType: {this.VoxelTypeIndex}, PolyType: {this.PolyType}, SurfaceType: {this.SurfaceType}, Visibility: {this.Visibility}, Color: {this.Color}, ColliderType: {this.ColliderType}";
}
