﻿namespace HQS.VUE.OLD;

public readonly struct SurfaceTypeIndex : IEquatable<SurfaceTypeIndex>
{
	public SurfaceTypeIndex(byte index)
	{
		this.Index = index;
	}

	public static SurfaceTypeIndex Empty => default;

	public byte Index { get; }

	public static bool operator ==(SurfaceTypeIndex lhs, SurfaceTypeIndex rhs) => lhs.Equals(rhs);

	public static bool operator !=(SurfaceTypeIndex lhs, SurfaceTypeIndex rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(SurfaceTypeIndex other) => this.Index == other.Index;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.Index.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.Index.ToString();
}
