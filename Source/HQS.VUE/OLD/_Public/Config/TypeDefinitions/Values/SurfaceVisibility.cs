﻿namespace HQS.VUE.OLD;

public enum SurfaceVisibility : byte
{
	None = 0,
	Transparent = 1,
	Opaque = 2,
	DominantTransparent = 3,
	DominantOpaque = 4,
}
