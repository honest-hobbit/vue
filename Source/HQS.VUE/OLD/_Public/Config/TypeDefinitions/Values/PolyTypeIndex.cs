﻿namespace HQS.VUE.OLD;

public readonly struct PolyTypeIndex : IEquatable<PolyTypeIndex>
{
	public PolyTypeIndex(ushort index)
	{
		this.Index = index;
	}

	public static PolyTypeIndex Empty => default;

	public ushort Index { get; }

	public static bool operator ==(PolyTypeIndex lhs, PolyTypeIndex rhs) => lhs.Equals(rhs);

	public static bool operator !=(PolyTypeIndex lhs, PolyTypeIndex rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(PolyTypeIndex other) => this.Index == other.Index;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.Index.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => this.Index.ToString();
}
