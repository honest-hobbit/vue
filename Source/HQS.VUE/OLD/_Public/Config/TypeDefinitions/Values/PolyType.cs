﻿namespace HQS.VUE.OLD;

public readonly struct PolyType
{
	// TODO need to add which mesh the quad goes to
	public PolyType(SurfaceTypeIndex surfaceType, ColorRGB color)
	{
		this.SurfaceType = surfaceType;
		this.Color = color;
	}

	public static PolyType Empty => default;

	public SurfaceTypeIndex SurfaceType { get; }

	public ColorRGB Color { get; }
}
