﻿namespace HQS.VUE.OLD;

public interface ITypeList<TIndex, TValue> : IReadOnlyList<TValue>
{
	TValue this[TIndex index] { get; }
}
