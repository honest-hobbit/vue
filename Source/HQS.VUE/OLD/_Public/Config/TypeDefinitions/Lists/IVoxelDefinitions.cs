﻿namespace HQS.VUE.OLD;

public interface IVoxelDefinitions
{
	ITypeList<Voxel, VoxelTypeData> Voxels { get; }

	ITypeList<SurfaceTypeIndex, SurfaceVisibility> Surfaces { get; }

	ITypeList<PolyTypeIndex, PolyType> Polys { get; }

	ITypeList<ColliderTypeIndex, ColliderType> Colliders { get; }
}
