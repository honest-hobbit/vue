﻿namespace HQS.VUE.OLD;

public class VoxelTypesConfig
{
	internal VoxelTypesConfig(VoxelTypesConfigBuilder builder)
	{
		Debug.Assert(builder != null);

		this.Definitions = CreateVoxelDefinitions(builder);
	}

	internal VoxelDefinitions Definitions { get; }

	internal Surface GetSurface(Voxel voxel) => this.Definitions.Voxels[voxel.TypeIndex].Surface;

	internal ColliderTypeIndex GetColliderTypeIndex(Voxel voxel) => this.Definitions.Voxels[voxel.TypeIndex].ColliderType;

	internal ColorRGB GetColor(PolyTypeIndex polyType) => this.Definitions.Polys[polyType.Index].Color;

	private static VoxelDefinitions CreateVoxelDefinitions(VoxelTypesConfigBuilder builder)
	{
		Debug.Assert(builder != null);

		var voxelTypes = new VoxelTypeData[builder.VoxelTypes.Count];

		for (int index = 0; index < voxelTypes.Length; index++)
		{
			var voxelType = builder.VoxelTypes[index];
			var polyType = builder.PolyTypes[voxelType.PolyType.Index];

			voxelTypes[index] = new VoxelTypeData(
				(ushort)index,
				voxelType.PolyType,
				polyType.SurfaceType,
				builder.SurfaceTypes[polyType.SurfaceType.Index],
				polyType.Color,
				voxelType.ColliderType);
		}

		return new VoxelDefinitions(
			new VoxelTypes(voxelTypes),
			new SurfaceTypes(builder.SurfaceTypes.ToArrayFromReadOnly()),
			new PolyTypes(builder.PolyTypes.ToArrayFromReadOnly()),
			new ColliderTypes(builder.ColliderTypes.ToArrayFromReadOnly()));
	}
}
