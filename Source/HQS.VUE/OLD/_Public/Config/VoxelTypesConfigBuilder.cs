﻿namespace HQS.VUE.OLD;

public class VoxelTypesConfigBuilder
{
	private readonly List<SurfaceVisibility> surfaceTypes = new List<SurfaceVisibility>();

	private readonly List<PolyType> polyTypes = new List<PolyType>();

	private readonly List<ColliderType> colliderTypes = new List<ColliderType>();

	private readonly List<VoxelType> voxelTypes = new List<VoxelType>();

	public VoxelTypesConfigBuilder()
	{
		this.surfaceTypes.Add(SurfaceVisibility.None);
		this.polyTypes.Add(PolyType.Empty);
		this.colliderTypes.Add(ColliderType.Empty);
		this.voxelTypes.Add(new VoxelType(PolyTypeIndex.Empty, ColliderTypeIndex.Empty));
	}

	internal IReadOnlyList<SurfaceVisibility> SurfaceTypes => this.surfaceTypes;

	internal IReadOnlyList<PolyType> PolyTypes => this.polyTypes;

	internal IReadOnlyList<ColliderType> ColliderTypes => this.colliderTypes;

	internal IReadOnlyList<VoxelType> VoxelTypes => this.voxelTypes;

	public SurfaceTypesBuilder StartSurfaceTypes() => new SurfaceTypesBuilder(this);

	public readonly struct SurfaceTypesBuilder
	{
		private readonly VoxelTypesConfigBuilder builder;

		internal SurfaceTypesBuilder(VoxelTypesConfigBuilder builder)
		{
			Debug.Assert(builder != null);

			this.builder = builder;
		}

		public SurfaceTypesBuilder Add(SurfaceVisibility type)
		{
			this.builder.surfaceTypes.Add(type);
			return this;
		}

		public SurfaceTypesBuilder AddMany(params SurfaceVisibility[] types) => this.AddMany((IEnumerable<SurfaceVisibility>)types);

		public SurfaceTypesBuilder AddMany(IEnumerable<SurfaceVisibility> types)
		{
			Ensure.That(types, nameof(types)).IsNotNull();

			this.builder.surfaceTypes.AddRange(types);
			return this;
		}

		public PolyTypesBuilder StartPolyTypes() => new PolyTypesBuilder(this.builder);
	}

	public readonly struct PolyTypesBuilder
	{
		private readonly VoxelTypesConfigBuilder builder;

		internal PolyTypesBuilder(VoxelTypesConfigBuilder builder)
		{
			Debug.Assert(builder != null);

			this.builder = builder;
		}

		public PolyTypesBuilder Add(SurfaceTypeIndex surfaceType, ColorRGB color) =>
			this.Add(new PolyType(surfaceType, color));

		public PolyTypesBuilder Add(PolyType type)
		{
			Ensure.That((int)type.SurfaceType.Index, nameof(type.SurfaceType)).IsInRange(0, this.builder.surfaceTypes.Count - 1);

			this.builder.polyTypes.Add(type);
			return this;
		}

		public PolyTypesBuilder AddMany(params PolyType[] types) => this.AddMany((IEnumerable<PolyType>)types);

		public PolyTypesBuilder AddMany(IEnumerable<PolyType> types)
		{
			Ensure.That(types, nameof(types)).IsNotNull();

			foreach (var type in types)
			{
				this.Add(type);
			}

			return this;
		}

		public ColliderTypesBuilder StartColliderTypes() => new ColliderTypesBuilder(this.builder);
	}

	public readonly struct ColliderTypesBuilder
	{
		private readonly VoxelTypesConfigBuilder builder;

		internal ColliderTypesBuilder(VoxelTypesConfigBuilder builder)
		{
			Debug.Assert(builder != null);

			this.builder = builder;
		}

		public ColliderTypesBuilder Add(ColliderType type)
		{
			this.builder.colliderTypes.Add(type);
			return this;
		}

		public ColliderTypesBuilder AddMany(params ColliderType[] types) => this.AddMany((IEnumerable<ColliderType>)types);

		public ColliderTypesBuilder AddMany(IEnumerable<ColliderType> types)
		{
			Ensure.That(types, nameof(types)).IsNotNull();

			this.builder.colliderTypes.AddRange(types);
			return this;
		}

		public VoxelTypesBuilder StartVoxelTypes() => new VoxelTypesBuilder(this.builder);
	}

	public readonly struct VoxelTypesBuilder
	{
		private readonly VoxelTypesConfigBuilder builder;

		internal VoxelTypesBuilder(VoxelTypesConfigBuilder builder)
		{
			Debug.Assert(builder != null);

			this.builder = builder;
		}

		public VoxelTypesBuilder Add(PolyTypeIndex polyType, ColliderTypeIndex colliderType) =>
			this.Add(new VoxelType(polyType, colliderType));

		public VoxelTypesBuilder Add(VoxelType type)
		{
			Ensure.That((int)type.PolyType.Index, nameof(type.PolyType)).IsInRange(0, this.builder.polyTypes.Count - 1);
			Ensure.That((int)type.ColliderType.Index, nameof(type.ColliderType)).IsInRange(0, this.builder.colliderTypes.Count - 1);

			this.builder.voxelTypes.Add(type);
			return this;
		}

		public VoxelTypesBuilder AddMany(params VoxelType[] types) => this.AddMany((IEnumerable<VoxelType>)types);

		public VoxelTypesBuilder AddMany(IEnumerable<VoxelType> types)
		{
			Ensure.That(types, nameof(types)).IsNotNull();

			foreach (var type in types)
			{
				this.Add(type);
			}

			return this;
		}

		public VoxelTypesConfig BuildConfig() => new VoxelTypesConfig(this.builder);
	}
}
