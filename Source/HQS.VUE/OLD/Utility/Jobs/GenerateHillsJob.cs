﻿namespace HQS.VUE.OLD.Utility.Jobs;

public class GenerateHillsJob : WriteSingleVolumeJob, IVoxelChangeAnimatorTiming
{
	private readonly FastNoiseLite noise = new FastNoiseLite();

	public GenerateHillsJob()
	{
		this.noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		this.noise.SetFrequency(.01f);
	}

	public IEnumerable<Int2> Shape { get; set; }

	public Int3 Origin { get; set; }

	public int Seed
	{
		get => this.noise.GetSeed();
		set => this.noise.SetSeed(value);
	}

	public Voxel GroundFirst { get; set; } = new Voxel(1);

	public Voxel GroundSecond { get; set; } = new Voxel(3);

	public Voxel GroundDots { get; set; } = new Voxel(2);

	public int GroundDotsFirstHeight { get; set; } = 1;

	public int GroundDotsSecondHeight { get; set; } = 5;

	public bool ClearAboveGround { get; set; } = false;

	public int ClearHeight { get; set; } = 30;

	/// <inheritdoc />
	public VoxelChangeTiming TimingMode { get; set; } = VoxelChangeTiming.Immediate;

	/// <inheritdoc />
	protected override void Run(IVoxelWriteView voxels)
	{
		if (this.Shape == null)
		{
			return;
		}

		foreach (var i in this.Shape.Select(i => new Int3(i.X + this.Origin.X, 0, i.Y + this.Origin.Z)))
		{
			int hillHeight = (this.noise.GetNoise(i.X, i.Z) * 22).ClampToInt() + 12;

			int iY = 0;
			for (; iY <= hillHeight; iY++)
			{
				var voxelRead = voxels.Read[i.X, iY, i.Z];
				if (voxelRead != this.GroundSecond)
				{
					voxels[i.X, iY, i.Z] = (voxelRead == this.GroundFirst) ? this.GroundSecond : this.GroundFirst;
				}
			}

			float random = Noise.Get2DZeroToOne(i.X, i.Z, (uint)this.Seed);
			if (random < .025f)
			{
				int dotsHeight = (voxels.Read[i.X, hillHeight + 1, i.Z] != this.GroundDots) ?
					this.GroundDotsFirstHeight : this.GroundDotsSecondHeight;

				for (int iDots = 1; iDots <= dotsHeight; iDots++)
				{
					voxels[i.X, iY, i.Z] = this.GroundDots;
					iY++;
				}
			}

			if (this.ClearAboveGround)
			{
				for (; iY <= this.ClearHeight; iY++)
				{
					voxels[i.X, iY, i.Z] = Voxel.Empty;
				}
			}
		}
	}
}
