﻿namespace HQS.VUE.OLD.Utility.Jobs;

public sealed class ReadVoxelJob : ReadSingleVolumeJob, IVoxelJobCompletion
{
	public Action<string> LogMessage { get; set; }

	public Int3 Index { get; set; }

	private VoxelTypeData voxelData;

	/// <inheritdoc />
	void IVoxelJobCompletion.Complete(VoxelJobDiagnostics results)
	{
		this.LogMessage?.Invoke($"{this.Index} = {this.voxelData}");
	}

	/// <inheritdoc />
	protected override void Run(IVoxelReadView volume)
	{
		this.voxelData = volume.Definitions.Voxels[volume[this.Index].TypeIndex];
	}
}
