﻿namespace HQS.VUE.OLD.Utility.Jobs;

public class CheckerJob : WriteSingleVolumeJob
{
	public Voxel VoxelA { get; set; }

	public Voxel VoxelB { get; set; }

	public Int3 Min { get; set; }

	public Int3 Dimensions { get; set; }

	/// <inheritdoc />
	protected override void Run(IVoxelWriteView volume)
	{
		var max = this.Min + this.Dimensions;

		for (int iX = this.Min.X; iX < max.X; iX++)
		{
			for (int iY = this.Min.Y; iY < max.Y; iY++)
			{
				bool isA = (iX + iY + this.Min.Z).IsEven();
				for (int iZ = this.Min.Z; iZ < max.Z; iZ++)
				{
					volume[iX, iY, iZ] = isA ? this.VoxelA : this.VoxelB;
					isA = !isA;
				}
			}
		}
	}
}
