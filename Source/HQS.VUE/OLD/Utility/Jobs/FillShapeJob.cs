﻿namespace HQS.VUE.OLD.Utility.Jobs;

public class FillShapeJob : WriteSingleVolumeJob
{
	public IEnumerable<Int3> Shape { get; set; }

	public Int3 Origin { get; set; }

	public Voxel Voxel { get; set; }

	/// <inheritdoc />
	protected override void Run(IVoxelWriteView volume)
	{
		if (this.Shape == null)
		{
			return;
		}

		foreach (var i in this.Shape.Select(i => i + this.Origin))
		{
			volume[i] = this.Voxel;
		}
	}
}
