﻿namespace HQS.VUE.OLD.Utility.Jobs;

public class SetVoxelJob : WriteSingleVolumeJob
{
	public Voxel Voxel { get; set; }

	public Int3 Index { get; set; }

	/// <inheritdoc />
	protected override void Run(IVoxelWriteView volume)
	{
		volume[this.Index] = this.Voxel;
	}
}
