﻿namespace HQS.VUE.OLD.Utility;

public interface IVoxelChangeAnimatorTiming
{
	VoxelChangeTiming TimingMode { get; }
}
