﻿namespace HQS.VUE.OLD.Utility.Stores;

public class FileStoreConfig
{
	internal FileStoreConfig(FileStoreConfigBuilder config)
	{
		Debug.Assert(config != null);
		config.Validate();

		this.VoxelSizeConfig = config.VoxelSizeConfig;
		this.VoxelGridFileExtension = config.VoxelGridFileExtension;
		this.VoxelShapeFileExtension = config.VoxelShapeFileExtension;
		this.FileConnectionDuration = config.FileConnectionDuration;
	}

	public IVoxelSizeConfig VoxelSizeConfig { get; set; }

	public string VoxelGridFileExtension { get; }

	public string VoxelShapeFileExtension { get; }

	public TimeSpan FileConnectionDuration { get; }
}
