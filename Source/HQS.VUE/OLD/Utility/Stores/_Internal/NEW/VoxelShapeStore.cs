﻿namespace HQS.VUE.OLD.Utility.Stores;

internal class VoxelShapeStore : AbstractChunkGridStore<VoxelShapeRecord>, IVoxelShapeStore
{
	public VoxelShapeStore(Guid chunkGridKey)
		: base(chunkGridKey)
	{
	}

	/// <inheritdoc />
	public override IEnumerable<VoxelShapeRecord> GetAllChunkGrids()
	{
		this.ValidateNotDisposed();

		return null;
	}

	/// <inheritdoc />
	public override bool TryGetChunkGrid(Guid chunkGridKey, out VoxelShapeRecord gridRecord)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);

		gridRecord = default;
		return false;
	}

	/// <inheritdoc />
	public override void UpdateChunks(
		VoxelShapeRecord gridRecord, IReadOnlyList<ChunkRecord> chunkRecords)
	{
		this.ValidateNotDisposed();
		Ensure.That(chunkRecords, nameof(chunkRecords)).IsNotNull();
		this.ValidateOrAssignChunkGridKey(gridRecord.Key);
	}

	/// <inheritdoc />
	public override void DeleteChunkGrid(Guid chunkGridKey)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);
	}
}
