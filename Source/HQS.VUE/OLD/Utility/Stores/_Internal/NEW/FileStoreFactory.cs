﻿namespace HQS.VUE.OLD.Utility.Stores;

internal class FileStoreFactory
{
	private const string VoxelGridType = "VoxelGrid";

	private const string VoxelShapeType = "VoxelShape";

	private readonly FileStoreConfig config;

	public FileStoreFactory(FileStoreConfig config)
	{
		Debug.Assert(config != null);

		this.config = config;
	}

	public VoxelGridStore CreateVoxelGridStore(string path)
	{
		ValidatePath(path, this.config.VoxelGridFileExtension);

		return null;
	}

	public VoxelShapeStore CreateVoxelShapeStore(string path)
	{
		ValidatePath(path, this.config.VoxelShapeFileExtension);

		return null;
	}

	private static void ValidatePath(string path, string fileExtension)
	{
		Debug.Assert(!fileExtension.IsNullOrWhiteSpace());

		if (!PathUtility.IsPathValid(path))
		{
			throw new IOException($"{path} is not a valid path.");
		}

		if (Path.GetExtension(path) != fileExtension)
		{
			throw new ArgumentException($"File extension of {path} must be '{fileExtension}'.");
		}
	}
}
