﻿namespace HQS.VUE.OLD.Utility.Stores;

internal abstract class AbstractChunkGridStore<T> : IChunkGridStore<T>, IDisposed
	where T : struct, IKeyed<Guid>
{
	private readonly object padlock = new object();

	private Guid chunkGridKey;

	public AbstractChunkGridStore(Guid chunkGridKey)
	{
		this.chunkGridKey = chunkGridKey;
	}

	/// <inheritdoc />
	public string FilePath => string.Empty;

	/// <inheritdoc />
	public Guid ChunkGridKey
	{
		get
		{
			lock (this.padlock)
			{
				return this.chunkGridKey;
			}
		}
	}

	/// <inheritdoc />
	public bool IsDisposed => false;

	public IDisposable Disposable => null;

	/// <inheritdoc />
	public abstract IEnumerable<T> GetAllChunkGrids();

	/// <inheritdoc />
	public abstract bool TryGetChunkGrid(Guid chunkGridKey, out T gridRecord);

	/// <inheritdoc />
	public abstract void UpdateChunks(
		T gridRecord, IReadOnlyList<ChunkRecord> chunkRecords);

	/// <inheritdoc />
	public abstract void DeleteChunkGrid(Guid chunkGridKey);

	/// <inheritdoc />
	public IEnumerable<ChunkIdRecord> GetAllChunkIdsOfGrid(Guid chunkGridKey)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);

		return null;
	}

	/// <inheritdoc />
	public bool TryGetChunk(ChunkKey key, out ChunkRecord record)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(key.ChunkGrid);

		record = default;
		return false;
	}

	protected void ValidateChunkGridKey(Guid key)
	{
		Ensure.That(key, nameof(key)).IsNotEmpty();

		lock (this.padlock)
		{
			if (this.chunkGridKey != Guid.Empty)
			{
				Ensure.That(key, nameof(key)).Is(this.chunkGridKey);
			}
		}
	}

	protected void ValidateOrAssignChunkGridKey(Guid key)
	{
		Ensure.That(key, nameof(key)).IsNotEmpty();

		lock (this.padlock)
		{
			if (this.chunkGridKey == Guid.Empty)
			{
				this.chunkGridKey = key;
			}
			else
			{
				Ensure.That(key, nameof(key)).Is(this.chunkGridKey);
			}
		}
	}
}
