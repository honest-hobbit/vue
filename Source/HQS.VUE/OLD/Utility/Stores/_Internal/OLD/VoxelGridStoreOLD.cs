﻿namespace HQS.VUE.OLD.Utility.Stores;

internal class VoxelGridStoreOLD : AbstractChunkGridStoreOLD<VoxelGridRecord>, IVoxelGridStore
{
	public VoxelGridStoreOLD(IFileStore store, Guid chunkGridKey)
		: base(store, chunkGridKey)
	{
	}

	/// <inheritdoc />
	public override IEnumerable<VoxelGridRecord> GetAllChunkGrids()
	{
		this.ValidateNotDisposed();

		return this.Store.All<VoxelGridEntity>().Select(x => x.GetRecord()).ToArray();
	}

	/// <inheritdoc />
	public override bool TryGetChunkGrid(Guid chunkGridKey, out VoxelGridRecord gridRecord)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);

		if (this.Store.TryGet(chunkGridKey.ToByteArray(), out VoxelGridEntity entity))
		{
			gridRecord = entity.GetRecord();
			return true;
		}
		else
		{
			gridRecord = default;
			return false;
		}
	}

	/// <inheritdoc />
	public override void UpdateChunks(VoxelGridRecord gridRecord, IReadOnlyList<ChunkRecord> chunkRecords)
	{
		this.ValidateNotDisposed();
		Ensure.That(chunkRecords, nameof(chunkRecords)).IsNotNull();
		this.ValidateOrAssignChunkGridKey(gridRecord.Key);

		this.Store.RunInTransaction(x =>
		{
			x.AddOrUpdate(new VoxelGridEntity().SetTo(gridRecord));

			if (chunkRecords.Count > 0)
			{
				x.AddOrUpdateMany(chunkRecords.Select(y => new ChunkEntity().SetTo(y)));
			}
		});
	}

	/// <inheritdoc />
	public override void DeleteChunkGrid(Guid chunkGridKey)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);

		this.Store.DisposeAndDeleteFile();
	}
}
