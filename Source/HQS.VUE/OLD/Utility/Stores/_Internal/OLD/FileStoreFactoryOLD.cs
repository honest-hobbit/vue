﻿namespace HQS.VUE.OLD.Utility.Stores;

internal class FileStoreFactoryOLD
{
	private const string VoxelGridType = "VoxelGrid";

	private const string VoxelShapeType = "VoxelShape";

	private readonly FileStoreConfig config;

	public FileStoreFactoryOLD(FileStoreConfig config)
	{
		Debug.Assert(config != null);

		this.config = config;
	}

	public VoxelGridStoreOLD CreateVoxelGridStore(string path)
	{
		ValidatePath(path, this.config.VoxelGridFileExtension);

		return new VoxelGridStoreOLD(this.CreateStore<VoxelGridEntity>(path, VoxelGridType, out var key), key);
	}

	public VoxelShapeStoreOLD CreateVoxelShapeStore(string path)
	{
		ValidatePath(path, this.config.VoxelShapeFileExtension);

		return new VoxelShapeStoreOLD(this.CreateStore<VoxelShapeEntity>(path, VoxelShapeType, out var key), key);
	}

	private static void ValidatePath(string path, string fileExtension)
	{
		Debug.Assert(!fileExtension.IsNullOrWhiteSpace());

		if (!PathUtility.IsPathValid(path))
		{
			throw new IOException($"{path} is not a valid path.");
		}

		if (Path.GetExtension(path) != fileExtension)
		{
			throw new ArgumentException($"File extension of {path} must be '{fileExtension}'.");
		}
	}

	private static void Validate<T>(ConfigStore<SQLiteConfigEntity> store, IConfigKey<T> key, T expected)
		where T : IEquatable<T>
	{
		Debug.Assert(!store.IsNull);
		Debug.Assert(key != null);

		T found = store.GetOrAddDefault(key, expected);
		if (!found.Equals(expected))
		{
			throw new IOException(
				$"{key.Key} of store is '{found}' and does not match the expected value of '{expected}'.");
		}
	}

	private IFileStore CreateStore<TGrid>(string path, string chunkGridType, out Guid chunkGridKey)
		where TGrid : ChunkGridEntity, new()
	{
		Debug.Assert(!path.IsNullOrWhiteSpace());
		Debug.Assert(!chunkGridType.IsNullOrWhiteSpace());

		IFileStore store = null;
		try
		{
			SQLiteStore.CreateDirectoryForStore(path);

			store = new ConcurrentStore(new TimeoutStore(this.config.FileConnectionDuration, () => new SQLiteStore(path)));
			////store = new ConcurrentStore(new TransientStore(() => new SQLiteStore(path)));
			////store = new ConcurrentStore(new SQLiteStore(path));

			var storeConfig = store.ConfigInSQLite();
			Validate(storeConfig, Config.ChunkGridType, chunkGridType);
			Validate(storeConfig, Config.VoxelExponent, this.config.VoxelSizeConfig.VoxelIndexer.Exponent);
			Validate(storeConfig, Config.SubchunkExponent, this.config.VoxelSizeConfig.SubchunkIndexer.Exponent);

			store.Initialize<TGrid, ChunkEntity>();

			var chunkGrids = store.All<TGrid>().ToArray();
			chunkGridKey = chunkGrids.Length switch
			{
				0 => Guid.Empty,
				1 => chunkGrids[0].KeyGuid,
				_ => throw new IOException($"Store can't contain more than 1 {typeof(TGrid).Name}."),
			};
		}
		catch (Exception)
		{
			store?.Dispose();
			throw;
		}

		return store;
	}

	private static class Config
	{
		public static IConfigKey<string> ChunkGridType { get; } = ConfigKey.ForString("VUE.ChunkGridType");

		public static IConfigKey<int> VoxelExponent { get; } = ConfigKey.ForInt("VUE.VoxelExponent");

		public static IConfigKey<int> SubchunkExponent { get; } = ConfigKey.ForInt("VUE.SubchunkExponent");
	}
}
