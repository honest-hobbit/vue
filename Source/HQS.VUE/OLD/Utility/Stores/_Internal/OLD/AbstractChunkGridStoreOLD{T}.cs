﻿namespace HQS.VUE.OLD.Utility.Stores;

internal abstract class AbstractChunkGridStoreOLD<T> : IChunkGridStore<T>, IDisposed
	where T : struct, IKeyed<Guid>
{
	private readonly object padlock = new object();

	private Guid chunkGridKey;

	public AbstractChunkGridStoreOLD(IFileStore store, Guid chunkGridKey)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);

		this.Store = store;
		this.chunkGridKey = chunkGridKey;
	}

	/// <inheritdoc />
	public string FilePath => this.Store.Path;

	/// <inheritdoc />
	public Guid ChunkGridKey
	{
		get
		{
			lock (this.padlock)
			{
				return this.chunkGridKey;
			}
		}
	}

	/// <inheritdoc />
	public bool IsDisposed => this.Store.IsDisposed;

	public IDisposable Disposable => this.Store;

	protected IFileStore Store { get; }

	/// <inheritdoc />
	public abstract IEnumerable<T> GetAllChunkGrids();

	/// <inheritdoc />
	public abstract bool TryGetChunkGrid(Guid chunkGridKey, out T gridRecord);

	/// <inheritdoc />
	public abstract void UpdateChunks(T gridRecord, IReadOnlyList<ChunkRecord> chunkRecords);

	/// <inheritdoc />
	public abstract void DeleteChunkGrid(Guid chunkGridKey);

	/// <inheritdoc />
	public IEnumerable<ChunkIdRecord> GetAllChunkIdsOfGrid(Guid chunkGridKey)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);

		return this.Store.All<ChunkIdEntity>().Select(
			x => new ChunkIdRecord(x.RecordKey, new ChunkKey(chunkGridKey, x.Location))).ToArray();
	}

	/// <inheritdoc />
	public bool TryGetChunk(ChunkKey key, out ChunkRecord record)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(key.ChunkGrid);

		// TODO this is probably creating a closure and thus garbage (but it must be done)
		int x = key.Location.X;
		int y = key.Location.Y;
		int z = key.Location.Z;

		var chunk = this.Store.Where((ChunkEntity entity) =>
			entity.LocationX == x && entity.LocationY == y && entity.LocationZ == z).SingleOrDefault();
		if (chunk != null)
		{
			record = new ChunkRecord(chunk.RecordKey, key, chunk.Version, chunk.Data);
			return true;
		}
		else
		{
			record = default;
			return false;
		}
	}

	protected void ValidateChunkGridKey(Guid key)
	{
		Ensure.That(key, nameof(key)).IsNotEmpty();

		lock (this.padlock)
		{
			if (this.chunkGridKey != Guid.Empty)
			{
				Ensure.That(key, nameof(key)).Is(this.chunkGridKey);
			}
		}
	}

	protected void ValidateOrAssignChunkGridKey(Guid key)
	{
		Ensure.That(key, nameof(key)).IsNotEmpty();

		lock (this.padlock)
		{
			if (this.chunkGridKey == Guid.Empty)
			{
				this.chunkGridKey = key;
			}
			else
			{
				Ensure.That(key, nameof(key)).Is(this.chunkGridKey);
			}
		}
	}
}
