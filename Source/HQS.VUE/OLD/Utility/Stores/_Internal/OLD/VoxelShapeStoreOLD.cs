﻿namespace HQS.VUE.OLD.Utility.Stores;

internal class VoxelShapeStoreOLD : AbstractChunkGridStoreOLD<VoxelShapeRecord>, IVoxelShapeStore
{
	public VoxelShapeStoreOLD(IFileStore store, Guid chunkGridKey)
		: base(store, chunkGridKey)
	{
	}

	/// <inheritdoc />
	public override IEnumerable<VoxelShapeRecord> GetAllChunkGrids()
	{
		this.ValidateNotDisposed();

		return this.Store.All<VoxelShapeEntity>().Select(x => x.GetRecord()).ToArray();
	}

	/// <inheritdoc />
	public override bool TryGetChunkGrid(Guid chunkGridKey, out VoxelShapeRecord gridRecord)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);

		if (this.Store.TryGet(chunkGridKey.ToByteArray(), out VoxelShapeEntity entity))
		{
			gridRecord = entity.GetRecord();
			return true;
		}
		else
		{
			gridRecord = default;
			return false;
		}
	}

	/// <inheritdoc />
	public override void UpdateChunks(VoxelShapeRecord gridRecord, IReadOnlyList<ChunkRecord> chunkRecords)
	{
		this.ValidateNotDisposed();
		Ensure.That(chunkRecords, nameof(chunkRecords)).IsNotNull();
		this.ValidateOrAssignChunkGridKey(gridRecord.Key);

		this.Store.RunInTransaction(x =>
		{
			x.AddOrUpdate(new VoxelShapeEntity().SetTo(gridRecord));

			if (chunkRecords.Count > 0)
			{
				x.AddOrUpdateMany(chunkRecords.Select(y => new ChunkEntity().SetTo(y)));
			}
		});
	}

	/// <inheritdoc />
	public override void DeleteChunkGrid(Guid chunkGridKey)
	{
		this.ValidateNotDisposed();
		this.ValidateChunkGridKey(chunkGridKey);

		this.Store.DisposeAndDeleteFile();
	}
}
