﻿namespace HQS.VUE.OLD.Utility.Stores;

[Table("VoxelShapes")]
internal class VoxelShapeEntity : ChunkGridEntity
{
	public VoxelShapeRecord GetRecord() => new VoxelShapeRecord(
		this.KeyGuid,
		this.Created,
		this.Name,
		this.Autosave,
		this.LastModified,
		this.Version);

	public ChunkGridEntity SetTo(VoxelShapeRecord record)
	{
		this.KeyGuid = record.Key;
		this.Created = record.Created;
		this.Name = record.Name;
		this.Autosave = record.Autosave;
		this.LastModified = record.LastModified;
		this.Version = record.Version;
		return this;
	}
}
