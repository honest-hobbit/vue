﻿namespace HQS.VUE.OLD.Utility.Stores;

[Table("Chunks")]
internal class ChunkEntity : ChunkIdEntity
{
	public byte[] Data { get; set; }

	public ChunkEntity SetTo(ChunkRecord record)
	{
		this.RecordKey = record.RecordKey;
		this.Location = record.ChunkKey.Location;
		this.Version = record.Version;
		this.Data = record.Data;
		return this;
	}

	public override void Clear()
	{
		base.Clear();
		this.Data = null;
	}
}
