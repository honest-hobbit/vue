﻿namespace HQS.VUE.OLD.Utility.Stores;

internal class ChunkGridEntity : IKeyed<byte[]>
{
	/// <inheritdoc />
	[PrimaryKey]
	public byte[] Key { get; set; }

	[Ignore]
	public Guid KeyGuid
	{
		get => GuidUtility.FromNullableArray(this.Key);
		set => this.Key = value.ToNullableByteArray(this.Key);
	}

	public DateTime Created { get; set; }

	public string Name { get; set; }

	public bool Autosave { get; set; }

	public DateTime LastModified { get; set; }

	public long Version { get; set; }

	public void Clear()
	{
		this.KeyGuid = Guid.Empty;
		this.Created = DateTime.MinValue;
		this.Name = string.Empty;
		this.Autosave = false;
		this.LastModified = DateTime.MinValue;
		this.Version = 0;
	}
}
