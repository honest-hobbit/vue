﻿namespace HQS.VUE.OLD.Utility.Stores;

[Table("VoxelGrids")]
internal class VoxelGridEntity : ChunkGridEntity
{
	public VoxelGridRecord GetRecord() => new VoxelGridRecord(
		this.KeyGuid,
		this.Created,
		this.Name,
		this.Autosave,
		this.LastModified,
		this.Version);

	public ChunkGridEntity SetTo(VoxelGridRecord record)
	{
		this.KeyGuid = record.Key;
		this.Created = record.Created;
		this.Name = record.Name;
		this.Autosave = record.Autosave;
		this.LastModified = record.LastModified;
		this.Version = record.Version;
		return this;
	}
}
