﻿namespace HQS.VUE.OLD.Utility.Stores;

[Table("Chunks")]
internal class ChunkIdEntity : IKeyed<byte[]>
{
	/// <inheritdoc />
	[PrimaryKey]
	public byte[] Key { get; set; }

	[Indexed]
	public int LocationX { get; set; }

	[Indexed]
	public int LocationY { get; set; }

	[Indexed]
	public int LocationZ { get; set; }

	[Ignore]
	public Guid RecordKey
	{
		get => GuidUtility.FromNullableArray(this.Key);
		set => this.Key = value.ToNullableByteArray(this.Key);
	}

	[Ignore]
	public Int3 Location
	{
		get => new Int3(this.LocationX, this.LocationY, this.LocationZ);
		set
		{
			this.LocationX = value.X;
			this.LocationY = value.Y;
			this.LocationZ = value.Z;
		}
	}

	public long Version { get; set; }

	public virtual void Clear()
	{
		this.RecordKey = Guid.Empty;
		this.Location = Int3.Zero;
		this.Version = 0;
	}
}
