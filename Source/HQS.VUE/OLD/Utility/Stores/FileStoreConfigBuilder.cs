﻿namespace HQS.VUE.OLD.Utility.Stores;

public class FileStoreConfigBuilder : AbstractConfigBuilder
{
	public IVoxelSizeConfig VoxelSizeConfig { get; set; }

	public string VoxelGridFileExtension { get; set; } = ".vuevg";

	public string VoxelShapeFileExtension { get; set; } = ".vuevs";

	public TimeSpan FileConnectionDuration { get; set; } = TimeSpan.FromSeconds(1);

	public FileStoreConfig BuildConfig() => new FileStoreConfig(this);

	/// <inheritdoc />
	protected override void PerformValidation()
	{
		this.Validate(
			this.VoxelSizeConfig,
			nameof(this.VoxelSizeConfig),
			(value, name) => Ensure.That(value, name).IsNotNull());

		this.Validate(
			this.VoxelGridFileExtension,
			nameof(this.VoxelGridFileExtension),
			(value, name) => PathUtility.ValidateFileExtension(value, name));

		this.Validate(
			this.VoxelShapeFileExtension,
			nameof(this.VoxelShapeFileExtension),
			(value, name) => PathUtility.ValidateFileExtension(value, name));

		if (this.VoxelShapeFileExtension == this.VoxelGridFileExtension)
		{
			this.AddException(new ArgumentException(
				"All file extensions must be unique.", nameof(this.VoxelShapeFileExtension)));
		}

		this.Validate(
			this.FileConnectionDuration,
			nameof(this.FileConnectionDuration),
			(value, name) => Duration.ValidateIsDuration(value, name));
	}
}
