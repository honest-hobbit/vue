﻿namespace HQS.VUE.OLD.Utility.Stores;

public class FileStores : AbstractDisposable
{
	private readonly Dictionary<string, VoxelGridFile> voxelGrids =
		new Dictionary<string, VoxelGridFile>(EqualityComparer.ForClass<string>());

	private readonly Dictionary<string, VoxelShapeFile> voxelShapes =
		new Dictionary<string, VoxelShapeFile>(EqualityComparer.ForClass<string>());

	private readonly List<IDisposable> disposables = new List<IDisposable>();

	private readonly FileStoreFactoryOLD storeFactory;

	////private readonly FileStoreFactory storeFactory;

	public FileStores(FileStoreConfig config)
	{
		Ensure.That(config, nameof(config)).IsNotNull();

		this.Config = config;
		this.storeFactory = new FileStoreFactoryOLD(config);

		////this.storeFactory = new FileStoreFactory(config);

		this.VoxelGrids = this.voxelGrids.AsReadOnlyDictionary();
		this.VoxelShapes = this.voxelShapes.AsReadOnlyDictionary();
	}

	public FileStoreConfig Config { get; }

	public IReadOnlyDictionary<string, VoxelGridFile> VoxelGrids { get; }

	public IReadOnlyDictionary<string, VoxelShapeFile> VoxelShapes { get; }

	public void LoadAllVolumesFromCurrentStores(IVolumes volumes)
	{
		this.ValidateNotDisposed();
		Ensure.That(volumes, nameof(volumes)).IsNotNull();

		foreach (var store in this.voxelGrids.Values)
		{
			store.TryLoadVolume(volumes);
		}

		foreach (var store in this.voxelShapes.Values)
		{
			store.TryAttachToLoadedVolume(volumes);
		}
	}

	public void FindAllStoresInDirectory(string directory, SearchOption searchOption)
	{
		this.ValidateNotDisposed();

		directory = Path.GetFullPath(directory);
		if (!PathUtility.IsPathValid(directory))
		{
			throw new IOException($"{directory} is not a valid path.");
		}

		Enumeration.ValidateIsDefined(searchOption, nameof(searchOption));

		if (!Directory.Exists(directory))
		{
			throw new DirectoryNotFoundException($"{directory} does not exist.");
		}

		foreach (var filePath in Directory.EnumerateFiles(directory, "*.*", searchOption))
		{
			var extension = Path.GetExtension(filePath);
			if (extension == this.Config.VoxelGridFileExtension)
			{
				this.GetOrCreateVoxelGridStore(filePath);
			}
			else if (extension == this.Config.VoxelShapeFileExtension)
			{
				this.GetOrCreateVoxelShapeStore(filePath);
			}
		}
	}

	public VoxelGridFile GetOrCreateVoxelGridStore(string path)
	{
		this.ValidateNotDisposed();

		path = PreparePath(path, this.Config.VoxelGridFileExtension);
		return this.voxelGrids.TryGetValue(path, out var result) ? result : this.AddNewVoxelGridStore(path);
	}

	public VoxelShapeFile GetOrCreateVoxelShapeStore(string path)
	{
		this.ValidateNotDisposed();

		path = PreparePath(path, this.Config.VoxelShapeFileExtension);
		return this.voxelShapes.TryGetValue(path, out var result) ? result : this.AddNewVoxelShapeStore(path);
	}

	public VoxelGridFile CreateVoxelGridStore(string path)
	{
		this.ValidateNotDisposed();

		path = PreparePath(path, this.Config.VoxelGridFileExtension);
		if (this.voxelGrids.ContainsKey(path))
		{
			throw new ArgumentException($"{path} is already in use.", nameof(path));
		}

		return this.AddNewVoxelGridStore(path);
	}

	public VoxelShapeFile CreateVoxelShapeStore(string path)
	{
		this.ValidateNotDisposed();

		path = PreparePath(path, this.Config.VoxelShapeFileExtension);
		if (this.voxelGrids.ContainsKey(path))
		{
			throw new ArgumentException($"{path} is already in use.", nameof(path));
		}

		return this.AddNewVoxelShapeStore(path);
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		foreach (var store in this.disposables)
		{
			store.Dispose();
		}

		this.disposables.Clear();
		this.voxelGrids.Clear();
		this.voxelShapes.Clear();
	}

	private static string PreparePath(string path, string fileExtension)
	{
		Debug.Assert(!fileExtension.IsNullOrWhiteSpace());

		path = Path.GetFullPath(path);
		if (!Path.HasExtension(path))
		{
			path = Path.ChangeExtension(path, fileExtension);
		}

		return path;
	}

	private VoxelGridFile AddNewVoxelGridStore(string path)
	{
		var store = this.storeFactory.CreateVoxelGridStore(path);
		var file = new VoxelGridFile(store);
		this.voxelGrids.Add(path, file);
		this.disposables.Add(store.Disposable);
		return file;
	}

	private VoxelShapeFile AddNewVoxelShapeStore(string path)
	{
		var store = this.storeFactory.CreateVoxelShapeStore(path);
		var file = new VoxelShapeFile(store);
		this.voxelShapes.Add(path, file);
		this.disposables.Add(store.Disposable);
		return file;
	}
}
