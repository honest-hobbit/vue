﻿namespace HQS.VUE.OLD.Utility.Stores;

public class VoxelGridFile : IDisposed
{
	private readonly IVoxelGridStore store;

	internal VoxelGridFile(IVoxelGridStore store)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);

		this.store = store;
	}

	/// <inheritdoc />
	public bool IsDisposed => this.store.IsDisposed;

	public string FilePath => this.store.FilePath;

	public Guid VoxelGridKey => this.store.ChunkGridKey;

	public Volume VoxelGrid { get; private set; }

	public void AttachTo(Volume volume)
	{
		this.ValidateNotDisposed();
		if (this.VoxelGrid != null)
		{
			throw new InvalidOperationException(
				$"This {nameof(VoxelGridFile)} has already been attached to a {nameof(Volume)}.");
		}

		Ensure.That(volume, nameof(volume)).IsNotNull();
		if (this.VoxelGridKey != Guid.Empty && volume.Key != this.VoxelGridKey)
		{
			throw new ArgumentException(
				$"Key of {nameof(volume)} does not match {nameof(this.VoxelGridKey)}.", nameof(volume));
		}

		volume.SetVoxelGridStore(this.store);
		this.VoxelGrid = volume;
	}

	public bool TryLoadVolume(IVolumes volumes)
	{
		this.ValidateNotDisposed();
		Ensure.That(volumes, nameof(volumes)).IsNotNull();

		if (this.VoxelGrid != null || this.VoxelGridKey == Guid.Empty)
		{
			return false;
		}

		if (!volumes.Collection.TryGetValue(this.VoxelGridKey, out var volume))
		{
			volume = volumes.CreateVolume(this.store.GetAllChunkGrids().Single());
		}

		if (volume.HasVoxelGridStore)
		{
			return false;
		}

		this.AttachTo(volume);
		return true;
	}
}
