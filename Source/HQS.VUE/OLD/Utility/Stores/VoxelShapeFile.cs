﻿namespace HQS.VUE.OLD.Utility.Stores;

public class VoxelShapeFile : IDisposed
{
	private readonly IVoxelShapeStore store;

	internal VoxelShapeFile(IVoxelShapeStore store)
	{
		Debug.Assert(store != null);
		Debug.Assert(!store.IsDisposed);

		this.store = store;
	}

	/// <inheritdoc />
	public bool IsDisposed => this.store.IsDisposed;

	public string FilePath => this.store.FilePath;

	public Guid VoxelShapeKey => this.store.ChunkGridKey;

	public Volume VoxelShape { get; private set; }

	public void AttachTo(Volume volume)
	{
		this.ValidateNotDisposed();
		if (this.VoxelShape != null)
		{
			throw new InvalidOperationException(
				$"This {nameof(VoxelShapeFile)} has already been attached to a {nameof(Volume)}.");
		}

		Ensure.That(volume, nameof(volume)).IsNotNull();
		if (this.VoxelShapeKey != Guid.Empty && volume.Key != this.VoxelShapeKey)
		{
			throw new ArgumentException(
				$"Key of {nameof(volume)} does not match {nameof(this.VoxelShapeKey)}.", nameof(volume));
		}

		volume.SetVoxelShapeStore(this.store);
	}

	public bool TryAttachToLoadedVolume(IVolumes volumes)
	{
		this.ValidateNotDisposed();
		Ensure.That(volumes, nameof(volumes)).IsNotNull();

		if (this.VoxelShape != null || this.VoxelShapeKey == Guid.Empty)
		{
			return false;
		}

		if (!volumes.Collection.TryGetValue(this.VoxelShapeKey, out var volume) || volume.HasVoxelShapeStore)
		{
			return false;
		}

		this.AttachTo(volume);
		return true;
	}
}
