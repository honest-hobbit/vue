﻿namespace HQS.VUE.OLD.Utility.Particles;

public class VoxelParticlesRecord : IVoxelParticlesRecord, IGranularChangeRecord, IChunkedChangeRecord
{
	private readonly ITypeList<Voxel, VoxelTypeData> voxelTypes;

	private readonly VoxelParticleCombinerRunner particleCombiner;

	private IVoxelChangesBlock block;

	private bool keepAdding;

	private bool keepRemoving;

	private Random shuffleRandom;

	public VoxelParticlesRecord(IVoxelDefinitions definitions, VoxelParticleCombinerTable table, int maxParticles)
		: this(definitions, table, maxParticles, maxParticles)
	{
	}

	public VoxelParticlesRecord(
		IVoxelDefinitions definitions, VoxelParticleCombinerTable table, int maxAddParticles, int maxRemoveParticles)
	{
		if (definitions == null)
		{
			throw new ArgumentNullException(nameof(definitions));
		}

		if (table == null)
		{
			throw new ArgumentNullException(nameof(table));
		}

		if (maxAddParticles < 0)
		{
			throw new ArgumentOutOfRangeException(nameof(maxAddParticles));
		}

		if (maxRemoveParticles < 0)
		{
			throw new ArgumentOutOfRangeException(nameof(maxRemoveParticles));
		}

		this.voxelTypes = definitions.Voxels;
		this.particleCombiner = new VoxelParticleCombinerRunner(
			table, definitions, this.CreateAddParticle, this.CreateRemoveParticle);
		this.AddParticles = new VoxelParticle[maxAddParticles];
		this.RemoveParticles = new VoxelParticle[maxRemoveParticles];
	}

	public bool AnimateReplacing { get; set; }

	public bool OnlySurfaces { get; set; }

	public bool CombineParticles { get; set; }

	public bool ClearIfFull { get; set; }

	public int DontAnimateAddingMaxThreshold { get; set; }

	public int DontAnimateRemovingMaxThreshold { get; set; }

	public int ReduceAddingThreshold { get; set; } = -1;

	public int ReduceRemovingThreshold { get; set; } = -1;

	/// <inheritdoc />
	public VoxelParticle[] AddParticles { get; }

	/// <inheritdoc />
	public VoxelParticle[] RemoveParticles { get; }

	/// <inheritdoc />
	public int AddCount { get; private set; }

	/// <inheritdoc />
	public int RemoveCount { get; private set; }

	/// <inheritdoc />
	public VoxelChangeTiming TimingMode { get; private set; }

	/// <inheritdoc />
	public RecordChangesMode RecordMode { get; private set; }

	/// <inheritdoc />
	public bool IncludeAdjacentVoxels => this.OnlySurfaces;

	/// <inheritdoc />
	public bool StartRecording(Pinned<IVoxelJobInfo> jobCompleted, int voxelsChanged)
	{
		if (voxelsChanged <= 0)
		{
			throw new ArgumentOutOfRangeException(nameof(voxelsChanged));
		}

		this.RecordMode = (this.OnlySurfaces || this.CombineParticles) ? RecordChangesMode.Chunked : RecordChangesMode.Granular;
		this.TimingMode = (jobCompleted.ValueOrNull as IVoxelChangeAnimatorTiming)?.TimingMode ?? VoxelChangeTiming.Immediate;

		if (this.CombineParticles)
		{
			this.particleCombiner.AnimateReplacing = this.AnimateReplacing;
			this.particleCombiner.OnlySurfaces = this.OnlySurfaces;
		}

		this.keepAdding = this.AddParticles.Length > 0;
		this.keepRemoving = this.RemoveParticles.Length > 0;
		this.AddCount = 0;
		this.RemoveCount = 0;
		return this.keepAdding || this.keepRemoving;
	}

	/// <inheritdoc />
	public void PassStarted(IVoxelChangesView view)
	{
		if (view == null)
		{
			throw new ArgumentNullException(nameof(view));
		}
	}

	/// <inheritdoc />
	public void PassStarted(IVoxelChangesBlock block)
	{
		this.block = block ?? throw new ArgumentNullException(nameof(block));
		if (this.CombineParticles)
		{
			this.particleCombiner.Initialize(block);
		}
	}

	/// <inheritdoc />
	public bool RecordChange(Int3 index, Voxel oldVoxel, Voxel newVoxel)
	{
		if (newVoxel.TypeIndex != 0 && (this.AnimateReplacing || oldVoxel.TypeIndex == 0))
		{
			// a voxel was added or replaced
			if (this.keepAdding)
			{
				this.CreateAddParticle(index, newVoxel);
			}
		}
		else if (oldVoxel.TypeIndex != 0 && newVoxel.TypeIndex == 0)
		{
			// a voxel was removed
			if (this.keepRemoving)
			{
				this.CreateRemoveParticle(index, oldVoxel);
			}
		}

		return this.keepAdding || this.keepRemoving;
	}

	/// <inheritdoc />
	public bool NextBlock()
	{
		if (this.CombineParticles)
		{
			return this.particleCombiner.NextBlock();
		}
		else
		{
			var oldVoxels = this.block.OldVoxels.Array;
			var newVoxels = this.block.NewVoxels.Array;
			int max = oldVoxels.Length;

			for (int i = 0; i < max; i++)
			{
				var oldVoxel = oldVoxels[i];
				var newVoxel = newVoxels[i];
				if (oldVoxel == newVoxel)
				{
					continue;
				}

				if (newVoxel.TypeIndex != 0 && (this.AnimateReplacing || oldVoxel.TypeIndex == 0))
				{
					// a voxel was added or replaced
					if (this.keepAdding && this.block.GetNewVoxelIsSurface(i))
					{
						this.CreateAddParticle(this.block.GetVoxelIndex(i), newVoxel);
					}
				}
				else if (oldVoxel.TypeIndex != 0 && newVoxel.TypeIndex == 0)
				{
					// a voxel was removed
					if (this.keepRemoving && this.block.GetOldVoxelIsSurface(i))
					{
						this.CreateRemoveParticle(this.block.GetVoxelIndex(i), oldVoxel);
					}
				}

				if (!this.keepAdding && !this.keepRemoving)
				{
					return false;
				}
			}

			return true;
		}
	}

	/// <inheritdoc />
	public bool PassFinished() => false;

	/// <inheritdoc />
	public void FinishedRecording()
	{
		this.block = null;
		this.keepAdding = false;
		this.keepRemoving = false;

		if (this.CombineParticles)
		{
			this.particleCombiner.Deinitialize();
		}

		if (this.AddCount <= this.DontAnimateAddingMaxThreshold)
		{
			this.AddCount = 0;
		}

		if (this.RemoveCount <= this.DontAnimateRemovingMaxThreshold)
		{
			this.RemoveCount = 0;
		}

		if (this.ClearIfFull)
		{
			if (this.AddCount == this.AddParticles.Length)
			{
				this.AddCount = 0;
			}

			if (this.RemoveCount == this.RemoveParticles.Length)
			{
				this.RemoveCount = 0;
			}
		}

		if (this.ReduceAddingThreshold >= 0 && this.AddCount > this.ReduceAddingThreshold)
		{
			if (this.shuffleRandom == null)
			{
				this.shuffleRandom = new Random();
			}

			this.AddParticles.Shuffle(this.AddCount, this.shuffleRandom);
			this.AddCount = this.ReduceAddingThreshold;
		}

		if (this.ReduceRemovingThreshold >= 0 && this.RemoveCount > this.ReduceRemovingThreshold)
		{
			if (this.shuffleRandom == null)
			{
				this.shuffleRandom = new Random();
			}

			this.RemoveParticles.Shuffle(this.RemoveCount, this.shuffleRandom);
			this.RemoveCount = this.ReduceRemovingThreshold;
		}
	}

	private void CreateAddParticle(Int3 index, Voxel voxel) =>
		this.CreateAddParticle(new VoxelParticle(
			new Vector3(1, 1, 1),
			new Vector3(index.X + .5f, index.Y + .5f, index.Z + .5f),
			this.voxelTypes[voxel].Color));

	private void CreateRemoveParticle(Int3 index, Voxel voxel) =>
		this.CreateRemoveParticle(new VoxelParticle(
			new Vector3(1, 1, 1),
			new Vector3(index.X + .5f, index.Y + .5f, index.Z + .5f),
			this.voxelTypes[voxel].Color));

	private bool CreateAddParticle(VoxelParticle particle)
	{
		this.AddParticles[this.AddCount] = particle;

		this.AddCount++;
		if (this.AddCount == this.AddParticles.Length)
		{
			this.keepAdding = false;
		}

		return this.keepAdding;
	}

	private bool CreateRemoveParticle(VoxelParticle particle)
	{
		this.RemoveParticles[this.RemoveCount] = particle;

		this.RemoveCount++;
		if (this.RemoveCount == this.RemoveParticles.Length)
		{
			this.keepRemoving = false;
		}

		return this.keepRemoving;
	}
}
