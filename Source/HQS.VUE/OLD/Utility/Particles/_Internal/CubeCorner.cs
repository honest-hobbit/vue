﻿namespace HQS.VUE.OLD.Utility.Particles;

internal enum CubeCorner
{
	NNN,
	PNN,
	NPN,
	PPN,
	NNP,
	PNP,
	NPP,
	PPP,
}
