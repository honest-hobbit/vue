﻿namespace HQS.VUE.OLD.Utility.Particles;

internal class VoxelParticleCombiner
{
	private const int CountOfCorners = 8;

	private const int CountOfFaces = 6;

	private const int CountOfEdges = 12;

	private static readonly byte[] FaceCorner1 = new byte[CountOfFaces];

	private static readonly byte[] FaceCorner2 = new byte[CountOfFaces];

	private static readonly byte[] FaceCorner3 = new byte[CountOfFaces];

	private static readonly byte[] FaceCorner4 = new byte[CountOfFaces];

	private static readonly Vector3[] FaceSize = new Vector3[CountOfFaces];

	private static readonly Vector3[] FacePosition = new Vector3[CountOfFaces];

	private static readonly byte[] EdgeCorner1 = new byte[CountOfEdges];

	private static readonly byte[] EdgeCorner2 = new byte[CountOfEdges];

	private static readonly Vector3[] EdgeSize = new Vector3[CountOfEdges];

	private static readonly Vector3[] EdgePosition = new Vector3[CountOfEdges];

	static VoxelParticleCombiner()
	{
		// - - - - - Faces - - - - -
		// face Nxx
		int i = 0;
		FaceCorner1[i] = (byte)CubeCorner.NNN;
		FaceCorner2[i] = (byte)CubeCorner.NPN;
		FaceCorner3[i] = (byte)CubeCorner.NNP;
		FaceCorner4[i] = (byte)CubeCorner.NPP;
		FaceSize[i] = new Vector3(1, 2, 2);
		FacePosition[i] = new Vector3(0.5f, 1, 1);

		// face Pxx
		i = 1;
		FaceCorner1[i] = (byte)CubeCorner.PNN;
		FaceCorner2[i] = (byte)CubeCorner.PPN;
		FaceCorner3[i] = (byte)CubeCorner.PNP;
		FaceCorner4[i] = (byte)CubeCorner.PPP;
		FaceSize[i] = new Vector3(1, 2, 2);
		FacePosition[i] = new Vector3(1.5f, 1, 1);

		// face xNx
		i = 2;
		FaceCorner1[i] = (byte)CubeCorner.NNN;
		FaceCorner2[i] = (byte)CubeCorner.PNN;
		FaceCorner3[i] = (byte)CubeCorner.NNP;
		FaceCorner4[i] = (byte)CubeCorner.PNP;
		FaceSize[i] = new Vector3(2, 1, 2);
		FacePosition[i] = new Vector3(1, 0.5f, 1);

		// face xPx
		i = 3;
		FaceCorner1[i] = (byte)CubeCorner.NPN;
		FaceCorner2[i] = (byte)CubeCorner.PPN;
		FaceCorner3[i] = (byte)CubeCorner.NPP;
		FaceCorner4[i] = (byte)CubeCorner.PPP;
		FaceSize[i] = new Vector3(2, 1, 2);
		FacePosition[i] = new Vector3(1, 1.5f, 1);

		// face xxN
		i = 4;
		FaceCorner1[i] = (byte)CubeCorner.NNN;
		FaceCorner2[i] = (byte)CubeCorner.PNN;
		FaceCorner3[i] = (byte)CubeCorner.NPN;
		FaceCorner4[i] = (byte)CubeCorner.PPN;
		FaceSize[i] = new Vector3(2, 2, 1);
		FacePosition[i] = new Vector3(1, 1, 0.5f);

		// face xxP
		i = 5;
		FaceCorner1[i] = (byte)CubeCorner.NNP;
		FaceCorner2[i] = (byte)CubeCorner.PNP;
		FaceCorner3[i] = (byte)CubeCorner.NPP;
		FaceCorner4[i] = (byte)CubeCorner.PPP;
		FaceSize[i] = new Vector3(2, 2, 1);
		FacePosition[i] = new Vector3(1, 1, 1.5f);

		// - - - - - Edges  - - - - -
		// - - - - - X axis - - - - -
		// edge xNN
		i = 0;
		EdgeCorner1[i] = (byte)CubeCorner.NNN;
		EdgeCorner2[i] = (byte)CubeCorner.PNN;
		EdgeSize[i] = new Vector3(2, 1, 1);
		EdgePosition[i] = new Vector3(1, 0.5f, 0.5f);

		// edge xPN
		i = 1;
		EdgeCorner1[i] = (byte)CubeCorner.NPN;
		EdgeCorner2[i] = (byte)CubeCorner.PPN;
		EdgeSize[i] = new Vector3(2, 1, 1);
		EdgePosition[i] = new Vector3(1, 1.5f, 0.5f);

		// edge xNP
		i = 2;
		EdgeCorner1[i] = (byte)CubeCorner.NNP;
		EdgeCorner2[i] = (byte)CubeCorner.PNP;
		EdgeSize[i] = new Vector3(2, 1, 1);
		EdgePosition[i] = new Vector3(1, 0.5f, 1.5f);

		// edge xPP
		i = 3;
		EdgeCorner1[i] = (byte)CubeCorner.NPP;
		EdgeCorner2[i] = (byte)CubeCorner.PPP;
		EdgeSize[i] = new Vector3(2, 1, 1);
		EdgePosition[i] = new Vector3(1, 1.5f, 1.5f);

		// - - - - - Y axis - - - - -
		// edge NxN
		i = 4;
		EdgeCorner1[i] = (byte)CubeCorner.NNN;
		EdgeCorner2[i] = (byte)CubeCorner.NPN;
		EdgeSize[i] = new Vector3(1, 2, 1);
		EdgePosition[i] = new Vector3(0.5f, 1, 0.5f);

		// edge PxN
		i = 5;
		EdgeCorner1[i] = (byte)CubeCorner.PNN;
		EdgeCorner2[i] = (byte)CubeCorner.PPN;
		EdgeSize[i] = new Vector3(1, 2, 1);
		EdgePosition[i] = new Vector3(1.5f, 1, 0.5f);

		// edge NxP
		i = 6;
		EdgeCorner1[i] = (byte)CubeCorner.NNP;
		EdgeCorner2[i] = (byte)CubeCorner.NPP;
		EdgeSize[i] = new Vector3(1, 2, 1);
		EdgePosition[i] = new Vector3(0.5f, 1, 1.5f);

		// edge PxP
		i = 7;
		EdgeCorner1[i] = (byte)CubeCorner.PNP;
		EdgeCorner2[i] = (byte)CubeCorner.PPP;
		EdgeSize[i] = new Vector3(1, 2, 1);
		EdgePosition[i] = new Vector3(1.5f, 1, 1.5f);

		// - - - - - Z axis - - - - -
		// edge NNx
		i = 8;
		EdgeCorner1[i] = (byte)CubeCorner.NNN;
		EdgeCorner2[i] = (byte)CubeCorner.NNP;
		EdgeSize[i] = new Vector3(1, 1, 2);
		EdgePosition[i] = new Vector3(0.5f, 0.5f, 1);

		// edge PNx
		i = 9;
		EdgeCorner1[i] = (byte)CubeCorner.PNN;
		EdgeCorner2[i] = (byte)CubeCorner.PNP;
		EdgeSize[i] = new Vector3(1, 1, 2);
		EdgePosition[i] = new Vector3(1.5f, 0.5f, 1);

		// edge NPx
		i = 10;
		EdgeCorner1[i] = (byte)CubeCorner.NPN;
		EdgeCorner2[i] = (byte)CubeCorner.NPP;
		EdgeSize[i] = new Vector3(1, 1, 2);
		EdgePosition[i] = new Vector3(0.5f, 1.5f, 1);

		// edge PPx
		i = 11;
		EdgeCorner1[i] = (byte)CubeCorner.PPN;
		EdgeCorner2[i] = (byte)CubeCorner.PPP;
		EdgeSize[i] = new Vector3(1, 1, 2);
		EdgePosition[i] = new Vector3(1.5f, 1.5f, 1);
	}

	private readonly Voxel[] voxel = new Voxel[CountOfCorners];

	private readonly bool[] isSurface = new bool[CountOfCorners];

	// all pending corners must be used in order to be finished
	private readonly bool[] isPending = new bool[CountOfCorners];

	// all corners start as available, but available corner don't have to be used
	// thus isPending is always a subset of isAvailable
	private readonly bool[] isAvailable = new bool[CountOfCorners];

	private readonly PolyTypeIndex[] polyType = new PolyTypeIndex[CountOfCorners];

	private readonly Random random = new Random();

	private readonly byte[] randomizedFace;

	private readonly byte[] randomizedEdge;

	private readonly IVoxelDefinitions definitions;

	private readonly Func<VoxelParticle, bool> createParticle;

	public VoxelParticleCombiner(IVoxelDefinitions definitions, Func<VoxelParticle, bool> createParticle)
	{
		Debug.Assert(definitions != null);
		Debug.Assert(createParticle != null);

		this.definitions = definitions;
		this.createParticle = createParticle;
		this.randomizedFace = Factory.CreateArray(CountOfFaces / 2, i => (byte)(i * 2));
		this.randomizedEdge = Factory.CreateArray(CountOfEdges, i => (byte)i);
	}

	public bool KeepCreatingParticles { get; set; }

	public bool OnlySurfaces { get; set; }

	public Int3 VoxelIndex { get; set; }

	public void SetVoxel(CubeCorner corner, Voxel voxel) => this.voxel[(int)corner] = voxel;

	public Voxel GetVoxel(CubeCorner corner) => this.voxel[(int)corner];

	public void SetIsSurface(CubeCorner corner, bool isSurface) => this.isSurface[(int)corner] = isSurface;

	public bool GetIsSurface(CubeCorner corner) => this.isSurface[(int)corner];

	private bool IsFinished => !this.KeepCreatingParticles || !(
		this.isPending[0] || this.isPending[1] || this.isPending[2] || this.isPending[3] ||
		this.isPending[4] || this.isPending[5] || this.isPending[6] || this.isPending[7]);

	public void GenerateParticles()
	{
		if (!this.KeepCreatingParticles || (this.OnlySurfaces &&
			!this.isSurface[0] && !this.isSurface[1] && !this.isSurface[2] && !this.isSurface[3] &&
			!this.isSurface[4] && !this.isSurface[5] && !this.isSurface[6] && !this.isSurface[7]))
		{
			return;
		}

		this.PrepareData();

		if (this.IsFinished || this.TryFullCube() || this.TryFaces() || this.TryEdges())
		{
			return;
		}

		this.Corners();
	}

	private void PrepareData()
	{
		SetData(0);
		SetData(1);
		SetData(2);
		SetData(3);
		SetData(4);
		SetData(5);
		SetData(6);
		SetData(7);

		void SetData(int corner)
		{
			this.polyType[corner] = this.definitions.Voxels[this.voxel[corner]].PolyType;
			this.isPending[corner] = this.polyType[corner].Index != 0 && (!this.OnlySurfaces || this.isSurface[corner]);
			this.isAvailable[corner] = true;
		}
	}

	// returns true if the entire combiner is finished, otherwise false
	private bool TryFullCube()
	{
		if (this.polyType[0] != this.polyType[1] ||
			this.polyType[0] != this.polyType[2] ||
			this.polyType[0] != this.polyType[3] ||
			this.polyType[0] != this.polyType[4] ||
			this.polyType[0] != this.polyType[5] ||
			this.polyType[0] != this.polyType[6] ||
			this.polyType[0] != this.polyType[7])
		{
			return false;
		}

		this.KeepCreatingParticles = this.createParticle(new VoxelParticle(
			new Vector3(2, 2, 2),
			new Vector3(this.VoxelIndex.X + 1, this.VoxelIndex.Y + 1, this.VoxelIndex.Z + 1),
			this.definitions.Polys[this.polyType[0]].Color));

		return true;
	}

	// returns true if the entire combiner is finished, otherwise false
	private bool TryFaces()
	{
		this.randomizedFace.Shuffle(this.random);

		if (TryAxis(this.randomizedFace[0], out bool result) ||
			TryAxis(this.randomizedFace[1], out result) ||
			TryAxis(this.randomizedFace[2], out result))
		{
			return result;
		}

		return this.IsFinished;

		// returns true to use result and exit early, otherwise false (try next axis)
		bool TryAxis(int i, out bool isFinished)
		{
			if (TryFace(i))
			{
				if (TryFace(i + 1))
				{
					isFinished = true;
					return true;
				}
			}
			else
			{
				if (TryFace(i + 1))
				{
					isFinished = this.IsFinished;
					return true;
				}
			}

			isFinished = false;
			return false;
		}

		// returns true if the face was created, otherwise false
		bool TryFace(int i)
		{
			int corner1 = FaceCorner1[i];
			int corner2 = FaceCorner2[i];
			int corner3 = FaceCorner3[i];
			int corner4 = FaceCorner4[i];

			if (!this.isAvailable[corner1] || !this.isAvailable[corner2] ||
				!this.isAvailable[corner3] || !this.isAvailable[corner4])
			{
				return false;
			}

			if (!this.isPending[corner1] && !this.isPending[corner2] &&
				!this.isPending[corner3] && !this.isPending[corner4])
			{
				return false;
			}

			var polyType = this.polyType[corner1];
			if (polyType != this.polyType[corner2] ||
				polyType != this.polyType[corner3] ||
				polyType != this.polyType[corner4])
			{
				return false;
			}

			this.isAvailable[corner1] = false;
			this.isAvailable[corner2] = false;
			this.isAvailable[corner3] = false;
			this.isAvailable[corner4] = false;
			this.isPending[corner1] = false;
			this.isPending[corner2] = false;
			this.isPending[corner3] = false;
			this.isPending[corner4] = false;

			this.KeepCreatingParticles = this.createParticle(new VoxelParticle(
				FaceSize[i],
				this.VoxelIndex.ToVector() + FacePosition[i],
				this.definitions.Polys[polyType].Color));

			return true;
		}
	}

	// returns true if the entire combiner is finished, otherwise false
	private bool TryEdges()
	{
		this.randomizedEdge.Shuffle(this.random);

		return TryEdge(this.randomizedEdge[0])
			|| TryEdge(this.randomizedEdge[1])
			|| TryEdge(this.randomizedEdge[2])
			|| TryEdge(this.randomizedEdge[3])
			|| TryEdge(this.randomizedEdge[4])
			|| TryEdge(this.randomizedEdge[5])
			|| TryEdge(this.randomizedEdge[6])
			|| TryEdge(this.randomizedEdge[7])
			|| TryEdge(this.randomizedEdge[8])
			|| TryEdge(this.randomizedEdge[9])
			|| TryEdge(this.randomizedEdge[10])
			|| TryEdge(this.randomizedEdge[11]);

		// returns true if the entire combiner is finished, otherwise false
		// (this is different than TryFaces nested TryFace method)
		bool TryEdge(int i)
		{
			int corner1 = EdgeCorner1[i];
			int corner2 = EdgeCorner2[i];

			if (!this.isAvailable[corner1] || !this.isAvailable[corner2])
			{
				return false;
			}

			if (!this.isPending[corner1] && !this.isPending[corner2])
			{
				return false;
			}

			if (this.polyType[corner1] != this.polyType[corner2])
			{
				return false;
			}

			this.isAvailable[corner1] = false;
			this.isAvailable[corner2] = false;
			this.isPending[corner1] = false;
			this.isPending[corner2] = false;

			this.KeepCreatingParticles = this.createParticle(new VoxelParticle(
				EdgeSize[i],
				this.VoxelIndex.ToVector() + EdgePosition[i],
				this.definitions.Polys[this.polyType[corner1]].Color));

			return this.IsFinished;
		}
	}

	private void Corners()
	{
		float x = this.VoxelIndex.X + .5f;
		float y = this.VoxelIndex.Y + .5f;
		float z = this.VoxelIndex.Z + .5f;

		if (TryCorner(CubeCorner.NNN, 0, 0, 0) ||
			TryCorner(CubeCorner.PNN, 1, 0, 0) ||
			TryCorner(CubeCorner.NPN, 0, 1, 0) ||
			TryCorner(CubeCorner.PPN, 1, 1, 0) ||
			TryCorner(CubeCorner.NNP, 0, 0, 1) ||
			TryCorner(CubeCorner.PNP, 1, 0, 1) ||
			TryCorner(CubeCorner.NPP, 0, 1, 1) ||
			TryCorner(CubeCorner.PPP, 1, 1, 1))
		{
			return;
		}

		bool TryCorner(CubeCorner corner, float xOffset, float yOffset, float zOffset)
		{
			// a pending corner is always available and not an empty poly type
			if (this.isPending[(int)corner])
			{
				this.KeepCreatingParticles = this.createParticle(new VoxelParticle(
					new Vector3(1, 1, 1),
					new Vector3(x + xOffset, y + yOffset, z + zOffset),
					this.definitions.Polys[this.polyType[(int)corner]].Color));
			}

			return !this.KeepCreatingParticles;
		}
	}
}
