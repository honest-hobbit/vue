﻿namespace HQS.VUE.OLD.Utility.Particles;

public class VoxelParticleCombinerTable : IReadOnlyList<int>
{
	// these are grouped into segments of 8 defining the 8 corners of the VoxelParticleCombiner in the order of;
	// NNN, PNN, NPN, PPN, NNP, PNP, NPP, PPP (aka the binary counting sequence)
	private readonly ushort[] indices;

	public VoxelParticleCombinerTable(CubeArrayIndexer blockIndexer)
	{
		this.indices = new ushort[blockIndexer.Length];
		int sideLength = blockIndexer.SideLength;

		int index = 0;
		for (int iX = 0; iX < sideLength; iX += 2)
		{
			for (int iY = 0; iY < sideLength; iY += 2)
			{
				for (int iZ = 0; iZ < sideLength; iZ += 2)
				{
					this.indices[index + (int)CubeCorner.NNN] = (ushort)blockIndexer[iX + 0, iY + 0, iZ + 0];
					this.indices[index + (int)CubeCorner.PNN] = (ushort)blockIndexer[iX + 1, iY + 0, iZ + 0];
					this.indices[index + (int)CubeCorner.NPN] = (ushort)blockIndexer[iX + 0, iY + 1, iZ + 0];
					this.indices[index + (int)CubeCorner.PPN] = (ushort)blockIndexer[iX + 1, iY + 1, iZ + 0];
					this.indices[index + (int)CubeCorner.NNP] = (ushort)blockIndexer[iX + 0, iY + 0, iZ + 1];
					this.indices[index + (int)CubeCorner.PNP] = (ushort)blockIndexer[iX + 1, iY + 0, iZ + 1];
					this.indices[index + (int)CubeCorner.NPP] = (ushort)blockIndexer[iX + 0, iY + 1, iZ + 1];
					this.indices[index + (int)CubeCorner.PPP] = (ushort)blockIndexer[iX + 1, iY + 1, iZ + 1];
					index += 8;
				}
			}
		}
	}

	public int this[int index] => this.indices[index];

	public int Count => this.indices.Length;

	/// <inheritdoc />
	public IEnumerator<int> GetEnumerator() => this.indices.Select(x => (int)x).GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
