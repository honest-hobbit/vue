﻿namespace HQS.VUE.OLD.Utility.Particles;

[SuppressMessage("Design", "CA1051", Justification = "Mutable tuple-like struct.")]
public struct VoxelParticle : IEquatable<VoxelParticle>
{
	public VoxelParticle(Vector3 size, Vector3 position, ColorRGB color)
	{
		this.Size = size;
		this.Position = position;
		this.Color = color;
	}

	public Vector3 Size;

	public Vector3 Position;

	public ColorRGB Color;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(VoxelParticle other) =>
		this.Size == other.Size && this.Position == other.Position && this.Color == other.Color;

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Size, this.Position, this.Color);

	public static bool operator ==(VoxelParticle left, VoxelParticle right) => left.Equals(right);

	public static bool operator !=(VoxelParticle left, VoxelParticle right) => !(left == right);
}
