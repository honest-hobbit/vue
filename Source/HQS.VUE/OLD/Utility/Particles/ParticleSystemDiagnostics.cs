﻿namespace HQS.VUE.OLD.Utility.Particles;

public readonly struct ParticleSystemDiagnostics
{
	public ParticleSystemDiagnostics(TimeSpan emitDuration, int particlesEmitted, int totalActiveParticles)
	{
		this.EmitDuration = emitDuration;
		this.ParticlesEmitted = particlesEmitted;
		this.TotalActiveParticles = totalActiveParticles;
	}

	public static int AverageStringLength => 100;

	public TimeSpan EmitDuration { get; }

	public int ParticlesEmitted { get; }

	public int TotalActiveParticles { get; }

	public override string ToString()
	{
		var builder = new StringBuilder(AverageStringLength);
		this.AddToBuilder(builder);
		return builder.ToString();
	}

	public void AddToBuilder(StringBuilder builder)
	{
		Debug.Assert(builder != null);

		builder.AppendLine($"{nameof(this.EmitDuration)}: {this.EmitDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.ParticlesEmitted)}: {this.ParticlesEmitted:n0}");
		builder.AppendLine($"{nameof(this.TotalActiveParticles)}: {this.TotalActiveParticles:n0}");
	}
}
