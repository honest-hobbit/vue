﻿namespace HQS.VUE.OLD.Utility.Particles;

public class VoxelParticleCombinerRunner
{
	private readonly VoxelParticleCombinerTable table;

	private readonly VoxelParticleCombiner addCombiner;

	private readonly VoxelParticleCombiner removeCombiner;

	private IVoxelChangesBlock block;

	private ReadOnlyArray<Voxel> oldVoxels;

	private ReadOnlyArray<Voxel> newVoxels;

	public VoxelParticleCombinerRunner(
		VoxelParticleCombinerTable table,
		IVoxelDefinitions definitions,
		Func<VoxelParticle, bool> createAddParticle,
		Func<VoxelParticle, bool> createRemoveParticle)
	{
		Debug.Assert(table != null);
		Debug.Assert(definitions != null);
		Debug.Assert(createAddParticle != null);
		Debug.Assert(createRemoveParticle != null);

		this.table = table;
		this.addCombiner = new VoxelParticleCombiner(definitions, createAddParticle);
		this.removeCombiner = new VoxelParticleCombiner(definitions, createRemoveParticle);
	}

	public bool IsInitialized => this.block != null;

	public bool AnimateReplacing { get; set; }

	public bool OnlySurfaces
	{
		get => this.addCombiner.OnlySurfaces;
		set
		{
			this.addCombiner.OnlySurfaces = value;
			this.removeCombiner.OnlySurfaces = value;
		}
	}

	public void Initialize(IVoxelChangesBlock block)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(block != null);
		Debug.Assert(block.OldVoxels.Array.Length == this.table.Count);
		Debug.Assert(block.NewVoxels.Array.Length == this.table.Count);

		this.block = block;
		this.oldVoxels = block.OldVoxels.Array;
		this.newVoxels = block.NewVoxels.Array;
		this.addCombiner.KeepCreatingParticles = true;
		this.removeCombiner.KeepCreatingParticles = true;
	}

	public void Deinitialize()
	{
		this.block = null;
		this.oldVoxels = default;
		this.newVoxels = default;
	}

	public bool NextBlock()
	{
		Debug.Assert(this.IsInitialized);

		int max = this.table.Count;
		if (!this.OnlySurfaces)
		{
			// IsSurface does not need to be assigned
			for (int i = 0; i < max; i += 8)
			{
				this.HandleCornerVoxel(i, CubeCorner.NNN);
				this.HandleCornerVoxel(i, CubeCorner.PNN);
				this.HandleCornerVoxel(i, CubeCorner.NPN);
				this.HandleCornerVoxel(i, CubeCorner.PPN);
				this.HandleCornerVoxel(i, CubeCorner.NNP);
				this.HandleCornerVoxel(i, CubeCorner.PNP);
				this.HandleCornerVoxel(i, CubeCorner.NPP);
				this.HandleCornerVoxel(i, CubeCorner.PPP);
				if (!this.GenerateParticles(i))
				{
					return false;
				}
			}
		}
		else
		{
			// IsSurface must be assigned
			for (int i = 0; i < max; i += 8)
			{
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.NNN);
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.PNN);
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.NPN);
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.PPN);
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.NNP);
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.PNP);
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.NPP);
				this.HandleCornerVoxelAndIsSurface(i, CubeCorner.PPP);
				if (!this.GenerateParticles(i))
				{
					return false;
				}
			}
		}

		return true;
	}

	private bool GenerateParticles(int index)
	{
		var voxelIndex = this.block.GetVoxelIndex(this.table[index]);
		this.addCombiner.VoxelIndex = voxelIndex;
		this.removeCombiner.VoxelIndex = voxelIndex;
		this.addCombiner.GenerateParticles();
		this.removeCombiner.GenerateParticles();
		return this.addCombiner.KeepCreatingParticles || this.removeCombiner.KeepCreatingParticles;
	}

	private void HandleCornerVoxel(int loopIndex, CubeCorner corner)
	{
		int index = this.table[loopIndex + (int)corner];
		var oldVoxel = this.oldVoxels[index];
		var newVoxel = this.newVoxels[index];

		if (oldVoxel == newVoxel)
		{
			// nothing changed
			this.addCombiner.SetVoxel(corner, Voxel.Empty);
			this.removeCombiner.SetVoxel(corner, Voxel.Empty);
			return;
		}

		if (newVoxel.TypeIndex != 0 && (this.AnimateReplacing || oldVoxel.TypeIndex == 0))
		{
			// a voxel was added or replaced
			this.addCombiner.SetVoxel(corner, newVoxel);
			this.removeCombiner.SetVoxel(corner, Voxel.Empty);
		}
		else if (oldVoxel.TypeIndex != 0 && newVoxel.TypeIndex == 0)
		{
			// a voxel was removed
			this.removeCombiner.SetVoxel(corner, oldVoxel);
			this.addCombiner.SetVoxel(corner, Voxel.Empty);
		}
	}

	private void HandleCornerVoxelAndIsSurface(int loopIndex, CubeCorner corner)
	{
		int index = this.table[loopIndex + (int)corner];
		var oldVoxel = this.oldVoxels[index];
		var newVoxel = this.newVoxels[index];

		if (oldVoxel == newVoxel)
		{
			// nothing changed
			this.addCombiner.SetVoxel(corner, Voxel.Empty);
			this.addCombiner.SetIsSurface(corner, false);

			this.removeCombiner.SetVoxel(corner, Voxel.Empty);
			this.removeCombiner.SetIsSurface(corner, false);
			return;
		}

		if (newVoxel.TypeIndex != 0 && (this.AnimateReplacing || oldVoxel.TypeIndex == 0))
		{
			// a voxel was added or replaced
			this.addCombiner.SetVoxel(corner, newVoxel);
			this.addCombiner.SetIsSurface(corner, this.block.GetNewVoxelIsSurface(index));

			this.removeCombiner.SetVoxel(corner, Voxel.Empty);
			this.removeCombiner.SetIsSurface(corner, false);
		}
		else if (oldVoxel.TypeIndex != 0 && newVoxel.TypeIndex == 0)
		{
			// a voxel was removed
			this.removeCombiner.SetVoxel(corner, oldVoxel);
			this.removeCombiner.SetIsSurface(corner, this.block.GetOldVoxelIsSurface(index));

			this.addCombiner.SetVoxel(corner, Voxel.Empty);
			this.addCombiner.SetIsSurface(corner, false);
		}
	}
}
