﻿namespace HQS.VUE.OLD.Utility.Particles;

public interface IVoxelParticlesRecord : IVoxelChangesRecord, IVoxelChangeAnimatorTiming
{
	VoxelParticle[] AddParticles { get; }

	VoxelParticle[] RemoveParticles { get; }

	int AddCount { get; }

	int RemoveCount { get; }
}
