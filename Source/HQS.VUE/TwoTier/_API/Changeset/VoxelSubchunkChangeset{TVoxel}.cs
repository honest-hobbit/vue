﻿namespace HQS.VUE.TwoTier;

internal readonly struct VoxelSubchunkChangeset<TVoxel> : IPinnable<VoxelSubchunkChangeset<TVoxel>, VoxelSubchunkChangeset<TVoxel>.Pin>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly VoxelSubchunk<TVoxel>.Pin previous;

	private readonly VoxelSubchunk<TVoxel>.Pin next;

	private VoxelSubchunkChangeset(VoxelSubchunk<TVoxel>.Pin previous, VoxelSubchunk<TVoxel>.Pin next)
	{
		this.previous = previous;
		this.next = next;
	}

	/// <inheritdoc />
	public bool HasData => this.previous.Data.HasData;

	public VoxelSubchunk<TVoxel> Previous => this.previous.Data;

	public VoxelSubchunk<TVoxel> Next => this.next.Data;

	/// <inheritdoc />
	public Pin CreatePin()
	{
		Debug.Assert(this.HasData);

		return new Pin(this.previous.Data.CreatePin(), this.next.Data.CreatePin());
	}

	public readonly struct Pin : IPin<VoxelSubchunkChangeset<TVoxel>, Pin>
	{
		public Pin(VoxelSubchunk<TVoxel>.Pin previous, VoxelSubchunk<TVoxel>.Pin next)
		{
			Debug.Assert(previous.Data.HasData);
			Debug.Assert(next.Data.HasData);
			Debug.Assert(previous.Data != next.Data);

			this.Data = new VoxelSubchunkChangeset<TVoxel>(previous, next);
		}

		/// <inheritdoc />
		public VoxelSubchunkChangeset<TVoxel> Data { get; }

		/// <inheritdoc />
		public bool HasData => this.Data.HasData;

		/// <inheritdoc />
		public Pin CreatePin() => this.Data.CreatePin();

		/// <inheritdoc />
		public void Dispose()
		{
			this.Data.previous.Dispose();
			this.Data.next.Dispose();
		}
	}
}
