﻿namespace HQS.VUE.TwoTier;

internal static class VoxelChunkChangesetExtensions
{
	public static bool TryGetChanged<TVoxel>(
		this ChunkChangeset<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin> changeset,
		int subchunkIndex,
		out VoxelSubchunkChangeset<TVoxel>.Pin subchunks)
		where TVoxel : struct, IEquatable<TVoxel>
	{
		Debug.Assert(changeset.HasData);
		Debug.Assert(changeset.Previous.ChunkConfig == changeset.Next.ChunkConfig);
		Debug.Assert(subchunkIndex >= 0);

		var previousSubchunk = changeset.Previous.GetSubchunk(subchunkIndex);
		var nextSubchunk = changeset.Next.GetSubchunk(subchunkIndex);

		if (previousSubchunk == nextSubchunk)
		{
			subchunks = default;
			return false;
		}
		else
		{
			subchunks = new VoxelSubchunkChangeset<TVoxel>.Pin(
				previousSubchunk.CreatePin(), nextSubchunk.CreatePin());
			return true;
		}
	}

	public static void GetChangeCounts<TVoxel>(
		this ChunkChangeset<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin> changeset,
		out int subchunksChanged,
		out int voxelsChanged)
		where TVoxel : struct, IEquatable<TVoxel>
	{
		Debug.Assert(changeset.HasData);
		Debug.Assert(changeset.Previous.ChunkConfig == changeset.Next.ChunkConfig);

		subchunksChanged = 0;
		voxelsChanged = 0;

		var previous = changeset.Previous;
		var next = changeset.Next;
		var chunkConfig = previous.ChunkConfig;
		int subchunksLength = chunkConfig.SubchunkIndexer3D.Length;
		int voxelsLength = chunkConfig.CellIndexer3D.Length;

		if (previous.IsUniform)
		{
			if (next.IsUniform)
			{
				Debug.Assert(!previous.UniformVoxel.Equals(next.UniformVoxel));

				subchunksChanged = subchunksLength;
				voxelsChanged = subchunksLength * voxelsLength;
			}
			else
			{
				var uniformVoxel = previous.UniformVoxel;
				var subchunks = next.Subchunks;

				Debug.Assert(subchunks.Length == subchunksLength);

				for (int i = 0; i < subchunksLength; i++)
				{
					GetChangeCounts(uniformVoxel, subchunks[i], ref subchunksChanged, ref voxelsChanged);
				}
			}
		}
		else
		{
			if (next.IsUniform)
			{
				var subchunks = previous.Subchunks;
				var uniformVoxel = next.UniformVoxel;

				Debug.Assert(subchunks.Length == subchunksLength);

				for (int i = 0; i < subchunksLength; i++)
				{
					GetChangeCounts(uniformVoxel, subchunks[i], ref subchunksChanged, ref voxelsChanged);
				}
			}
			else
			{
				var previousSubchunks = previous.Subchunks;
				var nextSubchunks = next.Subchunks;

				Debug.Assert(previousSubchunks.Length == subchunksLength);
				Debug.Assert(nextSubchunks.Length == subchunksLength);

				for (int i = 0; i < subchunksLength; i++)
				{
					GetChangeCountsBothNotUniform(
						previousSubchunks[i], nextSubchunks[i], ref subchunksChanged, ref voxelsChanged);
				}
			}
		}

		Debug.Assert(subchunksChanged >= 1);
		Debug.Assert(voxelsChanged >= 1);

		void GetChangeCounts(TVoxel uniformVoxel, VoxelSubchunk<TVoxel> subchunk, ref int subchunksChanged, ref int voxelsChanged)
		{
			Debug.Assert(subchunk.HasData);

			if (subchunk.IsUniform)
			{
				if (!subchunk.UniformVoxel.Equals(uniformVoxel))
				{
					subchunksChanged++;
					voxelsChanged += voxelsLength;
				}
			}
			else
			{
				subchunksChanged++;
				voxelsChanged += GetVoxelsChanged(uniformVoxel, subchunk.Voxels);
			}
		}

		void GetChangeCountsBothNotUniform(
			VoxelSubchunk<TVoxel> subchunkA,
			VoxelSubchunk<TVoxel> subchunkB,
			ref int subchunksChanged,
			ref int voxelsChanged)
		{
			if (subchunkA.IsUniform)
			{
				if (subchunkB.IsUniform)
				{
					if (!subchunkA.UniformVoxel.Equals(subchunkB.UniformVoxel))
					{
						subchunksChanged++;
						voxelsChanged += voxelsLength;
					}
				}
				else
				{
					subchunksChanged++;
					voxelsChanged += GetVoxelsChanged(subchunkA.UniformVoxel, subchunkB.Voxels);
				}
			}
			else
			{
				if (subchunkB.IsUniform)
				{
					subchunksChanged++;
					voxelsChanged += GetVoxelsChanged(subchunkB.UniformVoxel, subchunkA.Voxels);
				}
				else
				{
					var voxelsA = subchunkA.Voxels;
					var voxelsB = subchunkB.Voxels;

					Debug.Assert(voxelsA.Length == voxelsLength);
					Debug.Assert(voxelsB.Length == voxelsLength);

					if (voxelsA != voxelsB)
					{
						subchunksChanged++;

						for (int i = 0; i < voxelsLength; i++)
						{
							if (!voxelsA[i].Equals(voxelsB[i]))
							{
								voxelsChanged++;
							}
						}
					}
				}
			}
		}

		int GetVoxelsChanged(TVoxel uniformVoxel, ReadOnlyArray<TVoxel> voxels)
		{
			Debug.Assert(!voxels.IsDefault);
			Debug.Assert(voxels.Length == voxelsLength);

			int voxelsChanged = 0;
			for (int i = 0; i < voxelsLength; i++)
			{
				if (!voxels[i].Equals(uniformVoxel))
				{
					voxelsChanged++;
				}
			}

			Debug.Assert(voxelsChanged >= 1);

			return voxelsChanged;
		}
	}
}
