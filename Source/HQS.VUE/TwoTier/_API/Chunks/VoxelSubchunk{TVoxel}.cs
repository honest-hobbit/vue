﻿namespace HQS.VUE.TwoTier;

internal readonly struct VoxelSubchunk<TVoxel> :
	IEquatable<VoxelSubchunk<TVoxel>>, IPinnable<VoxelSubchunk<TVoxel>, VoxelSubchunk<TVoxel>.Pin>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly RawVoxelSubchunk<TVoxel> rawSubchunk;

	public VoxelSubchunk(TVoxel uniformVoxel)
	{
		this.rawSubchunk = new RawVoxelSubchunk<TVoxel>(uniformVoxel);
	}

	internal VoxelSubchunk(RawVoxelSubchunk<TVoxel> subchunk)
	{
		this.rawSubchunk = subchunk;
	}

	/// <inheritdoc />
	public bool HasData => this.rawSubchunk.HasData;

	public bool HasVoxels => this.rawSubchunk.HasVoxels;

	public bool IsUniform => this.rawSubchunk.IsUniform;

	public TVoxel UniformVoxel => this.rawSubchunk.UniformVoxel;

	public ReadOnlyArray<TVoxel> Voxels => new ReadOnlyArray<TVoxel>(this.rawSubchunk.Voxels.ValueOrNull);

	public static bool operator ==(VoxelSubchunk<TVoxel> lhs, VoxelSubchunk<TVoxel> rhs) => lhs.Equals(rhs);

	public static bool operator !=(VoxelSubchunk<TVoxel> lhs, VoxelSubchunk<TVoxel> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(VoxelSubchunk<TVoxel> other) => this.rawSubchunk.Equals(other.rawSubchunk);

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.rawSubchunk.GetHashCode();

	/// <inheritdoc />
	public Pin CreatePin()
	{
		Debug.Assert(this.HasData);

		return new Pin(this.rawSubchunk.CreatePin());
	}

	public readonly struct Pin : IPin<VoxelSubchunk<TVoxel>, Pin>
	{
		public Pin(TVoxel uniformVoxel)
		{
			this.Data = new VoxelSubchunk<TVoxel>(uniformVoxel);
		}

		public Pin(RawVoxelSubchunk<TVoxel> subchunk)
		{
			Debug.Assert(subchunk.HasData);

			this.Data = new VoxelSubchunk<TVoxel>(subchunk);
		}

		/// <inheritdoc />
		public VoxelSubchunk<TVoxel> Data { get; }

		/// <inheritdoc />
		public bool HasData => this.Data.HasData;

		/// <inheritdoc />
		public Pin CreatePin() => this.Data.CreatePin();

		/// <inheritdoc />
		public void Dispose() => this.Data.rawSubchunk.Voxels.Dispose();
	}
}
