﻿namespace HQS.VUE.TwoTier;

internal readonly struct VoxelSubchunkArray<TVoxel> :
	IEquatable<VoxelSubchunkArray<TVoxel>>, IReadOnlyList<VoxelSubchunk<TVoxel>>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly RawVoxelSubchunk<TVoxel>[] array;

	public VoxelSubchunkArray(RawVoxelSubchunk<TVoxel>[] array)
	{
		this.array = array;
	}

	public bool IsNull => this.array == null;

	public int Length => this.array.Length;

	/// <inheritdoc />
	int IReadOnlyCollection<VoxelSubchunk<TVoxel>>.Count => this.array.Length;

	/// <inheritdoc />
	public VoxelSubchunk<TVoxel> this[int index] => new VoxelSubchunk<TVoxel>(this.array[index]);

	public static bool operator ==(VoxelSubchunkArray<TVoxel> lhs, VoxelSubchunkArray<TVoxel> rhs) => lhs.Equals(rhs);

	public static bool operator !=(VoxelSubchunkArray<TVoxel> lhs, VoxelSubchunkArray<TVoxel> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(VoxelSubchunkArray<TVoxel> other) => this.array == other.array;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.array.GetHashCodeNullSafe();

	/// <inheritdoc />
	public IEnumerator<VoxelSubchunk<TVoxel>> GetEnumerator()
	{
		int max = this.array.Length;
		for (int i = 0; i < max; i++)
		{
			yield return new VoxelSubchunk<TVoxel>(this.array[i]);
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
