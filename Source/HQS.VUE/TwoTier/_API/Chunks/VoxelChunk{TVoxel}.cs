﻿namespace HQS.VUE.TwoTier;

internal readonly struct VoxelChunk<TVoxel> :
	IPinnable<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>,
	IChunkConfigDependent<ChunkConfig>,
	IEquatable<VoxelChunk<TVoxel>>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly RawVoxelChunk<TVoxel> rawChunk;

	public VoxelChunk(ChunkConfig chunkConfig, TVoxel uniformVoxel)
	{
		Debug.Assert(chunkConfig.IsAssigned);

		this.rawChunk = new RawVoxelChunk<TVoxel>(chunkConfig, uniformVoxel);
	}

	private VoxelChunk(RawVoxelChunk<TVoxel> chunk)
	{
		this.rawChunk = chunk;
	}

	/// <inheritdoc />
	public ChunkConfig ChunkConfig => this.rawChunk.ChunkConfig;

	/// <inheritdoc />
	public bool HasData => this.rawChunk.HasData;

	public bool HasSubchunks => this.rawChunk.HasSubchunks;

	public bool IsUniform => this.rawChunk.IsUniform;

	public TVoxel UniformVoxel => this.rawChunk.UniformVoxel;

	public VoxelSubchunkArray<TVoxel> Subchunks => new VoxelSubchunkArray<TVoxel>(this.rawChunk.Subchunks.ValueOrNull);

	public static bool operator ==(VoxelChunk<TVoxel> lhs, VoxelChunk<TVoxel> rhs) => lhs.Equals(rhs);

	public static bool operator !=(VoxelChunk<TVoxel> lhs, VoxelChunk<TVoxel> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(VoxelChunk<TVoxel> other) => this.rawChunk.Equals(other.rawChunk);

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.rawChunk.GetHashCode();

	public VoxelSubchunk<TVoxel> GetSubchunk(int index)
	{
		Debug.Assert(this.HasData);

		return this.rawChunk.GetSubchunk(index);
	}

	/// <inheritdoc />
	public Pin CreatePin()
	{
		Debug.Assert(this.HasData);

		return new Pin(this.rawChunk.CreatePin());
	}

	public readonly struct Pin : IPin<VoxelChunk<TVoxel>, Pin>, IDisposable
	{
		public Pin(ChunkConfig chunkConfig, TVoxel uniformVoxel)
		{
			Debug.Assert(chunkConfig.IsAssigned);

			this.Data = new VoxelChunk<TVoxel>(chunkConfig, uniformVoxel);
		}

		internal Pin(RawVoxelChunk<TVoxel> subchunk)
		{
			Debug.Assert(subchunk.HasData);

			this.Data = new VoxelChunk<TVoxel>(subchunk);
		}

		/// <inheritdoc />
		public VoxelChunk<TVoxel> Data { get; }

		/// <inheritdoc />
		public bool HasData => this.Data.HasData;

		/// <inheritdoc />
		public Pin CreatePin() => this.Data.CreatePin();

		/// <inheritdoc />
		public void Dispose() => this.Data.rawChunk.Subchunks.Dispose();
	}

	public class Builder :
		IChunkConfigDependent<ChunkConfig>, IChunkBuilder<VoxelChunk<TVoxel>, Pin>, IDeinitializable
	{
		private readonly IKeyResolver<ChunkConfig, IPinPool<RawVoxelSubchunk<TVoxel>[]>> subchunkArrayPools;

		private readonly IKeyResolver<ChunkConfig, IPinPool<TVoxel[]>> voxelArrayPools;

		private IPinPool<RawVoxelSubchunk<TVoxel>[]> subchunkArrays;

		private IPinPool<TVoxel[]> voxelArrays;

		private int subchunkArrayLength;

		private int voxelArrayLength;

		private RawVoxelChunk<TVoxel> chunk;

		// this is kept separate of the chunk as an optimization to avoid going through the pin to get the array
		// this is dangerous and should only be done with significant care
		private RawVoxelSubchunk<TVoxel>[] subchunks;

		private Pin previousChunkPin;

		// this is kept separate of the chunk as an optimization to avoid going through the pin to get the array
		// this is dangerous and should only be done with significant care
		private RawVoxelSubchunk<TVoxel>[] previousSubchunks;

		public Builder(
			IKeyResolver<ChunkConfig, IPinPool<RawVoxelSubchunk<TVoxel>[]>> subchunkArrayPools,
			IKeyResolver<ChunkConfig, IPinPool<TVoxel[]>> voxelArrayPools)
		{
			Debug.Assert(subchunkArrayPools != null);
			Debug.Assert(voxelArrayPools != null);

			this.subchunkArrayPools = subchunkArrayPools;
			this.voxelArrayPools = voxelArrayPools;
		}

		/// <inheritdoc />
		public ChunkConfig ChunkConfig { get; private set; }

		public VoxelChunk<TVoxel> Preview => new VoxelChunk<TVoxel>(this.chunk);

		public void Clear()
		{
			this.ChunkConfig = default;
			this.subchunkArrays = null;
			this.voxelArrays = null;
			this.subchunkArrayLength = 0;
			this.voxelArrayLength = 0;
			this.chunk.Subchunks.Dispose();
			this.chunk = default;
			this.subchunks = null;
			this.previousChunkPin.Dispose();
			this.previousChunkPin = default;
			this.previousSubchunks = null;
		}

		/// <inheritdoc />
		void IDeinitializable.Deinitialize() => this.Clear();

		/// <inheritdoc />
		public Pin Build(bool optimize = true)
		{
			this.RequireChunkConfigIsSet();

			if (optimize)
			{
				this.Optimize();
			}

			var result = new Pin(this.chunk.CreatePin());
			this.Clear();
			return result;
		}

		/// <inheritdoc />
		public bool TryBuildChangeset(out ChunkChangeset<VoxelChunk<TVoxel>, Pin>.Pin changesetPin)
		{
			this.RequireChunkConfigIsSet();
			Debug.Assert(this.previousChunkPin.Data.HasData);

			this.Optimize();

			if (this.previousChunkPin.Data.rawChunk == this.chunk)
			{
				changesetPin = default;
				this.Clear();
				return false;
			}
			else
			{
				changesetPin = new ChunkChangeset<VoxelChunk<TVoxel>, Pin>.Pin(
					this.previousChunkPin.Data.CreatePin(),
					new Pin(this.chunk.CreatePin()));
				this.Clear();
				return true;
			}
		}

		/// <inheritdoc />
		public void SetCopyOf(VoxelChunk<TVoxel> copyingChunk)
		{
			Debug.Assert(copyingChunk.HasData);

			this.SetCopyOf(copyingChunk.CreatePin());
		}

		/// <inheritdoc />
		public void SetCopyOf(Pin copyingPin)
		{
			Debug.Assert(copyingPin.Data.HasData);

			var copyingRawChunk = copyingPin.Data.rawChunk;
			if (copyingRawChunk.IsUniform)
			{
				this.SetUniform(copyingRawChunk.ChunkConfig, copyingRawChunk.UniformVoxel);

				// storing the chunk pin after because the above will call Clear
				this.previousChunkPin = copyingPin;
			}
			else
			{
				this.ClearAndSetNewChunkConfig(copyingRawChunk.ChunkConfig);
				this.AssignSubchunksArray();

				// storing the chunk pin after because the above will call Clear
				this.previousChunkPin = copyingPin;
				this.previousSubchunks = copyingRawChunk.Subchunks.Value;

				Debug.Assert(this.subchunkArrayLength > 0);
				Debug.Assert(this.subchunks != null);
				Debug.Assert(this.subchunks.Length == this.subchunkArrayLength);
				Debug.Assert(this.previousSubchunks.Length == this.subchunkArrayLength);

				for (int i = 0; i < this.subchunkArrayLength; i++)
				{
					this.subchunks[i] = this.previousSubchunks[i].CreatePin();
				}
			}
		}

		public void SetUniform(ChunkConfig chunkConfig, TVoxel uniformVoxel)
		{
			Debug.Assert(chunkConfig.IsAssigned);

			this.ClearAndSetNewChunkConfig(chunkConfig);
			this.chunk = new RawVoxelChunk<TVoxel>(chunkConfig, uniformVoxel);
		}

		public void SetSubchunkUniform(int subchunkIndex, TVoxel uniformVoxel)
		{
			this.RequireChunkConfigIsSet();
			this.RequireSubchunkIndexIsValid(subchunkIndex);

			if (this.chunk.IsUniform)
			{
				if (this.chunk.UniformVoxel.Equals(uniformVoxel))
				{
					return;
				}

				this.AssignSubchunksArrayAndFillWithUniformSubchunks(this.chunk.UniformVoxel);

				this.subchunks[subchunkIndex] = new RawVoxelSubchunk<TVoxel>(uniformVoxel);
			}
			else
			{
				this.subchunks[subchunkIndex].Voxels.Dispose();
				this.subchunks[subchunkIndex] = new RawVoxelSubchunk<TVoxel>(uniformVoxel);
			}
		}

		public VoxelSubchunk<TVoxel> GetSubchunkForReading(int subchunkIndex)
		{
			this.RequireChunkConfigIsSet();
			this.RequireSubchunkIndexIsValid(subchunkIndex);

			return this.chunk.IsUniform ?
				new VoxelSubchunk<TVoxel>(this.chunk.UniformVoxel) :
				new VoxelSubchunk<TVoxel>(this.subchunks[subchunkIndex]);
		}

		public Pinned<TVoxel[]> GetSubchunkVoxelsForFullRewrite(int subchunkIndex) =>
			this.GetSubchunkVoxels(subchunkIndex, false);

		public Pinned<TVoxel[]> GetSubchunkVoxels(int subchunkIndex, bool assignVoxels = true)
		{
			this.RequireChunkConfigIsSet();
			this.RequireSubchunkIndexIsValid(subchunkIndex);

			if (this.chunk.IsUniform)
			{
				// make the chunk no longer uniform and assign voxel array pin to subchunk
				this.AssignSubchunksArrayAndFillWithUniformSubchunks(this.chunk.UniformVoxel);

				return this.SetAndGetVoxelArrayPinWithUniformVoxels(subchunkIndex, assignVoxels);
			}
			else
			{
				// chunk is not uniform
				if (this.subchunks[subchunkIndex].IsUniform)
				{
					// assign voxel array pin to subchunk
					return this.SetAndGetVoxelArrayPinWithUniformVoxels(subchunkIndex, assignVoxels);
				}
				else
				{
					if (this.IsSubchunkSharedSafe(subchunkIndex))
					{
						// current voxel array is shared with copied in chunk
						// so assign new voxel array pin to subchunk and copy over voxels
						return this.SetAndGetVoxelArrayPinWithUniqueVoxels(subchunkIndex, assignVoxels);
					}
					else
					{
						// subchunk already has its own unique voxel array pin for writing to so return that
						return this.subchunks[subchunkIndex].Voxels.AsPinned;
					}
				}
			}
		}

		[Conditional(CompilationSymbol.Debug)]
		private void RequireChunkConfigIsSet()
		{
			Debug.Assert(this.ChunkConfig.IsAssigned);
			Debug.Assert(this.subchunkArrayLength > 0);
			Debug.Assert(this.voxelArrayLength > 0);
			Debug.Assert(this.subchunkArrays != null);
			Debug.Assert(this.voxelArrays != null);
		}

		[Conditional(CompilationSymbol.Debug)]
		private void RequireChunkIsUniform()
		{
			// call this.RequireChunkConfigIsSet();
			Debug.Assert(this.chunk.IsUniform);
			Debug.Assert(this.subchunks == null);
		}

		[Conditional(CompilationSymbol.Debug)]
		private void RequireChunkIsNotUniform()
		{
			// call this.RequireChunkConfigIsSet();
			Debug.Assert(this.chunk.HasSubchunks);
			Debug.Assert(this.subchunks != null);
			Debug.Assert(this.subchunks.Length == this.subchunkArrayLength);
		}

		[Conditional(CompilationSymbol.Debug)]
		private void RequirePreviousChunkIsNotUniform()
		{
			// call this.RequireChunkConfigIsSet();
			Debug.Assert(this.previousChunkPin.Data.HasSubchunks);
			Debug.Assert(this.previousSubchunks != null);
			Debug.Assert(this.previousSubchunks.Length == this.subchunkArrayLength);
		}

		[Conditional(CompilationSymbol.Debug)]
		private void RequireSubchunkIndexIsValid(int subchunkIndex)
		{
			// call this.RequireChunkConfigIsSet();
			Debug.Assert(subchunkIndex >= 0);
			Debug.Assert(subchunkIndex < this.subchunkArrayLength);
		}

		[Conditional(CompilationSymbol.Debug)]
		private void RequireSubchunkIsNotUniformOrShared(int subchunkIndex)
		{
			this.RequireSubchunkIndexIsValid(subchunkIndex);
			Debug.Assert(!this.subchunks[subchunkIndex].IsUniform);
			Debug.Assert(!this.IsSubchunkSharedSafe(subchunkIndex));
		}

		private void ClearAndSetNewChunkConfig(ChunkConfig chunkConfig)
		{
			Debug.Assert(chunkConfig.IsAssigned);

			this.Clear();

			this.ChunkConfig = chunkConfig;
			this.subchunkArrayLength = chunkConfig.SubchunkIndexer3D.Length;
			this.voxelArrayLength = chunkConfig.CellIndexer3D.Length;
			this.subchunkArrays = this.subchunkArrayPools.Get(chunkConfig);
			this.voxelArrays = this.voxelArrayPools.Get(chunkConfig);
		}

		private bool IsSubchunkSharedSafe(int subchunkIndex)
		{
			if (this.previousSubchunks == null)
			{
				return false;
			}

			return this.IsSubchunkSharedFast(subchunkIndex);
		}

		private bool IsSubchunkSharedFast(int subchunkIndex)
		{
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsNotUniform();
			this.RequirePreviousChunkIsNotUniform();
			this.RequireSubchunkIndexIsValid(subchunkIndex);

			return this.previousSubchunks[subchunkIndex].Voxels.IsSameValueAs(this.subchunks[subchunkIndex].Voxels);
		}

		private void AssignSubchunksArray()
		{
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsUniform();

			this.chunk = new RawVoxelChunk<TVoxel>(this.ChunkConfig, this.subchunkArrays.Rent());
			this.subchunks = this.chunk.Subchunks.Value;

			Debug.Assert(this.subchunks.Length == this.subchunkArrayLength);
		}

		private void AssignSubchunksArrayAndFillWithUniformSubchunks(TVoxel uniformVoxel)
		{
			this.AssignSubchunksArray();

			for (int i = 0; i < this.subchunkArrayLength; i++)
			{
				this.subchunks[i] = new RawVoxelSubchunk<TVoxel>(uniformVoxel);
			}
		}

		private Pin<TVoxel[]> GetNewVoxelsArrayPin()
		{
			this.RequireChunkConfigIsSet();

			var voxels = this.voxelArrays.Rent();

			Debug.Assert(voxels.Value.Length == this.voxelArrayLength);

			return voxels;
		}

		private Pinned<TVoxel[]> SetAndGetVoxelArrayPinWithUniformVoxels(int subchunkIndex, bool assignVoxels)
		{
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsNotUniform();
			Debug.Assert(this.subchunks[subchunkIndex].IsUniform);
			Debug.Assert(!this.IsSubchunkSharedSafe(subchunkIndex));

			var voxelsPin = this.GetNewVoxelsArrayPin();
			if (assignVoxels)
			{
				voxelsPin.Value.SetAllTo(this.subchunks[subchunkIndex].UniformVoxel);
			}

			this.subchunks[subchunkIndex] = new RawVoxelSubchunk<TVoxel>(voxelsPin);
			return voxelsPin.AsPinned;
		}

		private Pinned<TVoxel[]> SetAndGetVoxelArrayPinWithUniqueVoxels(int subchunkIndex, bool assignVoxels)
		{
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsNotUniform();
			Debug.Assert(!this.subchunks[subchunkIndex].IsUniform);
			Debug.Assert(this.IsSubchunkSharedSafe(subchunkIndex));

			var sharedVoxelsPin = this.subchunks[subchunkIndex].Voxels;
			var voxelsPin = this.GetNewVoxelsArrayPin();
			if (assignVoxels)
			{
				Array.Copy(sharedVoxelsPin.Value, voxelsPin.Value, this.voxelArrayLength);
			}

			sharedVoxelsPin.Dispose();
			this.subchunks[subchunkIndex] = new RawVoxelSubchunk<TVoxel>(voxelsPin);
			return voxelsPin.AsPinned;
		}

		private void Optimize()
		{
			this.RequireChunkConfigIsSet();

			if (this.chunk.IsUniform)
			{
				// chunk is uniform so there's nothing that could be optimized
				return;
			}

			Debug.Assert(this.subchunks != null);
			Debug.Assert(this.subchunks.Length == this.subchunkArrayLength);

			bool allSubchunksAreUniform = true;

			// check each subchunk to see if they can be set to uniform
			if (this.previousSubchunks == null)
			{
				// this chunk was not created as a copy of another chunk or that copied chunk is uniform
				for (int subchunkIndex = 0; subchunkIndex < this.subchunkArrayLength; subchunkIndex++)
				{
					if (this.subchunks[subchunkIndex].IsUniform)
					{
						continue;
					}

					if (!this.TryUnifySubchunk(subchunkIndex))
					{
						allSubchunksAreUniform = false;
					}
				}
			}
			else
			{
				// the chunk was created by copying another chunk
				for (int subchunkIndex = 0; subchunkIndex < this.subchunkArrayLength; subchunkIndex++)
				{
					if (this.subchunks[subchunkIndex].IsUniform)
					{
						continue;
					}

					if (this.IsSubchunkSharedFast(subchunkIndex))
					{
						allSubchunksAreUniform = false;
						continue;
					}

					if (!this.TryUnifySubchunk(subchunkIndex))
					{
						allSubchunksAreUniform = false;
						this.CheckIfSubchunkIsModified(subchunkIndex);
					}
				}
			}

			// check to see if the entire chunk can be set to uniform
			if (allSubchunksAreUniform && this.TryUnifyEntireChunk())
			{
				// the entire chunk was unified so no need to go on checking
				// if any subchunks were actually changed from a copied chunk
				return;
			}

			this.CheckIfAnySubchunkIsModified();
		}

		private bool TryUnifySubchunk(int subchunkIndex)
		{
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsNotUniform();
			this.RequireSubchunkIsNotUniformOrShared(subchunkIndex);

			var voxels = this.subchunks[subchunkIndex].Voxels.Value;

			Debug.Assert(voxels.Length == this.voxelArrayLength);

			var firstVoxel = voxels[0];

			for (int i = 1; i < this.voxelArrayLength; i++)
			{
				if (!voxels[i].Equals(firstVoxel))
				{
					return false;
				}
			}

			this.subchunks[subchunkIndex].Voxels.Dispose();
			this.subchunks[subchunkIndex] = new RawVoxelSubchunk<TVoxel>(firstVoxel);
			return true;
		}

		private void CheckIfSubchunkIsModified(int subchunkIndex)
		{
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsNotUniform();
			this.RequirePreviousChunkIsNotUniform();
			this.RequireSubchunkIsNotUniformOrShared(subchunkIndex);

			if (this.previousSubchunks[subchunkIndex].IsUniform)
			{
				return;
			}

			var previousVoxels = this.previousSubchunks[subchunkIndex].Voxels.Value;
			var nextVoxels = this.subchunks[subchunkIndex].Voxels.Value;

			Debug.Assert(previousVoxels.Length == this.voxelArrayLength);
			Debug.Assert(nextVoxels.Length == this.voxelArrayLength);

			for (int i = 0; i < this.voxelArrayLength; i++)
			{
				if (!previousVoxels[i].Equals(nextVoxels[i]))
				{
					return;
				}
			}

			// subchunk was not actually modified so set it back to the previous subchunk
			this.subchunks[subchunkIndex].Voxels.Dispose();
			this.subchunks[subchunkIndex] = new RawVoxelSubchunk<TVoxel>(
				this.previousSubchunks[subchunkIndex].Voxels.CreatePin());
		}

		private bool TryUnifyEntireChunk()
		{
			// this method also requires that all subchunks are uniform
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsNotUniform();

			var uniformVoxel = this.subchunks[0].UniformVoxel;
			for (int subchunkIndex = 1; subchunkIndex < this.subchunkArrayLength; subchunkIndex++)
			{
				if (!this.subchunks[subchunkIndex].UniformVoxel.Equals(uniformVoxel))
				{
					return false;
				}
			}

			this.SetUniform(this.ChunkConfig, uniformVoxel);
			return true;
		}

		private void CheckIfAnySubchunkIsModified()
		{
			this.RequireChunkConfigIsSet();
			this.RequireChunkIsNotUniform();

			if (this.previousSubchunks == null)
			{
				// this chunk was not created as a copy of another chunk or that copied chunk is uniform
				// either way this chunk will need to keep its unique subchunks array
				return;
			}

			for (int subchunkIndex = 0; subchunkIndex < this.subchunkArrayLength; subchunkIndex++)
			{
				if (this.subchunks[subchunkIndex] != this.previousSubchunks[subchunkIndex])
				{
					// this subchunk was modified so this chunk still needs to keep its unique subchunks array
					return;
				}
			}

			// all subchunks match the copied chunk so this chunk does not need to keep a unique subchunks array
			this.chunk.Subchunks.Dispose();
			this.chunk = this.previousChunkPin.Data.rawChunk;
			this.subchunks = this.previousSubchunks;
		}
	}
}
