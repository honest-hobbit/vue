﻿namespace HQS.VUE.TwoTier;

internal class VoxelChunkIndexer : IChunkConfigDependent<ChunkConfig>, IDeinitializable
{
	private int chunkExponent;

	private CubeArrayIndexer subchunkIndexer;

	private CubeArrayIndexer voxelIndexer;

	private int subchunkMask;

	private int voxelMask;

	public bool IsInitialized => this.ChunkConfig.IsAssigned;

	/// <inheritdoc />
	public ChunkConfig ChunkConfig { get; private set; }

	public void Initialize(ChunkConfig config)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(config.IsAssigned);

		this.ChunkConfig = config;
		this.chunkExponent = config.CombinedExponent;
		this.subchunkIndexer = config.SubchunkIndexer3D;
		this.voxelIndexer = config.CellIndexer3D;
		this.subchunkMask = BitMask.CreateIntWithOnesInLowerBits(this.chunkExponent);
		this.voxelMask = BitMask.CreateIntWithOnesInLowerBits(this.voxelIndexer.Exponent);
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		this.ChunkConfig = default;
	}

	public Int3 ConvertVoxelLocationToChunkLocation(Int3 voxelLocation)
	{
		Debug.Assert(this.IsInitialized);

		return voxelLocation >> this.chunkExponent;
	}

	public int ConvertVoxelLocationToSubchunkIndex(Int3 voxelLocation)
	{
		Debug.Assert(this.IsInitialized);

		return this.subchunkIndexer[(voxelLocation & this.subchunkMask) >> this.voxelIndexer.Exponent];
	}

	public int ConvertVoxelLocationToVoxelIndex(Int3 voxelLocation)
	{
		Debug.Assert(this.IsInitialized);

		return this.voxelIndexer[voxelLocation & this.voxelMask];
	}
}
