﻿namespace HQS.VUE.TwoTier;

internal class VoxelReadView<TVoxel> : ICells3D<TVoxel>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly VoxelChunkIndexer indexer;

	private readonly CachedChunkDictionary<VoxelChunk<TVoxel>> chunks;

	private Int3 chunkLocation;

	private int subchunkIndex;

	private VoxelSubchunkArray<TVoxel> subchunks;

	private ReadOnlyArray<TVoxel> voxels;

	private TVoxel uniformVoxel;

	public VoxelReadView(VoxelChunkIndexer indexer, CachedChunkDictionary<VoxelChunk<TVoxel>> chunks)
	{
		Debug.Assert(indexer != null);
		Debug.Assert(chunks != null);

		this.indexer = indexer;
		this.chunks = chunks;
		this.Clear();
	}

	public bool IsInitialized => this.indexer.IsInitialized && this.chunks.IsInitialized;

	public ICells3D<TVoxel> AsReadOnly => this;

	// TODO need to implement this
	/// <inheritdoc />
	public IGrid3D Grid => throw new NotImplementedException();

	/// <inheritdoc />
	public Guid Key => this.chunks.Key;

	/// <inheritdoc />
	public TVoxel this[int x, int y, int z] => this[new Int3(x, y, z)];

	/// <inheritdoc />
	public TVoxel this[Int3 voxelLocation]
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			var chunkLocation = this.indexer.ConvertVoxelLocationToChunkLocation(voxelLocation);
			if (this.chunkLocation != chunkLocation)
			{
				this.chunkLocation = chunkLocation;

				var chunk = this.chunks[chunkLocation].Value.Data.Value.Chunk;

				Debug.Assert(chunk.HasData);
				Debug.Assert(chunk.ChunkConfig == this.indexer.ChunkConfig);

				if (chunk.IsUniform)
				{
					this.subchunks = default;
					this.uniformVoxel = chunk.UniformVoxel;
					return this.uniformVoxel;
				}

				this.subchunks = chunk.Subchunks;
				this.subchunkIndex = -1;
			}
			else if (this.subchunks.IsNull)
			{
				return this.uniformVoxel;
			}

			var subchunkIndex = this.indexer.ConvertVoxelLocationToSubchunkIndex(voxelLocation);
			if (this.subchunkIndex != subchunkIndex)
			{
				this.subchunkIndex = subchunkIndex;
				var subchunk = this.subchunks[subchunkIndex];

				if (subchunk.IsUniform)
				{
					this.voxels = default;
					this.uniformVoxel = subchunk.UniformVoxel;
					return this.uniformVoxel;
				}

				this.voxels = subchunk.Voxels;
			}
			else if (this.voxels.IsDefault)
			{
				return this.uniformVoxel;
			}

			return this.voxels[this.indexer.ConvertVoxelLocationToVoxelIndex(voxelLocation)];
		}
	}

	public void Clear()
	{
		// by setting to MinValue the chunk location is guaranteed not to match when the first chunk is accessed
		// because the min chunk index is MinValue / 4 (4 is minimum chunk width in voxels)
		this.chunkLocation = Int3.MinValue;

		// by setting to -1 the subchunk index is guaranteed not to match at first too
		this.subchunkIndex = -1;
		this.subchunks = default;
		this.voxels = default;
		this.uniformVoxel = default;
	}
}
