﻿namespace HQS.VUE.TwoTier;

internal class VoxelViews<TVoxel> : IKeyed<Guid>, IChunkConfigDependent<ChunkConfig>, IDeinitializable
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly VoxelChunkIndexer chunkIndexer = new VoxelChunkIndexer();

	private readonly ChunkCache<VoxelChunk<TVoxel>> chunkCache;

	private readonly CachedChunkDictionary<VoxelChunk<TVoxel>> chunks;

	private readonly ChunkBuilderDictionary<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin, VoxelChunk<TVoxel>.Builder> builders;

	private readonly ICommandSubmitter<
		BuildChunkGridChangesetCommand<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>.Args,
		Pinned<ChunkGridChangeset<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>>> buildChangesetSubmitter;

	private readonly VoxelReadView<TVoxel> previousView;

	private readonly VoxelWriteView<TVoxel> currentView;

	public VoxelViews(
		ChunkCache<VoxelChunk<TVoxel>> chunkCache,
		IPinPool<VoxelChunk<TVoxel>.Builder> chunkBuilderPool,
		ICommandSubmitter<
			BuildChunkGridChangesetCommand<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>.Args,
			Pinned<ChunkGridChangeset<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>>> buildChangesetSubmitter)
	{
		Debug.Assert(chunkCache != null);
		Debug.Assert(chunkBuilderPool != null);
		Debug.Assert(buildChangesetSubmitter != null);

		this.chunkCache = chunkCache;
		this.chunks = new CachedChunkDictionary<VoxelChunk<TVoxel>>(chunkCache);
		this.builders = new ChunkBuilderDictionary<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin, VoxelChunk<TVoxel>.Builder>(
			this.chunks, chunkBuilderPool);
		this.buildChangesetSubmitter = buildChangesetSubmitter;

		this.previousView = new VoxelReadView<TVoxel>(this.chunkIndexer, this.chunks);
		this.currentView = new VoxelWriteView<TVoxel>(this.chunkIndexer, this.builders, this);
	}

	public bool IsInitialized => this.chunkIndexer.IsInitialized && this.chunkCache.IsInitialized;

	/// <inheritdoc />
	public Guid Key => this.chunkCache.Key;

	/// <inheritdoc />
	public ChunkConfig ChunkConfig => this.chunkIndexer.ChunkConfig;

	public long Version { get; private set; }

	public ICells3D<TVoxel> PreviousView
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.previousView;
		}
	}

	public ICells3DWriter<TVoxel> CurrentView
	{
		get
		{
			Debug.Assert(this.IsInitialized);

			return this.currentView;
		}
	}

	public void DiscardChanges() => this.currentView.DiscardChanges();

	public Pin<IWaitable<Pinned<ChunkGridChangeset<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>>>> GetBuildChangesetCommand()
	{
		Debug.Assert(this.IsInitialized);

		return this.buildChangesetSubmitter.SubmitAndGetWaitableResult(
			new BuildChunkGridChangesetCommand<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>.Args()
			{
				ChunkGridKey = this.Key,
				ChunkGridVersion = this.Version,
				BuilderPairs = this.builders.BuilderPairs,
				CachedChunks = this.builders.CachedChunks,
			});
	}

	// store can be null
	// generator can't be null because voxel chunks must always have a default value
	public void Initialize(
		Guid chunkGridKey,
		ChunkConfig chunkGridConfig,
		long chunkGridVersion,
		IAssetStore store,
		IChunkGenerator<VoxelChunk<TVoxel>> generator)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(chunkGridKey != Guid.Empty);
		Debug.Assert(chunkGridConfig.IsAssigned);
		Debug.Assert(chunkGridVersion >= 0);
		Debug.Assert(generator != null);

		this.chunkIndexer.Initialize(chunkGridConfig);

		// TODO this class is responsible for initializing the deinitializing the chunk cache
		// I don't know if it should stay that way
		this.chunkCache.Initialize(chunkGridKey, store, generator);

		this.Version = chunkGridVersion;
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		this.Version = -1;
		this.currentView.Clear();
		this.previousView.Clear();
		this.builders.DiscardChanges();
		this.chunks.Clear();
		this.chunkIndexer.Deinitialize();

		// TODO this class is responsible for initializing the deinitializing the chunk cache
		// I don't know if it should stay that way
		this.chunkCache.Deinitialize();
	}
}
