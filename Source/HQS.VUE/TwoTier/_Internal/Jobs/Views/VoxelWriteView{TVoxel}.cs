﻿namespace HQS.VUE.TwoTier;

internal class VoxelWriteView<TVoxel> : ICells3DWriter<TVoxel>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly VoxelChunkIndexer indexer;

	private readonly ChunkBuilderDictionary<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin, VoxelChunk<TVoxel>.Builder> chunks;

	private readonly VoxelViews<TVoxel> views;

	private Int3 writeChunkLocation;

	private Int3 readChunkLocation;

	private VoxelChunk<TVoxel>.Builder writeChunkBuilder;

	private VoxelChunk<TVoxel>.Builder readChunkBuilder;

	private int writeSubchunkIndex;

	private int readSubchunkIndex;

	private TVoxel[] writeVoxels;

	private ReadOnlyArray<TVoxel> readVoxels;

	private TVoxel readUniformVoxel;

	public VoxelWriteView(
		VoxelChunkIndexer indexer,
		ChunkBuilderDictionary<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin, VoxelChunk<TVoxel>.Builder> chunks,
		VoxelViews<TVoxel> views)
	{
		Debug.Assert(indexer != null);
		Debug.Assert(chunks != null);
		Debug.Assert(views != null);

		this.indexer = indexer;
		this.chunks = chunks;
		this.views = views;
		this.Clear();
	}

	public bool IsInitialized => this.indexer.IsInitialized && this.chunks.IsInitialized;

	// TODO need to implement this
	public ICells3D<TVoxel> AsReadOnly => throw new NotImplementedException();

	// TODO need to implement this
	/// <inheritdoc />
	public IGrid3DWriter Grid => throw new NotImplementedException();

	// TODO need to implement this
	/// <inheritdoc />
	IGrid3D ICells3D<TVoxel>.Grid => throw new NotImplementedException();

	/// <inheritdoc />
	public ICells3D<TVoxel> Previous => this.views.PreviousView;

	/// <inheritdoc />
	public Guid Key => this.chunks.Key;

	/// <inheritdoc />
	public bool HasChanges => throw new NotImplementedException();

	/// <inheritdoc />
	public TVoxel this[Int3 index]
	{
		get => this.ReadVoxel(index);
		set => this.Replace(index, value);
	}

	/// <inheritdoc />
	public TVoxel this[int x, int y, int z]
	{
		get => this.ReadVoxel(new Int3(x, y, z));
		set => this.Replace(new Int3(x, y, z), value);
	}

	/// <inheritdoc />
	TVoxel ICells3D<TVoxel>.this[Int3 index] => this.ReadVoxel(index);

	/// <inheritdoc />
	TVoxel ICells3D<TVoxel>.this[int x, int y, int z] => this.ReadVoxel(new Int3(x, y, z));

	/// <inheritdoc />
	public TVoxel Replace(int x, int y, int z, TVoxel voxel) => this.Replace(new Int3(x, y, z), voxel);

	/// <inheritdoc />
	public TVoxel Replace(Int3 voxelLocation, TVoxel voxel)
	{
		Debug.Assert(this.IsInitialized);

		var chunkLocation = this.indexer.ConvertVoxelLocationToChunkLocation(voxelLocation);
		if (this.writeChunkLocation != chunkLocation)
		{
			this.writeChunkLocation = chunkLocation;
			this.writeChunkBuilder = this.chunks[chunkLocation].Value;
			this.writeSubchunkIndex = -1;

			Debug.Assert(this.writeChunkBuilder != null);
			Debug.Assert(this.writeChunkBuilder.ChunkConfig == this.indexer.ChunkConfig);
		}

		var subchunkIndex = this.indexer.ConvertVoxelLocationToSubchunkIndex(voxelLocation);
		if (this.writeSubchunkIndex != subchunkIndex)
		{
			this.writeSubchunkIndex = subchunkIndex;
			this.writeVoxels = this.writeChunkBuilder.GetSubchunkVoxels(subchunkIndex, assignVoxels: true).Value;

			if (this.writeSubchunkIndex == this.readSubchunkIndex && this.writeChunkBuilder == this.readChunkBuilder)
			{
				this.readVoxels = new ReadOnlyArray<TVoxel>(this.writeVoxels);
			}
		}

		int voxelIndex = this.indexer.ConvertVoxelLocationToVoxelIndex(voxelLocation);
		var replacedVoxel = this.writeVoxels[voxelIndex];
		this.writeVoxels[voxelIndex] = voxel;
		return replacedVoxel;
	}

	public void MergeWith(ICells3D<TVoxel> source, Func<TVoxel, TVoxel, TVoxel> reducer, Int3 offset)
	{
		throw new NotImplementedException();
	}

	public void MergeWith(IEnumerable<ICells3D<TVoxel>> sources, Func<TVoxel, TVoxel, TVoxel> reducer, Int3 offset)
	{
		throw new NotImplementedException();
	}

	/// <inheritdoc />
	public void DiscardChanges()
	{
		Debug.Assert(this.IsInitialized);

		this.chunks.DiscardChanges();
		this.Clear();
	}

	public void Clear()
	{
		// by setting to MinValue the chunk location is guaranteed not to match when the first chunk is accessed
		// because the min chunk index is MinValue / 4 (4 is minimum chunk width in voxels)
		this.writeChunkLocation = Int3.MinValue;
		this.readChunkLocation = Int3.MinValue;

		// by setting to -1 the subchunk index is guaranteed not to match at first too
		this.writeSubchunkIndex = -1;
		this.readSubchunkIndex = -1;

		this.writeChunkBuilder = null;
		this.readChunkBuilder = null;

		this.writeVoxels = null;
		this.readVoxels = default;
		this.readUniformVoxel = default;
	}

	private TVoxel ReadVoxel(Int3 voxelLocation)
	{
		Debug.Assert(this.IsInitialized);

		var chunkLocation = this.indexer.ConvertVoxelLocationToChunkLocation(voxelLocation);
		if (this.readChunkLocation != chunkLocation)
		{
			this.readChunkLocation = chunkLocation;
			this.readChunkBuilder = this.chunks[chunkLocation].Value;
			this.readSubchunkIndex = -1;

			Debug.Assert(this.readChunkBuilder != null);
			Debug.Assert(this.readChunkBuilder.ChunkConfig == this.indexer.ChunkConfig);
		}

		var subchunkIndex = this.indexer.ConvertVoxelLocationToSubchunkIndex(voxelLocation);
		if (this.readSubchunkIndex != subchunkIndex)
		{
			this.readSubchunkIndex = subchunkIndex;
			var subchunk = this.readChunkBuilder.GetSubchunkForReading(subchunkIndex);

			if (subchunk.IsUniform)
			{
				this.readVoxels = default;
				this.readUniformVoxel = subchunk.UniformVoxel;
				return this.readUniformVoxel;
			}

			this.readVoxels = subchunk.Voxels;
		}
		else if (this.readVoxels.IsDefault)
		{
			return this.readUniformVoxel;
		}

		return this.readVoxels[this.indexer.ConvertVoxelLocationToVoxelIndex(voxelLocation)];
	}
}
