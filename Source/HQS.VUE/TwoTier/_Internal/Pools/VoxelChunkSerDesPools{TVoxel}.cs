﻿namespace HQS.VUE.TwoTier;

internal class VoxelChunkSerDesPools<TVoxel> : AbstractDisposable
	where TVoxel : struct, IEquatable<TVoxel>
{
	public VoxelChunkSerDesPools(ISerDes<TVoxel> voxelSerDes)
	{
		Debug.Assert(voxelSerDes != null);

		this.Encoders = new PinPool<VoxelChunkEncoder<TVoxel>>(
			() => new VoxelChunkEncoder<TVoxel>(voxelSerDes),
			new PoolOptions<VoxelChunkEncoder<TVoxel>>()
			{
				Deinitialize = encoder => encoder.Clear(),
			});

		this.Decoders = new PinPool<VoxelChunkDecoder<TVoxel>>(
			() => new VoxelChunkDecoder<TVoxel>(voxelSerDes),
			new PoolOptions<VoxelChunkDecoder<TVoxel>>()
			{
				Deinitialize = decoder => decoder.Clear(),
			});
	}

	public IPinPool<VoxelChunkEncoder<TVoxel>> Encoders { get; }

	public IPinPool<VoxelChunkDecoder<TVoxel>> Decoders { get; }

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.Encoders.ReleaseAll();
		this.Decoders.ReleaseAll();
	}
}
