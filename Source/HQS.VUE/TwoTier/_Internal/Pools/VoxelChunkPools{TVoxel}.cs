﻿namespace HQS.VUE.TwoTier;

internal class VoxelChunkPools<TVoxel> : AbstractDisposable
	where TVoxel : struct, IEquatable<TVoxel>
{
	public VoxelChunkPools()
	{
		// TODO these expiries could have initial sizes set, maybe multiple of the size of the cache?
		this.VoxelArrays = new StaticArrayResolver<ChunkConfig, ExpiryPinPool<TVoxel[]>>(
			config => config.CellExponent - ChunkConfig.MinExponent,
			ChunkConfig.EnumerateCellExponents().Select(x =>
			{
				int length = x.CellIndexer3D.Length;
				return KeyValuePair.Create(x, new ExpiryPinPool<TVoxel[]>(new PinPool<TVoxel[]>(() => new TVoxel[length])));
			}));

		var subchunkPoolOptions = new PoolOptions<RawVoxelSubchunk<TVoxel>[]>()
		{
			Deinitialize = subchunks =>
			{
				int max = subchunks.Length;
				for (int i = 0; i < max; i++)
				{
					subchunks[i].Voxels.Dispose();
				}
			},
		};

		this.SubchunkArrays = new StaticArrayResolver<ChunkConfig, ExpiryPinPool<RawVoxelSubchunk<TVoxel>[]>>(
			config => config.SubchunkExponent - ChunkConfig.MinExponent,
			ChunkConfig.EnumerateSubchunkExponents().Select(x =>
			{
				int length = x.SubchunkIndexer3D.Length;
				return KeyValuePair.Create(x, new ExpiryPinPool<RawVoxelSubchunk<TVoxel>[]>(
					new PinPool<RawVoxelSubchunk<TVoxel>[]>(
						() => new RawVoxelSubchunk<TVoxel>[length],
						subchunkPoolOptions)));
			}));

		this.ChunkBuilders = Pool.ThreadSafe.Pins.Create(
			() => new VoxelChunk<TVoxel>.Builder(this.SubchunkArrays, this.VoxelArrays));

		this.VersionedVoxelChunks = Pool.ThreadSafe.Pins.Create<VersionedChunk<VoxelChunk<TVoxel>>>(
			() => new VersionedChunk<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>());

		this.CachedChunks = new ExpiryPinPool<CachedChunk<VoxelChunk<TVoxel>>>(
			Pool.ThreadSafe.Pins.Create(() => new CachedChunk<VoxelChunk<TVoxel>>(this.VersionedVoxelChunks)));
	}

	public IKeyResolver<ChunkConfig, ExpiryPinPool<TVoxel[]>> VoxelArrays { get; }

	public IKeyResolver<ChunkConfig, ExpiryPinPool<RawVoxelSubchunk<TVoxel>[]>> SubchunkArrays { get; }

	public IPinPool<VoxelChunk<TVoxel>.Builder> ChunkBuilders { get; }

	public IPinPool<VersionedChunk<VoxelChunk<TVoxel>>> VersionedVoxelChunks { get; }

	public ExpiryPinPool<CachedChunk<VoxelChunk<TVoxel>>> CachedChunks { get; }

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.CachedChunks.ReleaseAll();
		this.VersionedVoxelChunks.ReleaseAll();
		this.ChunkBuilders.ReleaseAll();

		foreach (var pool in this.SubchunkArrays)
		{
			pool.ReleaseAll();
		}

		foreach (var pool in this.VoxelArrays)
		{
			pool.ReleaseAll();
		}
	}
}
