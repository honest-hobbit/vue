﻿namespace HQS.VUE.TwoTier;

internal class VoxelChunkConfigSerDes : ISerDes<ChunkConfig>, ISerDes<int>
{
	private const int Uninitialized = -1;

	private int count = Uninitialized;

	public VoxelChunkConfigSerDes()
	{
	}

	/// <inheritdoc />
	public void Serialize(ChunkConfig config, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(config, writeByte);

		writeByte(config.Value);
	}

	/// <inheritdoc />
	public ChunkConfig Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		var chunkConfig = new ChunkConfig(readByte());
		this.count = chunkConfig.CombinedIndexer3D.Length;
		return chunkConfig;
	}

	/// <inheritdoc />
	void ISerializer<int>.Serialize(int value, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(value, writeByte);
	}

	/// <inheritdoc />
	int IDeserializer<int>.Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);
		Debug.Assert(this.count != Uninitialized);

		int count = this.count;
		this.count = Uninitialized;
		return count;
	}
}
