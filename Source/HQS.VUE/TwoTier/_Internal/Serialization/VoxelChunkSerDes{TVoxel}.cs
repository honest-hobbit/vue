﻿namespace HQS.VUE.TwoTier;

internal class VoxelChunkSerDes<TVoxel> : ISerDes<VoxelChunk<TVoxel>.Pin>, ISerializer<VoxelChunk<TVoxel>>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly IPinPool<VoxelChunkEncoder<TVoxel>> encoderPool;

	private readonly IPinPool<VoxelChunkDecoder<TVoxel>> decoderPool;

	private readonly IPinPool<VoxelChunk<TVoxel>.Builder> builderPool;

	public VoxelChunkSerDes(
		IPinPool<VoxelChunkEncoder<TVoxel>> encoderPool,
		IPinPool<VoxelChunkDecoder<TVoxel>> decoderPool,
		IPinPool<VoxelChunk<TVoxel>.Builder> builderPool)
	{
		Debug.Assert(encoderPool != null);
		Debug.Assert(decoderPool != null);
		Debug.Assert(builderPool != null);

		this.encoderPool = encoderPool;
		this.decoderPool = decoderPool;
		this.builderPool = builderPool;
	}

	/// <inheritdoc />
	public void Serialize(VoxelChunk<TVoxel>.Pin pin, Action<byte> writeByte) => this.Serialize(pin.Data, writeByte);

	/// <inheritdoc />
	public void Serialize(VoxelChunk<TVoxel> chunk, Action<byte> writeByte)
	{
		ISerializerContracts.Serialize(chunk, writeByte);
		Debug.Assert(chunk.HasData);

		using var pin = this.encoderPool.Rent(out var encoder);
		encoder.StartEncoding(writeByte, chunk.ChunkConfig);

		if (chunk.IsUniform)
		{
			// encode a single run length for the entire chunk of the uniform voxel type
			encoder.EncodeNext(chunk.UniformVoxel, chunk.ChunkConfig.CombinedIndexer3D.Length);
		}
		else
		{
			// encode each subchunk separately
			int subchunksPerChunk = chunk.ChunkConfig.SubchunkIndexer3D.Length;
			int voxelsPerSubchunk = chunk.ChunkConfig.CellIndexer3D.Length;
			var subchunks = chunk.Subchunks;

			for (int subchunkIndex = 0; subchunkIndex < subchunksPerChunk; subchunkIndex++)
			{
				var subchunk = subchunks[subchunkIndex];
				if (subchunk.IsUniform)
				{
					// encode a single run length for the entire subchunk of the uniform voxel type
					encoder.EncodeNext(subchunk.UniformVoxel, voxelsPerSubchunk);
				}
				else
				{
					// encode each voxel individually
					var voxels = subchunk.Voxels;
					for (int voxelIndex = 0; voxelIndex < voxelsPerSubchunk; voxelIndex++)
					{
						encoder.EncodeNext(voxels[voxelIndex]);
					}
				}
			}
		}

		Debug.Assert(encoder.RemainingCount == 0);
	}

	/// <inheritdoc />
	public VoxelChunk<TVoxel>.Pin Deserialize(Func<byte> readByte)
	{
		IDeserializerContracts.Deserialize(readByte);

		using var pin = this.decoderPool.Rent(out var decoder);
		var chunkConfig = decoder.StartDecoding(readByte);

		Debug.Assert(chunkConfig.IsAssigned);

		// there should always be at least 1 run of voxels
		decoder.DecodeNext(out int runLength, out var voxel);

		Debug.Assert(runLength > 0);

		if (decoder.RemainingCount == 0)
		{
			Debug.Assert(runLength == chunkConfig.CombinedIndexer3D.Length);

			// this single run of voxels covers the entire chunk so make it uniform
			return new VoxelChunk<TVoxel>.Pin(chunkConfig, voxel);
		}
		else
		{
			return this.DecodeNotUniformChunk(decoder, chunkConfig, runLength, voxel);
		}
	}

	private VoxelChunk<TVoxel>.Pin DecodeNotUniformChunk(
		VoxelChunkDecoder<TVoxel> decoder, ChunkConfig chunkConfig, int runLength, TVoxel voxel)
	{
		Debug.Assert(decoder != null);
		Debug.Assert(chunkConfig.IsAssigned);
		int voxelsPerChunk = chunkConfig.CombinedIndexer3D.Length;
		Debug.Assert(runLength > 0);
		Debug.Assert(runLength < voxelsPerChunk);
		Debug.Assert(decoder.RemainingCount == voxelsPerChunk - runLength);

		// the chunk is not completely uniform so we need a builder
		using var builderPin = this.builderPool.Rent(out var builder);
		builder.SetUniform(chunkConfig, default);

		// this state is shared by the nested do while loops below
		int voxelsPerSubchunk = chunkConfig.CellIndexer3D.Length;
		TVoxel[] subchunkVoxels = null;
		int subchunkIndex = 0;
		int voxelIndex = 0;

		// do while the decoder still has more run lengths to read
		do
		{
			// do while an individual run length of voxels is still greater than 0
			do
			{
				// handle assigning the run length of voxels to 1 or more subchunks
				if (voxelIndex == 0 && runLength >= voxelsPerSubchunk)
				{
					// we're starting a new subchunk and the run of voxels is long enough to fill it entirely
					// so make this next subchunk uniform
					builder.SetSubchunkUniform(subchunkIndex, voxel);
					subchunkIndex++;
					runLength -= voxelsPerSubchunk;

					Debug.Assert(subchunkVoxels == null);
				}
				else
				{
					// we're either in the middle of filling a subchunk or the run of voxels doesn't
					// fill the entire subchunk so we fill part of a subchunk with these voxels
					subchunkVoxels ??= builder.GetSubchunkVoxelsForFullRewrite(subchunkIndex).Value;

					int max = (voxelIndex + runLength).ClampUpper(voxelsPerSubchunk);
					runLength -= max - voxelIndex;

					for (; voxelIndex < max; voxelIndex++)
					{
						subchunkVoxels[voxelIndex] = voxel;
					}

					if (voxelIndex == voxelsPerSubchunk)
					{
						// the run of voxels reached the end of the subchunk so start a new one
						voxelIndex = 0;
						subchunkIndex++;
						subchunkVoxels = null;
					}
				}
			}
			while (runLength > 0);
		}
		while (decoder.TryDecodeNext(out runLength, out voxel));

		Debug.Assert(runLength == 0);
		Debug.Assert(decoder.RemainingCount == 0);

		return builder.Build(optimize: false);
	}
}
