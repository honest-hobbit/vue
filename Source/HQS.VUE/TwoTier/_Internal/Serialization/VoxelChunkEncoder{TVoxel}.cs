﻿namespace HQS.VUE.TwoTier;

internal class VoxelChunkEncoder<TVoxel>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly VoxelChunkConfigSerDes configSerDes = new VoxelChunkConfigSerDes();

	private readonly RunLengthEncoder<TVoxel> encoder;

	public VoxelChunkEncoder(ISerDes<TVoxel> voxelSerDes)
	{
		Debug.Assert(voxelSerDes != null);

		// TODO Capacity should be configurable???
		this.encoder = new RunLengthEncoder<TVoxel>(
			voxelSerDes,
			new RunLengthEncoderOptions<TVoxel>()
			{
				Comparer = EqualityComparer.ForStruct<TVoxel>(),
				Capacity = 512,
				CountSerDes = new CountSerDes(this.configSerDes, 0, int.MaxValue),
				RunLengthSerDes = SerializeCount.AsSignedVarint,
			});
	}

	public int MaxRepeatRunLength => this.encoder.MaxRepeatRunLength;

	public int MaxUniqueRunLength => this.encoder.MaxUniqueRunLength;

	public int Count => this.encoder.Count;

	public int RemainingCount => this.encoder.RemainingCount;

	public bool IsStarted => this.encoder.IsStarted;

	public void Clear() => this.encoder.Clear();

	public void StartEncoding(Action<byte> writeByte, ChunkConfig chunkConfig)
	{
		Debug.Assert(!this.IsStarted);
		Debug.Assert(writeByte != null);
		Debug.Assert(chunkConfig.IsAssigned);

		this.configSerDes.Serialize(chunkConfig, writeByte);
		this.encoder.StartEncoding(writeByte, chunkConfig.CombinedIndexer3D.Length);
	}

	public void EncodeNext(TVoxel value, int count = 1) => this.encoder.EncodeNext(value, count);
}
