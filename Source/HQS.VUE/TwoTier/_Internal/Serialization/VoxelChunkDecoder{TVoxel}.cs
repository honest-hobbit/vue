﻿namespace HQS.VUE.TwoTier;

internal class VoxelChunkDecoder<TVoxel>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly VoxelChunkConfigSerDes configSerDes = new VoxelChunkConfigSerDes();

	private readonly RunLengthDecoder<TVoxel> decoder;

	public VoxelChunkDecoder(ISerDes<TVoxel> voxelSerDes)
	{
		Debug.Assert(voxelSerDes != null);

		this.decoder = new RunLengthDecoder<TVoxel>(
			voxelSerDes,
			new RunLengthOptions()
			{
				CountSerDes = new CountSerDes(this.configSerDes, 0, int.MaxValue),
				RunLengthSerDes = SerializeCount.AsSignedVarint,
			});
	}

	public int MaxRepeatRunLength => this.decoder.MaxRepeatRunLength;

	public int MaxUniqueRunLength => this.decoder.MaxUniqueRunLength;

	public int Count => this.decoder.Count;

	public int RemainingCount => this.decoder.RemainingCount;

	public bool IsStarted => this.decoder.IsStarted;

	public void Clear() => this.decoder.Clear();

	public ChunkConfig StartDecoding(Func<byte> readByte)
	{
		Debug.Assert(!this.IsStarted);
		Debug.Assert(readByte != null);

		var chunkConfig = this.configSerDes.Deserialize(readByte);
		this.decoder.StartDecoding(readByte);
		return chunkConfig;
	}

	public bool TryDecodeNext(out int runLength, out TVoxel value) => this.decoder.TryDecodeNext(out runLength, out value);

	public void DecodeNext(out int runLength, out TVoxel value) => this.decoder.DecodeNext(out runLength, out value);
}
