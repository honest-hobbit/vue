﻿namespace HQS.VUE.TwoTier;

internal readonly struct RawVoxelSubchunk<TVoxel> : IEquatable<RawVoxelSubchunk<TVoxel>>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly TVoxel uniformVoxel;

	public RawVoxelSubchunk(TVoxel uniformVoxel)
	{
		this.uniformVoxel = uniformVoxel;
		this.Voxels = default;
	}

	public RawVoxelSubchunk(Pin<TVoxel[]> voxels)
	{
		Debug.Assert(voxels.IsPinned);

		this.uniformVoxel = default;
		this.Voxels = voxels;
	}

	public bool HasData => this.IsUniform || this.HasVoxels;

	public bool HasVoxels => this.Voxels.IsPinned;

	public bool IsUniform => this.Voxels.IsUnassigned;

	public TVoxel UniformVoxel
	{
		get
		{
			Debug.Assert(this.IsUniform);

			return this.uniformVoxel;
		}
	}

	public Pin<TVoxel[]> Voxels { get; }

	public static bool operator ==(RawVoxelSubchunk<TVoxel> lhs, RawVoxelSubchunk<TVoxel> rhs) => lhs.Equals(rhs);

	public static bool operator !=(RawVoxelSubchunk<TVoxel> lhs, RawVoxelSubchunk<TVoxel> rhs) => !lhs.Equals(rhs);

	public RawVoxelSubchunk<TVoxel> CreatePin() => this.IsUniform ?
		new RawVoxelSubchunk<TVoxel>(this.uniformVoxel) :
		new RawVoxelSubchunk<TVoxel>(this.Voxels.CreatePin());

	/// <inheritdoc />
	public bool Equals(RawVoxelSubchunk<TVoxel> other)
	{
		if (this.IsUniform)
		{
			if (other.IsUniform)
			{
				return this.uniformVoxel.Equals(other.uniformVoxel);
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (other.IsUniform)
			{
				return false;
			}
			else
			{
				return this.Voxels.IsSameValueAs(other.Voxels);
			}
		}
	}

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.IsUniform ? this.uniformVoxel.GetHashCode() : this.Voxels.GetHashCode();
}
