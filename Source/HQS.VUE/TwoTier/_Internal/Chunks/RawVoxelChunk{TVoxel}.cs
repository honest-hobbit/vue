﻿namespace HQS.VUE.TwoTier;

internal readonly struct RawVoxelChunk<TVoxel> : IEquatable<RawVoxelChunk<TVoxel>>, IChunkConfigDependent<ChunkConfig>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly TVoxel uniformVoxel;

	public RawVoxelChunk(ChunkConfig chunkConfig, TVoxel uniformVoxel)
	{
		Debug.Assert(chunkConfig.IsAssigned);

		this.ChunkConfig = chunkConfig;
		this.uniformVoxel = uniformVoxel;
		this.Subchunks = default;
	}

	public RawVoxelChunk(ChunkConfig chunkConfig, Pin<RawVoxelSubchunk<TVoxel>[]> subchunks)
	{
		Debug.Assert(chunkConfig.IsAssigned);
		Debug.Assert(subchunks.IsPinned);

		this.ChunkConfig = chunkConfig;
		this.uniformVoxel = default;
		this.Subchunks = subchunks;
	}

	/// <inheritdoc />
	public ChunkConfig ChunkConfig { get; }

	public bool HasData => this.IsUniform || this.HasSubchunks;

	public bool HasSubchunks => this.Subchunks.IsPinned;

	public bool IsUniform => this.ChunkConfig.IsAssigned && this.Subchunks.IsUnassigned;

	public TVoxel UniformVoxel
	{
		get
		{
			Debug.Assert(this.IsUniform);

			return this.uniformVoxel;
		}
	}

	public Pin<RawVoxelSubchunk<TVoxel>[]> Subchunks { get; }

	public static bool operator ==(RawVoxelChunk<TVoxel> lhs, RawVoxelChunk<TVoxel> rhs) => lhs.Equals(rhs);

	public static bool operator !=(RawVoxelChunk<TVoxel> lhs, RawVoxelChunk<TVoxel> rhs) => !lhs.Equals(rhs);

	public RawVoxelChunk<TVoxel> CreatePin()
	{
		Debug.Assert(this.HasData);

		return this.IsUniform ?
			new RawVoxelChunk<TVoxel>(this.ChunkConfig, this.uniformVoxel) :
			new RawVoxelChunk<TVoxel>(this.ChunkConfig, this.Subchunks.CreatePin());
	}

	public VoxelSubchunk<TVoxel> GetSubchunk(int index)
	{
		Debug.Assert(this.HasData);
		Debug.Assert(index >= 0);
		Debug.Assert(index < this.ChunkConfig.SubchunkIndexer3D.Length);

		return new VoxelSubchunk<TVoxel>(
			this.IsUniform ? new RawVoxelSubchunk<TVoxel>(this.uniformVoxel) : this.Subchunks.Value[index]);
	}

	/// <inheritdoc />
	public bool Equals(RawVoxelChunk<TVoxel> other)
	{
		if (this.ChunkConfig != other.ChunkConfig)
		{
			return false;
		}

		if (this.ChunkConfig.IsUnassigned)
		{
			return true;
		}

		if (this.Subchunks.IsUnassigned)
		{
			if (other.Subchunks.IsUnassigned)
			{
				return this.uniformVoxel.Equals(other.uniformVoxel);
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (other.Subchunks.IsUnassigned)
			{
				return false;
			}
			else
			{
				return this.Subchunks.IsSameValueAs(other.Subchunks);
			}
		}
	}

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(
		this.ChunkConfig, this.Subchunks.IsUnassigned ? this.uniformVoxel.GetHashCode() : this.Subchunks.GetHashCode());
}
