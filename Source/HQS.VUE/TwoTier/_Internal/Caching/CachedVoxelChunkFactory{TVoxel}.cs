﻿namespace HQS.VUE.TwoTier;

internal class CachedVoxelChunkFactory<TVoxel> : ICachedChunkFactory<VoxelChunk<TVoxel>>
	where TVoxel : struct, IEquatable<TVoxel>
{
	private readonly ExpiryPinPool<CachedChunk<VoxelChunk<TVoxel>>> cachedChunks;

	private readonly IKeyResolver<ChunkConfig, ExpiryPinPool<RawVoxelSubchunk<TVoxel>[]>> subchunkArrays;

	private readonly IKeyResolver<ChunkConfig, ExpiryPinPool<TVoxel[]>> voxelArrays;

	private readonly ICommandSubmitter<LoadChunkCommand<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>.Args> loadProcessor;

	public CachedVoxelChunkFactory(
		ExpiryPinPool<CachedChunk<VoxelChunk<TVoxel>>> cachedChunks,
		IKeyResolver<ChunkConfig, ExpiryPinPool<RawVoxelSubchunk<TVoxel>[]>> subchunkArrays,
		IKeyResolver<ChunkConfig, ExpiryPinPool<TVoxel[]>> voxelArrays,
		ICommandSubmitter<LoadChunkCommand<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>.Args> loadProcessor)
	{
		Debug.Assert(cachedChunks != null);
		Debug.Assert(subchunkArrays != null);
		Debug.Assert(voxelArrays != null);
		Debug.Assert(loadProcessor != null);

		this.cachedChunks = cachedChunks;
		this.subchunkArrays = subchunkArrays;
		this.voxelArrays = voxelArrays;
		this.loadProcessor = loadProcessor;
	}

	/// <inheritdoc />
	public CachedChunk<VoxelChunk<TVoxel>> CreateValue(Int3 chunkLocation)
	{
		ICachedChunkFactoryContracts.CreateValue(chunkLocation);

		var pin = this.cachedChunks.Rent(out var chunk);
		chunk.Initialize(pin, chunkLocation);
		return chunk;
	}

	/// <inheritdoc />
	public void EnqueueToken(
		Int3 chunkLocation, CachedChunk<VoxelChunk<TVoxel>> cachedChunk, CacheExpiration token)
	{
		ICachedChunkFactoryContracts.EnqueueToken(chunkLocation, cachedChunk);

		cachedChunk.WaitForResults(out var pin, out var exception);

		if (exception != null)
		{
			Debug.Assert(pin.IsUnassigned);

			token.TryExpire();
			return;
		}

		this.cachedChunks.AddExpiration(token);

		var data = pin.Value;
		if (!data.HasChunk || data.Chunk.IsUniform)
		{
			return;
		}

		var chunkConfig = data.Chunk.ChunkConfig;
		this.subchunkArrays.Get(chunkConfig).AddExpiration(token);
		this.voxelArrays.Get(chunkConfig).AddExpiration(token);
	}

	/// <inheritdoc />
	public void ValueExpired(Int3 chunkLocation, CachedChunk<VoxelChunk<TVoxel>> cachedChunk)
	{
		ICachedChunkFactoryContracts.ValueExpired(chunkLocation, cachedChunk);

		cachedChunk.DisposeSelfPoolPin();
	}

	/// <inheritdoc />
	public void LoadChunk(
		Guid chunkGridKey,
		Pinned<CachedChunk<VoxelChunk<TVoxel>>> chunkPin,
		IAssetStore store,
		IChunkGenerator<VoxelChunk<TVoxel>> generator)
	{
		ICachedChunkFactoryContracts.LoadChunk(chunkGridKey, chunkPin, store, generator);

		this.loadProcessor.Submit(new LoadChunkCommand<VoxelChunk<TVoxel>, VoxelChunk<TVoxel>.Pin>.Args()
		{
			ChunkPin = chunkPin,
			Store = store,
			Generator = generator,
		});
	}
}
