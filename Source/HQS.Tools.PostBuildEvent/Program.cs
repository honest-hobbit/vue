﻿using EnsureThat;
using HQS.Tools.Utility;
using HQS.Utility.IO;

//System.Diagnostics.Debugger.Launch();

var additionalExclusionsFile = @"Misc\PostBuildEventExclusions.txt";

EnsureArg.Is(args.Length, 2, $"{nameof(args)}.Length");
EnsureArg.IsNotNullOrWhiteSpace(args[0], $"{nameof(args)}[0]");
EnsureArg.IsNotNullOrWhiteSpace(args[1], $"{nameof(args)}[1]");

var projectBuildDirectoryPath = Path.GetFullPath(args[0]);
var repositoryDirectoryPath = Path.GetFullPath(args[1]);

Console.WriteLine("HQS - Post Build Event - Starting for Project: " + projectBuildDirectoryPath);

var unityProvidedAssemblyFilePaths =
	UnityProject.GetAssemblyReferences(repositoryDirectoryPath)
	.Where(path => !path.Contains(UnityProject.BuiltPluginsDirectory))
	.ToArray();

var additionalExclusions =
	File.ReadAllLines(Path.Combine(repositoryDirectoryPath, additionalExclusionsFile))
	.Where(x => !string.IsNullOrWhiteSpace(x))
	.ToArray();

var unityProvidedAssemblyNames = unityProvidedAssemblyFilePaths
	.Concat(additionalExclusions)
	.Select(path => Path.GetFileNameWithoutExtension(path))
	.ToHashSet();

var destinationDirectoryPath = Path.Combine(repositoryDirectoryPath, UnityProject.BuiltPluginsDirectory);
int filesCopied = 0;
int filesSkipped = 0;
int filesUnchanged = 0;
int filesFailed = 0;

CopyFiles("*.dll");
CopyFiles("*.pdb");
CopyFiles("*.xml");

Console.WriteLine($"HQS - Post Build Event - {filesCopied} copied. {filesUnchanged} unchanged. {filesSkipped} skipped. {filesFailed} failed.");

void CopyFiles(string searchPattern) => DirectoryUtility.CopyDirectory(
	projectBuildDirectoryPath, destinationDirectoryPath, searchPattern, SearchOption.AllDirectories, CopyFile);

void CopyFile(FileInfo sourceFile, FileInfo destinationFile)
{
	EnsureArg.IsNotNull(sourceFile, nameof(sourceFile));
	EnsureArg.IsNotNull(destinationFile, nameof(destinationFile));
	EnsureArg.IsNotNull(unityProvidedAssemblyNames, nameof(unityProvidedAssemblyNames));
	EnsureArg.IsNotNullOrWhiteSpace(projectBuildDirectoryPath, nameof(projectBuildDirectoryPath));

	if (unityProvidedAssemblyNames.Contains(Path.GetFileNameWithoutExtension(sourceFile.Name)))
	{
		filesSkipped++;
		return;
	}

	var displayName = sourceFile.FullName.Remove(0, projectBuildDirectoryPath.Length);

	try
	{
		if (destinationFile.Exists && sourceFile.LastWriteTime <= destinationFile.LastWriteTime)
		{
			filesUnchanged++;
			return;
		}

		sourceFile.CopyTo(destinationFile.FullName, overwrite: true);
		filesCopied++;
		Console.WriteLine($"HQS - Post Build Event - Copied: {displayName}");
	}
	catch (Exception e)
	{
		filesFailed++;
		Console.WriteLine($"HQS - Post Build Event - Failed to copy: {displayName} with Exception: {e}");
	}
}
