﻿namespace HQS.Test.ConsoleApp;

internal class VoxelPlaneContourerPerformanceTester
{
	private readonly CoplanarChunk<Surface16>.Builder output;

	private readonly IPlaneContourer<Surface16, Voxel16SurfaceExtractor> contourer;

	public VoxelPlaneContourerPerformanceTester(
		ChunkSize size, SurfacePattern pattern, bool useSimple = false)
	{
		var contourerBuilder = ContourerBuilder.Create(size, useSimple);

		this.output = contourerBuilder.CreateCoplanarChunkBuilder();

		this.contourer = contourerBuilder.CreatePlaneContourer();
		this.contourer.Output = this.output;

		var filler = new SurfacePlaneFiller()
		{
			Pattern = pattern,
			NoiseFrequency = .1f,
			NoiseWeight = 3,
			NoiseCountOfVoxelTypes = 5,
		};

		filler.FillPlane(this.contourer.Surfaces);
	}

	public IPlaneContourerProfiler Profiler => this.contourer.Profiler;

	public void Contour()
	{
		this.output.ClearPages();
		this.contourer.ContourPlane(new PlaneIndex(0));
	}
}
