﻿namespace HQS.Test.ConsoleApp;

internal class VoxelPlaneContouringSubject
{
	private readonly CoplanarChunk<Surface16>.Builder output;

	private readonly IPlaneContourer<Surface16, Voxel16SurfaceExtractor> simpleContourer;

	private readonly IPlaneContourer<Surface16, Voxel16SurfaceExtractor> edgeContourer;

	public VoxelPlaneContouringSubject(ChunkSize size)
	{
		size.AssertValid();

		var contourerBuilder = ContourerBuilder.Create(size, false);

		this.output = contourerBuilder.CreateCoplanarChunkBuilder();

		this.simpleContourer = new SimplePlaneContourer<Surface16, Voxel16SurfaceExtractor>(size)
		{
			Comparer = Voxel16SurfaceExtractor.Default,
			Output = this.output,
		};

		this.edgeContourer = new TessalatePlaneContourer<Surface16, Voxel16SurfaceExtractor>(
			size, new LibTessDotNet.Pools(), contourerBuilder.Options.ContourerTrianglesCapacity)
		{
			Comparer = Voxel16SurfaceExtractor.Default,
			Output = this.output,
		};
	}

	public void SetContouringBounds(Int2 lower, Int2 dimensions)
	{
		this.simpleContourer.Bounds.SetBounds(lower, dimensions);
		this.edgeContourer.Bounds.SetBounds(lower, dimensions);
	}

	public void FillAllWith(SurfaceQuad<Surface16> quad) => this.FillAllWith(
		quad, Int2.Zero, this.simpleContourer.Surfaces.Indexer.Dimensions);

	public void FillAllWith(SurfaceQuad<Surface16> quad, Int2 origin, Int2 dimensions)
	{
		this.simpleContourer.Surfaces.FillArea(origin, dimensions, quad);
		this.edgeContourer.Surfaces.FillArea(origin, dimensions, quad);
	}

	public void FillWithNoise() => this.FillWithNoise(
		Int2.Zero, this.simpleContourer.Surfaces.Indexer.Dimensions);

	public void FillWithNoise(Int2 origin, Int2 dimensions)
	{
		var noise = new FastNoiseLite();
		noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		noise.SetFrequency(.1f);
		noise.SetSeed(0);

		int voxelTypes = 5;
		float noiseWeight = 3;
		var getValue = (float val) => (Surface16)((val * voxelTypes) + noiseWeight).Clamp(0, voxelTypes);

		this.simpleContourer.Surfaces.FillNoise(origin, dimensions, 0, noise, getValue);
		this.edgeContourer.Surfaces.FillNoise(origin, dimensions, 0, noise, getValue);
	}

	public void ContourAll()
	{
		this.output.ClearPages();
		this.simpleContourer.ContourPlane(new PlaneIndex(0));
		this.output.ClearPages();
		this.edgeContourer.ContourPlane(new PlaneIndex(0));
	}

	public void Contour(bool useSimple = false)
	{
		this.output.ClearPages();
		this.GetContourer(useSimple).ContourPlane(new PlaneIndex(0));
	}

	public void ContourPartitions(int partitions, bool useSimple = false)
	{
		Debug.Assert(partitions >= 1);

		var contourer = this.GetContourer(useSimple);
		var dimensions = contourer.Bounds.MaxDimensions / partitions;

		for (int iY = 0; iY < partitions; iY++)
		{
			for (int iX = 0; iX < partitions; iX++)
			{
				contourer.Bounds.SetBounds(new Int2(iX, iY) * dimensions, dimensions);
				this.output.ClearPages();
				contourer.ContourPlane(new PlaneIndex(0));
			}
		}
	}

	public void DebugContouring()
	{
		this.VerifyVoxelsMatch();

		this.output.ClearPages();
		this.simpleContourer.ContourPlane(new PlaneIndex(0));

		Console.WriteLine($"{nameof(this.simpleContourer)} Surfaces: {this.output.Surfaces.Count}");
		Console.WriteLine($"{nameof(this.simpleContourer)} Triangles: {this.output.Triangles.Count}");

		this.output.ClearPages();
		this.edgeContourer.ContourPlane(new PlaneIndex(0));

		Console.WriteLine($"{nameof(this.edgeContourer)} Surfaces: {this.output.Surfaces.Count}");
		Console.WriteLine($"{nameof(this.edgeContourer)} Triangles: {this.output.Triangles.Count}");
	}

	private IPlaneContourer<Surface16, Voxel16SurfaceExtractor> GetContourer(bool useSimple) =>
		useSimple ? this.simpleContourer : this.edgeContourer;

	private void VerifyVoxelsMatch()
	{
		int sideLength = this.simpleContourer.Surfaces.Indexer.SideLength;
		for (int iX = 0; iX < sideLength; iX++)
		{
			for (int iY = 0; iY < sideLength; iY++)
			{
				var voxel = this.simpleContourer.Surfaces[iX, iY];
				if (this.edgeContourer.Surfaces[iX, iY] != voxel)
				{
					throw new ArgumentException("Voxels don't match!");
				}
			}
		}

		Console.WriteLine("Voxels are the same.");
	}
}
