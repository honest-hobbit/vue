﻿namespace HQS.Test.ConsoleApp;

public class VoxelPlaneContourerPerformanceLoggingTests
{
	private const int WarmUpRepetitions = 5;

	private const int TestRepetitions = 50;

	public static void RunAllTests()
	{
		RunTests(new ChunkSize(7), SurfacePattern.Pentagons);
		RunTests(new ChunkSize(7), SurfacePattern.Grid);
		RunTests(new ChunkSize(7), SurfacePattern.SpinesVertical);
		RunTests(new ChunkSize(7), SurfacePattern.SpinesHorizontal);
		RunTests(new ChunkSize(7), SurfacePattern.Noise);
	}

	private static void RunTests(ChunkSize size, SurfacePattern pattern)
	{
		size.AssertValid();

		var subject = new VoxelPlaneContourerPerformanceTester(size, pattern);

		for (int count = 0; count < WarmUpRepetitions; count++)
		{
			subject.Contour();
		}

		subject.Profiler.Reset();

		for (int count = 0; count < TestRepetitions; count++)
		{
			subject.Contour();
		}

		Console.WriteLine($"{nameof(ChunkSize)}: {size}   {pattern}");

		var message = new StringBuilder();
		subject.Profiler.AppendPlaneAveragesTo(message);

		Console.WriteLine(message);
	}
}
