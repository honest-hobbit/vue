﻿using SP = HQS.VUE.Neo.SurfacePattern;

namespace HQS.Test.ConsoleApp;

// This tests multiple voxel patterns at different sizes to see what's fastest.
[MemoryDiagnoser]
public class VoxelPlaneContouringPatternBenchmarks
{
	private readonly SurfacePlaneFiller filler;

	private CoplanarChunk<Surface16>.Builder output;

	private IPlaneContourer<Surface16, Voxel16SurfaceExtractor> subject;

	public VoxelPlaneContouringPatternBenchmarks()
	{
		this.filler = new SurfacePlaneFiller()
		{
			NoiseFrequency = .1f,
			NoiseWeight = 3,
			NoiseCountOfVoxelTypes = 5,
		};
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<VoxelPlaneContouringPatternBenchmarks>();

	[Params(false)]
	public bool UseSimple { get; set; }

	////[Params(2, 3, 4, 5, 6, 7)]
	[Params(7)]
	public int Size { get; set; }

	[Params(SP.Empty, SP.Uniform, SP.Checkered, SP.Diamonds, SP.Triangles, SP.Pentagons, SP.Grid, SP.SpinesVertical, SP.SpinesHorizontal, SP.Noise)]
	public SurfacePattern Pattern { get; set; }

	[GlobalSetup]
	public void GlobalSetup()
	{
		var contourerBuilder = ContourerBuilder.Create(new ChunkSize(this.Size), this.UseSimple);
		this.output = contourerBuilder.CreateCoplanarChunkBuilder();
		this.subject = contourerBuilder.CreatePlaneContourer();
		this.subject.Output = this.output;
		this.filler.Pattern = this.Pattern;
		this.filler.FillPlane(this.subject.Surfaces);
		this.output.ClearPages();
		this.subject.ContourPlane(new PlaneIndex(0));
		this.output.ClearPages();
	}

	[Benchmark]
	public void Contour()
	{
		this.output.ClearPages();
		this.subject.ContourPlane(new PlaneIndex(0));
	}

	public void Debug()
	{
		this.UseSimple = false;
		this.Size = 7;
		this.Pattern = SP.Pentagons;

		Console.WriteLine("Pentagons");
		this.GlobalSetup();

		Console.WriteLine("Triangles");
		this.Pattern = SP.Triangles;
		this.GlobalSetup();

		Console.WriteLine("Checkered");
		this.Pattern = SP.Checkered;
		this.GlobalSetup();

		Console.WriteLine("Diamonds");
		this.Pattern = SP.Diamonds;
		this.GlobalSetup();

		Console.WriteLine("Grid");
		this.Pattern = SP.Grid;
		this.GlobalSetup();

		Console.WriteLine("Spines");
		this.Pattern = SP.SpinesVertical;
		this.GlobalSetup();

		Console.WriteLine("Noise");
		this.Pattern = SP.Noise;
		this.GlobalSetup();
	}
}
