﻿namespace HQS.Test.ConsoleApp;

public class VoxelPlaneContourerMultithreadingTests
{
	private readonly VoxelPlaneContouringSubject subject;

	private VoxelPlaneContourerMultithreadingTests(ChunkSize size)
	{
		size.AssertValid();

		this.subject = new VoxelPlaneContouringSubject(size);
		this.subject.FillWithNoise();
		this.subject.ContourAll();
	}

	public static void RunAllTests()
	{
		RunSizeTest(4);
		RunSizeTest(5);
		RunSizeTest(6);
		RunSizeTest(7);
	}

	public void ContourAll() => this.subject.ContourAll();

	private static void RunSizeTest(int chunkSize)
	{
		Console.WriteLine($"Started size {chunkSize}");

		var timer = new Stopwatch();
		int maxDegreeOfParallelism = DegreeOfParallelism.ProcessorCount;
		int repeat = 1000;

		timer.Restart();

		var pool = new ThreadSafePool<VoxelPlaneContourerMultithreadingTests>(
			() => new VoxelPlaneContourerMultithreadingTests(new ChunkSize(chunkSize)));
		pool.Create(maxDegreeOfParallelism);

		timer.Stop();
		Console.WriteLine($"Setup time: {timer.Elapsed}");
		timer.Restart();

		Parallel.For(
			0,
			repeat,
			new ParallelOptions() { MaxDegreeOfParallelism = maxDegreeOfParallelism },
			i =>
			{
				var subject = pool.Rent();
				subject.ContourAll();
				pool.Return(subject);
			});

		timer.Stop();
		var multiTime = timer.Elapsed;
		Console.WriteLine($"Multithreaded time: {multiTime}");
		timer.Restart();

		for (int i = 0; i < repeat; i++)
		{
			var subject = pool.Rent();
			subject.ContourAll();
			pool.Return(subject);
		}

		timer.Stop();
		var singleTime = timer.Elapsed;
		Console.WriteLine($"Single threaded time: {singleTime}");

		Console.WriteLine($"Multithreaded is {singleTime / multiTime:N2} times faster.");
		Console.WriteLine();
	}
}
