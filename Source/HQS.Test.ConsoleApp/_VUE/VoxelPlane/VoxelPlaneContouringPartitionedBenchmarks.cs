﻿namespace HQS.Test.ConsoleApp;

// This tests contouring the same amount of voxels but as a number of smaller areas to see what's fastest.
public class VoxelPlaneContouringPartitionedBenchmarks
{
	private readonly VoxelPlaneContouringSubject size7 = new VoxelPlaneContouringSubject(new ChunkSize(7));

	public VoxelPlaneContouringPartitionedBenchmarks()
	{
		this.size7.FillWithNoise();
		this.size7.ContourAll();
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<VoxelPlaneContouringPartitionedBenchmarks>();

	[Benchmark]
	public void Edge_1Partition() => this.size7.ContourPartitions(1);

	[Benchmark]
	public void Edge_2Partitions() => this.size7.ContourPartitions(2);

	[Benchmark]
	public void Edge_4Partitions() => this.size7.ContourPartitions(4);

	[Benchmark]
	public void Edge_8Partitions() => this.size7.ContourPartitions(8);

	[Benchmark]
	public void Edge_16Partitions() => this.size7.ContourPartitions(16);
}
