﻿namespace HQS.Test.ConsoleApp;

// This tests contouring an 8 by 8 area of voxels, but using larger and larger sized contourers
// to see if there is any performance loss for using contourers that are larger than what's requirred.
public class VoxelPlaneContouringOversizedBenchmarks
{
	private readonly VoxelPlaneContouringSubject size3 = new VoxelPlaneContouringSubject(new ChunkSize(3));

	private readonly VoxelPlaneContouringSubject size4 = new VoxelPlaneContouringSubject(new ChunkSize(4));

	private readonly VoxelPlaneContouringSubject size5 = new VoxelPlaneContouringSubject(new ChunkSize(5));

	private readonly VoxelPlaneContouringSubject size6 = new VoxelPlaneContouringSubject(new ChunkSize(6));

	private readonly VoxelPlaneContouringSubject size7 = new VoxelPlaneContouringSubject(new ChunkSize(7));

	public VoxelPlaneContouringOversizedBenchmarks()
	{
		var origin = Int2.Zero;
		var dimensions = new Int2(8);

		Initialize(this.size3);
		Initialize(this.size4);
		Initialize(this.size5);
		Initialize(this.size6);
		Initialize(this.size7);

		void Initialize(VoxelPlaneContouringSubject subject)
		{
			Debug.Assert(subject != null);

			subject.FillWithNoise(origin, dimensions);
			subject.SetContouringBounds(origin, dimensions);
			subject.ContourAll();
		}
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<VoxelPlaneContouringOversizedBenchmarks>();

	[Benchmark]
	public void Size3_EdgeLoopContour() => this.size3.Contour();

	[Benchmark]
	public void Size4_EdgeLoopContour() => this.size4.Contour();

	[Benchmark]
	public void Size5_EdgeLoopContour() => this.size5.Contour();

	[Benchmark]
	public void Size6_EdgeLoopContour() => this.size6.Contour();

	[Benchmark]
	public void Size7_EdgeLoopContour() => this.size7.Contour();
}
