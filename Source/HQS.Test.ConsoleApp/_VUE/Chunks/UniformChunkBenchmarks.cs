﻿namespace HQS.Test.ConsoleApp;

public class UniformChunkBenchmarks
{
	private const int RepeatPer = 10;

	private const int RepeatRandom = 1000;

	private const int NumChunks = 1000;

	private const int VoxelsLength = 4096;

	private readonly List<CubeChunk> chunks = new List<CubeChunk>(NumChunks);

	private readonly ushort[] voxelsBuffer = new ushort[VoxelsLength];

	public UniformChunkBenchmarks()
	{
		for (int i = 0; i < NumChunks; i++)
		{
			if (i.IsDivisibleBy(3))
			{
				this.chunks.Add(new CubeChunk((ushort)i));
			}
			else
			{
				this.chunks.Add(CubeChunk.CreateNonuniformChunk(i));
			}
		}

		this.chunks.ShuffleList(new Random(1));
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<UniformChunkBenchmarks>();

	[Benchmark]
	public void PerChunkIfUniformOnce()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.chunks[chunkIndex];
				long sum = 0;

				if (chunk.IsUniform)
				{
					for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
					{
						sum += chunk.UniformVoxel;
					}
				}
				else
				{
					for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
					{
						sum += chunk.Voxels[voxelIndex];
					}
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	[Benchmark]
	public void PerChunkIfUniformAssign()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.chunks[chunkIndex];
				long sum = 0;

				ReadOnlyArray<ushort> voxels;
				if (chunk.IsUniform)
				{
					this.voxelsBuffer.SetAllTo(chunk.UniformVoxel);
					voxels = new ReadOnlyArray<ushort>(this.voxelsBuffer);
				}
				else
				{
					voxels = chunk.Voxels;
				}

				for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
				{
					sum += voxels[voxelIndex];
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	[Benchmark]
	public void PerChunkCopy()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.chunks[chunkIndex];
				long sum = 0;

				if (chunk.IsUniform)
				{
					this.voxelsBuffer.SetAllTo(chunk.UniformVoxel);
				}
				else
				{
					chunk.Voxels.CopyTo(this.voxelsBuffer);
				}

				for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
				{
					sum += this.voxelsBuffer[voxelIndex];
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	[Benchmark]
	public void PerChunkIfUniform()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.chunks[chunkIndex];
				long sum = 0;

				for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
				{
					if (chunk.IsUniform)
					{
						sum += chunk.UniformVoxel;
					}
					else
					{
						sum += chunk.Voxels[voxelIndex];
					}
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	[Benchmark]
	public void PerChunkAlwaysVoxels()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.chunks[chunkIndex];
				long sum = 0;

				for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
				{
					sum += chunk.Voxels[voxelIndex];
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	[Benchmark]
	public void RandomChunksIfUniform()
	{
		for (int count = 0; count < RepeatRandom; count++)
		{
			long sum = 0;
			int voxelIndex = 0;

			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.chunks[chunkIndex];
				if (chunk.IsUniform)
				{
					sum += chunk.UniformVoxel;
				}
				else
				{
					sum += chunk.Voxels[voxelIndex];
				}

				voxelIndex += 3;
				voxelIndex %= VoxelsLength;
			}

			if (sum < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
			}
		}
	}

	[Benchmark]
	public void RandomChunksAlwaysVoxels()
	{
		for (int count = 0; count < RepeatRandom; count++)
		{
			long sum = 0;
			int voxelIndex = 0;

			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.chunks[chunkIndex];
				sum += chunk.Voxels[voxelIndex];
				voxelIndex += 3;
				voxelIndex %= VoxelsLength;
			}

			if (sum < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
			}
		}
	}

	private readonly struct CubeChunk
	{
		private readonly ushort[] voxels;

		public CubeChunk(ushort uniformVoxel)
		{
			this.IsUniform = true;
			this.UniformVoxel = uniformVoxel;
			this.voxels = Factory.CreateArray(VoxelsLength, uniformVoxel);
		}

		public CubeChunk(ushort[] voxels)
		{
			this.IsUniform = false;
			this.UniformVoxel = default;
			this.voxels = voxels;
		}

		public bool IsUniform { get; }

		public ushort UniformVoxel { get; }

		public ReadOnlyArray<ushort> Voxels => new ReadOnlyArray<ushort>(this.voxels);

		public static CubeChunk CreateNonuniformChunk(int start)
		{
			return new CubeChunk(Factory.CreateArray(VoxelsLength, i => (ushort)(start + i)));
		}
	}
}
