﻿namespace HQS.Test.ConsoleApp;

public class WedgeChunkLayoutBenchmarks
{
	private const int RepeatPer = 10;

	private const int RepeatRandom = 1000;

	private const int NumChunks = 1000;

	private const int VoxelsLength = 4096;

	private readonly List<WedgeChunkLookup> lookupChunks = new List<WedgeChunkLookup>(NumChunks);

	private readonly List<WedgeChunkInline> inlineChunks = new List<WedgeChunkInline>(NumChunks);

	private readonly List<WedgeChunkParallel> parallelChunks = new List<WedgeChunkParallel>(NumChunks);

	public WedgeChunkLayoutBenchmarks()
	{
		for (int i = 0; i < NumChunks; i++)
		{
			this.lookupChunks.Add(WedgeChunkLookup.Create(i));
			this.inlineChunks.Add(WedgeChunkInline.Create(i));
			this.parallelChunks.Add(WedgeChunkParallel.Create(i));
		}

		this.lookupChunks.ShuffleList(new Random(1));
		this.inlineChunks.ShuffleList(new Random(1));
		this.parallelChunks.ShuffleList(new Random(1));
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<WedgeChunkLayoutBenchmarks>();

	[Benchmark]
	public void PerChunkLookup()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				long sum = 0;
				var chunk = this.lookupChunks[chunkIndex];

				for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
				{
					////int offset = chunk.Offsets[voxelIndex] & WedgeChunkLookup.Mask;
					////var voxel = chunk.Voxels[offset];
					////sum += voxel.Sum(voxelIndex % 6);

					sum += chunk.Sum(voxelIndex);

					////if (voxel.Sum(voxelIndex % 6) != chunk.Sum(voxelIndex))
					////{
					////	throw new Exception("Opps a bad happened");
					////}
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	[Benchmark]
	public void PerChunkInline()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				long sum = 0;
				var chunk = this.inlineChunks[chunkIndex];

				for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
				{
					var voxel = chunk.Voxels[voxelIndex];
					sum += voxel.Sum(5);
					////sum += voxel.Sum(voxelIndex % 6);
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	////[Benchmark]
	public void PerChunkParallel()
	{
		for (int count = 0; count < RepeatPer; count++)
		{
			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				long sum = 0;
				var chunk = this.parallelChunks[chunkIndex];

				for (int voxelIndex = 0; voxelIndex < VoxelsLength; voxelIndex++)
				{
					var voxel = chunk.GetVoxel(voxelIndex);
					sum += voxel.Sum(voxelIndex % 6);
				}

				if (sum < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
				}
			}
		}
	}

	[Benchmark]
	public void RandomChunksLookup()
	{
		for (int count = 0; count < RepeatRandom; count++)
		{
			long sum = 0;
			int voxelIndex = 0;

			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.lookupChunks[chunkIndex];
				////int offset = chunk.Offsets[voxelIndex] & WedgeChunkLookup.Mask;
				////var voxel = chunk.Voxels[offset];
				////sum += voxel.Sum(6);

				sum += chunk.Sum(voxelIndex);

				////if (voxel.Sum(voxelIndex % 6) != chunk.Sum(voxelIndex))
				////{
				////	throw new Exception("Opps a bad happened");
				////}

				voxelIndex += 3;
				voxelIndex %= VoxelsLength;
			}

			if (sum < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
			}
		}
	}

	[Benchmark]
	public void RandomChunksInline()
	{
		for (int count = 0; count < RepeatRandom; count++)
		{
			long sum = 0;
			int voxelIndex = 0;

			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.inlineChunks[chunkIndex];
				var voxel = chunk.Voxels[voxelIndex];
				sum += voxel.Sum(6);

				voxelIndex += 3;
				voxelIndex %= VoxelsLength;
			}

			if (sum < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
			}
		}
	}

	////[Benchmark]
	public void RandomChunksParallel()
	{
		for (int count = 0; count < RepeatRandom; count++)
		{
			long sum = 0;
			int voxelIndex = 0;

			for (int chunkIndex = 0; chunkIndex < NumChunks; chunkIndex++)
			{
				var chunk = this.parallelChunks[chunkIndex];
				var voxel = chunk.GetVoxel(voxelIndex);
				sum += voxel.Sum(6);

				voxelIndex += 3;
				voxelIndex %= VoxelsLength;
			}

			if (sum < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(sum), "Should be positive.");
			}
		}
	}

	[SuppressMessage("StyleCop", "SA1313", Justification = "Record syntax.")]
	private readonly record struct WedgeVoxel(ushort A, ushort B, ushort C, ushort D, ushort E, ushort F)
	{
		public static WedgeVoxel Create(int a, int b, int c, int d, int e, int f) =>
			new WedgeVoxel((ushort)a, (ushort)b, (ushort)c, (ushort)d, (ushort)e, (ushort)f);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public long Sum(int count)
		{
			long result = 0;
			if (count > 0) { result += this.A; }
			if (count > 1) { result += this.B; }
			if (count > 2) { result += this.C; }
			if (count > 3) { result += this.D; }
			if (count > 4) { result += this.E; }
			if (count > 5) { result += this.F; }
			return result;
		}
	}

	private readonly struct WedgeChunkLookup
	{
		public static readonly int Mask = BitMask.CreateIntWithOnesInLowerBits(Shift);

		private const int Shift = 28;

		private readonly int[] offset;

		private readonly WedgeVoxel[] voxels;

		public WedgeChunkLookup(int[] offset, WedgeVoxel[] voxels)
		{
			this.offset = offset;
			this.voxels = voxels;
		}

		public ReadOnlyArray<int> Offsets => new ReadOnlyArray<int>(this.offset);

		public ReadOnlyArray<WedgeVoxel> Voxels => new ReadOnlyArray<WedgeVoxel>(this.voxels);

		public static WedgeChunkLookup Create(int s)
		{
			////Factory.CreateArray(VoxelsLength, i => i | ((i % 6) << Shift)),
			return new WedgeChunkLookup(
				Factory.CreateArray(VoxelsLength, i => i | (5 << Shift)),
				Factory.CreateArray(VoxelsLength, i => WedgeVoxel.Create(
					s + i, s + i + 1, s + i + 2, s + i + 3, s + i + 4, s + i + 5)));
		}

		public long Sum(int i)
		{
			int offset = this.offset[i];
			return this.voxels[offset & Mask].Sum(offset >> Shift);
		}
	}

	private readonly struct WedgeChunkInline
	{
		private readonly WedgeVoxel[] voxels;

		public WedgeChunkInline(WedgeVoxel[] voxels)
		{
			this.voxels = voxels;
		}

		public ReadOnlyArray<WedgeVoxel> Voxels => new ReadOnlyArray<WedgeVoxel>(this.voxels);

		public static WedgeChunkInline Create(int s)
		{
			return new WedgeChunkInline(Factory.CreateArray(VoxelsLength, i => WedgeVoxel.Create(
				s + i, s + i + 1, s + i + 2, s + i + 3, s + i + 4, s + i + 5)));
		}
	}

	private readonly struct WedgeChunkParallel
	{
		private readonly ushort[] voxelsA;

		private readonly ushort[] voxelsB;

		private readonly ushort[] voxelsC;

		private readonly ushort[] voxelsD;

		private readonly ushort[] voxelsE;

		private readonly ushort[] voxelsF;

		public WedgeChunkParallel(ushort[] a, ushort[] b, ushort[] c, ushort[] d, ushort[] e, ushort[] f)
		{
			this.voxelsA = a;
			this.voxelsB = b;
			this.voxelsC = c;
			this.voxelsD = d;
			this.voxelsE = e;
			this.voxelsF = f;
		}

		public ReadOnlyArray<ushort> VoxelsA => new ReadOnlyArray<ushort>(this.voxelsA);

		public ReadOnlyArray<ushort> VoxelsB => new ReadOnlyArray<ushort>(this.voxelsB);

		public ReadOnlyArray<ushort> VoxelsC => new ReadOnlyArray<ushort>(this.voxelsC);

		public ReadOnlyArray<ushort> VoxelsD => new ReadOnlyArray<ushort>(this.voxelsD);

		public ReadOnlyArray<ushort> VoxelsE => new ReadOnlyArray<ushort>(this.voxelsE);

		public ReadOnlyArray<ushort> VoxelsF => new ReadOnlyArray<ushort>(this.voxelsF);

		public static WedgeChunkParallel Create(int s)
		{
			return new WedgeChunkParallel(
				Factory.CreateArray(VoxelsLength, i => (ushort)(s + i)),
				Factory.CreateArray(VoxelsLength, i => (ushort)(s + i + 1)),
				Factory.CreateArray(VoxelsLength, i => (ushort)(s + i + 2)),
				Factory.CreateArray(VoxelsLength, i => (ushort)(s + i + 3)),
				Factory.CreateArray(VoxelsLength, i => (ushort)(s + i + 4)),
				Factory.CreateArray(VoxelsLength, i => (ushort)(s + i + 5)));
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public WedgeVoxel GetVoxel(int i)
		{
			return new WedgeVoxel(
				this.voxelsA[i], this.voxelsB[i], this.voxelsC[i], this.voxelsD[i], this.voxelsE[i], this.voxelsF[i]);
		}
	}
}
