﻿namespace HQS.Test.ConsoleApp;

[MemoryDiagnoser]
public class CubeChunkContourerBenchmarks
{
	private ContourerBuilder<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>> contourerBuilder;

	private ICubeChunkContourer<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>> chunkContourer;

	private CubeChunk<Voxel16> cubeChunk;

	public static void RunBenchmarks() => BenchmarkRunner.Run<CubeChunkContourerBenchmarks>();

	public static void TestRun()
	{
		var subject = new CubeChunkContourerBenchmarks()
		{
			ChunkSize = 7,
			Threads = 8,
			Repetitions = 1,
		};

		subject.GlobalSetup();
		subject.Contour();
		subject.chunkContourer.Profiler.Reset();

		Console.WriteLine("Setup Done");
		Console.WriteLine();

		subject.Repetitions = 10;
		subject.Contour();

		Console.WriteLine("Workload 1 Done");
		Console.WriteLine();

		Console.WriteLine(subject.chunkContourer.Profiler.GetChunkAverages());
	}

	[Params(7)]
	////[Params(3, 4, 5, 6, 7)]
	public int ChunkSize { get; set; }

	[Params(1, 8)]
	////[Params(1, 2, 4, 8)]
	////[Params(1, 2, 4, 6, 8, 12, 16, 20)]
	public int Threads { get; set; }

	[Params(1)]
	public int Repetitions { get; set; }

	[GlobalSetup]
	public void GlobalSetup()
	{
		var voxelSize = new ChunkSize(this.ChunkSize);
		var origin = new Int3(0, 0, 0);
		var dimensions = new Int3(128, 128, 128);

		this.contourerBuilder = ContourerBuilder.CreatePalette(voxelSize, false);
		////this.contourerBuilder = ContourerBuilder.Create(voxelSize, false);

		this.chunkContourer = this.contourerBuilder.CreateCubeChunkContourer();
		this.chunkContourer.SortAndCompactOutput = true;
		this.chunkContourer.CheckForTrianglesMissed = false;
		this.chunkContourer.MaxDegreeOfParallelism = this.Threads;

		this.chunkContourer.Bounds.ClampToBounds(ref origin, ref dimensions);
		this.chunkContourer.Bounds.SetBounds(origin, dimensions);

		this.contourerBuilder.CoplanarChunkPools.PlanePages.Create(300);
		this.contourerBuilder.CoplanarChunkPools.SurfacePages.Create(300);
		this.contourerBuilder.CoplanarChunkPools.TrianglePages.Create(4000);
		this.contourerBuilder.DualPlanes.Create(this.chunkContourer.MaxDegreeOfParallelism);

		var cubeChunkFiller = new NoiseCubeChunkFiller()
		{
			VoxelTypeA = 1,
			VoxelTypeB = 2,
			VoxelTypeC = 3,
			Seed = 0,
			Frequency = .1f,
			Weight = .75f,
		};

		var cubeChunkBuilder = this.contourerBuilder.CreateCubeChunkBuilder();
		cubeChunkBuilder.Initialize(voxelSize);
		cubeChunkFiller.FillChunk(cubeChunkBuilder);

		this.cubeChunk = cubeChunkBuilder.Build();

		var coplanarChunkWarmUp = this.chunkContourer.ContourChunk(this.cubeChunk);
		this.contourerBuilder.CoplanarChunkPools.ReturnChunk(coplanarChunkWarmUp);
	}

	[Benchmark]
	public void Contour()
	{
		for (int i = 0; i < this.Repetitions; i++)
		{
			var coplanarChunkWorkload = this.chunkContourer.ContourChunk(this.cubeChunk);
			this.contourerBuilder.CoplanarChunkPools.ReturnChunk(coplanarChunkWorkload);
		}
	}
}
