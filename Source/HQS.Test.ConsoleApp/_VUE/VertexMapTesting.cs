﻿namespace HQS.Test.ConsoleApp;

public static class VertexMapTesting
{
	public static void RunTest()
	{
		var subject = new PointToVertexIndexMap(ChunkSize.Max);
		var pointToVertexIndex = new Dictionary<CompactPoint2, int>(EqualityComparer.ForStruct<CompactPoint2>());

		int maxIndex = subject.Length;

		Console.WriteLine("Writing vertices until vertex threshold resets.");

		RunUntilVertexThresholdResets(maxIndex);

		Console.WriteLine("Vertex threshold reset. Repeating test.");

		RunUntilVertexThresholdResets(maxIndex);

		Console.WriteLine("Vertex threshold reset again. Test successful.");

		void RunUntilVertexThresholdResets(int maxPointIndex)
		{
			Ensure.That(maxPointIndex, nameof(maxPointIndex)).IsInRange(1, subject.Length);

			Console.WriteLine($"{nameof(subject.VertexThreshold)}: {subject.VertexThreshold}");

			long count = 0;
			int previousVertexThreshold = subject.VertexThreshold;

			while (previousVertexThreshold <= subject.VertexThreshold)
			{
				int expectedVertexIndex = 0;

				for (int i = 0; i < maxPointIndex; i++)
				{
					var point = new CompactPoint2((ushort)i);
					var shared = subject.TryGetOrCreateVertexIndex(point, out var vertexIndex);

					if (shared)
					{
						throw new Exception("Vertex should not be shared.");
					}

					if (vertexIndex != expectedVertexIndex)
					{
						throw new Exception("Vertex index does not match expected index.");
					}

					expectedVertexIndex++;
					pointToVertexIndex.Add(point, vertexIndex);
				}

				foreach (var pair in pointToVertexIndex)
				{
					var shared = subject.TryGetOrCreateVertexIndex(pair.Key, out var vertexIndex);

					if (!shared)
					{
						throw new Exception("Vertex should be shared.");
					}

					if (vertexIndex != pair.Value)
					{
						throw new Exception("Vertex index does not match.");
					}
				}

				pointToVertexIndex.Clear();

				count++;
				if (count == 1000)
				{
					count = 0;
					Console.WriteLine($"{nameof(subject.VertexThreshold)}: {subject.VertexThreshold}");
				}

				previousVertexThreshold = subject.VertexThreshold;
				subject.Clear();
			}

			Console.WriteLine($"{nameof(subject.VertexThreshold)}: {subject.VertexThreshold}");
		}
	}
}
