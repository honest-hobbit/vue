﻿namespace HQS.Test.ConsoleApp;

public static class SqliteFileStoreTesting
{
	public static void RunTest()
	{
		var timer = new Stopwatch();
		CreateFolders();
		var connection = CreateConnectionBuilder();
		var streamManager = new RecyclableMemoryStreamManager();

		timer.Restart();

		using var store = new SqliteAssetStore(connection, streamManager.GetStream, alwaysOpen: false);

		timer.Stop();
		Console.WriteLine("Create: " + timer.ElapsedMilliseconds);

		timer.Restart();

		store.Initialize();

		timer.Stop();
		Console.WriteLine("Initialize: " + timer.ElapsedMilliseconds);

		int maxX = 5; // 5
		int maxY = 1; // 1
		var chunksFirst = new List<ChunkDocument<Int2>>();
		var chunksSecond = new List<ChunkDocument<Int2>>();

		var assetKey = Guid.NewGuid();
		for (int iX = 0; iX <= maxX; iX++)
		{
			for (int iY = 0; iY <= maxY; iY++)
			{
				var inputStream = new FileStream(
					@$"D:\Testing2\Source\tile {iX},{iY}.png", FileMode.Open, FileAccess.Read);
				chunksFirst.Add(new ChunkDocument<Int2>(
					Guid.NewGuid(), assetKey, new Int2(iX, iY), 1, inputStream));
			}
		}

		timer.Restart();

		store.UpdateAsset(
			new AssetDocument(assetKey, "Texture", MakeBlob(4, 0), MakeBlob(8, 8)), chunksFirst);

		timer.Stop();
		Console.WriteLine("Write 1: " + timer.ElapsedMilliseconds);

		chunksFirst.ForEach(x => x.Body.Dispose());
		chunksFirst.Clear();

		var secondAssetKey = Guid.NewGuid();
		for (int iX = 0; iX <= maxX; iX++)
		{
			for (int iY = 0; iY <= maxY; iY++)
			{
				var replaceStream = new FileStream(
					@$"D:\Testing2\Source\tile 0,0.png", FileMode.Open, FileAccess.Read);
				chunksSecond.Add(new ChunkDocument<Int2>(
					Guid.NewGuid(), secondAssetKey, new Int2(iX, iY), 1, replaceStream));
			}
		}

		timer.Restart();

		store.UpdateAsset(
			new AssetDocument(secondAssetKey, "Texture", MakeBlob(3, 1), MakeBlob(5, 2)), chunksSecond);

		timer.Stop();
		Console.WriteLine("Write 2: " + timer.ElapsedMilliseconds);

		chunksSecond.ForEach(x => x.Body.Dispose());
		chunksSecond.Clear();

		////using var getAssetStream = new MemoryStream();
		////store.TryGetAsset(secondAssetKey, getAssetStream);

		////store.DeleteAsset(secondAssetKey);
		////store.DeleteAllAssets();

		timer.Restart();

		for (int iX = 0; iX <= maxX; iX++)
		{
			for (int iY = 0; iY <= maxY; iY++)
			{
				if (store.TryGetChunk(assetKey, new Int2(iX, iY), out var chunkRecord))
				{
					using var outputStream = new FileStream(
						@$"D:\Testing2\Result\tile {iX},{iY}.png", FileMode.OpenOrCreate, FileAccess.ReadWrite);
					chunkRecord.Body.CopyTo(outputStream);
					chunkRecord.Body.Dispose();
				}
			}
		}

		timer.Stop();
		Console.WriteLine("Read: " + timer.ElapsedMilliseconds);
	}

	public static void RunPagingAssetsTest()
	{
		var timer = new Stopwatch();
		CreateFolders();
		var connection = CreateConnectionBuilder();
		var streamManager = new RecyclableMemoryStreamManager();
		using var store = new SqliteAssetStore(connection, streamManager.GetStream, alwaysOpen: true);
		store.Initialize();

		var key1 = Guid.Parse("9B0F8391-1536-4926-868B-B93C9324778F");
		var key2 = Guid.Parse("9C0F8391-1536-4926-868B-B93C9324778F");
		var key3 = Guid.Parse("9D0F8391-1536-4926-868B-B93C9324778F");
		var key4 = Guid.Parse("9E0F8391-1536-4926-868B-B93C9324778F");
		var key5 = Guid.Parse("9F0F8391-1536-4926-868B-B93C9324778F");
		var key6 = Guid.Parse("A00F8391-1536-4926-868B-B93C9324778F");
		var key7 = Guid.Parse("A10F8391-1536-4926-868B-B93C9324778F");
		var key8 = Guid.Parse("A20F8391-1536-4926-868B-B93C9324778F");
		var key9 = Guid.Parse("A30F8391-1536-4926-868B-B93C9324778F");
		var key10 = Guid.Parse("A40F8391-1536-4926-868B-B93C9324778F");

		store.UpdateAsset(new AssetDocument(key1, "Blobbiely", new MemoryStream(new byte[] { 1 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key2, "Blobbie", new MemoryStream(new byte[] { 2 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key3, "Blobbie", new MemoryStream(new byte[] { 3 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key4, "Blobbie", new MemoryStream(new byte[] { 4 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key5, "Blobbie", new MemoryStream(new byte[] { 5 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key6, "Blobbie", new MemoryStream(new byte[] { 6 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key7, "Blobbie", new MemoryStream(new byte[] { 7 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key8, "Blobbie", new MemoryStream(new byte[] { 8 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key9, "Blobbie", new MemoryStream(new byte[] { 9 }), MakeBlob(4, 0)));
		store.UpdateAsset(new AssetDocument(key10, "Blobbiely", new MemoryStream(new byte[] { 10 }), MakeBlob(4, 0)));

		var assets = new List<AssetHeader>();
		var firstKey = Guid.Empty;

		Iterate(5, false);
		Iterate(3, true);

		Iterate(5, false);
		Iterate(3, true);

		Iterate(5, false);
		Iterate(3, true);

		Iterate(5, false);
		Iterate(3, true);

		void Iterate(int count, bool reverse)
		{
			////store.GetAllAssetHeaders(assets, new PagingArgs(firstKey, count, reverse));
			store.GetAssetHeaders("Blobbie", assets, new PagingArgs(firstKey, count, reverse));

			foreach (var header in assets)
			{
				Console.WriteLine(header.Data.ReadByte());
			}

			Console.WriteLine();
			firstKey = assets[^1].Key;
			assets.Clear();
		}
	}

	public static void RunPagingChunksTest()
	{
		var timer = new Stopwatch();
		CreateFolders();
		var connection = CreateConnectionBuilder();
		var streamManager = new RecyclableMemoryStreamManager();
		using var store = new SqliteAssetStore(connection, streamManager.GetStream, alwaysOpen: true);
		store.Initialize();

		var key1 = Guid.Parse("9B0F8391-1536-4926-868B-B93C9324778F");
		var key2 = Guid.Parse("9C0F8391-1536-4926-868B-B93C9324778F");
		var key3 = Guid.Parse("9D0F8391-1536-4926-868B-B93C9324778F");
		var key4 = Guid.Parse("9E0F8391-1536-4926-868B-B93C9324778F");
		var key5 = Guid.Parse("9F0F8391-1536-4926-868B-B93C9324778F");
		var key6 = Guid.Parse("A00F8391-1536-4926-868B-B93C9324778F");
		var key7 = Guid.Parse("A10F8391-1536-4926-868B-B93C9324778F");
		var key8 = Guid.Parse("A20F8391-1536-4926-868B-B93C9324778F");
		var key9 = Guid.Parse("A30F8391-1536-4926-868B-B93C9324778F");
		var key10 = Guid.Parse("A40F8391-1536-4926-868B-B93C9324778F");

		var assetKey = Guid.NewGuid();
		var chunks = new List<ChunkDocument<Int2>>()
			{
				new ChunkDocument<Int2>(key1, assetKey, new Int2(1, 0), 0, new MemoryStream(new byte[] { 1 })),
				new ChunkDocument<Int2>(key2, assetKey, new Int2(2, 0), 0, new MemoryStream(new byte[] { 2 })),
				new ChunkDocument<Int2>(key3, assetKey, new Int2(3, 0), 0, new MemoryStream(new byte[] { 3 })),
				new ChunkDocument<Int2>(key4, assetKey, new Int2(4, 0), 0, new MemoryStream(new byte[] { 4 })),
				new ChunkDocument<Int2>(key5, assetKey, new Int2(5, 0), 0, new MemoryStream(new byte[] { 5 })),
				new ChunkDocument<Int2>(key6, assetKey, new Int2(6, 0), 0, new MemoryStream(new byte[] { 6 })),
				new ChunkDocument<Int2>(key7, assetKey, new Int2(7, 0), 0, new MemoryStream(new byte[] { 7 })),
				new ChunkDocument<Int2>(key8, assetKey, new Int2(8, 0), 0, new MemoryStream(new byte[] { 8 })),
				new ChunkDocument<Int2>(key9, assetKey, new Int2(9, 0), 0, new MemoryStream(new byte[] { 9 })),
				new ChunkDocument<Int2>(key10, assetKey, new Int2(10, 0), 0, new MemoryStream(new byte[] { 10 })),
			};

		store.UpdateAsset(
			new AssetDocument(assetKey, "Blobbie", MakeBlob(4, 0), MakeBlob(4, 0)), chunks);

		var assetKey2 = Guid.NewGuid();
		chunks.Clear();
		chunks.Add(new ChunkDocument<Int2>(Guid.NewGuid(), assetKey2, new Int2(0, 1), 0, new MemoryStream(new byte[] { 255 })));
		chunks.Add(new ChunkDocument<Int2>(Guid.NewGuid(), assetKey2, new Int2(0, 2), 0, new MemoryStream(new byte[] { 255 })));
		chunks.Add(new ChunkDocument<Int2>(Guid.NewGuid(), assetKey2, new Int2(0, 3), 0, new MemoryStream(new byte[] { 255 })));
		chunks.Add(new ChunkDocument<Int2>(Guid.NewGuid(), assetKey2, new Int2(0, 4), 0, new MemoryStream(new byte[] { 255 })));
		store.UpdateAsset(
			new AssetDocument(assetKey2, "Blobbie", MakeBlob(4, 0), MakeBlob(4, 0)), chunks);

		var firstKey = Guid.Empty;
		var chunkLabels = new List<ChunkHeader<Int2>>();

		Console.WriteLine();

		Iterate(5, false);
		Iterate(3, true);

		Iterate(5, false);
		Iterate(3, true);

		Iterate(5, false);
		Iterate(3, true);

		Iterate(5, false);
		Iterate(3, true);

		void Iterate(int count, bool reverse)
		{
			store.GetChunkHeaders(assetKey, chunkLabels, new PagingArgs(firstKey, count, reverse));

			foreach (var label in chunkLabels)
			{
				Console.WriteLine(label.Location.X.ToString());
			}

			Console.WriteLine();
			firstKey = chunkLabels[^1].Key;
			chunkLabels.Clear();
		}
	}

	public static void ReadWriteReadBlobTest()
	{
		var timer = new Stopwatch();
		CreateFolders();
		var connection = CreateConnectionBuilder();
		var streamManager = new RecyclableMemoryStreamManager();
		using var store = new SqliteAssetStore(connection, streamManager.GetStream, alwaysOpen: true);
		store.Initialize();

		int blobSize = 102400;
		var originalBytes = Factory.CreateArray(blobSize, i => (byte)i);
		var newBytes = Factory.CreateArray(blobSize, i => (byte)255);

		var key = Guid.NewGuid();
		store.UpdateAsset(new AssetDocument(key, "Blobs", new MemoryStream(originalBytes), MakeBlob(4, 0)));

		var assets = new List<AssetHeader>();
		store.GetAssetHeaders("Blobs", assets, PagingArgs.GetAll);

		store.UpdateAsset(new AssetDocument(key, "Blobs", new MemoryStream(newBytes), MakeBlob(4, 0)));

		var returnedBytes = assets[0].Data;

		returnedBytes.Length.Should().Be(blobSize);

		for (int i = 0; i < blobSize; i++)
		{
			returnedBytes.ReadByte().Should().Be(i);
		}

		returnedBytes.Dispose();
	}

	public static void RunManyTransactionsTest()
	{
		var timer = new Stopwatch();
		CreateFolders();
		var connection = CreateConnectionBuilder();
		var streamManager = new RecyclableMemoryStreamManager();

		bool useTimeout = true;

		IAssetStore store = null;
		if (useTimeout)
		{
			store = new TimeoutAssetStore(
				TimeSpan.FromSeconds(1),
				() =>
				{
					var tempStore = new SqliteAssetStore(
						connection, streamManager.GetStream, alwaysOpen: true);
					tempStore.Initialize();
					return tempStore;
				});
		}
		else
		{
			var tempStore = new SqliteAssetStore(
				connection, streamManager.GetStream, alwaysOpen: false);
			tempStore.Initialize();
			store = tempStore;
		}

		timer.Restart();

		for (int count = 0; count < 1000; count++)
		{
			store.UpdateAsset(new AssetDocument(Guid.NewGuid(), "Blobbie", MakeBlob(4, 0), MakeBlob(8, 8)));
		}

		timer.Stop();
		Console.WriteLine(timer.ElapsedMilliseconds);
	}

	public static void RunManyAssetStores()
	{
		var timer = new Stopwatch();
		CreateFolders();
		var connection = CreateConnectionBuilder();
		var streamManager = new RecyclableMemoryStreamManager();

		var stores = new List<SqliteAssetStore>();

		timer.Restart();

		for (int i = 0; i < 1000; i++)
		{
			connection.DataSource = @$"D:\Testing2\Result\{Guid.NewGuid()}.db";
			var store = new SqliteAssetStore(connection, streamManager.GetStream, alwaysOpen: false);
			stores.Add(store);
		}

		foreach (var store in stores)
		{
			store.Initialize();
		}

		timer.Stop();
		Console.WriteLine(timer.ElapsedMilliseconds);

		Console.WriteLine("Press Enter to Dispose");
		Console.ReadLine();

		stores.ForEach(x => x.Dispose());
		stores.Clear();

		Console.WriteLine("Press Enter to Exit");
		Console.ReadLine();
	}

	public static void RunConcurrencyTests()
	{
		var timer = new Stopwatch();
		CreateFolders();
		var connection = CreateConnectionBuilder();
		var streamManager = new RecyclableMemoryStreamManager();

		var alwaysOpen = true;
		var multithread = true;
		int blobSize = 204800;

		var key = Guid.NewGuid();
		var bytes = Factory.CreateArray(blobSize, i => (byte)i);

		using var store = new SqliteAssetStore(connection, streamManager.GetStream, alwaysOpen);
		store.Initialize();
		store.UpdateAsset(new AssetDocument(key, "Blobbie", new MemoryStream(bytes), MakeBlob(4, 4)));

		timer.Restart();

		var tasks = new Task[1000];
		for (int i = 0; i < tasks.Length; i++)
		{
			if (multithread)
			{
				tasks[i] = Task.Run(() => ReadStore(store));
			}
			else
			{
				ReadStore(store);
			}
		}

		if (multithread)
		{
			Task.WaitAll(tasks);
		}

		timer.Stop();
		Console.WriteLine(timer.ElapsedMilliseconds);

		void ReadStore(SqliteAssetStore passedStore)
		{
			var localStore = passedStore ??
				new SqliteAssetStore(connection, streamManager.GetStream, alwaysOpen);
			try
			{
				if (passedStore == null && alwaysOpen)
				{
					localStore.Initialize();
				}

				if (localStore.TryGetAsset(key, out var result, true, true))
				{
					var header = result.Header.Data;

					header.Length.Should().Be(blobSize);

					for (int i = 0; i < blobSize; i++)
					{
						header.ReadByte().Should().Be(i);
					}

					header.Dispose();
				}
				else
				{
					throw new Exception("FAILURE");
				}
			}
			finally
			{
				if (passedStore == null)
				{
					localStore.Dispose();
				}
			}
		}
	}

	private static void CreateFolders()
	{
		if (Directory.Exists(@"D:\Testing2\Result\"))
		{
			Directory.Delete(@"D:\Testing2\Result\", true);
		}

		Directory.CreateDirectory(@"D:\Testing2\Result\");
	}

	private static SqliteConnectionStringBuilder CreateConnectionBuilder() =>
		new SqliteConnectionStringBuilder()
		{
			DataSource = @$"D:\Testing2\Result\test.db",
			Pooling = false,
			Cache = SqliteCacheMode.Shared,
			ForeignKeys = true,
			Mode = SqliteOpenMode.ReadWriteCreate,
		};

	private static Stream MakeBlob(int length, int start) =>
		new MemoryStream(Factory.CreateArray(length, i => (byte)(start + i)));
}
