﻿namespace HQS.Test.ConsoleApp;

public static class TestFlagsExtensions
{
	[MethodImpl(MethodImplOptions.NoInlining)]
	public static bool HasNonInlined(this TestFlags current, TestFlags flag) => (current & flag) == flag;

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static bool HasAnyNonInlined(this TestFlags current, TestFlags flags) => (current & flags) != 0;

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static TestFlags AddNonInlined(this TestFlags current, TestFlags flags) => current | flags;

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static TestFlags RemoveNonInlined(this TestFlags current, TestFlags flags) => current & ~flags;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool HasInlined(this TestFlags current, TestFlags flag) => (current & flag) == flag;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool HasAnyInlined(this TestFlags current, TestFlags flags) => (current & flags) != 0;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static TestFlags AddInlined(this TestFlags current, TestFlags flags) => current | flags;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static TestFlags RemoveInlined(this TestFlags current, TestFlags flags) => current & ~flags;
}
