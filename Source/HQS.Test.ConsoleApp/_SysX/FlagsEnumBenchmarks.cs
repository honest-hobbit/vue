﻿using SysX.Enums;

namespace HQS.Test.ConsoleApp;

[SuppressMessage("Performance", "CA1822", Justification = "Benchmarks can't be static")]
public class FlagsEnumBenchmarks
{
	private static readonly int Repetitions = 10000;

	public static void RunBenchmarks() => BenchmarkRunner.Run<FlagsEnumBenchmarks>();

	[GlobalSetup]
	public void GlobalSetup()
	{
		// doing this should ensure SysX.FlagsEnum has already cached delegates
		////var subject = TestFlags.None;
		var subject = default(TestFlags);
		subject = subject.Add(TestFlags.A);
		subject = subject.Add(TestFlags.B);
		subject = subject.Remove(TestFlags.A);

		if (subject.Has(TestFlags.A))
		{
			throw new Exception("Failed. This shouldn't happen.");
		}

		if (!subject.Has(TestFlags.B))
		{
			throw new Exception("Failed. This shouldn't happen.");
		}

		if (!subject.HasAny(TestFlags.A.Add(TestFlags.B)))
		{
			throw new Exception("Failed. This shouldn't happen.");
		}
	}

	[MethodImpl(MethodImplOptions.NoOptimization)]
	[Benchmark]
	public void ManualCodeNotOptimized()
	{
		for (int i = 0; i < Repetitions; i++)
		{
			////var subject = TestFlags.None;
			var subject = default(TestFlags);
			subject |= TestFlags.A;
			subject |= TestFlags.B;
			subject &= ~TestFlags.A;

			if ((subject & TestFlags.A) == TestFlags.A)
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if ((subject & TestFlags.B) != TestFlags.B)
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if ((subject & (TestFlags.A | TestFlags.B)) == 0)
			{
				throw new Exception("Failed. This shouldn't happen.");
			}
		}
	}

	[Benchmark]
	public void ManualCodeOptimized()
	{
		for (int i = 0; i < Repetitions; i++)
		{
			////var subject = TestFlags.None;
			var subject = default(TestFlags);
			subject |= TestFlags.A;
			subject |= TestFlags.B;
			subject &= ~TestFlags.A;

			if ((subject & TestFlags.A) == TestFlags.A)
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if ((subject & TestFlags.B) != TestFlags.B)
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if ((subject & (TestFlags.A | TestFlags.B)) == 0)
			{
				throw new Exception("Failed. This shouldn't happen.");
			}
		}
	}

	[Benchmark]
	public void NonInlinedMethod()
	{
		for (int i = 0; i < Repetitions; i++)
		{
			////var subject = TestFlags.None;
			var subject = default(TestFlags);
			subject = subject.AddNonInlined(TestFlags.A);
			subject = subject.AddNonInlined(TestFlags.B);
			subject = subject.RemoveNonInlined(TestFlags.A);

			if (subject.HasNonInlined(TestFlags.A))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if (!subject.HasNonInlined(TestFlags.B))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if (!subject.HasAnyNonInlined(TestFlags.A.Add(TestFlags.B)))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}
		}
	}

	[Benchmark]
	public void InlinedMethod()
	{
		for (int i = 0; i < Repetitions; i++)
		{
			////var subject = TestFlags.None;
			var subject = default(TestFlags);
			subject = subject.AddInlined(TestFlags.A);
			subject = subject.AddInlined(TestFlags.B);
			subject = subject.RemoveInlined(TestFlags.A);

			if (subject.HasInlined(TestFlags.A))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if (!subject.HasInlined(TestFlags.B))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if (!subject.HasAnyInlined(TestFlags.A.Add(TestFlags.B)))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}
		}
	}

	[Benchmark]
	public void FlagsEnumMethod()
	{
		for (int i = 0; i < Repetitions; i++)
		{
			////var subject = TestFlags.None;
			var subject = default(TestFlags);
			subject = subject.Add(TestFlags.A);
			subject = subject.Add(TestFlags.B);
			subject = subject.Remove(TestFlags.A);

			if (subject.Has(TestFlags.A))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if (!subject.Has(TestFlags.B))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}

			if (!subject.HasAny(TestFlags.A.Add(TestFlags.B)))
			{
				throw new Exception("Failed. This shouldn't happen.");
			}
		}
	}
}
