﻿namespace HQS.Test.ConsoleApp;

public class LockingBenchmarks
{
	private const int RepeatStack = 100000;

	private const int RepeatCount = 100000;

	private readonly AtomicInt atomicCount = new AtomicInt(0);

	private readonly LocklessCount locklessCount = new LocklessCount();

	private readonly LockedCount lockedCount = new LockedCount();

	private readonly LocklessStack locklessStack = new LocklessStack();

	private readonly LockedStack lockedStack = new LockedStack();

	public static void RunBenchmarks() => BenchmarkRunner.Run<LockingBenchmarks>();

	[Benchmark]
	public void CountAtomic()
	{
		var subject = this.atomicCount;

		for (int count = 0; count < RepeatStack; count++)
		{
			if (subject.Increment() == count)
			{
				throw new Exception("They should be different!");
			}
		}

		subject.Write(0);
	}

	[Benchmark]
	public void CountWithoutLock()
	{
		var subject = this.locklessCount;

		for (int count = 0; count < RepeatStack; count++)
		{
			if (subject.Increment() == count)
			{
				throw new Exception("They should be different!");
			}
		}

		subject.Reset();
	}

	[Benchmark]
	public void CountWithLock()
	{
		var subject = this.lockedCount;

		for (int count = 0; count < RepeatStack; count++)
		{
			if (subject.Increment() == count)
			{
				throw new Exception("They should be different!");
			}
		}

		subject.Reset();
	}

	[Benchmark]
	public void StackWithoutLock()
	{
		var subject = this.locklessStack;

		for (int count = 0; count < RepeatStack; count++)
		{
			var a = subject.Pop();
			var b = subject.Pop();
			if (a == b)
			{
				throw new Exception("They should be different!");
			}

			subject.Push(a);
			subject.Push(b);
		}
	}

	[Benchmark]
	public void StackWithLock()
	{
		var subject = this.lockedStack;

		for (int count = 0; count < RepeatStack; count++)
		{
			var a = subject.Pop();
			var b = subject.Pop();
			if (a == b)
			{
				throw new Exception("They should be different!");
			}

			subject.Push(a);
			subject.Push(b);
		}
	}

	private class LocklessCount
	{
		private int count = 0;

		public void Reset() => this.count = 0;

		public int Increment()
		{
			this.count++;
			return this.count;
		}
	}

	private class LockedCount
	{
		private readonly object padlock = new object();

		private int count = 0;

		public void Reset()
		{
			lock (this.padlock)
			{
				this.count = 0;
			}
		}

		public int Increment()
		{
			lock (this.padlock)
			{
				this.count++;
				return this.count;
			}
		}
	}

	private class LocklessStack
	{
		private Stack<object> stack = new Stack<object>(2);

		public LocklessStack()
		{
			this.stack.Push(new object());
			this.stack.Push(new object());
		}

		public void Push(object value)
		{
			this.stack.Push(value);
		}

		public object Pop()
		{
			return this.stack.Pop();
		}
	}

	private class LockedStack
	{
		private readonly object padlock = new object();

		private Stack<object> stack = new Stack<object>(2);

		public LockedStack()
		{
			this.stack.Push(new object());
			this.stack.Push(new object());
		}

		public void Push(object value)
		{
			lock (this.padlock)
			{
				this.stack.Push(value);
			}
		}

		public object Pop()
		{
			lock (this.padlock)
			{
				return this.stack.Pop();
			}
		}
	}
}
