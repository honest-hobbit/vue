﻿namespace HQS.Test.ConsoleApp;

public class NullableBenchmark
{
	private static readonly int Repeat = 100000;

	private int?[] values;

	private long expectedSum;

	private int defaultValue;

	public static void RunBenchmarks() => BenchmarkRunner.Run<NullableBenchmark>();

	[GlobalSetup]
	public void GlobalSetup()
	{
		this.values = new int?[100];

		for (int i = 0; i < this.values.Length; i += 2)
		{
			this.expectedSum += i;
			this.values[i] = i;
			this.values[i + 1] = null;
		}

		this.expectedSum *= Repeat;
		this.defaultValue = 0;
	}

	[Benchmark]
	public void HasValue()
	{
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			for (int i = 0; i < this.values.Length; i++)
			{
				if (this.values[i].HasValue)
				{
					sum += this.values[i].Value;
				}
			}
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void HasValueWithElse()
	{
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			for (int i = 0; i < this.values.Length; i++)
			{
				sum += this.values[i].HasValue ? this.values[i].Value : this.defaultValue;
			}
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void HasValueGetValueWithElse()
	{
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			for (int i = 0; i < this.values.Length; i++)
			{
				sum += this.values[i].HasValue ?
					this.values[i].GetValueOrDefault() : this.defaultValue;
			}
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void HasValueGetValueOrDefault()
	{
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			for (int i = 0; i < this.values.Length; i++)
			{
				if (this.values[i].HasValue)
				{
					sum += this.values[i].GetValueOrDefault();
				}
			}
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void GetValueOrDefault()
	{
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			for (int i = 0; i < this.values.Length; i++)
			{
				sum += this.values[i].GetValueOrDefault();
			}
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void GetValueOrDefaultWithConstant()
	{
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			for (int i = 0; i < this.values.Length; i++)
			{
				sum += this.values[i].GetValueOrDefault(0);
			}
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}
}
