﻿namespace HQS.Test.ConsoleApp;

public class PropertyCachingBenchmarks
{
	private const int Size = 10000000;

	private readonly int[] arraySubject = new int[Size];

	private readonly List<int> listSubject = new List<int>(Size);

	private readonly long expectedSum;

	public PropertyCachingBenchmarks()
	{
		for (int i = 0; i < Size; i++)
		{
			this.arraySubject[i] = i;
			this.listSubject.Add(i);
			this.expectedSum += i;
		}
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<PropertyCachingBenchmarks>();

	[Benchmark]
	public void ArrayLength()
	{
		long sum = 0;
		for (int i = 0; i < this.arraySubject.Length; i++)
		{
			sum += this.arraySubject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ArrayLengthCached()
	{
		long sum = 0;
		var length = this.arraySubject.Length;
		for (int i = 0; i < length; i++)
		{
			sum += this.arraySubject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ListCount()
	{
		long sum = 0;
		for (int i = 0; i < this.listSubject.Count; i++)
		{
			sum += this.listSubject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ListCountCached()
	{
		long sum = 0;
		var count = this.listSubject.Count;
		for (int i = 0; i < count; i++)
		{
			sum += this.listSubject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}
}
