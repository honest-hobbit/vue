﻿namespace HQS.Test.ConsoleApp;

[MemoryDiagnoser]
public class MethodCallOverheadBenchmarks
{
	private const int Repeat = 10000000;

	private readonly long expectedSum;

	public MethodCallOverheadBenchmarks()
	{
		int max = Repeat + 1;
		for (int count = 1; count < max; count++)
		{
			this.expectedSum += count;
		}
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<MethodCallOverheadBenchmarks>();

	public static Func<int> CreateDelegate()
	{
		int number = 0;
		return () =>
		{
			number++;
			return number;
		};
	}

	public static IMethodInterface CreateClassInterfaceSubject(string input)
	{
		// input and check here is purely to force the compiler
		// to treat this object as returned by its interface
		if (input.IsNullOrWhiteSpace())
		{
			return null;
		}
		else
		{
			return new ClassInterface();
		}
	}

	public static IMethodInterface CreateStructInterfaceSubject(string input)
	{
		// input and check here is purely to force the compiler
		// to treat this object as returned by its interface
		if (input.IsNullOrWhiteSpace())
		{
			return null;
		}
		else
		{
			return default(StructInterface);
		}
	}

	public static T CreateGenericSubject<T>(string input)
		where T : IMethodInterface, new()
	{
		// input and check here is purely to force the compiler
		// to treat this object as returned by its generic
		if (input.IsNullOrWhiteSpace())
		{
			return default;
		}
		else
		{
			return new T();
		}
	}

	[Benchmark]
	public void ByInlinedCode()
	{
		int number = 0;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			number++;
			sum += number;
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByDelegate()
	{
		var subject = CreateDelegate();
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByClass()
	{
		var subject = new ClassMethod();
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByClassDelegate()
	{
		var source = new ClassMethod();
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByClassInterface()
	{
		IMethodInterface subject = CreateClassInterfaceSubject("make subject");
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByClassInterfaceDelegate()
	{
		IMethodInterface source = CreateClassInterfaceSubject("make subject");
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByConcreteClassInterface()
	{
		var subject = new ClassInterface();
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByConcreteClassInterfaceDelegate()
	{
		var source = new ClassInterface();
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByGenericClassInterface()
	{
		var subject = CreateGenericSubject<ClassInterface>("make subject");
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByGenericClassInterfaceDelegate()
	{
		var source = CreateGenericSubject<ClassInterface>("make subject");
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByStruct()
	{
		var subject = default(StructMethod);
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByStructDelegate()
	{
		var source = default(StructMethod);
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByStructInterface()
	{
		IMethodInterface subject = CreateStructInterfaceSubject("make subject");
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByStructInterfaceDelegate()
	{
		IMethodInterface source = CreateStructInterfaceSubject("make subject");
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByConcreteStructInterface()
	{
		var subject = default(StructInterface);
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByConcreteStructInterfaceDelegate()
	{
		var source = default(StructInterface);
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByGenericStructInterface()
	{
		var subject = CreateGenericSubject<StructInterface>("make subject");
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject.GetNumber();
		}

		this.ValidateSum(sum);
	}

	[Benchmark]
	public void ByGenericStructInterfaceDelegate()
	{
		var source = CreateGenericSubject<StructInterface>("make subject");
		var subject = source.GetNumber;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += subject();
		}

		this.ValidateSum(sum);
	}

	private void ValidateSum(long sum)
	{
		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sum is wrong!", nameof(sum));
		}
	}

	public class ClassMethod
	{
		private int number;

		public int GetNumber()
		{
			this.number++;
			return this.number;
		}
	}

	public class ClassInterface : IMethodInterface
	{
		private int number;

		public int GetNumber()
		{
			this.number++;
			return this.number;
		}
	}

	public struct StructMethod
	{
		private int number;

		public int GetNumber()
		{
			this.number++;
			return this.number;
		}
	}

	public struct StructInterface : IMethodInterface
	{
		private int number;

		public int GetNumber()
		{
			this.number++;
			return this.number;
		}
	}

	public interface IMethodInterface
	{
		int GetNumber();
	}
}
