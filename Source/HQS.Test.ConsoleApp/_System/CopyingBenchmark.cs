﻿namespace HQS.Test.ConsoleApp;

public class CopyingBenchmark
{
	private const int Length = 10000;

	private readonly int[] sourceArray = new int[Length];

	private readonly int[] destinationArray = new int[Length];

	private readonly FastList<int> sourceList = new FastList<int>(Length);

	private readonly FastList<int> destinationList = new FastList<int>(Length);

	public CopyingBenchmark()
	{
		for (int i = 0; i < Length; i++)
		{
			this.sourceArray[i] = i;
			this.sourceList.Add(i);
		}
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<CopyingBenchmark>();

	[Benchmark]
	public void ArrayCopy()
	{
		Array.Copy(this.sourceArray, this.destinationArray, Length);
	}

	[Benchmark]
	public void ArrayForLoop()
	{
		for (int i = 0; i < Length; i++)
		{
			this.destinationArray[i] = this.sourceArray[i];
		}
	}

	[Benchmark]
	public void FastList()
	{
		this.destinationList.FastClear();

		foreach (var value in this.sourceList)
		{
			this.destinationList.Add(value);
		}
	}

	[Benchmark]
	public void ArrayCopySegmented10() => this.ArrayCopySegmented(10);

	[Benchmark]
	public void ArrayCopySegmented20() => this.ArrayCopySegmented(20);

	[Benchmark]
	public void ArrayCopySegmented100() => this.ArrayCopySegmented(100);

	[Benchmark]
	public void ArrayCopySegmented1000() => this.ArrayCopySegmented(10000);

	[Benchmark]
	public void SpanCopySegmented10() => this.SpanCopySegmented(10);

	[Benchmark]
	public void SpanCopySegmented20() => this.SpanCopySegmented(20);

	[Benchmark]
	public void SpanCopySegmented100() => this.SpanCopySegmented(100);

	[Benchmark]
	public void SpanCopySegmented1000() => this.SpanCopySegmented(10000);

	public void ArrayCopySegmented(int segmentLength)
	{
		for (int i = 0; i < Length; i += segmentLength)
		{
			Array.Copy(this.sourceArray, i, this.destinationArray, i, segmentLength);
		}
	}

	public void SpanCopySegmented(int segmentLength)
	{
		for (int i = 0; i < Length; i += segmentLength)
		{
			var source = new ReadOnlySpan<int>(this.sourceArray, 1, segmentLength);
			var destination = new Span<int>(this.destinationArray, 1, segmentLength);
			source.CopyTo(destination);
		}
	}
}
