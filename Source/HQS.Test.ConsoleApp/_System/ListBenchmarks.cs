﻿namespace HQS.Test.ConsoleApp;

public class ListBenchmarks
{
	private static readonly int Length = 100000;

	private readonly List<int> list;

	private readonly FastList<int> fastList;

	private readonly int[] array;

	private readonly long expectedSum;

	public ListBenchmarks()
	{
		this.list = new List<int>(Length);
		this.fastList = new FastList<int>(Length);
		this.array = new int[Length];

		for (int i = 0; i < Length; i++)
		{
			this.list.Add(i);
			this.fastList.Add(i);
			this.array[i] = i;
			this.expectedSum += i;
		}
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<ListBenchmarks>();

	[Benchmark]
	public void List()
	{
		var subject = this.list;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ListAsIList()
	{
		var subject = (IList<int>)this.list;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ListAsIReadOnlyList()
	{
		var subject = (IReadOnlyList<int>)this.list;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void FastList()
	{
		var subject = this.fastList;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void FastListAsIReadOnlyList()
	{
		var subject = (IReadOnlyList<int>)this.fastList;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void FastListArrayProperty()
	{
		var subject = this.fastList.Array;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void FastListArrayPropertyAsReadOnlyArray()
	{
		var subject = new ReadOnlyArray<int>(this.fastList.Array);
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void FastListArrayPropertyAsSpan()
	{
		var subject = new Span<int>(this.fastList.Array, 0, Length);
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void FastListArrayPropertyAsReadOnlySpan()
	{
		var subject = new ReadOnlySpan<int>(this.fastList.Array, 0, Length);
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void Array()
	{
		var subject = this.array;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ArrayAsIList()
	{
		var subject = (IList<int>)this.array;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ArrayAsIReadOnlyList()
	{
		var subject = (IReadOnlyList<int>)this.array;
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ArrayAsReadOnlyArray()
	{
		var subject = new ReadOnlyArray<int>(this.array);
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ArrayAsSpan()
	{
		var subject = new Span<int>(this.array, 0, Length);
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void ArrayAsReadOnlySpan()
	{
		var subject = new ReadOnlySpan<int>(this.array, 0, Length);
		long sum = 0;

		for (int i = 0; i < Length; i++)
		{
			sum += subject[i];
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}
}
