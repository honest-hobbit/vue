﻿namespace HQS.Test.ConsoleApp;

public class MathBenchmarks
{
	private static readonly int Repeat = 1000000;

	private long expectedSum;

	public static void RunBenchmarks() => BenchmarkRunner.Run<MathBenchmarks>();

	[Params(2, 3)]
	public int Amount { get; set; }

	[GlobalSetup]
	public void GlobalSetup()
	{
		int amount = (int)Math.Pow(2, this.Amount);
		this.expectedSum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			this.expectedSum += count * amount;
		}
	}

	[Benchmark]
	public void Multiply()
	{
		int amount = (int)Math.Pow(2, this.Amount);
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += count * amount;
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void BitShift()
	{
		int amount = this.Amount;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += count << amount;
		}

		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sums don't match!", nameof(sum));
		}
	}

	[Benchmark]
	public void DoubleBitShift()
	{
		int amount = this.Amount + 1;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			sum += (count << amount) - (count << 1);
		}

		////if (sum != this.expectedSum)
		////{
		////	throw new ArgumentException("Sums don't match!", nameof(sum));
		////}
	}
}
