﻿using Ardalis.GuardClauses;
using Dawn;

namespace HQS.Test.ConsoleApp;

[MemoryDiagnoser]
public class GuardBenchmarks
{
	private const int Repeat = 10000000;

	private readonly long expectedSum;

	public GuardBenchmarks()
	{
		int max = Repeat + 1;
		for (int count = 1; count < max; count++)
		{
			this.expectedSum += count;
		}

		int value = 0;
		default(InlinedMethod).GetNumber(value);
		default(UnguardedMethod).GetNumber(value);
		default(DebugAssertMethod).GetNumber(value);
		default(IfMethod).GetNumber(value);
		default(EnsureThatComparableMethod).GetNumber(value);
		default(EnsureThatArgMethod).GetNumber(value);
		default(EnsureThatFluentMethod).GetNumber(value);
		default(DawnGuardMethod).GetNumber(value);
		////default(DawnGuardFancyMethod).GetNumber(value);
		default(ArdalisGuardMethod).GetNumber(value);
	}

	public static void RunBenchmarks() => BenchmarkRunner.Run<GuardBenchmarks>();

	[Benchmark]
	public void Inlined() => this.TestSubject(default(InlinedMethod));

	[Benchmark]
	public void Unguarded() => this.TestSubject(default(UnguardedMethod));

	[Benchmark]
	public void DebugAssert() => this.TestSubject(default(DebugAssertMethod));

	[Benchmark]
	public void If() => this.TestSubject(default(IfMethod));

	[Benchmark]
	public void EnsureThatComparable() => this.TestSubject(default(EnsureThatComparableMethod));

	[Benchmark]
	public void EnsureThatArg() => this.TestSubject(default(EnsureThatArgMethod));

	[Benchmark]
	public void EnsureThatFluent() => this.TestSubject(default(EnsureThatFluentMethod));

	[Benchmark]
	public void DawnGuard() => this.TestSubject(default(DawnGuardMethod));

	////[Benchmark]
	////public void DawnGuardFancy() => this.TestSubject(default(DawnGuardFancyMethod));

	[Benchmark]
	public void ArdalisGuard() => this.TestSubject(default(ArdalisGuardMethod));

	private void TestSubject<T>(T subject)
		where T : IGetNumber
	{
		int number = 0;
		long sum = 0;

		for (int count = 0; count < Repeat; count++)
		{
			number = subject.GetNumber(number);
			sum += number;
		}

		this.ValidateSum(sum);
	}

	private void ValidateSum(long sum)
	{
		if (sum != this.expectedSum)
		{
			throw new ArgumentException("Sum is wrong!", nameof(sum));
		}
	}

	private interface IGetNumber
	{
		int GetNumber(int value);
	}

	private readonly struct InlinedMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int GetNumber(int value)
		{
			return value + 1;
		}
	}

	private readonly struct UnguardedMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			return value + 1;
		}
	}

	private readonly struct DebugAssertMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			Debug.Assert(value >= 0);

			return value + 1;
		}
	}

	private readonly struct IfMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			if (value < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(value));
			}

			return value + 1;
		}
	}

	private readonly struct EnsureThatComparableMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			Ensure.Comparable.IsGte(value, 0, nameof(value));

			return value + 1;
		}
	}

	private readonly struct EnsureThatArgMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			EnsureArg.IsGte(value, 0, nameof(value));

			return value + 1;
		}
	}

	private readonly struct EnsureThatFluentMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			Ensure.That(value, nameof(value)).IsGte(0);

			return value + 1;
		}
	}

	private readonly struct ArdalisGuardMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			Ardalis.GuardClauses.Guard.Against.Negative(value, nameof(value));

			return value + 1;
		}
	}

	private readonly struct DawnGuardMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			Dawn.Guard.Argument(value, nameof(value)).NotNegative();

			return value + 1;
		}
	}

	private readonly struct DawnGuardFancyMethod : IGetNumber
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public int GetNumber(int value)
		{
			Dawn.Guard.Argument(() => value).NotNegative();

			return value + 1;
		}
	}
}
