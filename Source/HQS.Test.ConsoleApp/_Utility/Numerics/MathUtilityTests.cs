﻿namespace HQS.Test.ConsoleApp;

public static class MathUtilityTests
{
	public static void PrintRangeOfActualModulo()
	{
		int range = 7;
		int mod = -3;

		Console.WriteLine("num \t% \tActual");

		for (int i = -range; i <= range; i++)
		{
			Console.WriteLine($"{i} \t{i % mod} \t{MathUtility.ActualModulo(i, mod)}");
		}

		Console.ReadLine();
	}
}
