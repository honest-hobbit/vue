﻿namespace HQS.Test.ConsoleApp;

public class ArrayBenchmarks
{
	private readonly Subjects smallSubjects = new Subjects(3, 2);

	private readonly Subjects largeSubjects = new Subjects(7, 5);

	public static void RunBenchmarks() => BenchmarkRunner.Run<ArrayBenchmarks>();

	[Benchmark]
	public void SmallArray2D() => this.smallSubjects.TestArray2D();

	[Benchmark]
	public void SmallArray2DAs1D() => this.smallSubjects.TestArray2DAs1D();

	[Benchmark]
	public void SmallSquareArray() => this.smallSubjects.TestSquareArray();

	[Benchmark]
	public void SmallArray3D() => this.smallSubjects.TestArray3D();

	[Benchmark]
	public void SmallArray3DAs1D() => this.smallSubjects.TestArray3DAs1D();

	[Benchmark]
	public void SmallCubeArray() => this.smallSubjects.TestCubeArray();

	[Benchmark]
	public void LargeArray2D() => this.largeSubjects.TestArray2D();

	[Benchmark]
	public void LargeArray2DAs1D() => this.largeSubjects.TestArray2DAs1D();

	[Benchmark]
	public void LargeSquareArray() => this.largeSubjects.TestSquareArray();

	[Benchmark]
	public void LargeArray3D() => this.largeSubjects.TestArray3D();

	[Benchmark]
	public void LargeArray3DAs1D() => this.largeSubjects.TestArray3DAs1D();

	[Benchmark]
	public void LargeCubeArray() => this.largeSubjects.TestCubeArray();

	private class Subjects
	{
		private readonly SquareArrayIndexer indexer2D;

		private readonly int[,] array2D;

		private readonly int[] array2DAs1D;

		private readonly SquareArray<int> squareArray;

		private readonly long sum2D;

		private readonly CubeArrayIndexer indexer3D;

		private readonly int[,,] array3D;

		private readonly int[] array3DAs1D;

		private readonly CubeArray<int> cubeArray;

		private readonly long sum3D;

		public Subjects(int exponent2D, int exponent3D)
		{
			this.indexer2D = new SquareArrayIndexer(exponent2D);
			this.array2D = new int[this.indexer2D.SideLength, this.indexer2D.SideLength];
			this.array2DAs1D = new int[this.indexer2D.Length];
			this.squareArray = new SquareArray<int>(this.indexer2D);

			this.indexer3D = new CubeArrayIndexer(exponent3D);
			this.array3D = new int[this.indexer3D.SideLength, this.indexer3D.SideLength, this.indexer3D.SideLength];
			this.array3DAs1D = new int[this.indexer3D.Length];
			this.cubeArray = new CubeArray<int>(this.indexer3D);

			int count = 0;
			int length = this.indexer2D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					count++;
					this.sum2D += count;
					this.array2D[iX, iY] = count;
					this.array2DAs1D[this.indexer2D[iX, iY]] = count;
					this.squareArray[iX, iY] = count;
				}
			}

			count = 0;
			length = this.indexer3D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					for (int iZ = 0; iZ < length; iZ++)
					{
						count++;
						this.sum3D += count;
						this.array3D[iX, iY, iZ] = count;
						this.array3DAs1D[this.indexer3D[iX, iY, iZ]] = count;
						this.cubeArray[iX, iY, iZ] = count;
					}
				}
			}
		}

		public void TestArray2D()
		{
			long sum = 0;
			int length = this.indexer2D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					sum += this.array2D[iX, iY];
				}
			}

			if (sum != this.sum2D)
			{
				throw new ArgumentException("Sums don't match!", nameof(sum));
			}
		}

		public void TestArray2DAs1D()
		{
			long sum = 0;
			int length = this.indexer2D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					sum += this.array2DAs1D[this.indexer2D[iX, iY]];
				}
			}

			if (sum != this.sum2D)
			{
				throw new ArgumentException("Sums don't match!", nameof(sum));
			}
		}

		public void TestSquareArray()
		{
			long sum = 0;
			int length = this.indexer2D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					sum += this.squareArray[iX, iY];
				}
			}

			if (sum != this.sum2D)
			{
				throw new ArgumentException("Sums don't match!", nameof(sum));
			}
		}

		public void TestArray3D()
		{
			long sum = 0;
			int length = this.indexer3D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					for (int iZ = 0; iZ < length; iZ++)
					{
						sum += this.array3D[iX, iY, iZ];
					}
				}
			}

			if (sum != this.sum3D)
			{
				throw new ArgumentException("Sums don't match!", nameof(sum));
			}
		}

		public void TestArray3DAs1D()
		{
			long sum = 0;
			int length = this.indexer3D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					for (int iZ = 0; iZ < length; iZ++)
					{
						sum += this.array3DAs1D[this.indexer3D[iX, iY, iZ]];
					}
				}
			}

			if (sum != this.sum3D)
			{
				throw new ArgumentException("Sums don't match!", nameof(sum));
			}
		}

		public void TestCubeArray()
		{
			long sum = 0;
			int length = this.indexer3D.SideLength;
			for (int iX = 0; iX < length; iX++)
			{
				for (int iY = 0; iY < length; iY++)
				{
					for (int iZ = 0; iZ < length; iZ++)
					{
						sum += this.cubeArray[iX, iY, iZ];
					}
				}
			}

			if (sum != this.sum3D)
			{
				throw new ArgumentException("Sums don't match!", nameof(sum));
			}
		}
	}
}
