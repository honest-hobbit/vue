﻿namespace HQS.Test.ConsoleApp;

public static class BitFieldTests
{
	public static void WriteLineIterations()
	{
		PrintLines(() => new Bit8(0));
		PrintLines(() => new Bit16(0));
		PrintLines(() => new Bit32(0));
		PrintLines(() => new Bit64(0));
	}

	private static void PrintLines<T>(Func<T> getValue)
		where T : struct, IBitValue
	{
		int length = getValue().Count;
		for (int i = 0; i < length; i++)
		{
			var subject = getValue();
			subject[i] = true;
			Console.WriteLine($"{i,4} : {subject}");
		}

		Console.WriteLine();
		Console.WriteLine();
		Console.WriteLine();
	}
}
