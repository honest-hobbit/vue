﻿namespace HQS.Test.ConsoleApp;

public class MathUtilityBenchmarks
{
	private const int Values = 100000;

	private const int Divisors = 1000;

	public static void RunBenchmarks() => BenchmarkRunner.Run<MathUtilityBenchmarks>();

	[Benchmark]
	public void Operators()
	{
		for (int divisor = 1; divisor < Divisors; divisor++)
		{
			for (int value = 0; value < Values; value++)
			{
				var result = value / divisor;
				var remainder = value % divisor;
			}
		}
	}

	[Benchmark]
	public void MathDivRem()
	{
		for (int divisor = 1; divisor < Divisors; divisor++)
		{
			for (int value = 0; value < Values; value++)
			{
				var result = Math.DivRem(value, divisor, out var remainder);
			}
		}
	}

	[Benchmark]
	public void MathDivRemTuple()
	{
		for (int divisor = 1; divisor < Divisors; divisor++)
		{
			for (int value = 0; value < Values; value++)
			{
				var result = Math.DivRem(value, divisor);
			}
		}
	}
}
