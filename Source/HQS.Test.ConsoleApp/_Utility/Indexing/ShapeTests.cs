﻿namespace HQS.Test.ConsoleApp;

public static class ShapeTests
{
	public static void PrintShapes()
	{
		while (true)
		{
			Console.Write("Enter integer diameter: ");
			var input = Console.ReadLine();
			Console.WriteLine();

			if (!int.TryParse(input, out int diameter))
			{
				return;
			}

			if (diameter <= 0)
			{
				return;
			}

			Console.Clear();
			var padding = new Index2D(1);

			////var square = IndexSquare.Create(diameter);

			////Console.WriteLine("Square Length: " + square.Length);
			////Console.WriteLine("Square Count: " + square.Count);
			////Console.WriteLine();

			////Console.WriteLine("Square Contains");
			////Console.Write(square.ToContainsMask(padding).ToGridString(value => value ? "X" : "."));
			////Console.WriteLine();

			////Console.WriteLine("Square Counted");
			////Console.Write(square.ToCountedMask(padding).ToGridString(value => value != 0 ? value.ToString() : "."));
			////Console.WriteLine();

			////Console.WriteLine("Square Ordered");
			////Console.Write(square.ToOrderedMask(padding).ToGridString(
			////	value => (value == 0 ? "." : value.ToString()).PadLeft(MathUtilities.CountDigits(square.Count))));
			////Console.WriteLine();

			var circle = IndexCircle.Create(diameter);

			Console.WriteLine("Circle Diameter: " + circle.Diameter);
			Console.WriteLine("Circle Count: " + circle.Count);
			Console.WriteLine();

			Console.WriteLine("Circle Contains");
			Console.Write(circle.ToContainsMask(padding).ToGridString(value => value ? "X" : "."));
			Console.WriteLine();

			Console.WriteLine("Circle Counted");
			Console.Write(circle.ToCountedMask(padding).ToGridString(value => value != 0 ? value.ToString() : "."));
			Console.WriteLine();

			Console.WriteLine("Circle Ordered");
			Console.Write(circle.ToOrderedMask(padding).ToGridString(
				value => (value == 0 ? "." : value.ToString()).PadLeft(MathUtility.CountDigits(circle.Count))));
			Console.WriteLine();
		}
	}
}
