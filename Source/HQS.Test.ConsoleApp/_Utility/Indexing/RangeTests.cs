﻿namespace HQS.Test.ConsoleApp;

public static class RangeTests
{
	public static void RangeTest()
	{
		int iterations = 0;
		foreach (var index in new Index3D(0).Range(new Index3D(3)))
		{
			Console.WriteLine($"{index} - {index.ToMortonCode()}");
			iterations++;
		}

		Console.WriteLine("Iterations: " + iterations);
	}
}
