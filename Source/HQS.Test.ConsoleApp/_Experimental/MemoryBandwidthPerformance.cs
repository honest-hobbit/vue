﻿namespace HQS.Test.ConsoleApp;

public static class MemoryBandwidthPerformance
{
	public static void RunSingleThreadedTest()
	{
		Console.WriteLine("Single Threaded");

		uint[] data = new uint[10000000 * 32];

		for (int j = 0; j < 15; j++)
		{
			uint sum = 0;
			var sw = Stopwatch.StartNew();
			for (uint i = 0; i < data.Length; i += 64)
			{
				sum += data[i] + data[i + 16] + data[i + 32] + data[i + 48];
			}

			sw.Stop();
			long dataSize = data.Length * 4;
			Console.WriteLine("{0} {1:0.000} GB/s", sum, dataSize / sw.Elapsed.TotalSeconds / (1024 * 1024 * 1024));
		}
	}

	public static void RunMultiThreadedTest()
	{
		Console.WriteLine("Multi-Threaded");

		int n = 64;
		uint[][] data = new uint[n][];
		for (int k = 0; k < n; k++)
		{
			data[k] = new uint[1000000 * 32];
		}

		for (int j = 0; j < 15; j++)
		{
			long total = 0;
			var sw = Stopwatch.StartNew();
			Parallel.For(0, n, k =>
			{
				uint sum = 0;
				uint[] d = data[k];

				// this version skips many additions and is more likely to be RAM bandwidth bound
				for (uint i = 0; i < d.Length; i += 64)
				{
					sum += d[i] + d[i + 16] + d[i + 32] + d[i + 48];
				}

				// this version accesses every memory slot and is more likely to end up CPU compute bound
				////for (uint i = 0; i < d.Length; i++)
				////{
				////	sum += d[i];
				////}

				Interlocked.Add(ref total, sum);
			});

			sw.Stop();
			long dataSize = (long)data[0].Length * n * 4;
			Console.WriteLine("{0} {1:0.000} GB/s", total, dataSize / sw.Elapsed.TotalSeconds / (1024 * 1024 * 1024));
		}
	}
}
