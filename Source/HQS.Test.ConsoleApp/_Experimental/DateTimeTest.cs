﻿namespace HQS.Test.ConsoleApp;

public static class DateTimeTest
{
	public static void Now()
	{
		int repeat = 100;
		List<long> dates = new List<long>(repeat);

		for (int count = 0; count < repeat; count++)
		{
			dates.Add(DateTime.Now.Ticks);
		}

		for (int count = 0; count < repeat; count++)
		{
			Console.WriteLine(dates[count]);
		}

		_ = Console.ReadLine();
	}

	public static void Timer()
	{
		int repeat = 100;
		var timer = new Stopwatch();
		Console.WriteLine("IsHighResolution: " + Stopwatch.IsHighResolution);

		timer.Start();
		List<long> dates = new List<long>(repeat);

		for (int count = 0; count < repeat; count++)
		{
			dates.Add(timer.ElapsedTicks);
		}

		for (int count = 0; count < repeat; count++)
		{
			Console.WriteLine(dates[count]);
		}

		_ = Console.ReadLine();
	}
}
