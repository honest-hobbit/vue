﻿namespace HQS.Test.ConsoleApp;

public static class LockingTest
{
	public static void TestReaderWriterLock()
	{
		using var padlock = new ReaderWriterLockSlim();
		padlock.EnterUpgradeableReadLock();
		padlock.EnterWriteLock();
		padlock.ExitWriteLock();
		padlock.ExitUpgradeableReadLock();

		Console.WriteLine("Normal use succesful.");

		padlock.EnterUpgradeableReadLock();
		padlock.EnterWriteLock();
		padlock.ExitWriteLock();
		padlock.EnterReadLock();
		padlock.ExitUpgradeableReadLock();
		padlock.ExitReadLock();

		Console.WriteLine("Demoting to reader succesful.");

		Console.WriteLine("Press enter to finish.");
		Console.ReadLine();
	}

	// this demonstrates that while a thread is waiting to enter the commit lock
	// all threads that try to enter the read lock will block
	public static void TestCommitLock()
	{
		int delay = 500;
		using var padlock = new CommitLock();
		var threadB = new Thread(() =>
		{
			padlock.EnterWriteLock();
			Console.WriteLine("Thread B entered write.");
			padlock.EnterCommitLock();
			Console.WriteLine("Thread B entered commit.");
			padlock.ExitCommitLock();
			padlock.ExitWriteLock();
			Console.WriteLine("Thread B finished.");
		});

		var threadC = new Thread(() =>
		{
			padlock.EnterReadLock();
			Console.WriteLine("Thread C entered read.");
			padlock.ExitReadLock();
			Console.WriteLine("Thread C finished.");
		});

		padlock.EnterWriteLock();
		padlock.EnterCommitLock();
		Console.WriteLine("Thread A entered commit.");

		threadB.Start();
		Thread.Sleep(delay);

		padlock.ExitCommitLock();
		padlock.EnterReadLock();
		padlock.ExitWriteLock();
		Console.WriteLine("Thread A downgraded to reader.");

		Thread.Sleep(delay);
		threadC.Start();

		Thread.Sleep(delay);
		padlock.ExitReadLock();

		Console.WriteLine("Thread A finished.");
		threadB.Join();
		threadC.Join();

		Console.WriteLine("Press enter to finish.");
		Console.ReadLine();
	}
}
