﻿namespace HQS.Test.ConsoleApp;

public static class SQLiteExperimenting
{
	public static class FileStore
	{
		private const string Folder = "C:\\SQLiteTesting";

		public static void TestMove()
		{
			using (var store = SQLiteStore.CreateDirectoryAndStore(Path.Combine(Folder, "store.db")))
			{
				store.Initialize<TestEntity>();

				var entity = new TestEntity()
				{
					Text = "Hello world!",
				};

				store.Add(entity);

				var found = store.Get<int, TestEntity>(entity.Key);
				if (found.Text != entity.Text)
				{
					throw new ArgumentException("Strings don't match!");
				}

				Console.WriteLine("Initial store created. Press enter to continue.");
				Console.ReadLine();

				// move to same location
				store.MoveFileTo(Path.Combine(Folder, "store.db"));

				found = store.Get<int, TestEntity>(entity.Key);
				if (found.Text != entity.Text)
				{
					throw new ArgumentException("Strings don't match!");
				}

				Console.WriteLine("Store moved to same location. Press enter to continue.");
				Console.ReadLine();

				// move to new file name
				store.MoveFileTo(Path.Combine(Folder, "newstorename.db"));

				found = store.Get<int, TestEntity>(entity.Key);
				if (found.Text != entity.Text)
				{
					throw new ArgumentException("Strings don't match!");
				}

				Console.WriteLine("Store renamed. Press enter to continue.");
				Console.ReadLine();

				// move to new folder
				var subfolder = Path.Combine(Folder, "Subfolder");
				Directory.CreateDirectory(subfolder);
				store.MoveFileTo(Path.Combine(subfolder, "store.db"));

				found = store.Get<int, TestEntity>(entity.Key);
				if (found.Text != entity.Text)
				{
					throw new ArgumentException("Strings don't match!");
				}
			}

			Console.WriteLine("Store moved to subfolder. Press enter to continue.");
			Console.ReadLine();

			Console.WriteLine("Finished. Press enter to exit.");
			Console.ReadLine();
		}

		public static void TestCopy()
		{
			using (var store = SQLiteStore.CreateDirectoryAndStore(Path.Combine(Folder, "store.db")))
			{
				store.Initialize<TestEntity>();

				var entity = new TestEntity()
				{
					Text = "Hello world!",
				};

				store.Add(entity);

				var found = store.Get<int, TestEntity>(entity.Key);
				if (found.Text != entity.Text)
				{
					throw new ArgumentException("Strings don't match!");
				}

				Console.WriteLine("Initial store created. Press enter to continue.");
				Console.ReadLine();

				// copy to same location
				store.CopyFileTo(Path.Combine(Folder, "store.db"));

				found = store.Get<int, TestEntity>(entity.Key);
				if (found.Text != entity.Text)
				{
					throw new ArgumentException("Strings don't match!");
				}

				Console.WriteLine("Store copied to same location. Press enter to continue.");
				Console.ReadLine();

				// copy to new file name
				var newStorePath = Path.Combine(Folder, "newstorename.db");
				store.CopyFileTo(newStorePath);

				using (var newStore = new SQLiteStore(newStorePath))
				{
					found = newStore.Get<int, TestEntity>(entity.Key);
					if (found.Text != entity.Text)
					{
						throw new ArgumentException("Strings don't match!");
					}
				}

				Console.WriteLine("Store copied to new name. Press enter to continue.");
				Console.ReadLine();

				// move to new folder
				var subfolder = Path.Combine(Folder, "Subfolder");
				Directory.CreateDirectory(subfolder);
				newStorePath = Path.Combine(subfolder, "store.db");
				store.CopyFileTo(newStorePath);

				using (var newStore = new SQLiteStore(newStorePath))
				{
					found = newStore.Get<int, TestEntity>(entity.Key);
					if (found.Text != entity.Text)
					{
						throw new ArgumentException("Strings don't match!");
					}
				}
			}

			Console.WriteLine("Store copied to subfolder. Press enter to continue.");
			Console.ReadLine();

			Console.WriteLine("Finished. Press enter to exit.");
			Console.ReadLine();
		}
	}

	public static class Sizes
	{
		public static void TestChunkSizes()
		{
			TestBigChunks();
			Console.WriteLine();
			TestSmallChunks();

			Console.WriteLine();
			Console.WriteLine("Finished. Press enter to exit.");
			Console.ReadLine();
		}

		private static void TestBigChunks()
		{
			Console.WriteLine("Big Chunks");

			long fileSize = StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestChunkEntity>();

				var timer = new Stopwatch();
				timer.Restart();

				PopulateStore(store, 10, 8000);

				timer.Stop();
				Console.WriteLine($"Populating took: {timer.Elapsed.TotalMilliseconds:N0}");

				timer.Restart();

				int count = store.Where((TestChunkEntity chunk) => chunk.X == 3 && chunk.Y == 3 && chunk.Z == 3).Count();
				Console.WriteLine($"Count of: {count:N0}");

				timer.Stop();
				Console.WriteLine($"Where took: {timer.Elapsed.TotalMilliseconds:N0}");
			});

			Console.WriteLine($"File size of: {fileSize:N0}");
		}

		private static void TestSmallChunks()
		{
			Console.WriteLine("Small Chunks");

			long fileSize = StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestChunkEntity>();

				var timer = new Stopwatch();
				timer.Restart();

				PopulateStore(store, 40, 125);

				timer.Stop();
				Console.WriteLine($"Populating took: {timer.Elapsed.TotalMilliseconds:N0}");

				timer.Restart();

				int count = store.Where((TestChunkEntity chunk) =>
					chunk.X >= 3 && chunk.X <= 6 && chunk.Y >= 3 && chunk.Y <= 6 && chunk.Z >= 3 && chunk.Z <= 6).Count();
				Console.WriteLine($"Count of: {count:N0}");

				timer.Stop();
				Console.WriteLine($"Where took: {timer.Elapsed.TotalMilliseconds:N0}");

				timer.Restart();

				count = 0;
				for (int iX = 3; iX <= 6; iX++)
				{
					for (int iY = 3; iY <= 6; iY++)
					{
						for (int iZ = 3; iZ <= 6; iZ++)
						{
							count += store.Where((TestChunkEntity chunk) =>
								chunk.X == iX && chunk.Y == iY && chunk.Z == iZ).Count();
						}
					}
				}

				Console.WriteLine($"Count of: {count:N0}");

				timer.Stop();
				Console.WriteLine($"Many Wheres took: {timer.Elapsed.TotalMilliseconds:N0}");
			});

			Console.WriteLine($"File size of: {fileSize:N0}");
		}

		private static void PopulateStore(IStore store, int chunksWide, int bytesOfData)
		{
			var indices = new List<Index3D>(chunksWide * chunksWide * chunksWide);
			for (int iX = 0; iX < chunksWide; iX++)
			{
				for (int iY = 0; iY < chunksWide; iY++)
				{
					for (int iZ = 0; iZ < chunksWide; iZ++)
					{
						indices.Add(new Index3D(iX, iY, iZ));
					}
				}
			}

			store.AddMany(CreateEntities());

			IEnumerable<TestChunkEntity> CreateEntities()
			{
				var random = new Random(7);
				indices.ShuffleList(random);
				foreach (var index in indices)
				{
					yield return new TestChunkEntity()
					{
						KeyGuid = Guid.NewGuid(),
						Index = index,
						Data = Factory.CreateArray(bytesOfData, () => (byte)random.Next(byte.MaxValue + 1)),
					};
				}
			}
		}

		[Table(nameof(TestChunkEntity))]
		public class TestChunkEntity : IKeyed<byte[]>
		{
			/// <inheritdoc />
			[PrimaryKey]
			public byte[] Key { get; set; }

			[Ignore]
			public Guid KeyGuid
			{
				get => GuidUtility.FromNullableArray(this.Key);
				set => this.Key = value.ToNullableByteArray(this.Key);
			}

			[Ignore]
			public Index3D Index
			{
				get => new Index3D(this.X, this.Y, this.Z);
				set
				{
					this.X = value.X;
					this.Y = value.Y;
					this.Z = value.Z;
				}
			}

			[Indexed]
			public int X { get; set; }

			[Indexed]
			public int Y { get; set; }

			[Indexed]
			public int Z { get; set; }

			public byte[] Data { get; set; }

			public override string ToString() => this.Index.ToString();
		}
	}

	public static class Select
	{
		public static void TestSelect()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity, TestEntitySelect>();

				var entity = new TestEntity()
				{
					Number = 13,
					Text = "Hello world",
				};

				store.Add(entity);
				var retrieved = store.TryGet<int, TestEntity>(entity.Key);
				var retrievedSelect = store.TryGet<int, TestEntitySelect>(entity.Key);

				Console.WriteLine(retrieved);
				Console.WriteLine(retrievedSelect);
			});

			Console.WriteLine("Finished. Press enter to exit.");
			Console.ReadLine();
		}

		public static void TestSelectPerformance()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity, TestEntitySelect>();

				int intValue = 13;
				var entity = new TestEntity()
				{
					Number = intValue,
					Text = StringFactory.CreateRandom(100000, new Random(1)),
				};

				store.Add(entity);

				int repeats = 1000;
				int startCountOn = 10;
				var timer = new Stopwatch();

				for (int count = 0; count < repeats; count++)
				{
					if (count == startCountOn)
					{
						timer.Restart();
					}

					var retrieved = store.TryGet<int, TestEntity>(entity.Key);
					if (retrieved.Value.Number != intValue)
					{
						throw new ArgumentException("Int values don't match!");
					}
				}

				timer.Stop();
				var retrievedTime = timer.ElapsedTicks;

				for (int count = 0; count < repeats; count++)
				{
					if (count == startCountOn)
					{
						timer.Restart();
					}

					var retrievedSelect = store.TryGet<int, TestEntitySelect>(entity.Key);
					if (retrievedSelect.Value.Number != intValue)
					{
						throw new ArgumentException("Int values don't match!");
					}
				}

				timer.Stop();
				var retrievedSelectTime = timer.ElapsedTicks;

				float runs = repeats - startCountOn;
				Console.WriteLine($"Normal Time: {retrievedTime / runs:N0}");
				Console.WriteLine($"Select Time: {retrievedSelectTime / runs:N0}");
			});

			Console.WriteLine("Finished. Press enter to exit.");
			Console.ReadLine();
		}
	}

	public static class Reads
	{
		public static void TestParallelReads()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();

				int countOfEntities = 1000;
				int lengthOfStrings = 1000;
				store = new ConcurrentStore(store);
				var random = new Random(1);
				var entities = Factory.CreateArray(
					countOfEntities, () => new TestEntity() { Text = StringFactory.CreateRandom(lengthOfStrings, random) });
				store.AddMany(entities);

				TimeSingleThread(store, entities);
				TimeSingleThread(store, entities);
				TimeSingleThread(store, entities);
				TimeTasks(store, entities);
				TimeTasks(store, entities);
				TimeTasks(store, entities);
				TimeThreadPool(store, entities);
				TimeThreadPool(store, entities);
				TimeThreadPool(store, entities);
				TimeDataflow(store, entities);
				TimeDataflow(store, entities);
				TimeDataflow(store, entities);
			});

			Console.WriteLine("Finished. Press enter to exit.");
			Console.ReadLine();
		}

		private static void TimeSingleThread(IStore store, TestEntity[] entities)
		{
			Debug.Assert(store != null);
			Debug.Assert(entities != null);

			var timer = new Stopwatch();
			timer.Restart();
			for (int i = 0; i < entities.Length; i++)
			{
				if (store.Get<int, TestEntity>(i + 1).Text != entities[i].Text)
				{
					throw new ArgumentException("String values don't match!");
				}
			}

			timer.Stop();
			Console.WriteLine($"Single thread took: {timer.ElapsedTicks:N0}");
		}

		private static void TimeTasks(IStore store, TestEntity[] entities)
		{
			Debug.Assert(store != null);
			Debug.Assert(entities != null);

			var tasks = new List<Task>(entities.Length);
			var timer = new Stopwatch();
			timer.Restart();

			for (int i = 0; i < entities.Length; i++)
			{
				int index = i;  // seperate variable needed to capture just the current value of 'i'
				tasks.Add(Task.Run(() =>
				{
					if (store.Get<int, TestEntity>(index + 1).Text != entities[index].Text)
					{
						throw new ArgumentException("String values don't match!");
					}
				}));
			}

			Task.WhenAll(tasks).Wait();
			timer.Stop();
			Console.WriteLine($"Tasks took: {timer.ElapsedTicks:N0}");
		}

		private static void TimeThreadPool(IStore store, TestEntity[] entities)
		{
			Debug.Assert(store != null);
			Debug.Assert(entities != null);

			using var countdown = new CountdownEvent(entities.Length);
			var timer = new Stopwatch();
			timer.Restart();

			for (int i = 0; i < entities.Length; i++)
			{
				ThreadPool.QueueUserWorkItem(
					state =>
					{
						int index = (int)state;
						if (store.Get<int, TestEntity>(index + 1).Text != entities[index].Text)
						{
							throw new ArgumentException("String values don't match!");
						}

						countdown.Signal();
					}, i);
			}

			countdown.Wait();
			timer.Stop();
			Console.WriteLine($"ThreadPool took: {timer.ElapsedTicks:N0}");
		}

		private static void TimeDataflow(IStore store, TestEntity[] entities)
		{
			Debug.Assert(store != null);
			Debug.Assert(entities != null);

			using var countdown = new CountdownEvent(entities.Length);
			var block = new ActionBlock<int>(
				i =>
				{
					if (store.Get<int, TestEntity>(i + 1).Text != entities[i].Text)
					{
						throw new ArgumentException("String values don't match!");
					}

					countdown.Signal();
				}, new ExecutionDataflowBlockOptions() { MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded });

			var timer = new Stopwatch();
			timer.Restart();

			for (int i = 0; i < entities.Length; i++)
			{
				block.Post(i);
			}

			countdown.Wait();
			timer.Stop();
			Console.WriteLine($"Dataflow took: {timer.ElapsedTicks:N0}");
			block.Complete();
		}
	}

	[Table(nameof(TestEntity))]
	public class TestEntity : TestEntitySelect
	{
		public string Text { get; set; }

		public override string ToString() => $"Key: {this.Key}, Int: {this.Number}, String: {this.Text}";
	}

	[Table(nameof(TestEntity))]
	public class TestEntitySelect : IKeyed<int>
	{
		[PrimaryKey, AutoIncrement]
		public int Key { get; set; }

		public int Number { get; set; }

		public string GetMetaData() => $"Key: {this.Key}, Int: {this.Number}";

		public override string ToString() => this.GetMetaData();
	}

	////[Table(nameof(TestEntity))]
	////public class TestEntity : IKeyed<int>
	////{
	////	[PrimaryKey, AutoIncrement]
	////	public int Key { get; set; }

	////	public int Int { get; set; }

	////	public string String { get; set; }

	////	public override string ToString() => $"Key: {this.Key}, Int: {this.Int}, String: {this.String}";
	////}

	////[Table(nameof(TestEntity))]
	////public class TestEntitySelect : IKeyed<int>
	////{
	////	[PrimaryKey, AutoIncrement]
	////	public int Key { get; set; }

	////	public int Int { get; set; }

	////	public override string ToString() => $"Key: {this.Key}, Int: {this.Int}";
	////}
}
