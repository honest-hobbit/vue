﻿namespace HQS.Test.ConsoleApp;

public static class MutableStructsTest
{
	public static void RunTests()
	{
		Console.WriteLine("Method local variable");

		var value = default(MutableStruct);
		value.Field = 1;
		value.Property = 2;

		Console.WriteLine("Want 1:   Is: " + value.Field);
		Console.WriteLine("Want 2:   Is: " + value.Property);

		value.Method(3);

		Console.WriteLine("Want 3:   Is: " + value.Field);
		Console.WriteLine("Want 3:   Is: " + value.Property);

		value = new MutableStruct
		{
			Field = 4,
			Property = 5,
		};

		Console.WriteLine("Want 4:   Is: " + value.Field);
		Console.WriteLine("Want 5:   Is: " + value.Property);

		Console.WriteLine("Method local variable by reference");
		ChangeByReference(ref value);

		Console.WriteLine("Array");

		var array = new MutableStruct[1];
		array[0].Field = 1;
		array[0].Property = 2;

		Console.WriteLine("Want 1:   Is: " + array[0].Field);
		Console.WriteLine("Want 2:   Is: " + array[0].Property);

		array[0].Method(3);

		Console.WriteLine("Want 3:   Is: " + array[0].Field);
		Console.WriteLine("Want 3:   Is: " + array[0].Property);

		array[0] = new MutableStruct
		{
			Field = 4,
			Property = 5,
		};

		Console.WriteLine("Want 4:   Is: " + array[0].Field);
		Console.WriteLine("Want 5:   Is: " + array[0].Property);

		Console.WriteLine("Array by reference");
		ChangeByReference(ref array[0]);

		Console.WriteLine("List");

		var list = new List<MutableStruct>(1)
			{
				default,
			};

		list[0] = new MutableStruct
		{
			Field = 1,
			Property = 2,
		};

		Console.WriteLine("Want 1:   Is: " + list[0].Field);
		Console.WriteLine("Want 2:   Is: " + list[0].Property);

		list[0].Method(3);

		Console.WriteLine("Want 3:   Is: " + list[0].Field);
		Console.WriteLine("Want 3:   Is: " + list[0].Property);

		var container = new Container();
		container.Test();

		Console.WriteLine("Press Enter to exit.");
		Console.ReadLine();
	}

	private static void ChangeByReference(ref MutableStruct value)
	{
		value.Field = 1;
		value.Property = 2;

		Console.WriteLine("Want 1:   Is: " + value.Field);
		Console.WriteLine("Want 2:   Is: " + value.Property);

		value.Method(3);

		Console.WriteLine("Want 3:   Is: " + value.Field);
		Console.WriteLine("Want 3:   Is: " + value.Property);

		value = new MutableStruct
		{
			Field = 4,
			Property = 5,
		};

		Console.WriteLine("Want 4:   Is: " + value.Field);
		Console.WriteLine("Want 5:   Is: " + value.Property);
	}

	private struct MutableStruct
	{
		public int Field;

		public int Property { get; set; }

		public void Method(int value)
		{
			this.Field = value;
			this.Property = value;
		}
	}

	private class Container
	{
		private MutableStruct value = default;

		public void Test()
		{
			Console.WriteLine("Class local variable");

			this.value.Field = 1;
			this.value.Property = 2;

			Console.WriteLine("Want 1:   Is: " + this.value.Field);
			Console.WriteLine("Want 2:   Is: " + this.value.Property);

			this.value.Method(3);

			Console.WriteLine("Want 3:   Is: " + this.value.Field);
			Console.WriteLine("Want 3:   Is: " + this.value.Property);

			this.value = new MutableStruct
			{
				Field = 4,
				Property = 5,
			};

			Console.WriteLine("Want 4:   Is: " + this.value.Field);
			Console.WriteLine("Want 5:   Is: " + this.value.Property);

			Console.WriteLine("Class local variable by reference");
			ChangeByReference(ref this.value);
		}
	}
}
