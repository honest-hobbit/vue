﻿namespace HQS.Test.ConsoleApp;

public static class ContextSwitchingPerformance
{
	private const int ContextSwitchCount = 1000;

	private const int WorkLoopCount = 10000;

	public static void TestSpeed()
	{
		var timer = new Stopwatch();
		timer.Restart();

		TestSingleThread();

		timer.Stop();
		Console.WriteLine($"Single thread took: {timer.Elapsed.TotalMilliseconds:N2}");
		timer.Restart();

		TestContextSwitching();

		timer.Stop();
		Console.WriteLine($"Context switching took: {timer.Elapsed.TotalMilliseconds:N2}");
	}

	private static void TestSingleThread()
	{
		long total = 0;
		for (int i = 0; i < ContextSwitchCount; i++)
		{
			total += CalculateWork(i);
		}

		Console.WriteLine("Result: " + total);
	}

	private static void TestContextSwitching()
	{
		long total = 0;
		for (int i = 0; i < ContextSwitchCount; i++)
		{
			total += Task.Run(() => CalculateWork(i)).Result;
		}

		Console.WriteLine("Result: " + total);
	}

	private static long CalculateWork(long start)
	{
		for (long i = 1; i < WorkLoopCount; i++)
		{
			start *= i;
			start *= i + 1;
			start /= i;
			start /= i + 1;
		}

		return start;
	}
}
