﻿namespace HQS.Test.ConsoleApp;

public static class ComparerPerformance
{
	private const int Length = 10000;

	private const int Repeat = 1000;

	private static readonly int[] Ints = new int[Length];

	private static readonly Guid[] Guids = new Guid[Length];

	private static readonly Wrapper[] Wrappers = new Wrapper[Length];

	public static void RunTests()
	{
		Console.WriteLine("Running Comparer Performance Tests");
		Console.WriteLine();

		FillArrays(1337);

		var intComparer = EqualityComparer<int>.Default;
		ByEqualityComparer(Ints, intComparer, "Int");
		ByComparerInterface(Ints, intComparer, "Int");
		ByFunc(Ints, (x, y) => intComparer.Equals(x, y), "Int Func Comparer");
		ByFunc(Ints, (x, y) => x == y, "Int ==");
		ByEquals(Ints, "Int");
		HardCodedInt();

		Console.WriteLine();

		var guidComparer = EqualityComparer<Guid>.Default;
		ByEqualityComparer(Guids, guidComparer, "Guid");
		ByComparerInterface(Guids, guidComparer, "Guid");
		ByFunc(Guids, (x, y) => guidComparer.Equals(x, y), "Guid Func Comparer");
		ByFunc(Guids, (x, y) => x == y, "Guid ==");
		ByEquals(Guids, "Guid");
		HardCodedGuid();

		Console.WriteLine();

		var wrapperComparer = WrapperComparer.Default;
		ByEqualityComparer(Wrappers, wrapperComparer, "Wrapper");
		ByComparerInterface(Wrappers, wrapperComparer, "Wrapper");
		ByFunc(Wrappers, (x, y) => wrapperComparer.Equals(x, y), "Wrapper Func Comparer");
		ByFunc(Wrappers, (x, y) => x == y, "Wrapper ==");
		ByEquals(Wrappers, "Wrapper");
		HardCodedWrapper();
		HardCodedWrapperProperties();

		Console.WriteLine();
		Console.WriteLine("Press enter to end.");
		Console.ReadLine();
	}

	private static void FillArrays(int seed)
	{
		var random = new Random(seed);
		var bytes = new byte[16];

		for (int i = 0; i < Length; i++)
		{
			Ints[i] = random.Next(int.MinValue, int.MaxValue);
			Wrappers[i] = new Wrapper((ushort)random.Next(0, ushort.MaxValue));

			random.NextBytes(bytes);
			Guids[i] = new Guid(bytes);
		}
	}

	private static Exception CreateException() => new Exception("THE TEST FAILED. THIS SHOULD NOT HAPPEN!");

	private static void WriteResults(Stopwatch timer, string name) =>
		Console.WriteLine($"{timer.ElapsedTicks / (double)Repeat} ticks for {name}");

	private static void ByEqualityComparer<T>(T[] array, EqualityComparer<T> comparer, string name)
	{
		Debug.Assert(array != null);
		Debug.Assert(comparer != null);

		var timer = new Stopwatch();
		timer.Start();

		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (!comparer.Equals(array[i], array[i]))
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(ByEqualityComparer) + " " + name);
	}

	private static void ByComparerInterface<T>(T[] array, IEqualityComparer<T> comparer, string name)
	{
		Debug.Assert(array != null);
		Debug.Assert(comparer != null);

		var timer = new Stopwatch();
		timer.Start();

		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (!comparer.Equals(array[i], array[i]))
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(ByComparerInterface) + " " + name);
	}

	private static void ByFunc<T>(T[] array, Func<T, T, bool> comparer, string name)
	{
		Debug.Assert(array != null);
		Debug.Assert(comparer != null);

		var timer = new Stopwatch();
		timer.Start();

		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (!comparer(array[i], array[i]))
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(ByFunc) + " " + name);
	}

	private static void ByEquals<T>(T[] array, string name)
		where T : IEquatable<T>
	{
		Debug.Assert(array != null);

		var timer = new Stopwatch();
		timer.Start();

		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (!array[i].Equals(array[i]))
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(ByEquals) + " " + name);
	}

	private static void HardCodedInt()
	{
		var timer = new Stopwatch();
		timer.Start();

		var array = Ints;
		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (array[i] != array[i])
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(HardCodedInt));
	}

	private static void HardCodedGuid()
	{
		var timer = new Stopwatch();
		timer.Start();

		var array = Guids;
		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (array[i] != array[i])
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(HardCodedGuid));
	}

	private static void HardCodedWrapper()
	{
		var timer = new Stopwatch();
		timer.Start();

		var array = Wrappers;
		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (array[i] != array[i])
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(HardCodedWrapper));
	}

	private static void HardCodedWrapperProperties()
	{
		var timer = new Stopwatch();
		timer.Start();

		var array = Wrappers;
		for (int repeat = 0; repeat < Repeat; repeat++)
		{
			for (int i = 0; i < Length; i++)
			{
				if (array[i].Value != array[i].Value)
				{
					throw CreateException();
				}
			}
		}

		timer.Stop();
		WriteResults(timer, nameof(HardCodedWrapperProperties));
	}

	private readonly struct Wrapper : IEquatable<Wrapper>
	{
		public Wrapper(ushort value)
		{
			this.Value = value;
		}

		public static bool operator ==(Wrapper lhs, Wrapper rhs) => lhs.Equals(rhs);

		public static bool operator !=(Wrapper lhs, Wrapper rhs) => !lhs.Equals(rhs);

		public ushort Value { get; }

		public bool Equals(Wrapper other) => this.Value == other.Value;

		public override bool Equals(object obj)
		{
			throw new NotImplementedException();
		}

		public override int GetHashCode()
		{
			throw new NotImplementedException();
		}
	}

	private class WrapperComparer : EqualityComparer<Wrapper>
	{
		private WrapperComparer()
		{
		}

		public static EqualityComparer<Wrapper> Instance { get; } = new WrapperComparer();

		public override bool Equals(Wrapper x, Wrapper y) => x == y;

		public override int GetHashCode(Wrapper obj)
		{
			throw new NotImplementedException();
		}
	}
}
