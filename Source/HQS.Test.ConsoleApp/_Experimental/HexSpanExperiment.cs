﻿namespace HQS.Test.ConsoleApp;

public static class HexSpanExperiment
{
	public static void Run()
	{
		Span<int> intSpan = stackalloc int[1];

		for (int i = 0; i <= 2000; i++)
		{
			intSpan[0] = i;
			var source = MemoryMarshal.AsBytes(intSpan);

			////var num1 = (byte)(i >> 8);
			////var num2 = (byte)(i & byte.MaxValue);

			var asHex = Convert.ToHexString(source);
			var result = Convert.FromHexString(asHex);

			Console.WriteLine($"{i}   [{asHex}]   {source[0]} {source[1]} {source[2]} {source[3]}   {result[0]} {result[1]} {result[2]} {result[3]}");
		}
	}
}
