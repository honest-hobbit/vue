﻿namespace HQS.Test.ConsoleApp;

public static class MemoryUsage
{
	public static void TestMemoryUsed()
	{
		_ = Process.GetCurrentProcess();
		var initialSize = GC.GetTotalMemory(forceFullCollection: true);
		_ = Array.Empty<TestValue>();
		var overheadSize = GC.GetTotalMemory(forceFullCollection: true) - initialSize;

		var count = 128;
		_ = new TestValue[count];
		var finalSize = GC.GetTotalMemory(forceFullCollection: true);
		var sizePerValue = (float)(finalSize - initialSize - overheadSize) / count;

		Console.WriteLine("Memory Used Per Value: " + sizePerValue);
		Console.ReadLine();
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	private readonly struct TestValue
	{
		public TestValue(int number)
		{
			unchecked
			{
				this.Type = (byte)number;
				this.Next = (byte)(this.Type + 1);
				this.Value0 = (byte)(this.Next + 1);
				this.Value1 = (byte)(this.Value0 + 1);
				this.Value2 = (byte)(this.Value1 + 1);
				this.Value3 = (byte)(this.Value2 + 1);
				this.Value4 = (byte)(this.Value3 + 1);
				this.Value5 = (byte)(this.Value4 + 1);
				this.Value6 = (byte)(this.Value5 + 1);
				this.Value7 = (byte)(this.Value6 + 1);
				this.Value8 = (byte)(this.Value7 + 1);
				this.Value9 = (byte)(this.Value8 + 1);
				this.Value10 = (byte)(this.Value9 + 1);
				////this.Value11 = (byte)(this.Value10 + 1);
			}
		}

		public ushort Type { get; }

		public ushort Next { get; }

		public byte Value0 { get; }

		public byte Value1 { get; }

		public byte Value2 { get; }

		public byte Value3 { get; }

		public byte Value4 { get; }

		public byte Value5 { get; }

		public byte Value6 { get; }

		public byte Value7 { get; }

		public byte Value8 { get; }

		public byte Value9 { get; }

		public byte Value10 { get; }

		////public byte Value11 { get; }
	}
}
