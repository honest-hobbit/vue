﻿namespace HQS.Test.ConsoleApp;

public static class ThreeByteInt
{
	public static void TestMemoryUsed()
	{
		_ = Process.GetCurrentProcess();
		var initialSize = GC.GetTotalMemory(forceFullCollection: true);
		_ = Array.Empty<Int24>();
		var overheadSize = GC.GetTotalMemory(forceFullCollection: true) - initialSize;

		var count = 256;
		_ = new Int24[count];
		var finalSize = GC.GetTotalMemory(forceFullCollection: true);
		var sizePerValue = (float)(finalSize - initialSize - overheadSize) / count;

		Console.WriteLine("Memory Used Per Value: " + sizePerValue);
		Console.ReadLine();
	}

	public static void TestValues()
	{
		int max = MathUtility.IntegerPower(2, 24);
		for (int count = 0; count < max; count++)
		{
			var value = new Int24(count);
			if (value.Value != count)
			{
				throw new ArgumentException("Values don't match!!!");
			}
		}

		Console.WriteLine($"All {max} test values were successful");
		Console.ReadLine();
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	private readonly struct Int24
	{
		public Int24(int value)
		{
			unchecked
			{
				this.Byte0 = (byte)value;
				this.Byte1 = (byte)(value >> 8);
				this.Byte2 = (byte)(value >> 16);
			}
		}

		public byte Byte0 { get; }

		public byte Byte1 { get; }

		public byte Byte2 { get; }

		public int Value => (this.Byte2 << 16) | (this.Byte1 << 8) | this.Byte0;
	}
}
