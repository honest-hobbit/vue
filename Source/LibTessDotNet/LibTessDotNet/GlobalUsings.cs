﻿global using System;
global using System.Collections.Generic;
global using System.Diagnostics;

#if DOUBLE
global using Real = System.Double;
#else
global using Real = System.Single;
#endif
