﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

// TODO the pooling system of LibTessDotNet does not work with multiple threads yet
internal class ThreadSafePools : Pools
{
	protected override ITypePool<T> CreatePool<T>(Action<T> initialize, Action<T> deinitialize)
	{
		return new ThreadSafeTypePool<T>(initialize, deinitialize);
	}
}
