﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class Mesh
{
	private ITypePool<Vertex> _vertexPool;

	private ITypePool<Face> _facePool;

	private ITypePool<Edge> _edgePool;

	public Vertex _vHead;

	public Face _fHead;

	public Edge _eHead, _eHeadSym;

	public void Init(
		ITypePool<Vertex> vertexPool,
		ITypePool<Face> facePool,
		ITypePool<Edge> edgePool)
	{
		Debug.Assert(vertexPool != null);
		Debug.Assert(facePool != null);
		Debug.Assert(edgePool != null);

		_vertexPool = vertexPool;
		_facePool = facePool;
		_edgePool = edgePool;

		var v = _vHead = vertexPool.Rent();
		var f = _fHead = facePool.Rent();

		var pair = EdgePair.Create(edgePool);
		var e = _eHead = pair._e;
		var eSym = _eHeadSym = pair._eSym;

		v._next = v._prev = v;
		v._anEdge = null;

		f._next = f._prev = f;
		f._anEdge = null;
		f._trail = null;
		f._marked = false;
		f._inside = false;

		e._next = e;
		e._Sym = eSym;
		e._Onext = null;
		e._Lnext = null;
		e._Org = null;
		e._Lface = null;
		e._winding = 0;
		e._activeRegion = null;

		eSym._next = eSym;
		eSym._Sym = e;
		eSym._Onext = null;
		eSym._Lnext = null;
		eSym._Org = null;
		eSym._Lface = null;
		eSym._winding = 0;
		eSym._activeRegion = null;
	}

	public void Reset()
	{
		for (Face f = _fHead, fNext = _fHead; f._next != null; f = fNext)
		{
			fNext = f._next;
			_facePool.Return(f);
		}

		for (Vertex v = _vHead, vNext = _vHead; v._next != null; v = vNext)
		{
			vNext = v._next;
			_vertexPool.Return(v);
		}

		for (Edge e = _eHead, eNext = _eHead; e._next != null; e = eNext)
		{
			eNext = e._next;
			_edgePool.Return(e._Sym);
			_edgePool.Return(e);
		}

		_vHead = null;
		_fHead = null;
		_eHead = _eHeadSym = null;

		_vertexPool = null;
		_facePool = null;
		_edgePool = null;
	}

	/// <summary>
	/// Creates one edge, two vertices and a loop (face).
	/// The loop consists of the two new half-edges.
	/// </summary>
	public Edge MakeEdge()
	{
		var e = MakeEdge(_eHead);

		MakeVertex(e, _vHead);
		MakeVertex(e._Sym, _vHead);
		MakeFace(e, _fHead);

		return e;
	}

	/// <summary>
	/// Splice is the basic operation for changing the
	/// mesh connectivity and topology.  It changes the mesh so that
	///	 eOrg->Onext = OLD( eDst->Onext )
	///	 eDst->Onext = OLD( eOrg->Onext )
	/// where OLD(...) means the value before the meshSplice operation.
	/// 
	/// This can have two effects on the vertex structure:
	///  - if eOrg->Org != eDst->Org, the two vertices are merged together
	///  - if eOrg->Org == eDst->Org, the origin is split into two vertices
	/// In both cases, eDst->Org is changed and eOrg->Org is untouched.
	/// 
	/// Similarly (and independently) for the face structure,
	///  - if eOrg->Lface == eDst->Lface, one loop is split into two
	///  - if eOrg->Lface != eDst->Lface, two distinct loops are joined into one
	/// In both cases, eDst->Lface is changed and eOrg->Lface is unaffected.
	/// 
	/// Some special cases:
	/// If eDst == eOrg, the operation has no effect.
	/// If eDst == eOrg->Lnext, the new face will have a single edge.
	/// If eDst == eOrg->Lprev, the old face will have a single edge.
	/// If eDst == eOrg->Onext, the new vertex will have a single edge.
	/// If eDst == eOrg->Oprev, the old vertex will have a single edge.
	/// </summary>
	public void Splice(Edge eOrg, Edge eDst)
	{
		Debug.Assert(eOrg != null);
		Debug.Assert(eDst != null);

		if (eOrg == eDst)
		{
			return;
		}

		bool joiningVertices = false;
		if (eDst._Org != eOrg._Org)
		{
			// We are merging two disjoint vertices -- destroy eDst->Org
			joiningVertices = true;
			KillVertex(eDst._Org, eOrg._Org);
		}
		bool joiningLoops = false;
		if (eDst._Lface != eOrg._Lface)
		{
			// We are connecting two disjoint loops -- destroy eDst->Lface
			joiningLoops = true;
			KillFace(eDst._Lface, eOrg._Lface);
		}

		// Change the edge structure
		SpliceEdge(eDst, eOrg);

		if (!joiningVertices)
		{
			// We split one vertex into two -- the new vertex is eDst->Org.
			// Make sure the old vertex points to a valid half-edge.
			MakeVertex(eDst, eOrg._Org);
			eOrg._Org._anEdge = eOrg;
		}
		if (!joiningLoops)
		{
			// We split one loop into two -- the new loop is eDst->Lface.
			// Make sure the old face points to a valid half-edge.
			MakeFace(eDst, eOrg._Lface);
			eOrg._Lface._anEdge = eOrg;
		}
	}

	/// <summary>
	/// Removes the edge eDel. There are several cases:
	/// if (eDel->Lface != eDel->Rface), we join two loops into one; the loop
	/// eDel->Lface is deleted. Otherwise, we are splitting one loop into two;
	/// the newly created loop will contain eDel->Dst. If the deletion of eDel
	/// would create isolated vertices, those are deleted as well.
	/// </summary>
	public void Delete(Edge eDel)
	{
		Debug.Assert(eDel != null);

		var eDelSym = eDel._Sym;

		// First step: disconnect the origin vertex eDel->Org.  We make all
		// changes to get a consistent mesh in this "intermediate" state.

		bool joiningLoops = false;
		if (eDel._Lface != eDel._Rface)
		{
			// We are joining two loops into one -- remove the left face
			joiningLoops = true;
			KillFace(eDel._Lface, eDel._Rface);
		}

		if (eDel._Onext == eDel)
		{
			KillVertex(eDel._Org, null);
		}
		else
		{
			// Make sure that eDel->Org and eDel->Rface point to valid half-edges
			eDel._Rface._anEdge = eDel._Oprev;
			eDel._Org._anEdge = eDel._Onext;

			SpliceEdge(eDel, eDel._Oprev);

			if (!joiningLoops)
			{
				// We are splitting one loop into two -- create a new loop for eDel.
				MakeFace(eDel, eDel._Lface);
			}
		}

		// Claim: the mesh is now in a consistent state, except that eDel->Org
		// may have been deleted.  Now we disconnect eDel->Dst.

		if (eDelSym._Onext == eDelSym)
		{
			KillVertex(eDelSym._Org, null);
			KillFace(eDelSym._Lface, null);
		}
		else
		{
			// Make sure that eDel->Dst and eDel->Lface point to valid half-edges
			eDel._Lface._anEdge = eDelSym._Oprev;
			eDelSym._Org._anEdge = eDelSym._Onext;
			SpliceEdge(eDelSym, eDelSym._Oprev);
		}

		// Any isolated vertices or faces have already been freed.
		KillEdge(eDel);
	}

	/// <summary>
	/// Creates a new edge such that eNew == eOrg.Lnext and eNew.Dst is a newly created vertex.
	/// eOrg and eNew will have the same left face.
	/// </summary>
	public Edge AddEdgeVertex(Edge eOrg)
	{
		Debug.Assert(eOrg != null);

		var eNew = MakeEdge(eOrg);
		var eNewSym = eNew._Sym;

		// Connect the new edge appropriately
		SpliceEdge(eNew, eOrg._Lnext);

		// Set vertex and face information
		eNew._Org = eOrg._Dst;
		MakeVertex(eNewSym, eNew._Org);
		eNew._Lface = eNewSym._Lface = eOrg._Lface;

		return eNew;
	}

	/// <summary>
	/// Splits eOrg into two edges eOrg and eNew such that eNew == eOrg.Lnext.
	/// The new vertex is eOrg.Dst == eNew.Org.
	/// eOrg and eNew will have the same left face.
	/// </summary>
	public Edge SplitEdge(Edge eOrg)
	{
		Debug.Assert(eOrg != null);

		var eTmp = AddEdgeVertex(eOrg);
		var eNew = eTmp._Sym;

		// Disconnect eOrg from eOrg->Dst and connect it to eNew->Org
		SpliceEdge(eOrg._Sym, eOrg._Sym._Oprev);
		SpliceEdge(eOrg._Sym, eNew);

		// Set the vertex and face information
		eOrg._Dst = eNew._Org;
		eNew._Dst._anEdge = eNew._Sym; // may have pointed to eOrg->Sym
		eNew._Rface = eOrg._Rface;
		eNew._winding = eOrg._winding; // copy old winding information
		eNew._Sym._winding = eOrg._Sym._winding;

		return eNew;
	}

	/// <summary>
	/// Creates a new edge from eOrg->Dst to eDst->Org, and returns the corresponding half-edge eNew.
	/// If eOrg->Lface == eDst->Lface, this splits one loop into two,
	/// and the newly created loop is eNew->Lface.  Otherwise, two disjoint
	/// loops are merged into one, and the loop eDst->Lface is destroyed.
	/// 
	/// If (eOrg == eDst), the new face will have only two edges.
	/// If (eOrg->Lnext == eDst), the old face is reduced to a single edge.
	/// If (eOrg->Lnext->Lnext == eDst), the old face is reduced to two edges.
	/// </summary>
	public Edge Connect(Edge eOrg, Edge eDst)
	{
		Debug.Assert(eOrg != null);
		Debug.Assert(eDst != null);

		var eNew = MakeEdge(eOrg);
		var eNewSym = eNew._Sym;

		bool joiningLoops = false;
		if (eDst._Lface != eOrg._Lface)
		{
			// We are connecting two disjoint loops -- destroy eDst->Lface
			joiningLoops = true;
			KillFace(eDst._Lface, eOrg._Lface);
		}

		// Connect the new edge appropriately
		SpliceEdge(eNew, eOrg._Lnext);
		SpliceEdge(eNewSym, eDst);

		// Set the vertex and face information
		eNew._Org = eOrg._Dst;
		eNewSym._Org = eDst._Org;
		eNew._Lface = eNewSym._Lface = eOrg._Lface;

		// Make sure the old face points to a valid half-edge
		eOrg._Lface._anEdge = eNewSym;

		if (!joiningLoops)
		{
			MakeFace(eNew, eOrg._Lface);
		}

		return eNew;
	}

	/// <summary>
	/// Destroys a face and removes it from the global face list. All edges of
	/// fZap will have a NULL pointer as their left face. Any edges which
	/// also have a NULL pointer as their right face are deleted entirely
	/// (along with any isolated vertices this produces).
	/// An entire mesh can be deleted by zapping its faces, one at a time,
	/// in any order. Zapped faces cannot be used in further mesh operations!
	/// </summary>
	public void ZapFace(Face fZap)
	{
		Debug.Assert(fZap != null);

		var eStart = fZap._anEdge;

		// walk around face, deleting edges whose right face is also NULL
		var eNext = eStart._Lnext;
		Edge e, eSym;
		do
		{
			e = eNext;
			eNext = e._Lnext;

			e._Lface = null;
			if (e._Rface == null)
			{
				// delete the edge -- see TESSmeshDelete above

				if (e._Onext == e)
				{
					KillVertex(e._Org, null);
				}
				else
				{
					// Make sure that e._Org points to a valid half-edge
					e._Org._anEdge = e._Onext;
					SpliceEdge(e, e._Oprev);
				}
				eSym = e._Sym;
				if (eSym._Onext == eSym)
				{
					KillVertex(eSym._Org, null);
				}
				else
				{
					// Make sure that eSym._Org points to a valid half-edge
					eSym._Org._anEdge = eSym._Onext;
					SpliceEdge(eSym, eSym._Oprev);
				}
				KillEdge(e);
			}
		} while (e != eStart);

		/* delete from circular doubly-linked list */
		var fPrev = fZap._prev;
		var fNext = fZap._next;
		fNext._prev = fPrev;
		fPrev._next = fNext;

		_facePool.Return(fZap);
	}

	public void MergeConvexFaces(int maxVertsPerFace)
	{
		for (var f = _fHead._next; f != _fHead; f = f._next)
		{
			// Skip faces which are outside the result
			if (!f._inside)
			{
				continue;
			}

			var eCur = f._anEdge;
			var vStart = eCur._Org;

			while (true)
			{
				var eNext = eCur._Lnext;
				var eSym = eCur._Sym;

				if (eSym != null && eSym._Lface != null && eSym._Lface._inside)
				{
					// Try to merge the neighbour faces if the resulting polygons
					// does not exceed maximum number of vertices.
					int curNv = f.VertsCount;
					int symNv = eSym._Lface.VertsCount;
					if ((curNv + symNv - 2) <= maxVertsPerFace)
					{
						// Merge if the resulting poly is convex.
						if (Geom.VertCCW(eCur._Lprev._Org, eCur._Org, eSym._Lnext._Lnext._Org) &&
							Geom.VertCCW(eSym._Lprev._Org, eSym._Org, eCur._Lnext._Lnext._Org))
						{
							eNext = eSym._Lnext;
							Delete(eSym);
							eCur = null;
						}
					}
				}

				if (eCur != null && eCur._Lnext._Org == vStart)
					break;

				// Continue to next edge.
				eCur = eNext;
			}
		}
	}

	[Conditional("DEBUG")]
	public void Check()
	{
		Edge e;

		Face fPrev = _fHead, f;
		for (fPrev = _fHead; (f = fPrev._next) != _fHead; fPrev = f)
		{
			e = f._anEdge;
			do
			{
				Debug.Assert(e._Sym != e);
				Debug.Assert(e._Sym._Sym == e);
				Debug.Assert(e._Lnext._Onext._Sym == e);
				Debug.Assert(e._Onext._Sym._Lnext == e);
				Debug.Assert(e._Lface == f);
				e = e._Lnext;
			} while (e != f._anEdge);
		}
		Debug.Assert(f._prev == fPrev && f._anEdge == null);

		Vertex vPrev = _vHead, v;
		for (vPrev = _vHead; (v = vPrev._next) != _vHead; vPrev = v)
		{
			Debug.Assert(v._prev == vPrev);
			e = v._anEdge;
			do
			{
				Debug.Assert(e._Sym != e);
				Debug.Assert(e._Sym._Sym == e);
				Debug.Assert(e._Lnext._Onext._Sym == e);
				Debug.Assert(e._Onext._Sym._Lnext == e);
				Debug.Assert(e._Org == v);
				e = e._Onext;
			} while (e != v._anEdge);
		}
		Debug.Assert(v._prev == vPrev && v._anEdge == null);

		Edge ePrev = _eHead;
		for (ePrev = _eHead; (e = ePrev._next) != _eHead; ePrev = e)
		{
			Debug.Assert(e._Sym._next == ePrev._Sym);
			Debug.Assert(e._Sym != e);
			Debug.Assert(e._Sym._Sym == e);
			Debug.Assert(e._Org != null);
			Debug.Assert(e._Dst != null);
			Debug.Assert(e._Lnext._Onext._Sym == e);
			Debug.Assert(e._Onext._Sym._Lnext == e);
		}
		Debug.Assert(e._Sym._next == ePrev._Sym
			&& e._Sym == _eHeadSym
			&& e._Sym._Sym == e
			&& e._Org == null && e._Dst == null
			&& e._Lface == null && e._Rface == null);
	}

	/// <summary>
	/// Splice( a, b ) is best described by the Guibas/Stolfi paper or the
	/// CS348a notes (see Mesh.cs). Basically it modifies the mesh so that
	/// a->Onext and b->Onext are exchanged. This can have various effects
	/// depending on whether a and b belong to different face or vertex rings.
	/// For more explanation see Mesh.Splice().
	/// </summary>
	private static void SpliceEdge(Edge a, Edge b)
	{
		Debug.Assert(a != null);
		Debug.Assert(b != null);

		var aOnext = a._Onext;
		var bOnext = b._Onext;

		aOnext._Sym._Lnext = b;
		bOnext._Sym._Lnext = a;
		a._Onext = bOnext;
		b._Onext = aOnext;
	}

	private static void EnsureFirstEdge(ref Edge e)
	{
		Debug.Assert(e != null);

		if (e == e._pair._eSym)
		{
			e = e._Sym;
		}
	}

	/// <summary>
	/// MakeVertex( eOrig, vNext ) attaches a new vertex and makes it the
	/// origin of all edges in the vertex loop to which eOrig belongs. "vNext" gives
	/// a place to insert the new vertex in the global vertex list. We insert
	/// the new vertex *before* vNext so that algorithms which walk the vertex
	/// list will not see the newly created vertices.
	/// </summary>
	private void MakeVertex(Edge eOrig, Vertex vNext)
	{
		Debug.Assert(eOrig != null);
		Debug.Assert(vNext != null);

		var vNew = _vertexPool.Rent();

		// insert in circular doubly-linked list before vNext
		var vPrev = vNext._prev;
		vNew._prev = vPrev;
		vPrev._next = vNew;
		vNew._next = vNext;
		vNext._prev = vNew;

		vNew._anEdge = eOrig;
		// leave coords, s, t undefined

		// fix other edges on this vertex loop
		var e = eOrig;
		do
		{
			e._Org = vNew;
			e = e._Onext;
		} while (e != eOrig);
	}

	/// <summary>
	/// MakeFace( eOrig, fNext ) attaches a new face and makes it the left
	/// face of all edges in the face loop to which eOrig belongs. "fNext" gives
	/// a place to insert the new face in the global face list. We insert
	/// the new face *before* fNext so that algorithms which walk the face
	/// list will not see the newly created faces.
	/// </summary>
	private void MakeFace(Edge eOrig, Face fNext)
	{
		Debug.Assert(eOrig != null);
		Debug.Assert(fNext != null);

		var fNew = _facePool.Rent();

		// insert in circular doubly-linked list before fNext
		var fPrev = fNext._prev;
		fNew._prev = fPrev;
		fPrev._next = fNew;
		fNew._next = fNext;
		fNext._prev = fNew;

		fNew._anEdge = eOrig;
		fNew._trail = null;
		fNew._marked = false;

		// The new face is marked "inside" if the old one was. This is a
		// convenience for the common case where a face has been split in two.
		fNew._inside = fNext._inside;

		// fix other edges on this face loop
		var e = eOrig;
		do
		{
			e._Lface = fNew;
			e = e._Lnext;
		} while (e != eOrig);
	}

	/// <summary>
	/// MakeEdge creates a new pair of half-edges which form their own loop.
	/// No vertex or face structures are allocated, but these must be assigned
	/// before the current edge operation is completed.
	/// </summary>
	private Edge MakeEdge(Edge eNext)
	{
		Debug.Assert(eNext != null);

		var pair = EdgePair.Create(_edgePool);
		var e = pair._e;
		var eSym = pair._eSym;

		// Make sure eNext points to the first edge of the edge pair
		EnsureFirstEdge(ref eNext);

		// Insert in circular doubly-linked list before eNext.
		// Note that the prev pointer is stored in Sym->next.
		var ePrev = eNext._Sym._next;
		eSym._next = ePrev;
		ePrev._Sym._next = e;
		e._next = eNext;
		eNext._Sym._next = eSym;

		e._Sym = eSym;
		e._Onext = e;
		e._Lnext = eSym;
		e._Org = null;
		e._Lface = null;
		e._winding = 0;
		e._activeRegion = null;

		eSym._Sym = e;
		eSym._Onext = eSym;
		eSym._Lnext = e;
		eSym._Org = null;
		eSym._Lface = null;
		eSym._winding = 0;
		eSym._activeRegion = null;

		return e;
	}

	/// <summary>
	/// KillEdge( eDel ) destroys an edge (the half-edges eDel and eDel->Sym),
	/// and removes from the global edge list.
	/// </summary>
	private void KillEdge(Edge eDel)
	{
		Debug.Assert(eDel != null);

		// Half-edges are allocated in pairs, see EdgePair above
		EnsureFirstEdge(ref eDel);

		// delete from circular doubly-linked list
		var eNext = eDel._next;
		var ePrev = eDel._Sym._next;
		eNext._Sym._next = ePrev;
		ePrev._Sym._next = eNext;

		_edgePool.Return(eDel._Sym);
		_edgePool.Return(eDel);
	}

	/// <summary>
	/// KillVertex( vDel ) destroys a vertex and removes it from the global
	/// vertex list. It updates the vertex loop to point to a given new vertex.
	/// </summary>
	private void KillVertex(Vertex vDel, Vertex newOrg)
	{
		Debug.Assert(vDel != null);
		//Debug.Assert(newOrg != null);

		var eStart = vDel._anEdge;

		// change the origin of all affected edges
		var e = eStart;
		do
		{
			e._Org = newOrg;
			e = e._Onext;
		} while (e != eStart);

		// delete from circular doubly-linked list
		var vPrev = vDel._prev;
		var vNext = vDel._next;
		vNext._prev = vPrev;
		vPrev._next = vNext;

		_vertexPool.Return(vDel);
	}

	/// <summary>
	/// KillFace( fDel ) destroys a face and removes it from the global face
	/// list. It updates the face loop to point to a given new face.
	/// </summary>
	private void KillFace(Face fDel, Face newLFace)
	{
		Debug.Assert(fDel != null);
		//Debug.Assert(newLFace != null);

		var eStart = fDel._anEdge;

		// change the left face of all affected edges
		var e = eStart;
		do
		{
			e._Lface = newLFace;
			e = e._Lnext;
		} while (e != eStart);

		// delete from circular doubly-linked list
		var fPrev = fDel._prev;
		var fNext = fDel._next;
		fNext._prev = fPrev;
		fPrev._next = fNext;

		_facePool.Return(fDel);
	}
}
