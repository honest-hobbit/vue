﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

// TODO the pooling system of LibTessDotNet does not work with multiple threads yet
internal sealed class ThreadSafeTypePool<T> : ITypePool<T>
	where T : class, new()
{
	private readonly object _padlock = new object();

	private readonly Stack<T> _pool = new Stack<T>();

	private readonly Action<T> _initialize;

	private readonly Action<T> _deinitialize;

	private int _created;

	public ThreadSafeTypePool(Action<T> initialize, Action<T> deinitialize)
	{
		_initialize = initialize ?? (x => { });
		_deinitialize = deinitialize ?? (x => { });
	}

	public Type PooledType { get; } = typeof(T);

	public int Created
	{
		get
		{
			lock (_padlock)
			{
				return _created;
			}
		}
	}

	public int Available
	{
		get
		{
			lock (_padlock)
			{
				return _pool.Count;
			}
		}
	}

	public int Active
	{
		get
		{
			lock (_padlock)
			{
				return _created - _pool.Count;
			}
		}
	}

	public void Create(int count)
	{
		if (count < 0)
		{
			throw new ArgumentOutOfRangeException(nameof(count));
		}

		lock (_padlock)
		{
			for (int i = 0; i < count; i++)
			{
				_pool.Push(new T());
			}

			_created += count;
		}
	}

	public T Rent()
	{
		T result;
		lock (_padlock)
		{
			if (_pool.Count > 0)
			{
				result = _pool.Pop();
			}
			else
			{
				result = new T();
				_created++;
			}
		}

		_initialize(result);
		return result;
	}

	public void Return(T obj)
	{
		Debug.Assert(obj != null);

		_deinitialize(obj);

		lock (_padlock)
		{
#if DEBUG
			foreach (var other in _pool)
			{
				if (ReferenceEquals(other, obj))
				{
					throw new InvalidOperationException("Object already in the pool.");
				}
			}
#endif

			_pool.Push(obj);
		}
	}
}
