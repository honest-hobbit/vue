﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class Edge
{
	public EdgePair _pair;

	public Edge _next, _Sym, _Onext, _Lnext;

	public Vertex _Org;

	public Face _Lface;

	public ActiveRegion _activeRegion;

	public int _winding;

	public Face _Rface { get { return _Sym._Lface; } set { _Sym._Lface = value; } }

	public Vertex _Dst { get { return _Sym._Org; } set { _Sym._Org = value; } }

	public Edge _Oprev { get { return _Sym._Lnext; } set { _Sym._Lnext = value; } }

	public Edge _Lprev { get { return _Onext._Sym; } set { _Onext._Sym = value; } }

	public Edge _Dprev { get { return _Lnext._Sym; } set { _Lnext._Sym = value; } }

	public Edge _Rprev { get { return _Sym._Onext; } set { _Sym._Onext = value; } }

	public Edge _Dnext { get { return _Rprev._Sym; } set { _Rprev._Sym = value; } }

	public Edge _Rnext { get { return _Oprev._Sym; } set { _Oprev._Sym = value; } }

	public void Reset()
	{
		_pair = default;
		_next = _Sym = _Onext = _Lnext = null;
		_Org = null;
		_Lface = null;
		_activeRegion = null;
		_winding = 0;
	}
}
