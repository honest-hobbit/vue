﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class Face
{
	public Face _prev, _next;

	public Edge _anEdge;

	public Face _trail;

	public int _n;

	public bool _marked, _inside;

	public int VertsCount
	{
		get
		{
			int n = 0;
			var eCur = _anEdge;
			do
			{
				n++;
				eCur = eCur._Lnext;
			} while (eCur != _anEdge);
			return n;
		}
	}

	/// <summary>
	/// Return signed area of face.
	/// </summary>
	public Real SignedArea
	{
		get
		{
			Real area = 0;
			var e = _anEdge;
			do
			{
				area += (e._Org._s - e._Dst._s) * (e._Org._t + e._Dst._t);
				e = e._Lnext;
			} while (e != _anEdge);
			return area;
		}
	}

	public void Reset()
	{
		_prev = _next = null;
		_anEdge = null;
		_trail = null;
		_n = 0;
		_marked = false;
		_inside = false;
	}
}
