﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class DefaultTypePool<T> : ITypePool<T>
	where T : class, new()
{
	private readonly Stack<T> _pool = new Stack<T>();

	private readonly Action<T> _initialize;

	private readonly Action<T> _deinitialize;

	public DefaultTypePool(Action<T> initialize, Action<T> deinitialize)
	{
		_initialize = initialize ?? (x => { });
		_deinitialize = deinitialize ?? (x => { });
	}

	public Type PooledType { get; } = typeof(T);

	public int Created { get; private set; }

	public int Available => _pool.Count;

	public int Active => this.Created - this.Available;

	public void Create(int count)
	{
		if (count < 0)
		{
			throw new ArgumentOutOfRangeException(nameof(count));
		}

		for (int i = 0; i < count; i++)
		{
			_pool.Push(new T());
		}

		Created += count;
	}

	public T Rent()
	{
		T result;
		if (_pool.Count > 0)
		{
			result = _pool.Pop();
		}
		else
		{
			result = new T();
			Created++;
		}

		_initialize(result);
		return result;
	}

	public void Return(T obj)
	{
		Debug.Assert(obj != null);

		_deinitialize(obj);

#if DEBUG
		foreach (var other in _pool)
		{
			if (ReferenceEquals(other, obj))
			{
				throw new InvalidOperationException("Object already in the pool.");
			}
		}
#endif

		_pool.Push(obj);
	}
}
