﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class ActiveRegion
{
	public Edge _eUp;

	public Node<ActiveRegion> _nodeUp;

	public int _windingNumber;

	public bool _inside, _sentinel, _dirty, _fixUpperEdge;

	public void Reset()
	{
		_eUp = null;
		_nodeUp = null; // don't return to pool as Dict takes care of that
		_windingNumber = 0;
		_inside = false;
		_sentinel = false;
		_dirty = false;
		_fixUpperEdge = false;
	}
}
