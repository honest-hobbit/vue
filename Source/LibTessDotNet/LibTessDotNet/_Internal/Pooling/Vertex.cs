﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class Vertex
{
	public Vertex _prev, _next;

	public Edge _anEdge;

	public Vec3 _coords;

	public Real _s, _t;

	public PQHandle _pqHandle;

	public int _n;

	public object _data;

	public void Reset()
	{
		_prev = _next = null;
		_anEdge = null;
		_coords = Vec3.Zero;
		_s = 0;
		_t = 0;
		_pqHandle = default;
		_n = 0;
		_data = null;
	}
}
