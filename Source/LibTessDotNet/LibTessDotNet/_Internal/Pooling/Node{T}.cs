﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class Node<T>
	where T : class
{
	public T _key;

	public Node<T> _prev, _next;

	public void Reset()
	{
		_key = null;
		_prev = null;
		_next = null;
	}
}
