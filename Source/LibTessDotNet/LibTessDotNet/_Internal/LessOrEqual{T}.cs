﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal delegate bool LessOrEqual<T>(T lhs, T rhs);
