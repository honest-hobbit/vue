﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal struct EdgePair
{
	public Edge _e, _eSym;

	public static EdgePair Create(ITypePool<Edge> pool)
	{
		Debug.Assert(pool != null);

		var e = pool.Rent();
		var eSym = pool.Rent();

		e._pair._e = e;
		e._pair._eSym = eSym;
		eSym._pair = e._pair;
		return e._pair;
	}
}
