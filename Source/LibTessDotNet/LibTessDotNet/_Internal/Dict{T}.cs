﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class Dict<T>
	where T : class
{
	private readonly ITypePool<Node<T>> _pool;

	private readonly LessOrEqual<T> _leq;

	private Node<T> _head;

	public Dict(ITypePool<Node<T>> pool, LessOrEqual<T> leq)
	{
		Debug.Assert(pool != null);
		Debug.Assert(leq != null);

		_pool = pool;
		_leq = leq;

		Init();
	}

	public bool Empty { get { return _head._next == _head; } }

	public void Init()
	{
		_head = _pool.Rent();
		_head._prev = _head;
		_head._next = _head;
	}

	public void Reset()
	{
		_pool.Return(_head);
		_head = null;
	}

	public Node<T> Insert(T key)
	{
		return InsertBefore(_head, key);
	}

	public Node<T> InsertBefore(Node<T> node, T key)
	{
		do {
			node = node._prev;
		} while (node._key != null && !_leq(node._key, key));

		var newNode = _pool.Rent();
		newNode._key = key;
		newNode._next = node._next;
		node._next._prev = newNode;
		newNode._prev = node;
		node._next = newNode;

		return newNode;
	}

	public Node<T> Find(T key)
	{
		var node = _head;
		do {
			node = node._next;
		} while (node._key != null && !_leq(key, node._key));
		return node;
	}

	public Node<T> Min()
	{
		return _head._next;
	}

	public void Remove(Node<T> node)
	{
		node._next._prev = node._prev;
		node._prev._next = node._next;
		_pool.Return(node);
	}
}
