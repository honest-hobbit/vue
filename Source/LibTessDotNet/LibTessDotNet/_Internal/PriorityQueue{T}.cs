﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal sealed class PriorityQueue<T>
	where T : class
{
	private struct StackItem
	{
		public int p, r;
	};

	// do not create with capacity set to initialSize because
	// initialSize is often much larger than necessary
	private readonly Stack<StackItem> _stack = new Stack<StackItem>(8);

	private readonly LessOrEqual<T> _leq;

	private readonly PriorityHeap<T> _heap;

	private T[] _keys;

	private int[] _order;

	private int _size, _max;

	private bool _initialized;

	public PriorityQueue(int initialSize, LessOrEqual<T> leq)
	{
		Debug.Assert(initialSize >= 0);
		Debug.Assert(leq != null);

		_leq = leq;
		_heap = new PriorityHeap<T>(initialSize, leq);

		_keys = new T[initialSize];

		_size = 0;
		_max = initialSize;
		_initialized = false;
	}

	public bool Empty { get { return _size == 0 && _heap.Empty; } }


	public void Init()
	{
		int p, r, i, j, piv;
		uint seed = 2016473283;

		p = 0;
		r = _size - 1;

		if (_order == null || _order.Length < _size + 1)
		{
			_order = new int[_size + 1];
		}

		for (piv = 0, i = p; i <= r; ++piv, ++i)
		{
			_order[i] = piv;
		}

		_stack.Push(new StackItem { p = p, r = r });
		while (_stack.Count > 0)
		{
			var top = _stack.Pop();
			p = top.p;
			r = top.r;

			while (r > p + 10)
			{
				seed = seed * 1539415821 + 1;
				i = p + (int)(seed % (r - p + 1));
				piv = _order[i];
				_order[i] = _order[p];
				_order[p] = piv;
				i = p - 1;
				j = r + 1;
				do {
					do { ++i; } while (!_leq(_keys[_order[i]], _keys[piv]));
					do { --j; } while (!_leq(_keys[piv], _keys[_order[j]]));
					Swap(ref _order[i], ref _order[j]);
				} while (i < j);
				Swap(ref _order[i], ref _order[j]);
				if (i - p < r - j)
				{
					_stack.Push(new StackItem { p = j + 1, r = r });
					r = i - 1;
				}
				else
				{
					_stack.Push(new StackItem { p = p, r = i - 1 });
					p = j + 1;
				}
			}
			for (i = p + 1; i <= r; ++i)
			{
				piv = _order[i];
				for (j = i; j > p && !_leq(_keys[piv], _keys[_order[j - 1]]); --j)
				{
					_order[j] = _order[j - 1];
				}
				_order[j] = piv;
			}
		}

#if DEBUG
		p = 0;
		r = _size - 1;
		for (i = p; i < r; ++i)
		{
			Debug.Assert(_leq(_keys[_order[i + 1]], _keys[_order[i]]), "Wrong sort");
		}
#endif

		_max = _size;
		_initialized = true;
		_heap.Init();
	}

	public PQHandle Insert(T value)
	{
		if (_initialized)
		{
			return _heap.Insert(value);
		}

		int curr = _size;
		if (++_size >= _max)
		{
			_max <<= 1;
			Array.Resize(ref _keys, _max);
		}

		_keys[curr] = value;
		return new PQHandle { _handle = -(curr + 1) };
	}

	public T ExtractMin()
	{
		Debug.Assert(_initialized);

		if (_size == 0)
		{
			return _heap.ExtractMin();
		}
		T sortMin = _keys[_order[_size - 1]];
		if (!_heap.Empty)
		{
			T heapMin = _heap.Minimum();
			if (_leq(heapMin, sortMin))
				return _heap.ExtractMin();
		}
		do {
			--_size;
		} while (_size > 0 && _keys[_order[_size - 1]] == null);

		return sortMin;
	}

	public T Minimum()
	{
		Debug.Assert(_initialized);

		if (_size == 0)
		{
			return _heap.Minimum();
		}
		T sortMin = _keys[_order[_size - 1]];
		if (!_heap.Empty)
		{
			T heapMin = _heap.Minimum();
			if (_leq(heapMin, sortMin))
				return heapMin;
		}
		return sortMin;
	}

	public void Remove(PQHandle handle)
	{
		Debug.Assert(_initialized);

		int curr = handle._handle;
		if (curr >= 0)
		{
			_heap.Remove(handle);
			return;
		}
		curr = -(curr + 1);
		Debug.Assert(curr < _max && _keys[curr] != null);

		_keys[curr] = null;
		while (_size > 0 && _keys[_order[_size - 1]] == null)
		{
			--_size;
		}
	}

	private static void Swap(ref int a, ref int b)
	{
		int tmp = a;
		a = b;
		b = tmp;
	}
}
