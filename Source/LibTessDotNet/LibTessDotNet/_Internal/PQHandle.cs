﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

internal struct PQHandle
{
	public static readonly int Invalid = 0x0fffffff;

	internal int _handle;
}
