﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

public delegate object CombineCallback(Vec3 position, object[] data, Real[] weights);
