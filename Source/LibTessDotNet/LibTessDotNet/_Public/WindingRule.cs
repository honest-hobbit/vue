﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

/// <summary>
/// The winding rule determines how the different contours are combined together.
/// See OpenGL Programming Guide (section "Winding Numbers and Winding Rules") for description of the winding rules.
/// http://www.glprogramming.com/red/chapter11.html
/// </summary>
public enum WindingRule
{
	EvenOdd,
	NonZero,
	Positive,
	Negative,
	AbsGeqTwo
}
