﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

public class Pools
{
	internal readonly ITypePool<ActiveRegion> _regionPool;

	internal readonly ITypePool<Node<ActiveRegion>> _nodePool;

	internal readonly ITypePool<Mesh> _meshPool;

	internal readonly ITypePool<Vertex> _vertexPool;

	private readonly ITypePool<Face> _facePool;

	private readonly ITypePool<Edge> _edgePool;

	public Pools()
	{
		_regionPool = CreatePool<ActiveRegion>(null, x => x.Reset());
		_nodePool = CreatePool<Node<ActiveRegion>>(null, x => x.Reset());

		_vertexPool = CreatePool<Vertex>(null, x => x.Reset());
		_facePool = CreatePool<Face>(null, x => x.Reset());
		_edgePool = CreatePool<Edge>(null, x => x.Reset());

		_meshPool = CreatePool<Mesh>(x => x.Init(_vertexPool, _facePool, _edgePool), x => x.Reset());
	}

	public ITypePool MeshPool => _meshPool;

	public ITypePool VertexPool => _vertexPool;

	public ITypePool FacePool => _facePool;

	public ITypePool EdgePool => _edgePool;

	public ITypePool RegionPool => _regionPool;

	public ITypePool NodePool => _nodePool;

	public bool AllObjectsAvailable =>
		_meshPool.Active == 0 &&
		_vertexPool.Active == 0 &&
		_facePool.Active == 0 &&
		_edgePool.Active == 0 &&
		_regionPool.Active == 0 &&
		_nodePool.Active == 0;

	protected virtual ITypePool<T> CreatePool<T>(Action<T> initialize, Action<T> deinitialize)
		where T : class, new()
	{
		return new DefaultTypePool<T>(initialize, deinitialize);
	}
}
