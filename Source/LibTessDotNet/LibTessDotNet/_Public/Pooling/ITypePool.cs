﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

public interface ITypePool
{
	Type PooledType { get; }

	int Created { get; }

	int Available { get; }

	int Active { get; }

	void Create(int count);
}
