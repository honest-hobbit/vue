﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

public interface ITypePool<T> : ITypePool
{
	T Rent();

	void Return(T obj);
}
