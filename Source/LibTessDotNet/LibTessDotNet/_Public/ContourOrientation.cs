﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

public enum ContourOrientation
{
	Original,
	Clockwise,
	CounterClockwise
}
