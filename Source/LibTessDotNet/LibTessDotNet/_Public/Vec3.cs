﻿#if DOUBLE
namespace LibTessDotNet.Double;
#else
namespace LibTessDotNet;
#endif

public struct Vec3
{
	public readonly static Vec3 Zero = new Vec3();

	public Real X, Y, Z;

	public Real this[int index]
	{
		get
		{
			if (index == 0) return X;
			if (index == 1) return Y;
			if (index == 2) return Z;
			throw new IndexOutOfRangeException();
		}
		set
		{
			if (index == 0) X = value;
			else if (index == 1) Y = value;
			else if (index == 2) Z = value;
			else throw new IndexOutOfRangeException();
		}
	}

	public Vec3(Real x, Real y, Real z)
	{
		X = x;
		Y = y;
		Z = z;
	}

	public static void Sub(ref Vec3 lhs, ref Vec3 rhs, out Vec3 result)
	{
		result.X = lhs.X - rhs.X;
		result.Y = lhs.Y - rhs.Y;
		result.Z = lhs.Z - rhs.Z;
	}

	public static void Neg(ref Vec3 v)
	{
		v.X = -v.X;
		v.Y = -v.Y;
		v.Z = -v.Z;
	}

	public static void Dot(ref Vec3 u, ref Vec3 v, out Real dot)
	{
		dot = u.X * v.X + u.Y * v.Y + u.Z * v.Z;
	}

	public static void Normalize(ref Vec3 v)
	{
		var len = v.X * v.X + v.Y * v.Y + v.Z * v.Z;
		Debug.Assert(len >= 0.0f);
		len = 1.0f / (Real)Math.Sqrt(len);
		v.X *= len;
		v.Y *= len;
		v.Z *= len;
	}

	public static int LongAxis(ref Vec3 v)
	{
		int i = 0;
		if (Math.Abs(v.Y) > Math.Abs(v.X)) i = 1;
		if (Math.Abs(v.Z) > Math.Abs(i == 0 ? v.X : v.Y)) i = 2;
		return i;
	}

	public override string ToString()
	{
		return string.Format("{0}, {1}, {2}", X, Y, Z);
	}
}
