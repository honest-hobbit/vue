﻿namespace TessBed;

public static class UnitTests
{
	private static DataLoader _loader = new DataLoader();

	public static string TestDataPath = Path.Combine("..", "..", "TessBed", "TestData");

	[Fact]
	public static void Tesselate_WithSingleTriangle_ProducesSameTriangle()
	{
		string data = "0,0,0\n0,1,0\n1,1,0";
		var indices = new List<int>();
		var expectedIndices = new int[] { 0, 1, 2 };
		using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
		{
			var pset = DataLoader.LoadDat(stream);
			var pool = new Pools();
			var tess = new Tess(pool);

			PolyConvert.ToTess(pset, tess);
			tess.Tessellate(WindingRule.EvenOdd, ElementType.Polygons, 3);

			indices.Clear();
			for (int i = 0; i < tess.ElementCount; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					int index = tess.Elements[i * 3 + j];
					indices.Add(index);
				}
			}

			Assert.Equal(expectedIndices, indices.ToArray());
			AssetPoolCounts(pool);
		}
	}

	[Fact]
	// From https://github.com/memononen/libtess2/issues/14
	public static void Tesselate_WithThinQuad_DoesNotCrash()
	{
		string data = "9.5,7.5,-0.5\n9.5,2,-0.5\n9.5,2,-0.4999999701976776123\n9.5,7.5,-0.4999999701976776123";
		using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
		{
			var pset = DataLoader.LoadDat(stream);
			var tess = new Tess();
			PolyConvert.ToTess(pset, tess);
			tess.Tessellate(WindingRule.EvenOdd, ElementType.Polygons, 3);
		}
	}

	[Fact]
	// From https://github.com/speps/LibTessDotNet/issues/1
	public static void Tesselate_WithIssue1Quad_ReturnsSameResultAsLibtess2()
	{
		string data = "50,50\n300,50\n300,200\n50,200";
		var indices = new List<int>();
		var expectedIndices = new int[] { 0, 1, 2, 1, 0, 3 };
		using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
		{
			var pset = DataLoader.LoadDat(stream);
			var tess = new Tess();
			PolyConvert.ToTess(pset, tess);
			tess.Tessellate(WindingRule.EvenOdd, ElementType.Polygons, 3);
			indices.Clear();
			for (int i = 0; i < tess.ElementCount; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					int index = tess.Elements[i * 3 + j];
					indices.Add(index);
				}
			}

			Assert.Equal(expectedIndices, indices.ToArray());
		}
	}

	[Fact]
	// From https://github.com/speps/LibTessDotNet/issues/1
	public static void Tesselate_WithNoEmptyPolygonsTrue_RemovesEmptyPolygons()
	{
		string data = "2,0,4\n2,0,2\n4,0,2\n4,0,0\n0,0,0\n0,0,4";
		var indices = new List<int>();
		var expectedIndices = new int[] { 0, 1, 2, 2, 3, 4, 3, 1, 5 };
		using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
		{
			var pset = DataLoader.LoadDat(stream);
			var tess = new Tess();
			PolyConvert.ToTess(pset, tess);
			tess.NoEmptyPolygons = true;
			tess.Tessellate(WindingRule.EvenOdd, ElementType.Polygons, 3);
			indices.Clear();
			for (int i = 0; i < tess.ElementCount; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					int index = tess.Elements[i * 3 + j];
					indices.Add(index);
				}
			}

			Assert.Equal(expectedIndices, indices.ToArray());
		}
	}

	[Fact]
	public static void Tesselate_CalledTwiceOnSameInstance_DoesNotCrash()
	{
		string data = "0,0,0\n0,1,0\n1,1,0";
		var indices = new List<int>();
		var expectedIndices = new int[] { 0, 1, 2 };
		using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
		{
			var pset = DataLoader.LoadDat(stream);
			var tess = new Tess();

			// Call once
			PolyConvert.ToTess(pset, tess);
			tess.Tessellate(WindingRule.EvenOdd, ElementType.Polygons, 3);

			indices.Clear();
			for (int i = 0; i < tess.ElementCount; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					int index = tess.Elements[i * 3 + j];
					indices.Add(index);
				}
			}

			Assert.Equal(expectedIndices, indices.ToArray());

			// Call twice
			PolyConvert.ToTess(pset, tess);
			tess.Tessellate(WindingRule.EvenOdd, ElementType.Polygons, 3);

			indices.Clear();
			for (int i = 0; i < tess.ElementCount; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					int index = tess.Elements[i * 3 + j];
					indices.Add(index);
				}
			}

			Assert.Equal(expectedIndices, indices.ToArray());
		}
	}

	[Theory]
	[MemberData(nameof(GetTestCaseData))]
	public static void Tessellate_WithAsset_ReturnsExpectedTriangulation(TestCaseData data)
	{
		var pset = data.Asset.Polygons;
		var pool = new Pools();
		var tess = new Tess(pool);
		PolyConvert.ToTess(pset, tess);
		tess.Tessellate(data.Winding, ElementType.Polygons, data.ElementSize);

		var resourceName = Assembly.GetExecutingAssembly().GetName().Name + ".TestData." + data.Asset.Name + ".testdat";
		var testData = ParseTestData(data.Winding, data.ElementSize, Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName));

		Assert.NotNull(testData);
		Assert.Equal(testData.ElementSize, data.ElementSize);

		var indices = new List<int>();
		for (int i = 0; i < tess.ElementCount; i++)
		{
			for (int j = 0; j < data.ElementSize; j++)
			{
				int index = tess.Elements[i * data.ElementSize + j];
				indices.Add(index);
			}
		}

		Assert.Equal(testData.Indices, indices.ToArray());
		AssetPoolCounts(pool);
	}

	public static TestData ParseTestData(WindingRule winding, int elementSize, Stream resourceStream)
	{
		var lines = new List<string>();

		bool found = false;
		using (var stream = new StreamReader(resourceStream))
		{
			string line;
			while ((line = stream.ReadLine()) != null)
			{
				line = line.Trim();
				if (found && string.IsNullOrEmpty(line))
				{
					break;
				}
				if (found)
				{
					lines.Add(line);
				}
				var parts = line.Split(' ');
				if (parts.FirstOrDefault() == winding.ToString() && Int32.Parse(parts.LastOrDefault()) == elementSize)
				{
					found = true;
				}
			}
		}
		var indices = new List<int>();
		foreach (var line in lines)
		{
			var parts = line.Split(' ');
			if (parts.Length != elementSize)
			{
				continue;
			}
			foreach (var part in parts)
			{
				indices.Add(Int32.Parse(part));
			}
		}
		if (found)
		{
			return new TestData()
			{
				ElementSize = elementSize,
				Indices = indices.ToArray()
			};
		}
		return null;
	}

	public static IEnumerable<object[]> GetTestCaseData()
	{
		foreach (WindingRule winding in Enum.GetValues(typeof(WindingRule)))
		{
			foreach (var asset in _loader.Assets)
			{
				yield return new object[]
				{
					new TestCaseData { Asset = asset, Winding = winding, ElementSize = 3 },
				};
			}
		}
	}

	internal static void GenerateTestData()
	{
		foreach (var asset in _loader.Assets)
		{
			var pset = asset.Polygons;

			var lines = new List<string>();
			var indices = new List<int>();

			foreach (WindingRule winding in Enum.GetValues(typeof(WindingRule)))
			{
				var tess = new Tess();
				PolyConvert.ToTess(pset, tess);
				tess.Tessellate(winding, ElementType.Polygons, 3);

				lines.Add(string.Format("{0} {1}", winding, 3));
				for (int i = 0; i < tess.ElementCount; i++)
				{
					indices.Clear();
					for (int j = 0; j < 3; j++)
					{
						int index = tess.Elements[i * 3 + j];
						indices.Add(index);
					}
					lines.Add(string.Join(" ", indices));
				}
				lines.Add("");
			}

			File.WriteAllLines(Path.Combine(TestDataPath, asset.Name + ".testdat"), lines);
		}
	}

	private static void AssetPoolCounts(Pools pools)
	{
		Assert.NotNull(pools);
		Assert.Equal(pools.MeshPool.Created, pools.MeshPool.Available);
		Assert.Equal(pools.VertexPool.Created, pools.VertexPool.Available);
		Assert.Equal(pools.FacePool.Created, pools.FacePool.Available);
		Assert.Equal(pools.EdgePool.Created, pools.EdgePool.Available);
		Assert.Equal(pools.RegionPool.Created, pools.RegionPool.Available);
		Assert.Equal(pools.NodePool.Created, pools.NodePool.Available);
	}
}
