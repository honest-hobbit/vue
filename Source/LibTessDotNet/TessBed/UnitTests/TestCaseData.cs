﻿namespace TessBed;

public struct TestCaseData
{
	public DataAsset Asset;

	public WindingRule Winding;

	public int ElementSize;

	public override string ToString()
	{
		return string.Format("{0}, {1}, {2}", Winding, Asset.Name, ElementSize);
	}
}
