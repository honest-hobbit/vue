﻿namespace TessBed;

public class Polygon : List<PolygonPoint>
{
	public ContourOrientation Orientation = ContourOrientation.Original;

	public Polygon()
	{

	}

	public Polygon(ICollection<PolygonPoint> points)
		: base(points)
	{

	}
}
