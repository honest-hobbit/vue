﻿namespace TessBed;

[DebuggerDisplay("{X}, {Y}, {Z}")]
public struct PolygonPoint
{
	public float X, Y, Z;
	public Color Color;
}
