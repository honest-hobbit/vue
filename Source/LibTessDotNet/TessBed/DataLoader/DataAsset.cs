﻿namespace TessBed;

public class DataAsset
{
	public string Name;


	public PolygonSet Polygons;

	public override string ToString() => Name;
}
