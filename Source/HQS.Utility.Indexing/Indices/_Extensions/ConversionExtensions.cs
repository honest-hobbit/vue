﻿namespace HQS.Utility.Indexing.Indices;

using HQS.Utility.Numerics;

public static class ConversionExtensions
{
	public static Vector2 ToVector(this Index2D index) => new Vector2(index.X, index.Y);

	public static Vector3 ToVector(this Index3D index) => new Vector3(index.X, index.Y, index.Z);

	public static Vector4 ToVector(this Index4D index) => new Vector4(index.X, index.Y, index.Z, index.W);

	public static Int2 ToInt(this Index2D index) => new Int2(index.X, index.Y);

	public static Int3 ToInt(this Index3D index) => new Int3(index.X, index.Y, index.Z);

	public static Int4 ToInt(this Index4D index) => new Int4(index.X, index.Y, index.Z, index.W);

	public static Index2D ToIndex(this Int2 index) => new Index2D(index.X, index.Y);

	public static Index3D ToIndex(this Int3 index) => new Index3D(index.X, index.Y, index.Z);

	public static Index4D ToIndex(this Int4 index) => new Index4D(index.X, index.Y, index.Z, index.W);
}
