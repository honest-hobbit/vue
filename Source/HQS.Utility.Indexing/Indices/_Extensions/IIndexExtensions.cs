﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// Provides extension methods for use with implementations of the <see cref="IIndex{TIndex}"/> interface.
/// </summary>
public static class IIndexExtensions
{
	public static IEnumerable<int> Dimensions<TIndex>(this TIndex index)
		where TIndex : struct, IIndex<TIndex> => index.Select(pair => pair.Dimension);

	public static IEnumerable<int> Coordinates<TIndex>(this TIndex index)
		where TIndex : struct, IIndex<TIndex> => index.Select(pair => pair.Coordinate);

	public static int ToMortonCode<TIndex>(this TIndex index)
		where TIndex : struct, IIndex<TIndex> => index.MortonCurve.EncodeIndex(index);
}
