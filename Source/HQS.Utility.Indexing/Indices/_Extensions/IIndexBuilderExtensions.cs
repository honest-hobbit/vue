﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// Provides extension methods for <see cref="IIndexBuilder{TIndex}"/>.
/// </summary>
public static class IIndexBuilderExtensions
{
	public static IIndexBuilder<TIndex> SetTo<TIndex>(this IIndexBuilder<TIndex> builder, TIndex index)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(builder != null);
		Debug.Assert(builder.Rank == index.Rank);

		for (int dimension = 0; dimension < builder.Rank; dimension++)
		{
			builder[dimension] = index[dimension];
		}

		return builder;
	}

	public static IIndexBuilder<TIndex> SetAllTo<TIndex>(this IIndexBuilder<TIndex> builder, int value)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(builder != null);

		for (int dimension = 0; dimension < builder.Rank; dimension++)
		{
			builder[dimension] = value;
		}

		return builder;
	}
}
