﻿namespace HQS.Utility.Indexing.Indices;

public class IndexBuilder4D : IIndexBuilder<Index4D>
{
	/// <summary>
	/// Gets or sets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; set; }

	/// <summary>
	/// Gets or sets the Y coordinate.
	/// </summary>
	/// <value>
	/// The Y coordinate.
	/// </value>
	public int Y { get; set; }

	/// <summary>
	/// Gets or sets the Z coordinate.
	/// </summary>
	/// <value>
	/// The Z coordinate.
	/// </value>
	public int Z { get; set; }

	/// <summary>
	/// Gets or sets the W coordinate.
	/// </summary>
	/// <value>
	/// The W coordinate.
	/// </value>
	public int W { get; set; }

	/// <inheritdoc />
	public int Rank => 4;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			return dimension switch
			{
				AxisValue.X => this.X,
				AxisValue.Y => this.Y,
				AxisValue.Z => this.Z,
				AxisValue.W => this.W,
				_ => throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage),
			};
		}

		set
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			switch (dimension)
			{
				case AxisValue.X: this.X = value; break;
				case AxisValue.Y: this.Y = value; break;
				case AxisValue.Z: this.Z = value; break;
				case AxisValue.W: this.W = value; break;
				default: throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage);
			}
		}
	}

	/// <inheritdoc />
	public override string ToString() => this.ToIndex().ToString();

	/// <inheritdoc />
	public Index4D ToIndex() => new Index4D(this.X, this.Y, this.Z, this.W);

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
		yield return new DimensionCoordinatePair(AxisValue.Y, this.Y);
		yield return new DimensionCoordinatePair(AxisValue.Z, this.Z);
		yield return new DimensionCoordinatePair(AxisValue.W, this.W);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
