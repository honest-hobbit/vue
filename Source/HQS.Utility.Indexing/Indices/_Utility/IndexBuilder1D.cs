﻿namespace HQS.Utility.Indexing.Indices;

public class IndexBuilder1D : IIndexBuilder<Index1D>
{
	/// <summary>
	/// Gets or sets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; set; }

	/// <inheritdoc />
	public int Rank => 1;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			return this.X;
		}

		set
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			this.X = value;
		}
	}

	/// <inheritdoc />
	public override string ToString() => this.ToIndex().ToString();

	/// <inheritdoc />
	public Index1D ToIndex() => new Index1D(this.X);

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
