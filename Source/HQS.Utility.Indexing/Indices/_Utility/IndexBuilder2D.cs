﻿namespace HQS.Utility.Indexing.Indices;

public class IndexBuilder2D : IIndexBuilder<Index2D>
{
	/// <summary>
	/// Gets or sets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; set; }

	/// <summary>
	/// Gets or sets the Y coordinate.
	/// </summary>
	/// <value>
	/// The Y coordinate.
	/// </value>
	public int Y { get; set; }

	/// <inheritdoc />
	public int Rank => 2;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			return dimension switch
			{
				AxisValue.X => this.X,
				AxisValue.Y => this.Y,
				_ => throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage),
			};
		}

		set
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			switch (dimension)
			{
				case AxisValue.X: this.X = value; break;
				case AxisValue.Y: this.Y = value; break;
				default: throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage);
			}
		}
	}

	/// <inheritdoc />
	public override string ToString() => this.ToIndex().ToString();

	/// <inheritdoc />
	public Index2D ToIndex() => new Index2D(this.X, this.Y);

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
		yield return new DimensionCoordinatePair(AxisValue.Y, this.Y);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
