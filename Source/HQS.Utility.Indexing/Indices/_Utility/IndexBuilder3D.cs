﻿namespace HQS.Utility.Indexing.Indices;

public class IndexBuilder3D : IIndexBuilder<Index3D>
{
	/// <summary>
	/// Gets or sets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; set; }

	/// <summary>
	/// Gets or sets the Y coordinate.
	/// </summary>
	/// <value>
	/// The Y coordinate.
	/// </value>
	public int Y { get; set; }

	/// <summary>
	/// Gets or sets the Z coordinate.
	/// </summary>
	/// <value>
	/// The Z coordinate.
	/// </value>
	public int Z { get; set; }

	/// <inheritdoc />
	public int Rank => 3;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			return dimension switch
			{
				AxisValue.X => this.X,
				AxisValue.Y => this.Y,
				AxisValue.Z => this.Z,
				_ => throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage),
			};
		}

		set
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			switch (dimension)
			{
				case AxisValue.X: this.X = value; break;
				case AxisValue.Y: this.Y = value; break;
				case AxisValue.Z: this.Z = value; break;
				default: throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage);
			}
		}
	}

	/// <inheritdoc />
	public override string ToString() => this.ToIndex().ToString();

	/// <inheritdoc />
	public Index3D ToIndex() => new Index3D(this.X, this.Y, this.Z);

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
		yield return new DimensionCoordinatePair(AxisValue.Y, this.Y);
		yield return new DimensionCoordinatePair(AxisValue.Z, this.Z);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
