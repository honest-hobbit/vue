﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// Provides a collection of helper methods for use with indices.
/// </summary>
public static class IndexUtility
{
	/// <summary>
	/// Gets the justification message for the unreachable code exceptions.
	/// </summary>
	/// <value>
	/// The unreachable code exception message.
	/// </value>
	internal static string InvalidDimensionExceptionMessage => "Contract requires dimension be within the interval [0, Rank)";

	public static int Rank<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Constants<TIndex>.Rank;

	public static Curves.ISpaceFillingCurve<TIndex> MortonEncoder<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Constants<TIndex>.MortonEncoder;

	public static TIndex Zero<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Constants<TIndex>.Zero;

	public static TIndex One<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Constants<TIndex>.One;

	public static TIndex NegativeOne<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Constants<TIndex>.NegativeOne;

	public static TIndex MinValue<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Constants<TIndex>.MinValue;

	public static TIndex MaxValue<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Constants<TIndex>.MaxValue;

	public static TIndex Create<TIndex>(int value)
		where TIndex : struct, IIndex<TIndex> => CreateBuilder<TIndex>().SetAllTo(value).ToIndex();

	public static IIndexBuilder<TIndex> CreateBuilder<TIndex>()
		where TIndex : struct, IIndex<TIndex> => Zero<TIndex>().ToBuilder();

	public static bool IsIn<TIndex>(TIndex index, TIndex min, TIndex max)
		where TIndex : struct, IIndex<TIndex>
	{
		IIndexContracts.IsIn(min, max);
		Debug.Assert(index.Rank == min.Rank);
		Debug.Assert(min.Rank == max.Rank);

		for (int dimension = 0; dimension < index.Rank; dimension++)
		{
			var coordinate = index[dimension];
			if (coordinate < min[dimension] || coordinate > max[dimension])
			{
				return false;
			}
		}

		return true;
	}

	public static TIndex Midpoint<TIndex>(TIndex index1, TIndex index2, bool roundUp = false)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(index1.Rank == index2.Rank);

		var builder = index1.ToBuilder();
		for (int dimension = 0; dimension < builder.Rank; dimension++)
		{
			builder[dimension] = MathUtility.IntegerMidpoint(index1[dimension], index2[dimension], roundUp);
		}

		return builder.ToIndex();
	}

	public static TIndex Divide<TIndex>(TIndex index, TIndex divisor, out TIndex remainder)
		where TIndex : struct, IIndex<TIndex>
	{
		IIndexContracts.Divide(divisor);
		Debug.Assert(index.Rank == divisor.Rank);

		var quotientBuilder = index.ToBuilder();
		var remainderBuilder = index.ToBuilder();

		for (int dimension = 0; dimension < quotientBuilder.Rank; dimension++)
		{
			quotientBuilder[dimension] = Math.DivRem(
				quotientBuilder[dimension], divisor[dimension], out var intRemainder);
			remainderBuilder[dimension] = intRemainder;
		}

		remainder = remainderBuilder.ToIndex();
		return quotientBuilder.ToIndex();
	}

	public static TIndex DivideRoundUp<TIndex>(TIndex index, TIndex divisor)
		where TIndex : struct, IIndex<TIndex>
	{
		IIndexContracts.DivideRoundUp(index, divisor);
		Debug.Assert(index.Rank == divisor.Rank);

		var builder = index.ToBuilder();
		for (int dimension = 0; dimension < builder.Rank; dimension++)
		{
			builder[dimension] = MathUtility.DivideRoundUp(builder[dimension], divisor[dimension]);
		}

		return builder.ToIndex();
	}

	public static IEnumerable<TIndex> Range<TIndex>(TIndex startIndex, TIndex dimensions)
		where TIndex : struct, IIndex<TIndex>
	{
		IIndexContracts.Range(dimensions);
		Debug.Assert(startIndex.Rank == dimensions.Rank);

		return YieldRange();
		IEnumerable<TIndex> YieldRange()
		{
			if (dimensions.Coordinates().Any(coordinate => coordinate == 0))
			{
				yield break;
			}

			var endIndex = Operator<TIndex>.Add(startIndex, dimensions);
			var builder = startIndex.ToBuilder();

		Repeat:
			var max = endIndex[0];
			for (int count = startIndex[0]; count < max; count++)
			{
				builder[0] = count;
				yield return builder.ToIndex();
			}

			builder[0] = startIndex[0];
			for (int dimension = 1; dimension < builder.Rank; dimension++)
			{
				var next = builder[dimension] + 1;
				if (next < endIndex[dimension])
				{
					builder[dimension] = next;
					goto Repeat;
				}
				else
				{
					builder[dimension] = startIndex[dimension];
				}
			}
		}
	}

	private static class Constants<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
		[SuppressMessage("ErrorProne.Net", "ERP022", Justification = "Intentionally discarding all exceptions.")]
		static Constants()
		{
			try
			{
				Zero = Operator<TIndex>.Zero;
				Rank = Zero.Rank;
				MortonEncoder = Zero.MortonCurve;

				var builder = Zero.ToBuilder();
				One = builder.SetAllTo(1).ToIndex();
				NegativeOne = builder.SetAllTo(-1).ToIndex();
				MinValue = builder.SetAllTo(int.MinValue).ToIndex();
				MaxValue = builder.SetAllTo(int.MaxValue).ToIndex();
			}
			catch (Exception)
			{
				Rank = 0;
				MortonEncoder = null;
				Zero = default;
				One = default;
				NegativeOne = default;
				MinValue = default;
				MaxValue = default;
			}
		}

		public static int Rank { get; }

		public static Curves.ISpaceFillingCurve<TIndex> MortonEncoder { get; }

		public static TIndex Zero { get; }

		public static TIndex One { get; }

		public static TIndex NegativeOne { get; }

		public static TIndex MinValue { get; }

		public static TIndex MaxValue { get; }
	}
}
