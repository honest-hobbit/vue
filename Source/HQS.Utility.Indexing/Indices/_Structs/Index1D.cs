﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// A three dimensional integer index for accessing arrays or grid based structures.
/// </summary>
public readonly struct Index1D : IIndex<Index1D>
{
	#region Constructors

	/// <summary>
	/// Initializes a new instance of the <see cref="Index1D"/> struct.
	/// </summary>
	/// <param name="x">The x index.</param>
	public Index1D(int x)
	{
		this.X = x;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Index1D" /> struct.
	/// </summary>
	/// <param name="index">The index whose coordinates to copy.</param>
	public Index1D(Index1D index)
	{
		this.X = index.X;
	}

	#endregion

	#region Properties/Indexer

	/// <summary>
	/// Gets the zero index.
	/// </summary>
	/// <value>
	/// The zero index.
	/// </value>
	public static Index1D Zero => new Index1D(0);

	public static Index1D UnitX => new Index1D(1);

	/// <summary>
	/// Gets the minimum index.
	/// </summary>
	/// <value>
	/// The minimum index.
	/// </value>
	public static Index1D MinValue => new Index1D(int.MinValue);

	/// <summary>
	/// Gets the maximum index.
	/// </summary>
	/// <value>
	/// The maximum index.
	/// </value>
	public static Index1D MaxValue => new Index1D(int.MaxValue);

	/// <inheritdoc />
	public Curves.ISpaceFillingCurve<Index1D> MortonCurve => Curves.MortonCurve.Index1D;

	/// <summary>
	/// Gets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; }

	/// <inheritdoc />
	public int Rank => 1;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			ICoordinatesContracts.Indexer(this, dimension);

			return this.X;
		}
	}

	#endregion

	#region Operators

	public static Index1D operator +(Index1D value) => value;

	public static Index1D operator -(Index1D value) => new Index1D(-value.X);

	public static Index1D operator +(Index1D lhs, Index1D rhs) => new Index1D(lhs.X + rhs.X);

	public static Index1D operator -(Index1D lhs, Index1D rhs) => new Index1D(lhs.X - rhs.X);

	public static Index1D operator *(Index1D lhs, Index1D rhs) => new Index1D(lhs.X * rhs.X);

	public static Index1D operator *(Index1D index, int scalar) => new Index1D(index.X * scalar);

	public static Index1D operator *(int scalar, Index1D index) => new Index1D(index.X * scalar);

	public static Index1D operator /(Index1D lhs, Index1D rhs) => new Index1D(lhs.X / rhs.X);

	public static Index1D operator /(Index1D index, int scalar) => new Index1D(index.X / scalar);

	public static Index1D operator %(Index1D lhs, Index1D rhs) => new Index1D(lhs.X % rhs.X);

	public static Index1D operator %(Index1D lhs, int scalar) => new Index1D(lhs.X % scalar);

	public static Index1D operator &(Index1D index, Index1D mask) => new Index1D(index.X & mask.X);

	public static Index1D operator &(Index1D index, int mask) => new Index1D(index.X & mask);

	public static Index1D operator |(Index1D index, Index1D mask) => new Index1D(index.X | mask.X);

	public static Index1D operator |(Index1D index, int mask) => new Index1D(index.X | mask);

	public static Index1D operator ^(Index1D index, Index1D mask) => new Index1D(index.X ^ mask.X);

	public static Index1D operator ^(Index1D index, int mask) => new Index1D(index.X ^ mask);

	public static Index1D operator ~(Index1D index) => new Index1D(~index.X);

	public static Index1D operator <<(Index1D index, int shift) => new Index1D(index.X << shift);

	public static Index1D operator >>(Index1D index, int shift) => new Index1D(index.X >> shift);

	public static bool operator ==(Index1D lhs, Index1D rhs) => lhs.Equals(rhs);

	public static bool operator !=(Index1D lhs, Index1D rhs) => !lhs.Equals(rhs);

	#endregion

	#region Methods

	/// <inheritdoc />
	public bool Equals(Index1D other) => this.X == other.X;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.X.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => $"({this.X})";

	/// <inheritdoc />
	public IIndexBuilder<Index1D> ToBuilder() => new IndexBuilder1D()
	{
		X = this.X,
	};

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	public bool IsIn(Index1D min, Index1D max)
	{
		IIndexContracts.IsIn(min, max);

		return this.X >= min.X && this.X <= max.X;
	}

	/// <inheritdoc />
	public Index1D Clamp(Index1D min, Index1D max) => new Index1D(this.X.Clamp(min.X, max.X));

	/// <inheritdoc />
	public Index1D ClampUpper(Index1D max) => new Index1D(this.X.ClampUpper(max.X));

	/// <inheritdoc />
	public Index1D ClampLower(Index1D min) => new Index1D(this.X.ClampLower(min.X));

	/// <inheritdoc />
	public Index1D Midpoint(Index1D index, bool roundUp = false) =>
		new Index1D(MathUtility.IntegerMidpoint(this.X, index.X, roundUp));

	/// <inheritdoc />
	public Index1D Divide(Index1D divisor, out Index1D remainder)
	{
		IIndexContracts.Divide(divisor);

		var result = new Index1D(Math.DivRem(this.X, divisor.X, out int remainderX));
		remainder = new Index1D(remainderX);
		return result;
	}

	/// <inheritdoc />
	public Index1D DivideRoundUp(Index1D divisor)
	{
		IIndexContracts.DivideRoundUp(this, divisor);

		return new Index1D(MathUtility.DivideRoundUp(this.X, divisor.X));
	}

	/// <inheritdoc />
	public IEnumerable<Index1D> Range(Index1D dimensions)
	{
		IIndexContracts.Range(dimensions);

		var lastIndex = this + dimensions;
		for (int iX = this.X; iX < lastIndex.X; iX++)
		{
			yield return new Index1D(iX);
		}
	}

	/// <summary>
	/// Creates a new array whose dimensions are taken from this index.
	/// </summary>
	/// <typeparam name="T">The type of the value of the array.</typeparam>
	/// <returns>The new array.</returns>
	public T[] CreateArray<T>()
	{
		Debug.Assert(this.Coordinates().All(value => value >= 0));

		return new T[this.X];
	}

	#endregion
}
