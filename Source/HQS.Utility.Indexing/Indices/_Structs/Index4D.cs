﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// A four dimensional integer index for accessing arrays or grid based structures.
/// </summary>
public readonly struct Index4D : IIndex<Index4D>
{
	#region Constructors

	/// <summary>
	/// Initializes a new instance of the <see cref="Index4D"/> struct.
	/// </summary>
	/// <param name="number">The number to assign to each index.</param>
	public Index4D(int number)
	{
		this.X = number;
		this.Y = number;
		this.Z = number;
		this.W = number;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Index4D"/> struct.
	/// </summary>
	/// <param name="x">The x index.</param>
	/// <param name="y">The y index.</param>
	/// <param name="z">The z index.</param>
	/// <param name="w">The w index.</param>
	public Index4D(int x, int y, int z, int w)
	{
		this.X = x;
		this.Y = y;
		this.Z = z;
		this.W = w;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Index4D" /> struct.
	/// </summary>
	/// <param name="index">The index whose coordinates to copy.</param>
	public Index4D(Index4D index)
	{
		this.X = index.X;
		this.Y = index.Y;
		this.Z = index.Z;
		this.W = index.W;
	}

	#endregion

	#region Properties/Indexer

	/// <summary>
	/// Gets the zero index.
	/// </summary>
	/// <value>
	/// The zero index.
	/// </value>
	public static Index4D Zero => new Index4D(0);

	public static Index4D UnitX => new Index4D(1, 0, 0, 0);

	public static Index4D UnitY => new Index4D(0, 1, 0, 0);

	public static Index4D UnitZ => new Index4D(0, 0, 1, 0);

	public static Index4D UnitW => new Index4D(0, 0, 0, 1);

	/// <summary>
	/// Gets the minimum index.
	/// </summary>
	/// <value>
	/// The minimum index.
	/// </value>
	public static Index4D MinValue => new Index4D(int.MinValue);

	/// <summary>
	/// Gets the maximum index.
	/// </summary>
	/// <value>
	/// The maximum index.
	/// </value>
	public static Index4D MaxValue => new Index4D(int.MaxValue);

	/// <inheritdoc />
	public Curves.ISpaceFillingCurve<Index4D> MortonCurve => Curves.MortonCurve.Index4D;

	/// <summary>
	/// Gets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; }

	/// <summary>
	/// Gets the Y coordinate.
	/// </summary>
	/// <value>
	/// The Y coordinate.
	/// </value>
	public int Y { get; }

	/// <summary>
	/// Gets the Z coordinate.
	/// </summary>
	/// <value>
	/// The Z coordinate.
	/// </value>
	public int Z { get; }

	/// <summary>
	/// Gets the W coordinate.
	/// </summary>
	/// <value>
	/// The W coordinate.
	/// </value>
	public int W { get; }

	/// <inheritdoc />
	public int Rank => 4;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			ICoordinatesContracts.Indexer(this, dimension);

			return dimension switch
			{
				AxisValue.X => this.X,
				AxisValue.Y => this.Y,
				AxisValue.Z => this.Z,
				AxisValue.W => this.W,
				_ => throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage),
			};
		}
	}

	#endregion

	#region Operators

	public static Index4D operator +(Index4D value) => value;

	public static Index4D operator -(Index4D value) => new Index4D(-value.X, -value.Y, -value.Z, -value.W);

	public static Index4D operator +(Index4D lhs, Index4D rhs) =>
		new Index4D(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z, lhs.W + rhs.W);

	public static Index4D operator -(Index4D lhs, Index4D rhs) =>
		new Index4D(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z, lhs.W - rhs.W);

	public static Index4D operator *(Index4D lhs, Index4D rhs) =>
		new Index4D(lhs.X * rhs.X, lhs.Y * rhs.Y, lhs.Z * rhs.Z, lhs.W * rhs.W);

	public static Index4D operator *(Index4D index, int scalar) =>
		new Index4D(index.X * scalar, index.Y * scalar, index.Z * scalar, index.W * scalar);

	public static Index4D operator *(int scalar, Index4D index) =>
		new Index4D(index.X * scalar, index.Y * scalar, index.Z * scalar, index.W * scalar);

	public static Index4D operator /(Index4D lhs, Index4D rhs) =>
		new Index4D(lhs.X / rhs.X, lhs.Y / rhs.Y, lhs.Z / rhs.Z, lhs.W / rhs.W);

	public static Index4D operator /(Index4D index, int scalar) =>
		new Index4D(index.X / scalar, index.Y / scalar, index.Z / scalar, index.W / scalar);

	public static Index4D operator %(Index4D lhs, Index4D rhs) =>
		new Index4D(lhs.X % rhs.X, lhs.Y % rhs.Y, lhs.Z % rhs.Z, lhs.W % rhs.W);

	public static Index4D operator %(Index4D lhs, int scalar) =>
		new Index4D(lhs.X % scalar, lhs.Y % scalar, lhs.Z % scalar, lhs.W % scalar);

	public static Index4D operator &(Index4D index, Index4D mask) =>
		new Index4D(index.X & mask.X, index.Y & mask.Y, index.Z & mask.Z, index.W & mask.W);

	public static Index4D operator &(Index4D index, int mask) =>
		new Index4D(index.X & mask, index.Y & mask, index.Z & mask, index.W & mask);

	public static Index4D operator |(Index4D index, Index4D mask) =>
		new Index4D(index.X | mask.X, index.Y | mask.Y, index.Z | mask.Z, index.W | mask.W);

	public static Index4D operator |(Index4D index, int mask) =>
		new Index4D(index.X | mask, index.Y | mask, index.Z | mask, index.W | mask);

	public static Index4D operator ^(Index4D index, Index4D mask) =>
		new Index4D(index.X ^ mask.X, index.Y ^ mask.Y, index.Z ^ mask.Z, index.W ^ mask.W);

	public static Index4D operator ^(Index4D index, int mask) =>
		new Index4D(index.X ^ mask, index.Y ^ mask, index.Z ^ mask, index.W ^ mask);

	public static Index4D operator ~(Index4D index) => new Index4D(~index.X, ~index.Y, ~index.Z, ~index.W);

	public static Index4D operator <<(Index4D index, int shift) =>
		new Index4D(index.X << shift, index.Y << shift, index.Z << shift, index.W << shift);

	public static Index4D operator >>(Index4D index, int shift) =>
		new Index4D(index.X >> shift, index.Y >> shift, index.Z >> shift, index.W >> shift);

	public static bool operator ==(Index4D lhs, Index4D rhs) => lhs.Equals(rhs);

	public static bool operator !=(Index4D lhs, Index4D rhs) => !lhs.Equals(rhs);

	#endregion

	#region Methods

	/// <inheritdoc />
	public bool Equals(Index4D other) =>
		this.X == other.X && this.Y == other.Y && this.Z == other.Z && this.W == other.W;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.X, this.Y, this.Z, this.W);

	/// <inheritdoc />
	public override string ToString() => $"({this.X}, {this.Y}, {this.Z}, {this.W})";

	/// <inheritdoc />
	public IIndexBuilder<Index4D> ToBuilder() => new IndexBuilder4D()
	{
		X = this.X,
		Y = this.Y,
		Z = this.Z,
		W = this.W,
	};

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
		yield return new DimensionCoordinatePair(AxisValue.Y, this.Y);
		yield return new DimensionCoordinatePair(AxisValue.Z, this.Z);
		yield return new DimensionCoordinatePair(AxisValue.W, this.W);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	public bool IsIn(Index4D min, Index4D max)
	{
		IIndexContracts.IsIn(min, max);

		return this.X >= min.X && this.X <= max.X
			&& this.Y >= min.Y && this.Y <= max.Y
			&& this.Z >= min.Z && this.Z <= max.Z
			&& this.W >= min.W && this.W <= max.W;
	}

	/// <inheritdoc />
	public Index4D Clamp(Index4D min, Index4D max) =>
		new Index4D(this.X.Clamp(min.X, max.X), this.Y.Clamp(min.Y, max.Y), this.Z.Clamp(min.Z, max.Z), this.W.Clamp(min.W, max.W));

	/// <inheritdoc />
	public Index4D ClampUpper(Index4D max) =>
		new Index4D(this.X.ClampUpper(max.X), this.Y.ClampUpper(max.Y), this.Z.ClampUpper(max.Z), this.W.ClampUpper(max.W));

	/// <inheritdoc />
	public Index4D ClampLower(Index4D min) =>
		new Index4D(this.X.ClampLower(min.X), this.Y.ClampLower(min.Y), this.Z.ClampLower(min.Z), this.W.ClampLower(min.W));

	/// <inheritdoc />
	public Index4D Midpoint(Index4D index, bool roundUp = false) => new Index4D(
		MathUtility.IntegerMidpoint(this.X, index.X, roundUp),
		MathUtility.IntegerMidpoint(this.Y, index.Y, roundUp),
		MathUtility.IntegerMidpoint(this.Z, index.Z, roundUp),
		MathUtility.IntegerMidpoint(this.W, index.W, roundUp));

	/// <inheritdoc />
	public Index4D Divide(Index4D divisor, out Index4D remainder)
	{
		IIndexContracts.Divide(divisor);

		var result = new Index4D(
			Math.DivRem(this.X, divisor.X, out var remainderX),
			Math.DivRem(this.Y, divisor.Y, out var remainderY),
			Math.DivRem(this.Z, divisor.Z, out var remainderZ),
			Math.DivRem(this.W, divisor.W, out var remainderW));
		remainder = new Index4D(remainderX, remainderY, remainderZ, remainderW);
		return result;
	}

	/// <inheritdoc />
	public Index4D DivideRoundUp(Index4D divisor)
	{
		IIndexContracts.DivideRoundUp(this, divisor);

		return new Index4D(
			MathUtility.DivideRoundUp(this.X, divisor.X),
			MathUtility.DivideRoundUp(this.Y, divisor.Y),
			MathUtility.DivideRoundUp(this.Z, divisor.Z),
			MathUtility.DivideRoundUp(this.W, divisor.W));
	}

	/// <inheritdoc />
	public IEnumerable<Index4D> Range(Index4D dimensions)
	{
		IIndexContracts.Range(dimensions);

		var lastIndex = this + dimensions;
		for (int iW = this.W; iW < lastIndex.W; iW++)
		{
			for (int iZ = this.Z; iZ < lastIndex.Z; iZ++)
			{
				for (int iY = this.Y; iY < lastIndex.Y; iY++)
				{
					for (int iX = this.X; iX < lastIndex.X; iX++)
					{
						yield return new Index4D(iX, iY, iZ, iW);
					}
				}
			}
		}
	}

	/// <summary>
	/// Creates a new array whose dimensions are taken from this index.
	/// </summary>
	/// <typeparam name="T">The type of the value of the array.</typeparam>
	/// <returns>The new array.</returns>
	public T[,,,] CreateArray<T>()
	{
		Debug.Assert(this.Coordinates().All(value => value >= 0));

		return new T[this.X, this.Y, this.Z, this.W];
	}

	#endregion
}
