﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// A three dimensional integer index for accessing arrays or grid based structures.
/// </summary>
public readonly struct Index3D : IIndex<Index3D>
{
	#region Constructors

	/// <summary>
	/// Initializes a new instance of the <see cref="Index3D"/> struct.
	/// </summary>
	/// <param name="number">The number to assign to each index.</param>
	public Index3D(int number)
	{
		this.X = number;
		this.Y = number;
		this.Z = number;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Index3D"/> struct.
	/// </summary>
	/// <param name="x">The x index.</param>
	/// <param name="y">The y index.</param>
	/// <param name="z">The z index.</param>
	public Index3D(int x, int y, int z)
	{
		this.X = x;
		this.Y = y;
		this.Z = z;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Index3D" /> struct.
	/// </summary>
	/// <param name="index">The index whose coordinates to copy.</param>
	public Index3D(Index3D index)
	{
		this.X = index.X;
		this.Y = index.Y;
		this.Z = index.Z;
	}

	#endregion

	#region Properties/Indexer

	/// <summary>
	/// Gets the zero index.
	/// </summary>
	/// <value>
	/// The zero index.
	/// </value>
	public static Index3D Zero => new Index3D(0);

	public static Index3D UnitX => new Index3D(1, 0, 0);

	public static Index3D UnitY => new Index3D(0, 1, 0);

	public static Index3D UnitZ => new Index3D(0, 0, 1);

	/// <summary>
	/// Gets the minimum index.
	/// </summary>
	/// <value>
	/// The minimum index.
	/// </value>
	public static Index3D MinValue => new Index3D(int.MinValue);

	/// <summary>
	/// Gets the maximum index.
	/// </summary>
	/// <value>
	/// The maximum index.
	/// </value>
	public static Index3D MaxValue => new Index3D(int.MaxValue);

	/// <inheritdoc />
	public Curves.ISpaceFillingCurve<Index3D> MortonCurve => Curves.MortonCurve.Index3D;

	/// <summary>
	/// Gets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; }

	/// <summary>
	/// Gets the Y coordinate.
	/// </summary>
	/// <value>
	/// The Y coordinate.
	/// </value>
	public int Y { get; }

	/// <summary>
	/// Gets the Z coordinate.
	/// </summary>
	/// <value>
	/// The Z coordinate.
	/// </value>
	public int Z { get; }

	/// <inheritdoc />
	public int Rank => 3;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			ICoordinatesContracts.Indexer(this, dimension);

			return dimension switch
			{
				AxisValue.X => this.X,
				AxisValue.Y => this.Y,
				AxisValue.Z => this.Z,
				_ => throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage),
			};
		}
	}

	#endregion

	#region Operators

	public static Index3D operator +(Index3D value) => value;

	public static Index3D operator -(Index3D value) => new Index3D(-value.X, -value.Y, -value.Z);

	public static Index3D operator +(Index3D lhs, Index3D rhs) => new Index3D(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z);

	public static Index3D operator -(Index3D lhs, Index3D rhs) => new Index3D(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z);

	public static Index3D operator *(Index3D lhs, Index3D rhs) => new Index3D(lhs.X * rhs.X, lhs.Y * rhs.Y, lhs.Z * rhs.Z);

	public static Index3D operator *(Index3D index, int scalar) => new Index3D(index.X * scalar, index.Y * scalar, index.Z * scalar);

	public static Index3D operator *(int scalar, Index3D index) => new Index3D(index.X * scalar, index.Y * scalar, index.Z * scalar);

	public static Index3D operator /(Index3D lhs, Index3D rhs) => new Index3D(lhs.X / rhs.X, lhs.Y / rhs.Y, lhs.Z / rhs.Z);

	public static Index3D operator /(Index3D index, int scalar) => new Index3D(index.X / scalar, index.Y / scalar, index.Z / scalar);

	public static Index3D operator %(Index3D lhs, Index3D rhs) => new Index3D(lhs.X % rhs.X, lhs.Y % rhs.Y, lhs.Z % rhs.Z);

	public static Index3D operator %(Index3D lhs, int scalar) => new Index3D(lhs.X % scalar, lhs.Y % scalar, lhs.Z % scalar);

	public static Index3D operator &(Index3D index, Index3D mask) => new Index3D(index.X & mask.X, index.Y & mask.Y, index.Z & mask.Z);

	public static Index3D operator &(Index3D index, int mask) => new Index3D(index.X & mask, index.Y & mask, index.Z & mask);

	public static Index3D operator |(Index3D index, Index3D mask) => new Index3D(index.X | mask.X, index.Y | mask.Y, index.Z | mask.Z);

	public static Index3D operator |(Index3D index, int mask) => new Index3D(index.X | mask, index.Y | mask, index.Z | mask);

	public static Index3D operator ^(Index3D index, Index3D mask) => new Index3D(index.X ^ mask.X, index.Y ^ mask.Y, index.Z ^ mask.Z);

	public static Index3D operator ^(Index3D index, int mask) => new Index3D(index.X ^ mask, index.Y ^ mask, index.Z ^ mask);

	public static Index3D operator ~(Index3D index) => new Index3D(~index.X, ~index.Y, ~index.Z);

	public static Index3D operator <<(Index3D index, int shift) => new Index3D(index.X << shift, index.Y << shift, index.Z << shift);

	public static Index3D operator >>(Index3D index, int shift) => new Index3D(index.X >> shift, index.Y >> shift, index.Z >> shift);

	public static bool operator ==(Index3D lhs, Index3D rhs) => lhs.Equals(rhs);

	public static bool operator !=(Index3D lhs, Index3D rhs) => !lhs.Equals(rhs);

	#endregion

	#region Methods

	/// <inheritdoc />
	public bool Equals(Index3D other) => this.X == other.X && this.Y == other.Y && this.Z == other.Z;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.X, this.Y, this.Z);

	/// <inheritdoc />
	public override string ToString() => $"({this.X}, {this.Y}, {this.Z})";

	/// <inheritdoc />
	public IIndexBuilder<Index3D> ToBuilder() => new IndexBuilder3D()
	{
		X = this.X,
		Y = this.Y,
		Z = this.Z,
	};

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
		yield return new DimensionCoordinatePair(AxisValue.Y, this.Y);
		yield return new DimensionCoordinatePair(AxisValue.Z, this.Z);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	public bool IsIn(Index3D min, Index3D max)
	{
		IIndexContracts.IsIn(min, max);

		return this.X >= min.X && this.X <= max.X
			&& this.Y >= min.Y && this.Y <= max.Y
			&& this.Z >= min.Z && this.Z <= max.Z;
	}

	/// <inheritdoc />
	public Index3D Clamp(Index3D min, Index3D max) =>
		new Index3D(this.X.Clamp(min.X, max.X), this.Y.Clamp(min.Y, max.Y), this.Z.Clamp(min.Z, max.Z));

	/// <inheritdoc />
	public Index3D ClampUpper(Index3D max) =>
		new Index3D(this.X.ClampUpper(max.X), this.Y.ClampUpper(max.Y), this.Z.ClampUpper(max.Z));

	/// <inheritdoc />
	public Index3D ClampLower(Index3D min) =>
		new Index3D(this.X.ClampLower(min.X), this.Y.ClampLower(min.Y), this.Z.ClampLower(min.Z));

	/// <inheritdoc />
	public Index3D Midpoint(Index3D index, bool roundUp = false) => new Index3D(
		MathUtility.IntegerMidpoint(this.X, index.X, roundUp),
		MathUtility.IntegerMidpoint(this.Y, index.Y, roundUp),
		MathUtility.IntegerMidpoint(this.Z, index.Z, roundUp));

	/// <inheritdoc />
	public Index3D Divide(Index3D divisor, out Index3D remainder)
	{
		IIndexContracts.Divide(divisor);

		var result = new Index3D(
			Math.DivRem(this.X, divisor.X, out var remainderX),
			Math.DivRem(this.Y, divisor.Y, out var remainderY),
			Math.DivRem(this.Z, divisor.Z, out var remainderZ));
		remainder = new Index3D(remainderX, remainderY, remainderZ);
		return result;
	}

	/// <inheritdoc />
	public Index3D DivideRoundUp(Index3D divisor)
	{
		IIndexContracts.DivideRoundUp(this, divisor);

		return new Index3D(
			MathUtility.DivideRoundUp(this.X, divisor.X),
			MathUtility.DivideRoundUp(this.Y, divisor.Y),
			MathUtility.DivideRoundUp(this.Z, divisor.Z));
	}

	/// <inheritdoc />
	public IEnumerable<Index3D> Range(Index3D dimensions)
	{
		IIndexContracts.Range(dimensions);

		var lastIndex = this + dimensions;
		for (int iZ = this.Z; iZ < lastIndex.Z; iZ++)
		{
			for (int iY = this.Y; iY < lastIndex.Y; iY++)
			{
				for (int iX = this.X; iX < lastIndex.X; iX++)
				{
					yield return new Index3D(iX, iY, iZ);
				}
			}
		}
	}

	/// <summary>
	/// Creates a new array whose dimensions are taken from this index.
	/// </summary>
	/// <typeparam name="T">The type of the value of the array.</typeparam>
	/// <returns>The new array.</returns>
	public T[,,] CreateArray<T>()
	{
		Debug.Assert(this.Coordinates().All(value => value >= 0));

		return new T[this.X, this.Y, this.Z];
	}

	#endregion
}
