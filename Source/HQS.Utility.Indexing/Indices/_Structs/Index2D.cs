﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// A three dimensional integer index for accessing arrays or grid based structures.
/// </summary>
public readonly struct Index2D : IIndex<Index2D>
{
	#region Constructors

	/// <summary>
	/// Initializes a new instance of the <see cref="Index2D"/> struct.
	/// </summary>
	/// <param name="number">The number to assign to each index.</param>
	public Index2D(int number)
	{
		this.X = number;
		this.Y = number;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Index2D"/> struct.
	/// </summary>
	/// <param name="x">The x index.</param>
	/// <param name="y">The y index.</param>
	public Index2D(int x, int y)
	{
		this.X = x;
		this.Y = y;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Index2D" /> struct.
	/// </summary>
	/// <param name="index">The index whose coordinates to copy.</param>
	public Index2D(Index2D index)
	{
		this.X = index.X;
		this.Y = index.Y;
	}

	#endregion

	#region Properties/Indexer

	/// <summary>
	/// Gets the zero index.
	/// </summary>
	/// <value>
	/// The zero index.
	/// </value>
	public static Index2D Zero => new Index2D(0);

	public static Index2D UnitX => new Index2D(1, 0);

	public static Index2D UnitY => new Index2D(0, 1);

	/// <summary>
	/// Gets the minimum index.
	/// </summary>
	/// <value>
	/// The minimum index.
	/// </value>
	public static Index2D MinValue => new Index2D(int.MinValue);

	/// <summary>
	/// Gets the maximum index.
	/// </summary>
	/// <value>
	/// The maximum index.
	/// </value>
	public static Index2D MaxValue => new Index2D(int.MaxValue);

	/// <inheritdoc />
	public Curves.ISpaceFillingCurve<Index2D> MortonCurve => Curves.MortonCurve.Index2D;

	/// <summary>
	/// Gets the X coordinate.
	/// </summary>
	/// <value>
	/// The X coordinate.
	/// </value>
	public int X { get; }

	/// <summary>
	/// Gets the Y coordinate.
	/// </summary>
	/// <value>
	/// The Y coordinate.
	/// </value>
	public int Y { get; }

	/// <inheritdoc />
	public int Rank => 2;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			ICoordinatesContracts.Indexer(this, dimension);

			return dimension switch
			{
				AxisValue.X => this.X,
				AxisValue.Y => this.Y,
				_ => throw new ArgumentOutOfRangeException(nameof(dimension), IndexUtility.InvalidDimensionExceptionMessage),
			};
		}
	}

	#endregion

	#region Operators

	public static Index2D operator +(Index2D value) => value;

	public static Index2D operator -(Index2D value) => new Index2D(-value.X, -value.Y);

	public static Index2D operator +(Index2D lhs, Index2D rhs) => new Index2D(lhs.X + rhs.X, lhs.Y + rhs.Y);

	public static Index2D operator -(Index2D lhs, Index2D rhs) => new Index2D(lhs.X - rhs.X, lhs.Y - rhs.Y);

	public static Index2D operator *(Index2D lhs, Index2D rhs) => new Index2D(lhs.X * rhs.X, lhs.Y * rhs.Y);

	public static Index2D operator *(Index2D index, int scalar) => new Index2D(index.X * scalar, index.Y * scalar);

	public static Index2D operator *(int scalar, Index2D index) => new Index2D(index.X * scalar, index.Y * scalar);

	public static Index2D operator /(Index2D lhs, Index2D rhs) => new Index2D(lhs.X / rhs.X, lhs.Y / rhs.Y);

	public static Index2D operator /(Index2D index, int scalar) => new Index2D(index.X / scalar, index.Y / scalar);

	public static Index2D operator %(Index2D lhs, Index2D rhs) => new Index2D(lhs.X % rhs.X, lhs.Y % rhs.Y);

	public static Index2D operator %(Index2D lhs, int scalar) => new Index2D(lhs.X % scalar, lhs.Y % scalar);

	public static Index2D operator &(Index2D index, Index2D mask) => new Index2D(index.X & mask.X, index.Y & mask.Y);

	public static Index2D operator &(Index2D index, int mask) => new Index2D(index.X & mask, index.Y & mask);

	public static Index2D operator |(Index2D index, Index2D mask) => new Index2D(index.X | mask.X, index.Y | mask.Y);

	public static Index2D operator |(Index2D index, int mask) => new Index2D(index.X | mask, index.Y | mask);

	public static Index2D operator ^(Index2D index, Index2D mask) => new Index2D(index.X ^ mask.X, index.Y ^ mask.Y);

	public static Index2D operator ^(Index2D index, int mask) => new Index2D(index.X ^ mask, index.Y ^ mask);

	public static Index2D operator ~(Index2D index) => new Index2D(~index.X, ~index.Y);

	public static Index2D operator <<(Index2D index, int shift) => new Index2D(index.X << shift, index.Y << shift);

	public static Index2D operator >>(Index2D index, int shift) => new Index2D(index.X >> shift, index.Y >> shift);

	public static bool operator ==(Index2D lhs, Index2D rhs) => lhs.Equals(rhs);

	public static bool operator !=(Index2D lhs, Index2D rhs) => !lhs.Equals(rhs);

	#endregion

	#region Methods

	/// <inheritdoc />
	public bool Equals(Index2D other) => this.X == other.X && this.Y == other.Y;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.X, this.Y);

	/// <inheritdoc />
	public override string ToString() => $"({this.X}, {this.Y})";

	/// <inheritdoc />
	public IIndexBuilder<Index2D> ToBuilder() => new IndexBuilder2D()
	{
		X = this.X,
		Y = this.Y,
	};

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator()
	{
		yield return new DimensionCoordinatePair(AxisValue.X, this.X);
		yield return new DimensionCoordinatePair(AxisValue.Y, this.Y);
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	public bool IsIn(Index2D min, Index2D max)
	{
		IIndexContracts.IsIn(min, max);

		return this.X >= min.X && this.X <= max.X
			&& this.Y >= min.Y && this.Y <= max.Y;
	}

	/// <inheritdoc />
	public Index2D Clamp(Index2D min, Index2D max) =>
		new Index2D(this.X.Clamp(min.X, max.X), this.Y.Clamp(min.Y, max.Y));

	/// <inheritdoc />
	public Index2D ClampUpper(Index2D max) =>
		new Index2D(this.X.ClampUpper(max.X), this.Y.ClampUpper(max.Y));

	/// <inheritdoc />
	public Index2D ClampLower(Index2D min) =>
		new Index2D(this.X.ClampLower(min.X), this.Y.ClampLower(min.Y));

	/// <inheritdoc />
	public Index2D Midpoint(Index2D index, bool roundUp = false) => new Index2D(
		MathUtility.IntegerMidpoint(this.X, index.X, roundUp),
		MathUtility.IntegerMidpoint(this.Y, index.Y, roundUp));

	/// <inheritdoc />
	public Index2D Divide(Index2D divisor, out Index2D remainder)
	{
		IIndexContracts.Divide(divisor);

		var result = new Index2D(
			Math.DivRem(this.X, divisor.X, out int remainderX),
			Math.DivRem(this.Y, divisor.Y, out int remainderY));
		remainder = new Index2D(remainderX, remainderY);
		return result;
	}

	/// <inheritdoc />
	public Index2D DivideRoundUp(Index2D divisor)
	{
		IIndexContracts.DivideRoundUp(this, divisor);

		return new Index2D(MathUtility.DivideRoundUp(this.X, divisor.X), MathUtility.DivideRoundUp(this.Y, divisor.Y));
	}

	/// <inheritdoc />
	public IEnumerable<Index2D> Range(Index2D dimensions)
	{
		IIndexContracts.Range(dimensions);

		var lastIndex = this + dimensions;
		for (int iY = this.Y; iY < lastIndex.Y; iY++)
		{
			for (int iX = this.X; iX < lastIndex.X; iX++)
			{
				yield return new Index2D(iX, iY);
			}
		}
	}

	/// <summary>
	/// Creates a new array whose dimensions are taken from this index.
	/// </summary>
	/// <typeparam name="T">The type of the value of the array.</typeparam>
	/// <returns>The new array.</returns>
	public T[,] CreateArray<T>()
	{
		Debug.Assert(this.Coordinates().All(value => value >= 0));

		return new T[this.X, this.Y];
	}

	#endregion
}
