﻿namespace HQS.Utility.Indexing.Indices;

public readonly struct DimensionCoordinatePair : IEquatable<DimensionCoordinatePair>
{
	public DimensionCoordinatePair(int dimension, int coordinate)
	{
		Debug.Assert(dimension >= 0);

		this.Dimension = dimension;
		this.Coordinate = coordinate;
	}

	public int Dimension { get; }

	public int Coordinate { get; }

	/// <inheritdoc />
	public bool Equals(DimensionCoordinatePair other) => this.Dimension == other.Dimension && this.Coordinate == other.Coordinate;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Dimension, this.Coordinate);

	/// <inheritdoc />
	public override string ToString() => $"[Dimension {this.Dimension}: {this.Coordinate}]";

	public static bool operator ==(DimensionCoordinatePair left, DimensionCoordinatePair right) => left.Equals(right);

	public static bool operator !=(DimensionCoordinatePair left, DimensionCoordinatePair right) => !(left == right);
}
