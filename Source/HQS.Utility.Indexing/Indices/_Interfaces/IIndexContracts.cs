﻿namespace HQS.Utility.Indexing.Indices;

public static class IIndexContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void IsIn<TIndex>(TIndex min, TIndex max)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(min.All(pair => pair.Coordinate <= max[pair.Dimension]));
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Divide<TIndex>(TIndex divisor)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(divisor.Coordinates().All(value => value != 0));
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void DivideRoundUp<TIndex>(TIndex instance, TIndex divisor)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(instance.Coordinates().All(value => value >= 0));
		Debug.Assert(divisor.Coordinates().All(value => value >= 1));
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void Range<TIndex>(TIndex dimensions)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(dimensions.Coordinates().All(value => value >= 0));
	}
}
