﻿namespace HQS.Utility.Indexing.Indices;

public interface ICoordinates : IEnumerable<DimensionCoordinatePair>
{
	/// <summary>
	/// Gets the rank (number of dimensions) of the IIndex. For example, a one-dimensional index returns 1,
	/// a two-dimensional index returns 2, and so on.
	/// </summary>
	/// <value>
	/// The number of dimensions.
	/// </value>
	int Rank { get; }

	/// <summary>
	/// Gets the coordinate value (index) of a particular dimension of the IIndex.
	/// </summary>
	/// <param name="dimension">
	/// A zero-based dimension of the IIndex whose coordinate value needs to be determined.
	/// </param>
	/// <returns>
	/// The value of the dimensional coordinate.
	/// </returns>
	int this[int dimension] { get; }
}
