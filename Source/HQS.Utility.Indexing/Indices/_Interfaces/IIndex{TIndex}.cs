﻿namespace HQS.Utility.Indexing.Indices;

/// <summary>
/// An integer based index into an array or grid based structure of an undefined
/// number of dimensions. Implementations of this interface define how many
/// dimensions the index can represent.
/// </summary>
/// <typeparam name="TIndex">The type of the index.</typeparam>
public interface IIndex<TIndex> : IEquatable<TIndex>, ICoordinates
	where TIndex : struct, IIndex<TIndex>
{
	ISpaceFillingCurve<TIndex> MortonCurve { get; }

	IIndexBuilder<TIndex> ToBuilder();

	bool IsIn(TIndex min, TIndex max);

	TIndex Clamp(TIndex min, TIndex max);

	TIndex ClampUpper(TIndex max);

	TIndex ClampLower(TIndex min);

	TIndex Midpoint(TIndex index, bool roundUp = false);

	TIndex Divide(TIndex divisor, out TIndex remainder);

	TIndex DivideRoundUp(TIndex divisor);

	IEnumerable<TIndex> Range(TIndex dimensions);
}
