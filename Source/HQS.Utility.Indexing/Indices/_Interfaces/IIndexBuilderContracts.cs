﻿namespace HQS.Utility.Indexing.Indices;

public static class IIndexBuilderContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Indexer<TIndex>(IIndexBuilder<TIndex> instance, int dimension)
		where TIndex : struct, IIndex<TIndex>
	{
		ICoordinatesContracts.Indexer(instance, dimension);
	}
}
