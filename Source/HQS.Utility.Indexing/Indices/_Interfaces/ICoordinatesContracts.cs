﻿namespace HQS.Utility.Indexing.Indices;

public static class ICoordinatesContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Indexer(ICoordinates instance, int dimension)
	{
		Debug.Assert(instance != null);
		Debug.Assert(dimension >= 0 && dimension < instance.Rank);
	}
}
