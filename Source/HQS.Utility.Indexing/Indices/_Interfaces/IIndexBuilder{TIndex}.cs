﻿namespace HQS.Utility.Indexing.Indices;

public interface IIndexBuilder<TIndex> : ICoordinates
	where TIndex : struct, IIndex<TIndex>
{
	new int this[int dimension] { get; set; }

	TIndex ToIndex();
}
