﻿namespace HQS.Utility.Indexing.Utility;

/// <summary>
///
/// </summary>
public enum OscillationOrder3D
{
	XYZ,

	XZY,

	YXZ,

	YZX,

	ZXY,

	ZYX,
}
