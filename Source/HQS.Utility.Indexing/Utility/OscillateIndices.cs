﻿namespace HQS.Utility.Indexing.Utility;

public static partial class OscillateIndices
{
	public static IEnumerable<Index2D> Range(Index2D start, Index2D dimensions, OscillationOrder2D order = OscillationOrder2D.XY)
	{
		Debug.Assert(dimensions.Coordinates().All(value => value >= 0));

		return order switch
		{
			OscillationOrder2D.XY => OscillateRangeXY(start, dimensions),
			OscillationOrder2D.YX => OscillateRangeYX(start, dimensions),
			_ => throw InvalidEnumArgument.CreateException(nameof(order), order),
		};
	}

	public static IEnumerable<Index3D> Range(Index3D start, Index3D dimensions, OscillationOrder3D order = OscillationOrder3D.XYZ)
	{
		Debug.Assert(dimensions.Coordinates().All(value => value >= 0));

		return order switch
		{
			OscillationOrder3D.XYZ => OscillateRangeXYZ(start, dimensions),
			OscillationOrder3D.XZY => OscillateRangeXZY(start, dimensions),
			OscillationOrder3D.YXZ => OscillateRangeYXZ(start, dimensions),
			OscillationOrder3D.YZX => OscillateRangeYZX(start, dimensions),
			OscillationOrder3D.ZXY => OscillateRangeZXY(start, dimensions),
			OscillationOrder3D.ZYX => OscillateRangeZYX(start, dimensions),
			_ => throw InvalidEnumArgument.CreateException(nameof(order), order),
		};
	}

	#region Private OscillateRange 2D

	private static IEnumerable<Index2D> OscillateRangeXY(Index2D start, Index2D dimensions)
	{
		var xAxis = new OscillatingRange(start.X, dimensions.X);

		foreach (int iY in Enumerable.Range(start.Y, dimensions.Y))
		{
			foreach (int iX in xAxis)
			{
				yield return new Index2D(iX, iY);
			}
		}
	}

	private static IEnumerable<Index2D> OscillateRangeYX(Index2D start, Index2D dimensions)
	{
		var yAxis = new OscillatingRange(start.Y, dimensions.Y);

		foreach (int iX in Enumerable.Range(start.X, dimensions.X))
		{
			foreach (int iY in yAxis)
			{
				yield return new Index2D(iX, iY);
			}
		}
	}

	#endregion

	#region Private OscillateRange 3D

	private static IEnumerable<Index3D> OscillateRangeXYZ(Index3D start, Index3D dimensions)
	{
		var xAxis = new OscillatingRange(start.X, dimensions.X);
		var yAxis = new OscillatingRange(start.Y, dimensions.Y);

		foreach (int iZ in Enumerable.Range(start.Z, dimensions.Z))
		{
			foreach (int iY in yAxis)
			{
				foreach (int iX in xAxis)
				{
					yield return new Index3D(iX, iY, iZ);
				}
			}
		}
	}

	private static IEnumerable<Index3D> OscillateRangeXZY(Index3D start, Index3D dimensions)
	{
		var xAxis = new OscillatingRange(start.X, dimensions.X);
		var zAxis = new OscillatingRange(start.Z, dimensions.Z);

		foreach (int iY in Enumerable.Range(start.Y, dimensions.Y))
		{
			foreach (int iZ in zAxis)
			{
				foreach (int iX in xAxis)
				{
					yield return new Index3D(iX, iY, iZ);
				}
			}
		}
	}

	private static IEnumerable<Index3D> OscillateRangeYXZ(Index3D start, Index3D dimensions)
	{
		var xAxis = new OscillatingRange(start.X, dimensions.X);
		var yAxis = new OscillatingRange(start.Y, dimensions.Y);

		foreach (int iZ in Enumerable.Range(start.Z, dimensions.Z))
		{
			foreach (int iX in xAxis)
			{
				foreach (int iY in yAxis)
				{
					yield return new Index3D(iX, iY, iZ);
				}
			}
		}
	}

	private static IEnumerable<Index3D> OscillateRangeYZX(Index3D start, Index3D dimensions)
	{
		var yAxis = new OscillatingRange(start.Y, dimensions.Y);
		var zAxis = new OscillatingRange(start.Z, dimensions.Z);

		foreach (int iX in Enumerable.Range(start.X, dimensions.X))
		{
			foreach (int iZ in zAxis)
			{
				foreach (int iY in yAxis)
				{
					yield return new Index3D(iX, iY, iZ);
				}
			}
		}
	}

	private static IEnumerable<Index3D> OscillateRangeZXY(Index3D start, Index3D dimensions)
	{
		var xAxis = new OscillatingRange(start.X, dimensions.X);
		var zAxis = new OscillatingRange(start.Z, dimensions.Z);

		foreach (int iY in Enumerable.Range(start.Y, dimensions.Y))
		{
			foreach (int iX in xAxis)
			{
				foreach (int iZ in zAxis)
				{
					yield return new Index3D(iX, iY, iZ);
				}
			}
		}
	}

	private static IEnumerable<Index3D> OscillateRangeZYX(Index3D start, Index3D dimensions)
	{
		var yAxis = new OscillatingRange(start.Y, dimensions.Y);
		var zAxis = new OscillatingRange(start.Z, dimensions.Z);

		foreach (int iX in Enumerable.Range(start.X, dimensions.X))
		{
			foreach (int iY in yAxis)
			{
				foreach (int iZ in zAxis)
				{
					yield return new Index3D(iX, iY, iZ);
				}
			}
		}
	}

	#endregion
}
