﻿namespace HQS.Utility.Indexing.Utility;

public static class AxisValue
{
	public const int X = 0;

	public const int Y = 1;

	public const int Z = 2;

	public const int W = 3;
}
