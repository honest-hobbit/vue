﻿namespace HQS.Utility.Indexing.Utility;

/// <summary>
/// Provides extensions methods for one dimensional arrays.
/// </summary>
public static class ArrayExtensions3D
{
	public static T Get<T>(this T[,,] array, Index3D index)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.IsIndexInBounds(index));

		return array[index.X, index.Y, index.Z];
	}

	public static void Set<T>(this T[,,] array, Index3D index, T value)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.IsIndexInBounds(index));

		array[index.X, index.Y, index.Z] = value;
	}

	/// <summary>
	/// Gets the dimensions of an array.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the array.</typeparam>
	/// <param name="array">The array.</param>
	/// <returns>The dimensions.</returns>
	public static Index3D GetDimensions<T>(this T[,,] array)
	{
		Debug.Assert(array != null);

		return new Index3D(array.GetLength(AxisValue.X), array.GetLength(AxisValue.Y), array.GetLength(AxisValue.Z));
	}

	/// <summary>
	/// Gets the inclusive lower bounds of an array.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the array.</typeparam>
	/// <param name="array">The array.</param>
	/// <returns>The inclusive upper bounds.</returns>
	public static Index3D GetLowerBounds<T>(this T[,,] array)
	{
		Debug.Assert(array != null);

		return new Index3D(array.GetLowerBound(AxisValue.X), array.GetLowerBound(AxisValue.Y), array.GetLowerBound(AxisValue.Z));
	}

	/// <summary>
	/// Gets the inclusive upper bounds of an array.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the array.</typeparam>
	/// <param name="array">The array.</param>
	/// <returns>The inclusive upper bounds.</returns>
	public static Index3D GetUpperBounds<T>(this T[,,] array)
	{
		Debug.Assert(array != null);

		return new Index3D(array.GetUpperBound(AxisValue.X), array.GetUpperBound(AxisValue.Y), array.GetUpperBound(AxisValue.Z));
	}

	public static Index3D GetMiddleIndex<T>(this T[,,] array, bool roundUp = false)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.HasCapacity());

		return array.GetLowerBounds().Midpoint(array.GetUpperBounds(), roundUp);
	}

	/// <summary>
	/// Determines whether the specified index is valid to use with this array's indexer.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the array.</typeparam>
	/// <param name="array">The array.</param>
	/// <param name="index">The index to check.</param>
	/// <returns>True if the index is valid to use, otherwise false.</returns>
	public static bool IsIndexInBounds<T>(this T[,,] array, Index3D index)
	{
		Debug.Assert(array != null);

		return array.HasCapacity() ? index.IsIn(array.GetLowerBounds(), array.GetUpperBounds()) : false;
	}

	/// <summary>
	/// Gets an enumerable sequence of all the indices of an array.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the array.</typeparam>
	/// <param name="array">The array.</param>
	/// <returns>The enumerable sequence of indices.</returns>
	public static IEnumerable<Index3D> GetIndices<T>(this T[,,] array)
	{
		Debug.Assert(array != null);

		return array.GetLowerBounds().Range(array.GetDimensions());
	}

	/// <summary>
	/// Gets an enumerable sequence of all the indices along with their associated values of an array.
	/// </summary>
	/// <typeparam name="T">The type of values stored in the array.</typeparam>
	/// <param name="array">The array.</param>
	/// <returns>The enumerable sequence of indices paired with their values.</returns>
	public static IEnumerable<IndexValuePair<Index3D, T>> GetIndexValuePairs<T>(this T[,,] array)
	{
		Debug.Assert(array != null);

		foreach (var index in array.GetIndices())
		{
			yield return IndexValuePair.New(index, array.Get(index));
		}
	}

	public static TResult[,,] ConvertAll<TSource, TResult>(this TSource[,,] array, Func<TSource, TResult> converter)
	{
		Debug.Assert(array != null);
		Debug.Assert(converter != null);

		var result = array.GetDimensions().CreateArray<TResult>();
		foreach (var pair in array.GetIndexValuePairs())
		{
			result.Set(pair.Index, converter(pair.Value));
		}

		return result;
	}
}
