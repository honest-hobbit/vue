﻿namespace HQS.Utility.Indexing.Interests;

/// <summary>
/// Provides extension methods for <see cref="IInterestMerger{TInterest}"/>.
/// </summary>
public static class IInterestMergerExtensions
{
	public static bool TryAdd<TInterest>(
		this IInterestMerger<TInterest> merger, Try<TInterest> current, TInterest add, out Try<TInterest> result)
	{
		Debug.Assert(merger != null);

		var isDifferent = merger.TryAdd(current.ToValueOrDefault(merger.None), add, out var resultInterests);
		result = merger.IsNone(resultInterests) ? Try.None<TInterest>() : Try.Value(resultInterests);
		return isDifferent;
	}

	public static bool TryRemove<TInterest>(
		this IInterestMerger<TInterest> merger, Try<TInterest> current, TInterest remove, out Try<TInterest> result)
	{
		Debug.Assert(merger != null);

		var isDifferent = merger.TryRemove(current.ToValueOrDefault(merger.None), remove, out var resultInterests);
		result = merger.IsNone(resultInterests) ? Try.None<TInterest>() : Try.Value(resultInterests);
		return isDifferent;
	}
}
