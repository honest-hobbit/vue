﻿namespace HQS.Utility.Indexing.Interests;

/// <summary>
/// Provides extension methods for <see cref="IInterestMapView{TKey, TInterest}.InterestsChanged"/>.
/// </summary>
public static class InterestsChangedExtensions
{
	public static IObservable<KeyValuePair<TKey, Status>> Activity<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged.Opened().Select(opened => KeyValuePair.Create(opened.Key, Status.Active))
			.Merge(interestsChanged.Closed().Select(closed => KeyValuePair.Create(closed.Key, Status.Inactive)));
	}

	// KeyValuePair.Value is the first interest value
	public static IObservable<KeyValuePair<TKey, TInterests>> Opened<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged
			.Where(changed => !changed.Value.Previous.HasValue && changed.Value.Next.HasValue)
			.Select(changed => KeyValuePair.Create(changed.Key, changed.Value.Next.Value));
	}

	// KeyValuePair.Value is the last interest value
	public static IObservable<KeyValuePair<TKey, TInterests>> Closed<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged
			.Where(changed => changed.Value.Previous.HasValue && !changed.Value.Next.HasValue)
			.Select(changed => KeyValuePair.Create(changed.Key, changed.Value.Previous.Value));
	}

	public static IObservable<KeyValuePair<TKey, SequentialPair<TInterests>>> HasBoth<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged
			.Where(changed => changed.Value.Previous.HasValue && changed.Value.Next.HasValue)
			.Select(changed => KeyValuePair.Create(
				changed.Key, SequentialPair.New(changed.Value.Previous.Value, changed.Value.Next.Value)));
	}

	public static IObservable<KeyValuePair<TKey, TInterests>> HasPrevious<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged
			.Where(changed => changed.Value.Previous.HasValue)
			.Select(changed => KeyValuePair.Create(changed.Key, changed.Value.Previous.Value));
	}

	public static IObservable<KeyValuePair<TKey, TInterests>> HasNext<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged
			.Where(changed => changed.Value.Next.HasValue)
			.Select(changed => KeyValuePair.Create(changed.Key, changed.Value.Next.Value));
	}

	public static IObservable<KeyValuePair<TKey, Try<TInterests>>> SelectPrevious<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged.Select(changed => KeyValuePair.Create(changed.Key, changed.Value.Previous));
	}

	public static IObservable<KeyValuePair<TKey, Try<TInterests>>> SelectNext<TKey, TInterests>(
		this IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterests>>>> interestsChanged)
	{
		Debug.Assert(interestsChanged != null);

		return interestsChanged.Select(changed => KeyValuePair.Create(changed.Key, changed.Value.Next));
	}
}
