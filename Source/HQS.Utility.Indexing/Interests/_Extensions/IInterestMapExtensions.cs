﻿namespace HQS.Utility.Indexing.Interests;

/// <summary>
/// Provides extension methods for <see cref="IInterestMap{TKey, TInterest}"/>.
/// </summary>
public static class IInterestMapExtensions
{
	public static void AddInterests<TKey, TInterest>(
		this IInterestMap<TKey, TInterest> map, TInterest interest, IEnumerable<TKey> addTo)
	{
		Debug.Assert(map != null);
		Debug.Assert(addTo.AllAndSelfNotNull());

		foreach (var key in addTo)
		{
			map.AddInterest(key, interest);
		}
	}

	public static void RemoveInterests<TKey, TInterest>(
		this IInterestMap<TKey, TInterest> map, TInterest interest, IEnumerable<TKey> removeFrom)
	{
		Debug.Assert(map != null);
		Debug.Assert(removeFrom.AllAndSelfNotNull());

		foreach (var key in removeFrom)
		{
			map.RemoveInterest(key, interest);
		}
	}

	public static void UpdateInterests<TKey, TInterest>(
		this IInterestMap<TKey, TInterest> map, TInterest interest, IReadOnlySet<TKey> addTo, IReadOnlySet<TKey> removeFrom)
	{
		Debug.Assert(map != null);
		Debug.Assert(addTo.AllAndSelfNotNull());
		Debug.Assert(removeFrom.AllAndSelfNotNull());

		if (addTo.Count != 0 && removeFrom.Count == 0)
		{
			map.AddInterests(interest, addTo);
			return;
		}

		if (addTo.Count == 0 && removeFrom.Count != 0)
		{
			map.RemoveInterests(interest, removeFrom);
			return;
		}

		foreach (var key in addTo)
		{
			if (!removeFrom.Contains(key))
			{
				map.AddInterest(key, interest);
			}
		}

		foreach (var key in removeFrom)
		{
			if (!addTo.Contains(key))
			{
				map.RemoveInterest(key, interest);
			}
		}
	}

	public static IDisposable SubscribeArea<TKey, TInterest>(
		this IInterestMap<TKey, TInterest> map, IInterestArea<TKey, TInterest> area)
	{
		Debug.Assert(map != null);
		Debug.Assert(area != null);

		return map.DoSubscribeArea(area, null);
	}

	public static IDisposable SubscribeArea<TKey, TInterest>(
		this IInterestMap<TKey, TInterest> map, IInterestArea<TKey, TInterest> area, IObservable<Nothing> linkDisposalTo)
	{
		Debug.Assert(map != null);
		Debug.Assert(area != null);
		Debug.Assert(linkDisposalTo != null);

		return map.DoSubscribeArea(area, linkDisposalTo);
	}

	public static IInterestMapView<TKey, TInterest> AsView<TKey, TInterest>(this IInterestMap<TKey, TInterest> map)
	{
		Debug.Assert(map != null);

		return new InterestMapView<TKey, TInterest>(map);
	}

	private static IDisposable DoSubscribeArea<TKey, TInterest>(
		this IInterestMap<TKey, TInterest> map, IInterestArea<TKey, TInterest> area, IObservable<Nothing> linkDisposalTo)
	{
		Debug.Assert(map != null);
		Debug.Assert(area != null);

		// isDisposed is used to make the observable complete when the subscription is disposed
		// thus causing the area of interest to be removed from the interest map
		var isDisposed = new SingleSignalObservable();
		var keysChanged = area.KeysChanged.TakeUntil(isDisposed);

		if (linkDisposalTo != null)
		{
			// linking disposal will cause the area of interest to auto unsubscribe when the linked disposal completes
			keysChanged = keysChanged.TakeUntil(linkDisposalTo);
		}

		keysChanged.PairLatest().Subscribe(keys => map.UpdateInterests(
			area.Interest,
			addTo: keys.Next.ToValueOrDefault(ReadOnlySet.Empty<TKey>()),
			removeFrom: keys.Previous.ToValueOrDefault(ReadOnlySet.Empty<TKey>())));

		return isDisposed;
	}
}
