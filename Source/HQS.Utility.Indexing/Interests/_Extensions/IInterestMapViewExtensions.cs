﻿namespace HQS.Utility.Indexing.Interests;

/// <summary>
/// Provides extension methods for <see cref="IInterestMapView{TKey, TInterest}"/>.
/// </summary>
public static class IInterestMapViewExtensions
{
	public static bool HasAnyInterests<TKey, TInterest>(this IInterestMapView<TKey, TInterest> map, TKey key)
	{
		Debug.Assert(map != null);

		return map[key].HasValue;
	}
}
