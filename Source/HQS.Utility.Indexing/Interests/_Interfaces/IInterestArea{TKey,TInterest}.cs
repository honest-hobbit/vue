﻿namespace HQS.Utility.Indexing.Interests;

public interface IInterestArea<TKey, TInterest>
{
	TInterest Interest { get; }

	IObservable<IReadOnlySet<TKey>> KeysChanged { get; }
}
