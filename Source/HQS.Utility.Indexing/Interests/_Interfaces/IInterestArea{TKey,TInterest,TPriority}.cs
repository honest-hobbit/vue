﻿namespace HQS.Utility.Indexing.Interests;

public interface IInterestArea<TKey, TInterest, TPriority> : IInterestArea<TKey, TInterest>
{
	IObservable<IReadOnlyDictionary<TKey, TPriority>> PrioritiesChanged { get; }
}
