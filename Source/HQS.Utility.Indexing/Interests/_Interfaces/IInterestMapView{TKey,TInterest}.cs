﻿namespace HQS.Utility.Indexing.Interests;

public interface IInterestMapView<TKey, TInterest>
{
	IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> InterestsChanged { get; }

	Try<TInterest> this[TKey key] { get; }
}
