﻿namespace HQS.Utility.Indexing.Interests;

public interface IInterestMap<TKey, TInterest> : IInterestMapView<TKey, TInterest>
{
	void AddInterest(TKey key, TInterest interest);

	void RemoveInterest(TKey key, TInterest interest);
}
