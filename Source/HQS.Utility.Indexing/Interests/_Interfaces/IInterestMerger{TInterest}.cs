﻿namespace HQS.Utility.Indexing.Interests;

public interface IInterestMerger<TInterest>
{
	TInterest None { get; }

	bool IsNone(TInterest interests);

	// returns true if result is different than current
	bool TryAdd(TInterest current, TInterest add, out TInterest result);

	// returns true if result is different than current
	bool TryRemove(TInterest current, TInterest remove, out TInterest result);
}
