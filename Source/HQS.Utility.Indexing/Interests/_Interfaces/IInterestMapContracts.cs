﻿namespace HQS.Utility.Indexing.Interests;

public static class IInterestMapContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void AddInterest<TKey>(TKey key)
	{
		Debug.Assert(key != null);
	}

	[Conditional(CompilationSymbol.Debug)]
	public static void RemoveInterest<TKey>(TKey key)
	{
		Debug.Assert(key != null);
	}
}
