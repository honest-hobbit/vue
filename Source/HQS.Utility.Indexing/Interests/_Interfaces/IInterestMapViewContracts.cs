﻿namespace HQS.Utility.Indexing.Interests;

public static class IInterestMapViewContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void Indexer<TKey>(TKey key)
	{
		Debug.Assert(key != null);
	}
}
