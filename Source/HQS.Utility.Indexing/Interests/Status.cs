﻿namespace HQS.Utility.Indexing.Interests;

public enum Status
{
	Active,
	Inactive,
}
