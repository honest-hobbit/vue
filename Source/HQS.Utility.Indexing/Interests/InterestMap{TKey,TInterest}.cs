﻿namespace HQS.Utility.Indexing.Interests;

public class InterestMap<TKey, TInterest> : AbstractDisposable, IInterestMap<TKey, TInterest>
{
	private readonly Subject<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> interestsChanged =
		new Subject<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>>();

	private readonly IInterestMerger<TInterest> merger;

	private readonly IDictionary<TKey, TInterest> map;

	public InterestMap(IInterestMerger<TInterest> merger, IEqualityComparer<TKey> comparer = null)
		: this(merger, new Dictionary<TKey, TInterest>(comparer))
	{
	}

	public InterestMap(IInterestMerger<TInterest> merger, IDictionary<TKey, TInterest> map)
	{
		Debug.Assert(merger != null);
		Debug.Assert(map != null);

		this.merger = merger;
		this.map = map;
		this.InterestsChanged = this.interestsChanged.AsObservableOnly();
	}

	/// <inheritdoc />
	public IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> InterestsChanged { get; }

	public bool IsCompleted { get; private set; } = false;

	public void Complete()
	{
		Debug.Assert(!this.IsDisposed);

		if (this.IsCompleted)
		{
			return;
		}

		this.IsCompleted = true;
		this.interestsChanged.OnCompleted();
	}

	/// <inheritdoc />
	public Try<TInterest> this[TKey key]
	{
		get
		{
			Debug.Assert(!this.IsDisposed);
			IInterestMapViewContracts.Indexer(key);

			return this.map.TryGetValue(key);
		}
	}

	/// <inheritdoc />
	public void AddInterest(TKey key, TInterest interest)
	{
		Debug.Assert(!this.IsDisposed);
		IInterestMapContracts.AddInterest(key);

		var previous = this[key];
		if (this.merger.TryAdd(previous, interest, out var next))
		{
			this.HandleInterestsChanged(key, previous, next);
		}
	}

	/// <inheritdoc />
	public void RemoveInterest(TKey key, TInterest interest)
	{
		Debug.Assert(!this.IsDisposed);
		IInterestMapContracts.RemoveInterest(key);

		var previous = this[key];
		if (this.merger.TryRemove(previous, interest, out var next))
		{
			this.HandleInterestsChanged(key, previous, next);
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.interestsChanged.Dispose();

	private void HandleInterestsChanged(TKey key, Try<TInterest> previous, Try<TInterest> next)
	{
		if (next.HasValue)
		{
			this.map[key] = next.Value;
		}
		else
		{
			this.map.Remove(key);
		}

		if (!this.IsCompleted)
		{
			this.interestsChanged.OnNext(KeyValuePair.Create(key, SequentialPair.New(previous, next)));
		}
	}
}
