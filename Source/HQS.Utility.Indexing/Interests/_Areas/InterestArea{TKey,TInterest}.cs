﻿namespace HQS.Utility.Indexing.Interests;

public class InterestArea<TKey, TInterest> : IInterestArea<TKey, TInterest>
{
	public InterestArea(TInterest interest, IObservable<TKey> originChanged, IObservable<IReadOnlySet<TKey>> keysChanged)
	{
		Debug.Assert(originChanged != null);
		Debug.Assert(keysChanged != null);

		this.Interest = interest;
		this.KeysChanged = originChanged.CombineLatest(keysChanged, (origin, keys) => (IReadOnlySet<TKey>)new OffsetSet(origin, keys));
	}

	public InterestArea(TInterest interest, IObservable<TKey> originChanged, IReadOnlySet<TKey> keys)
		: this(interest, originChanged, Observable.Return(keys))
	{
		Debug.Assert(keys != null);
	}

	/// <inheritdoc />
	public TInterest Interest { get; }

	/// <inheritdoc />
	public IObservable<IReadOnlySet<TKey>> KeysChanged { get; }

	protected class OffsetSet : IReadOnlySet<TKey>
	{
		private readonly TKey origin;

		private readonly IReadOnlySet<TKey> relativeKeys;

		public OffsetSet(TKey origin, IReadOnlySet<TKey> relativeKeys)
		{
			Debug.Assert(relativeKeys != null);

			this.origin = origin;
			this.relativeKeys = relativeKeys;
		}

		/// <inheritdoc />
		public int Count => this.relativeKeys.Count;

		/// <inheritdoc />
		public bool Contains(TKey key) => this.relativeKeys.Contains(Operator.Subtract(key, this.origin));

		/// <inheritdoc />
		public IEnumerator<TKey> GetEnumerator() => this.relativeKeys.Select(key => Operator.Add(key, this.origin)).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
