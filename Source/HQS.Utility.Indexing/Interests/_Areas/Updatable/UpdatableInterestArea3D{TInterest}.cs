﻿namespace HQS.Utility.Indexing.Interests;

public class UpdatableInterestArea3D<TInterest> : AbstractUpdatableInterestArea3D<TInterest>
{
	public UpdatableInterestArea3D(TInterest interest, ObservableChunkIndex.Threshold threshold)
		: base(interest, threshold)
	{
	}

	public new void SetArea(IReadOnlySet<Index3D> area) => base.SetArea(area);
}
