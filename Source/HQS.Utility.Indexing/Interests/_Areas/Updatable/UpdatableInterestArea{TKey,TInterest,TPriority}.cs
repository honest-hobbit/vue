﻿namespace HQS.Utility.Indexing.Interests;

public class UpdatableInterestArea<TKey, TInterest, TPriority> : AbstractUpdatableInterestArea<TKey, TInterest, TPriority>
{
	public UpdatableInterestArea(
		TInterest interest,
		Func<TKey, TPriority> getPriority,
		Func<IObservable<TKey>, IObservable<TKey>> filterKeys = null)
		: base(interest, getPriority, filterKeys)
	{
	}

	public new void SetArea(IReadOnlySet<TKey> area) => base.SetArea(area);
}
