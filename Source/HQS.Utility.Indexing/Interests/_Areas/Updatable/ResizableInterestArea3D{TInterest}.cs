﻿namespace HQS.Utility.Indexing.Interests;

public class ResizableInterestArea3D<TInterest> : ResizableInterestArea<Index3D, TInterest, int>
{
	public ResizableInterestArea3D(
		TInterest interest,
		Func<int, IReadOnlySet<Index3D>> createArea,
		ObservableChunkIndex.Threshold threshold = ObservableChunkIndex.Threshold.TwoChunks)
		: base(interest, createArea, InterestArea.WithPriorities.FromOffset, indices => indices.FilteredThreshold(threshold))
	{
	}
}
