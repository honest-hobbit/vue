﻿namespace HQS.Utility.Indexing.Interests;

public static class ResizableInterestArea
{
	public static ResizableInterestArea3D<TInterest> CreateColumn<TInterest>(TInterest interest) =>
		new ResizableInterestArea3D<TInterest>(interest, radius => IndexShape.CreateColumn(radius * 2, radius * 2));

	public static ResizableInterestArea3D<TInterest> CreateCylinder<TInterest>(TInterest interest) =>
		new ResizableInterestArea3D<TInterest>(interest, radius => IndexShape.CreateCylinder(radius * 2, radius * 2));

	public static ResizableInterestArea3D<TInterest> CreateSphere<TInterest>(TInterest interest) =>
		new ResizableInterestArea3D<TInterest>(interest, radius => IndexShape.CreateSphere(radius * 2));

	public static ResizableInterestArea3D<TInterest> Create<TInterest>(TInterest interest, Shape3D shape)
	{
		Debug.Assert(Enumeration.IsDefined(shape));

		return shape switch
		{
			Shape3D.Column => CreateColumn(interest),
			Shape3D.Cylinder => CreateCylinder(interest),
			Shape3D.Sphere => CreateSphere(interest),
			_ => throw InvalidEnumArgument.CreateException(nameof(shape), shape),
		};
	}
}
