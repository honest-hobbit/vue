﻿namespace HQS.Utility.Indexing.Interests;

public class InterestSpere<TInterest> : AbstractUpdatableInterestArea3D<TInterest>
{
	private int radius = 0;

	public InterestSpere(TInterest interest)
		: base(interest, ObservableChunkIndex.Threshold.TwoChunks)
	{
	}

	public int Radius
	{
		get { return this.radius; }

		set
		{
			Debug.Assert(value >= 1);

			if (value == this.radius)
			{
				return;
			}

			this.radius = value;
			this.SetArea(IndexShape.CreateSphere(value * 2));
		}
	}
}
