﻿namespace HQS.Utility.Indexing.Interests;

public class InterestCylinder<TInterest> : AbstractVerticalInterestArea<TInterest>
{
	public InterestCylinder(TInterest interest)
		: base(interest)
	{
	}

	protected override void UpdateArea() => this.SetArea(IndexShape.CreateCylinder(this.RadialWidth * 2, this.RadialHeight * 2));
}
