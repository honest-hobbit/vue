﻿namespace HQS.Utility.Indexing.Interests;

public abstract class AbstractUpdatableInterestArea<TKey, TInterest, TPriority> :
	AbstractDisposable, IInterestArea<TKey, TInterest, TPriority>, IObserver<TKey>
{
	private readonly ReplaySubject<TKey> originChanged = new ReplaySubject<TKey>(1);

	private readonly ReplaySubject<IReadOnlySet<TKey>> areaChanged = new ReplaySubject<IReadOnlySet<TKey>>(1);

	private readonly IInterestArea<TKey, TInterest, TPriority> areaOfInterest;

	public AbstractUpdatableInterestArea(
		TInterest interest,
		Func<TKey, TPriority> getPriority,
		Func<IObservable<TKey>,
			IObservable<TKey>> filterKeys = null)
	{
		Debug.Assert(getPriority != null);

		filterKeys ??= Observable.DistinctUntilChanged;

		this.areaOfInterest = new InterestArea<TKey, TInterest, TPriority>(
			interest, getPriority, filterKeys(this.originChanged.AsObservableOnly()), this.areaChanged.DistinctUntilChanged());
	}

	/// <inheritdoc />
	public TInterest Interest => this.areaOfInterest.Interest;

	/// <inheritdoc />
	public IObservable<IReadOnlySet<TKey>> KeysChanged => this.areaOfInterest.KeysChanged;

	/// <inheritdoc />
	public IObservable<IReadOnlyDictionary<TKey, TPriority>> PrioritiesChanged => this.areaOfInterest.PrioritiesChanged;

	public bool IsCompleted { get; private set; } = false;

	public void SetOrigin(TKey origin)
	{
		Debug.Assert(!this.IsDisposed);

		if (!this.IsCompleted)
		{
			this.originChanged.OnNext(origin);
		}
	}

	public void Complete()
	{
		Debug.Assert(!this.IsDisposed);

		if (this.IsCompleted)
		{
			return;
		}

		this.IsCompleted = true;
		this.originChanged.OnCompleted();
		this.areaChanged.OnCompleted();
	}

	/// <inheritdoc />
	public void OnNext(TKey value) => this.SetOrigin(value);

	/// <inheritdoc />
	public void OnError(Exception error)
	{
		Debug.Assert(!this.IsDisposed);

		if (!this.IsCompleted)
		{
			this.IsCompleted = true;
			this.originChanged.OnError(error);
			this.areaChanged.OnError(error);
		}
	}

	/// <inheritdoc />
	public void OnCompleted() => this.Complete();

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.originChanged.Dispose();
		this.areaChanged.Dispose();
	}

	protected void SetArea(IReadOnlySet<TKey> area)
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(area != null);

		if (!this.IsCompleted)
		{
			this.areaChanged.OnNext(area);
		}
	}
}
