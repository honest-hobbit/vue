﻿namespace HQS.Utility.Indexing.Interests;

public class ResizableInterestArea<TKey, TInterest, TPriority> : AbstractUpdatableInterestArea<TKey, TInterest, TPriority>
{
	private readonly Func<int, IReadOnlySet<TKey>> createArea;

	private int radius = 0;

	public ResizableInterestArea(
		TInterest interest,
		Func<int, IReadOnlySet<TKey>> createArea,
		Func<TKey, TPriority> getPriority,
		Func<IObservable<TKey>, IObservable<TKey>> filterKeys = null)
		: base(interest, getPriority, filterKeys)
	{
		Debug.Assert(createArea != null);

		this.createArea = createArea;
	}

	public int Radius
	{
		get { return this.radius; }

		set
		{
			Debug.Assert(value >= 1);

			if (value == this.radius)
			{
				return;
			}

			this.radius = value;
			this.SetArea(this.createArea(value));
		}
	}
}
