﻿namespace HQS.Utility.Indexing.Interests;

public class InterestColumn<TInterest> : AbstractVerticalInterestArea<TInterest>
{
	public InterestColumn(TInterest interest)
		: base(interest)
	{
	}

	protected override void UpdateArea() => this.SetArea(IndexShape.CreateColumn(this.RadialWidth * 2, this.RadialHeight * 2));
}
