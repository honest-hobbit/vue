﻿namespace HQS.Utility.Indexing.Interests;

public abstract class AbstractUpdatableInterestArea3D<TInterest> : AbstractUpdatableInterestArea<Index3D, TInterest, int>
{
	public AbstractUpdatableInterestArea3D(TInterest interest, ObservableChunkIndex.Threshold threshold)
		: base(interest, InterestArea.WithPriorities.FromOffset, indices => indices.FilteredThreshold(threshold))
	{
	}
}
