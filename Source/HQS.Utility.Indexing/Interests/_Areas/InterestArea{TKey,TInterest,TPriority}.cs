﻿namespace HQS.Utility.Indexing.Interests;

public class InterestArea<TKey, TInterest, TPriority> : InterestArea<TKey, TInterest>, IInterestArea<TKey, TInterest, TPriority>
{
	public InterestArea(
		TInterest interest,
		Func<TKey, TPriority> getPriority,
		IObservable<TKey> originChanged,
		IObservable<IReadOnlySet<TKey>> keysChanged)
		: base(interest, originChanged, keysChanged)
	{
		Debug.Assert(getPriority != null);
		Debug.Assert(originChanged != null);
		Debug.Assert(keysChanged != null);

		this.PrioritiesChanged = originChanged.CombineLatest(
			keysChanged, (origin, keys) => (IReadOnlyDictionary<TKey, TPriority>)new PriorityDictionary(origin, keys, getPriority));
	}

	public InterestArea(
		TInterest interest, Func<TKey, TPriority> getPriority, IObservable<TKey> originChanged, IReadOnlySet<TKey> keys)
		: this(interest, getPriority, originChanged, Observable.Return(keys))
	{
		Debug.Assert(keys != null);
	}

	/// <inheritdoc />
	public IObservable<IReadOnlyDictionary<TKey, TPriority>> PrioritiesChanged { get; }

	protected class PriorityDictionary : IReadOnlyDictionary<TKey, TPriority>
	{
		private readonly TKey origin;

		private readonly IReadOnlySet<TKey> relativeKeys;

		private readonly Func<TKey, TPriority> getPriority;

		private readonly OffsetSet keys;

		public PriorityDictionary(TKey origin, IReadOnlySet<TKey> relativeKeys, Func<TKey, TPriority> getPriority)
		{
			Debug.Assert(relativeKeys != null);
			Debug.Assert(getPriority != null);

			this.origin = origin;
			this.relativeKeys = relativeKeys;
			this.getPriority = getPriority;
			this.keys = new OffsetSet(origin, relativeKeys);
		}

		/// <inheritdoc />
		public int Count => this.keys.Count;

		/// <inheritdoc />
		public IEnumerable<TKey> Keys => this.keys;

		/// <inheritdoc />
		public IEnumerable<TPriority> Values => this.relativeKeys.Select(key => this.getPriority(key));

		/// <inheritdoc />
		public TPriority this[TKey key]
		{
			get
			{
				IReadOnlyDictionaryContracts.Indexer(this, key);

				return this.getPriority(Operator.Subtract(key, this.origin));
			}
		}

		/// <inheritdoc />
		public bool ContainsKey(TKey key)
		{
			IReadOnlyDictionaryContracts.ContainsKey(key);

			return this.Keys.Contains(key);
		}

		/// <inheritdoc />
		public bool TryGetValue(TKey key, out TPriority value)
		{
			IReadOnlyDictionaryContracts.TryGetValue(key);

			if (this.ContainsKey(key))
			{
				value = this[key];
				return true;
			}
			else
			{
				value = default;
				return false;
			}
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<TKey, TPriority>> GetEnumerator() => this.relativeKeys.Select(
			key => KeyValuePair.Create(Operator.Add(key, this.origin), this.getPriority(key))).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
