﻿namespace HQS.Utility.Indexing.Interests;

public static class InterestArea
{
	public static IInterestArea<Index3D, TInterest> Create<TInterest>(
		TInterest interest, IObservable<IReadOnlySet<Index3D>> areaChanged, IObservable<Index3D> originChanged)
	{
		Debug.Assert(areaChanged != null);
		Debug.Assert(originChanged != null);

		return new InterestArea<Index3D, TInterest>(interest, originChanged, areaChanged);
	}

	public static IInterestArea<Index3D, TInterest> Create<TInterest>(
		TInterest interest, IReadOnlySet<Index3D> area, IObservable<Index3D> originChanged)
	{
		Debug.Assert(area != null);
		Debug.Assert(originChanged != null);

		return new InterestArea<Index3D, TInterest>(interest, originChanged, area);
	}

	public static class WithPriorities
	{
		public static int FromOffset(Index3D key) =>
			Math.Max(Math.Max(key.X.Abs(), key.Y.Abs()), key.Z.Abs());

		public static IInterestArea<Index3D, TInterest, int> Create<TInterest>(
			TInterest interest, IObservable<IReadOnlySet<Index3D>> areaChanged, IObservable<Index3D> originChanged)
		{
			Debug.Assert(areaChanged != null);
			Debug.Assert(originChanged != null);

			return new InterestArea<Index3D, TInterest, int>(interest, FromOffset, originChanged, areaChanged);
		}

		public static IInterestArea<Index3D, TInterest, int> Create<TInterest>(
			TInterest interest, IReadOnlySet<Index3D> area, IObservable<Index3D> originChanged)
		{
			Debug.Assert(area != null);
			Debug.Assert(originChanged != null);

			return new InterestArea<Index3D, TInterest, int>(interest, FromOffset, originChanged, area);
		}
	}
}
