﻿namespace HQS.Utility.Indexing.Interests;

public class InterestMapView<TKey, TInterest> : IInterestMapView<TKey, TInterest>
{
	private readonly IInterestMap<TKey, TInterest> map;

	public InterestMapView(IInterestMap<TKey, TInterest> map)
	{
		Debug.Assert(map != null);

		this.map = map;
	}

	/// <inheritdoc />
	public IObservable<KeyValuePair<TKey, SequentialPair<Try<TInterest>>>> InterestsChanged => this.map.InterestsChanged;

	/// <inheritdoc />
	public Try<TInterest> this[TKey key] => this.map[key];
}
