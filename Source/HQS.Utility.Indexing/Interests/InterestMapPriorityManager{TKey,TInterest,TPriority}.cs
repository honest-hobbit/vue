﻿namespace HQS.Utility.Indexing.Interests;

public class InterestMapPriorityManager<TKey, TInterest, TPriority> : AbstractDisposable
{
	private readonly Subject<IReadOnlyDictionary<TKey, TPriority>> prioritiesChanged =
		new Subject<IReadOnlyDictionary<TKey, TPriority>>();

	private readonly IInterestMap<TKey, TInterest> interestMap;

	public InterestMapPriorityManager(IInterestMap<TKey, TInterest> interestMap)
	{
		Debug.Assert(interestMap != null);

		this.interestMap = interestMap;
		this.InterestMap = interestMap.AsView();
		this.PriorityChanged = this.prioritiesChanged
			.SelectMany(priorities => priorities.Select(pair => KeyValuePair.Create(pair.Key, Try.Value(pair.Value))))
			.Merge(this.interestMap.InterestsChanged.Closed().Select(pair => KeyValuePair.Create(pair.Key, Try.None<TPriority>())));
	}

	public IObservable<KeyValuePair<TKey, Try<TPriority>>> PriorityChanged { get; }

	public IInterestMapView<TKey, TInterest> InterestMap { get; }

	public IDisposable SubscribeArea(IInterestArea<TKey, TInterest, TPriority> area)
	{
		Debug.Assert(area != null);

		return DisposableFactory.Combine(
			area.PrioritiesChanged.Subscribe(this.prioritiesChanged),
			this.interestMap.SubscribeArea(area));
	}

	/// <inheritdoc />
	protected override void ManagedDisposal() => this.prioritiesChanged.Dispose();
}
