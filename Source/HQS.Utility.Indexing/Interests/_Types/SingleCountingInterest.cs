﻿namespace HQS.Utility.Indexing.Interests;

public class SingleCountingInterest : IInterestMerger<int>
{
	private SingleCountingInterest()
	{
	}

	public static IInterestMerger<int> Merger { get; } = new SingleCountingInterest();

	/// <inheritdoc />
	public int None => 0;

	/// <inheritdoc />
	public bool IsNone(int interests) => interests == this.None;

	/// <inheritdoc />
	public bool TryAdd(int current, int add, out int result)
	{
		result = current + add;
		return result != current;
	}

	/// <inheritdoc />
	public bool TryRemove(int current, int remove, out int result)
	{
		result = current - remove;
		return result != current;
	}
}
