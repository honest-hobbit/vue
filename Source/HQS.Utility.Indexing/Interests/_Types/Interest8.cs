﻿namespace HQS.Utility.Indexing.Interests;

public readonly struct Interest8 : IEquatable<Interest8>, IEnumerable<int>
{
	private const int SegmentEnd = Length.OfByte.InBits;

	private readonly byte segment;

	private Interest8(int segment)
	{
		this.segment = (byte)segment;
	}

	public static IInterestMerger<Interest8> Merger { get; } = new InterestMerger();

	public static Interest8 None => new Interest8(0);

	public static Interest8 operator +(Interest8 lhs, Interest8 rhs) => new Interest8(lhs.segment | rhs.segment);

	public static Interest8 operator -(Interest8 lhs, Interest8 rhs) => new Interest8(lhs.segment & ~rhs.segment);

	public static bool operator ==(Interest8 lhs, Interest8 rhs) => lhs.Equals(rhs);

	public static bool operator !=(Interest8 lhs, Interest8 rhs) => !lhs.Equals(rhs);

	public static Interest8 New(int interest)
	{
		Debug.Assert(interest >= 0 && interest < SegmentEnd);

		return new Interest8(1 << interest);
	}

	public bool HasFlag(Interest8 flags) => (this.segment & flags.segment) == flags.segment;

	public bool HasAnyFlag(Interest8 flags) => (this.segment & flags.segment) != 0;

	/// <inheritdoc />
	public bool Equals(Interest8 other) => this.segment == other.segment;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.segment.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => InterestUtilities.ToString(this, SegmentEnd);

	/// <inheritdoc />
	public IEnumerator<int> GetEnumerator()
	{
		int bitmask = 1;
		int bits = this.segment;
		for (int count = 0; count < SegmentEnd; count++)
		{
			if ((bits & bitmask) == bitmask)
			{
				yield return count;
			}

			bits >>= 1;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private class InterestMerger : IInterestMerger<Interest8>
	{
		/// <inheritdoc />
		public Interest8 None => Interest8.None;

		/// <inheritdoc />
		public bool IsNone(Interest8 interests) => interests == this.None;

		/// <inheritdoc />
		public bool TryAdd(Interest8 current, Interest8 add, out Interest8 result)
		{
			result = current + add;
			return result != current;
		}

		/// <inheritdoc />
		public bool TryRemove(Interest8 current, Interest8 remove, out Interest8 result)
		{
			result = current - remove;
			return result != current;
		}
	}
}
