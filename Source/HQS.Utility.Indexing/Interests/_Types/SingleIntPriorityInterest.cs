﻿namespace HQS.Utility.Indexing.Interests;

public class SingleIntPriorityInterest : IInterestMerger<int>
{
	private SingleIntPriorityInterest()
	{
	}

	public static IInterestMerger<int> Merger { get; } = new SingleIntPriorityInterest();

	/// <inheritdoc />
	public int None => int.MaxValue;

	/// <inheritdoc />
	public bool IsNone(int interests) => interests == this.None;

	/// <inheritdoc />
	public bool TryAdd(int current, int add, out int result)
	{
		result = add;
		return result != current;
	}

	/// <inheritdoc />
	public bool TryRemove(int current, int remove, out int result)
	{
		result = remove == current ? this.None : current;
		return result != current;
	}
}
