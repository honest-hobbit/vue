﻿namespace HQS.Utility.Indexing.Interests;

internal static class InterestUtilities
{
	public static string ToString(IEnumerable<int> interests, int maxInterest)
	{
		Debug.Assert(interests != null);
		Debug.Assert(maxInterest >= 0);

		var chars = new char[maxInterest];
		for (var index = 0; index < chars.Length; index++)
		{
			chars[index] = '0';
		}

		var lastIndex = chars.Length - 1;
		foreach (var index in interests)
		{
			chars[lastIndex - index] = '1';
		}

		return new string(chars);
	}
}
