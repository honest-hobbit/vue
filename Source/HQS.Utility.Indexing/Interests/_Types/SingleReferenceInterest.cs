﻿namespace HQS.Utility.Indexing.Interests;

public static class SingleReferenceInterest
{
	public static IInterestMerger<T> Merger<T>()
		where T : class => InterestMerger<T>.Merger;

	private class InterestMerger<T> : IInterestMerger<T>
		where T : class
	{
		private InterestMerger()
		{
		}

		public static IInterestMerger<T> Merger { get; } = new InterestMerger<T>();

		/// <inheritdoc />
		public T None => null;

		/// <inheritdoc />
		public bool IsNone(T interests) => interests == this.None;

		/// <inheritdoc />
		public bool TryAdd(T current, T add, out T result)
		{
			result = add;
			return result != current;
		}

		/// <inheritdoc />
		public bool TryRemove(T current, T remove, out T result)
		{
			result = remove == current ? this.None : current;
			return result != current;
		}
	}
}
