﻿namespace HQS.Utility.Indexing.Interests;

public readonly struct Interest32 : IEquatable<Interest32>, IEnumerable<int>
{
	private const int SegmentEnd = Length.OfInt.InBits;

	private readonly int segment;

	private Interest32(int segment)
	{
		this.segment = segment;
	}

	public static IInterestMerger<Interest32> Merger { get; } = new InterestMerger();

	public static Interest32 None => new Interest32(0);

	public static Interest32 operator +(Interest32 lhs, Interest32 rhs) => new Interest32(lhs.segment | rhs.segment);

	public static Interest32 operator -(Interest32 lhs, Interest32 rhs) => new Interest32(lhs.segment & ~rhs.segment);

	public static bool operator ==(Interest32 lhs, Interest32 rhs) => lhs.Equals(rhs);

	public static bool operator !=(Interest32 lhs, Interest32 rhs) => !lhs.Equals(rhs);

	public static Interest32 New(int interest)
	{
		Debug.Assert(interest >= 0 && interest < SegmentEnd);

		return new Interest32(1 << interest);
	}

	public bool HasFlag(Interest32 flags) => (this.segment & flags.segment) == flags.segment;

	public bool HasAnyFlag(Interest32 flags) => (this.segment & flags.segment) != 0;

	/// <inheritdoc />
	public bool Equals(Interest32 other) => this.segment == other.segment;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.segment.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => InterestUtilities.ToString(this, SegmentEnd);

	/// <inheritdoc />
	public IEnumerator<int> GetEnumerator()
	{
		int bitmask = 1;
		int bits = this.segment;
		for (int count = 0; count < SegmentEnd; count++)
		{
			if ((bits & bitmask) == bitmask)
			{
				yield return count;
			}

			bits >>= 1;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private class InterestMerger : IInterestMerger<Interest32>
	{
		/// <inheritdoc />
		public Interest32 None => Interest32.None;

		/// <inheritdoc />
		public bool IsNone(Interest32 interests) => interests == this.None;

		/// <inheritdoc />
		public bool TryAdd(Interest32 current, Interest32 add, out Interest32 result)
		{
			result = current + add;
			return result != current;
		}

		/// <inheritdoc />
		public bool TryRemove(Interest32 current, Interest32 remove, out Interest32 result)
		{
			result = current - remove;
			return result != current;
		}
	}
}
