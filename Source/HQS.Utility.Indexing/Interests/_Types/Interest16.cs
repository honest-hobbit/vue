﻿namespace HQS.Utility.Indexing.Interests;

public readonly struct Interest16 : IEquatable<Interest16>, IEnumerable<int>
{
	private const int SegmentEnd = Length.OfUShort.InBits;

	private readonly ushort segment;

	private Interest16(int segment)
	{
		this.segment = (ushort)segment;
	}

	public static IInterestMerger<Interest16> Merger { get; } = new InterestMerger();

	public static Interest16 None => new Interest16(0);

	public static Interest16 operator +(Interest16 lhs, Interest16 rhs) => new Interest16(lhs.segment | rhs.segment);

	public static Interest16 operator -(Interest16 lhs, Interest16 rhs) => new Interest16(lhs.segment & ~rhs.segment);

	public static bool operator ==(Interest16 lhs, Interest16 rhs) => lhs.Equals(rhs);

	public static bool operator !=(Interest16 lhs, Interest16 rhs) => !lhs.Equals(rhs);

	public static Interest16 New(int interest)
	{
		Debug.Assert(interest >= 0 && interest < SegmentEnd);

		return new Interest16(1 << interest);
	}

	public bool HasFlag(Interest16 flags) => (this.segment & flags.segment) == flags.segment;

	public bool HasAnyFlag(Interest16 flags) => (this.segment & flags.segment) != 0;

	/// <inheritdoc />
	public bool Equals(Interest16 other) => this.segment == other.segment;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.segment.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => InterestUtilities.ToString(this, SegmentEnd);

	/// <inheritdoc />
	public IEnumerator<int> GetEnumerator()
	{
		int bitmask = 1;
		int bits = this.segment;
		for (int count = 0; count < SegmentEnd; count++)
		{
			if ((bits & bitmask) == bitmask)
			{
				yield return count;
			}

			bits >>= 1;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private class InterestMerger : IInterestMerger<Interest16>
	{
		/// <inheritdoc />
		public Interest16 None => Interest16.None;

		/// <inheritdoc />
		public bool IsNone(Interest16 interests) => interests == this.None;

		/// <inheritdoc />
		public bool TryAdd(Interest16 current, Interest16 add, out Interest16 result)
		{
			result = current + add;
			return result != current;
		}

		/// <inheritdoc />
		public bool TryRemove(Interest16 current, Interest16 remove, out Interest16 result)
		{
			result = current - remove;
			return result != current;
		}
	}
}
