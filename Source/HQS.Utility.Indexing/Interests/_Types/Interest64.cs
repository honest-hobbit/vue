﻿namespace HQS.Utility.Indexing.Interests;

public readonly struct Interest64 : IEquatable<Interest64>, IEnumerable<int>
{
	private const int SegmentEnd = Length.OfLong.InBits;

	private readonly long segment;

	private Interest64(long segment)
	{
		this.segment = segment;
	}

	public static IInterestMerger<Interest64> Merger { get; } = new InterestMerger();

	public static Interest64 None => new Interest64(0);

	public static Interest64 operator +(Interest64 lhs, Interest64 rhs) => new Interest64(lhs.segment | rhs.segment);

	public static Interest64 operator -(Interest64 lhs, Interest64 rhs) => new Interest64(lhs.segment & ~rhs.segment);

	public static bool operator ==(Interest64 lhs, Interest64 rhs) => lhs.Equals(rhs);

	public static bool operator !=(Interest64 lhs, Interest64 rhs) => !lhs.Equals(rhs);

	public static Interest64 New(int interest)
	{
		Debug.Assert(interest >= 0 && interest < SegmentEnd);

		return new Interest64(1L << interest);
	}

	public bool HasFlag(Interest64 flags) => (this.segment & flags.segment) == flags.segment;

	public bool HasAnyFlag(Interest64 flags) => (this.segment & flags.segment) != 0;

	/// <inheritdoc />
	public bool Equals(Interest64 other) => this.segment == other.segment;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => this.segment.GetHashCode();

	/// <inheritdoc />
	public override string ToString() => InterestUtilities.ToString(this, SegmentEnd);

	/// <inheritdoc />
	public IEnumerator<int> GetEnumerator()
	{
		long bitmask = 1;
		long bits = this.segment;
		for (int count = 0; count < SegmentEnd; count++)
		{
			if ((bits & bitmask) == bitmask)
			{
				yield return count;
			}

			bits >>= 1;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	private class InterestMerger : IInterestMerger<Interest64>
	{
		/// <inheritdoc />
		public Interest64 None => Interest64.None;

		/// <inheritdoc />
		public bool IsNone(Interest64 interests) => interests == this.None;

		/// <inheritdoc />
		public bool TryAdd(Interest64 current, Interest64 add, out Interest64 result)
		{
			result = current + add;
			return result != current;
		}

		/// <inheritdoc />
		public bool TryRemove(Interest64 current, Interest64 remove, out Interest64 result)
		{
			result = current - remove;
			return result != current;
		}
	}
}
