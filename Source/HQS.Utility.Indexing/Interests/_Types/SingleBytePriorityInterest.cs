﻿namespace HQS.Utility.Indexing.Interests;

public class SingleBytePriorityInterest : IInterestMerger<byte>
{
	private SingleBytePriorityInterest()
	{
	}

	public static IInterestMerger<byte> Merger { get; } = new SingleBytePriorityInterest();

	/// <inheritdoc />
	public byte None => byte.MaxValue;

	/// <inheritdoc />
	public bool IsNone(byte interests) => interests == this.None;

	/// <inheritdoc />
	public bool TryAdd(byte current, byte add, out byte result)
	{
		result = add;
		return result != current;
	}

	/// <inheritdoc />
	public bool TryRemove(byte current, byte remove, out byte result)
	{
		result = remove == current ? this.None : current;
		return result != current;
	}
}
