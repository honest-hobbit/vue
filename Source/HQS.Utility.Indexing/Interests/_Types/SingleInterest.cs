﻿namespace HQS.Utility.Indexing.Interests;

public class SingleInterest : IInterestMerger<bool>
{
	private SingleInterest()
	{
	}

	public static IInterestMerger<bool> Merger { get; } = new SingleInterest();

	/// <inheritdoc />
	public bool None => false;

	/// <inheritdoc />
	public bool IsNone(bool interests) => interests == this.None;

	/// <inheritdoc />
	public bool TryAdd(bool current, bool add, out bool result)
	{
		result = add;
		return result != current;
	}

	/// <inheritdoc />
	public bool TryRemove(bool current, bool remove, out bool result)
	{
		result = remove == current ? this.None : current;
		return result != current;
	}
}
