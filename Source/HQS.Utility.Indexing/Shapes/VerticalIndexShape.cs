﻿namespace HQS.Utility.Indexing.Shapes;

public class VerticalIndexShape : IIndexShape<Index3D>
{
	public VerticalIndexShape(IIndexShape<Index2D> baseShape, int height)
	{
		Debug.Assert(baseShape != null);
		Debug.Assert(height >= 1);

		this.BaseShape = baseShape;

		this.Dimensions = new Index3D(baseShape.Dimensions.X, height, baseShape.Dimensions.Y);
		this.LowerBounds = new Index3D(baseShape.LowerBounds.X, -height / 2, baseShape.LowerBounds.Y);
		this.UpperBounds = this.LowerBounds + this.Dimensions - new Index3D(1);
	}

	public int Height => this.Dimensions.Y;

	/// <inheritdoc />
	public int Count => this.BaseShape.Count * this.Height;

	/// <inheritdoc />
	public Index3D Dimensions { get; }

	/// <inheritdoc />
	public Index3D LowerBounds { get; }

	/// <inheritdoc />
	public Index3D UpperBounds { get; }

	protected IIndexShape<Index2D> BaseShape { get; }

	public static bool operator ==(VerticalIndexShape lhs, VerticalIndexShape rhs) => lhs.Equals(rhs);

	public static bool operator !=(VerticalIndexShape lhs, VerticalIndexShape rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public override bool Equals(object obj)
	{
		if (!this.TypesEqualNullSafe(obj))
		{
			return false;
		}

		var other = (VerticalIndexShape)obj;
		return other.Height == this.Height && other.BaseShape.Equals(this.BaseShape);
	}

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Height, this.BaseShape);

	/// <inheritdoc />
	public bool Contains(Index3D index) =>
		index.Y >= this.LowerBounds.Y && index.Y <= this.UpperBounds.Y && this.BaseShape.Contains(index.ProjectDownYAxis());

	/// <inheritdoc />
	public IEnumerator<Index3D> GetEnumerator() =>
		ShapeIndices.Columns(this.BaseShape, this.LowerBounds.Y, this.UpperBounds.Y).GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
