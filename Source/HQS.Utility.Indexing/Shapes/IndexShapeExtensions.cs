﻿namespace HQS.Utility.Indexing.Shapes;

/// <summary>
/// Provides extension methods for shapes made of indices.
/// </summary>
public static class IndexShapeExtensions
{
	public static IIndexable<TIndex, TValue> ToUnassignedMask<TIndex, TValue>(this IIndexShape<TIndex> shape)
		where TIndex : struct, IIndex<TIndex> => shape.ToUnassignedMask<TIndex, TValue>(IndexUtility.Zero<TIndex>());

	public static IIndexable<TIndex, TValue> ToUnassignedMask<TIndex, TValue>(this IIndexShape<TIndex> shape, TIndex padding)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(shape != null);

		var dimensions = Operator.Add(shape.Dimensions, Operator.Add(padding, padding));
		var result = new IndexableDictionary<TIndex, TValue>(dimensions);
		return OffsetIndexable.CenterOnZero(result, roundUp: true);
	}

	public static IIndexable<TIndex, bool> ToContainsMask<TIndex>(this IIndexShape<TIndex> shape)
		where TIndex : struct, IIndex<TIndex> => shape.ToContainsMask(IndexUtility.Zero<TIndex>());

	public static IIndexable<TIndex, bool> ToContainsMask<TIndex>(this IIndexShape<TIndex> shape, TIndex padding)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(shape != null);

		var result = ToUnassignedMask<TIndex, bool>(shape, padding);
		foreach (var index in result.GetIndices())
		{
			result[index] = shape.Contains(index);
		}

		return result;
	}

	public static IIndexable<TIndex, bool> ToBoolMask<TIndex>(this IIndexShape<TIndex> shape)
		where TIndex : struct, IIndex<TIndex> => shape.ToBoolMask(IndexUtility.Zero<TIndex>());

	public static IIndexable<TIndex, bool> ToBoolMask<TIndex>(this IIndexShape<TIndex> shape, TIndex padding)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(shape != null);

		var result = ToUnassignedMask<TIndex, bool>(shape, padding);
		foreach (var index in shape)
		{
			result[index] = true;
		}

		return result;
	}

	public static IIndexable<TIndex, int> ToOrderedMask<TIndex>(this IIndexShape<TIndex> shape)
		where TIndex : struct, IIndex<TIndex> => shape.ToOrderedMask(IndexUtility.Zero<TIndex>());

	public static IIndexable<TIndex, int> ToOrderedMask<TIndex>(this IIndexShape<TIndex> shape, TIndex padding)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(shape != null);

		var result = ToUnassignedMask<TIndex, int>(shape, padding);
		int count = 1;
		foreach (var index in shape)
		{
			result[index] = count;
			count++;
		}

		return result;
	}

	public static IIndexable<TIndex, int> ToCountedMask<TIndex>(this IIndexShape<TIndex> shape)
		where TIndex : struct, IIndex<TIndex> => shape.ToCountedMask(IndexUtility.Zero<TIndex>());

	public static IIndexable<TIndex, int> ToCountedMask<TIndex>(this IIndexShape<TIndex> shape, TIndex padding)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(shape != null);

		var result = ToUnassignedMask<TIndex, int>(shape, padding);
		foreach (var index in shape)
		{
			result[index] = result[index] + 1;
		}

		return result;
	}

	public static string ToGridString<T>(this IIndexable<Index2D, T> shape) =>
		shape.ToGridString(value => value.ToStringNullSafe());

	public static string ToGridString<T>(this IIndexable<Index2D, T> shape, Func<T, string> toString)
	{
		Debug.Assert(shape != null);
		Debug.Assert(toString != null);

		var result = new StringBuilder(shape.Dimensions.Coordinates().Multiply() * 3);
		var lowerBounds = shape.LowerBounds;
		var upperBounds = shape.UpperBounds;

		for (int iY = upperBounds.Y; iY >= lowerBounds.Y; iY--)
		{
			for (int iX = lowerBounds.X; iX <= upperBounds.X; iX++)
			{
				result.Append(toString(shape[new Index2D(iX, iY)]));
				result.Append(' ');
			}

			result.AppendLine();
		}

		return result.ToString();
	}
}
