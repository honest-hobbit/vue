﻿namespace HQS.Utility.Indexing.Shapes;

public static class IndexShape
{
	public static IndexCircle CreateCircle(int diameter) => IndexCircle.Create(diameter);

	public static IndexSquare CreateSquare(int length) => IndexSquare.Create(length);

	public static IndexSphere CreateSphere(int diameter) => IndexSphere.Create(diameter);

	public static IndexColumn CreateCube(int length) => IndexColumn.CreateCube(length);

	public static IndexColumn CreateColumn(int length, int height) => IndexColumn.Create(length, height);

	public static IndexCylinder CreateCylinder(int diameter, int height) => IndexCylinder.Create(diameter, height);

	public static IIndexShape<Index2D> Create(Shape2D shape, int length)
	{
		Debug.Assert(Enumeration.IsDefined(shape));
		Debug.Assert(length >= 1);

		return shape switch
		{
			Shape2D.Circle => CreateCircle(length),
			Shape2D.Square => CreateSquare(length),
			_ => throw InvalidEnumArgument.CreateException(nameof(shape), shape),
		};
	}

	public static IIndexShape<Index3D> Create(Shape3D shape, int length)
	{
		Debug.Assert(Enumeration.IsDefined(shape));
		Debug.Assert(length >= 1);

		return shape switch
		{
			Shape3D.Column => CreateColumn(length, length),
			Shape3D.Cylinder => CreateCylinder(length, length),
			Shape3D.Sphere => CreateSphere(length),
			_ => throw InvalidEnumArgument.CreateException(nameof(shape), shape),
		};
	}
}
