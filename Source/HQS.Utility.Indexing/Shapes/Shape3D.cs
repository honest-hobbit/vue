﻿namespace HQS.Utility.Indexing.Shapes;

public enum Shape3D
{
	Column,

	Cylinder,

	Sphere,
}
