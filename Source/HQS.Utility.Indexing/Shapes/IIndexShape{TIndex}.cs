﻿namespace HQS.Utility.Indexing.Shapes;

public interface IIndexShape<TIndex> : IReadOnlySet<TIndex>, IIndexingBounds<TIndex>
	where TIndex : struct, IIndex<TIndex>
{
}
