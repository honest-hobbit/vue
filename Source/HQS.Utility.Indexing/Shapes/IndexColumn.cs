﻿namespace HQS.Utility.Indexing.Shapes;

public class IndexColumn : VerticalIndexShape
{
	private IndexColumn(int length, int height)
		: base(IndexSquare.Create(length), height)
	{
	}

	public int Length => this.Dimensions.X;

	public static bool operator ==(IndexColumn lhs, IndexColumn rhs) => lhs.Equals(rhs);

	public static bool operator !=(IndexColumn lhs, IndexColumn rhs) => !lhs.Equals(rhs);

	public static IndexColumn CreateCube(int length)
	{
		Debug.Assert(length >= 1);

		return new IndexColumn(length, length);
	}

	public static IndexColumn Create(int length, int height)
	{
		Debug.Assert(length >= 1);
		Debug.Assert(height >= 1);

		return new IndexColumn(length, height);
	}

	/// <inheritdoc />
	public override bool Equals(object obj) => base.Equals(obj);

	/// <inheritdoc />
	public override int GetHashCode() => base.GetHashCode();
}
