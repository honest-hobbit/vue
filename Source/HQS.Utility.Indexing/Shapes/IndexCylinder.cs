﻿namespace HQS.Utility.Indexing.Shapes;

public class IndexCylinder : VerticalIndexShape
{
	private IndexCylinder(int diameter, int height)
		: base(IndexCircle.Create(diameter), height)
	{
	}

	public int Diameter => this.Dimensions.X;

	public static bool operator ==(IndexCylinder lhs, IndexCylinder rhs) => lhs.Equals(rhs);

	public static bool operator !=(IndexCylinder lhs, IndexCylinder rhs) => !lhs.Equals(rhs);

	public static IndexCylinder Create(int diameter, int height)
	{
		Debug.Assert(diameter >= 1);
		Debug.Assert(height >= 1);

		return new IndexCylinder(diameter, height);
	}

	/// <inheritdoc />
	public override bool Equals(object obj) => base.Equals(obj);

	/// <inheritdoc />
	public override int GetHashCode() => base.GetHashCode();
}
