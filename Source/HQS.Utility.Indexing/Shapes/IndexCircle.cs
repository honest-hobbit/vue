﻿namespace HQS.Utility.Indexing.Shapes;

public abstract class IndexCircle : EquidimensionalIndexShape<Index2D>
{
	private IndexCircle(int diameter)
		: base(diameter)
	{
		this.RadiusSquared = ShapeIndices.GetRadiusSquared(this.Layers);
		this.Count = this.Count();  // don't use CountExtended(), it would cause an infinite loop
	}

	public int Diameter => this.Length;

	/// <inheritdoc />
	public override int Count { get; }

	protected float RadiusSquared { get; }

	public static bool operator ==(IndexCircle lhs, IndexCircle rhs) => lhs.Equals(rhs);

	public static bool operator !=(IndexCircle lhs, IndexCircle rhs) => !lhs.Equals(rhs);

	public static IndexCircle Create(int diameter)
	{
		Debug.Assert(diameter >= 1);

		return diameter.IsEven() ? (IndexCircle)new EvenCircle(diameter) : new OddCircle(diameter);
	}

	/// <inheritdoc />
	public override bool Equals(object obj) => base.Equals(obj);

	/// <inheritdoc />
	public override int GetHashCode() => base.GetHashCode();

	/// <inheritdoc />
	public override bool Contains(Index2D index) => this.GetDistanceFast(index) < this.RadiusSquared;

	protected abstract int GetDistanceFast(Index2D index);

	private class OddCircle : IndexCircle
	{
		public OddCircle(int diameter)
			: base(diameter)
		{
		}

		/// <inheritdoc />
		public override IEnumerator<Index2D> GetEnumerator() =>
			ShapeIndices.Circle.RangeOdd(this.Layers, this.RadiusSquared).GetEnumerator();

		/// <inheritdoc />
		protected override int GetDistanceFast(Index2D index) => (index.X * index.X) + (index.Y * index.Y);
	}

	private class EvenCircle : IndexCircle
	{
		public EvenCircle(int diameter)
			: base(diameter)
		{
		}

		/// <inheritdoc />
		public override IEnumerator<Index2D> GetEnumerator() =>
			ShapeIndices.Circle.RangeEven(this.Layers, this.RadiusSquared).GetEnumerator();

		/// <inheritdoc />
		protected override int GetDistanceFast(Index2D index)
		{
			int x = index.X >= 0 ? index.X : -index.X - 1;
			int y = index.Y >= 0 ? index.Y : -index.Y - 1;
			return (x * x) + (y * y);
		}
	}
}
