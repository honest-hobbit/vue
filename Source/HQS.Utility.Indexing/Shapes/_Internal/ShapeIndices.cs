﻿namespace HQS.Utility.Indexing.Shapes;

internal static class ShapeIndices
{
	private delegate void UpdateYieldBounds(int layer, ref int endIndex, ref bool yieldDiagonalAxis);

	public static float GetRadiusSquared(int layers)
	{
		Debug.Assert(layers >= 0);

		float radius = (layers - .5f).ClampLower(0);
		return radius * radius;
	}

	public static IEnumerable<Index3D> Columns(IEnumerable<Index2D> baseIndices, int minHeight, int maxHeight)
	{
		Debug.Assert(baseIndices != null);
		Debug.Assert(minHeight <= maxHeight);

		int midpoint = MathUtility.IntegerMidpoint(minHeight, maxHeight, roundUp: false);

		foreach (var index in baseIndices)
		{
			yield return new Index3D(index.X, midpoint, index.Y);
			int yUp = midpoint + 1;
			int yDown = midpoint - 1;
			bool continueLoop = true;
			while (continueLoop)
			{
				continueLoop = false;

				if (yUp <= maxHeight)
				{
					yield return new Index3D(index.X, yUp, index.Y);
					yUp++;
					continueLoop = true;
				}

				if (yDown >= minHeight)
				{
					yield return new Index3D(index.X, yDown, index.Y);
					yDown--;
					continueLoop = true;
				}
			}
		}
	}

	private static IEnumerable<Index2D> RangeOdd(int layers, UpdateYieldBounds updateYieldBounds)
	{
		Debug.Assert(layers >= 0);
		Debug.Assert(updateYieldBounds != null);

		if (layers == 0)
		{
			yield break;
		}

		// origin special case
		yield return new Index2D(0, 0);

		int endIndex = 0;
		bool yieldDiagonalAxis = true;

		// iterate each layer of the circle
		for (int layerCount = 1; layerCount < layers; layerCount++)
		{
			updateYieldBounds(layerCount, ref endIndex, ref yieldDiagonalAxis);

			// orthogonal axis special case
			yield return new Index2D(0, layerCount);
			yield return new Index2D(layerCount, 0);
			yield return new Index2D(0, -layerCount);
			yield return new Index2D(-layerCount, 0);

			// iterate the scan line of the circle
			for (int index = 1; index <= endIndex; index++)
			{
				yield return new Index2D(index, layerCount);
				yield return new Index2D(layerCount, index);

				yield return new Index2D(layerCount, -index);
				yield return new Index2D(index, -layerCount);

				yield return new Index2D(-index, -layerCount);
				yield return new Index2D(-layerCount, -index);

				yield return new Index2D(-layerCount, index);
				yield return new Index2D(-index, layerCount);
			}

			// diagonal axis special case
			if (yieldDiagonalAxis)
			{
				yield return new Index2D(layerCount, layerCount);
				yield return new Index2D(layerCount, -layerCount);
				yield return new Index2D(-layerCount, -layerCount);
				yield return new Index2D(-layerCount, layerCount);
			}

			if (!yieldDiagonalAxis)
			{
				endIndex--;
			}
		}
	}

	private static IEnumerable<Index2D> RangeEven(int layers, UpdateYieldBounds updateYieldBounds)
	{
		Debug.Assert(layers >= 0);
		Debug.Assert(updateYieldBounds != null);

		if (layers == 0)
		{
			yield break;
		}

		foreach (var index in RangeEvenQuadrant(layers, updateYieldBounds))
		{
			yield return index;
			yield return new Index2D(index.X, -index.Y - 1);
			yield return new Index2D(-index.X - 1, -index.Y - 1);
			yield return new Index2D(-index.X - 1, index.Y);
		}
	}

	private static IEnumerable<Index2D> RangeEvenQuadrant(int layers, UpdateYieldBounds updateYieldBounds)
	{
		Debug.Assert(layers > 0);
		Debug.Assert(updateYieldBounds != null);

		// origin special case
		yield return new Index2D(0, 0);

		int endIndex = 0;
		bool yieldDiagonalAxis = true;

		// iterate each layer of the circle
		for (int layerCount = 1; layerCount < layers; layerCount++)
		{
			updateYieldBounds(layerCount, ref endIndex, ref yieldDiagonalAxis);

			// orthogonal axis special case
			yield return new Index2D(0, layerCount);
			yield return new Index2D(layerCount, 0);

			// iterate the scan line of the circle
			for (int index = 1; index <= endIndex; index++)
			{
				yield return new Index2D(index, layerCount);
				yield return new Index2D(layerCount, index);
			}

			// diagonal axis special case
			if (yieldDiagonalAxis)
			{
				yield return new Index2D(layerCount, layerCount);
			}

			if (!yieldDiagonalAxis)
			{
				endIndex--;
			}
		}
	}

	public static class Square
	{
		public static IEnumerable<Index2D> RangeOdd(int layers)
		{
			Debug.Assert(layers >= 0);

			return ShapeIndices.RangeOdd(layers, SquareYieldBounds);
		}

		public static IEnumerable<Index2D> RangeEven(int layers)
		{
			Debug.Assert(layers >= 0);

			return ShapeIndices.RangeEven(layers, SquareYieldBounds);
		}

		private static void SquareYieldBounds(int layer, ref int endIndex, ref bool yieldDiagonalAxis) => endIndex = layer - 1;
	}

	public static class Circle
	{
		public static IEnumerable<Index2D> RangeOdd(int layers, float radiusSquared)
		{
			Debug.Assert(layers >= 0);
			Debug.Assert(radiusSquared >= 0);

			return ShapeIndices.RangeOdd(layers, GetUpdateYieldBounds(radiusSquared));
		}

		public static IEnumerable<Index2D> RangeEven(int layers, float radiusSquared)
		{
			Debug.Assert(layers >= 0);
			Debug.Assert(radiusSquared >= 0);

			return ShapeIndices.RangeEven(layers, GetUpdateYieldBounds(radiusSquared));
		}

		public static void UpdateBounds(int layer, float radiusSquared, ref int endIndex, ref bool yieldDiagonalAxis)
		{
			Debug.Assert(layer > 0);
			Debug.Assert(radiusSquared >= 0);

			int distanceAxis1 = layer * layer;

			if (yieldDiagonalAxis)
			{
				endIndex = layer - 1;
				yieldDiagonalAxis = distanceAxis1 + distanceAxis1 < radiusSquared;
			}

			if (!yieldDiagonalAxis)
			{
				// if not yielding the diagonal axis then shift endIndex back towards the
				// orthogonal axis until a point is found that will be iterated
				for (; endIndex >= 1; endIndex--)
				{
					int distanceAxis2 = endIndex * endIndex;
					if (distanceAxis1 + distanceAxis2 < radiusSquared)
					{
						break;
					}
				}
			}
		}

		private static UpdateYieldBounds GetUpdateYieldBounds(float radiusSquared)
		{
			Debug.Assert(radiusSquared >= 0);

			return (int layer, ref int endIndex, ref bool yieldDiagonalAxis) =>
				UpdateBounds(layer, radiusSquared, ref endIndex, ref yieldDiagonalAxis);
		}
	}

	public static class Sphere
	{
		public static IEnumerable<Index3D> RangeOdd(int layers, float radiusSquared)
		{
			Debug.Assert(layers >= 0);
			Debug.Assert(radiusSquared >= 0);

			if (layers == 0)
			{
				yield break;
			}

			// origin special case
			yield return new Index3D(0, 0, 0);

			int orthogonalHeight = layers - 1;
			for (int height = 1; height <= orthogonalHeight; height++)
			{
				yield return new Index3D(0, height, 0);
				yield return new Index3D(0, -height, 0);
			}

			int endIndex = 0;
			bool yieldDiagonalAxis = true;

			// iterate each layer of the circle
			for (int layerCount = 1; layerCount < layers; layerCount++)
			{
				Circle.UpdateBounds(layerCount, radiusSquared, ref endIndex, ref yieldDiagonalAxis);

				int distanceAxis1 = layerCount * layerCount;
				orthogonalHeight = DecreaseHeight(orthogonalHeight, distanceAxis1, radiusSquared);

				// orthogonal axis special case
				yield return new Index3D(0, 0, layerCount);
				yield return new Index3D(layerCount, 0, 0);
				yield return new Index3D(0, 0, -layerCount);
				yield return new Index3D(-layerCount, 0, 0);

				for (int height = 1; height <= orthogonalHeight; height++)
				{
					yield return new Index3D(0, height, layerCount);
					yield return new Index3D(0, -height, layerCount);

					yield return new Index3D(layerCount, height, 0);
					yield return new Index3D(layerCount, -height, 0);

					yield return new Index3D(0, height, -layerCount);
					yield return new Index3D(0, -height, -layerCount);

					yield return new Index3D(-layerCount, height, 0);
					yield return new Index3D(-layerCount, -height, 0);
				}

				// iterate the scan line of the circle
				int endHeight = orthogonalHeight;
				for (int index = 1; index <= endIndex; index++)
				{
					yield return new Index3D(index, 0, layerCount);
					yield return new Index3D(layerCount, 0, index);

					yield return new Index3D(layerCount, 0, -index);
					yield return new Index3D(index, 0, -layerCount);

					yield return new Index3D(-index, 0, -layerCount);
					yield return new Index3D(-layerCount, 0, -index);

					yield return new Index3D(-layerCount, 0, index);
					yield return new Index3D(-index, 0, layerCount);

					int distanceAxis2 = index * index;
					endHeight = DecreaseHeight(endHeight, distanceAxis1 + distanceAxis2, radiusSquared);
					for (int height = 1; height <= endHeight; height++)
					{
						yield return new Index3D(index, height, layerCount);
						yield return new Index3D(index, -height, layerCount);
						yield return new Index3D(layerCount, height, index);
						yield return new Index3D(layerCount, -height, index);

						yield return new Index3D(layerCount, height, -index);
						yield return new Index3D(layerCount, -height, -index);
						yield return new Index3D(index, height, -layerCount);
						yield return new Index3D(index, -height, -layerCount);

						yield return new Index3D(-index, height, -layerCount);
						yield return new Index3D(-index, -height, -layerCount);
						yield return new Index3D(-layerCount, height, -index);
						yield return new Index3D(-layerCount, -height, -index);

						yield return new Index3D(-layerCount, height, index);
						yield return new Index3D(-layerCount, -height, index);
						yield return new Index3D(-index, height, layerCount);
						yield return new Index3D(-index, -height, layerCount);
					}
				}

				// diagonal axis special case
				if (yieldDiagonalAxis)
				{
					yield return new Index3D(layerCount, 0, layerCount);
					yield return new Index3D(layerCount, 0, -layerCount);
					yield return new Index3D(-layerCount, 0, -layerCount);
					yield return new Index3D(-layerCount, 0, layerCount);

					endHeight = DecreaseHeight(endHeight, distanceAxis1 + distanceAxis1, radiusSquared);
					for (int height = 1; height <= endHeight; height++)
					{
						yield return new Index3D(layerCount, height, layerCount);
						yield return new Index3D(layerCount, -height, layerCount);

						yield return new Index3D(layerCount, height, -layerCount);
						yield return new Index3D(layerCount, -height, -layerCount);

						yield return new Index3D(-layerCount, height, -layerCount);
						yield return new Index3D(-layerCount, -height, -layerCount);

						yield return new Index3D(-layerCount, height, layerCount);
						yield return new Index3D(-layerCount, -height, layerCount);
					}
				}

				if (!yieldDiagonalAxis)
				{
					endIndex--;
				}
			}
		}

		public static IEnumerable<Index3D> RangeEven(int layers, float radiusSquared)
		{
			Debug.Assert(layers >= 0);
			Debug.Assert(radiusSquared >= 0);

			if (layers == 0)
			{
				yield break;
			}

			foreach (var index in RangeEvenOctant(layers, radiusSquared))
			{
				yield return index;
				yield return new Index3D(index.X, -index.Y - 1, index.Z);
				yield return new Index3D(index.X, index.Y, -index.Z - 1);
				yield return new Index3D(index.X, -index.Y - 1, -index.Z - 1);
				yield return new Index3D(-index.X - 1, index.Y, -index.Z - 1);
				yield return new Index3D(-index.X - 1, -index.Y - 1, -index.Z - 1);
				yield return new Index3D(-index.X - 1, index.Y, index.Z);
				yield return new Index3D(-index.X - 1, -index.Y - 1, index.Z);
			}
		}

		private static IEnumerable<Index3D> RangeEvenOctant(int layers, float radiusSquared)
		{
			Debug.Assert(layers > 0);
			Debug.Assert(radiusSquared >= 0);

			int orthogonalHeight = layers - 1;

			// origin special case
			for (int height = 0; height <= orthogonalHeight; height++)
			{
				yield return new Index3D(0, height, 0);
			}

			int endIndex = 0;
			bool yieldDiagonalAxis = true;

			// iterate each layer of the circle
			for (int layerCount = 1; layerCount < layers; layerCount++)
			{
				Circle.UpdateBounds(layerCount, radiusSquared, ref endIndex, ref yieldDiagonalAxis);

				int distanceAxis1 = layerCount * layerCount;
				orthogonalHeight = DecreaseHeight(orthogonalHeight, distanceAxis1, radiusSquared);

				// orthogonal axis special case
				for (int height = 0; height <= orthogonalHeight; height++)
				{
					yield return new Index3D(0, height, layerCount);
					yield return new Index3D(layerCount, height, 0);
				}

				// iterate the scan line of the circle
				int endHeight = orthogonalHeight;
				for (int index = 1; index <= endIndex; index++)
				{
					int distanceAxis2 = index * index;
					endHeight = DecreaseHeight(endHeight, distanceAxis1 + distanceAxis2, radiusSquared);

					for (int height = 0; height <= endHeight; height++)
					{
						yield return new Index3D(index, height, layerCount);
						yield return new Index3D(layerCount, height, index);
					}
				}

				// diagonal axis special case
				if (yieldDiagonalAxis)
				{
					endHeight = DecreaseHeight(endHeight, distanceAxis1 + distanceAxis1, radiusSquared);
					for (int height = 0; height <= endHeight; height++)
					{
						yield return new Index3D(layerCount, height, layerCount);
					}
				}

				if (!yieldDiagonalAxis)
				{
					endIndex--;
				}
			}
		}

		private static int DecreaseHeight(int height, int horizontalDistanceSquared, float radiusSquared)
		{
			Debug.Assert(horizontalDistanceSquared >= 0);
			Debug.Assert(radiusSquared >= 0);

			for (; height >= 1; height--)
			{
				if (horizontalDistanceSquared + (height * height) < radiusSquared)
				{
					break;
				}
			}

			return height;
		}
	}
}
