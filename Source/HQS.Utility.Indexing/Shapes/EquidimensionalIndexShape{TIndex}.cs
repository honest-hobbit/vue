﻿namespace HQS.Utility.Indexing.Shapes;

public abstract class EquidimensionalIndexShape<TIndex> : IIndexShape<TIndex>
	where TIndex : struct, IIndex<TIndex>
{
	protected EquidimensionalIndexShape(int length)
	{
		Debug.Assert(length >= 1);

		this.Dimensions = IndexUtility.Create<TIndex>(length);
		this.LowerBounds = IndexUtility.Create<TIndex>(-length / 2);
		this.UpperBounds = Operator.Add(this.LowerBounds, Operator.Subtract(this.Dimensions, IndexUtility.One<TIndex>()));
	}

	/// <inheritdoc />
	public abstract int Count { get; }

	/// <inheritdoc />
	public TIndex Dimensions { get; }

	/// <inheritdoc />
	public TIndex LowerBounds { get; }

	/// <inheritdoc />
	public TIndex UpperBounds { get; }

	protected int Layers => MathUtility.DivideRoundUp(this.Length, 2);

	protected int Length => this.Dimensions[0];

	public static bool operator ==(EquidimensionalIndexShape<TIndex> lhs, EquidimensionalIndexShape<TIndex> rhs) => lhs.Equals(rhs);

	public static bool operator !=(EquidimensionalIndexShape<TIndex> lhs, EquidimensionalIndexShape<TIndex> rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public override bool Equals(object obj)
	{
		if (!this.TypesEqualNullSafe(obj))
		{
			return false;
		}

		var other = (EquidimensionalIndexShape<TIndex>)obj;
		return other.Length == this.Length;
	}

	/// <inheritdoc />
	public override int GetHashCode() => this.Length.GetHashCode();

	/// <inheritdoc />
	public abstract bool Contains(TIndex index);

	/// <inheritdoc />
	public abstract IEnumerator<TIndex> GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
