﻿namespace HQS.Utility.Indexing.Shapes;

public abstract class IndexSphere : EquidimensionalIndexShape<Index3D>
{
	private IndexSphere(int diameter)
		: base(diameter)
	{
		this.RadiusSquared = ShapeIndices.GetRadiusSquared(this.Layers);
		this.Count = this.Count();  // don't use CountExtended(), it would cause an infinite loop
	}

	public int Diameter => this.Length;

	/// <inheritdoc />
	public override int Count { get; }

	protected float RadiusSquared { get; }

	public static bool operator ==(IndexSphere lhs, IndexSphere rhs) => lhs.Equals(rhs);

	public static bool operator !=(IndexSphere lhs, IndexSphere rhs) => !lhs.Equals(rhs);

	public static IndexSphere Create(int diameter)
	{
		Debug.Assert(diameter >= 1);

		return diameter.IsEven() ? (IndexSphere)new EvenSphere(diameter) : new OddSphere(diameter);
	}

	/// <inheritdoc />
	public override bool Equals(object obj) => base.Equals(obj);

	/// <inheritdoc />
	public override int GetHashCode() => base.GetHashCode();

	/// <inheritdoc />
	public override bool Contains(Index3D index) => this.GetDistanceFast(index) < this.RadiusSquared;

	protected abstract int GetDistanceFast(Index3D index);

	private class OddSphere : IndexSphere
	{
		public OddSphere(int diameter)
			: base(diameter)
		{
		}

		/// <inheritdoc />
		public override IEnumerator<Index3D> GetEnumerator() =>
			ShapeIndices.Sphere.RangeOdd(this.Layers, this.RadiusSquared).GetEnumerator();

		/// <inheritdoc />
		protected override int GetDistanceFast(Index3D index) => (index.X * index.X) + (index.Y * index.Y) + (index.Z * index.Z);
	}

	private class EvenSphere : IndexSphere
	{
		public EvenSphere(int diameter)
			: base(diameter)
		{
		}

		/// <inheritdoc />
		public override IEnumerator<Index3D> GetEnumerator() =>
			ShapeIndices.Sphere.RangeEven(this.Layers, this.RadiusSquared).GetEnumerator();

		/// <inheritdoc />
		protected override int GetDistanceFast(Index3D index)
		{
			int x = index.X >= 0 ? index.X : -index.X - 1;
			int y = index.Y >= 0 ? index.Y : -index.Y - 1;
			int z = index.Z >= 0 ? index.Z : -index.Z - 1;
			return (x * x) + (y * y) + (z * z);
		}
	}
}
