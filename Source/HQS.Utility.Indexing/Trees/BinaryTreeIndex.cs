﻿namespace HQS.Utility.Indexing.Trees;

/// <summary>
///
/// </summary>
public static class BinaryTreeIndex
{
	public static ISpaceFillingCurve<BinaryTreeIndex<TIndex>> MortonEncoder<TIndex>()
		where TIndex : struct, IIndex<TIndex> => BinaryTreeIndexMortonEncoder<TIndex>.Instance;

	public static BinaryTreeIndex<TIndex> New<TIndex>(int layer, TIndex index)
		where TIndex : struct, IIndex<TIndex> => new BinaryTreeIndex<TIndex>(layer, index);

	public static BinaryTreeIndex<TIndex> Zero<TIndex>()
		where TIndex : struct, IIndex<TIndex> => new BinaryTreeIndex<TIndex>(0, default);

	public static void MatchLayers<TIndex>(ref BinaryTreeIndex<TIndex> index1, ref BinaryTreeIndex<TIndex> index2)
		where TIndex : struct, IIndex<TIndex>
	{
		if (index1.Layer == index2.Layer)
		{
			return;
		}

		if (index1.Layer < index2.Layer)
		{
			index1 = index1.GetChangedLayer(index2.Layer);
		}
		else
		{
			index2 = index2.GetChangedLayer(index1.Layer);
		}
	}

	public static void MatchLayers<TIndex>(
		ref BinaryTreeIndex<TIndex> index1, ref BinaryTreeIndex<TIndex> index2, ref BinaryTreeIndex<TIndex> index3)
		where TIndex : struct, IIndex<TIndex>
	{
		if (index1.Layer == index2.Layer && index1.Layer == index3.Layer)
		{
			return;
		}

		var layer = Math.Max(index1.Layer, Math.Max(index2.Layer, index3.Layer));
		index1.GetChangedLayer(layer);
		index2.GetChangedLayer(layer);
		index3.GetChangedLayer(layer);
	}

	private class BinaryTreeIndexMortonEncoder<TIndex> : ISpaceFillingCurve<BinaryTreeIndex<TIndex>>
		where TIndex : struct, IIndex<TIndex>
	{
		private readonly ISpaceFillingCurve<TIndex> indexEncoder;

		private BinaryTreeIndexMortonEncoder()
		{
			this.indexEncoder = IndexUtility.MortonEncoder<TIndex>();
		}

		public static ISpaceFillingCurve<BinaryTreeIndex<TIndex>> Instance { get; } = new BinaryTreeIndexMortonEncoder<TIndex>();

		/// <inheritdoc />
		public IEnumerable<BinaryTreeIndex<TIndex>> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return this.indexEncoder.GetCurve(iterations).Select(index => BinaryTreeIndex.New(iterations, index));
		}

		/// <inheritdoc />
		public BinaryTreeIndex<TIndex> DecodeIndex(int mortonCode) =>
			MortonIndexUtility.ToBinaryTreeIndex<TIndex>(mortonCode);

		/// <inheritdoc />
		public int EncodeIndex(BinaryTreeIndex<TIndex> index) =>
			MortonIndexUtility.GetLayerOffset(index.Layer, index.Rank) + this.indexEncoder.EncodeIndex(index.Index);
	}
}
