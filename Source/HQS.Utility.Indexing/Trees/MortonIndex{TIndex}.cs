﻿namespace HQS.Utility.Indexing.Trees;

public readonly struct MortonIndex<TIndex> : IEquatable<MortonIndex<TIndex>>
	where TIndex : struct, IIndex<TIndex>
{
	private readonly int layerOffset;

	private readonly int indexOffset;

	public MortonIndex(BinaryTreeIndex<TIndex> index)
		: this(MortonIndexUtility.GetLayerOffset(index.Layer, index.Rank), index.Index.ToMortonCode())
	{
	}

	public MortonIndex(int mortonCode)
	{
		MortonIndexUtility.SplitOffsets<TIndex>(mortonCode, out this.layerOffset, out this.indexOffset);
	}

	private MortonIndex(int layerOffset, int indexOffset)
	{
		Debug.Assert(layerOffset >= 0);
		Debug.Assert(indexOffset >= 0);

		this.layerOffset = layerOffset;
		this.indexOffset = indexOffset;
	}

	public int Rank => IndexUtility.Rank<TIndex>();

	public MortonIndex<TIndex> Parent => new MortonIndex<TIndex>(this.layerOffset >> this.Rank, this.indexOffset >> this.Rank);

	public MortonIndex<TIndex> GetAncestor(int layers)
	{
		Debug.Assert(layers >= 0);

		int shift = this.Rank * layers;
		return new MortonIndex<TIndex>(this.layerOffset >> shift, this.indexOffset >> shift);
	}

	public MortonIndex<TIndex> GetNeighbor(TIndex index) => new MortonIndex<TIndex>(this.layerOffset, index.ToMortonCode());

	public int ToMortonCode() => this.layerOffset + this.indexOffset;

	public BinaryTreeIndex<TIndex> ToIndex()
	{
		int layer = MortonIndexUtility.GetLayer<TIndex>(this.layerOffset);
		return new BinaryTreeIndex<TIndex>(layer, IndexUtility.MortonEncoder<TIndex>().DecodeIndex(this.indexOffset));
	}

	/// <inheritdoc />
	public bool Equals(MortonIndex<TIndex> other) =>
		this.layerOffset == other.layerOffset && this.indexOffset == other.indexOffset;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.layerOffset, this.indexOffset);

	/// <inheritdoc />
	public override string ToString() => this.ToMortonCode().ToString();

	public static bool operator ==(MortonIndex<TIndex> left, MortonIndex<TIndex> right) => left.Equals(right);

	public static bool operator !=(MortonIndex<TIndex> left, MortonIndex<TIndex> right) => !(left == right);
}
