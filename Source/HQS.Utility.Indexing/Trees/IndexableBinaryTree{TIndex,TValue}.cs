﻿namespace HQS.Utility.Indexing.Trees;

public class IndexableBinaryTree<TIndex, TValue> : IIndexable<BinaryTreeIndex<TIndex>, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	private readonly int layers;

	private readonly TValue[] array;

	// single root node counts as a layer and is layer 0
	// so layers = 1 means just the root node
	public IndexableBinaryTree(int layers)
	{
		Debug.Assert(layers >= 1);

		this.layers = layers;
		this.array = new TValue[MortonIndexUtility.GetCombinedLayersLength<TIndex>(layers)];
	}

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> Dimensions => BinaryTreeIndex.New(this.layers, IndexUtility.Create<TIndex>(1 << (this.layers - 1)));

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> LowerBounds => BinaryTreeIndex.New(0, IndexUtility.Zero<TIndex>());

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> UpperBounds =>
		BinaryTreeIndex.New(this.layers - 1, Operator.Subtract(this.Dimensions.Index, IndexUtility.One<TIndex>()));

	/// <inheritdoc />
	public TValue this[BinaryTreeIndex<TIndex> index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.array[index.ToMortonCode()];
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.array[index.ToMortonCode()] = value;
		}
	}

	public TValue this[MortonIndex<TIndex> index]
	{
		get
		{
			Debug.Assert(this.array.IsIndexInBounds(index.ToMortonCode()));

			return this.array[index.ToMortonCode()];
		}

		set
		{
			Debug.Assert(this.array.IsIndexInBounds(index.ToMortonCode()));

			this.array[index.ToMortonCode()] = value;
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<BinaryTreeIndex<TIndex>, TValue>> GetEnumerator()
	{
		// constants
		var encoder = this.Dimensions.Index.MortonCurve;
		var rank = this.Dimensions.Index.Rank;

		// loop
		int index = 0;
		int layerLength = 1;
		for (int layer = 0; layer < this.layers; layer++)
		{
			for (int count = 0; count < layerLength; count++)
			{
				yield return IndexValuePair.New(BinaryTreeIndex.New(layer, encoder.DecodeIndex(count)), this.array[index]);

				index++;
			}

			layerLength <<= rank;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
