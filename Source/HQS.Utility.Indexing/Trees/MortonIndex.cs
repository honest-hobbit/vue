﻿namespace HQS.Utility.Indexing.Trees;

public static class MortonIndex
{
	public static MortonIndex<TIndex> New<TIndex>(BinaryTreeIndex<TIndex> index)
		where TIndex : struct, IIndex<TIndex> => new MortonIndex<TIndex>(index);

	public static MortonIndex<TIndex> New<TIndex>(int mortonCode)
		where TIndex : struct, IIndex<TIndex> => new MortonIndex<TIndex>(mortonCode);
}
