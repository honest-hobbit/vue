﻿namespace HQS.Utility.Indexing.Trees;

/// <summary>
///
/// </summary>
public static class MortonIndexUtility
{
	public static BinaryTreeIndex<TIndex> ToBinaryTreeIndex<TIndex>(int mortonCode)
		where TIndex : struct, IIndex<TIndex>
	{
		int rank = IndexUtility.Rank<TIndex>();
		int layerLength = 1;
		int layers = 0;

		while (mortonCode >= layerLength)
		{
			mortonCode -= layerLength;
			layerLength <<= rank;
			layers++;
		}

		return new BinaryTreeIndex<TIndex>(layers, IndexUtility.MortonEncoder<TIndex>().DecodeIndex(mortonCode));
	}

	public static int GetLayer<TIndex>(int mortonCode)
		where TIndex : struct, IIndex<TIndex> => GetLayer(mortonCode, IndexUtility.Rank<TIndex>());

	public static int GetLayer(int mortonCode, int rank)
	{
		Debug.Assert(rank >= 1);

		int layerLength = 1;
		int layers = 0;

		while (mortonCode >= layerLength)
		{
			mortonCode -= layerLength;
			layerLength <<= rank;
			layers++;
		}

		return layers;
	}

	public static void SplitOffsets<TIndex>(int mortonCode, out int layerOffset, out int indexOffset)
		where TIndex : struct, IIndex<TIndex> =>
		SplitOffsets(mortonCode, out layerOffset, out indexOffset, IndexUtility.Rank<TIndex>());

	public static void SplitOffsets(int mortonCode, out int layerOffset, out int indexOffset, int rank)
	{
		Debug.Assert(rank >= 1);

		int combinedLayersLength = 1;
		int nextLayerLength = 1;

		while (mortonCode >= combinedLayersLength)
		{
			nextLayerLength <<= rank;
			combinedLayersLength += nextLayerLength;
		}

		combinedLayersLength -= nextLayerLength;
		layerOffset = combinedLayersLength;
		indexOffset = mortonCode - layerOffset;
	}

	public static int GetLayerLength<TIndex>(int layer)
		where TIndex : struct, IIndex<TIndex> => GetLayerLength(layer, IndexUtility.Rank<TIndex>());

	public static int GetLayerLength(int layer, int rank)
	{
		Debug.Assert(layer >= 0);
		Debug.Assert(rank >= 1);

		int result = 1;
		for (int count = 0; count < layer; count++)
		{
			result <<= rank;
		}

		return result;
	}

	public static int GetCombinedLayersLength<TIndex>(int layers)
		where TIndex : struct, IIndex<TIndex> => GetCombinedLayersLength(layers, IndexUtility.Rank<TIndex>());

	public static int GetCombinedLayersLength(int layers, int rank)
	{
		Debug.Assert(layers >= 0);
		Debug.Assert(rank >= 1);

		int result = 0;
		for (int count = 0; count < layers; count++)
		{
			result = (result << rank) | 1;
		}

		return result;
	}

	public static int GetLayerOffset<TIndex>(int layer)
		where TIndex : struct, IIndex<TIndex> => GetLayerOffset(layer, IndexUtility.Rank<TIndex>());

	public static int GetLayerOffset(int layer, int rank) => GetCombinedLayersLength(layer, rank);
}
