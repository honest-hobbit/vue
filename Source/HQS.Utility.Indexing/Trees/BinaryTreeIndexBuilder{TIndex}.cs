﻿namespace HQS.Utility.Indexing.Trees;

public class BinaryTreeIndexBuilder<TIndex> : IIndexBuilder<BinaryTreeIndex<TIndex>>
	where TIndex : struct, IIndex<TIndex>
{
	private readonly IIndexBuilder<TIndex> builder = IndexUtility.CreateBuilder<TIndex>();

	private int layer;

	public int Layer
	{
		get { return this.layer; }

		set
		{
			Debug.Assert(value >= 0);

			this.layer = value;
		}
	}

	public TIndex Index
	{
		get { return this.builder.ToIndex(); }
		set { this.builder.SetTo(value); }
	}

	/// <inheritdoc />
	public int Rank => this.builder.Rank;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			return this.builder[dimension];
		}

		set
		{
			IIndexBuilderContracts.Indexer(this, dimension);

			this.builder[dimension] = value;
		}
	}

	/// <inheritdoc />
	public override string ToString() => this.ToIndex().ToString();

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> ToIndex() => new BinaryTreeIndex<TIndex>(this.Layer, this.Index);

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator() => this.builder.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
