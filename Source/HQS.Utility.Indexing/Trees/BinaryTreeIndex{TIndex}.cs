﻿namespace HQS.Utility.Indexing.Trees;

public readonly struct BinaryTreeIndex<TIndex> : IIndex<BinaryTreeIndex<TIndex>>
	where TIndex : struct, IIndex<TIndex>
{
	public BinaryTreeIndex(int layer, TIndex index)
	{
		Debug.Assert(layer >= 0);

		this.Layer = layer;
		this.Index = index;
	}

	#region Properties/Indexer

	/// <inheritdoc />
	public Curves.ISpaceFillingCurve<BinaryTreeIndex<TIndex>> MortonCurve => BinaryTreeIndex.MortonEncoder<TIndex>();

	public int Layer { get; }

	public TIndex Index { get; }

	/// <inheritdoc />
	public int Rank => this.Index.Rank;

	/// <inheritdoc />
	public int this[int dimension]
	{
		get
		{
			ICoordinatesContracts.Indexer(this, dimension);

			return this.Index[dimension];
		}
	}

	#endregion

	#region Operators

	public static BinaryTreeIndex<TIndex> operator +(BinaryTreeIndex<TIndex> value) => value;

	public static BinaryTreeIndex<TIndex> operator -(BinaryTreeIndex<TIndex> value) =>
		new BinaryTreeIndex<TIndex>(value.Layer, Operator.Negate(value.Index));

	public static BinaryTreeIndex<TIndex> operator +(BinaryTreeIndex<TIndex> lhs, BinaryTreeIndex<TIndex> rhs)
	{
		BinaryTreeIndex.MatchLayers(ref lhs, ref rhs);
		return new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Add(lhs.Index, rhs.Index));
	}

	public static BinaryTreeIndex<TIndex> operator +(BinaryTreeIndex<TIndex> lhs, TIndex rhs) =>
		new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Add(lhs.Index, rhs));

	public static BinaryTreeIndex<TIndex> operator -(BinaryTreeIndex<TIndex> lhs, BinaryTreeIndex<TIndex> rhs)
	{
		BinaryTreeIndex.MatchLayers(ref lhs, ref rhs);
		return new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Subtract(lhs.Index, rhs.Index));
	}

	public static BinaryTreeIndex<TIndex> operator -(BinaryTreeIndex<TIndex> lhs, TIndex rhs) =>
		new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Subtract(lhs.Index, rhs));

	public static BinaryTreeIndex<TIndex> operator *(BinaryTreeIndex<TIndex> lhs, BinaryTreeIndex<TIndex> rhs)
	{
		BinaryTreeIndex.MatchLayers(ref lhs, ref rhs);
		return new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Multiply(lhs.Index, rhs.Index));
	}

	public static BinaryTreeIndex<TIndex> operator *(BinaryTreeIndex<TIndex> lhs, TIndex rhs) =>
		new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Multiply(lhs.Index, rhs));

	public static BinaryTreeIndex<TIndex> operator *(TIndex lhs, BinaryTreeIndex<TIndex> rhs) =>
		new BinaryTreeIndex<TIndex>(rhs.Layer, Operator.Multiply(rhs.Index, lhs));

	public static BinaryTreeIndex<TIndex> operator *(BinaryTreeIndex<TIndex> value, int scalar) =>
		new BinaryTreeIndex<TIndex>(value.Layer, Operator.MultiplyAlternative(value.Index, scalar));

	public static BinaryTreeIndex<TIndex> operator *(int scalar, BinaryTreeIndex<TIndex> value) =>
		new BinaryTreeIndex<TIndex>(value.Layer, Operator.MultiplyAlternative(value.Index, scalar));

	public static BinaryTreeIndex<TIndex> operator /(BinaryTreeIndex<TIndex> lhs, BinaryTreeIndex<TIndex> rhs)
	{
		BinaryTreeIndex.MatchLayers(ref lhs, ref rhs);
		return new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Divide(lhs.Index, rhs.Index));
	}

	public static BinaryTreeIndex<TIndex> operator /(BinaryTreeIndex<TIndex> lhs, TIndex rhs) =>
		new BinaryTreeIndex<TIndex>(lhs.Layer, Operator.Divide(lhs.Index, rhs));

	public static BinaryTreeIndex<TIndex> operator /(BinaryTreeIndex<TIndex> value, int scalar) =>
		new BinaryTreeIndex<TIndex>(value.Layer, Operator.DivideInt32(value.Index, scalar));

	public static bool operator ==(BinaryTreeIndex<TIndex> lhs, BinaryTreeIndex<TIndex> rhs) => lhs.Equals(rhs);

	public static bool operator !=(BinaryTreeIndex<TIndex> lhs, BinaryTreeIndex<TIndex> rhs) => !lhs.Equals(rhs);

	#endregion

	#region Methods

	/// <inheritdoc />
	public bool Equals(BinaryTreeIndex<TIndex> other) => this.Layer == other.Layer && this.Index.Equals(other.Index);

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Layer, this.Index);

	/// <inheritdoc />
	public override string ToString() => $"[{this.Layer}, {this.Index}]";

	/// <inheritdoc />
	public IIndexBuilder<BinaryTreeIndex<TIndex>> ToBuilder() => new BinaryTreeIndexBuilder<TIndex>()
	{
		Layer = this.Layer,
		Index = this.Index,
	};

	/// <inheritdoc />
	public IEnumerator<DimensionCoordinatePair> GetEnumerator() => this.Index.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	public bool IsIn(BinaryTreeIndex<TIndex> min, BinaryTreeIndex<TIndex> max)
	{
		IIndexContracts.IsIn(min, max);

		if (this.Layer < min.Layer || this.Layer > max.Layer)
		{
			return false;
		}

		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref min, ref max);

		return self.Index.IsIn(min.Index, max.Index);
	}

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> Clamp(BinaryTreeIndex<TIndex> min, BinaryTreeIndex<TIndex> max)
	{
		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref min, ref max);

		return new BinaryTreeIndex<TIndex>(self.Layer, self.Index.Clamp(min.Index, max.Index));
	}

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> ClampUpper(BinaryTreeIndex<TIndex> max)
	{
		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref max);

		return new BinaryTreeIndex<TIndex>(self.Layer, self.Index.ClampUpper(max.Index));
	}

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> ClampLower(BinaryTreeIndex<TIndex> min)
	{
		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref min);

		return new BinaryTreeIndex<TIndex>(self.Layer, self.Index.ClampLower(min.Index));
	}

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> Midpoint(BinaryTreeIndex<TIndex> index, bool roundUp = false)
	{
		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref index);

		return new BinaryTreeIndex<TIndex>(self.Layer, self.Index.Midpoint(index.Index, roundUp));
	}

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> Divide(BinaryTreeIndex<TIndex> divisor, out BinaryTreeIndex<TIndex> remainder)
	{
		IIndexContracts.Divide(divisor);

		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref divisor);

		TIndex resultIndex = self.Index.Divide(divisor.Index, out var remainderIndex);

		remainder = new BinaryTreeIndex<TIndex>(self.Layer, remainderIndex);
		return new BinaryTreeIndex<TIndex>(self.Layer, resultIndex);
	}

	/// <inheritdoc />
	public BinaryTreeIndex<TIndex> DivideRoundUp(BinaryTreeIndex<TIndex> divisor)
	{
		IIndexContracts.DivideRoundUp(this, divisor);

		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref divisor);

		return new BinaryTreeIndex<TIndex>(self.Layer, self.Index.DivideRoundUp(divisor.Index));
	}

	/// <inheritdoc />
	public IEnumerable<BinaryTreeIndex<TIndex>> Range(BinaryTreeIndex<TIndex> dimensions)
	{
		IIndexContracts.Range(dimensions);

		var self = this;
		BinaryTreeIndex.MatchLayers(ref self, ref dimensions);

		return self.Index.Range(dimensions.Index).Select(index => new BinaryTreeIndex<TIndex>(self.Layer, index));
	}

	public BinaryTreeIndex<TIndex> GetChangedLayer(int layer)
	{
		Debug.Assert(layer >= 0);

		if (layer == this.Layer)
		{
			return this;
		}

		if (layer < this.Layer)
		{
			var scalar = 1 << (this.Layer - layer);

			// >> could be used rather than Operator.DivideInt32 if only...
			return new BinaryTreeIndex<TIndex>(layer, Operator.DivideInt32(this.Index, scalar));
		}
		else
		{
			var scalar = 1 << (layer - this.Layer);

			// << could be used rather than Operator.MultiplyAlternative if only...
			return new BinaryTreeIndex<TIndex>(layer, Operator.MultiplyAlternative(this.Index, scalar));
		}
	}

	public MortonIndex<TIndex> ToMortonIndex() => new MortonIndex<TIndex>(this);

	#endregion
}
