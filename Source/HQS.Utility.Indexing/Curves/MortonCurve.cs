﻿namespace HQS.Utility.Indexing.Curves;

public static class MortonCurve
{
	public static ISpaceFillingCurve<Index1D> Index1D => MortonCurve1D.Instance;

	public static ISpaceFillingCurve<Index2D> Index2D => MortonCurve2D.Instance;

	public static ISpaceFillingCurve<Index3D> Index3D => MortonCurve3D.Instance;

	public static ISpaceFillingCurve<Index4D> Index4D => MortonCurve4D.Instance;

	public static EqualityComparer<TIndex> Comparer<TIndex>()
		where TIndex : struct, IIndex<TIndex> => MortonComparer<TIndex>.Instance;

	#region Private Classes

	private class MortonComparer<TIndex> : EqualityComparer<TIndex>
		where TIndex : struct, IIndex<TIndex>
	{
		private MortonComparer()
		{
		}

		public static EqualityComparer<TIndex> Instance { get; } = new MortonComparer<TIndex>();

		/// <inheritdoc />
		public override bool Equals(TIndex lhs, TIndex rhs) => lhs.Equals(rhs);

		/// <inheritdoc />
		public override int GetHashCode(TIndex index) => index.ToMortonCode();
	}

	private class MortonCurve1D : ISpaceFillingCurve<Index1D>
	{
		private MortonCurve1D()
		{
		}

		public static ISpaceFillingCurve<Index1D> Instance { get; } = new MortonCurve1D();

		/// <inheritdoc />
		public IEnumerable<Index1D> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return YieldCurve();
			IEnumerable<Index1D> YieldCurve()
			{
				if (iterations == 0)
				{
					yield break;
				}

				int max = 1 << iterations;
				for (int index = 0; index < max; index++)
				{
					yield return new Index1D(index);
				}
			}
		}

		/// <inheritdoc />
		public Index1D DecodeIndex(int mortonCode) => new Index1D(mortonCode);

		/// <inheritdoc />
		public int EncodeIndex(Index1D index) => index.X;
	}

	private class MortonCurve2D : ISpaceFillingCurve<Index2D>
	{
		private const uint Mask1 = 0b0000_0000_0000_0000_1111_1111_1111_1111;

		private const uint Mask2 = 0b0000_0000_1111_1111_0000_0000_1111_1111;

		private const uint Mask3 = 0b0000_1111_0000_1111_0000_1111_0000_1111;

		private const uint Mask4 = 0b0011_0011_0011_0011_0011_0011_0011_0011;

		private const uint Mask5 = 0b0101_0101_0101_0101_0101_0101_0101_0101;

		private MortonCurve2D()
		{
		}

		public static ISpaceFillingCurve<Index2D> Instance { get; } = new MortonCurve2D();

		/// <inheritdoc />
		public IEnumerable<Index2D> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return iterations switch
			{
				0 => Enumerable.Empty<Index2D>(),
				1 => SingleIteration(),
				_ => RecursiveCurve(),
			};

			IEnumerable<Index2D> RecursiveCurve()
			{
				foreach (var outterIndex in Instance.GetCurve(iterations - 1))
				{
					var offsetIndex = outterIndex << 1;
					foreach (var innerIndex in SingleIteration())
					{
						yield return innerIndex + offsetIndex;
					}
				}
			}

			static IEnumerable<Index2D> SingleIteration()
			{
				yield return new Index2D(0, 0);
				yield return new Index2D(1, 0);
				yield return new Index2D(0, 1);
				yield return new Index2D(1, 1);
			}
		}

		/// <inheritdoc />
		public Index2D DecodeIndex(int mortonCode) => new Index2D(Compact(mortonCode), Compact(mortonCode >> 1));

		/// <inheritdoc />
		public int EncodeIndex(Index2D index) => Split(index.X) | (Split(index.Y) << 1);

		private static int Split(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask1;
				x = (x | (x << 8)) & Mask2;
				x = (x | (x << 4)) & Mask3;
				x = (x | (x << 2)) & Mask4;
				x = (x | (x << 1)) & Mask5;
				return (int)x;
			}
		}

		private static int Compact(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask5;
				x = (x | (x >> 1)) & Mask4;
				x = (x | (x >> 2)) & Mask3;
				x = (x | (x >> 4)) & Mask2;
				x = (x | (x >> 8)) & Mask1;
				return (int)x;
			}
		}
	}

	private class MortonCurve3D : ISpaceFillingCurve<Index3D>
	{
		private const uint Mask1 = 0b0000_0000_0000_0000_0000_0011_1111_1111;

		private const uint Mask2 = 0b0000_0011_0000_0000_0000_0000_1111_1111;

		private const uint Mask3 = 0b0000_0011_0000_0000_1111_0000_0000_1111;

		private const uint Mask4 = 0b0000_0011_0000_1100_0011_0000_1100_0011;

		private const uint Mask5 = 0b0000_1001_0010_0100_1001_0010_0100_1001;

		private MortonCurve3D()
		{
		}

		public static ISpaceFillingCurve<Index3D> Instance { get; } = new MortonCurve3D();

		/// <inheritdoc />
		public IEnumerable<Index3D> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return iterations switch
			{
				0 => Enumerable.Empty<Index3D>(),
				1 => SingleIteration(),
				_ => RecursiveCurve(),
			};

			IEnumerable<Index3D> RecursiveCurve()
			{
				foreach (var outterIndex in Instance.GetCurve(iterations - 1))
				{
					var offsetIndex = outterIndex << 1;
					foreach (var innerIndex in SingleIteration())
					{
						yield return innerIndex + offsetIndex;
					}
				}
			}

			static IEnumerable<Index3D> SingleIteration()
			{
				yield return new Index3D(0, 0, 0);
				yield return new Index3D(1, 0, 0);
				yield return new Index3D(0, 1, 0);
				yield return new Index3D(1, 1, 0);
				yield return new Index3D(0, 0, 1);
				yield return new Index3D(1, 0, 1);
				yield return new Index3D(0, 1, 1);
				yield return new Index3D(1, 1, 1);
			}
		}

		/// <inheritdoc />
		public Index3D DecodeIndex(int mortonCode) => new Index3D(Compact(mortonCode), Compact(mortonCode >> 1), Compact(mortonCode >> 2));

		/// <inheritdoc />
		public int EncodeIndex(Index3D index) => Split(index.X) | (Split(index.Y) << 1) | (Split(index.Z) << 2);

		private static int Split(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask1;
				x = (x | (x << 16)) & Mask2;
				x = (x | (x << 8)) & Mask3;
				x = (x | (x << 4)) & Mask4;
				x = (x | (x << 2)) & Mask5;
				return (int)x;
			}
		}

		private static int Compact(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask5;
				x = (x | (x >> 2)) & Mask4;
				x = (x | (x >> 4)) & Mask3;
				x = (x | (x >> 8)) & Mask2;
				x = (x | (x >> 16)) & Mask1;
				return (int)x;
			}
		}
	}

	private class MortonCurve4D : ISpaceFillingCurve<Index4D>
	{
		private const uint Mask1 = 0b0000_0000_0000_0000_0000_0000_1111_1111;

		private const uint Mask2 = 0b0000_0000_0000_1111_0000_0000_0000_1111;

		private const uint Mask3 = 0b0000_0011_0000_0011_0000_0011_0000_0011;

		private const uint Mask4 = 0b0001_0001_0001_0001_0001_0001_0001_0001;

		private MortonCurve4D()
		{
		}

		public static ISpaceFillingCurve<Index4D> Instance { get; } = new MortonCurve4D();

		/// <inheritdoc />
		public IEnumerable<Index4D> GetCurve(int iterations)
		{
			ISpaceFillingCurveContracts.GetCurve(iterations);

			return iterations switch
			{
				0 => Enumerable.Empty<Index4D>(),
				1 => SingleIteration(),
				_ => RecursiveCurve(),
			};

			IEnumerable<Index4D> RecursiveCurve()
			{
				foreach (var outterIndex in Instance.GetCurve(iterations - 1))
				{
					var offsetIndex = outterIndex << 1;
					foreach (var innerIndex in SingleIteration())
					{
						yield return innerIndex + offsetIndex;
					}
				}
			}

			static IEnumerable<Index4D> SingleIteration()
			{
				yield return new Index4D(0, 0, 0, 0);
				yield return new Index4D(1, 0, 0, 0);
				yield return new Index4D(0, 1, 0, 0);
				yield return new Index4D(1, 1, 0, 0);
				yield return new Index4D(0, 0, 1, 0);
				yield return new Index4D(1, 0, 1, 0);
				yield return new Index4D(0, 1, 1, 0);
				yield return new Index4D(1, 1, 1, 0);
				yield return new Index4D(0, 0, 0, 1);
				yield return new Index4D(1, 0, 0, 1);
				yield return new Index4D(0, 1, 0, 1);
				yield return new Index4D(1, 1, 0, 1);
				yield return new Index4D(0, 0, 1, 1);
				yield return new Index4D(1, 0, 1, 1);
				yield return new Index4D(0, 1, 1, 1);
				yield return new Index4D(1, 1, 1, 1);
			}
		}

		/// <inheritdoc />
		public Index4D DecodeIndex(int mortonCode) =>
			new Index4D(Compact(mortonCode), Compact(mortonCode >> 1), Compact(mortonCode >> 2), Compact(mortonCode >> 3));

		/// <inheritdoc />
		public int EncodeIndex(Index4D index) =>
			Split(index.X) | (Split(index.Y) << 1) | (Split(index.Z) << 2) | (Split(index.W) << 3);

		private static int Split(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask1;
				x = (x | (x << 12)) & Mask2;
				x = (x | (x << 6)) & Mask3;
				x = (x | (x << 3)) & Mask4;
				return (int)x;
			}
		}

		private static int Compact(int value)
		{
			unchecked
			{
				uint x = (uint)value;
				x &= Mask4;
				x = (x | (x >> 3)) & Mask3;
				x = (x | (x >> 6)) & Mask2;
				x = (x | (x >> 12)) & Mask1;
				return (int)x;
			}
		}
	}

	#endregion
}
