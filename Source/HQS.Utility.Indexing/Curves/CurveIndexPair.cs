﻿namespace HQS.Utility.Indexing.Curves;

/// <summary>
///
/// </summary>
public static class CurveIndexPair
{
	public static CurveIndexPair<TIndex> New<TIndex>(int code, TIndex index)
		where TIndex : struct, IIndex<TIndex> => new CurveIndexPair<TIndex>(code, index);
}
