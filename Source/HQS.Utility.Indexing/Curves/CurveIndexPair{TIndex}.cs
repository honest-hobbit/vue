﻿namespace HQS.Utility.Indexing.Curves;

public readonly struct CurveIndexPair<TIndex> : IEquatable<CurveIndexPair<TIndex>>
	where TIndex : struct, IIndex<TIndex>
{
	public CurveIndexPair(int code, TIndex index)
	{
		this.Code = code;
		this.Index = index;
	}

	public int Code { get; }

	public TIndex Index { get; }

	/// <inheritdoc />
	public override string ToString() => $"[{this.Code}: {this.Index}]";

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(CurveIndexPair<TIndex> other) => this.Code == other.Code && this.Index.Equals(other.Index);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Code, this.Index);

	public static bool operator ==(CurveIndexPair<TIndex> left, CurveIndexPair<TIndex> right) => left.Equals(right);

	public static bool operator !=(CurveIndexPair<TIndex> left, CurveIndexPair<TIndex> right) => !(left == right);
}
