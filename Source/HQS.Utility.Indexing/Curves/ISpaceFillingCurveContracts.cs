﻿namespace HQS.Utility.Indexing.Curves;

public static class ISpaceFillingCurveContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void GetCurve(int iterations)
	{
		Debug.Assert(iterations >= 0);
	}
}
