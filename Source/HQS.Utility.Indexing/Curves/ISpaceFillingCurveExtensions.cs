﻿namespace HQS.Utility.Indexing.Curves;

/// <summary>
/// Provides extension methods for <see cref="ISpaceFillingCurve{TIndex}"/>.
/// </summary>
public static class ISpaceFillingCurveExtensions
{
	public static IEnumerable<CurveIndexPair<TIndex>> GetCurveWithCodes<TIndex>(
		this ISpaceFillingCurve<TIndex> encoder, int iterations)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(encoder != null);

		return encoder.GetCurve(iterations).Select((index, mortonCode) => CurveIndexPair.New(mortonCode, index));
	}
}
