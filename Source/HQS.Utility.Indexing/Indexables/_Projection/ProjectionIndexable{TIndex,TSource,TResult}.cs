﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
/// Wraps another indexable instance, projecting from its source type to a different result type. This allows
/// an indexable instance of one type to be viewed as an indexable instance of a different type while still
/// being the same instance.
/// </summary>
/// <typeparam name="TIndex">The type of the index.</typeparam>
/// <typeparam name="TSource">The type of the source to project from.</typeparam>
/// <typeparam name="TResult">The type of the result to project the source to.</typeparam>
public class ProjectionIndexable<TIndex, TSource, TResult> :
	AbstractProjectionIndexable<TIndex, TSource, TResult, IIndexable<TIndex, TSource>>, IIndexable<TIndex, TResult>
	where TIndex : struct, IIndex<TIndex>
{
	public ProjectionIndexable(IIndexable<TIndex, TSource> indexable, IDualConverter<TSource, TResult> converter)
		: base(indexable, converter)
	{
	}

	/// <inheritdoc />
	public new TResult this[TIndex index]
	{
		get { return base[index]; }

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.Indexable[index] = this.Converter.Convert(value);
		}
	}
}
