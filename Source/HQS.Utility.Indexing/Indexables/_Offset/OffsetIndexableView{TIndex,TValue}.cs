﻿namespace HQS.Utility.Indexing.Indexables;

public class OffsetIndexableView<TIndex, TValue> : AbstractOffsetIndexable<TIndex, TValue, IIndexableView<TIndex, TValue>>
	where TIndex : struct, IIndex<TIndex>
{
	/// <summary>
	/// Initializes a new instance of the <see cref="OffsetIndexableView{TKey, TValue}"/> class.
	/// </summary>
	/// <param name="indexable">The indexable to wrap.</param>
	/// <param name="offset">The offset of the zero origin.</param>
	public OffsetIndexableView(IIndexableView<TIndex, TValue> indexable, TIndex offset)
		: base(indexable, offset)
	{
	}
}
