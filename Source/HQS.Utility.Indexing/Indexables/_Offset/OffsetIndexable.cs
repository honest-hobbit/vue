﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
///
/// </summary>
public static class OffsetIndexable
{
	public static IIndexableView<TIndex, TValue> CenterOnZero<TIndex, TValue>(
		IIndexableView<TIndex, TValue> indexable, bool roundUp = false)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(indexable != null);

		var offset = Operator.Negate(indexable.GetMidpoint(roundUp));
		return offset.Equals(IndexUtility.Zero<TIndex>()) ? indexable : new OffsetIndexableView<TIndex, TValue>(indexable, offset);
	}

	public static IIndexable<TIndex, TValue> CenterOnZero<TIndex, TValue>(
		IIndexable<TIndex, TValue> indexable, bool roundUp = false)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(indexable != null);

		var offset = Operator.Negate(indexable.GetMidpoint(roundUp));
		return offset.Equals(IndexUtility.Zero<TIndex>()) ? indexable : new OffsetIndexable<TIndex, TValue>(indexable, offset);
	}
}
