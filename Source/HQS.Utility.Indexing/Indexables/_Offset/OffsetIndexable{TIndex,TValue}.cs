﻿namespace HQS.Utility.Indexing.Indexables;

public class OffsetIndexable<TIndex, TValue> :
	AbstractOffsetIndexable<TIndex, TValue, IIndexable<TIndex, TValue>>, IIndexable<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	/// <summary>
	/// Initializes a new instance of the <see cref="OffsetIndexable{TKey, TValue}"/> class.
	/// </summary>
	/// <param name="indexable">The indexable to wrap.</param>
	/// <param name="offset">The offset of the zero origin.</param>
	public OffsetIndexable(IIndexable<TIndex, TValue> indexable, TIndex offset)
		: base(indexable, offset)
	{
	}

	/// <inheritdoc />
	public new TValue this[TIndex index]
	{
		get { return base[index]; }

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.Indexable[Operator.Subtract(index, this.Offset)] = value;
		}
	}
}
