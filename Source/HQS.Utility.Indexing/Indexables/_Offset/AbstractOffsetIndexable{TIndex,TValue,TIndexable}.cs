﻿namespace HQS.Utility.Indexing.Indexables;

public abstract class AbstractOffsetIndexable<TIndex, TValue, TIndexable> : IIndexableView<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
	where TIndexable : IIndexableView<TIndex, TValue>
{
	public AbstractOffsetIndexable(TIndexable indexable, TIndex offset)
	{
		Debug.Assert(indexable != null);

		this.Indexable = indexable;
		this.Offset = offset;
	}

	/// <inheritdoc />
	public TIndex Dimensions => this.Indexable.Dimensions;

	/// <inheritdoc />
	public TIndex LowerBounds => Operator.Add(this.Indexable.LowerBounds, this.Offset);

	/// <inheritdoc />
	public TIndex UpperBounds => Operator.Add(this.Indexable.UpperBounds, this.Offset);

	protected TIndexable Indexable { get; }

	protected TIndex Offset { get; }

	/// <inheritdoc />
	public TValue this[TIndex index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.Indexable[Operator.Subtract(index, this.Offset)];
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<TIndex, TValue>> GetEnumerator() => this.Indexable.Select(
		pair => IndexValuePair.New(Operator.Add(pair.Index, this.Offset), pair.Value)).GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
