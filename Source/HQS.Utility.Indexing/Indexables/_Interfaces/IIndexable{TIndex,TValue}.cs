﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
/// Interface to define a grouping of values accessed by an <see cref="IIndex{TIndex}"/> of an undefined
/// number of dimensions. Implementations of this interface may define how many dimensions they support.
/// </summary>
/// <typeparam name="TIndex">
/// The type of the index used to access values.
/// This determines the number of dimensions that can be indexed.
/// </typeparam>
/// <typeparam name="TValue">The type of the stored values.</typeparam>
public interface IIndexable<TIndex, TValue> : IIndexableView<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	/// <summary>
	/// Gets or sets the <typeparamref name="TValue"/> at the specified index.
	/// </summary>
	/// <value>
	/// The <typeparamref name="TValue"/> to assign at the specified index.
	/// </value>
	/// <param name="index">The index.</param>
	/// <returns>
	/// The <typeparamref name="TValue"/> at the specified index.
	/// </returns>
	new TValue this[TIndex index] { get; set; }
}
