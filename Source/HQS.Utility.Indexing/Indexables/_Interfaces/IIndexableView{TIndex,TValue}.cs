﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
///
/// </summary>
/// <typeparam name="TIndex">
/// The type of the index used to access values.
/// This determines the number of dimensions that can be indexed.
/// </typeparam>
/// <typeparam name="TValue">The type of the stored values.</typeparam>
public interface IIndexableView<TIndex, TValue> : IIndexingBounds<TIndex>, IEnumerable<IndexValuePair<TIndex, TValue>>
	where TIndex : struct, IIndex<TIndex>
{
	/// <summary>
	/// Gets the <typeparamref name="TValue"/> at the specified index.
	/// </summary>
	/// <param name="index">The index.</param>
	/// <returns>
	/// The <typeparamref name="TValue"/> at the specified index.
	/// </returns>
	TValue this[TIndex index] { get; }
}
