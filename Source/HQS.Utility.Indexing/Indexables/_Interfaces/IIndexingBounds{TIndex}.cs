﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
/// Interface to define the static bounds of a grouping of values accessed by an <see cref="IIndex{TIndex}"/>
/// of an undefined number of dimensions. Implementations of this interface may define how many dimensions
/// they support.
/// </summary>
/// <typeparam name="TIndex">
/// The type of the index used to access values.
/// This determines the number of dimensions that can be indexed.
/// </typeparam>
/// <remarks>
/// <para>
/// This interface provides no mechanism for retrieving values, nor does it even define what the values are.
/// This interface is only for providing the bounds of an indexable collection. Use the <see cref="IIndexable{TIndex, TValue}"/>
/// interface for providing retrieval of values. Any time an 'indexable' is referred to in this interface it is
/// referring to the <see cref="IIndexable{TIndex, TValue}"/> this instance is associated with.
/// </para>
/// <para>
/// Implementations must be non-jagged, i.e. they must be rectangular, rigidly multidimensional, and/or uniform.
/// Each dimension must also be capable of being indexed. This means the length of each dimension must be greater
/// than or equal to one. A non indexable dimension would disallow the entire indexable from storing any values
/// and would needlessly complicate the bounds methods.
/// </para>
/// </remarks>
public interface IIndexingBounds<TIndex>
	where TIndex : struct, IIndex<TIndex>
{
	/// <summary>
	/// Gets the size of the dimensions of the indexable returned as the same value type used to index into the indexable.
	/// </summary>
	/// <value>
	/// The dimensions of the indexable.
	/// </value>
	TIndex Dimensions { get; }

	/// <summary>
	/// Gets the index of the first slot of each of the dimensions in the indexable.
	/// </summary>
	/// <value>
	/// The index of the first slot of each dimension in the indexable.
	/// </value>
	TIndex LowerBounds { get; }

	/// <summary>
	/// Gets the index of the last slot of each of the dimensions in the indexable.
	/// </summary>
	/// <value>
	/// The index of the last slot of each dimension in the indexable.
	/// </value>
	TIndex UpperBounds { get; }
}
