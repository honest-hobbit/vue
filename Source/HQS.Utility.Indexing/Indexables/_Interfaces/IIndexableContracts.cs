﻿namespace HQS.Utility.Indexing.Indexables;

public static class IIndexableContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void IndexerSet<TIndex, TValue>(IIndexable<TIndex, TValue> instance, TIndex index)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(instance != null);
		Debug.Assert(instance.IsIndexInBounds(index));
	}
}
