﻿namespace HQS.Utility.Indexing.Indexables;

public static class IIndexableViewContracts
{
	[Conditional(CompilationSymbol.Debug)]
	public static void IndexerGet<TIndex, TValue>(IIndexableView<TIndex, TValue> instance, TIndex index)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(instance != null);
		Debug.Assert(instance.IsIndexInBounds(index));
	}
}
