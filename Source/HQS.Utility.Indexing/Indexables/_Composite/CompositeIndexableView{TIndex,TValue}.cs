﻿namespace HQS.Utility.Indexing.Indexables;

public class CompositeIndexableView<TIndex, TValue> : AbstractCompositeIndexable<TIndex, TValue, IIndexableView<TIndex, TValue>>
	where TIndex : struct, IIndex<TIndex>
{
	public CompositeIndexableView(IIndexableView<TIndex, IIndexableView<TIndex, TValue>> indexables)
		: base(indexables)
	{
	}
}
