﻿namespace HQS.Utility.Indexing.Indexables;

public class CompositeBinaryIndexableView3D<TValue> : AbstractCompositeBinaryIndexable3D<TValue, IIndexableView<Index3D, TValue>>
{
	public CompositeBinaryIndexableView3D(IIndexableView<Index3D, IIndexableView<Index3D, TValue>> indexables, int powerOf2Exponent)
		: base(indexables, powerOf2Exponent)
	{
	}
}
