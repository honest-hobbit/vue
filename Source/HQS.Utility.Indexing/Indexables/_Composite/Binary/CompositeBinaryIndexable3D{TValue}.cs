﻿namespace HQS.Utility.Indexing.Indexables;

public class CompositeBinaryIndexable3D<TValue> :
	AbstractCompositeBinaryIndexable3D<TValue, IIndexable<Index3D, TValue>>, IIndexable<Index3D, TValue>
{
	public CompositeBinaryIndexable3D(IIndexableView<Index3D, IIndexable<Index3D, TValue>> indexables, int powerOf2Exponent)
		: base(indexables, powerOf2Exponent)
	{
	}

	/// <inheritdoc />
	public new TValue this[Index3D index]
	{
		get { return base[index]; }

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.GetIndexable(index, out var subIndex)[subIndex] = value;
		}
	}
}
