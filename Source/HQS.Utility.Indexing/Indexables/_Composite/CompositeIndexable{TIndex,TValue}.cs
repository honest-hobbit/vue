﻿namespace HQS.Utility.Indexing.Indexables;

public class CompositeIndexable<TIndex, TValue> :
	AbstractCompositeIndexable<TIndex, TValue, IIndexable<TIndex, TValue>>, IIndexable<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	public CompositeIndexable(IIndexableView<TIndex, IIndexable<TIndex, TValue>> indexables)
		: base(indexables)
	{
	}

	/// <inheritdoc />
	public new TValue this[TIndex index]
	{
		get { return base[index]; }

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.GetIndexable(index, out var subIndex)[subIndex] = value;
		}
	}
}
