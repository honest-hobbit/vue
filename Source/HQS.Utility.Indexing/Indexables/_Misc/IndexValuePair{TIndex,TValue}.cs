﻿namespace HQS.Utility.Indexing.Indexables;

public readonly struct IndexValuePair<TIndex, TValue> : IEquatable<IndexValuePair<TIndex, TValue>>
	where TIndex : struct, IIndex<TIndex>
{
	public IndexValuePair(TIndex index, TValue value)
	{
		this.Index = index;
		this.Value = value;
	}

	public TIndex Index { get; }

	public TValue Value { get; }

	/// <inheritdoc />
	public bool Equals(IndexValuePair<TIndex, TValue> other) =>
		this.Index.Equals(other.Index) && this.Value.EqualsNullSafe(other.Value);

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Index, this.Value);

	/// <inheritdoc />
	public override string ToString() => $"[{this.Index}: {this.Value}]";

	public static bool operator ==(IndexValuePair<TIndex, TValue> left, IndexValuePair<TIndex, TValue> right) => left.Equals(right);

	public static bool operator !=(IndexValuePair<TIndex, TValue> left, IndexValuePair<TIndex, TValue> right) => !(left == right);
}
