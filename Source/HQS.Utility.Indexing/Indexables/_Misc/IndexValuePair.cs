﻿namespace HQS.Utility.Indexing.Indexables;

public static class IndexValuePair
{
	public static IndexValuePair<TIndex, TValue> New<TIndex, TValue>(TIndex index, TValue value)
		where TIndex : struct, IIndex<TIndex> => new IndexValuePair<TIndex, TValue>(index, value);
}
