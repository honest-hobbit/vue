﻿namespace HQS.Utility.Indexing.Indexables;

public class IndexableDictionary<TIndex, TValue> : IndexingBounds<TIndex>, IIndexable<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	private readonly IDictionary<TIndex, TValue> values;

	public IndexableDictionary(TIndex dimensions)
		: this(IndexUtility.Zero<TIndex>(), dimensions)
	{
	}

	public IndexableDictionary(TIndex lowerBounds, TIndex dimensions)
		: base(lowerBounds, dimensions)
	{
		int capacity = dimensions.Coordinates().Multiply();
		this.values = new Dictionary<TIndex, TValue>(capacity, StructComparer.Equality<TIndex>());
	}

	/// <inheritdoc />
	public TValue this[TIndex index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.values.GetOrAdd(index, default(TValue));
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.values[index] = value;
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<TIndex, TValue>> GetEnumerator()
	{
		foreach (var pair in this.values)
		{
			yield return IndexValuePair.New(pair.Key, pair.Value);
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
