﻿namespace HQS.Utility.Indexing.Indexables;

public class IndexingBounds<TIndex> : IIndexingBounds<TIndex>
	where TIndex : struct, IIndex<TIndex>
{
	public IndexingBounds(TIndex dimensions)
		: this(IndexUtility.Zero<TIndex>(), dimensions)
	{
	}

	public IndexingBounds(TIndex lowerBounds, TIndex dimensions)
	{
		Debug.Assert(dimensions.Coordinates().All(value => value >= 1));

		this.Dimensions = dimensions;
		this.LowerBounds = lowerBounds;
		this.UpperBounds = Operator.Add(this.LowerBounds, Operator.Subtract(this.Dimensions, IndexUtility.One<TIndex>()));
	}

	/// <inheritdoc />
	public TIndex Dimensions { get; }

	/// <inheritdoc />
	public TIndex LowerBounds { get; }

	/// <inheritdoc />
	public TIndex UpperBounds { get; }
}
