﻿namespace HQS.Utility.Indexing.Indexables;

public class BinaryIndexable3D<T> : AbstractBinaryIndexable<Index3D, T>
{
	public BinaryIndexable3D(int powerOf2Exponent)
		: base(powerOf2Exponent)
	{
	}

	/// <inheritdoc />
	public override IEnumerator<IndexValuePair<Index3D, T>> GetEnumerator()
	{
		foreach (var index in Index3D.Zero.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	protected override int ToArrayIndex(Index3D index) =>
		index.X + ((index.Y + (index.Z << this.PowerOf2Exponent)) << this.PowerOf2Exponent);
}
