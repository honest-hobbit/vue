﻿namespace HQS.Utility.Indexing.Indexables;

public class BinaryIndexable2D<T> : AbstractBinaryIndexable<Index2D, T>
{
	public BinaryIndexable2D(int powerOf2Exponent)
		: base(powerOf2Exponent)
	{
	}

	/// <inheritdoc />
	public override IEnumerator<IndexValuePair<Index2D, T>> GetEnumerator()
	{
		foreach (var index in Index2D.Zero.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	protected override int ToArrayIndex(Index2D index) => index.X + (index.Y << this.PowerOf2Exponent);
}
