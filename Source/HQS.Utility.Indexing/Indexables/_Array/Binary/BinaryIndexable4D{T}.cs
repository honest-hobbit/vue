﻿namespace HQS.Utility.Indexing.Indexables;

public class BinaryIndexable4D<T> : AbstractBinaryIndexable<Index4D, T>
{
	public BinaryIndexable4D(int powerOf2Exponent)
		: base(powerOf2Exponent)
	{
	}

	/// <inheritdoc />
	public override IEnumerator<IndexValuePair<Index4D, T>> GetEnumerator()
	{
		foreach (var index in Index4D.Zero.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	protected override int ToArrayIndex(Index4D index) =>
		index.X + ((index.Y + ((index.Z + (index.W << this.PowerOf2Exponent)) << this.PowerOf2Exponent)) << this.PowerOf2Exponent);
}
