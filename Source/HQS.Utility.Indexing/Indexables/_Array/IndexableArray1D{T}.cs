﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
/// A generic four dimensional array that implements <see cref="IIndexable{TIndex, TValue}"/>.
/// </summary>
/// <typeparam name="T">The type of the value stored in the array.</typeparam>
public class IndexableArray1D<T> : IIndexable<Index1D, T>
{
	/// <summary>
	/// The array being wrapped by this implementation.
	/// </summary>
	private readonly T[] array;

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexableArray1D{TValue}"/> class.
	/// </summary>
	/// <param name="array">The array to wrap.</param>
	public IndexableArray1D(T[] array)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.HasCapacity());

		this.array = array;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexableArray1D{TValue}"/> class.
	/// </summary>
	/// <param name="dimensions">The dimensions of the array.</param>
	public IndexableArray1D(Index1D dimensions)
		: this(dimensions.CreateArray<T>())
	{
	}

	/// <inheritdoc />
	public Index1D Dimensions => this.array.GetDimensions();

	/// <inheritdoc />
	public Index1D LowerBounds => this.array.GetLowerBounds();

	/// <inheritdoc />
	public Index1D UpperBounds => this.array.GetUpperBounds();

	/// <inheritdoc />
	public T this[Index1D index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.array.Get(index);
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.array.Set(index, value);
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index1D, T>> GetEnumerator() => this.array.GetIndexValuePairs().GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
