﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
/// A generic four dimensional array that implements <see cref="IIndexable{TIndex, TValue}"/>.
/// </summary>
/// <typeparam name="T">The type of the value stored in the array.</typeparam>
public class IndexableArray4D<T> : IIndexable<Index4D, T>
{
	/// <summary>
	/// The array being wrapped by this implementation.
	/// </summary>
	private readonly T[,,,] array;

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexableArray4D{TValue}"/> class.
	/// </summary>
	/// <param name="array">The array to wrap.</param>
	public IndexableArray4D(T[,,,] array)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.HasCapacity());

		this.array = array;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexableArray4D{TValue}"/> class.
	/// </summary>
	/// <param name="dimensions">The dimensions of the array.</param>
	public IndexableArray4D(Index4D dimensions)
		: this(dimensions.CreateArray<T>())
	{
	}

	/// <inheritdoc />
	public Index4D Dimensions => this.array.GetDimensions();

	/// <inheritdoc />
	public Index4D LowerBounds => this.array.GetLowerBounds();

	/// <inheritdoc />
	public Index4D UpperBounds => this.array.GetUpperBounds();

	/// <inheritdoc />
	public T this[Index4D index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.array.Get(index);
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.array.Set(index, value);
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index4D, T>> GetEnumerator() => this.array.GetIndexValuePairs().GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
