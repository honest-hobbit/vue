﻿namespace HQS.Utility.Indexing.Indexables;

public class IndexableFlatArray2D<T> : AbstractIndexableFlatArray<Index2D, T>
{
	public IndexableFlatArray2D(Index2D dimensions)
		: base(dimensions)
	{
	}

	/// <inheritdoc />
	public override IEnumerator<IndexValuePair<Index2D, T>> GetEnumerator()
	{
		foreach (var index in Index2D.Zero.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	protected override int ToArrayIndex(Index2D index) => index.X + (index.Y * this.Dimensions.X);
}
