﻿namespace HQS.Utility.Indexing.Indexables;

public class IndexableFlatArray4D<T> : AbstractIndexableFlatArray<Index4D, T>
{
	public IndexableFlatArray4D(Index4D dimensions)
		: base(dimensions)
	{
	}

	/// <inheritdoc />
	public override IEnumerator<IndexValuePair<Index4D, T>> GetEnumerator()
	{
		foreach (var index in Index4D.Zero.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	protected override int ToArrayIndex(Index4D index) =>
		index.X + ((index.Y + ((index.Z + (index.W * this.Dimensions.Z)) * this.Dimensions.Y)) * this.Dimensions.X);
}
