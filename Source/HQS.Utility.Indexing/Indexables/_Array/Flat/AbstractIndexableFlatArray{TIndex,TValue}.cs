﻿namespace HQS.Utility.Indexing.Indexables;

public abstract class AbstractIndexableFlatArray<TIndex, TValue> : IndexingBounds<TIndex>, IIndexable<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	private readonly TValue[] array;

	public AbstractIndexableFlatArray(TIndex dimensions)
		: base(dimensions)
	{
		this.array = new TValue[dimensions.Coordinates().Multiply()];
	}

	/// <inheritdoc />
	public TValue this[TIndex index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.array[this.ToArrayIndex(index)];
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.array[this.ToArrayIndex(index)] = value;
		}
	}

	/// <inheritdoc />
	public abstract IEnumerator<IndexValuePair<TIndex, TValue>> GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	protected abstract int ToArrayIndex(TIndex index);
}
