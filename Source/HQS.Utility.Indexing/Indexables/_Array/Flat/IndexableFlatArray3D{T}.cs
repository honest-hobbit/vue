﻿namespace HQS.Utility.Indexing.Indexables;

public class IndexableFlatArray3D<T> : AbstractIndexableFlatArray<Index3D, T>
{
	public IndexableFlatArray3D(Index3D dimensions)
		: base(dimensions)
	{
	}

	/// <inheritdoc />
	public override IEnumerator<IndexValuePair<Index3D, T>> GetEnumerator()
	{
		foreach (var index in Index3D.Zero.Range(this.Dimensions))
		{
			yield return IndexValuePair.New(index, this[index]);
		}
	}

	/// <inheritdoc />
	protected override int ToArrayIndex(Index3D index) =>
		index.X + ((index.Y + (index.Z * this.Dimensions.Y)) * this.Dimensions.X);
}
