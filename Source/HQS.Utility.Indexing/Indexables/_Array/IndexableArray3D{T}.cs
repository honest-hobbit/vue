﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
/// A generic four dimensional array that implements <see cref="IIndexable{TIndex, TValue}"/>.
/// </summary>
/// <typeparam name="T">The type of the value stored in the array.</typeparam>
public class IndexableArray3D<T> : IIndexable<Index3D, T>
{
	/// <summary>
	/// The array being wrapped by this implementation.
	/// </summary>
	private readonly T[,,] array;

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexableArray3D{TValue}"/> class.
	/// </summary>
	/// <param name="array">The array to wrap.</param>
	public IndexableArray3D(T[,,] array)
	{
		Debug.Assert(array != null);
		Debug.Assert(array.HasCapacity());

		this.array = array;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="IndexableArray3D{TValue}"/> class.
	/// </summary>
	/// <param name="dimensions">The dimensions of the array.</param>
	public IndexableArray3D(Index3D dimensions)
		: this(dimensions.CreateArray<T>())
	{
	}

	/// <inheritdoc />
	public Index3D Dimensions => this.array.GetDimensions();

	/// <inheritdoc />
	public Index3D LowerBounds => this.array.GetLowerBounds();

	/// <inheritdoc />
	public Index3D UpperBounds => this.array.GetUpperBounds();

	/// <inheritdoc />
	public T this[Index3D index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.array.Get(index);
		}

		set
		{
			IIndexableContracts.IndexerSet(this, index);

			this.array.Set(index, value);
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index3D, T>> GetEnumerator() => this.array.GetIndexValuePairs().GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
