﻿namespace HQS.Utility.Indexing.Indexables;

/// <summary>
/// Provides extension methods for use with implementations of the <see cref="IIndexingBounds{TIndex}"/> interface.
/// </summary>
public static class IIndexingBoundsExtensions
{
	/// <summary>
	/// Determines whether the specified index is within the bounds of the <see cref="IIndexingBounds{TIndex}"/>.
	/// </summary>
	/// <typeparam name="TIndex">The type of the index.</typeparam>
	/// <param name="bounds">The bounds to check against.</param>
	/// <param name="index">The index to check.</param>
	/// <returns>
	///   <c>true</c> if the index is within bounds; otherwise, <c>false</c>.
	/// </returns>
	public static bool IsIndexInBounds<TIndex>(this IIndexingBounds<TIndex> bounds, TIndex index)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(bounds != null);
		Debug.Assert(bounds.Dimensions.Rank == index.Rank);

		return index.IsIn(bounds.LowerBounds, bounds.UpperBounds);
	}

	public static TIndex GetMidpoint<TIndex>(this IIndexingBounds<TIndex> bounds, bool roundUp = false)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(bounds != null);

		return bounds.LowerBounds.Midpoint(bounds.UpperBounds, roundUp);
	}

	public static IEnumerable<TIndex> GetIndices<TIndex>(this IIndexingBounds<TIndex> bounds)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(bounds != null);

		return bounds.LowerBounds.Range(bounds.Dimensions);
	}
}
