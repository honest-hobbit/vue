﻿namespace HQS.Utility.Indexing.Indexables;

public static class IIndexableViewExtensions
{
	public static bool TryGetValue<TIndex, TValue>(this IIndexableView<TIndex, TValue> indexable, TIndex index, out TValue value)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(indexable != null);

		if (indexable.IsIndexInBounds(index))
		{
			value = indexable[index];
			return true;
		}
		else
		{
			value = default;
			return false;
		}
	}
}
