﻿namespace HQS.Utility.Indexing.Indexables;

public static class IIndexableExtensions
{
	public static IIndexableView<TIndex, TValue> AsReadOnly<TIndex, TValue>(this IIndexable<TIndex, TValue> indexable)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(indexable != null);

		return new IndexableView<TIndex, TValue>(indexable);
	}
}
