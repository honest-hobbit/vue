﻿namespace HQS.Utility.Indexing.Indexables;

public class ConstantIndexable<TIndex, TValue> : ConstantIndexableView<TIndex, TValue>, IIndexable<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	private readonly bool setThrowsException;

	public ConstantIndexable(TValue constantValue, TIndex dimensions, bool setThrowsException = true)
		: base(constantValue, dimensions)
	{
		this.setThrowsException = setThrowsException;
	}

	public ConstantIndexable(TValue constantValue, TIndex lowerBounds, TIndex dimensions, bool setThrowsException = true)
		: base(constantValue, lowerBounds, dimensions)
	{
		this.setThrowsException = setThrowsException;
	}

	/// <inheritdoc />
	public new TValue this[TIndex index]
	{
		get { return base[index]; }

		set
		{
			if (this.setThrowsException)
			{
				throw new NotSupportedException("Modifying constant indexable is not allowed.");
			}
		}
	}
}
