﻿namespace HQS.Utility.Indexing.Indexables;

public class ConstantIndexableView<TIndex, TValue> : IndexingBounds<TIndex>, IIndexableView<TIndex, TValue>
	where TIndex : struct, IIndex<TIndex>
{
	private readonly TValue constantValue;

	public ConstantIndexableView(TValue constantValue, TIndex dimensions)
		: base(dimensions)
	{
		this.constantValue = constantValue;
	}

	public ConstantIndexableView(TValue constantValue, TIndex lowerBounds, TIndex dimensions)
		: base(lowerBounds, dimensions)
	{
		this.constantValue = constantValue;
	}

	/// <inheritdoc />
	public TValue this[TIndex index]
	{
		get
		{
			IIndexableViewContracts.IndexerGet(this, index);

			return this.constantValue;
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<TIndex, TValue>> GetEnumerator() =>
		this.GetIndices().Select(index => IndexValuePair.New(index, this.constantValue)).GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
