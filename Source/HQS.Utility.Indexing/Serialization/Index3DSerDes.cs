﻿namespace HQS.Utility.Indexing.Serialization;

public class Index3DSerDes : CompositeSerDes<int, int, int, Index3D>
{
	public Index3DSerDes(ISerDes<int> serDes)
		: base(serDes, serDes, serDes)
	{
	}

	public Index3DSerDes(ISerDes<int> xSerDes, ISerDes<int> ySerDes, ISerDes<int> zSerDes)
		: base(xSerDes, ySerDes, zSerDes)
	{
	}

	public static ISerDes<Index3D> Instance { get; } = new Index3DSerDes(SerDes.OfInt);

	/// <inheritdoc />
	protected override Index3D ComposeValue(int x, int y, int z) => new Index3D(x, y, z);

	/// <inheritdoc />
	protected override void DecomposeValue(Index3D value, out int x, out int y, out int z)
	{
		x = value.X;
		y = value.Y;
		z = value.Z;
	}
}
