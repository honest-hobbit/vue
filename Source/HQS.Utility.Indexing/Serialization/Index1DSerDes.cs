﻿namespace HQS.Utility.Indexing.Serialization;

public static class Index1DSerDes
{
	public static ISerDes<Index1D> Instance { get; } = Create(SerDes.OfInt);

	public static ISerDes<Index1D> Create(ISerDes<int> serDes) => ConverterSerDes.Create(x => x.X, x => new Index1D(x), serDes);
}
