﻿namespace HQS.Utility.Indexing.Serialization;

public class Index2DSerDes : CompositeSerDes<int, int, Index2D>
{
	public Index2DSerDes(ISerDes<int> serDes)
		: base(serDes, serDes)
	{
	}

	public Index2DSerDes(ISerDes<int> xSerDes, ISerDes<int> ySerDes)
		: base(xSerDes, ySerDes)
	{
	}

	public static ISerDes<Index2D> Instance { get; } = new Index2DSerDes(SerDes.OfInt);

	/// <inheritdoc />
	protected override Index2D ComposeValue(int x, int y) => new Index2D(x, y);

	/// <inheritdoc />
	protected override void DecomposeValue(Index2D value, out int x, out int y)
	{
		x = value.X;
		y = value.Y;
	}
}
