﻿namespace HQS.Utility.Indexing.Serialization;

public class Index4DSerDes : CompositeSerDes<int, int, int, int, Index4D>
{
	public Index4DSerDes(ISerDes<int> serDes)
		: base(serDes, serDes, serDes, serDes)
	{
	}

	public Index4DSerDes(ISerDes<int> xSerDes, ISerDes<int> ySerDes, ISerDes<int> zSerDes, ISerDes<int> wSerDes)
		: base(xSerDes, ySerDes, zSerDes, wSerDes)
	{
	}

	public static ISerDes<Index4D> Instance { get; } = new Index4DSerDes(SerDes.OfInt);

	/// <inheritdoc />
	protected override Index4D ComposeValue(int x, int y, int z, int w) => new Index4D(x, y, z, w);

	/// <inheritdoc />
	protected override void DecomposeValue(Index4D value, out int x, out int y, out int z, out int w)
	{
		x = value.X;
		y = value.Y;
		z = value.Z;
		w = value.W;
	}
}
