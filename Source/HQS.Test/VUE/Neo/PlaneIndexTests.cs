﻿namespace HQS.Test.VUE.Neo;

public static class PlaneIndexTests
{
	public static IEnumerable<object[]> SampleData()
	{
		var random = new Random(42);
		var normals = Enumeration.Values<PlaneNormal>();

		for (int i = 0; i < normals.Count; i++)
		{
			yield return new object[] { normals[i], PlaneIndex.MinDepth };
			yield return new object[] { normals[i], PlaneIndex.MaxDepth };

			for (int count = 0; count < 10; count++)
			{
				int depth = random.Next(PlaneIndex.MinDepth, PlaneIndex.MaxDepth);
				yield return new object[] { normals[i], depth };
			}
		}
	}

	public static IEnumerable<object[]> SampleIndices()
	{
		yield return new object[] { ushort.MinValue };

		var random = new Random(42);

		for (int i = 0; i < 100;)
		{
			var value = (ushort)random.Next(ushort.MinValue, ushort.MaxValue);
			var temp = new PlaneIndex(value);
			if (Enumeration.IsDefined(temp.Normal))
			{
				yield return new object[] { value };
				i++;
			}
		}
	}

	[Theory]
	[MemberData(nameof(SampleData))]
	public static void ConvertToAndFromSampleData(PlaneNormal normal, int depth)
	{
		var subject = PlaneIndex.From(normal, depth);

		subject.Normal.Should().Be(normal);
		subject.Depth.Should().Be(depth);
	}

	[Theory]
	[MemberData(nameof(SampleIndices))]
	public static void ConvertToAndFromIndex(ushort index)
	{
		var subject = new PlaneIndex(index);
		var converted = PlaneIndex.From(subject.Normal, subject.Depth);

		subject.Index.Should().Be(index);
		converted.Index.Should().Be(index);
		converted.Index.Should().Be(subject.Index);
		converted.Normal.Should().Be(subject.Normal);
		converted.Depth.Should().Be(subject.Depth);
	}

	[Fact]
	public static void SortByNormalAndThenDepth()
	{
		foreach (var pair in GenerateIndices().AsPairs())
		{
			pair.Previous.Index.Should().BeLessThan(pair.Next.Index);
		}

		IEnumerable<PlaneIndex> GenerateIndices()
		{
			foreach (var normal in Enumeration.Values<PlaneNormal>())
			{
				for (int depth = PlaneIndex.MinDepth; depth <= PlaneIndex.MaxDepth; depth++)
				{
					yield return PlaneIndex.From(normal, depth);
				}
			}
		}
	}
}
