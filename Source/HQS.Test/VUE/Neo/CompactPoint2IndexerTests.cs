﻿namespace HQS.Test.VUE.Neo;

public static class CompactPoint2IndexerTests
{
	public static IEnumerable<object[]> SizeExponentValues()
	{
		for (int i = ChunkSize.MinExponent; i <= ChunkSize.MaxExponent; i++)
		{
			yield return new object[] { new ChunkSize(i) };
		}
	}

	[Theory]
	[MemberData(nameof(SizeExponentValues))]
	public static void ConvertToAndFromPoint(ChunkSize size)
	{
		var subject = new CompactPoint2Indexer(size);

		int length = CompactPoint2Indexer.GetLength(size);
		for (int i = 0; i < length; i++)
		{
			var original = new CompactPoint2((ushort)i);
			subject.GetPosition(original, out var x, out var y);
			var result = subject.GetPoint(x, y);

			result.Should().Be(original);
		}
	}

	[InlineData(0, 0, 0)]
	[InlineData(1, 1, 1)]
	[InlineData(2, 0, 2)]
	[InlineData(3, 1, 3)]
	[InlineData(4, 0, 4)]
	[InlineData(0, 2, 5)]
	[InlineData(1, 3, 6)]
	[InlineData(2, 2, 7)]
	[InlineData(3, 3, 8)]
	[InlineData(4, 2, 9)]
	[InlineData(0, 4, 10)]
	[InlineData(1, 5, 11)]
	[InlineData(2, 4, 12)]
	[InlineData(3, 5, 13)]
	[InlineData(4, 4, 14)]
	[Theory]
	public static void ConvertToAndFromSampleData(int x, int y, ushort index)
	{
		var subject = new CompactPoint2Indexer(new ChunkSize(1));

		var fromIndex = new CompactPoint2(index);
		subject.GetPosition(fromIndex, out var positionX, out var positionY);
		var fromPoint = subject.GetPoint(x, y);

		fromPoint.Should().Be(fromIndex);
		fromPoint.Index.Should().Be(index);
		positionX.Should().Be(x);
		positionY.Should().Be(y);
	}
}