﻿namespace HQS.Test.VUE.Neo;

public static class WedgePlaneTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
			new object[] { QuadParts.None },

			new object[] { QuadParts.Top },
			new object[] { QuadParts.Right },
			new object[] { QuadParts.Bottom },
			new object[] { QuadParts.Left },

			new object[] { QuadParts.Top.Add(QuadParts.Right) },
			new object[] { QuadParts.Top.Add(QuadParts.Bottom) },
			new object[] { QuadParts.Top.Add(QuadParts.Left) },

			new object[] { QuadParts.Right.Add(QuadParts.Bottom) },
			new object[] { QuadParts.Right.Add(QuadParts.Left) },

			new object[] { QuadParts.Bottom.Add(QuadParts.Left) },

			new object[] { QuadParts.All.Remove(QuadParts.Top) },
			new object[] { QuadParts.All.Remove(QuadParts.Right) },
			new object[] { QuadParts.All.Remove(QuadParts.Bottom) },
			new object[] { QuadParts.All.Remove(QuadParts.Left) },

			new object[] { QuadParts.All },
		};

	public static IEnumerable<object[]> ValuePairs()
	{
		foreach (var first in Values())
		{
			foreach (var second in Values())
			{
				yield return new object[] { (QuadParts)first[0], (QuadParts)second[0] };
			}
		}
	}

	[Fact]
	public static void ByteConstructor()
	{
		var subject = new WedgePlane(0);

		subject.Upper.Should().Be(QuadParts.None);
		subject.Lower.Should().Be(QuadParts.None);

		subject = new WedgePlane(255);

		subject.Upper.Should().Be(QuadParts.All);
		subject.Lower.Should().Be(QuadParts.All);
	}

	[Theory]
	[MemberData(nameof(ValuePairs))]
	public static void FaceTrianglesConstructor(QuadParts positive, QuadParts negative)
	{
		var subject = new WedgePlane(positive, negative);

		subject.Upper.Should().Be(positive);
		subject.Lower.Should().Be(negative);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public static void Upper(QuadParts upper)
	{
		var subject = new WedgePlane(0);
		subject.Upper = upper;
		subject.Lower = QuadParts.None;

		subject.Upper.Should().Be(upper);
		subject.Lower.Should().Be(QuadParts.None);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public static void Lower(QuadParts lower)
	{
		var subject = new WedgePlane(0);
		subject.Upper = QuadParts.None;
		subject.Lower = lower;

		subject.Upper.Should().Be(QuadParts.None);
		subject.Lower.Should().Be(lower);
	}

	[Theory]
	[MemberData(nameof(ValuePairs))]
	public static void UpperAndLower(QuadParts upper, QuadParts lower)
	{
		var subject = new WedgePlane(0);
		subject.Upper = upper;
		subject.Lower = lower;

		subject.Upper.Should().Be(upper);
		subject.Lower.Should().Be(lower);

		subject = new WedgePlane(0);
		subject.Lower = lower;
		subject.Upper = upper;

		subject.Lower.Should().Be(lower);
		subject.Upper.Should().Be(upper);
	}
}
