﻿namespace HQS.Test.VUE.Neo;

public static class VertexMapTests
{
	[Fact]
	public static void ClearMappedVerticesUntilVertexThresholdResets()
	{
		var subject = new PointToVertexIndexMap(ChunkSize.Max);
		var pointToVertexIndex = new Dictionary<CompactPoint2, int>(EqualityComparer.ForStruct<CompactPoint2>());

		for (int count = 0; count < 5; count++)
		{
			int expectedVertexIndex = 0;

			int length = subject.Length;
			for (int i = 0; i < length; i++)
			{
				var point = new CompactPoint2((ushort)i);
				var shared = subject.TryGetOrCreateVertexIndex(point, out var vertexIndex);

				shared.Should().BeFalse();
				vertexIndex.Should().Be(expectedVertexIndex);

				expectedVertexIndex++;
				pointToVertexIndex.Add(point, vertexIndex);
			}

			foreach (var pair in pointToVertexIndex)
			{
				var shared = subject.TryGetOrCreateVertexIndex(pair.Key, out var vertexIndex);

				shared.Should().BeTrue();
				vertexIndex.Should().Be(pair.Value);
			}

			pointToVertexIndex.Clear();

			int previousVertexThreshold = subject.VertexThreshold;
			subject.Clear();

			previousVertexThreshold.Should().BeLessThan(subject.VertexThreshold);
		}
	}
}
