﻿namespace HQS.Test.VUE.Neo;

public static class WedgeHexEncodingTests
{
	public static IEnumerable<object[]> FaceTriangleValues() =>
		new object[][]
		{
			new object[] { QuadParts.None },
			new object[] { QuadParts.All },
			new object[] { QuadParts.Bottom },
			new object[] { QuadParts.Left },
			new object[] { QuadParts.Right },
			new object[] { QuadParts.Top },
			new object[] { QuadParts.TopLeft },
			new object[] { QuadParts.TopRight },
			new object[] { QuadParts.BottomRight },
			new object[] { QuadParts.BottomLeft },
		};

	[Theory]
	[MemberData(nameof(FaceTriangleValues))]
	public static void EncodeFaceTriangles(QuadParts subject)
	{
		var serialized = WedgeHexEncoding.ToChar(subject);
		var result = WedgeHexEncoding.ToFaceTriangles(serialized);
		result.Should().Be(subject);
	}

	public static IEnumerable<object[]> WedgeCornersValues() =>
		new object[][]
		{
			new object[] { WedgeCorners.None },
			new object[] { WedgeCorners.All },
			new object[] { WedgeCorners.NNN },
			new object[] { WedgeCorners.NNP },
			new object[] { WedgeCorners.NPN },
			new object[] { WedgeCorners.NPP },
			new object[] { WedgeCorners.PNN },
			new object[] { WedgeCorners.PNP },
			new object[] { WedgeCorners.PPN },
			new object[] { WedgeCorners.PPP },
		};

	[Theory]
	[MemberData(nameof(WedgeCornersValues))]
	public static void EncodeWedgeCorners(WedgeCorners subject)
	{
		var serialized = new Span<char>(new char[2]);
		WedgeHexEncoding.ToChar(subject, serialized);
		var result = WedgeHexEncoding.ToWedgeCorners(serialized);
		result.Should().Be(subject);
	}
}
