﻿namespace HQS.Test.VUE.Assets.Grids;

public static class GridAssetTests
{
	[Fact]
	public static void PropertiesShouldNotBeNull() => AsyncContext.Run(async () =>
	{
		var universe = GridTestsSetup.CreateSubject();

		try
		{
			universe.Jobs.Submit(new AssetsShoulNotBeNullJob(), out var completion);
			await completion.MarshallContext();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	private class AssetsShoulNotBeNullJob : IJob
	{
		/// <inheritdoc />
		public void RunOn(IJobAssets assets)
		{
			assets.Should().NotBeNull();

			assets.GetGrids2D().Should().NotBeNull();
			assets.GetGrids2D().Committed.Should().NotBeNull();

			var grid2D = assets.GetGrids2D().CreateNew<int>(default(ChunkConfig));
			grid2D.Previous.Should().NotBeNull();

			assets.GetGrids3D().Should().NotBeNull();
			assets.GetGrids3D().Committed.Should().NotBeNull();

			var grid3D = assets.GetGrids3D().CreateNew<int>(default(ChunkConfig));
			grid3D.Previous.Should().NotBeNull();
		}
	}
}
