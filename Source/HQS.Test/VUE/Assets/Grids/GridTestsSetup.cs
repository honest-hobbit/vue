﻿namespace HQS.Test.VUE.Assets.Grids;

internal static class GridTestsSetup
{
	public static VoxelUniverseSubject CreateSubject()
	{
		var builder = new VoxelUniverseBuilder();
		builder.RegisterGridsAssets();

		return new VoxelUniverseSubject(builder.BuildUniverse());
	}
}
