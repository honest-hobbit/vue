﻿namespace HQS.Test.VUE.Assets.Development;

internal static class DevelopmentTestsSetup
{
	public static VoxelUniverseSubject CreateSubject()
	{
		var builder = new VoxelUniverseBuilder();
		builder.RegisterDevelopmentAssets();

		return new VoxelUniverseSubject(builder.BuildUniverse());
	}
}
