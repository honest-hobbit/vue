﻿namespace HQS.Test.VUE.Assets.Development;

public static class DevelopmentAssetTests
{
	[Fact]
	public static void PropertiesShouldNotBeNull() => AsyncContext.Run(async () =>
	{
		var universe = DevelopmentTestsSetup.CreateSubject();

		try
		{
			universe.Jobs.Submit(new AssetsShoulNotBeNullJob(), out var completion);
			await completion.MarshallContext();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	private class AssetsShoulNotBeNullJob : IJob
	{
		/// <inheritdoc />
		public void RunOn(IJobAssets assets)
		{
			assets.Should().NotBeNull();
			assets.GetPlaceholders().Should().NotBeNull();
			assets.GetPlaceholders().Committed.Should().NotBeNull();

			var placeholder = assets.GetPlaceholders().CreateNew();
			placeholder.Previous.Should().NotBeNull();
		}
	}
}
