﻿namespace HQS.Test.VUE.Assets.Coplanar;

public static class CoplanarAssetTests
{
	[Fact]
	public static void PropertiesShouldNotBeNull() => AsyncContext.Run(async () =>
	{
		var universe = CoplanarTestsSetup.CreateSubject();

		try
		{
			universe.Jobs.Submit(new AssetsShoulNotBeNullJob(), out var completion);
			await completion.MarshallContext();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	private class AssetsShoulNotBeNullJob : IJob
	{
		/// <inheritdoc />
		public void RunOn(IJobAssets assets)
		{
			assets.Should().NotBeNull();

			assets.GetGrids2D().Should().NotBeNull();
			assets.GetGrids2D().Committed.Should().NotBeNull();

			assets.GetGrids3D().Should().NotBeNull();
			assets.GetGrids3D().Committed.Should().NotBeNull();

			assets.GetShapes3D().Should().NotBeNull();
			assets.GetShapes3D().Committed.Should().NotBeNull();

			var shape = assets.GetShapes3D().CreateNew();
			shape.Previous.Should().NotBeNull();

			assets.GetVoxelPalettes().Should().NotBeNull();
			assets.GetVoxelPalettes().Committed.Should().NotBeNull();

			var voxelPalette = assets.GetVoxelPalettes().CreateNew();
			voxelPalette.Previous.Should().NotBeNull();

			assets.GetSurfacePalettes().Should().NotBeNull();
			assets.GetSurfacePalettes().Committed.Should().NotBeNull();

			var surfacePalette = assets.GetSurfacePalettes().CreateNew();
			surfacePalette.Previous.Should().NotBeNull();
		}
	}
}
