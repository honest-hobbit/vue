﻿namespace HQS.Test.VUE.Assets.Coplanar;

internal static class CoplanarTestsSetup
{
	public static VoxelUniverseSubject CreateSubject()
	{
		var builder = new VoxelUniverseBuilder();
		builder.RegisterGridsAssets();
		builder.RegisterCoplanerAssets();

		return new VoxelUniverseSubject(builder.BuildUniverse());
	}
}
