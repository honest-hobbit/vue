﻿namespace HQS.Test.VUE.Core;

public static class JobExceptionTests
{
	[Fact]
	public static void Submit() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var error = new DeliberateTestException();
			var job = new TestSubmitJob() { Error = error, };

			universe.AddExpectedError(error);
			universe.Jobs.Submit(job);
			job.WaitUntilRan();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPinView() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var error = new DeliberateTestException();
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			var jobPin = pool.Rent();
			jobPin.Value.Error = error;

			universe.AddExpectedError(error);
			universe.Jobs.Submit(jobPin.AsPinned);
			jobPin.Value.WaitUntilRan();

			// submitting job as PinView means it won't be auto disposed
			jobPin.IsDisposed.Should().Be(false);

			// ensure that we have the only remaining pin on the job
			// and that disposing that pin releases the job to the pool
			var job = jobPin.Value;
			jobPin.Dispose();
			job.WaitUntilReturnedToPool();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPin() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var error = new DeliberateTestException();
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			// submitting job as pin will auto dispose the pin
			// so hold onto the job itself in a separate field
			// so we can access it after it runs
			var jobPin = pool.Rent();
			var job = jobPin.Value;
			job.Error = error;

			universe.AddExpectedError(error);
			universe.Jobs.Submit(jobPin);
			job.WaitUntilRan();
			job.WaitUntilReturnedToPool();

			// ensure the job pin was auto disposed
			jobPin.IsDisposed.Should().Be(true);
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitWithCompletion() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var error = new DeliberateTestException();
			var job = new TestSubmitJob() { Error = error, };

			universe.AddExpectedError(error);
			universe.Jobs.Submit(job, out var completion);

			try
			{
				await completion.MarshallContext();
			}
			catch (DeliberateTestException caught)
			{
				caught.Should().Be(error);
			}
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPinViewWithCompletion() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var error = new DeliberateTestException();
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			var jobPin = pool.Rent();
			jobPin.Value.Error = error;

			universe.AddExpectedError(error);
			universe.Jobs.Submit(jobPin.AsPinned, out var completion);

			try
			{
				await completion.MarshallContext();
			}
			catch (DeliberateTestException caught)
			{
				caught.Should().Be(error);
			}

			// submitting job as PinView means it won't be auto disposed
			jobPin.IsDisposed.Should().Be(false);

			// ensure that we have the only remaining pins on the job
			// and that disposing that pin releases the job to the pool
			var job = jobPin.Value;
			jobPin.Dispose();
			job.WaitUntilReturnedToPool();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPinWithCompletion() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var error = new DeliberateTestException();
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			var jobPin = pool.Rent();
			var job = jobPin.Value;
			job.Error = error;

			universe.AddExpectedError(error);
			universe.Jobs.Submit(jobPin, out var completion);

			try
			{
				await completion.MarshallContext();
			}
			catch (DeliberateTestException caught)
			{
				caught.Should().Be(error);
			}

			// ensure the submitted job pin was auto disposed
			jobPin.IsDisposed.Should().Be(true);
			job.WaitUntilReturnedToPool();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});
}
