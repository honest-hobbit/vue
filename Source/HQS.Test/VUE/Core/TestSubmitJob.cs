﻿namespace HQS.Test.VUE.Core;

internal class TestSubmitJob : AbstractDisposable, IJob, IDeinitializable
{
	private static readonly TimeSpan Timeout = TimeSpan.FromSeconds(5);

	private readonly ManualResetEvent ran = new ManualResetEvent(false);

	private readonly ManualResetEvent deinitialized = new ManualResetEvent(false);

	public int ExpectedValue { get; set; }

	public int ResultValue { get; private set; }

	public Exception Error { get; set; }

	/// <inheritdoc />
	public void RunOn(IJobAssets resources)
	{
		try
		{
			if (this.Error != null)
			{
				throw this.Error;
			}

			this.ResultValue = this.ExpectedValue;
		}
		finally
		{
			this.ran.Set();
		}
	}

	/// <inheritdoc />
	public void Deinitialize() => this.deinitialized.Set();

	public void WaitUntilRan()
	{
		if (!this.ran.WaitOne(Timeout))
		{
			throw new TimeoutException($"{nameof(this.WaitUntilRan)} timed out.");
		}
	}

	public void WaitUntilReturnedToPool()
	{
		// deinitialize will be called by the pool just before the job is added back to the pool
		if (!this.deinitialized.WaitOne(Timeout))
		{
			throw new TimeoutException($"{nameof(this.WaitUntilReturnedToPool)} timed out.");
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.ran.Dispose();
		this.deinitialized.Dispose();
	}
}
