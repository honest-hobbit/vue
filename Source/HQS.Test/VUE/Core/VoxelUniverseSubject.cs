﻿namespace HQS.Test.VUE.Core;

// wraps a VoxelUniverse with a BlockingCollection to catch Exceptions reported by ErrorOccurred Observable
[SuppressMessage("Design", "CA1001", Justification = "IAsyncCompletable handles disposal.")]
internal class VoxelUniverseSubject : AbstractCompletable, IVoxelUniverse
{
	private readonly IVoxelUniverse universe;

	private readonly BlockingCollection<Exception> errors = new BlockingCollection<Exception>();

	private readonly ConcurrentDictionary<Exception, Exception> expectedErrors = new ConcurrentDictionary<Exception, Exception>();

	public VoxelUniverseSubject(IVoxelUniverse universe)
	{
		Debug.Assert(universe != null);

		this.universe = universe;
		this.universe.Diagnostics.ErrorOccurred.Subscribe(e => this.errors.Add(e), () => this.errors.CompleteAdding());
	}

	/// <inheritdoc />
	public HQS.VUE.Core.IDiagnostics Diagnostics => this.universe.Diagnostics;

	/// <inheritdoc />
	public IJobSubmitter Jobs => this.universe.Jobs;

	public void AddExpectedError(Exception error)
	{
		Debug.Assert(error != null);

		if (!this.expectedErrors.TryAdd(error, error))
		{
			throw new ArgumentException("Failed to add expected error.", nameof(error));
		}
	}

	protected override async Task CompleteAsync()
	{
		try
		{
			this.universe.Complete();
			await this.universe.Completion.MarshallContext();

			// check for any errors that were not expected
			foreach (var error in this.errors.GetConsumingEnumerable())
			{
				if (!this.expectedErrors.TryRemove(error, out var _))
				{
					throw new InvalidOperationException("Unexpected exception.", error);
				}
			}

			// check for any expected errors that were not thrown
			foreach (var error in this.expectedErrors.Keys)
			{
				throw new InvalidOperationException("Expected exception not thrown.", error);
			}
		}
		finally
		{
			this.errors.Dispose();
		}
	}
}
