﻿namespace HQS.Test.VUE.Core;

public static class UniverseTests
{
	[Fact]
	public static void PropertiesShouldNotBeNull() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		universe.Completion.Should().NotBeNull();
		universe.Jobs.Should().NotBeNull();
		universe.Diagnostics.Should().NotBeNull();
		universe.Diagnostics.ErrorOccurred.Should().NotBeNull();
		universe.Diagnostics.Pools.Should().NotBeNull();

		universe.Complete();
		await universe.Completion.MarshallContext();
	});
}
