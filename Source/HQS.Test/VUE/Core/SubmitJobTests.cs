﻿namespace HQS.Test.VUE.Core;

public static class SubmitJobTests
{
	[Fact]
	public static void Submit() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var job = new TestSubmitJob() { ExpectedValue = 7, };
			job.ResultValue.Should().NotBe(job.ExpectedValue);

			universe.Jobs.Submit(job);
			job.WaitUntilRan();

			job.ResultValue.Should().Be(job.ExpectedValue);
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPinView() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			var jobPin = pool.Rent();
			jobPin.Value.ExpectedValue = 7;
			jobPin.Value.ResultValue.Should().NotBe(jobPin.Value.ExpectedValue);

			universe.Jobs.Submit(jobPin.AsPinned);
			jobPin.Value.WaitUntilRan();

			// submitting job as PinView means it won't be auto disposed
			jobPin.IsDisposed.Should().Be(false);
			jobPin.Value.ResultValue.Should().Be(jobPin.Value.ExpectedValue);

			// ensure that we have the only remaining pin on the job
			// and that disposing that pin releases the job to the pool
			var job = jobPin.Value;
			jobPin.Dispose();
			job.WaitUntilReturnedToPool();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPin() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			// submitting job as pin will auto dispose the pin
			// so hold onto the job itself in a separate field
			// so we can access it after it runs
			var jobPin = pool.Rent();
			var job = jobPin.Value;
			job.ExpectedValue = 7;
			job.ResultValue.Should().NotBe(job.ExpectedValue);

			universe.Jobs.Submit(jobPin);
			job.WaitUntilRan();
			job.WaitUntilReturnedToPool();

			// ensure the job pin was auto disposed
			jobPin.IsDisposed.Should().Be(true);
			job.ResultValue.Should().Be(job.ExpectedValue);
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitWithCompletion() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var job = new TestSubmitJob() { ExpectedValue = 7, };
			job.ResultValue.Should().NotBe(job.ExpectedValue);

			universe.Jobs.Submit(job, out var completion);
			await completion.MarshallContext();

			job.ResultValue.Should().Be(job.ExpectedValue);
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPinViewWithCompletion() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			var jobPin = pool.Rent();
			jobPin.Value.ExpectedValue = 7;
			jobPin.Value.ResultValue.Should().NotBe(jobPin.Value.ExpectedValue);

			universe.Jobs.Submit(jobPin.AsPinned, out var completion);
			var resultPin = await completion.MarshallContext();

			// submitting job as PinView means it won't be auto disposed
			resultPin.IsDisposed.Should().Be(false);
			jobPin.IsDisposed.Should().Be(false);
			jobPin.Value.ResultValue.Should().Be(jobPin.Value.ExpectedValue);

			// ensure that we have the only remaining pins on the job
			// and that disposing that pin releases the job to the pool
			var job = jobPin.Value;
			jobPin.Dispose();
			resultPin.Dispose();
			job.WaitUntilReturnedToPool();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});

	[Fact]
	public static void SubmitPinWithCompletion() => AsyncContext.Run(async () =>
	{
		var universe = CoreTestsSetup.CreateSubject();

		try
		{
			var pool = Pool.ThreadSafe.Pins.Create<TestSubmitJob>();

			var jobPin = pool.Rent();
			jobPin.Value.ExpectedValue = 7;
			jobPin.Value.ResultValue.Should().NotBe(jobPin.Value.ExpectedValue);

			universe.Jobs.Submit(jobPin, out var completion);
			var resultPin = await completion.MarshallContext();

			// ensure the submitted job pin was auto disposed but not the awaited result pin
			jobPin.IsDisposed.Should().Be(true);
			resultPin.IsDisposed.Should().Be(false);
			resultPin.Value.ResultValue.Should().Be(resultPin.Value.ExpectedValue);

			var job = resultPin.Value;
			resultPin.Dispose();
			job.WaitUntilReturnedToPool();
		}
		finally
		{
			universe.Complete();
			await universe.Completion.MarshallContext();
		}
	});
}
