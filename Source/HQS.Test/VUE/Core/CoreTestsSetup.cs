﻿namespace HQS.Test.VUE.Core;

internal static class CoreTestsSetup
{
	public static VoxelUniverseSubject CreateSubject()
	{
		var builder = new VoxelUniverseBuilder();

		return new VoxelUniverseSubject(builder.BuildUniverse());
	}
}
