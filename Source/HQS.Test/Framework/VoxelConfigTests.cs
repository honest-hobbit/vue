﻿////using FluentAssertions;
////using HQS.VUE;
////using HQS.Utility.Indexing.Indices;
////using Xunit;

////namespace HQS.Test.Framework
////{
////	public static class VoxelConfigTests
////	{
////		[Fact]
////		public static void ConvertVoxelIndex()
////		{
////			var config = new VoxelSizeConfig(subchunkExponent: 2, voxelExponent: 1);
////			Index3D voxelIndex;

////			// chunk (-2, 0, 0)
////			voxelIndex = new Index3D(-9, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(3, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			// chunk (-1, 0, 0)
////			voxelIndex = new Index3D(-8, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(-7, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(-6, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(-5, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(-4, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(-3, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(-2, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(3, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(-1, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(-1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(3, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			// chunk (0, 0, 0)
////			voxelIndex = new Index3D(0, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(1, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(2, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(3, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(4, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(5, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(6, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(3, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(7, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(3, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			// chunk (1, 0, 0)
////			voxelIndex = new Index3D(8, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(9, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(10, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(11, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(12, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(13, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			voxelIndex = new Index3D(14, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(3, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));

////			voxelIndex = new Index3D(15, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(3, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(1, 0, 0));

////			// chunk (2, 0, 0)
////			voxelIndex = new Index3D(16, 0, 0);
////			config.ConvertVolumeVoxelToChunkIndex(voxelIndex).Should().Equal(new Index3D(2, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////			config.ConvertVolumeVoxelToSubchunkVoxelIndex(voxelIndex).Should().Equal(new Index3D(0, 0, 0));
////		}
////	}
////}
