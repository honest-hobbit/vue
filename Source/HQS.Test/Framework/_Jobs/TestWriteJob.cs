﻿namespace HQS.Test.Framework;

internal class TestWriteJob : WriteSingleVolumeJob
{
	public Int3 VoxelIndex { get; set; }

	public Voxel VoxelToWrite { get; set; } = Voxel.Empty;

	public Voxel VoxelReadBeforeWrite { get; private set; } = Voxel.Empty;

	/// <inheritdoc />
	protected override void Run(IVoxelWriteView volume)
	{
		Debug.Assert(volume != null);

		this.VoxelReadBeforeWrite = volume[this.VoxelIndex];
		volume.Read[this.VoxelIndex].Should().Be(this.VoxelReadBeforeWrite);

		volume[this.VoxelIndex] = this.VoxelToWrite;

		volume.Read[this.VoxelIndex].Should().Be(this.VoxelReadBeforeWrite);
	}
}
