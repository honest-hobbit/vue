﻿namespace HQS.Test.Framework;

internal class TestFailWriteJob : WriteSingleVolumeJob
{
	public Int3 VoxelIndex { get; set; }

	public Voxel VoxelToWrite { get; set; }

	public Exception ExceptionToThrow { get; set; }

	/// <inheritdoc />
	protected override void Run(IVoxelWriteView volume)
	{
		Debug.Assert(volume != null);

		volume[this.VoxelIndex] = this.VoxelToWrite;
		throw this.ExceptionToThrow;
	}
}
