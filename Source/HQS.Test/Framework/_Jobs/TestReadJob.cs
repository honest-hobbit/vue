﻿namespace HQS.Test.Framework;

internal class TestReadJob : ReadSingleVolumeJob
{
	public Int3 VoxelIndex { get; set; }

	public Voxel VoxelRead { get; private set; } = Voxel.Empty;

	/// <inheritdoc />
	protected override void Run(IVoxelReadView volume)
	{
		Debug.Assert(volume != null);

		this.VoxelRead = volume[this.VoxelIndex];
	}
}
