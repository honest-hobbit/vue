﻿namespace HQS.Test.Framework;

public static class VoxelUniverseTests
{
	public static class ReadVoxel
	{
		[Fact]
		public static void RunTest()
		{
			AsyncContext.Run(() => RunTestAsync(CreateUniverse()));
		}

		private static async Task RunTestAsync(VoxelUniverseOLD universe)
		{
			Debug.Assert(universe != null);

			try
			{
				var volume = universe.Volumes.CreateVolume();
				universe.Diagnostics.ErrorOccurred.Subscribe(onNext: x => throw x);

				var job = new TestReadJob() { Volume = volume };
				await universe.SubmitAndUpdateAsync(job).MarshallContext();
				job.VoxelRead.Should().Be(Voxel.Empty);
			}
			finally
			{
				universe.Complete();
				await universe.Completion.MarshallContext();
			}
		}
	}

	public static class WriteVoxel
	{
		[Fact]
		public static void RunTest()
		{
			AsyncContext.Run(() => RunTestAsync(CreateUniverse()));
		}

		private static async Task RunTestAsync(VoxelUniverseOLD universe)
		{
			Debug.Assert(universe != null);

			try
			{
				int thread1 = Thread.CurrentThread.ManagedThreadId;

				var volume = universe.Volumes.CreateVolume();
				universe.Diagnostics.ErrorOccurred.Subscribe(onNext: x => throw x);

				// initial read should be empty voxel
				var readJob = new TestReadJob() { Volume = volume };
				await universe.SubmitAndUpdateAsync(readJob).MarshallContext();
				readJob.VoxelRead.Should().Be(Voxel.Empty);

				int thread2 = Thread.CurrentThread.ManagedThreadId;

				// write voxel
				var writeVoxel = new Voxel(1);
				var writeJob = new TestWriteJob()
				{
					Volume = volume,
					VoxelToWrite = writeVoxel,
				};

				await universe.SubmitAndUpdateAsync(writeJob).MarshallContext();
				writeJob.VoxelReadBeforeWrite.Should().Be(Voxel.Empty);

				// read the written voxel
				await universe.SubmitAndUpdateAsync(readJob).MarshallContext();
				readJob.VoxelRead.Should().Be(writeVoxel);

				int thread4 = Thread.CurrentThread.ManagedThreadId;
			}
			finally
			{
				universe.Complete();
				await universe.Completion.MarshallContext();

				int thread5 = Thread.CurrentThread.ManagedThreadId;
			}
		}
	}

	public static class WriteVoxelFailed
	{
		[Fact]
		public static void RunTest()
		{
			AsyncContext.Run(() => RunTestAsync(CreateUniverse()));
		}

		private static async Task RunTestAsync(VoxelUniverseOLD universe)
		{
			Debug.Assert(universe != null);

			var errors = new BlockingCollection<Exception>();
			universe.Diagnostics.ErrorOccurred.Subscribe(onNext: e => errors.Add(e));

			try
			{
				var volume = universe.Volumes.CreateVolume();

				// initial read should be empty voxel
				var readJob = new TestReadJob() { Volume = volume };
				await universe.SubmitAndUpdateAsync(readJob).MarshallContext();
				readJob.VoxelRead.Should().Be(Voxel.Empty);

				// write voxel
				var writeVoxel = new Voxel(1);
				var writeJob = new TestFailWriteJob()
				{
					Volume = volume,
					VoxelToWrite = writeVoxel,
					ExceptionToThrow = new DeliberateTestException("The write failed. This is intentional."),
				};

				try
				{
					await universe.SubmitAndUpdateAsync(writeJob).MarshallContext();
				}
				catch (DeliberateTestException e) when (e == writeJob.ExceptionToThrow)
				{
				}

				var exception = errors.Take();
				exception.Should().Be(writeJob.ExceptionToThrow);
				errors.Count.Should().Be(0);

				// read the original voxel
				await universe.SubmitAndUpdateAsync(readJob).MarshallContext();
				readJob.VoxelRead.Should().Be(Voxel.Empty);

				errors.Count.Should().Be(0);
			}
			finally
			{
				errors.CompleteAdding();
				errors.Dispose();

				universe.Complete();
				await universe.Completion.MarshallContext();
			}
		}
	}

	public static class WriteManyVoxels
	{
		[Fact]
		public static void RunTest()
		{
			AsyncContext.Run(() => RunTestAsync(CreateUniverse()));
		}

		private static Lazy<IReadOnlyList<KeyValuePair<Int3, Voxel>>> Voxels { get; } =
			new Lazy<IReadOnlyList<KeyValuePair<Int3, Voxel>>>(() =>
			{
				int countOfVoxels = 100;
				int max = 100;
				int maxVoxelType = 3;
				var random = new Random(7);
				var tempVoxels = new Dictionary<Int3, Voxel>(
					countOfVoxels, HQS.Utility.Numerics.MortonCurve.EqualityComparer.ForInt3);

				for (int count = 0; count < countOfVoxels; count++)
				{
					tempVoxels[new Int3(random.Next(-max, max), random.Next(-max, max), random.Next(-max, max))] =
						new Voxel((ushort)random.Next(1, maxVoxelType + 1));
				}

				return tempVoxels.ToArray();
			});

		private static async Task RunTestAsync(VoxelUniverseOLD universe)
		{
			Debug.Assert(universe != null);

			try
			{
				var volume = universe.Volumes.CreateVolume();
				universe.Diagnostics.ErrorOccurred.Subscribe(onNext: x => throw x);

				var writeJob = new TestWriteJob() { Volume = volume };
				var readJob = new TestReadJob() { Volume = volume };
				var voxels = Voxels.Value;

				// write voxels
				for (int index = 0; index < voxels.Count; index++)
				{
					var pair = voxels[index];
					writeJob.VoxelIndex = pair.Key;
					writeJob.VoxelToWrite = pair.Value;

					await universe.SubmitAndUpdateAsync(writeJob).MarshallContext();
					writeJob.VoxelReadBeforeWrite.Should().Be(Voxel.Empty);
				}

				// read the written voxels
				for (int index = 0; index < voxels.Count; index++)
				{
					var pair = voxels[index];
					readJob.VoxelIndex = pair.Key;

					await universe.SubmitAndUpdateAsync(readJob).MarshallContext();
					readJob.VoxelRead.Should().Be(pair.Value);
				}
			}
			finally
			{
				universe.Complete();
				await universe.Completion.MarshallContext();
			}
		}
	}

	private static VoxelUniverseOLD CreateUniverse()
	{
		var voxelTypes = new VoxelTypesConfigBuilder()
			.StartSurfaceTypes().AddMany(
				SurfaceVisibility.Opaque, SurfaceVisibility.Transparent)
			.StartPolyTypes()
				.Add(new SurfaceTypeIndex(1), new ColorRGB(255, 0, 0, 255))
				.Add(new SurfaceTypeIndex(1), new ColorRGB(0, 255, 0, 255))
				.Add(new SurfaceTypeIndex(1), new ColorRGB(0, 0, 255, 255))
			.StartColliderTypes().AddMany(
				ColliderType.Empty) // this is a placeholder until real collider types added
			.StartVoxelTypes()
				.Add(new PolyTypeIndex(1), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(2), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(3), new ColliderTypeIndex(1))
			.BuildConfig();

		var config = new UniverseConfigBuilder().BuildConfig();

		return new VoxelUniverseOLD(config, voxelTypes);
	}
}
