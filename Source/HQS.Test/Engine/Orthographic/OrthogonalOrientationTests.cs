﻿namespace HQS.Test.Engine.Orthographic;

public static class OrthogonalOrientationTests
{
	[Fact]
	public static void FromTwoWayConversion()
	{
		foreach (var axis in Orthogonal.Orientations)
		{
			var subject1 = OrthogonalOrientation.From(axis);

			subject1.Orientation.Should().Be(axis);

			var subject2 = OrthogonalOrientation.From(subject1.Direction, subject1.Rotation, subject1.IsFlipped);

			subject2.Orientation.Should().Be(axis);
			subject2.Orientation.Should().Be(subject1.Orientation);
			subject2.Direction.Should().Be(subject1.Direction);
			subject2.Rotation.Should().Be(subject1.Rotation);
			subject2.IsFlipped.Should().Be(subject1.IsFlipped);
			subject2.Should().Be(subject1);
		}
	}
}
