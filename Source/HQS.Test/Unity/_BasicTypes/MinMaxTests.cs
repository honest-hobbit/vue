﻿namespace HQS.Test.Unity;

using HQS.Utility.Unity;

public static class MinMaxTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { 0, 0 },
				new object[] { 0, 1 },
				new object[] { 1, 0 },
				new object[] { -1, 0 },
				new object[] { 0, -1 },
				new object[] { -1, 1 },
				new object[] { 1, -1 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void ConstructorInt(int assignToMin, int assignToMax)
	{
		var subject = new MinMaxInt(assignToMin, assignToMax);
		subject = subject.Order();

		CheckResults(subject, assignToMin, assignToMax);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public static void ConstructorFloat(int assignToMin, int assignToMax)
	{
		var subject = new MinMaxFloat(assignToMin, assignToMax);
		subject = subject.Order();

		CheckResults(subject, (float)assignToMin, assignToMax);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public static void FieldsInt(int assignToMin, int assignToMax)
	{
		var subject = default(MinMaxInt);
		subject.Min = assignToMin;
		subject.Max = assignToMax;
		subject = subject.Order();

		CheckResults(subject, assignToMin, assignToMax);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public static void FieldsFloat(int assignToMin, int assignToMax)
	{
		var subject = default(MinMaxFloat);
		subject.Min = assignToMin;
		subject.Max = assignToMax;
		subject = subject.Order();

		CheckResults(subject, (float)assignToMin, assignToMax);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public static void OrderedInt(int assignToMin, int assignToMax)
	{
		var subject = MinMaxInt.Ordered(assignToMin, assignToMax);

		CheckResults(subject, assignToMin, assignToMax);
	}

	[Theory]
	[MemberData(nameof(Values))]
	public static void OrderedFloat(int assignToMin, int assignToMax)
	{
		var subject = MinMaxFloat.Ordered(assignToMin, assignToMax);

		CheckResults(subject, (float)assignToMin, assignToMax);
	}

	private static void CheckResults<TSelf, TValue>(TSelf subject, TValue value1, TValue value2)
		where TSelf : struct, IMinMax<TSelf, TValue>
		where TValue : IComparable<TValue>
	{
		subject.Min.Should().BeLessOrEqualTo(subject.Max);
		if (value1.IsLessThanOrEqualTo(value2))
		{
			subject.Min.Should().Be(value1);
			subject.Max.Should().Be(value2);
		}
		else
		{
			subject.Min.Should().Be(value2);
			subject.Max.Should().Be(value1);
		}
	}
}
