﻿namespace HQS.Test.Unity.Serialization;

using HQS.Utility.Unity.Serialization;
using UnityEngine;

public static class Color32SerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new Color32(0, 0, 0, 0) },
				new object[] { new Color32(255, 0, 0, 0) },
				new object[] { new Color32(0, 255, 0, 0) },
				new object[] { new Color32(0, 0, 255, 0) },
				new object[] { new Color32(0, 0, 0, 255) },
				new object[] { new Color32(255, 255, 255, 255) },
				new object[] { new Color32(33, 164, 113, 218) },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RGBATests(Color32 value) =>
		SerDesTests.RunTests(Color32SerDes.IncludeAlpha, value, Length.OfByte.InBytes * 4);

	[Theory]
	[MemberData(nameof(Values))]
	public static void RGBAlphaIs0Tests(Color32 value) => SerDesTests.RunTests(
		Color32SerDes.AlphaIs0, new Color32(value.r, value.g, value.b, 0), Length.OfByte.InBytes * 3);

	[Theory]
	[MemberData(nameof(Values))]
	public static void RGBAlphaIs255Tests(Color32 value) => SerDesTests.RunTests(
		Color32SerDes.AlphaIs255, new Color32(value.r, value.g, value.b, 255), Length.OfByte.InBytes * 3);
}
