﻿namespace HQS.Test.Utility.Serialization;

public static class ColorSerDesTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new ColorRGB(0, 0, 0, 0) },
				new object[] { new ColorRGB(255, 0, 0, 0) },
				new object[] { new ColorRGB(0, 255, 0, 0) },
				new object[] { new ColorRGB(0, 0, 255, 0) },
				new object[] { new ColorRGB(0, 0, 0, 255) },
				new object[] { new ColorRGB(255, 255, 255, 255) },
				new object[] { new ColorRGB(33, 164, 113, 218) },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RGBATests(ColorRGB value) =>
		SerDesTests.RunTests(ColorRGBSerDes.IncludeAlpha, value, Length.OfByte.InBytes * 4);

	[Theory]
	[MemberData(nameof(Values))]
	public static void RGBAlphaIs0Tests(ColorRGB value) => SerDesTests.RunTests(
		ColorRGBSerDes.AlphaIs0, new ColorRGB(value.R, value.G, value.B, 0), Length.OfByte.InBytes * 3);

	[Theory]
	[MemberData(nameof(Values))]
	public static void RGBAlphaIs255Tests(ColorRGB value) => SerDesTests.RunTests(
		ColorRGBSerDes.AlphaIs255, new ColorRGB(value.R, value.G, value.B, 255), Length.OfByte.InBytes * 3);
}
