﻿namespace HQS.Test.Utility.Serialization;

public static class SerDesTests
{
	public static void RunTests<T>(ISerDes<T> subject, T value, int expectedSerializedLength)
	{
		Debug.Assert(subject != null);

		var bytes = subject.Serialize(value);
		var result = subject.Deserialize(bytes);

		result.Should().Be(value);
		bytes.Length.Should().Be(expectedSerializedLength);
	}
}
