﻿namespace HQS.Test.Utility.Serialization;

public static class Vector2SerializerTests
{
	private const int ExpectedLength = Length.OfFloat.InBytes * 2;

	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new Vector2(float.MinValue, float.MinValue) },
				new object[] { new Vector2(float.MaxValue, float.MaxValue) },
				new object[] { Vector2.Zero },
				new object[] { new Vector2(7.13f, 20.4f) },
				new object[] { new Vector2(-7.13f, -20.4f) },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(Vector2 value) => SerDesTests.RunTests(VectorSerDes.For2D, value, ExpectedLength);
}
