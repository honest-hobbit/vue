﻿namespace HQS.Test.Utility.Serialization;

public static class Vector3SerializerTests
{
	private const int ExpectedLength = Length.OfFloat.InBytes * 3;

	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new Vector3(float.MinValue, float.MinValue, float.MinValue) },
				new object[] { new Vector3(float.MaxValue, float.MaxValue, float.MaxValue) },
				new object[] { Vector3.Zero },
				new object[] { new Vector3(7.13f, 20.4f, 13.9f) },
				new object[] { new Vector3(-7.13f, -20.4f, -13.9f) },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(Vector3 value) => SerDesTests.RunTests(VectorSerDes.For3D, value, ExpectedLength);
}
