﻿namespace HQS.Test.Utility.Serialization;

public static class Vector4SerializerTests
{
	private const int ExpectedLength = Length.OfFloat.InBytes * 4;

	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new Vector4(float.MinValue, float.MinValue, float.MinValue, float.MinValue) },
				new object[] { new Vector4(float.MaxValue, float.MaxValue, float.MaxValue, float.MaxValue) },
				new object[] { Vector4.Zero },
				new object[] { new Vector4(7.13f, 20.4f, 13.9f, 81.4f) },
				new object[] { new Vector4(-7.13f, -20.4f, -13.9f, -81.4f) },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(Vector4 value) => SerDesTests.RunTests(VectorSerDes.For4D, value, ExpectedLength);
}
