﻿namespace HQS.Test.Utility.Serialization;

public static class DecimalSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { decimal.MinValue },
				new object[] { decimal.MaxValue },
				new object[] { -7.13m },
				new object[] { 7.13m },
				new object[] { 0m },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(decimal value) => SerDesTests.RunTests(SerDes.OfDecimal, value, Length.OfDecimal.InBytes);
}
