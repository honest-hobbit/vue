﻿namespace HQS.Test.Utility.Serialization;

public static class DoubleSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { double.MinValue },
				new object[] { double.MaxValue },
				new object[] { -7.13 },
				new object[] { 7.13 },
				new object[] { 0 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(double value) => SerDesTests.RunTests(SerDes.OfDouble, value, Length.OfDouble.InBytes);
}
