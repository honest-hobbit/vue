﻿namespace HQS.Test.Utility.Serialization;

public static class UIntSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { uint.MinValue },
				new object[] { uint.MaxValue },
				new object[] { 7 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(uint value) => SerDesTests.RunTests(SerDes.OfUInt, value, Length.OfUInt.InBytes);
}
