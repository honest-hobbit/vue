﻿namespace HQS.Test.Utility.Serialization;

public static class CharSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { char.MinValue },
				new object[] { char.MaxValue },
				new object[] { 'c' },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(char value) => SerDesTests.RunTests(SerDes.OfChar, value, Length.OfChar.InBytes);
}
