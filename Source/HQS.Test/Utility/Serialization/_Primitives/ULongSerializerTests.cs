﻿namespace HQS.Test.Utility.Serialization;

public static class ULongSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { ulong.MinValue },
				new object[] { ulong.MaxValue },
				new object[] { 7 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunUnsignedTests(ulong value) => SerDesTests.RunTests(SerDes.OfULong, value, Length.OfULong.InBytes);
}
