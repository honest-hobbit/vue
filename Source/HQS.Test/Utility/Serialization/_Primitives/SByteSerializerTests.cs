﻿namespace HQS.Test.Utility.Serialization;

public static class SByteSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { sbyte.MinValue },
				new object[] { sbyte.MaxValue },
				new object[] { -7 },
				new object[] { 7 },
				new object[] { 0 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(sbyte value) => SerDesTests.RunTests(SerDes.OfSByte, value, Length.OfSByte.InBytes);
}
