﻿namespace HQS.Test.Utility.Serialization;

public static class BoolSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { true },
				new object[] { false },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(bool value) => SerDesTests.RunTests(SerDes.OfBool, value, Length.OfBool.InBytes);
}
