﻿namespace HQS.Test.Utility.Serialization;

public static class LongSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { long.MinValue },
				new object[] { long.MaxValue },
				new object[] { -7 },
				new object[] { 7 },
				new object[] { 0 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(long value) => SerDesTests.RunTests(SerDes.OfLong, value, Length.OfLong.InBytes);
}
