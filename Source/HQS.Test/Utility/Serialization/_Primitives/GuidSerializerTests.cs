﻿namespace HQS.Test.Utility.Serialization;

public static class GuidSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { Guid.Empty },
				new object[] { Guid.NewGuid() },
				new object[] { new Guid("a063aa06-13d1-4c37-b5c1-a92fcb10cbeb") },
				new object[] { new Guid("3aeca12f-348f-45a2-92d0-24a7f081fd8d") },
				new object[] { new Guid("fbb3f7d1-926d-4e3f-a434-0399f6aa6124") },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(Guid value) => SerDesTests.RunTests(SerDes.OfGuid, value, 16);
}
