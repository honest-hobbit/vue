﻿namespace HQS.Test.Utility.Serialization;

public static class StringSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { string.Empty },
				new object[] { "a" },
				new object[] { "abc" },
				new object[] { "Hello World" },
		};

	public static int GetLengthInclude(string value, int sizeOfCountInBytes) =>
		(value.Length * Length.OfChar.InBytes) + sizeOfCountInBytes;

	[Theory]
	[MemberData(nameof(Values))]
	public static void LengthAsByteTests(string value) => SerDesTests.RunTests(
		StringSerDes.LengthAsByte, value, GetLengthInclude(value, Length.OfByte.InBytes));

	[Theory]
	[MemberData(nameof(Values))]
	public static void LengthAsUShortTests(string value) => SerDesTests.RunTests(
		StringSerDes.LengthAsUShort, value, GetLengthInclude(value, Length.OfUShort.InBytes));

	[Theory]
	[MemberData(nameof(Values))]
	public static void LengthAsIntTests(string value) => SerDesTests.RunTests(
		StringSerDes.LengthAsInt, value, GetLengthInclude(value, Length.OfInt.InBytes));

	// sizeOfCountInBytes = 1 works here only because the test Value strings are short
	[Theory]
	[MemberData(nameof(Values))]
	public static void LengthAsVarintTests(string value) => SerDesTests.RunTests(
		StringSerDes.LengthAsVarint, value, GetLengthInclude(value, 1));
}
