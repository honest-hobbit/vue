﻿namespace HQS.Test.Utility.Serialization;

public static class ByteSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { byte.MinValue },
				new object[] { byte.MaxValue },
				new object[] { 7 },
				new object[] { 200 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(byte value) => SerDesTests.RunTests(SerDes.OfByte, value, Length.OfByte.InBytes);
}
