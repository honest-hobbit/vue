﻿namespace HQS.Test.Utility.Serialization;

public static class DateTimeSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { DateTime.MinValue },
				new object[] { DateTime.MaxValue },
				new object[] { DateTime.Today },
				new object[] { new DateTime(1234567) },
				new object[] { new DateTime(12345, DateTimeKind.Local) },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(DateTime value) => SerDesTests.RunTests(SerDes.OfDateTime, value, Length.OfLong.InBytes);
}
