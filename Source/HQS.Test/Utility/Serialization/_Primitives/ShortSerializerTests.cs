﻿namespace HQS.Test.Utility.Serialization;

public static class ShortSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { short.MinValue },
				new object[] { short.MaxValue },
				new object[] { -7 },
				new object[] { 7 },
				new object[] { 0 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(short value) => SerDesTests.RunTests(SerDes.OfShort, value, Length.OfShort.InBytes);
}
