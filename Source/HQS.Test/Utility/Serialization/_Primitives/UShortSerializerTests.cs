﻿namespace HQS.Test.Utility.Serialization;

public static class UShortSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { ushort.MinValue },
				new object[] { ushort.MaxValue },
				new object[] { 7 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(ushort value) => SerDesTests.RunTests(SerDes.OfUShort, value, Length.OfUShort.InBytes);
}
