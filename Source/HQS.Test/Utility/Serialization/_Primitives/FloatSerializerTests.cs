﻿namespace HQS.Test.Utility.Serialization;

public static class FloatSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { float.MinValue },
				new object[] { float.MaxValue },
				new object[] { -7.13 },
				new object[] { 7.13 },
				new object[] { 0 },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(float value) => SerDesTests.RunTests(SerDes.OfFloat, value, Length.OfFloat.InBytes);
}
