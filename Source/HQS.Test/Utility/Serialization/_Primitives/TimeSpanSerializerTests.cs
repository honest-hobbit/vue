﻿namespace HQS.Test.Utility.Serialization;

public static class TimeSpanSerializerTests
{
	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
			new object[] { TimeSpan.MinValue },
			new object[] { TimeSpan.MaxValue },
			new object[] { TimeSpan.Zero },
			new object[] { new TimeSpan(123456789) },
			new object[] { new TimeSpan(1, 2, 3) },
			new object[] { new TimeSpan(4, 3, 2, 1) },
			new object[] { new TimeSpan(1, 2, 3, 2, 1) },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(TimeSpan value) => SerDesTests.RunTests(SerDes.OfTimeSpan, value, Length.OfLong.InBytes);
}
