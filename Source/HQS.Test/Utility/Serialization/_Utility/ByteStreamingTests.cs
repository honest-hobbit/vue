﻿namespace HQS.Test.Utility.Serialization;

public static class ByteStreamingTests
{
	public static void SerializeAndDeserialize<T>(ISerDes<T> serializer, IEnumerable<T> values)
	{
		Debug.Assert(serializer != null);
		Debug.Assert(values.AllAndSelfNotNull());

		var initialCapacity = 0;
		using var stream = new MemoryStream(initialCapacity);
		foreach (var value in values)
		{
			serializer.Serialize(value, stream.WriteByte);
		}

		// move back to the start of the stream to start reading from
		stream.Position = 0;

		foreach (var value in values)
		{
			serializer.Deserialize(stream.ReadByteAsByte).Should().Be(value);
		}
	}
}
