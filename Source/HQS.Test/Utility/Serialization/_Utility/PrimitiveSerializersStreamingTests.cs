﻿namespace HQS.Test.Utility.Serialization;

public static class PrimitiveSerializersStreamingTests
{
	[Fact]
	public static void TestBool()
	{
		var values = new bool[] { true, true, false, true, false, false };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfBool, values);
	}

	[Fact]
	public static void TestChar()
	{
		var values = new char[] { 'a', 'b', 'A', 'z', '!', '7', char.MinValue, char.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfChar, values);
	}

	[Fact]
	public static void TestFloat()
	{
		var values = new float[] { 7.13f, 0f, -7.13f, float.MinValue, float.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfFloat, values);
	}

	[Fact]
	public static void TestDouble()
	{
		var values = new double[] { 7.13, 0.0, -7.13, double.MinValue, double.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfDouble, values);
	}

	[Fact]
	public static void TestDecimal()
	{
		var values = new decimal[] { 7.13m, 0m, -7.13m, decimal.MinValue, decimal.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfDecimal, values);
	}

	[Fact]
	public static void TestByte()
	{
		var values = new byte[] { 3, 99, 20, 7, byte.MinValue, byte.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfByte, values);
	}

	[Fact]
	public static void TestSByte()
	{
		var values = new sbyte[] { 7, 0, 20, -5, sbyte.MinValue, sbyte.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfSByte, values);
	}

	[Fact]
	public static void TestShort()
	{
		var values = new short[] { 7, 0, 20, -5, short.MinValue, short.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfShort, values);
	}

	[Fact]
	public static void TestInt()
	{
		var values = new int[] { 7, 0, 20, -5, int.MinValue, int.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfInt, values);
	}

	[Fact]
	public static void TestLong()
	{
		var values = new long[] { 7, 0, 20, -5, long.MinValue, long.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfLong, values);
	}

	[Fact]
	public static void TestUShort()
	{
		var values = new ushort[] { 7, 999, 20, ushort.MinValue, ushort.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfUShort, values);
	}

	[Fact]
	public static void TestUInt()
	{
		var values = new uint[] { 7, 999, 20, uint.MinValue, uint.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfUInt, values);
	}

	[Fact]
	public static void TestULong()
	{
		var values = new ulong[] { 7, 999, 20, ulong.MinValue, ulong.MaxValue };

		ByteStreamingTests.SerializeAndDeserialize(SerDes.OfULong, values);
	}

	[Fact]
	public static void TestString()
	{
		var values = new string[] { string.Empty, "a", "abc", "Hello World!", $"New{Environment.NewLine}Line" };

		ByteStreamingTests.SerializeAndDeserialize(StringSerDes.LengthAsInt, values);
	}
}
