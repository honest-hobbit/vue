﻿namespace HQS.Test.Utility.Serialization;

public static class SignedVarintSerDesTests
{
	public static class OfLong
	{
		public static IEnumerable<object[]> Values() => GetValues();

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(long value, int length) => SerDesTests.RunTests(VarintSerDes.ForSigned, value, length);
	}

	public static class OfInt
	{
		public static IEnumerable<object[]> Values() => GetValues().Where(x => ((long)x[0]).IsIn(int.MinValue, int.MaxValue));

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(int value, int length) => SerDesTests.RunTests(VarintSerDes.ForSigned, value, length);
	}

	public static class OfShort
	{
		public static IEnumerable<object[]> Values() => GetValues().Where(x => ((long)x[0]).IsIn(short.MinValue, short.MaxValue));

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(short value, int length) => SerDesTests.RunTests(VarintSerDes.ForSigned, value, length);
	}

	private static IEnumerable<object[]> GetValues()
	{
		yield return new object[] { -2L, 1 };
		yield return new object[] { -1L, 1 };
		yield return new object[] { 0L, 1 };
		yield return new object[] { 1L, 1 };
		yield return new object[] { 2L, 1 };

		yield return new object[] { long.MinValue, 10 };
		yield return new object[] { long.MinValue + 1, 10 };
		yield return new object[] { long.MaxValue - 1, 10 };
		yield return new object[] { long.MaxValue, 10 };

		yield return new object[] { (long)int.MinValue, 5 };
		yield return new object[] { (long)int.MinValue + 1, 5 };
		yield return new object[] { (long)int.MaxValue - 1, 5 };
		yield return new object[] { (long)int.MaxValue, 5 };

		yield return new object[] { (long)short.MinValue, 3 };
		yield return new object[] { (long)short.MinValue + 1, 3 };
		yield return new object[] { (long)short.MaxValue - 1, 3 };
		yield return new object[] { (long)short.MaxValue, 3 };

		foreach (var (value, length) in GetPositiveValues())
		{
			yield return new object[] { value, length };
			yield return new object[] { -(value + 1), length };
		}
	}

	private static IEnumerable<(long value, int length)> GetPositiveValues()
	{
		yield return (Calculate(1) - 2, 1);
		yield return (Calculate(1) - 1, 1);
		yield return (Calculate(1) + 0, 2);
		yield return (Calculate(1) + 1, 2);
		yield return (Calculate(1) + 2, 2);

		yield return (Calculate(2) - 2, 2);
		yield return (Calculate(2) - 1, 2);
		yield return (Calculate(2) + 0, 3);
		yield return (Calculate(2) + 1, 3);
		yield return (Calculate(2) + 2, 3);

		yield return (Calculate(3) - 2, 3);
		yield return (Calculate(3) - 1, 3);
		yield return (Calculate(3) + 0, 4);
		yield return (Calculate(3) + 1, 4);
		yield return (Calculate(3) + 2, 4);

		yield return (Calculate(4) - 2, 4);
		yield return (Calculate(4) - 1, 4);
		yield return (Calculate(4) + 0, 5);
		yield return (Calculate(4) + 1, 5);
		yield return (Calculate(4) + 2, 5);

		yield return (Calculate(5) - 2, 5);
		yield return (Calculate(5) - 1, 5);
		yield return (Calculate(5) + 0, 6);
		yield return (Calculate(5) + 1, 6);
		yield return (Calculate(5) + 2, 6);

		yield return (Calculate(6) - 2, 6);
		yield return (Calculate(6) - 1, 6);
		yield return (Calculate(6) + 0, 7);
		yield return (Calculate(6) + 1, 7);
		yield return (Calculate(6) + 2, 7);

		yield return (Calculate(7) - 2, 7);
		yield return (Calculate(7) - 1, 7);
		yield return (Calculate(7) + 0, 8);
		yield return (Calculate(7) + 1, 8);
		yield return (Calculate(7) + 2, 8);

		yield return (Calculate(8) - 2, 8);
		yield return (Calculate(8) - 1, 8);
		yield return (Calculate(8) + 0, 9);
		yield return (Calculate(8) + 1, 9);
		yield return (Calculate(8) + 2, 9);

		yield return (Calculate(9) - 2, 9);
		yield return (Calculate(9) - 1, 9);
		yield return (Calculate(9) + 0, 10);
		yield return (Calculate(9) + 1, 10);
		yield return (Calculate(9) + 2, 10);
	}

	private static long Calculate(int bytes) => MathUtility.LongPower(2, (bytes * 7) - 1);
}
