﻿namespace HQS.Test.Utility.Serialization;

public static class UnsignedVarintSerDesTests
{
	public static class OfULong
	{
		public static IEnumerable<object[]> Values() => GetValues();

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(ulong value, int length) => SerDesTests.RunTests(VarintSerDes.ForUnsigned, value, length);
	}

	public static class OfUInt
	{
		public static IEnumerable<object[]> Values() => GetValues().Where(x => (ulong)x[0] <= uint.MaxValue);

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(uint value, int length) => SerDesTests.RunTests(VarintSerDes.ForUnsigned, value, length);
	}

	public static class OfUShort
	{
		public static IEnumerable<object[]> Values() => GetValues().Where(x => (ulong)x[0] <= ushort.MaxValue);

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(ushort value, int length) => SerDesTests.RunTests(VarintSerDes.ForUnsigned, value, length);
	}

	public static class OfLong
	{
		public static IEnumerable<object[]> Values() => GetValues().Where(x => (ulong)x[0] <= long.MaxValue);

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(long value, int length) => SerDesTests.RunTests(VarintSerDes.ForUnsigned, value, length);
	}

	public static class OfInt
	{
		public static IEnumerable<object[]> Values() => GetValues().Where(x => (ulong)x[0] <= int.MaxValue);

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(int value, int length) => SerDesTests.RunTests(VarintSerDes.ForUnsigned, value, length);
	}

	public static class OfShort
	{
		public static IEnumerable<object[]> Values() => GetValues().Where(x => (ulong)x[0] <= (ulong)short.MaxValue);

		[Theory]
		[MemberData(nameof(Values))]
		public static void RunTests(short value, int length) => SerDesTests.RunTests(VarintSerDes.ForUnsigned, value, length);
	}

	private static IEnumerable<object[]> GetValues() =>
		new object[][]
		{
				new object[] { 0UL, 1 },
				new object[] { 1UL, 1 },
				new object[] { 2UL, 1 },

				new object[] { Calculate(1) - 2, 1 },
				new object[] { Calculate(1) - 1, 1 },
				new object[] { Calculate(1) + 0, 2 },
				new object[] { Calculate(1) + 1, 2 },
				new object[] { Calculate(1) + 2, 2 },

				new object[] { Calculate(2) - 2, 2 },
				new object[] { Calculate(2) - 1, 2 },
				new object[] { Calculate(2) + 0, 3 },
				new object[] { Calculate(2) + 1, 3 },
				new object[] { Calculate(2) + 2, 3 },

				new object[] { Calculate(3) - 2, 3 },
				new object[] { Calculate(3) - 1, 3 },
				new object[] { Calculate(3) + 0, 4 },
				new object[] { Calculate(3) + 1, 4 },
				new object[] { Calculate(3) + 2, 4 },

				new object[] { Calculate(4) - 2, 4 },
				new object[] { Calculate(4) - 1, 4 },
				new object[] { Calculate(4) + 0, 5 },
				new object[] { Calculate(4) + 1, 5 },
				new object[] { Calculate(4) + 2, 5 },

				new object[] { Calculate(5) - 2, 5 },
				new object[] { Calculate(5) - 1, 5 },
				new object[] { Calculate(5) + 0, 6 },
				new object[] { Calculate(5) + 1, 6 },
				new object[] { Calculate(5) + 2, 6 },

				new object[] { Calculate(6) - 2, 6 },
				new object[] { Calculate(6) - 1, 6 },
				new object[] { Calculate(6) + 0, 7 },
				new object[] { Calculate(6) + 1, 7 },
				new object[] { Calculate(6) + 2, 7 },

				new object[] { Calculate(7) - 2, 7 },
				new object[] { Calculate(7) - 1, 7 },
				new object[] { Calculate(7) + 0, 8 },
				new object[] { Calculate(7) + 1, 8 },
				new object[] { Calculate(7) + 2, 8 },

				new object[] { Calculate(8) - 2, 8 },
				new object[] { Calculate(8) - 1, 8 },
				new object[] { Calculate(8) + 0, 9 },
				new object[] { Calculate(8) + 1, 9 },
				new object[] { Calculate(8) + 2, 9 },

				new object[] { Calculate(9) - 2, 9 },
				new object[] { Calculate(9) - 1, 9 },
				new object[] { Calculate(9) + 0, 10 },
				new object[] { Calculate(9) + 1, 10 },
				new object[] { Calculate(9) + 2, 10 },

				new object[] { Calculate(10) - 2, 10 },
				new object[] { Calculate(10) - 1, 10 },

				new object[] { ulong.MaxValue - 1, 10 },
				new object[] { ulong.MaxValue, 10 },
				new object[] { (ulong)uint.MaxValue - 1, 5 },
				new object[] { (ulong)uint.MaxValue, 5 },
				new object[] { (ulong)ushort.MaxValue - 1, 3 },
				new object[] { (ulong)ushort.MaxValue, 3 },
				new object[] { (ulong)long.MaxValue - 1, 9 },
				new object[] { (ulong)long.MaxValue, 9 },
				new object[] { (ulong)int.MaxValue - 1, 5 },
				new object[] { (ulong)int.MaxValue, 5 },
				new object[] { (ulong)short.MaxValue - 1, 3 },
				new object[] { (ulong)short.MaxValue, 3 },
		};

	private static ulong Calculate(int bytes) => MathUtility.ULongPower(2, bytes * 7);
}
