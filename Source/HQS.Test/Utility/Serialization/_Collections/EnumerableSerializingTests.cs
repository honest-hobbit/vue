﻿namespace HQS.Test.Utility.Serialization;

public static class EnumerableSerializingTests
{
	public static IEnumerable<int> Empty { get; } = Enumerable.Empty<int>();

	public static IEnumerable<int> SingleValue { get; } = new[] { 13 };

	public static IEnumerable<int> UniqueValues { get; } =
		new[] { 3, 7, 24, 145, 72, 15, 223, 89, 154, 77, 36, 99, 1337, 55, 321, 80, 525, 70, 324, 78, 256, 333, 84, 428, 11, 9 };

	public static IEnumerable<int> RepeatedValues { get; } =
		new[] { 3, 7, 24, 24, 24, 15, 223, 89, 77, 77, 77, 77, 77, 55, 80, 80, 525, 324, 324, 78, 256, 333, 84, 428, 428, 428 };

	public static IEnumerable<int> WorstRunLengthOverhead { get; } =
		new[] { 3, 24, 24, 145, 72, 72, 223, 89, 89, 77, 36, 36, 1337, 55, 55, 80, 525, 525, 324, 78, 78, 333, 84, 84, 11, 9 };

	public static IEnumerable<int> BestRunLengthOverhead { get; } =
		new[] { 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26 };

	public static int RunTests<T>(ISerDes<IEnumerable<T>> subject, IEnumerable<T> values)
	{
		Debug.Assert(subject != null);
		Debug.Assert(values != null);

		var bytes = subject.Serialize(values);
		var result = subject.Deserialize(bytes);

		result.SequenceEqual(values).Should().BeTrue();
		return bytes.Length;
	}
}
