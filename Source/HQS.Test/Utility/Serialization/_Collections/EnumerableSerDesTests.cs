﻿namespace HQS.Test.Utility.Serialization;

public static class EnumerableSerDesTests
{
	private static int MaxRepeatCount => 129;

	private static int MaxUniqueCount => 128;

	private static EnumerableSerDes<int> RunLengthEncoderSubject { get; } = new EnumerableSerDes<int>(
		EnumerableSerDesStrategy.RunLengthEncoded(
			SerDes.OfInt,
			new RunLengthEncoderOptions<int>()
			{
				CountSerDes = SerializeCount.AsInt,
				RunLengthSerDes = SerializeCount.AsSByte,
			}));

	private static EnumerableSerDes<int> SimpleRepeatSubject { get; } = new EnumerableSerDes<int>(
		EnumerableSerDesStrategy.UniqueValues(SerDes.OfInt, SerializeCount.AsInt));

	[Fact]
	[SuppressMessage("Style", "IDE0059", Justification = "Here to compare results manually.")]
	public static void PerformanceTestEncodingStrategies()
	{
		int run1 = EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, EnumerableSerializingTests.Empty);
		int sim1 = EnumerableSerializingTests.RunTests(SimpleRepeatSubject, EnumerableSerializingTests.Empty);

		int run2 = EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, EnumerableSerializingTests.SingleValue);
		int sim2 = EnumerableSerializingTests.RunTests(SimpleRepeatSubject, EnumerableSerializingTests.SingleValue);

		int run3 = EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, EnumerableSerializingTests.UniqueValues);
		int sim3 = EnumerableSerializingTests.RunTests(SimpleRepeatSubject, EnumerableSerializingTests.UniqueValues);

		int run4 = EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, EnumerableSerializingTests.RepeatedValues);
		int sim4 = EnumerableSerializingTests.RunTests(SimpleRepeatSubject, EnumerableSerializingTests.RepeatedValues);

		int run5 = EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, EnumerableSerializingTests.WorstRunLengthOverhead);
		int sim5 = EnumerableSerializingTests.RunTests(SimpleRepeatSubject, EnumerableSerializingTests.WorstRunLengthOverhead);

		int run6 = EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, EnumerableSerializingTests.BestRunLengthOverhead);
		int sim6 = EnumerableSerializingTests.RunTests(SimpleRepeatSubject, EnumerableSerializingTests.BestRunLengthOverhead);
	}

	[Fact]
	public static void RunLengthEncodingTest()
	{
		EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Unique(1000));
		EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Repeat(1000, 350));
		EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Unique(2, 490));
		EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Repeat(2, 964));
		EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Unique(3, 543));
		EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Repeat(3, 570));

		MaxRunLengthSequenceTests(-3, 3);
	}

	private static void MaxRunLengthSequenceTests(int minOffset, int maxOffset)
	{
		Debug.Assert(minOffset <= maxOffset);

		for (int iA = minOffset; iA <= maxOffset; iA++)
		{
			EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Unique(MaxUniqueCount + iA, 661));
			EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Repeat(MaxRepeatCount + iA, 146));

			for (int iB = minOffset; iB <= maxOffset; iB++)
			{
				EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Unique(MaxUniqueCount + iA, 376).Repeat(MaxRepeatCount + iB, 738));
				EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Repeat(MaxRepeatCount + iA, 169).Unique(MaxUniqueCount + iB, 443));

				for (int iC = minOffset; iC <= maxOffset; iC++)
				{
					EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Unique(MaxUniqueCount + iA, 460).Repeat(MaxRepeatCount + iB, 508).Unique(MaxUniqueCount + iC, 467));
					EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Repeat(MaxRepeatCount + iA, 311).Unique(MaxUniqueCount + iB, 135).Repeat(MaxRepeatCount + iC, 313));

					for (int iD = minOffset; iD <= maxOffset; iD++)
					{
						EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Unique(MaxUniqueCount + iA, 710).Repeat(MaxRepeatCount + iB, 73).Unique(MaxUniqueCount + iC, 178).Repeat(MaxRepeatCount + iD, 974));
						EnumerableSerializingTests.RunTests(RunLengthEncoderSubject, Repeat(MaxRepeatCount + iA, 670).Unique(MaxUniqueCount + iB, 881).Repeat(MaxRepeatCount + iC, 377).Unique(MaxUniqueCount + iD, 791));
					}
				}
			}
		}
	}

	private static IEnumerable<int> Unique(int count, int seed = 0) => Enumerable.Range(seed, count);

	private static IEnumerable<int> Unique(this IEnumerable<int> appendTo, int count, int seed = 0) => appendTo.Concat(Unique(count, seed));

	private static IEnumerable<int> Repeat(int count, int value) => Enumerable.Range(0, count).Select(x => value);

	private static IEnumerable<int> Repeat(this IEnumerable<int> appendTo, int count, int value) => appendTo.Concat(Repeat(count, value));
}
