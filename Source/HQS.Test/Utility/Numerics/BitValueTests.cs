﻿namespace HQS.Test.Utility.Numerics;

public static class BitValueTests
{
	public static class Bits8
	{
		private static Bit8 FalseSubject => Bit8.AllFalse;

		[Fact]
		public static void StartsAllTrue() => TestStartsAllAs(() => Bit8.AllTrue, true);

		[Fact]
		public static void StartsAllFalse() => TestStartsAllAs(() => FalseSubject, false);

		[Fact]
		public static void IndexerSetTrue() => TestIndexerSetTrue(() => FalseSubject);

		[Fact]
		public static void IndexerSetFalse() => TestIndexerSetFalse(() => FalseSubject);

		[Fact]
		public static void SetAllTrue() => TestSetAllTrue(() => FalseSubject);

		[Fact]
		public static void SetAllFalse() => TestSetAllFalse(() => FalseSubject);

		[Fact]
		public static void ToStringIsCorrect() => TestToString(() => FalseSubject);

		[Fact]
		public static void EnumerationMatchesIndexer() => TestEnumerator(() => FalseSubject);
	}

	public static class Bits16
	{
		private static Bit16 FalseSubject => Bit16.AllFalse;

		[Fact]
		public static void StartsAllTrue() => TestStartsAllAs(() => Bit16.AllTrue, true);

		[Fact]
		public static void StartsAllFalse() => TestStartsAllAs(() => FalseSubject, false);

		[Fact]
		public static void IndexerSetTrue() => TestIndexerSetTrue(() => FalseSubject);

		[Fact]
		public static void IndexerSetFalse() => TestIndexerSetFalse(() => FalseSubject);

		[Fact]
		public static void SetAllTrue() => TestSetAllTrue(() => FalseSubject);

		[Fact]
		public static void SetAllFalse() => TestSetAllFalse(() => FalseSubject);

		[Fact]
		public static void ToStringIsCorrect() => TestToString(() => FalseSubject);

		[Fact]
		public static void EnumerationMatchesIndexer() => TestEnumerator(() => FalseSubject);
	}

	public static class Bits32
	{
		private static Bit32 FalseSubject => Bit32.AllFalse;

		[Fact]
		public static void StartsAllTrue() => TestStartsAllAs(() => Bit32.AllTrue, true);

		[Fact]
		public static void StartsAllFalse() => TestStartsAllAs(() => FalseSubject, false);

		[Fact]
		public static void IndexerSetTrue() => TestIndexerSetTrue(() => FalseSubject);

		[Fact]
		public static void IndexerSetFalse() => TestIndexerSetFalse(() => FalseSubject);

		[Fact]
		public static void SetAllTrue() => TestSetAllTrue(() => FalseSubject);

		[Fact]
		public static void SetAllFalse() => TestSetAllFalse(() => FalseSubject);

		[Fact]
		public static void ToStringIsCorrect() => TestToString(() => FalseSubject);

		[Fact]
		public static void EnumerationMatchesIndexer() => TestEnumerator(() => FalseSubject);
	}

	public static class Bits64
	{
		private static Bit64 FalseSubject => Bit64.AllFalse;

		[Fact]
		public static void StartsAllTrue() => TestStartsAllAs(() => Bit64.AllTrue, true);

		[Fact]
		public static void StartsAllFalse() => TestStartsAllAs(() => FalseSubject, false);

		[Fact]
		public static void IndexerSetTrue() => TestIndexerSetTrue(() => FalseSubject);

		[Fact]
		public static void IndexerSetFalse() => TestIndexerSetFalse(() => FalseSubject);

		[Fact]
		public static void SetAllTrue() => TestSetAllTrue(() => FalseSubject);

		[Fact]
		public static void SetAllFalse() => TestSetAllFalse(() => FalseSubject);

		[Fact]
		public static void ToStringIsCorrect() => TestToString(() => FalseSubject);

		[Fact]
		public static void EnumerationMatchesIndexer() => TestEnumerator(() => FalseSubject);
	}

	public static class Bits128
	{
		private static Bit128 FalseSubject => Bit128.AllFalse;

		[Fact]
		public static void StartsAllTrue() => TestStartsAllAs(() => Bit128.AllTrue, true);

		[Fact]
		public static void StartsAllFalse() => TestStartsAllAs(() => FalseSubject, false);

		[Fact]
		public static void IndexerSetTrue() => TestIndexerSetTrue(() => FalseSubject);

		[Fact]
		public static void IndexerSetFalse() => TestIndexerSetFalse(() => FalseSubject);

		[Fact]
		public static void SetAllTrue() => TestSetAllTrue(() => FalseSubject);

		[Fact]
		public static void SetAllFalse() => TestSetAllFalse(() => FalseSubject);

		[Fact]
		public static void ToStringIsCorrect() => TestToString(() => FalseSubject);

		[Fact]
		public static void EnumerationMatchesIndexer() => TestEnumerator(() => FalseSubject);
	}

	public static class Bits256
	{
		private static Bit256 FalseSubject => Bit256.AllFalse;

		[Fact]
		public static void StartsAllTrue() => TestStartsAllAs(() => Bit256.AllTrue, true);

		[Fact]
		public static void StartsAllFalse() => TestStartsAllAs(() => FalseSubject, false);

		[Fact]
		public static void IndexerSetTrue() => TestIndexerSetTrue(() => FalseSubject);

		[Fact]
		public static void IndexerSetFalse() => TestIndexerSetFalse(() => FalseSubject);

		[Fact]
		public static void SetAllTrue() => TestSetAllTrue(() => FalseSubject);

		[Fact]
		public static void SetAllFalse() => TestSetAllFalse(() => FalseSubject);

		[Fact]
		public static void ToStringIsCorrect() => TestToString(() => FalseSubject);

		[Fact]
		public static void EnumerationMatchesIndexer() => TestEnumerator(() => FalseSubject);
	}

	private static void TestStartsAllAs<T>(Func<T> getSubject, bool value)
	where T : IBitValue, IEquatable<T>
	{
		int max = getSubject().Count;
		var subject = getSubject();

		for (int index = 0; index < max; index++)
		{
			subject[index].Should().Be(value);
		}
	}

	private static void TestIndexerSetTrue<T>(Func<T> getSubject)
		where T : IBitValue, IEquatable<T>
	{
		int max = getSubject().Count;
		for (int iteration = 0; iteration < max; iteration++)
		{
			var subject = getSubject();
			subject[iteration] = true;

			for (int index = 0; index < max; index++)
			{
				subject[index].Should().Be(index == iteration);
			}
		}
	}

	private static void TestIndexerSetFalse<T>(Func<T> getSubject)
		where T : IBitValue, IEquatable<T>
	{
		int max = getSubject().Count;
		for (int iteration = 0; iteration < max; iteration++)
		{
			var subject = getSubject();
			for (int index = 0; index < max; index++)
			{
				subject[index] = true;
			}

			subject[iteration] = false;

			for (int index = 0; index < max; index++)
			{
				subject[index].Should().Be(index != iteration);
			}
		}
	}

	private static void TestSetAllTrue<T>(Func<T> getSubject)
		where T : IBitValue, IEquatable<T>
	{
		int max = getSubject().Count;
		var subject = getSubject();

		for (int index = 0; index < max; index++)
		{
			subject[index] = true;
		}

		for (int index = 0; index < max; index++)
		{
			subject[index].Should().BeTrue();
		}
	}

	private static void TestSetAllFalse<T>(Func<T> getSubject)
		where T : IBitValue, IEquatable<T>
	{
		int max = getSubject().Count;
		var subject = getSubject();

		for (int index = 0; index < max; index++)
		{
			subject[index] = true;
		}

		for (int index = 0; index < max; index++)
		{
			subject[index].Should().BeTrue();
		}

		for (int index = 0; index < max; index++)
		{
			subject[index] = false;
		}

		for (int index = 0; index < max; index++)
		{
			subject[index].Should().BeFalse();
		}
	}

	private static void TestToString<T>(Func<T> getSubject)
		where T : IBitValue, IEquatable<T>
	{
		int max = getSubject().Count;
		for (int iteration = 0; iteration < max; iteration++)
		{
			var subject = getSubject();
			subject[iteration] = true;
			var toString = subject.ToString();

			toString[0].Should().Be('[');
			toString[^1].Should().Be(']');

			// this part of the string should contain only the sequence of 0s and 1s
			for (int index = 0; index < max; index++)
			{
				toString[1 + index].Should().Be(max - 1 - index == iteration ? '1' : '0');
			}
		}
	}

	private static void TestEnumerator<T>(Func<T> getSubject)
		where T : IBitValue, IEquatable<T>
	{
		int max = getSubject().Count;
		for (int iteration = 0; iteration < max; iteration++)
		{
			var subject = getSubject();
			subject[iteration] = true;

			int i = 0;
			foreach (var bit in subject)
			{
				bit.Should().Be(subject[i]);
				i++;
			}
		}
	}
}
