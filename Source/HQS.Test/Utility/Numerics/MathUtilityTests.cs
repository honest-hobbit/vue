﻿namespace HQS.Test.Utility.Numerics;

public static class MathUtilityTests
{
	[Theory]
	[InlineData(-1, 1, true, 0)]
	[InlineData(-1, 1, false, 0)]
	[InlineData(1, 3, true, 2)]
	[InlineData(1, 3, false, 2)]
	[InlineData(-1, -3, true, -2)]
	[InlineData(-1, -3, false, -2)]
	[InlineData(2, 2, true, 2)]
	[InlineData(2, 2, false, 2)]
	[InlineData(-2, -2, true, -2)]
	[InlineData(-2, -2, false, -2)]
	[InlineData(0, 0, true, 0)]
	[InlineData(0, 0, false, 0)]
	[InlineData(-1, 2, true, 1)]
	[InlineData(-1, 2, false, 0)]
	[InlineData(-2, 1, true, 0)]
	[InlineData(-2, 1, false, -1)]
	[InlineData(1, 2, true, 2)]
	[InlineData(1, 2, false, 1)]
	[InlineData(1, 4, true, 3)]
	[InlineData(1, 4, false, 2)]
	[InlineData(-1, -2, true, -1)]
	[InlineData(-1, -2, false, -2)]
	[InlineData(-1, -4, true, -2)]
	[InlineData(-1, -4, false, -3)]
	public static void IntegerMidpoint(int value1, int value2, bool roundUp, int expectedResult)
	{
		var result = MathUtility.IntegerMidpoint(value1, value2, roundUp);
		result.Should().Be(expectedResult);
	}

	// tests adapted to C# taken from here
	// https://floating-point-gui.de/errors/NearlyEqualsTest.java
	public static class AreSimiliarFloats
	{
		// Regular large numbers - generally not problematic
		[Fact]
		public static void Big()
		{
			MathUtility.AreSimiliar(1000000f, 1000001f).Should().BeTrue();
			MathUtility.AreSimiliar(1000001f, 1000000f).Should().BeTrue();
			MathUtility.AreSimiliar(10000f, 10001f).Should().BeFalse();
			MathUtility.AreSimiliar(10001f, 10000f).Should().BeFalse();
		}

		// Negative large numbers
		[Fact]
		public static void BigNeg()
		{
			MathUtility.AreSimiliar(-1000000f, -1000001f).Should().BeTrue();
			MathUtility.AreSimiliar(-1000001f, -1000000f).Should().BeTrue();
			MathUtility.AreSimiliar(-10000f, -10001f).Should().BeFalse();
			MathUtility.AreSimiliar(-10001f, -10000f).Should().BeFalse();
		}

		// Numbers around 1
		[Fact]
		public static void Mid()
		{
			MathUtility.AreSimiliar(1.0000001f, 1.0000002f).Should().BeTrue();
			MathUtility.AreSimiliar(1.0000002f, 1.0000001f).Should().BeTrue();
			MathUtility.AreSimiliar(1.0002f, 1.0001f).Should().BeFalse();
			MathUtility.AreSimiliar(1.0001f, 1.0002f).Should().BeFalse();
		}

		// Numbers around -1
		[Fact]
		public static void MidNeg()
		{
			MathUtility.AreSimiliar(-1.000001f, -1.000002f).Should().BeTrue();
			MathUtility.AreSimiliar(-1.000002f, -1.000001f).Should().BeTrue();
			MathUtility.AreSimiliar(-1.0001f, -1.0002f).Should().BeFalse();
			MathUtility.AreSimiliar(-1.0002f, -1.0001f).Should().BeFalse();
		}

		// Numbers between 1 and 0
		[Fact]
		public static void Small()
		{
			MathUtility.AreSimiliar(0.000000001000001f, 0.000000001000002f).Should().BeTrue();
			MathUtility.AreSimiliar(0.000000001000002f, 0.000000001000001f).Should().BeTrue();
			MathUtility.AreSimiliar(0.000000000001002f, 0.000000000001001f).Should().BeFalse();
			MathUtility.AreSimiliar(0.000000000001001f, 0.000000000001002f).Should().BeFalse();
		}

		// Numbers between -1 and 0
		[Fact]
		public static void SmallNeg()
		{
			MathUtility.AreSimiliar(-0.000000001000001f, -0.000000001000002f).Should().BeTrue();
			MathUtility.AreSimiliar(-0.000000001000002f, -0.000000001000001f).Should().BeTrue();
			MathUtility.AreSimiliar(-0.000000000001002f, -0.000000000001001f).Should().BeFalse();
			MathUtility.AreSimiliar(-0.000000000001001f, -0.000000000001002f).Should().BeFalse();
		}

		// Small differences away from zero
		[Fact]
		public static void SmallDiffs()
		{
			MathUtility.AreSimiliar(0.3f, 0.30000003f).Should().BeTrue();
			MathUtility.AreSimiliar(-0.3f, -0.30000003f).Should().BeTrue();
		}

		// Comparisons involving zero
		[Fact]
		public static void Zero()
		{
			MathUtility.AreSimiliar(0.0f, 0.0f).Should().BeTrue();
			MathUtility.AreSimiliar(0.0f, -0.0f).Should().BeTrue();
			MathUtility.AreSimiliar(-0.0f, -0.0f).Should().BeTrue();

			MathUtility.AreSimiliar(0.00000001f, 0.0f).Should().BeFalse();
			MathUtility.AreSimiliar(0.0f, 0.00000001f).Should().BeFalse();
			MathUtility.AreSimiliar(-0.00000001f, 0.0f).Should().BeFalse();
			MathUtility.AreSimiliar(0.0f, -0.00000001f).Should().BeFalse();

			MathUtility.AreSimiliar(0.0f, 1e-40f, .01f).Should().BeTrue();
			MathUtility.AreSimiliar(1e-40f, 0.0f, .01f).Should().BeTrue();
			MathUtility.AreSimiliar(1e-40f, 0.0f).Should().BeFalse();
			MathUtility.AreSimiliar(0.0f, 1e-40f).Should().BeFalse();

			MathUtility.AreSimiliar(0.0f, -1e-40f, .01f).Should().BeTrue();
			MathUtility.AreSimiliar(-1e-40f, 0.0f, .01f).Should().BeTrue();
			MathUtility.AreSimiliar(-1e-40f, 0.0f).Should().BeFalse();
			MathUtility.AreSimiliar(0.0f, -1e-40f).Should().BeFalse();
		}

		// Comparisons involving extreme values (overflow potential)
		[Fact]
		public static void ExtremeMax()
		{
			MathUtility.AreSimiliar(float.MaxValue, float.MaxValue).Should().BeTrue();
			MathUtility.AreSimiliar(float.MaxValue, -float.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(-float.MaxValue, float.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(float.MaxValue, float.MaxValue / 2).Should().BeFalse();
			MathUtility.AreSimiliar(float.MaxValue, -float.MaxValue / 2).Should().BeFalse();
			MathUtility.AreSimiliar(-float.MaxValue, float.MaxValue / 2).Should().BeFalse();
		}

		// Comparisons involving infinities
		[Fact]
		public static void Infinities()
		{
			MathUtility.AreSimiliar(float.PositiveInfinity, float.PositiveInfinity).Should().BeTrue();
			MathUtility.AreSimiliar(float.NegativeInfinity, float.NegativeInfinity).Should().BeTrue();

			MathUtility.AreSimiliar(float.NegativeInfinity, float.PositiveInfinity).Should().BeFalse();

			MathUtility.AreSimiliar(float.PositiveInfinity, -float.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(float.NegativeInfinity, float.MaxValue).Should().BeFalse();

			MathUtility.AreSimiliar(float.PositiveInfinity, float.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(float.NegativeInfinity, -float.MaxValue).Should().BeFalse();
		}

		// Comparisons involving NaN values
		[Fact]
		public static void NaN()
		{
			MathUtility.AreSimiliar(float.NaN, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, 0.0f).Should().BeFalse();
			MathUtility.AreSimiliar(-0.0f, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, -0.0f).Should().BeFalse();
			MathUtility.AreSimiliar(0.0f, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, float.PositiveInfinity).Should().BeFalse();
			MathUtility.AreSimiliar(float.PositiveInfinity, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, float.NegativeInfinity).Should().BeFalse();
			MathUtility.AreSimiliar(float.NegativeInfinity, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, float.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(float.MaxValue, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, -float.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(-float.MaxValue, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, float.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(float.Epsilon, float.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(float.NaN, -float.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(-float.Epsilon, float.NaN).Should().BeFalse();
		}

		// Comparisons of numbers on opposite sides of 0
		[Fact]
		public static void Opposite()
		{
			MathUtility.AreSimiliar(1.000000001f, -1.0f).Should().BeFalse();
			MathUtility.AreSimiliar(-1.0f, 1.000000001f).Should().BeFalse();
			MathUtility.AreSimiliar(-1.000000001f, 1.0f).Should().BeFalse();
			MathUtility.AreSimiliar(1.0f, -1.000000001f).Should().BeFalse();

			MathUtility.AreSimiliar(10 * float.Epsilon, 10 * -float.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(10000 * float.Epsilon, 10000 * -float.Epsilon).Should().BeFalse();
		}

		// The really tricky part - comparisons of numbers very close to zero.
		[Fact]
		public static void Epsilon()
		{
			MathUtility.AreSimiliar(float.Epsilon, float.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(float.Epsilon, -float.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(-float.Epsilon, float.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(float.Epsilon, 0).Should().BeTrue();
			MathUtility.AreSimiliar(0, float.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(-float.Epsilon, 0).Should().BeTrue();
			MathUtility.AreSimiliar(0, -float.Epsilon).Should().BeTrue();

			MathUtility.AreSimiliar(0.000000001f, -float.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(0.000000001f, float.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(float.Epsilon, 0.000000001f).Should().BeFalse();
			MathUtility.AreSimiliar(-float.Epsilon, 0.000000001f).Should().BeFalse();
		}
	}

	// tests adapted to C# taken from here
	// https://floating-point-gui.de/errors/NearlyEqualsTest.java
	public static class AreSimiliarDoubles
	{
		// Regular large numbers - generally not problematic
		[Fact]
		public static void Big()
		{
			MathUtility.AreSimiliar(1000000d, 1000001d).Should().BeTrue();
			MathUtility.AreSimiliar(1000001d, 1000000d).Should().BeTrue();
			MathUtility.AreSimiliar(10000d, 10001d).Should().BeFalse();
			MathUtility.AreSimiliar(10001d, 10000d).Should().BeFalse();
		}

		// Negative large numbers
		[Fact]
		public static void BigNeg()
		{
			MathUtility.AreSimiliar(-1000000d, -1000001d).Should().BeTrue();
			MathUtility.AreSimiliar(-1000001d, -1000000d).Should().BeTrue();
			MathUtility.AreSimiliar(-10000d, -10001d).Should().BeFalse();
			MathUtility.AreSimiliar(-10001d, -10000d).Should().BeFalse();
		}

		// Numbers around 1
		[Fact]
		public static void Mid()
		{
			MathUtility.AreSimiliar(1.0000001d, 1.0000002d).Should().BeTrue();
			MathUtility.AreSimiliar(1.0000002d, 1.0000001d).Should().BeTrue();
			MathUtility.AreSimiliar(1.0002d, 1.0001d).Should().BeFalse();
			MathUtility.AreSimiliar(1.0001d, 1.0002d).Should().BeFalse();
		}

		// Numbers around -1
		[Fact]
		public static void MidNeg()
		{
			MathUtility.AreSimiliar(-1.000001d, -1.000002d).Should().BeTrue();
			MathUtility.AreSimiliar(-1.000002d, -1.000001d).Should().BeTrue();
			MathUtility.AreSimiliar(-1.0001d, -1.0002d).Should().BeFalse();
			MathUtility.AreSimiliar(-1.0002d, -1.0001d).Should().BeFalse();
		}

		// Numbers between 1 and 0
		[Fact]
		public static void Small()
		{
			MathUtility.AreSimiliar(0.000000001000001d, 0.000000001000002d).Should().BeTrue();
			MathUtility.AreSimiliar(0.000000001000002d, 0.000000001000001d).Should().BeTrue();
			MathUtility.AreSimiliar(0.000000000001002d, 0.000000000001001d).Should().BeFalse();
			MathUtility.AreSimiliar(0.000000000001001d, 0.000000000001002d).Should().BeFalse();
		}

		// Numbers between -1 and 0
		[Fact]
		public static void SmallNeg()
		{
			MathUtility.AreSimiliar(-0.000000001000001d, -0.000000001000002d).Should().BeTrue();
			MathUtility.AreSimiliar(-0.000000001000002d, -0.000000001000001d).Should().BeTrue();
			MathUtility.AreSimiliar(-0.000000000001002d, -0.000000000001001d).Should().BeFalse();
			MathUtility.AreSimiliar(-0.000000000001001d, -0.000000000001002d).Should().BeFalse();
		}

		// Small differences away from zero
		[Fact]
		public static void SmallDiffs()
		{
			MathUtility.AreSimiliar(0.3d, 0.30000003d).Should().BeTrue();
			MathUtility.AreSimiliar(-0.3d, -0.30000003d).Should().BeTrue();
		}

		// Comparisons involving zero
		[Fact]
		public static void Zero()
		{
			MathUtility.AreSimiliar(0.0d, 0.0d).Should().BeTrue();
			MathUtility.AreSimiliar(0.0d, -0.0d).Should().BeTrue();
			MathUtility.AreSimiliar(-0.0d, -0.0d).Should().BeTrue();

			MathUtility.AreSimiliar(0.00000001d, 0.0d).Should().BeFalse();
			MathUtility.AreSimiliar(0.0d, 0.00000001d).Should().BeFalse();
			MathUtility.AreSimiliar(-0.00000001d, 0.0d).Should().BeFalse();
			MathUtility.AreSimiliar(0.0d, -0.00000001d).Should().BeFalse();

			MathUtility.AreSimiliar(0.0d, 1e-310d, .01d).Should().BeTrue();
			MathUtility.AreSimiliar(1e-310d, 0.0d, .01d).Should().BeTrue();
			MathUtility.AreSimiliar(1e-310d, 0.0d).Should().BeFalse();
			MathUtility.AreSimiliar(0.0d, 1e-310d).Should().BeFalse();

			MathUtility.AreSimiliar(0.0d, -1e-310d, .01d).Should().BeTrue();
			MathUtility.AreSimiliar(-1e-310d, 0.0d, .01d).Should().BeTrue();
			MathUtility.AreSimiliar(-1e-310d, 0.0d).Should().BeFalse();
			MathUtility.AreSimiliar(0.0d, -1e-310d).Should().BeFalse();
		}

		// Comparisons involving extreme values (overflow potential)
		[Fact]
		public static void ExtremeMax()
		{
			MathUtility.AreSimiliar(double.MaxValue, double.MaxValue).Should().BeTrue();
			MathUtility.AreSimiliar(double.MaxValue, -double.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(-double.MaxValue, double.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(double.MaxValue, double.MaxValue / 2).Should().BeFalse();
			MathUtility.AreSimiliar(double.MaxValue, -double.MaxValue / 2).Should().BeFalse();
			MathUtility.AreSimiliar(-double.MaxValue, double.MaxValue / 2).Should().BeFalse();
		}

		// Comparisons involving infinities
		[Fact]
		public static void Infinities()
		{
			MathUtility.AreSimiliar(double.PositiveInfinity, double.PositiveInfinity).Should().BeTrue();
			MathUtility.AreSimiliar(double.NegativeInfinity, double.NegativeInfinity).Should().BeTrue();

			MathUtility.AreSimiliar(double.NegativeInfinity, double.PositiveInfinity).Should().BeFalse();

			MathUtility.AreSimiliar(double.PositiveInfinity, -double.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(double.NegativeInfinity, double.MaxValue).Should().BeFalse();

			MathUtility.AreSimiliar(double.PositiveInfinity, double.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(double.NegativeInfinity, -double.MaxValue).Should().BeFalse();
		}

		// Comparisons involving NaN values
		[Fact]
		public static void NaN()
		{
			MathUtility.AreSimiliar(double.NaN, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, 0.0d).Should().BeFalse();
			MathUtility.AreSimiliar(-0.0d, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, -0.0d).Should().BeFalse();
			MathUtility.AreSimiliar(0.0d, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, double.PositiveInfinity).Should().BeFalse();
			MathUtility.AreSimiliar(double.PositiveInfinity, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, double.NegativeInfinity).Should().BeFalse();
			MathUtility.AreSimiliar(double.NegativeInfinity, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, double.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(double.MaxValue, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, -double.MaxValue).Should().BeFalse();
			MathUtility.AreSimiliar(-double.MaxValue, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, double.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(double.Epsilon, double.NaN).Should().BeFalse();
			MathUtility.AreSimiliar(double.NaN, -double.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(-double.Epsilon, double.NaN).Should().BeFalse();
		}

		// Comparisons of numbers on opposite sides of 0
		[Fact]
		public static void Opposite()
		{
			MathUtility.AreSimiliar(1.000000001d, -1.0d).Should().BeFalse();
			MathUtility.AreSimiliar(-1.0d, 1.000000001d).Should().BeFalse();
			MathUtility.AreSimiliar(-1.000000001d, 1.0d).Should().BeFalse();
			MathUtility.AreSimiliar(1.0d, -1.000000001d).Should().BeFalse();

			MathUtility.AreSimiliar(10 * double.Epsilon, 10 * -double.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(1e12 * double.Epsilon, 1e12 * -double.Epsilon).Should().BeFalse();
		}

		// The really tricky part - comparisons of numbers very close to zero.
		[Fact]
		public static void Epsilon()
		{
			MathUtility.AreSimiliar(double.Epsilon, double.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(double.Epsilon, -double.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(-double.Epsilon, double.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(double.Epsilon, 0).Should().BeTrue();
			MathUtility.AreSimiliar(0, double.Epsilon).Should().BeTrue();
			MathUtility.AreSimiliar(-double.Epsilon, 0).Should().BeTrue();
			MathUtility.AreSimiliar(0, -double.Epsilon).Should().BeTrue();

			MathUtility.AreSimiliar(0.000000001d, -double.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(0.000000001d, double.Epsilon).Should().BeFalse();
			MathUtility.AreSimiliar(double.Epsilon, 0.000000001d).Should().BeFalse();
			MathUtility.AreSimiliar(-double.Epsilon, 0.000000001d).Should().BeFalse();
		}
	}
}
