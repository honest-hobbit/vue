﻿namespace HQS.Test.Utility.Numerics;

public static class HexUtilityTests
{
	[Theory]
	[InlineData(0, '0')]
	[InlineData(1, '1')]
	[InlineData(2, '2')]
	[InlineData(3, '3')]
	[InlineData(4, '4')]
	[InlineData(5, '5')]
	[InlineData(6, '6')]
	[InlineData(7, '7')]
	[InlineData(8, '8')]
	[InlineData(9, '9')]
	[InlineData(10, 'A')]
	[InlineData(11, 'B')]
	[InlineData(12, 'C')]
	[InlineData(13, 'D')]
	[InlineData(14, 'E')]
	[InlineData(15, 'F')]
	public static void ToCharSingle(byte value, char expectedResult)
	{
		var encoded = HexUtility.ToCharSingle(value);
		var decoded = HexUtility.ToByte(encoded);

		encoded.Should().Be(expectedResult);
		decoded.Should().Be(value);
	}

	[Fact]
	public static void ToChar()
	{
		Span<char> encoded = stackalloc char[2];
		Span<byte> bytes = stackalloc byte[1];

		for (int i = 0; i <= byte.MaxValue; i++)
		{
			encoded.Clear();
			bytes.Clear();

			var value = (byte)i;

			HexUtility.ToChar(value, encoded);
			var decoded = HexUtility.ToByte(encoded);

			bytes[0] = value;
			var expectedResult = Convert.ToHexString(bytes);

			new string(encoded).Should().Be(expectedResult);
			decoded.Should().Be(value);
		}
	}

	[Fact]
	public static void ToChars()
	{
		Span<int> intSpan = stackalloc int[1];
		Span<char> encodedChars = stackalloc char[8];
		Span<byte> decodedBytes = stackalloc byte[4];

		for (int i = 0; i <= 100000; i++)
		{
			intSpan[0] = i;
			encodedChars.Clear();
			decodedBytes.Clear();

			var sourceBytes = MemoryMarshal.AsBytes(intSpan);

			HexUtility.ToChars(sourceBytes, encodedChars);
			HexUtility.ToBytes(encodedChars, decodedBytes);

			var expectedResult = Convert.ToHexString(sourceBytes);

			new string(encodedChars).Should().Be(expectedResult);
			decodedBytes.SequenceEqual(sourceBytes);
		}
	}
}
