﻿namespace HQS.Test.Utility.Numerics;

public static class ForLoopIterator2Tests
{
	[Fact]
	public static void NothingCalledWhenLoopNotCalled()
	{
		var subject = new SubjectIterator();

		NoneAndSingleShouldNotBeCalled(subject);
		LineHorizontalShouldNotBeCalled(subject);
		LineVerticalShouldNotBeCalled(subject);
		CornersEdgesAndInsideShouldNotBeCalled(subject);
	}

	[Theory]
	[InlineData(0, 0)]
	[InlineData(2, 0)]
	[InlineData(0, 3)]
	public static void None(int dimensionX, int dimensionY)
	{
		Debug.Assert(dimensionX == 0 || dimensionY == 0);

		foreach (var start in EnumerateStartIndices())
		{
			var subject = new SubjectIterator();
			subject.InvokeLoop(start, new Int2(dimensionX, dimensionY));

			subject.NoneCalled.Value.Should().BeTrue();

			subject.CheckEveryCellAccessedOnce();

			subject.SingleIndex.HasValue.Should().BeFalse();
			LineHorizontalShouldNotBeCalled(subject);
			LineVerticalShouldNotBeCalled(subject);
			CornersEdgesAndInsideShouldNotBeCalled(subject);
		}
	}

	[Fact]
	public static void Single()
	{
		foreach (var start in EnumerateStartIndices())
		{
			var subject = new SubjectIterator();
			subject.InvokeLoop(start, new Int2(1, 1));

			subject.SingleIndex.Value.Should().Be(start);

			subject.CheckEveryCellAccessedOnce();

			subject.NoneCalled.HasValue.Should().BeFalse();
			LineHorizontalShouldNotBeCalled(subject);
			LineVerticalShouldNotBeCalled(subject);
			CornersEdgesAndInsideShouldNotBeCalled(subject);
		}
	}

	[Theory]
	[InlineData(2)]
	[InlineData(3)]
	[InlineData(4)]
	[InlineData(5)]
	public static void LineHorizontal(int length)
	{
		foreach (var (x, y) in EnumerateStartIndices())
		{
			var subject = new SubjectIterator();
			subject.InvokeLoop(new Int2(x, y), new Int2(length, 1));

			subject.LineHorizontalStartIndex.Value.Should().Be(new Int2(x, y));
			subject.LineHorizontalEndIndex.Value.Should().Be(new Int2(x + length - 1, y));
			subject.LineHorizontalArgs.Value.Should().Be((new Int2(x + 1, y), length - 2));

			subject.CheckEveryCellAccessedOnce();

			NoneAndSingleShouldNotBeCalled(subject);
			LineVerticalShouldNotBeCalled(subject);
			CornersEdgesAndInsideShouldNotBeCalled(subject);
		}
	}

	[Theory]
	[InlineData(2)]
	[InlineData(3)]
	[InlineData(4)]
	[InlineData(5)]
	public static void LineVertical(int length)
	{
		foreach (var (x, y) in EnumerateStartIndices())
		{
			var subject = new SubjectIterator();
			subject.InvokeLoop(new Int2(x, y), new Int2(1, length));

			subject.LineVerticalStartIndex.Value.Should().Be(new Int2(x, y));
			subject.LineVerticalEndIndex.Value.Should().Be(new Int2(x, y + length - 1));
			subject.LineVerticalArgs.Value.Should().Be((new Int2(x, y + 1), length - 2));

			subject.CheckEveryCellAccessedOnce();

			NoneAndSingleShouldNotBeCalled(subject);
			LineHorizontalShouldNotBeCalled(subject);
			CornersEdgesAndInsideShouldNotBeCalled(subject);
		}
	}

	[Theory]
	[InlineData(2, 2)]
	[InlineData(3, 3)]
	[InlineData(4, 4)]
	[InlineData(5, 5)]
	[InlineData(2, 3)]
	[InlineData(2, 4)]
	[InlineData(2, 5)]
	[InlineData(3, 2)]
	[InlineData(4, 2)]
	[InlineData(5, 2)]
	[InlineData(5, 8)]
	[InlineData(9, 3)]
	public static void CornersEdgesAndInside(int dimensionX, int dimensionY)
	{
		foreach (var (x, y) in EnumerateStartIndices())
		{
			var subject = new SubjectIterator();
			subject.InvokeLoop(new Int2(x, y), new Int2(dimensionX, dimensionY));

			subject.CornerLowXLowYIndex.Value.Should().Be(new Int2(x, y));
			subject.CornerLowXHighYIndex.Value.Should().Be(new Int2(x, y + dimensionY - 1));
			subject.CornerHighXLowYIndex.Value.Should().Be(new Int2(x + dimensionX - 1, y));
			subject.CornerHighXHighYIndex.Value.Should().Be(new Int2(x + dimensionX - 1, y + dimensionY - 1));

			subject.EdgeLowXArgs.Value.Should().Be((new Int2(x, y + 1), dimensionY - 2));
			subject.EdgeHighXArgs.Value.Should().Be((new Int2(x + dimensionX - 1, y + 1), dimensionY - 2));
			subject.EdgeLowYArgs.Value.Should().Be((new Int2(x + 1, y), dimensionX - 2));
			subject.EdgeHighYArgs.Value.Should().Be((new Int2(x + 1, y + dimensionY - 1), dimensionX - 2));

			subject.InsideArgs.Value.Should().Be((new Int2(x + 1, y + 1), new Int2(dimensionX - 2, dimensionY - 2)));

			subject.CheckEveryCellAccessedOnce();

			NoneAndSingleShouldNotBeCalled(subject);
			LineHorizontalShouldNotBeCalled(subject);
			LineVerticalShouldNotBeCalled(subject);
		}
	}

	private static IEnumerable<Int2> EnumerateStartIndices()
	{
		yield return new Int2(0, 0);
		yield return new Int2(1, 1);
		yield return new Int2(3, 8);
		yield return new Int2(-7, 4);
		yield return new Int2(5, -2);
		yield return new Int2(-4, -9);
	}

	private static void NoneAndSingleShouldNotBeCalled(SubjectIterator subject)
	{
		subject.NoneCalled.HasValue.Should().BeFalse();
		subject.SingleIndex.HasValue.Should().BeFalse();
	}

	private static void LineHorizontalShouldNotBeCalled(SubjectIterator subject)
	{
		subject.LineHorizontalStartIndex.HasValue.Should().BeFalse();
		subject.LineHorizontalEndIndex.HasValue.Should().BeFalse();
		subject.LineHorizontalArgs.HasValue.Should().BeFalse();
	}

	private static void LineVerticalShouldNotBeCalled(SubjectIterator subject)
	{
		subject.LineVerticalStartIndex.HasValue.Should().BeFalse();
		subject.LineVerticalEndIndex.HasValue.Should().BeFalse();
		subject.LineVerticalArgs.HasValue.Should().BeFalse();
	}

	private static void CornersEdgesAndInsideShouldNotBeCalled(SubjectIterator subject)
	{
		subject.CornerLowXLowYIndex.HasValue.Should().BeFalse();
		subject.CornerLowXHighYIndex.HasValue.Should().BeFalse();
		subject.CornerHighXLowYIndex.HasValue.Should().BeFalse();
		subject.CornerHighXHighYIndex.HasValue.Should().BeFalse();

		subject.EdgeLowXArgs.HasValue.Should().BeFalse();
		subject.EdgeHighXArgs.HasValue.Should().BeFalse();
		subject.EdgeLowYArgs.HasValue.Should().BeFalse();
		subject.EdgeHighYArgs.HasValue.Should().BeFalse();

		subject.InsideArgs.HasValue.Should().BeFalse();
	}

	private class SubjectIterator : ForLoopIterator2
	{
		private Int2 start;

		private int[,] accessCount;

		public Try<bool> NoneCalled { get; private set; }

		public Try<Int2> SingleIndex { get; private set; }

		public Try<Int2> LineHorizontalStartIndex { get; private set; }

		public Try<Int2> LineHorizontalEndIndex { get; private set; }

		public Try<(Int2 start, int lengthX)> LineHorizontalArgs { get; private set; }

		public Try<Int2> LineVerticalStartIndex { get; private set; }

		public Try<Int2> LineVerticalEndIndex { get; private set; }

		public Try<(Int2 start, int lengthY)> LineVerticalArgs { get; private set; }

		public Try<Int2> CornerLowXLowYIndex { get; private set; }

		public Try<Int2> CornerLowXHighYIndex { get; private set; }

		public Try<Int2> CornerHighXLowYIndex { get; private set; }

		public Try<Int2> CornerHighXHighYIndex { get; private set; }

		public Try<(Int2 start, int lengthY)> EdgeLowXArgs { get; private set; }

		public Try<(Int2 start, int lengthY)> EdgeHighXArgs { get; private set; }

		public Try<(Int2 start, int lengthX)> EdgeLowYArgs { get; private set; }

		public Try<(Int2 start, int lengthX)> EdgeHighYArgs { get; private set; }

		public Try<(Int2 start, Int2 dimensions)> InsideArgs { get; private set; }

		public override void InvokeLoop(Int2 start, Int2 dimensions)
		{
			this.start = start;
			this.accessCount = dimensions.ToArray<int>();

			base.InvokeLoop(start, dimensions);
		}

		public void CheckEveryCellAccessedOnce()
		{
			int lengthX = this.accessCount.GetLength(0);
			int lengthY = this.accessCount.GetLength(1);

			for (int iY = 0; iY < lengthY; iY++)
			{
				for (int iX = 0; iX < lengthX; iX++)
				{
					this.accessCount[iX, iY].Should().Be(1);
				}
			}
		}

		/// <inheritdoc />
		protected override void None()
		{
			this.NoneCalled.HasValue.Should().BeFalse();
			this.NoneCalled = true;
		}

		/// <inheritdoc />
		protected override void Single(Int2 index)
		{
			this.SingleIndex.HasValue.Should().BeFalse();
			this.SingleIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void LineHorizontalStartLow(Int2 index)
		{
			this.LineHorizontalStartIndex.HasValue.Should().BeFalse();
			this.LineHorizontalStartIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void LineHorizontalEndHigh(Int2 index)
		{
			this.LineHorizontalEndIndex.HasValue.Should().BeFalse();
			this.LineHorizontalEndIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void LineHorizontal(Int2 start, int lengthX)
		{
			this.LineHorizontalArgs.HasValue.Should().BeFalse();
			this.LineHorizontalArgs = (start, lengthX);

			start -= this.start;
			lengthX += start.X;

			for (int i = start.X; i < lengthX; i++)
			{
				this.accessCount[i, start.Y]++;
			}
		}

		/// <inheritdoc />
		protected override void LineVerticalStartLow(Int2 index)
		{
			this.LineVerticalStartIndex.HasValue.Should().BeFalse();
			this.LineVerticalStartIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void LineVerticalEndHigh(Int2 index)
		{
			this.LineVerticalEndIndex.HasValue.Should().BeFalse();
			this.LineVerticalEndIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void LineVertical(Int2 start, int lengthY)
		{
			this.LineVerticalArgs.HasValue.Should().BeFalse();
			this.LineVerticalArgs = (start, lengthY);

			start -= this.start;
			lengthY += start.Y;

			for (int i = start.Y; i < lengthY; i++)
			{
				this.accessCount[start.X, i]++;
			}
		}

		/// <inheritdoc />
		protected override void CornerLowXLowY(Int2 index)
		{
			this.CornerLowXLowYIndex.HasValue.Should().BeFalse();
			this.CornerLowXLowYIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void CornerLowXHighY(Int2 index)
		{
			this.CornerLowXHighYIndex.HasValue.Should().BeFalse();
			this.CornerLowXHighYIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void CornerHighXLowY(Int2 index)
		{
			this.CornerHighXLowYIndex.HasValue.Should().BeFalse();
			this.CornerHighXLowYIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void CornerHighXHighY(Int2 index)
		{
			this.CornerHighXHighYIndex.HasValue.Should().BeFalse();
			this.CornerHighXHighYIndex = index;

			index -= this.start;
			this.accessCount[index.X, index.Y]++;
		}

		/// <inheritdoc />
		protected override void EdgeHorizontalLowY(Int2 start, int lengthX)
		{
			this.EdgeLowYArgs.HasValue.Should().BeFalse();
			this.EdgeLowYArgs = (start, lengthX);

			start -= this.start;
			lengthX += start.X;

			for (int i = start.X; i < lengthX; i++)
			{
				this.accessCount[i, start.Y]++;
			}
		}

		/// <inheritdoc />
		protected override void EdgeHorizontalHighY(Int2 start, int lengthX)
		{
			this.EdgeHighYArgs.HasValue.Should().BeFalse();
			this.EdgeHighYArgs = (start, lengthX);

			start -= this.start;
			lengthX += start.X;

			for (int i = start.X; i < lengthX; i++)
			{
				this.accessCount[i, start.Y]++;
			}
		}

		/// <inheritdoc />
		protected override void EdgeVerticalLowX(Int2 start, int lengthY)
		{
			this.EdgeLowXArgs.HasValue.Should().BeFalse();
			this.EdgeLowXArgs = (start, lengthY);

			start -= this.start;
			lengthY += start.Y;

			for (int i = start.Y; i < lengthY; i++)
			{
				this.accessCount[start.X, i]++;
			}
		}

		/// <inheritdoc />
		protected override void EdgeVerticalHighX(Int2 start, int lengthY)
		{
			this.EdgeHighXArgs.HasValue.Should().BeFalse();
			this.EdgeHighXArgs = (start, lengthY);

			start -= this.start;
			lengthY += start.Y;

			for (int i = start.Y; i < lengthY; i++)
			{
				this.accessCount[start.X, i]++;
			}
		}

		/// <inheritdoc />
		protected override void Inside(Int2 start, Int2 dimensions)
		{
			this.InsideArgs.HasValue.Should().BeFalse();
			this.InsideArgs = (start, dimensions);

			start -= this.start;
			int lengthX = start.X + dimensions.X;
			int lengthY = start.Y + dimensions.Y;

			for (int iY = start.Y; iY < lengthY; iY++)
			{
				for (int iX = start.X; iX < lengthX; iX++)
				{
					this.accessCount[iX, iY]++;
				}
			}
		}
	}
}
