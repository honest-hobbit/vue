﻿namespace HQS.Test.Utility.Numerics;

public static class ZigZagEncodingTests
{
	public static class OfLong
	{
		public static IEnumerable<object[]> BitValues()
		{
			for (int power = 0; power < 64; power++)
			{
				yield return new object[] { MathUtility.LongPowerOf2(power) };
				yield return new object[] { -MathUtility.LongPowerOf2(power) };
			}
		}

		public static IEnumerable<object[]> RandomValues()
		{
			var random = new Random(0);
			for (int i = 0; i < 100; i++)
			{
				long number = random.NextLong(long.MaxValue);
				yield return new object[] { number };
				yield return new object[] { -number };
			}
		}

		[Theory]
		[InlineData(long.MinValue)]
		[InlineData(long.MinValue + 1)]
		[InlineData(-1L)]
		[InlineData(0L)]
		[InlineData(1L)]
		[InlineData(long.MaxValue - 1)]
		[InlineData(long.MaxValue)]
		public static void BaseTests(long value) => DualConverterTests.RunTests(ZigZagEncoding.OfLong, value);

		[Theory]
		[MemberData(nameof(BitValues))]
		public static void BitTests(long value) => DualConverterTests.RunTests(ZigZagEncoding.OfLong, value);

		[Theory]
		[MemberData(nameof(RandomValues))]
		public static void RandomTests(long value) => DualConverterTests.RunTests(ZigZagEncoding.OfLong, value);
	}

	public static class OfInt
	{
		public static IEnumerable<object[]> BitValues()
		{
			for (int power = 0; power < 32; power++)
			{
				yield return new object[] { MathUtility.PowerOf2(power) };
				yield return new object[] { -MathUtility.PowerOf2(power) };
			}
		}

		public static IEnumerable<object[]> RandomValues()
		{
			var random = new Random(0);
			for (int i = 0; i < 100; i++)
			{
				long number = random.Next(int.MaxValue);
				yield return new object[] { number };
				yield return new object[] { -number };
			}
		}

		[Theory]
		[InlineData(int.MinValue)]
		[InlineData(int.MinValue + 1)]
		[InlineData(-1)]
		[InlineData(0)]
		[InlineData(1)]
		[InlineData(int.MaxValue - 1)]
		[InlineData(int.MaxValue)]
		public static void BaseTests(int value) => DualConverterTests.RunTests(ZigZagEncoding.OfInt, value);

		[Theory]
		[MemberData(nameof(BitValues))]
		public static void BitTests(int value) => DualConverterTests.RunTests(ZigZagEncoding.OfInt, value);

		[Theory]
		[MemberData(nameof(RandomValues))]
		public static void RandomTests(int value) => DualConverterTests.RunTests(ZigZagEncoding.OfInt, value);
	}

	public static class OfShort
	{
		public static IEnumerable<object[]> BitValues()
		{
			for (int power = 0; power < 16; power++)
			{
				yield return new object[] { (short)MathUtility.PowerOf2(power) };
				yield return new object[] { (short)-MathUtility.PowerOf2(power) };
			}
		}

		public static IEnumerable<object[]> RandomValues()
		{
			var random = new Random(0);
			for (int i = 0; i < 100; i++)
			{
				int number = random.Next(short.MaxValue);
				yield return new object[] { (short)number };
				yield return new object[] { (short)-number };
			}
		}

		[Theory]
		[InlineData(short.MinValue)]
		[InlineData(short.MinValue + 1)]
		[InlineData(-1)]
		[InlineData(0)]
		[InlineData(1)]
		[InlineData(short.MaxValue - 1)]
		[InlineData(short.MaxValue)]
		public static void BaseTests(short value) => DualConverterTests.RunTests(ZigZagEncoding.OfShort, value);

		[Theory]
		[MemberData(nameof(BitValues))]
		public static void BitTests(short value) => DualConverterTests.RunTests(ZigZagEncoding.OfShort, value);

		[Theory]
		[MemberData(nameof(RandomValues))]
		public static void RandomTests(short value) => DualConverterTests.RunTests(ZigZagEncoding.OfShort, value);
	}

	public static class OfSByte
	{
		public static IEnumerable<object[]> EveryValue()
		{
			for (int value = sbyte.MinValue; value <= sbyte.MaxValue; value++)
			{
				yield return new object[] { value };
			}
		}

		[Theory]
		[MemberData(nameof(EveryValue))]
		public static void Tests(sbyte value) => DualConverterTests.RunTests(ZigZagEncoding.OfSByte, value);
	}
}
