﻿namespace HQS.Test.Utility.Types;

public static class GuidUtilityTests
{
	[Fact]
	public static void CopyToEmptyGuid() => TestCopyTo(Guid.Empty);

	[Theory]
	[InlineData("a063aa06-13d1-4c37-b5c1-a92fcb10cbeb")]
	[InlineData("3aeca12f-348f-45a2-92d0-24a7f081fd8d")]
	[InlineData("fbb3f7d1-926d-4e3f-a434-0399f6aa6124")]
	public static void CopyTo(string value) => TestCopyTo(new Guid(value));

	private static void TestCopyTo(Guid value)
	{
		var subject = new byte[16];
		value.CopyTo(subject);
		subject.Should().BeEquivalentTo(value.ToByteArray());
	}
}
