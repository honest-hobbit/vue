﻿namespace HQS.Test.Utility.Types;

public static class DualConverterTests
{
	public static void RunTests<T1, T2>(IDualConverter<T1, T2> subject, T1 value)
	{
		Debug.Assert(subject != null);

		var temp = subject.Convert(value);
		var result = subject.Convert(temp);

		result.Should().Be(value);
	}
}
