﻿namespace HQS.Test.Utility.Collections;

public static class RingListTests
{
	[Fact]
	public static void GetEnumeratorConstructorCapacityReturnsEmptyCollection()
	{
		var buffer = CreateSubject(5);

		buffer.Capacity.Should().Be(5);
		buffer.Count.Should().Be(0);
		buffer.ToArray().Length.Should().Be(0);
	}

	[Fact]
	public static void ConstructorSizeIndexAccessHasCorrectContent()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3);

		buffer.Capacity.Should().Be(5);
		buffer.Count.Should().Be(4);

		for (int i = 0; i < 4; i++)
		{
			buffer[i].Should().Be(i);
		}
	}

	[Fact]
	public static void GetEnumeratorConstructorDefinedArrayHasCorrectContent()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3);

		int x = 0;
		foreach (var item in buffer)
		{
			item.Should().Be(x);
			x++;
		}
	}

	[Fact]
	public static void PushBackHasCorrectContent()
	{
		var buffer = CreateSubject(5);

		for (int i = 0; i < 5; i++)
		{
			buffer.PushLastFixedCapacity(i);
		}

		buffer.First.Should().Be(0);
		for (int i = 0; i < 5; i++)
		{
			buffer[i].Should().Be(i);
		}
	}

	[Fact]
	public static void PushBackOverflowingBufferHasCorrectContent()
	{
		var buffer = CreateSubject(5);

		for (int i = 0; i < 10; i++)
		{
			buffer.PushLastFixedCapacity(i);
		}

		buffer.ToArray().Should().BeEquivalentTo(new[] { 5, 6, 7, 8, 9 });
	}

	[Fact]
	public static void GetEnumeratorOverflowedArrayHasCorrectContent()
	{
		var buffer = CreateSubject(5);

		for (int i = 0; i < 10; i++)
		{
			buffer.PushLastFixedCapacity(i);
		}

		// buffer should have [5,6,7,8,9]
		int x = 5;
		foreach (var item in buffer)
		{
			item.Should().Be(x);
			x++;
		}
	}

	[Fact]
	public static void ToArrayConstructorDefinedArrayHasCorrectContent()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3);

		buffer.ToArray().Should().BeEquivalentTo(new[] { 0, 1, 2, 3 });
	}

	[Fact]
	public static void ToArrayOverflowedBufferHasCorrectContent()
	{
		var buffer = CreateSubject(5);

		for (int i = 0; i < 10; i++)
		{
			buffer.PushLastFixedCapacity(i);
		}

		buffer.ToArray().Should().BeEquivalentTo(new[] { 5, 6, 7, 8, 9 });
	}

	[Fact]
	public static void PushFrontHasCorrectContent()
	{
		var buffer = CreateSubject(5);

		for (int i = 0; i < 5; i++)
		{
			buffer.PushFirstFixedCapacity(i);
		}

		buffer.ToArray().Should().BeEquivalentTo(new[] { 4, 3, 2, 1, 0 });
	}

	[Fact]
	public static void PushFrontAndOverflowHasCorrectContent()
	{
		var buffer = CreateSubject(5);

		for (int i = 0; i < 10; i++)
		{
			buffer.PushFirstFixedCapacity(i);
		}

		buffer.ToArray().Should().BeEquivalentTo(new[] { 9, 8, 7, 6, 5 });
	}

	[Fact]
	public static void FrontIsCorrectItem()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer.First.Should().Be(0);
	}

	[Fact]
	public static void BackIsCorrectItem()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer.Last.Should().Be(4);
	}

	[Fact]
	public static void BackOfBufferOverflowByOneIsCorrectItem()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);
		buffer.PushLastFixedCapacity(42);

		buffer.ToArray().Should().BeEquivalentTo(new[] { 1, 2, 3, 4, 42 });
		buffer.Last.Should().Be(42);
	}

	[Fact]
	public static void PopBackRemovesBackElement()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer.Count.Should().Be(5);

		buffer.PopLast();

		buffer.Count.Should().Be(4);
		buffer.ToArray().Should().BeEquivalentTo(new[] { 0, 1, 2, 3 });
	}

	[Fact]
	public static void PopBackInOverflowBufferRemovesBackElement()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);
		buffer.PushLastFixedCapacity(5);

		buffer.Count.Should().Be(5);
		buffer.ToArray().Should().BeEquivalentTo(new[] { 1, 2, 3, 4, 5 });

		buffer.PopLast();

		buffer.Count.Should().Be(4);
		buffer.ToArray().Should().BeEquivalentTo(new[] { 1, 2, 3, 4 });
	}

	[Fact]
	public static void PopFrontRemovesBackElement()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer.Count.Should().Be(5);

		buffer.PopFirst();

		buffer.Count.Should().Be(4);
		buffer.ToArray().Should().BeEquivalentTo(new[] { 1, 2, 3, 4 });
	}

	[Fact]
	public static void PopFrontInOverflowBufferRemovesBackElement()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);
		buffer.PushFirstFixedCapacity(5);

		buffer.Count.Should().Be(5);
		buffer.ToArray().Should().BeEquivalentTo(new[] { 5, 0, 1, 2, 3 });

		buffer.PopFirst();

		buffer.Count.Should().Be(4);
		buffer.ToArray().Should().BeEquivalentTo(new[] { 0, 1, 2, 3 });
	}

	[Fact]
	public static void SetIndexReplacesElement()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer[1] = 10;
		buffer[3] = 30;

		buffer.ToArray().Should().BeEquivalentTo(new[] { 0, 10, 2, 30, 4 });
	}

	[Fact]
	public static void WithDifferentSizeAndCapacityBackReturnsLastArrayPosition()
	{
		// test to confirm this issue does not happen anymore:
		// https://github.com/joaoportela/CircullarBuffer-CSharp/issues/2
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer.PopFirst(); // (make size and capacity different)

		buffer.Last.Should().Be(4);
	}

	[Fact]
	public static void ClearRemovesAllElements()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer.Clear();

		buffer.Capacity.Should().Be(5);
		buffer.Count.Should().Be(0);
		buffer.ToArray().Length.Should().Be(0);
		buffer.AsEnumerableOnly().Count().Should().Be(0);
	}

	[Fact]
	public static void ClearCanStillBePushedToAfterwards()
	{
		var buffer = CreateSubject(5, 0, 1, 2, 3, 4);

		buffer.Clear();

		buffer.PushFirstFixedCapacity(1);
		buffer.PushLastFixedCapacity(2);
		buffer.PushFirstFixedCapacity(0);
		buffer.PushLastFixedCapacity(3);

		buffer.Capacity.Should().Be(5);
		buffer.Count.Should().Be(4);

		for (int i = 0; i < buffer.Count; i++)
		{
			buffer[i].Should().Be(i);
		}

		var expected = new[] { 0, 1, 2, 3 };
		buffer.ToArray().Should().BeEquivalentTo(expected);
		buffer.Should().BeEquivalentTo(expected);
	}

	[Fact]
	public static void ZeroCapacityExpandsWhenPushedTo()
	{
		var buffer = CreateSubject(0);

		buffer.PushFirst(1);
		buffer.PushLast(2);

		buffer.Capacity.Should().BeGreaterThan(0);
		buffer.Count.Should().Be(2);

		var expected = new[] { 1, 2 };
		buffer.ToArray().Should().BeEquivalentTo(expected);
		buffer.Should().BeEquivalentTo(expected);
	}

	[Fact]
	public static void SingleCapacityExpandsWhenPushedTo()
	{
		var buffer = CreateSubject(1);

		buffer.PushFirst(1);
		buffer.PushLast(2);

		buffer.Capacity.Should().BeGreaterThan(1);
		buffer.Count.Should().Be(2);

		var expected = new[] { 1, 2 };
		buffer.ToArray().Should().BeEquivalentTo(expected);
		buffer.Should().BeEquivalentTo(expected);
	}

	[Fact]
	public static void DoesNotExpandWhenThereIsCapacity()
	{
		var buffer = CreateSubject(2);

		buffer.PushFirst(1);
		buffer.PushLast(2);

		buffer.Capacity.Should().Be(2);
		buffer.Count.Should().Be(2);

		var expected = new[] { 1, 2 };
		buffer.ToArray().Should().BeEquivalentTo(expected);
		buffer.Should().BeEquivalentTo(expected);
	}

	[Fact]
	public static void EnumerableConstructorUsesCountProperty()
	{
		var expected = new int[] { 0, 1, 2, 3, 4 };
		var buffer = new RingList<int>(expected);

		buffer.Capacity.Should().Be(expected.Length);
		buffer.Count.Should().Be(expected.Length);

		buffer.ToArray().Should().BeEquivalentTo(expected);
		buffer.Should().BeEquivalentTo(expected);
	}

	[Fact]
	public static void EnumerableConstructorCantUseCountProperty()
	{
		var expected = new int[] { 0, 1, 2, 3, 4 };
		var buffer = new RingList<int>(expected.AsEnumerableOnly());

		buffer.Capacity.Should().BeGreaterOrEqualTo(expected.Length);
		buffer.Count.Should().Be(expected.Length);

		buffer.ToArray().Should().BeEquivalentTo(expected);
		buffer.Should().BeEquivalentTo(expected);
	}

	private static RingList<int> CreateSubject(int capacity)
	{
		Debug.Assert(capacity >= 0);

		return new RingList<int>(capacity);
	}

	private static RingList<int> CreateSubject(int capacity, params int[] values)
	{
		Debug.Assert(capacity > 0);
		Debug.Assert(values != null);
		Debug.Assert(values.Length > 0);
		Debug.Assert(values.Length <= capacity);

		var result = new RingList<int>(capacity);
		foreach (var value in values)
		{
			result.PushLastFixedCapacity(value);
		}

		return result;
	}
}
