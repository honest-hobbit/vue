﻿namespace HQS.Test.Utility.Collections;

public static class OrderedDictionaryTests
{
	[Fact]
	public static void RunDictionaryTests()
	{
		IDictionaryTests.UsingStrings.RunTests(() => new OrderedDictionary<string, string>());
	}
}
