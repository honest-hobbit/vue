﻿namespace HQS.Test.Utility.Indexing.Serialization;

public static class Index4DSerializerTests
{
	private const int ExpectedLength = Length.OfInt.InBytes * 4;

	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new Index4D(int.MinValue) },
				new object[] { new Index4D(int.MaxValue) },
				new object[] { new Index4D(-7, 13, 42, -1337) },
				new object[] { Index4D.Zero },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(Index4D value) =>
		SerDesTests.RunTests(Index4DSerDes.Instance, value, ExpectedLength);
}
