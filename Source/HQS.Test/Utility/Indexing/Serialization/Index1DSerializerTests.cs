﻿namespace HQS.Test.Utility.Indexing.Serialization;

public static class Index1DSerializerTests
{
	private const int ExpectedLength = Length.OfInt.InBytes;

	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new Index1D(int.MinValue) },
				new object[] { new Index1D(int.MaxValue) },
				new object[] { new Index1D(-7) },
				new object[] { new Index1D(7) },
				new object[] { Index1D.Zero },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(Index1D value) =>
		SerDesTests.RunTests(Index1DSerDes.Instance, value, ExpectedLength);
}
