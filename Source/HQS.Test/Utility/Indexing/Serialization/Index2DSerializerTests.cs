﻿namespace HQS.Test.Utility.Indexing.Serialization;

public static class Index2DSerializerTests
{
	private const int ExpectedLength = Length.OfInt.InBytes * 2;

	public static IEnumerable<object[]> Values() =>
		new object[][]
		{
				new object[] { new Index2D(int.MinValue) },
				new object[] { new Index2D(int.MaxValue) },
				new object[] { new Index2D(-7, 13) },
				new object[] { Index2D.Zero },
		};

	[Theory]
	[MemberData(nameof(Values))]
	public static void RunTests(Index2D value) =>
		SerDesTests.RunTests(Index2DSerDes.Instance, value, ExpectedLength);
}
