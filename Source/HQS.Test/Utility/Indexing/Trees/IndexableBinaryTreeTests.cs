﻿namespace HQS.Test.Utility.Indexing.Trees;

using HQS.Utility.Indexing.Curves;

public static class IndexableBinaryTreeTests
{
	private static void TestIndexer<TIndex, TValue>(
		this IndexableBinaryTree<TIndex, TValue> subject, BinaryTreeIndex<TIndex> index, TValue value)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(subject != null);

		subject[index] = default;
		subject[index].Should().Be(default(TValue));

		subject[index] = value;
		subject[index].Should().Be(value);

		var mortonIndex = index.ToMortonIndex();

		subject[mortonIndex] = default;
		subject[mortonIndex].Should().Be(default(TValue));

		subject[mortonIndex] = value;
		subject[mortonIndex].Should().Be(value);
	}

	private static void TestLayer<TIndex>(
		IndexableBinaryTree<TIndex, int> subject,
		int layer,
		IEnumerable<CurveIndexPair<TIndex>> testValues = null)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(subject != null);
		Debug.Assert(layer >= 0);

		testValues ??= new[] { CurveIndexPair.New(0, IndexUtility.Zero<TIndex>()) };

		// used to differentiate test values stored in this layer from values stored in other layers
		int extraOffset = layer * 127;

		foreach (var pair in testValues)
		{
			subject.TestIndexer(BinaryTreeIndex.New(layer, pair.Index), extraOffset + pair.Code);
		}

		var enumeratedValues = subject.ToArray();
		int index = MortonIndexUtility.GetLayerOffset<TIndex>(layer);

		foreach (var pair in testValues)
		{
			subject[BinaryTreeIndex.New(layer, pair.Index)].Should().Be(extraOffset + pair.Code);

			enumeratedValues[index].Index.Equals(BinaryTreeIndex.New(layer, pair.Index)).Should().BeTrue();
			enumeratedValues[index].Value.Should().Be(extraOffset + pair.Code);
			index++;
		}
	}

	public static class BinaryTree
	{
		[Fact]
		public static void OneLayer()
		{
			var subject = new IndexableBinaryTree<Index1D, int>(1);

			subject.Dimensions.Equals(BinaryTreeIndex.New(1, new Index1D(1))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index1D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(0, new Index1D(0))).Should().BeTrue();

			TestLayer0(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index1D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void TwoLayers()
		{
			var subject = new IndexableBinaryTree<Index1D, int>(2);

			subject.Dimensions.Equals(BinaryTreeIndex.New(2, new Index1D(2))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index1D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(1, new Index1D(1))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index1D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void ThreeLayers()
		{
			var subject = new IndexableBinaryTree<Index1D, int>(3);

			subject.Dimensions.Equals(BinaryTreeIndex.New(3, new Index1D(4))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index1D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(2, new Index1D(3))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			TestLayer2(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index1D>(subject.Dimensions.Layer));
		}

		private static void TestLayer0(IndexableBinaryTree<Index1D, int> subject) =>
			TestLayer(subject, 0);

		private static void TestLayer1(IndexableBinaryTree<Index1D, int> subject) =>
			TestLayer(subject, 1, MortonCurve.Index1D.GetCurveWithCodes(1));

		private static void TestLayer2(IndexableBinaryTree<Index1D, int> subject) =>
			TestLayer(subject, 2, MortonCurve.Index1D.GetCurveWithCodes(2));
	}

	public static class Quadtree
	{
		[Fact]
		public static void OneLayer()
		{
			var subject = new IndexableBinaryTree<Index2D, int>(1);

			subject.Dimensions.Equals(BinaryTreeIndex.New(1, new Index2D(1))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index2D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(0, new Index2D(0))).Should().BeTrue();

			TestLayer0(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index2D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void TwoLayers()
		{
			var subject = new IndexableBinaryTree<Index2D, int>(2);

			subject.Dimensions.Equals(BinaryTreeIndex.New(2, new Index2D(2))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index2D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(1, new Index2D(1))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index2D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void ThreeLayers()
		{
			var subject = new IndexableBinaryTree<Index2D, int>(3);

			subject.Dimensions.Equals(BinaryTreeIndex.New(3, new Index2D(4))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index2D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(2, new Index2D(3))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			TestLayer2(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index2D>(subject.Dimensions.Layer));
		}

		private static void TestLayer0(IndexableBinaryTree<Index2D, int> subject) =>
			TestLayer(subject, 0);

		private static void TestLayer1(IndexableBinaryTree<Index2D, int> subject) =>
			TestLayer(subject, 1, MortonCurve.Index2D.GetCurveWithCodes(1));

		private static void TestLayer2(IndexableBinaryTree<Index2D, int> subject) =>
			TestLayer(subject, 2, MortonCurve.Index2D.GetCurveWithCodes(2));
	}

	public static class Octree
	{
		[Fact]
		public static void OneLayer()
		{
			var subject = new IndexableBinaryTree<Index3D, int>(1);

			subject.Dimensions.Equals(BinaryTreeIndex.New(1, new Index3D(1))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index3D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(0, new Index3D(0))).Should().BeTrue();

			TestLayer0(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index3D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void TwoLayers()
		{
			var subject = new IndexableBinaryTree<Index3D, int>(2);

			subject.Dimensions.Equals(BinaryTreeIndex.New(2, new Index3D(2))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index3D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(1, new Index3D(1))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index3D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void ThreeLayers()
		{
			var subject = new IndexableBinaryTree<Index3D, int>(3);

			subject.Dimensions.Equals(BinaryTreeIndex.New(3, new Index3D(4))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index3D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(2, new Index3D(3))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			TestLayer2(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index3D>(subject.Dimensions.Layer));
		}

		private static void TestLayer0(IndexableBinaryTree<Index3D, int> subject) =>
			TestLayer(subject, 0);

		private static void TestLayer1(IndexableBinaryTree<Index3D, int> subject) =>
			TestLayer(subject, 1, MortonCurve.Index3D.GetCurveWithCodes(1));

		private static void TestLayer2(IndexableBinaryTree<Index3D, int> subject) =>
			TestLayer(subject, 2, MortonCurve.Index3D.GetCurveWithCodes(2));
	}

	public static class FourDimensionalBinaryTree
	{
		[Fact]
		public static void OneLayer()
		{
			var subject = new IndexableBinaryTree<Index4D, int>(1);

			subject.Dimensions.Equals(BinaryTreeIndex.New(1, new Index4D(1))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index4D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(0, new Index4D(0))).Should().BeTrue();

			TestLayer0(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index4D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void TwoLayers()
		{
			var subject = new IndexableBinaryTree<Index4D, int>(2);

			subject.Dimensions.Equals(BinaryTreeIndex.New(2, new Index4D(2))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index4D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(1, new Index4D(1))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index4D>(subject.Dimensions.Layer));
		}

		[Fact]
		public static void ThreeLayers()
		{
			var subject = new IndexableBinaryTree<Index4D, int>(3);

			subject.Dimensions.Equals(BinaryTreeIndex.New(3, new Index4D(4))).Should().BeTrue();
			subject.LowerBounds.Equals(BinaryTreeIndex.New(0, new Index4D(0))).Should().BeTrue();
			subject.UpperBounds.Equals(BinaryTreeIndex.New(2, new Index4D(3))).Should().BeTrue();

			TestLayer0(subject);
			TestLayer1(subject);
			TestLayer2(subject);
			subject.Count().Should().Be(MortonIndexUtility.GetCombinedLayersLength<Index4D>(subject.Dimensions.Layer));
		}

		private static void TestLayer0(IndexableBinaryTree<Index4D, int> subject) =>
			TestLayer(subject, 0);

		private static void TestLayer1(IndexableBinaryTree<Index4D, int> subject) =>
			TestLayer(subject, 1, MortonCurve.Index4D.GetCurveWithCodes(1));

		private static void TestLayer2(IndexableBinaryTree<Index4D, int> subject) =>
			TestLayer(subject, 2, MortonCurve.Index4D.GetCurveWithCodes(2));
	}
}
