﻿namespace HQS.Test.Utility.Indexing.Trees;

using HQS.Utility.Indexing.Curves;

public static class MortonIndexTests
{
	[Fact]
	public static void MortonIndex1D() => TestMultipleLayers(MortonCurve.Index1D);

	[Fact]
	public static void MortonIndex2D() => TestMultipleLayers(MortonCurve.Index2D);

	[Fact]
	public static void MortonIndex3D() => TestMultipleLayers(MortonCurve.Index3D);

	[Fact]
	public static void MortonIndex4D() => TestMultipleLayers(MortonCurve.Index4D);

	private static void TestMultipleLayers<TIndex>(ISpaceFillingCurve<TIndex> encoder)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(encoder != null);

		TestMortonIndex(BinaryTreeIndex.New(0, IndexUtility.Zero<TIndex>()));
		TestLayer(encoder, 1);
		TestLayer(encoder, 2);
		TestLayer(encoder, 3);
		TestLayer(encoder, 4);
	}

	private static void TestLayer<TIndex>(ISpaceFillingCurve<TIndex> encoder, int layer)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(encoder != null);
		Debug.Assert(layer >= 0);

		foreach (var index in encoder.GetCurve(layer))
		{
			TestMortonIndex(BinaryTreeIndex.New(layer, index));
		}
	}

	private static void TestMortonIndex<TIndex>(BinaryTreeIndex<TIndex> index)
		where TIndex : struct, IIndex<TIndex>
	{
		var mortonCode = index.ToMortonCode();
		var mortonIndexFromIndex = index.ToMortonIndex();
		var mortonIndexFromCode = new MortonIndex<TIndex>(mortonCode);
		var treeIndexFromCode = BinaryTreeIndex.MortonEncoder<TIndex>().DecodeIndex(mortonCode);

		treeIndexFromCode.Equals(index).Should().BeTrue();
		treeIndexFromCode.ToMortonCode().Should().Be(mortonCode);

		mortonIndexFromIndex.ToMortonCode().Should().Be(mortonCode);
		mortonIndexFromCode.ToMortonCode().Should().Be(mortonCode);

		mortonIndexFromIndex.ToIndex().Equals(index).Should().BeTrue();
		mortonIndexFromCode.ToIndex().Equals(index).Should().BeTrue();
	}
}
