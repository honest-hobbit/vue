﻿namespace HQS.Test.Utility.Indexing.Indexables;

public static class IndexingBoundsTests
{
	[Fact]
	public static void HasBounds()
	{
		var subject = new IndexingBounds<Index3D>(new Index3D(2), new Index3D(4));

		subject.Dimensions.Equals(new Index3D(4)).Should().BeTrue();
		subject.LowerBounds.Equals(new Index3D(2)).Should().BeTrue();
		subject.UpperBounds.Equals(new Index3D(5)).Should().BeTrue();
	}
}
