﻿namespace HQS.Test.Utility.Indexing.Indexables;

/// <summary>
/// Experimental tests I wrote to help me out while updating some Indexable bounds contracts.
/// </summary>
public static class IndexableArraysTests
{
	/// <summary>
	/// Testing the bounds of a 1D array.
	/// </summary>
	/// <param name="length">The length of the array.</param>
	[Theory]
	[InlineData(1)]
	[InlineData(2)]
	[InlineData(3)]
	public static void TestingBoundsOfArray1D(int length)
	{
		IndexableArray1D<int> subject = new IndexableArray1D<int>(new Index1D(length));

		subject.LowerBounds.Should().Equal(new Index1D(0));
		subject.UpperBounds.Should().Equal(new Index1D(length - 1));
		subject.Dimensions.Should().Equal(new Index1D(length));
	}

	/// <summary>
	/// Testing the bounds of a 2D array.
	/// </summary>
	/// <param name="xLength">Length of the X dimension of the array.</param>
	/// <param name="yLength">Length of the Y dimension of the array.</param>
	[Theory]
	[InlineData(1, 1)]
	[InlineData(2, 2)]
	[InlineData(1, 2)]
	[InlineData(2, 1)]
	public static void TestingBoundsOfArray2D(int xLength, int yLength)
	{
		IndexableArray2D<int> subject = new IndexableArray2D<int>(new Index2D(xLength, yLength));

		subject.LowerBounds.Should().Equal(new Index2D(0, 0));
		subject.UpperBounds.Should().Equal(new Index2D(xLength - 1, yLength - 1));
		subject.Dimensions.Should().Equal(new Index2D(xLength, yLength));
	}
}
