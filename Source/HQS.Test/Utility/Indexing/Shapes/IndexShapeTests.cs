﻿namespace HQS.Test.Utility.Indexing.Shapes;

public static class IndexShapeTests
{
	public static IEnumerable<object[]> Lengths
	{
		get
		{
			yield return new object[] { 1 };
			yield return new object[] { 2 };
			yield return new object[] { 3 };
			yield return new object[] { 4 };
			yield return new object[] { 8 };
			yield return new object[] { 9 };
			yield return new object[] { 10 };
			yield return new object[] { 11 };
		}
	}

	public static IEnumerable<object[]> VerticalShapes
	{
		get
		{
			yield return new object[] { 1, 1 };
			yield return new object[] { 1, 2 };
			yield return new object[] { 2, 5 };
			yield return new object[] { 3, 6 };
			yield return new object[] { 4, 4 };
			yield return new object[] { 8, 4 };
			yield return new object[] { 9, 5 };
			yield return new object[] { 10, 1 };
			yield return new object[] { 11, 2 };
		}
	}

	[Theory]
	[MemberData(nameof(Lengths))]
	public static void Circle(int diameter)
	{
		Tests(IndexShape.CreateCircle(diameter));
	}

	[Theory]
	[MemberData(nameof(Lengths))]
	public static void Cube(int length)
	{
		Tests(IndexShape.CreateCube(length));
	}

	[Theory]
	[MemberData(nameof(Lengths))]
	public static void Sphere(int diameter)
	{
		Tests(IndexShape.CreateSphere(diameter));
	}

	[Theory]
	[MemberData(nameof(Lengths))]
	public static void Square(int length)
	{
		Tests(IndexShape.CreateSquare(length));
	}

	[Theory]
	[MemberData(nameof(VerticalShapes))]
	public static void Column(int length, int height)
	{
		Tests(IndexShape.CreateColumn(length, height));
	}

	[Theory]
	[MemberData(nameof(VerticalShapes))]
	public static void Cylinder(int diameter, int height)
	{
		Tests(IndexShape.CreateCylinder(diameter, height));
	}

	private static void Tests<TIndex>(IIndexShape<TIndex> shape)
		where TIndex : struct, IIndex<TIndex>
	{
		Debug.Assert(shape != null);

		var padding = IndexUtility.Create<TIndex>(2);
		var containsMask = shape.ToContainsMask(padding);
		var countedMask = shape.ToCountedMask(padding);

		shape.Count.Should().Be(containsMask.Count(pair => pair.Value));
		shape.Count.Should().Be(countedMask.Count(pair => pair.Value == 1));

		countedMask.All(pair => pair.Value == 0 || pair.Value == 1).Should().BeTrue();
		containsMask.All(pair => pair.Value ? countedMask[pair.Index] == 1 : countedMask[pair.Index] == 0).Should().BeTrue();
	}
}
