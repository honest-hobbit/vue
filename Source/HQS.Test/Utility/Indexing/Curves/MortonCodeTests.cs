﻿namespace HQS.Test.Utility.Indexing.Curves;

using HQS.Utility.Indexing.Curves;

/// <summary>
///
/// </summary>
public static class MortonCodeTests
{
	[Fact]
	public static void TestMortonCode1D()
	{
		foreach (var pair in MortonCurve.Index1D.GetCurveWithCodes(4))
		{
			TestMortonCode(pair.Index, pair.Code);
		}
	}

	[Fact]
	public static void TestMortonCode2D()
	{
		foreach (var pair in MortonCurve.Index2D.GetCurveWithCodes(4))
		{
			TestMortonCode(pair.Index, pair.Code);
		}
	}

	[Fact]
	public static void TestMortonCode3D()
	{
		foreach (var pair in MortonCurve.Index3D.GetCurveWithCodes(4))
		{
			TestMortonCode(pair.Index, pair.Code);
		}
	}

	[Fact]
	public static void TestMortonCode4D()
	{
		foreach (var pair in MortonCurve.Index4D.GetCurveWithCodes(4))
		{
			TestMortonCode(pair.Index, pair.Code);
		}
	}

	private static void TestMortonCode<TIndex>(TIndex index, int expectedMortonCode)
		where TIndex : struct, IIndex<TIndex>
	{
		var mortonCode = index.ToMortonCode();
		mortonCode.Should().Be(expectedMortonCode);
		index.MortonCurve.DecodeIndex(mortonCode).Equals(index).Should().BeTrue();
	}
}
