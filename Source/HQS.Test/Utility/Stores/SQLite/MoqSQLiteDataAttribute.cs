﻿namespace HQS.Test.Utility.Stores.SQLite;

public class MoqSQLiteDataAttribute : AutoDataAttribute
{
	public MoqSQLiteDataAttribute()
		: base(() => new Fixture().Customize(new AutoMoqCustomization()))
	{
	}
}
