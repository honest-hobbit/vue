﻿namespace HQS.Test.Utility.Stores.SQLite;

// change this class to public when you want to run the tests
// if these tests fail from a BadImageFormatException go to Test -> Test Settings -> Default Processor Architecture -> X64
public static class SQLiteStoreTests
{
	public static class AddAndGetTests
	{
		[Theory, MoqSQLiteData]
		public static void ShouldAddEntityToDatabase(TestEntity entity)
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				store.Add(entity);
				var retrieved = store.TryGet<int, TestEntity>(entity.Key);

				retrieved.HasValue.Should().BeTrue();
				retrieved.Value.Should().BeEquivalentTo(entity);
			});
		}
	}

	public static class UpdateTests
	{
		[Theory, MoqSQLiteData]
		public static void ShouldUpdateEntityInDatabase(TestEntity entity)
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				store.Add(entity);

				entity.TestString = "test";
				store.Update(entity);

				var retrieved = store.TryGet<int, TestEntity>(entity.Key);

				retrieved.HasValue.Should().BeTrue();
				retrieved.Value.Should().BeEquivalentTo(entity);
			});
		}
	}

	public static class AllTests
	{
		[Theory, MoqSQLiteData]
		public static void ShouldRetrieveAllEntitiesFromTable(TestEntity entity1, TestEntity entity2)
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				store.Add(entity1);
				store.Add(entity2);

				var entities = store.All<TestEntity>();

				entities.Count().Should().Be(2);
				entities.First(x => x.Key == entity1.Key).Should().BeEquivalentTo(entity1);
				entities.First(x => x.Key == entity2.Key).Should().BeEquivalentTo(entity2);
			});
		}

		[Fact]
		public static void ShouldReturnEmptyListForEmptyTable()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				var entities = store.All<TestEntity>();

				entities.Should().BeEmpty();
			});
		}
	}

	public static class RemoveTests
	{
		[Theory, MoqSQLiteData]
		public static void ShouldRemoveEntityFromDatabase(TestEntity entity)
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				store.Add(entity);
				store.Remove(entity);

				var entities = store.All<TestEntity>();
				entities.Should().NotContain(entity);
			});
		}
	}

	public static class PerformanceTests
	{
		private const int EntityCount = 100000;

		[Fact]
		public static void AddAll()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				store.All<TestEntity>().Count().Should().Be(0);

				store.AddMany(Factory.CreateMany(EntityCount, () => new TestEntity()));

				store.All<TestEntity>().Count().Should().Be(EntityCount);
			});
		}

		[Fact]
		public static void TransactionBulkAdd()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				store.All<TestEntity>().Count().Should().Be(0);

				store.RunInTransaction(transaction =>
				{
					foreach (var entity in Factory.CreateMany(EntityCount, () => new TestEntity()))
					{
						transaction.Add(entity);
					}
				});

				store.All<TestEntity>().Count().Should().Be(EntityCount);
			});
		}

		// this test can be used to time the difference between bulk add and individually adding
		// but warning, even at just 1000 entities it takes 13 seconds to complete
		////[Fact]
		internal static void IndividualWrites()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<TestEntity>();
				store.All<TestEntity>().Count().Should().Be(0);

				foreach (var entity in Factory.CreateArray(EntityCount, () => new TestEntity()))
				{
					store.Add(entity);
				}

				store.All<TestEntity>().Count().Should().Be(EntityCount);
			});
		}
	}

	public static class InternalEntityTests
	{
		[Theory, MoqSQLiteData]
		internal static void InternalVisibilityEntityShouldWork(InternalTestEntity entity)
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<InternalTestEntity>();
				store.Add(entity);
				var entityRetrieved = store.TryGet<int, InternalTestEntity>(entity.Key);

				entityRetrieved.HasValue.Should().BeTrue();
				entityRetrieved.Value.Should().BeEquivalentTo(entity);
			});
		}
	}

	public static class PrivateEntityTests
	{
		[Fact]
		public static void PrivateVisibilityEntityShouldWork()
		{
			StoreTesting.UseSQLite().Execute(store =>
			{
				store.Initialize<PrivateTestEntity>();

				var entity = new PrivateTestEntity()
				{
					TestInt = 7,
					TestString = "Hello world",
				};

				store.Add(entity);
				var entityRetrieved = store.TryGet<int, PrivateTestEntity>(entity.Key);

				entityRetrieved.HasValue.Should().BeTrue();
				entityRetrieved.Value.Should().BeEquivalentTo(entity);
			});
		}

		[Table(nameof(PrivateTestEntity))]
		private class PrivateTestEntity : IKeyed<int>
		{
			[PrimaryKey, AutoIncrement]
			public int Key { get; set; }

			public int TestInt { get; set; }

			public string TestString { get; set; }
		}
	}
}
