﻿namespace HQS.Test.Utility.Stores.SQLite;

[Table(nameof(TestEntity))]
public class TestEntity : IKeyed<int>
{
	[PrimaryKey, AutoIncrement]
	public int Key { get; set; }

	public int TestInt { get; set; }

	public string TestString { get; set; }
}
