﻿namespace HQS.Test.Utility.Stores.SQLite;

[Table(nameof(InternalTestEntity))]
internal class InternalTestEntity : IKeyed<int>
{
	[PrimaryKey, AutoIncrement]
	public int Key { get; set; }

	public int TestInt { get; set; }

	public string TestString { get; set; }
}
