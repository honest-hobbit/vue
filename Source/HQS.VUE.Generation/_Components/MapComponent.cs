﻿namespace HQS.VUE.Generation;

public class MapComponent : MonoBehaviour
{
	private Map map;

	[SerializeField]
	private int cellsPerZoneSide = 1;

	public int CellsPerZoneSide
	{
		get => this.cellsPerZoneSide;
		set
		{
			this.cellsPerZoneSide = value;
			this.OnValidate();
		}
	}

	public Map Map
	{
		get
		{
			if (this.map == null)
			{
				return this.CreateAndAssignMap();
			}

			if (this.map.CellsPerZoneSide != this.cellsPerZoneSide)
			{
				return this.CreateAndAssignMap();
			}

			return this.map;
		}
	}

	private Map CreateAndAssignMap()
	{
		this.OnValidate();
		this.map = new Map(new MapPools(this.cellsPerZoneSide));
		return this.map;
	}

	private void OnValidate()
	{
		this.cellsPerZoneSide = this.cellsPerZoneSide.ClampLower(1);
	}
}
