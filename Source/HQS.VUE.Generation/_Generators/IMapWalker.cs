﻿namespace HQS.VUE.Generation;

public interface IMapWalker
{
	bool TryMove();
}
