﻿namespace HQS.VUE.Generation;

public class WalkerGenerator
{
	private List<IMapWalker> currentGeneration = new List<IMapWalker>();

	private List<IMapWalker> nextGeneration = new List<IMapWalker>();

	private int version;

	public void AddToCurrentGeneration(IMapWalker walker)
	{
		Ensure.That(walker, nameof(walker)).IsNotNull();

		this.currentGeneration.Add(walker);
	}

	public void AddToNextGeneration(IMapWalker walker)
	{
		Ensure.That(walker, nameof(walker)).IsNotNull();

		this.nextGeneration.Add(walker);
	}

	public void Clear()
	{
		this.currentGeneration.Clear();
		this.nextGeneration.Clear();
	}

	public IEnumerable<IMapWalker> Generate()
	{
		this.version++;
		return this.YieldGenerate(this.version);
	}

	private IEnumerable<IMapWalker> YieldGenerate(int version)
	{
		this.CheckVersion(version);

		do
		{
			while (this.currentGeneration.Count > 0)
			{
				// iterating backwards so walkers can be added and removed safely
				for (int i = this.currentGeneration.Count - 1; i >= 0; i--)
				{
					var walker = this.currentGeneration[i];
					if (!walker.TryMove())
					{
						this.currentGeneration.RemoveAt(i);
					}

					yield return walker;

					this.CheckVersion(version);
				}
			}

			RefUtility.Swap(ref this.currentGeneration, ref this.nextGeneration);
		}
		while (this.currentGeneration.Count > 0);
	}

	private void CheckVersion(int version)
	{
		if (version != this.version)
		{
			throw new InvalidOperationException(
				$"{nameof(this.Generate)} was called again before the previous call finished with the enumerable returned.");
		}
	}
}
