﻿namespace HQS.VUE.Generation;

public class GrowingTreeMazeGenerator
{
	private readonly List<PathCell> paths = new List<PathCell>();

	private readonly ProbabilityTable<Adjacency> linkTable = new ProbabilityTable<Adjacency>();

	private float horizontalVsVertical;

	private float newLinkVsRelink;

	private int version;

	public Func<Random, int, int> PathSelector { get; set; }

	public float HorizontalVsVertical
	{
		get => this.horizontalVsVertical;
		set => this.horizontalVsVertical = value.Clamp(0, 1);
	}

	public float NewLinkVsRelink
	{
		get => this.newLinkVsRelink;
		set => this.newLinkVsRelink = value.Clamp(0, 1);
	}

	public IEnumerable<PathCell> Generate(PathCell startFrom, Random random)
	{
		Ensure.That(startFrom, nameof(startFrom)).IsNotNull();
		Ensure.That(random, nameof(random)).IsNotNull();

		this.version++;
		return this.YieldGenerate(this.version, startFrom, random);
	}

	private IEnumerable<PathCell> YieldGenerate(int version, PathCell startFrom, Random random)
	{
		Debug.Assert(startFrom != null);
		Debug.Assert(random != null);

		this.CheckVersion(version);
		yield return startFrom;

		this.CheckVersion(version);
		this.paths.Clear();
		this.paths.Add(startFrom);

		while (this.paths.Count > 0)
		{
			int pathIndex = this.PathSelector?.Invoke(random, this.paths.Count) ?? this.paths.Count - 1;
			var path = this.paths[pathIndex];

			this.linkTable.Dynamic.Clear();
			bool newLink = random.NextChanceOutcome(this.newLinkVsRelink);
			bool horizontalLink = random.NextChanceOutcome(this.horizontalVsVertical);

			if (newLink)
			{
				if (horizontalLink)
				{
					this.AddChanceIfNonexistent(path, Adjacency.North, 1);
					this.AddChanceIfNonexistent(path, Adjacency.South, 1);
					this.AddChanceIfNonexistent(path, Adjacency.East, 1);
					this.AddChanceIfNonexistent(path, Adjacency.West, 1);
				}
				else
				{
					this.AddChanceIfNonexistent(path, Adjacency.Above, 1);
					this.AddChanceIfNonexistent(path, Adjacency.Below, 1);
				}
			}
			else
			{
				if (horizontalLink)
				{
					this.AddChanceIfExists(path, Adjacency.North, 1);
					this.AddChanceIfExists(path, Adjacency.South, 1);
					this.AddChanceIfExists(path, Adjacency.East, 1);
					this.AddChanceIfExists(path, Adjacency.West, 1);
				}
				else
				{
					this.AddChanceIfExists(path, Adjacency.Above, 1);
					this.AddChanceIfExists(path, Adjacency.Below, 1);
				}
			}

			if (this.linkTable.Dynamic.Count == 0)
			{
				// if nothing was added then revert back to linking to new paths horizontally
				this.AddChanceIfNonexistent(path, Adjacency.North, 1);
				this.AddChanceIfNonexistent(path, Adjacency.South, 1);
				this.AddChanceIfNonexistent(path, Adjacency.East, 1);
				this.AddChanceIfNonexistent(path, Adjacency.West, 1);
			}

			if (this.linkTable.Dynamic.Count == 0)
			{
				// if there is still nothing in the table then remove this path
				this.paths.RemoveAt(pathIndex);
				continue;
			}

			if (path.TryLinkTo(this.linkTable.GetValue(random), out var adjacentPath))
			{
				this.paths.Add(adjacentPath);
				yield return adjacentPath;

				this.CheckVersion(version);
			}
		}
	}

	private void AddChanceIfExists(PathCell path, Adjacency side, float chance)
	{
		Debug.Assert(path != null);

		if (path.DoesUnlinkedPathExist(side))
		{
			this.linkTable.Dynamic.Add(chance, side);
		}
	}

	private void AddChanceIfNonexistent(PathCell path, Adjacency side, float chance)
	{
		Debug.Assert(path != null);

		if (!path.DoesPathExist(side))
		{
			this.linkTable.Dynamic.Add(chance, side);
		}
	}

	private void CheckVersion(int version)
	{
		if (version != this.version)
		{
			throw new InvalidOperationException(
				$"{nameof(this.Generate)} was called again before the previous call finished with the enumerable returned.");
		}
	}
}
