﻿namespace HQS.VUE.Generation;

public class MapPools
{
	public MapPools(int cellsPerZoneSide)
	{
		Ensure.That(cellsPerZoneSide, nameof(cellsPerZoneSide)).IsGt(0);

		this.CellsPerZoneSide = cellsPerZoneSide;
		this.Zones = new PinPool<MapZone>(() => new MapZone(this.CellsPerZoneSide));
		this.PathCells = Pool.ThreadSafe.Pins.Create<PathCell>();
	}

	public int CellsPerZoneSide { get; }

	public IPinPool<MapZone> Zones { get; }

	public IPinPool<PathCell> PathCells { get; }
}
