﻿namespace HQS.VUE.Generation;

public class Map
{
	private readonly float zoneLengthFloat;

	public Map(MapPools pools)
	{
		Ensure.That(pools, nameof(pools)).IsNotNull();

		this.Pools = pools;
		this.zoneLengthFloat = pools.CellsPerZoneSide;
		this.CellsPerZoneSide = pools.CellsPerZoneSide;
		this.CellsPerZone = pools.CellsPerZoneSide * pools.CellsPerZoneSide;

		this.Changes = new MapChanges();
		this.Zones = new MapZones(this);
		this.Cells = new MapCells(this);
		this.Paths = new PathCells(this);
		this.Snapshot = new MapSnapshot(this);
	}

	public int CellsPerZoneSide { get; }

	public int CellsPerZone { get; }

	public MapZones Zones { get; }

	public MapCells Cells { get; }

	public PathCells Paths { get; }

	public MapSnapshot Snapshot { get; }

	internal MapPools Pools { get; }

	internal MapChanges Changes { get; }

	public void Clear()
	{
		this.Zones.Clear();
		this.Changes.Clear();
		this.Snapshot.Clear();
	}

	[SuppressMessage("Performance", "CA1822", Justification = "Other overloads are not static.")]
	public Index2D GetZoneKey(PathCell path)
	{
		Ensure.That(path, nameof(path)).IsNotNull();

		return path.Cell.Zone.Key;
	}

	public Index2D GetZoneKey(PathKey pathKey) => this.GetZoneKeyFromCell(pathKey.CellKey);

	public Index2D GetZoneKeyFromCell(Index2D cellKey) => new Index2D(
		Mathf.FloorToInt(cellKey.X / this.zoneLengthFloat),
		Mathf.FloorToInt(cellKey.Y / this.zoneLengthFloat));

	private Index2D GetZoneCellKey(Index2D cellKey) => new Index2D(
		MathUtility.ActualModulo(cellKey.X, this.CellsPerZoneSide),
		MathUtility.ActualModulo(cellKey.Y, this.CellsPerZoneSide));

	[SuppressMessage("Naming", "CA1710", Justification = "Not using Collection suffix.")]
	public class MapZones : IReadOnlyDictionary<Index2D, MapZone>, IIndexingBounds<Index2D>
	{
		private readonly Dictionary<Index2D, Pin<MapZone>> zones =
			new Dictionary<Index2D, Pin<MapZone>>(HQS.Utility.Indexing.Curves.MortonCurve.Comparer<Index2D>());

		private readonly Map map;

		internal MapZones(Map map)
		{
			Debug.Assert(map != null);

			this.map = map;
			this.Keys = this.zones.Keys.AsEnumerableOnly();
			this.Values = this.zones.Values.Select(x => x.Value);
		}

		public IObservable<MapZone> Changed => this.map.Changes.ZoneChanged;

		/// <inheritdoc />
		public Index2D Dimensions => this.map.Changes.ZoneBounds.Dimensions;

		/// <inheritdoc />
		public Index2D LowerBounds => this.map.Changes.ZoneBounds.LowerBounds;

		/// <inheritdoc />
		public Index2D UpperBounds => this.map.Changes.ZoneBounds.UpperBounds;

		/// <inheritdoc />
		public IEnumerable<Index2D> Keys { get; }

		/// <inheritdoc />
		public IEnumerable<MapZone> Values { get; }

		/// <inheritdoc />
		public int Count => this.zones.Count;

		internal int Version { get; private set; }

		public MapZone this[int x, int y] => this[new Index2D(x, y)];

		/// <inheritdoc />
		public MapZone this[Index2D index]
		{
			get
			{
				if (this.zones.TryGetValue(index, out var pooled))
				{
					return pooled.Value;
				}
				else
				{
					pooled = this.map.Pools.Zones.Rent();
					var zone = pooled.Value;
					zone.Initialize(this.map, index);
					this.zones[index] = pooled;
					this.Version++;
					return zone;
				}
			}
		}

		public bool ContainsKey(int x, int y) => this.ContainsKey(new Index2D(x, y));

		public bool TryGetValue(int x, int y, out MapZone value) => this.TryGetValue(new Index2D(x, y), out value);

		/// <inheritdoc />
		public bool ContainsKey(Index2D key) => this.zones.ContainsKey(key);

		/// <inheritdoc />
		public bool TryGetValue(Index2D key, out MapZone value)
		{
			if (this.zones.TryGetValue(key, out var pooled))
			{
				value = pooled.Value;
				return true;
			}
			else
			{
				value = null;
				return false;
			}
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<Index2D, MapZone>> GetEnumerator() =>
			this.zones.Select(x => KeyValuePair.Create(x.Key, x.Value.Value)).GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		internal void Remove(Index2D key)
		{
			if (this.zones.TryRemove(key, out var zone))
			{
				zone.Dispose();
				this.Version++;
			}
		}

		internal void Clear()
		{
			foreach (var zone in this.zones.Values)
			{
				zone.Value.Clear(true);
				zone.Dispose();
			}

			this.zones.Clear();
			this.Version = 0;
		}
	}

	[SuppressMessage("Naming", "CA1710", Justification = "Not using Collection suffix.")]
	public class MapCells : IReadOnlyDictionary<Index2D, MapCell>, IIndexingBounds<Index2D>
	{
		private readonly Map map;

		internal MapCells(Map map)
		{
			Debug.Assert(map != null);

			this.map = map;
		}

		public IObservable<MapCell> Changed => this.map.Changes.CellChanged;

		/// <inheritdoc />
		public Index2D Dimensions => this.map.Changes.CellBounds.Dimensions;

		/// <inheritdoc />
		public Index2D LowerBounds => this.map.Changes.CellBounds.LowerBounds;

		/// <inheritdoc />
		public Index2D UpperBounds => this.map.Changes.CellBounds.UpperBounds;

		public int MinElevation => this.map.Changes.CellBounds.MinElevation;

		public int MaxElevation => this.map.Changes.CellBounds.MaxElevation;

		public int ElevationsCount => this.map.Changes.CellBounds.ElevationsCount;

		/// <inheritdoc />
		public IEnumerable<Index2D> Keys => this.Select(x => x.Key);

		/// <inheritdoc />
		public IEnumerable<MapCell> Values => this.Select(x => x.Value);

		/// <inheritdoc />
		public int Count => this.map.Zones.Count * this.map.CellsPerZone;

		public MapCell this[int x, int y] => this[new Index2D(x, y)];

		/// <inheritdoc />
		public MapCell this[Index2D key] => this.map.Zones[this.map.GetZoneKeyFromCell(key)][this.map.GetZoneCellKey(key)];

		public bool ContainsKey(int x, int y) => this.ContainsKey(new Index2D(x, y));

		public bool TryGetValue(int x, int y, out MapCell value) => this.TryGetValue(new Index2D(x, y), out value);

		/// <inheritdoc />
		public bool ContainsKey(Index2D key) => this.map.Zones.ContainsKey(this.map.GetZoneKeyFromCell(key));

		/// <inheritdoc />
		public bool TryGetValue(Index2D key, out MapCell cell)
		{
			if (this.map.Zones.TryGetValue(this.map.GetZoneKeyFromCell(key), out var zone))
			{
				cell = zone[this.map.GetZoneCellKey(key)];
				return true;
			}
			else
			{
				cell = null;
				return false;
			}
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<Index2D, MapCell>> GetEnumerator()
		{
			int cellsPerZoneSide = this.map.CellsPerZoneSide;
			foreach (var zone in this.map.Zones.Values)
			{
				for (int iX = 0; iX < cellsPerZoneSide; iX++)
				{
					for (int iY = 0; iY < cellsPerZoneSide; iY++)
					{
						var cell = zone[iX, iY];
						yield return new KeyValuePair<Index2D, MapCell>(cell.Key, cell);
					}
				}
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	[SuppressMessage("Naming", "CA1710", Justification = "Not using Collection suffix.")]
	public class PathCells : IReadOnlyDictionary<PathKey, PathCell>, IIndexingBounds<Index2D>
	{
		private readonly Map map;

		internal PathCells(Map map)
		{
			Debug.Assert(map != null);

			this.map = map;
		}

		public IObservable<PathCell> Added => this.map.Changes.PathAdded;

		public IObservable<PathCell> Changed => this.map.Changes.PathChanged;

		public IObservable<PathKey> Removed => this.map.Changes.PathRemoved;

		/// <inheritdoc />
		public Index2D Dimensions => this.map.Changes.PathBounds.Dimensions;

		/// <inheritdoc />
		public Index2D LowerBounds => this.map.Changes.PathBounds.LowerBounds;

		/// <inheritdoc />
		public Index2D UpperBounds => this.map.Changes.PathBounds.UpperBounds;

		public int MinElevation => this.map.Changes.PathBounds.MinElevation;

		public int MaxElevation => this.map.Changes.PathBounds.MaxElevation;

		public int ElevationsCount => this.map.Changes.PathBounds.ElevationsCount;

		/// <inheritdoc />
		public IEnumerable<PathKey> Keys => this.Select(x => x.Key);

		/// <inheritdoc />
		public IEnumerable<PathCell> Values => this.Select(x => x.Value);

		/// <inheritdoc />
		public int Count => this.map.Changes.PathsCount;

		public PathCell this[int x, int y, int elevation] => this[new PathKey(x, y, elevation)];

		/// <inheritdoc />
		public PathCell this[PathKey key] => this.map.Cells[key.CellKey].GetOrAdd(key.Elevation);

		public bool ContainsKey(int x, int y, int elevation) =>
			this.ContainsKey(new PathKey(x, y, elevation));

		public bool TryGetValue(int x, int y, int elevation, out PathCell value) =>
			this.TryGetValue(new PathKey(x, y, elevation), out value);

		/// <inheritdoc />
		public bool ContainsKey(PathKey key) =>
			this.map.Cells.TryGetValue(key.CellKey, out var cell) && cell.ContainsKey(key.Elevation);

		/// <inheritdoc />
		public bool TryGetValue(PathKey key, out PathCell cell)
		{
			cell = null;
			return this.map.Cells.TryGetValue(key.CellKey, out var mapCell)
				&& mapCell.TryGetValue(key.Elevation, out cell);
		}

		/// <inheritdoc />
		public IEnumerator<KeyValuePair<PathKey, PathCell>> GetEnumerator()
		{
			foreach (var pair in this.map.Cells)
			{
				var paths = pair.Value.Values;
				int max = paths.Count;
				for (int i = 0; i < max; i++)
				{
					var path = paths[i];
					yield return new KeyValuePair<PathKey, PathCell>(
						new PathKey(pair.Key, path.Elevation), path);
				}
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}

	public class MapSnapshot
	{
		private readonly List<MapZone> zonesSnapshot = new List<MapZone>();

		private readonly List<PathCell> pathsSnapshot = new List<PathCell>();

		private readonly Map map;

		private int zonesVersion;

		private int pathsVersion;

		internal MapSnapshot(Map map)
		{
			Debug.Assert(map != null);

			this.map = map;
			this.Zones = this.zonesSnapshot.Where(x => x.Map == this.map);
			this.Cells = this.EnumerateCells();
			this.Paths = this.pathsSnapshot.Where(x => x.Cell?.Zone.Map == this.map);
		}

		public IEnumerable<MapZone> Zones { get; }

		public IEnumerable<MapCell> Cells { get; }

		public IEnumerable<PathCell> Paths { get; }

		public void SetSnapshot()
		{
			int version = this.map.Zones.Version;
			if (this.zonesVersion != version)
			{
				this.zonesVersion = version;
				this.zonesSnapshot.Clear();
				this.zonesSnapshot.AddRange(this.map.Zones.Values);
			}

			version = this.map.Changes.PathsVersion;
			if (this.pathsVersion != version)
			{
				this.pathsVersion = version;
				this.pathsSnapshot.Clear();
				this.pathsSnapshot.AddRange(this.map.Paths.Values);
			}
		}

		internal void Clear()
		{
			this.zonesVersion = 0;
			this.pathsVersion = 0;
			this.zonesSnapshot.Clear();
			this.pathsSnapshot.Clear();
		}

		private IEnumerable<MapCell> EnumerateCells()
		{
			int cellsPerZoneSide = this.map.CellsPerZoneSide;
			int max = this.zonesSnapshot.Count;

			for (int i = 0; i < max; i++)
			{
				var zone = this.zonesSnapshot[i];
				if (zone.Map == this.map)
				{
					for (int iX = 0; iX < cellsPerZoneSide; iX++)
					{
						for (int iY = 0; iY < cellsPerZoneSide; iY++)
						{
							yield return zone[iX, iY];
						}
					}
				}
			}
		}
	}
}
