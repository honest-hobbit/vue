﻿namespace HQS.VUE.Generation;

public readonly struct PathKey : IEquatable<PathKey>
{
	public PathKey(int x, int y, int elevation)
		: this(new Index2D(x, y), elevation)
	{
	}

	public PathKey(Index2D cellKey, int elevation)
	{
		this.CellKey = cellKey;
		this.Elevation = elevation;
	}

	public Index2D CellKey { get; }

	public int Elevation { get; }

	public static bool operator ==(PathKey lhs, PathKey rhs) => lhs.Equals(rhs);

	public static bool operator !=(PathKey lhs, PathKey rhs) => !lhs.Equals(rhs);

	/// <inheritdoc />
	public bool Equals(PathKey other) => this.CellKey == other.CellKey && this.Elevation == other.Elevation;

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public override int GetHashCode() => HashCode.Combine(this.Elevation, this.CellKey);

	/// <inheritdoc />
	public override string ToString() => $"[{this.CellKey}: {this.Elevation}]";
}
