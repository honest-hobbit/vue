﻿namespace HQS.VUE.Generation;

[SuppressMessage("Naming", "CA1710", Justification = "Not using Collection suffix.")]
public class MapCell : IKeyed<Index2D>, IReadOnlyDictionary<int, PathCell>
{
	private readonly SortedList<int, Pin<PathCell>> paths = new SortedList<int, Pin<PathCell>>();

	private int elevation;

	internal MapCell(MapZone zone, Index2D offset)
	{
		Debug.Assert(zone != null);

		this.Zone = zone;
		this.Offset = offset;
		this.Keys = ReadOnlyList.From(this.paths.Keys);
		this.Values = ReadOnlyList.Convert(this.paths.Values, x => x.Value);
	}

	public MapZone Zone { get; }

	/// <inheritdoc />
	public Index2D Key => this.Offset + this.Zone.Offset;

	public Index2D Offset { get; }

	public int Elevation
	{
		get => this.elevation;
		set
		{
			this.ValidateIsPartOfMap(nameof(this.Elevation));

			this.elevation = value;
			this.Zone.Map.Changes.OnChanged(this);
		}
	}

	public MapCell North
	{
		get
		{
			int y = this.Offset.Y + 1;
			return y < this.Zone.SideLength ?
				this.Zone[this.Offset.X, y] :
				this.Zone.North?[this.Offset.X, 0];
		}
	}

	public MapCell South
	{
		get
		{
			int y = this.Offset.Y - 1;
			return y >= 0 ?
				this.Zone[this.Offset.X, y] :
				this.Zone.South?[this.Offset.X, this.Zone.MaxIndex];
		}
	}

	public MapCell East
	{
		get
		{
			int x = this.Offset.X + 1;
			return x < this.Zone.SideLength ?
				this.Zone[x, this.Offset.Y] :
				this.Zone.East?[0, this.Offset.Y];
		}
	}

	public MapCell West
	{
		get
		{
			int x = this.Offset.X - 1;
			return x >= 0 ?
				this.Zone[x, this.Offset.Y] :
				this.Zone.West?[this.Zone.MaxIndex, this.Offset.Y];
		}
	}

	public IReadOnlyList<int> Keys { get; }

	public IReadOnlyList<PathCell> Values { get; }

	/// <inheritdoc />
	IEnumerable<int> IReadOnlyDictionary<int, PathCell>.Keys => this.Keys;

	/// <inheritdoc />
	IEnumerable<PathCell> IReadOnlyDictionary<int, PathCell>.Values => this.Values;

	/// <inheritdoc />
	public int Count => this.paths.Count;

	/// <inheritdoc />
	public PathCell this[int elevation] => this.paths[elevation].Value;

	public MapCell this[Adjacency side] => side switch
	{
		Adjacency.North => this.North,
		Adjacency.East => this.East,
		Adjacency.South => this.South,
		Adjacency.West => this.West,
		Adjacency.Above => this,
		Adjacency.Below => this,
		_ => throw InvalidEnumArgument.CreateException(nameof(side), side),
	};

	/// <inheritdoc />
	public bool ContainsKey(int elevation) => this.paths.ContainsKey(elevation);

	/// <inheritdoc />
	public bool TryGetValue(int elevation, out PathCell path)
	{
		if (this.paths.TryGetValue(elevation, out var pooled))
		{
			path = pooled.Value;
			return true;
		}
		else
		{
			path = null;
			return false;
		}
	}

	/// <inheritdoc />
	public IEnumerator<KeyValuePair<int, PathCell>> GetEnumerator() =>
		this.paths.Select(x => KeyValuePair.Create(x.Key, x.Value.Value)).GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	public PathCell GetOrAdd(int elevation) => this.GetOrAdd(elevation, out _);

	public PathCell GetOrAdd(int elevation, out bool added)
	{
		this.ValidateIsPartOfMap(nameof(this.GetOrAdd));

		if (this.paths.TryGetValue(elevation, out var pooled))
		{
			var path = pooled.Value;
			Debug.Assert(path.Elevation == elevation);
			added = false;
			return path;
		}
		else
		{
			var map = this.Zone.Map;
			pooled = map.Pools.PathCells.Rent();
			this.paths[elevation] = pooled;
			var path = pooled.Value;
			path.Cell = this;
			path.Elevation = elevation;
			this.Zone.PathsCount++;
			map.Changes.OnAdded(path);
			added = true;
			return path;
		}
	}

	public void Clear()
	{
		var map = this.Zone.Map;
		if (map != null)
		{
			this.Clear(map, false);
		}
	}

	internal void Clear(Map map, bool removeAll)
	{
		Debug.Assert(map != null);

		// iterating backwards so paths can be removed safely
		var pathsList = this.paths.Values;
		for (int i = pathsList.Count - 1; i >= 0; i--)
		{
			var path = pathsList[i];
			path.Value.Clear(map, removeAll);
			path.Dispose();
		}

		this.paths.Clear();
		this.Elevation = 0;
	}

	internal void Remove(int elevation)
	{
		if (this.paths.Remove(elevation))
		{
			this.Zone.PathsCount--;
			this.Zone.Map.Changes.OnRemoved(new PathKey(this.Key, elevation), this);
		}
	}

	private void ValidateIsPartOfMap(string methodName)
	{
		if (this.Zone.Map == null)
		{
			MapUtility.ThrowNotPartOfMapException(typeof(MapCell), methodName);
		}
	}
}
