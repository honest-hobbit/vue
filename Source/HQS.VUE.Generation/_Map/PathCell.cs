﻿namespace HQS.VUE.Generation;

public class PathCell : IKeyed<PathKey>
{
	private const int CountOfSides = 6;

	public MapCell Cell { get; internal set; }

	/// <inheritdoc />
	public PathKey Key => new PathKey(this.Cell?.Key ?? Index2D.Zero, this.Elevation);

	public int Elevation { get; internal set; }

	public bool IsUnderGround => this.Elevation < this.Cell?.Elevation;

	public bool IsOnGround => this.Elevation == this.Cell?.Elevation;

	public bool IsAboveGround => this.Elevation > this.Cell?.Elevation;

	public int CountLinks => this.CountHorizontalLinks + this.CountVerticalLinks;

	public int CountHorizontalLinks =>
		((this.North != null) ? 1 : 0) +
		((this.East != null) ? 1 : 0) +
		((this.South != null) ? 1 : 0) +
		((this.West != null) ? 1 : 0);

	public int CountVerticalLinks => ((this.Above != null) ? 1 : 0) + ((this.Below != null) ? 1 : 0);

	public bool IsLinked => this.IsLinkedHorizontally || this.IsLinkedVertically;

	public bool IsLinkedHorizontally =>
		this.North != null || this.East != null || this.South != null || this.West != null;

	public bool IsLinkedVertically => this.Above != null || this.Below != null;

	public bool IsFullyLinked => this.IsFullyLinkedVertically && this.IsFullyLinkedHorizontally;

	public bool IsFullyLinkedHorizontally =>
		this.North != null && this.East != null && this.South != null && this.West != null;

	public bool IsFullyLinkedVertically => this.Above != null && this.Below != null;

	public PathCell North { get; private set; }

	public PathCell East { get; private set; }

	public PathCell South { get; private set; }

	public PathCell West { get; private set; }

	public PathCell Above { get; private set; }

	public PathCell Below { get; private set; }

	public PathCell this[Adjacency side]
	{
		get => side switch
		{
			Adjacency.North => this.North,
			Adjacency.East => this.East,
			Adjacency.South => this.South,
			Adjacency.West => this.West,
			Adjacency.Above => this.Above,
			Adjacency.Below => this.Below,
			_ => throw InvalidEnumArgument.CreateException(nameof(side), side),
		};

		private set
		{
			switch (side)
			{
				case Adjacency.North: this.North = value; break;
				case Adjacency.East: this.East = value; break;
				case Adjacency.South: this.South = value; break;
				case Adjacency.West: this.West = value; break;
				case Adjacency.Above: this.Above = value; break;
				case Adjacency.Below: this.Below = value; break;
			}
		}
	}

	public bool TryLinkTo(Adjacency side) => this.TryLinkTo(side, out _);

	public bool TryLinkTo(Adjacency side, out PathCell adjacentPath)
	{
		this.ValidateIsPartOfMap(nameof(this.TryLinkTo));

		adjacentPath = this[side];
		if (adjacentPath != null)
		{
			adjacentPath = null;
			return false;
		}

		adjacentPath = this.Cell[side].GetOrAdd(this.GetAdjacentElevation(side));
		this.LinkPathsAndRaisePathChanged(adjacentPath, side);
		return true;
	}

	public bool TryLinkToIfExists(Adjacency side) => this.TryLinkToIfExists(side, out _);

	public bool TryLinkToIfExists(Adjacency side, out PathCell adjacentPath)
	{
		this.ValidateIsPartOfMap(nameof(this.TryLinkToIfExists));

		adjacentPath = this[side];
		if (adjacentPath != null)
		{
			adjacentPath = null;
			return false;
		}

		if (this.Cell[side].TryGetValue(this.GetAdjacentElevation(side), out adjacentPath))
		{
			this.LinkPathsAndRaisePathChanged(adjacentPath, side);
			return true;
		}

		adjacentPath = null;
		return false;
	}

	public bool TryLinkToIfNonexistent(Adjacency side) => this.TryLinkToIfNonexistent(side, out _);

	public bool TryLinkToIfNonexistent(Adjacency side, out PathCell adjacentPath)
	{
		this.ValidateIsPartOfMap(nameof(this.TryLinkToIfNonexistent));

		adjacentPath = this[side];
		if (adjacentPath != null)
		{
			adjacentPath = null;
			return false;
		}

		adjacentPath = this.Cell[side].GetOrAdd(this.GetAdjacentElevation(side), out bool added);
		if (!added)
		{
			adjacentPath = null;
			return false;
		}

		this.LinkPathsAndRaisePathChanged(adjacentPath, side);
		return true;
	}

	public bool TryUnlinkFrom(Adjacency side) => this.TryUnlinkFrom(side, out _);

	public bool TryUnlinkFrom(Adjacency side, out PathCell adjacentPath)
	{
		this.ValidateIsPartOfMap(nameof(this.TryUnlinkFrom));

		adjacentPath = this[side];
		if (adjacentPath == null)
		{
			return false;
		}

		Debug.Assert(adjacentPath[side.GetOpposite()] == this);
		Debug.Assert(this[side] == adjacentPath);

		adjacentPath[side.GetOpposite()] = null;
		this[side] = null;
		this.RaisePathUpdated();
		adjacentPath.RaisePathUpdated();
		return true;
	}

	public void LinkToAll(bool horizontal = true, bool vertical = true)
	{
		if (horizontal)
		{
			this.TryLinkTo(Adjacency.North);
			this.TryLinkTo(Adjacency.East);
			this.TryLinkTo(Adjacency.South);
			this.TryLinkTo(Adjacency.West);
		}

		if (vertical)
		{
			this.TryLinkTo(Adjacency.Above);
			this.TryLinkTo(Adjacency.Below);
		}
	}

	public void LinkToAllThatExist(bool horizontal = true, bool vertical = true)
	{
		if (horizontal)
		{
			this.TryLinkToIfExists(Adjacency.North);
			this.TryLinkToIfExists(Adjacency.East);
			this.TryLinkToIfExists(Adjacency.South);
			this.TryLinkToIfExists(Adjacency.West);
		}

		if (vertical)
		{
			this.TryLinkToIfExists(Adjacency.Above);
			this.TryLinkToIfExists(Adjacency.Below);
		}
	}

	public void LinkToAllNonexistent(bool horizontal = true, bool vertical = true)
	{
		if (horizontal)
		{
			this.TryLinkToIfNonexistent(Adjacency.North);
			this.TryLinkToIfNonexistent(Adjacency.East);
			this.TryLinkToIfNonexistent(Adjacency.South);
			this.TryLinkToIfNonexistent(Adjacency.West);
		}

		if (vertical)
		{
			this.TryLinkToIfNonexistent(Adjacency.Above);
			this.TryLinkToIfNonexistent(Adjacency.Below);
		}
	}

	public void UnlinkFromAll(bool horizontal = true, bool vertical = true)
	{
		if (horizontal)
		{
			this.TryUnlinkFrom(Adjacency.North);
			this.TryUnlinkFrom(Adjacency.East);
			this.TryUnlinkFrom(Adjacency.South);
			this.TryUnlinkFrom(Adjacency.West);
		}

		if (vertical)
		{
			this.TryUnlinkFrom(Adjacency.Above);
			this.TryUnlinkFrom(Adjacency.Below);
		}
	}

	public bool DoesPathExist(Adjacency side)
	{
		this.ValidateIsPartOfMap(nameof(this.DoesPathExist));

		return this.Cell[side].ContainsKey(this.GetAdjacentElevation(side));
	}

	public bool DoesUnlinkedPathExist(Adjacency side)
	{
		this.ValidateIsPartOfMap(nameof(this.DoesPathExist));

		return this[side] == null && this.Cell[side].ContainsKey(this.GetAdjacentElevation(side));
	}

	public bool TryGetUnlinkedPathIfExists(Adjacency side, out PathCell adjacentPath)
	{
		this.ValidateIsPartOfMap(nameof(this.TryGetUnlinkedPathIfExists));

		adjacentPath = null;
		return this[side] == null && this.Cell[side].TryGetValue(this.GetAdjacentElevation(side), out adjacentPath);
	}

	public void GetUnlinkedSides(List<Adjacency> result, bool horizontal = true, bool vertical = true)
	{
		this.ValidateIsPartOfMap(nameof(this.GetUnlinkedSides));
		Ensure.That(result, nameof(result)).IsNotNull();

		if (horizontal)
		{
			CheckSide(Adjacency.North);
			CheckSide(Adjacency.East);
			CheckSide(Adjacency.South);
			CheckSide(Adjacency.West);
		}

		if (vertical)
		{
			CheckSide(Adjacency.Above);
			CheckSide(Adjacency.Below);
		}

		void CheckSide(Adjacency side)
		{
			if (this[side] == null)
			{
				result.Add(side);
			}
		}
	}

	public void GetUnlinkedSidesThatExist(List<Adjacency> result, bool horizontal = true, bool vertical = true)
	{
		this.ValidateIsPartOfMap(nameof(this.GetUnlinkedSidesThatExist));
		Ensure.That(result, nameof(result)).IsNotNull();

		if (horizontal)
		{
			CheckSide(Adjacency.North);
			CheckSide(Adjacency.East);
			CheckSide(Adjacency.South);
			CheckSide(Adjacency.West);
		}

		if (vertical)
		{
			CheckSide(Adjacency.Above);
			CheckSide(Adjacency.Below);
		}

		void CheckSide(Adjacency side)
		{
			if (this[side] == null && this.Cell[side].ContainsKey(this.GetAdjacentElevation(side)))
			{
				result.Add(side);
			}
		}
	}

	public void GetNonexistentSides(List<Adjacency> result, bool horizontal = true, bool vertical = true)
	{
		this.ValidateIsPartOfMap(nameof(this.GetNonexistentSides));
		Ensure.That(result, nameof(result)).IsNotNull();

		if (horizontal)
		{
			CheckSide(Adjacency.North);
			CheckSide(Adjacency.East);
			CheckSide(Adjacency.South);
			CheckSide(Adjacency.West);
		}

		if (vertical)
		{
			CheckSide(Adjacency.Above);
			CheckSide(Adjacency.Below);
		}

		void CheckSide(Adjacency side)
		{
			// don't need to check that the side is unlinked because it can't be linked if it doesn't exist
			if (!this.Cell[side].ContainsKey(this.GetAdjacentElevation(side)))
			{
				result.Add(side);
			}
		}
	}

	public void Remove()
	{
		if (this.Cell != null)
		{
			this.Clear(this.Cell.Zone.Map, false);
		}
	}

	internal void Clear(Map map, bool removeAll)
	{
		Debug.Assert(map != null);

		if (removeAll)
		{
			this.North = null;
			this.South = null;
			this.East = null;
			this.West = null;
			this.Above = null;
			this.Below = null;
		}
		else
		{
			for (int side = 0; side < CountOfSides; side++)
			{
				if (this[(Adjacency)side] != null)
				{
					this[(Adjacency)side][((Adjacency)side).GetOpposite()] = null;
					this[(Adjacency)side] = null;
				}
			}

			this.Cell.Remove(this.Elevation);
		}

		this.Cell = null;
		this.Elevation = 0;
	}

	private void ValidateIsPartOfMap(string methodName)
	{
		if (this.Cell == null)
		{
			MapUtility.ThrowNotPartOfMapException(typeof(PathCell), methodName);
		}
	}

	private int GetAdjacentElevation(Adjacency side) => this.Elevation + side.GetElevationOffset();

	private void LinkPathsAndRaisePathChanged(PathCell adjacentPath, Adjacency side)
	{
		Debug.Assert(adjacentPath[side.GetOpposite()] == null);
		Debug.Assert(this[side] == null);

		adjacentPath[side.GetOpposite()] = this;
		this[side] = adjacentPath;
		this.RaisePathUpdated();
		adjacentPath.RaisePathUpdated();
	}

	private void RaisePathUpdated() => this.Cell.Zone.Map.Changes.OnChanged(this);
}
