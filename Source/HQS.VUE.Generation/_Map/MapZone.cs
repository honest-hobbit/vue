﻿namespace HQS.VUE.Generation;

public class MapZone : IKeyed<Index2D>, IIndexableView<Index2D, MapCell>
{
	private readonly MapCell[,] cells;

	private MapZone north;

	private MapZone east;

	private MapZone south;

	private MapZone west;

	private IEnumerable<IndexValuePair<Index2D, MapCell>> enumerable;

	internal MapZone(int length)
	{
		Debug.Assert(length > 0);

		this.cells = new MapCell[length, length];

		for (int iX = 0; iX < length; iX++)
		{
			for (int iY = 0; iY < length; iY++)
			{
				this.cells[iX, iY] = new MapCell(this, new Index2D(iX, iY));
			}
		}
	}

	public Map Map { get; private set; }

	/// <inheritdoc />
	public Index2D Key { get; private set; }

	public Index2D Offset { get; private set; }

	public MapZone North => this.north ?? (this.north = this.Map?.Zones[this.Key + Index2D.UnitY]);

	public MapZone South => this.south ?? (this.south = this.Map?.Zones[this.Key - Index2D.UnitY]);

	public MapZone East => this.east ?? (this.east = this.Map?.Zones[this.Key + Index2D.UnitX]);

	public MapZone West => this.west ?? (this.west = this.Map?.Zones[this.Key - Index2D.UnitX]);

	public int PathsCount { get; internal set; }

	public int SideLength => this.cells.GetLength(0);

	/// <inheritdoc />
	public Index2D Dimensions => new Index2D(this.SideLength);

	/// <inheritdoc />
	public Index2D LowerBounds => Index2D.Zero;

	/// <inheritdoc />
	public Index2D UpperBounds => new Index2D(this.SideLength - 1);

	internal int MaxIndex => this.cells.GetUpperBound(0);

	public MapCell this[int x, int y] => this.cells[x, y];

	/// <inheritdoc />
	public MapCell this[Index2D index] => this.cells[index.X, index.Y];

	public MapZone this[Adjacency side] => side switch
	{
		Adjacency.North => this.North,
		Adjacency.East => this.East,
		Adjacency.South => this.South,
		Adjacency.West => this.West,
		Adjacency.Above => this,
		Adjacency.Below => this,
		_ => throw InvalidEnumArgument.CreateException(nameof(side), side),
	};

	public void Clear()
	{
		if (this.Map != null)
		{
			this.Clear(false);
		}
	}

	public IEnumerable<PathCell> GetPaths()
	{
		int sideLength = this.SideLength;
		for (int iX = 0; iX < sideLength; iX++)
		{
			for (int iY = 0; iY < sideLength; iY++)
			{
				var paths = this.cells[iX, iY].Values;
				int max = paths.Count;
				for (int i = 0; i < max; i++)
				{
					yield return paths[i];
				}
			}
		}
	}

	/// <inheritdoc />
	public IEnumerator<IndexValuePair<Index2D, MapCell>> GetEnumerator()
	{
		if (this.enumerable == null)
		{
			this.enumerable = this.cells.GetIndexValuePairs();
		}

		return this.enumerable.GetEnumerator();
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	internal void Initialize(Map map, Index2D key)
	{
		Debug.Assert(this.Map == null);
		Debug.Assert(map != null);

		this.Map = map;
		this.Key = key;
		this.Offset = key * map.CellsPerZoneSide;
	}

	internal void Clear(bool removeAll)
	{
		Debug.Assert(this.Map != null);

		var map = this.Map;
		int length = map.CellsPerZoneSide;

		for (int iX = 0; iX < length; iX++)
		{
			for (int iY = 0; iY < length; iY++)
			{
				this.cells[iX, iY].Clear(map, removeAll);
			}
		}

		if (removeAll)
		{
			this.north = null;
			this.south = null;
			this.east = null;
			this.west = null;
		}
		else
		{
			if (this.north != null)
			{
				this.north.south = null;
				this.north = null;
			}

			if (this.south != null)
			{
				this.south.north = null;
				this.south = null;
			}

			if (this.east != null)
			{
				this.east.west = null;
				this.east = null;
			}

			if (this.west != null)
			{
				this.west.east = null;
				this.west = null;
			}

			map.Zones.Remove(this.Key);
		}

		this.PathsCount = 0;
		this.Map = null;
		this.Key = Index2D.Zero;
	}
}
