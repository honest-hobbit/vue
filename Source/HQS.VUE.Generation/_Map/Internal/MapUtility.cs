﻿namespace HQS.VUE.Generation;

internal static class MapUtility
{
	public static void ThrowNotPartOfMapException(Type type, string methodName)
	{
		Debug.Assert(type != null);
		Debug.Assert(!methodName.IsNullOrWhiteSpace());

		throw new InvalidOperationException(
			$"Can't call {methodName} until this {type.Name} is part of a {nameof(Map)}.");
	}
}
