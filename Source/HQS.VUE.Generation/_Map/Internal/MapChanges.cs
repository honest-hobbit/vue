﻿namespace HQS.VUE.Generation;

[SuppressMessage("Design", "CA1001", Justification = "Subjects don't need to be disposed maybe?")]
internal class MapChanges
{
	private readonly Subject<PathCell> pathAdded = new Subject<PathCell>();

	private readonly Subject<PathCell> pathChanged = new Subject<PathCell>();

	private readonly Subject<PathKey> pathRemoved = new Subject<PathKey>();

	private readonly Subject<MapCell> cellChanged = new Subject<MapCell>();

	private readonly Subject<MapZone> zoneChanged = new Subject<MapZone>();

	public MapChanges()
	{
		this.PathAdded = this.pathAdded.AsObservableOnly();
		this.PathChanged = this.pathChanged.AsObservableOnly();
		this.PathRemoved = this.pathRemoved.AsObservableOnly();
		this.CellChanged = this.cellChanged.AsObservableOnly();
		this.ZoneChanged = this.zoneChanged.AsObservableOnly();
		this.Clear();
	}

	public IObservable<PathCell> PathAdded { get; }

	public IObservable<PathCell> PathChanged { get; }

	public IObservable<PathKey> PathRemoved { get; }

	public IObservable<MapCell> CellChanged { get; }

	public IObservable<MapZone> ZoneChanged { get; }

	public Bounds ZoneBounds { get; } = new Bounds();

	public Bounds CellBounds { get; } = new Bounds();

	public Bounds PathBounds { get; } = new Bounds();

	public int PathsVersion { get; private set; }

	public int PathsCount { get; private set; }

	public void OnCompleted()
	{
		this.pathAdded.OnCompleted();
		this.pathChanged.OnCompleted();
		this.pathRemoved.OnCompleted();
		this.cellChanged.OnCompleted();
		this.zoneChanged.OnCompleted();
	}

	public void OnAdded(PathCell path)
	{
		Debug.Assert(path != null);

		this.PathsVersion++;
		this.PathsCount++;
		this.UpdateBounds(path);
		this.pathAdded.OnNext(path);
		this.cellChanged.OnNext(path.Cell);
		this.zoneChanged.OnNext(path.Cell.Zone);
	}

	public void OnRemoved(PathKey key, MapCell cell)
	{
		Debug.Assert(cell != null);

		this.PathsVersion++;
		this.PathsCount--;
		this.pathRemoved.OnNext(key);
		this.cellChanged.OnNext(cell);
		this.zoneChanged.OnNext(cell.Zone);
	}

	public void OnChanged(PathCell path)
	{
		Debug.Assert(path != null);

		this.pathChanged.OnNext(path);
		this.cellChanged.OnNext(path.Cell);
		this.zoneChanged.OnNext(path.Cell.Zone);
	}

	public void OnChanged(MapCell cell)
	{
		Debug.Assert(cell != null);

		this.UpdateBounds(cell);
		this.cellChanged.OnNext(cell);
		this.zoneChanged.OnNext(cell.Zone);
	}

	public void Clear()
	{
		this.ZoneBounds.Clear();
		this.CellBounds.Clear();
		this.PathBounds.Clear();
		this.PathsVersion = 0;
		this.PathsCount = 0;
	}

	private void UpdateBounds(PathCell path)
	{
		Debug.Assert(path != null);

		this.PathBounds.UpdateBounds(path.Cell.Key, path.Elevation);
		this.UpdateBounds(path.Cell);
	}

	private void UpdateBounds(MapCell cell)
	{
		Debug.Assert(cell != null);

		this.CellBounds.UpdateBounds(cell.Key, cell.Elevation);
		this.ZoneBounds.UpdateBounds(cell.Zone.Key);
	}

	public class Bounds : IIndexingBounds<Index2D>
	{
		private bool hasBounds;

		private MinMaxInt x;

		private MinMaxInt y;

		private MinMaxInt elevation;

		public Bounds()
		{
			this.Clear();
		}

		/// <inheritdoc />
		public Index2D Dimensions => new Index2D(this.x.Range + 1, this.y.Range + 1);

		/// <inheritdoc />
		public Index2D LowerBounds => new Index2D(this.x.Min, this.y.Min);

		/// <inheritdoc />
		public Index2D UpperBounds => new Index2D(this.x.Max, this.y.Max);

		public int MinElevation => this.elevation.Min;

		public int MaxElevation => this.elevation.Max;

		public int ElevationsCount => this.MaxElevation - this.MinElevation + 1;

		public void UpdateBounds(Index2D key, int elevation)
		{
			if (this.hasBounds)
			{
				this.x = this.x.Encompass(key.X);
				this.y = this.y.Encompass(key.Y);
				this.elevation = this.elevation.Encompass(elevation);
			}
			else
			{
				this.x = new MinMaxInt(key.X, key.X);
				this.y = new MinMaxInt(key.Y, key.Y);
				this.elevation = new MinMaxInt(elevation, elevation);
				this.hasBounds = true;
			}
		}

		public void UpdateBounds(Index2D key)
		{
			if (this.hasBounds)
			{
				this.x = this.x.Encompass(key.X);
				this.y = this.y.Encompass(key.Y);
			}
			else
			{
				this.x = new MinMaxInt(key.X, key.X);
				this.y = new MinMaxInt(key.Y, key.Y);
				this.hasBounds = true;
			}
		}

		public void Clear()
		{
			this.hasBounds = false;
			this.x = new MinMaxInt(0, -1);
			this.y = new MinMaxInt(0, -1);
			this.elevation = new MinMaxInt(0, -1);
		}
	}
}
