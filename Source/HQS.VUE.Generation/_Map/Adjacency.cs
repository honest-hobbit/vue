﻿namespace HQS.VUE.Generation;

public enum Adjacency
{
	North,
	East,
	South,
	West,
	Above,
	Below,
}
