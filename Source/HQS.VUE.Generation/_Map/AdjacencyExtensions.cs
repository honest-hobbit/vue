﻿namespace HQS.VUE.Generation;

public static class AdjacencyExtensions
{
	private const int CountOfSides = 6;

	private static readonly Adjacency[] Opposites = new Adjacency[CountOfSides];

	private static readonly Index2D[] HorizontalOffset = new Index2D[CountOfSides];

	private static readonly int[] ElevationOffset = new int[CountOfSides];

	static AdjacencyExtensions()
	{
		Opposites[(int)Adjacency.North] = Adjacency.South;
		Opposites[(int)Adjacency.South] = Adjacency.North;
		Opposites[(int)Adjacency.East] = Adjacency.West;
		Opposites[(int)Adjacency.West] = Adjacency.East;
		Opposites[(int)Adjacency.Above] = Adjacency.Below;
		Opposites[(int)Adjacency.Below] = Adjacency.Above;

		HorizontalOffset[(int)Adjacency.North] = Index2D.UnitY;
		HorizontalOffset[(int)Adjacency.South] = -Index2D.UnitY;
		HorizontalOffset[(int)Adjacency.East] = Index2D.UnitX;
		HorizontalOffset[(int)Adjacency.West] = -Index2D.UnitX;
		HorizontalOffset[(int)Adjacency.Above] = Index2D.Zero;
		HorizontalOffset[(int)Adjacency.Below] = Index2D.Zero;

		ElevationOffset[(int)Adjacency.North] = 0;
		ElevationOffset[(int)Adjacency.South] = 0;
		ElevationOffset[(int)Adjacency.East] = 0;
		ElevationOffset[(int)Adjacency.West] = 0;
		ElevationOffset[(int)Adjacency.Above] = 1;
		ElevationOffset[(int)Adjacency.Below] = -1;
	}

	public static bool IsDefined(this Adjacency value)
	{
		int number = (int)value;
		return number >= 0 && number < CountOfSides;
	}

	public static Adjacency GetOpposite(this Adjacency value) => Opposites[(int)value];

	public static Index2D GetHorizontalOffset(this Adjacency value) => HorizontalOffset[(int)value];

	public static int GetElevationOffset(this Adjacency value) => ElevationOffset[(int)value];

	public static bool IsHorizontal(this Adjacency value) => ElevationOffset[(int)value] == 0;

	public static bool IsVertical(this Adjacency value) => ElevationOffset[(int)value] != 0;
}
