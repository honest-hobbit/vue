﻿namespace HQS.VUE.Generation;

[SuppressMessage("Design", "CA1051", Justification = "Unity loves public fields.")]
[SuppressMessage("StyleCop", "SA1401", Justification = "Unity loves public fields.")]
[CreateAssetMenu(menuName = "VUE/Maps/Debug Drawer Settings")]
public class DebugMapDrawerSettings : ScriptableObject
{
	public static class Default
	{
		public static Color32 ZoneCheckerColor1 => new Color32(255, 255, 255, 255);

		public static Color32 ZoneCheckerColor2 => new Color32(0, 0, 0, 0);

		public static Color32 CellGroundColor => new Color32(0, 204, 0, 255);

		public static Color32 CellAboveGroundColor => new Color32(222, 222, 255, 255);

		public static Color32 CellBelowGroundColor => new Color32(77, 38, 0, 255);

		public static float CellOutlineShading => 0;

		public static Color32 PathGroundColor => new Color32(128, 255, 128, 255);

		public static Color32 PathAboveGroundColor => new Color32(255, 255, 255, 255);

		public static Color32 PathBelowGroundColor => new Color32(255, 179, 102, 255);

		public static float PathOutlineShading => 0;

		public static Color32 PathBarrierColor => new Color32(0, 0, 0, 255);

		public static Sprite PathAscendIcon => null;

		public static Sprite PathDescendIcon => null;

		public static Sprite PathAscendDescendIcon => null;

		public static int CellPixelsInterior => 4;

		public static int CellPixelsOutline => 1;

		public static float CellIconScale => 1;

		public static float MapEdgesPadding => 0;

		public static float CurrentLayerAlpha => 1;

		public static float AboveCurrentLayerAlpha => 0;

		public static float BelowCurrentLayerAlpha => 0;
	}

	public Color32 ZoneCheckerColor1 = Default.ZoneCheckerColor1;

	public Color32 ZoneCheckerColor2 = Default.ZoneCheckerColor2;

	public Color32 CellGroundColor = Default.CellGroundColor;

	public Color32 CellAboveGroundColor = Default.CellAboveGroundColor;

	public Color32 CellBelowGroundColor = Default.CellBelowGroundColor;

	[Range(-1, 1)]
	public float CellOutlineShading = Default.CellOutlineShading;

	public Color32 PathGroundColor = Default.PathGroundColor;

	public Color32 PathAboveGroundColor = Default.PathAboveGroundColor;

	public Color32 PathBelowGroundColor = Default.PathBelowGroundColor;

	[Range(-1, 1)]
	public float PathOutlineShading = Default.PathOutlineShading;

	public Color32 PathBarrierColor = Default.PathBarrierColor;

	public Sprite PathAscendIcon = Default.PathAscendIcon;

	public Sprite PathDescendIcon = Default.PathDescendIcon;

	public Sprite PathAscendDescendIcon = Default.PathAscendDescendIcon;

	public int CellPixelsInterior = Default.CellPixelsInterior;

	public int CellPixelsOutline = Default.CellPixelsOutline;

	public float CellIconScale = Default.CellIconScale;

	public float MapEdgesPadding = Default.MapEdgesPadding;

	public float CurrentLayerAlpha = Default.CurrentLayerAlpha;

	public float AboveCurrentLayerAlpha = Default.AboveCurrentLayerAlpha;

	public float BelowCurrentLayerAlpha = Default.BelowCurrentLayerAlpha;

	public Color32 CellGroundOutlineColor { get; private set; }

	public Color32 CellAboveGroundOutlineColor { get; private set; }

	public Color32 CellBelowGroundOutlineColor { get; private set; }

	public Color32 PathGroundOutlineColor { get; private set; }

	public Color32 PathAboveGroundOutlineColor { get; private set; }

	public Color32 PathBelowGroundOutlineColor { get; private set; }

	public int CellOutlinePixelThickness { get; private set; }

	public int PathOutlinePixelThickness { get; private set; }

	public int GetCellPixels() => this.CellPixelsInterior + (this.CellPixelsOutline * 2);

	public void Validate() => this.OnValidate();

	// even though it isn't documented or highlighted in blue below, the OnValidate callback here still works
	private void OnValidate()
	{
		this.CellOutlineShading = this.CellOutlineShading.Clamp(-1, 1);
		this.PathOutlineShading = this.PathOutlineShading.Clamp(-1, 1);
		this.CellPixelsInterior = this.CellPixelsInterior.ClampLower(1);
		this.CellPixelsOutline = this.CellPixelsOutline.ClampLower(1);
		this.CellIconScale = this.CellIconScale.ClampLower(0);
		this.MapEdgesPadding = this.MapEdgesPadding.ClampLower(0);
		this.CurrentLayerAlpha = this.CurrentLayerAlpha.Clamp(0, 1);
		this.AboveCurrentLayerAlpha = this.AboveCurrentLayerAlpha.Clamp(0, 1);
		this.BelowCurrentLayerAlpha = this.BelowCurrentLayerAlpha.Clamp(0, 1);

		if (this.CellOutlineShading != 0)
		{
			this.CellOutlinePixelThickness = this.CellPixelsOutline;
			this.CellGroundOutlineColor = this.CellGroundColor.ToHSV()
				.AdjustShade(-this.CellOutlineShading, this.CellOutlineShading);
			this.CellAboveGroundOutlineColor = this.CellAboveGroundColor.ToHSV()
				.AdjustShade(-this.CellOutlineShading, this.CellOutlineShading);
			this.CellBelowGroundOutlineColor = this.CellBelowGroundColor.ToHSV()
				.AdjustShade(-this.CellOutlineShading, this.CellOutlineShading);
		}
		else
		{
			this.CellOutlinePixelThickness = 0;
			this.CellGroundOutlineColor = this.CellGroundColor;
			this.CellAboveGroundOutlineColor = this.CellAboveGroundColor;
			this.CellBelowGroundOutlineColor = this.CellBelowGroundColor;
		}

		if (this.PathOutlineShading != 0)
		{
			this.PathOutlinePixelThickness = this.CellPixelsOutline;
			this.PathGroundOutlineColor = this.PathGroundColor.ToHSV()
				.AdjustShade(-this.PathOutlineShading, this.PathOutlineShading);
			this.PathAboveGroundOutlineColor = this.PathAboveGroundColor.ToHSV()
				.AdjustShade(-this.PathOutlineShading, this.PathOutlineShading);
			this.PathBelowGroundOutlineColor = this.PathBelowGroundColor.ToHSV()
				.AdjustShade(-this.PathOutlineShading, this.PathOutlineShading);
		}
		else
		{
			this.PathOutlinePixelThickness = 0;
			this.PathGroundOutlineColor = this.PathGroundColor;
			this.PathAboveGroundOutlineColor = this.PathAboveGroundColor;
			this.PathBelowGroundOutlineColor = this.PathBelowGroundColor;
		}
	}
}
