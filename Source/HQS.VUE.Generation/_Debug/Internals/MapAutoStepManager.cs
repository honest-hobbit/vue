﻿namespace HQS.VUE.Generation;

internal class MapAutoStepManager
{
	private const int MinStepAmount = 1;

	private const float MinDelayAmount = .016f;

	private readonly DebugMapComponent component;

	private readonly MapGenerationManager generationManager;

	private int stepAmount = MinStepAmount;

	private float delayAmount = MinDelayAmount;

	private IEnumerator autoGenerateSteps;

	private bool hasGeneratedSincePaused;

	public MapAutoStepManager(DebugMapComponent component, MapGenerationManager generationManager)
	{
		Debug.Assert(component != null);
		Debug.Assert(generationManager != null);

		this.component = component;
		this.generationManager = generationManager;
	}

	public int StepAmount
	{
		get => this.stepAmount;
		set => this.stepAmount = value.ClampLower(MinStepAmount);
	}

	public float DelayAmount
	{
		get => this.delayAmount;
		set => this.delayAmount = value.ClampLower(MinDelayAmount);
	}

	public bool IsRunning { get; private set; }

	public void ToggleRunning()
	{
		if (this.autoGenerateSteps == null)
		{
			this.autoGenerateSteps = this.AutoGenerateSteps();
			this.component.StartCoroutine(this.autoGenerateSteps);
			this.IsRunning = true;
		}
		else
		{
			if (this.IsRunning)
			{
				this.component.StopCoroutine(this.autoGenerateSteps);
			}
			else
			{
				this.component.StartCoroutine(this.autoGenerateSteps);
			}

			this.IsRunning = !this.IsRunning;
		}
	}

	public void Pause()
	{
		if (this.IsRunning && this.autoGenerateSteps != null)
		{
			this.component.StopCoroutine(this.autoGenerateSteps);
		}

		this.IsRunning = false;
		this.hasGeneratedSincePaused = false;
	}

	private IEnumerator AutoGenerateSteps()
	{
		float delayAmount = this.DelayAmount;
		var delay = new WaitForSeconds(delayAmount);

		while (true)
		{
			this.component.ClearMapIfDoneGenerating();

			while (this.generationManager.GenerateSteps(this.StepAmount))
			{
				this.component.DrawMapAndUI();
				this.hasGeneratedSincePaused = true;

				if (delayAmount != this.DelayAmount)
				{
					delayAmount = this.DelayAmount;
					delay = new WaitForSeconds(delayAmount);
				}

				yield return delay;
			}

			if (this.hasGeneratedSincePaused)
			{
				// need to set IsRunning before updating the UI
				this.IsRunning = false;
				this.component.DrawMapAndUI();

				// pause this coroutine
				this.component.StopCoroutine(this.autoGenerateSteps);
				yield return null;
			}
		}
	}
}
