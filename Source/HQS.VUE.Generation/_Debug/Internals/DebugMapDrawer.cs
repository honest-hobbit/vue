﻿namespace HQS.VUE.Generation;

internal class DebugMapDrawer
{
	private readonly Stopwatch timer = new Stopwatch();

	private readonly Dictionary<Index2D, Pin<DebugMapZone>> zones =
		new Dictionary<Index2D, Pin<DebugMapZone>>(HQS.Utility.Indexing.Curves.MortonCurve.Comparer<Index2D>());

	private readonly List<Pin<DebugMapZone>> changedZones = new List<Pin<DebugMapZone>>();

	private readonly DebugMapPools pools;

	private readonly RectTransform mapParent;

	private readonly DebugMapZoneDrawer drawer;

	private Map map;

	private IDisposable changedSubscription;

	private bool repositionZones;

	private TimeSpan totalDrawTime;

	private int drawCount;

	public DebugMapDrawer(DebugMapPools pools, RectTransform mapParent, Action updateUI)
	{
		Debug.Assert(pools != null);
		Debug.Assert(mapParent != null);
		Debug.Assert(updateUI != null);

		this.pools = pools;
		this.mapParent = mapParent;
		this.Layers = new MapLayerManager(pools, mapParent, updateUI);
		this.drawer = new DebugMapZoneDrawer(pools, this.Layers);
	}

	public MapLayerManager Layers { get; }

	public Map Map
	{
		get => this.map;
		set
		{
			if (value == this.map)
			{
				return;
			}

			if (this.map != null)
			{
				this.map = null;
				this.changedSubscription.Dispose();
			}

			if (value != null)
			{
				this.map = value;
				this.changedSubscription = value.Zones.Changed.Subscribe(this.OnChanged);
				this.pools.SetMapSizes(value);
			}
		}
	}

	public DebugMapDrawerSettings Settings
	{
		get => this.drawer.Settings;
		set
		{
			this.drawer.Settings = value;
			this.Layers.SetSettings(value);
		}
	}

	public int ZonesCount => this.zones.Count;

	public TimeSpan DrawTime => this.timer.Elapsed;

	public TimeSpan DrawTimeAverage { get; private set; }

	public TimeSpan DrawTimeMax { get; private set; }

	public void ClearDrawnMap()
	{
		this.drawCount = 0;
		this.totalDrawTime = TimeSpan.Zero;
		this.DrawTimeAverage = TimeSpan.Zero;
		this.DrawTimeMax = TimeSpan.Zero;

		foreach (var zone in this.zones.Values)
		{
			zone.Dispose();
		}

		this.Layers.Clear();
		this.zones.Clear();
		this.changedZones.Clear();
	}

	public void DrawMap()
	{
		Debug.Assert(this.Map != null);

		this.timer.Restart();
		this.drawer.SetDrawingSizes();

		this.EnsureHasElevationLayers();
		this.RedrawZones();
		this.RepositionZones();

		// update timing stats
		this.timer.Stop();
		this.totalDrawTime += this.DrawTime;
		this.drawCount++;
		this.DrawTimeAverage = new TimeSpan(this.totalDrawTime.Ticks / this.drawCount);
		if (this.DrawTime > this.DrawTimeMax)
		{
			this.DrawTimeMax = this.DrawTime;
		}
	}

	private void EnsureHasElevationLayers()
	{
		int max = this.map.Paths.MaxElevation;
		for (int elevation = this.map.Paths.MinElevation; elevation <= max; elevation++)
		{
			if (this.Layers.TryAddLayer(elevation, out var layer))
			{
				this.repositionZones = true;
				foreach (var zone in this.zones.Values)
				{
					this.AddZoneElevation(zone, elevation, layer);
				}
			}
		}

		this.Layers.SetSiblingIndices();
	}

	private void RedrawZones()
	{
		int max = this.changedZones.Count;
		for (int i = 0; i < max; i++)
		{
			this.drawer.DrawZone(this.changedZones[i].Value);
		}

		this.changedZones.Clear();
	}

	private void RepositionZones()
	{
		if (!this.repositionZones)
		{
			return;
		}

		this.repositionZones = false;
		var padding = new Vector2(this.Settings.MapEdgesPadding, this.Settings.MapEdgesPadding);
		this.mapParent.sizeDelta = (this.map.Zones.Dimensions.ToUnityVector() * this.pools.ZoneSize) + (padding * 2);

		// update the positions of all the zones
		var offset = (-this.map.Zones.LowerBounds.ToUnityVector() * this.pools.ZoneSize) + padding;
		foreach (var pooledZone in this.zones.Values)
		{
			var zone = pooledZone.Value;
			zone.SetPosition((zone.Zone.Key.ToUnityVector() * this.pools.ZoneSize) + offset);
		}
	}

	private void OnChanged(MapZone mapZone)
	{
		Debug.Assert(mapZone != null);

		if (!this.zones.TryGetValue(mapZone.Key, out var pooledZone))
		{
			pooledZone = this.pools.Zones.Rent();
			var debugZone = pooledZone.Value;
			debugZone.Zone = mapZone;

			// setup the zone's boundary
			debugZone.Boundary.color =
				(debugZone.Zone.Key.X + debugZone.Zone.Key.Y).IsEven() ?
					this.Settings.ZoneCheckerColor1 : this.Settings.ZoneCheckerColor2;
			debugZone.Boundary.gameObject.hideFlags = HideFlags.None;
			debugZone.Boundary.transform.SetParent(this.Layers.ZoneBoundaryLayer, false);
			debugZone.Boundary.gameObject.SetActive(true);

			int max = this.Layers.MaxElevation;
			for (int elevation = this.Layers.MinElevation; elevation <= max; elevation++)
			{
				this.AddZoneElevation(pooledZone, elevation, this.Layers.GetLayer(elevation));
			}

			this.zones.Add(mapZone.Key, pooledZone);
			this.repositionZones = true;
		}

		this.TrackChangedZone(pooledZone);
	}

	private void AddZoneElevation(Pin<DebugMapZone> pooledZone, int elevation, RectTransform layer)
	{
		Debug.Assert(pooledZone.IsPinned);
		Debug.Assert(layer != null);

		var pooled = this.pools.ZoneLayers.Rent();
		var zoneLayer = pooled.Value;
		pooledZone.Value.Layers.Add(pooled);
		zoneLayer.Elevation = elevation;
		zoneLayer.transform.SetParent(layer, false);
		zoneLayer.gameObject.SetActive(true);

		this.TrackChangedZone(pooledZone);
	}

	private void TrackChangedZone(Pin<DebugMapZone> pooledZone)
	{
		Debug.Assert(pooledZone.IsPinned);

		var zone = pooledZone.Value;
		if (!zone.IsDirty)
		{
			zone.IsDirty = true;
			this.changedZones.Add(pooledZone);
		}
	}
}
