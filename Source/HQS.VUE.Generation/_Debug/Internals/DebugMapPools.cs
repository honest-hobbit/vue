﻿namespace HQS.VUE.Generation;

internal class DebugMapPools
{
	public DebugMapPools(
		CanvasGroup layerPrefab,
		RawImage zoneBoundaryPrefab,
		DebugMapZoneLayerComponent zonePrefab,
		DebugMapIconComponent iconPrefab)
	{
		Debug.Assert(layerPrefab != null);
		Debug.Assert(zoneBoundaryPrefab != null);
		Debug.Assert(zonePrefab != null);
		Debug.Assert(iconPrefab != null);

		this.ZoneSize = zonePrefab.GetComponent<RectTransform>().sizeDelta;

		var options = new PoolOptions<CanvasGroup>()
		{
			Deinitialize = x =>
			{
				x.alpha = 1;
				x.blocksRaycasts = false;
			},
		};

		this.Layers = GameObjectPool.ThreadSafe.Pins.Create(layerPrefab, null, options);

		this.Zones = new PinPool<DebugMapZone>(
			() => new DebugMapZone(UnityEngine.Object.Instantiate(zoneBoundaryPrefab)),
			new PoolOptions<DebugMapZone>() { Deinitialize = x => x.Clear() });

		this.ZoneLayers = GameObjectPool.ThreadSafe.Pins.Create(
			zonePrefab, null, new PoolOptions<DebugMapZoneLayerComponent>() { Deinitialize = x => x.Clear() });

		this.Icons = GameObjectPool.ThreadSafe.Pins.Create(
			iconPrefab, null, new PoolOptions<DebugMapIconComponent>() { Deinitialize = x => x.Clear() });
	}

	public int CellsPerZoneSide { get; private set; }

	public Vector2 ZoneSize { get; }

	public Vector2 CellSize { get; private set; }

	public Vector2 CellHalfSize { get; private set; }

	public IPinPool<CanvasGroup> Layers { get; }

	public IPinPool<DebugMapZone> Zones { get; }

	public IPinPool<DebugMapZoneLayerComponent> ZoneLayers { get; }

	public IPinPool<DebugMapIconComponent> Icons { get; }

	public void SetMapSizes(Map map)
	{
		Debug.Assert(map != null);

		this.CellsPerZoneSide = map.CellsPerZoneSide;
		this.CellSize = this.ZoneSize / this.CellsPerZoneSide;
		this.CellHalfSize = this.CellSize / 2f;
	}
}
