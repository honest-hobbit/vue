﻿namespace HQS.VUE.Generation;

internal class MapLayerManager
{
	private readonly SortedList<int, Pin<CanvasGroup>> layers = new SortedList<int, Pin<CanvasGroup>>();

	private readonly DebugMapPools pools;

	private readonly RectTransform mapParent;

	private readonly Action updateUI;

	private bool resortLayers;

	private float aboveCurrentAlpha;

	private float belowCurrentAlpha;

	private float currentAlpha;

	private bool showZones;

	public MapLayerManager(DebugMapPools pools, RectTransform mapParent, Action updateUI)
	{
		Debug.Assert(pools != null);
		Debug.Assert(mapParent != null);
		Debug.Assert(updateUI != null);

		this.pools = pools;
		this.mapParent = mapParent;
		this.updateUI = updateUI;
		this.SetSettings();

		this.ZoneBoundaryLayer = (RectTransform)new GameObject("Map Zone Boundaries", typeof(RectTransform)).transform;
		this.ZoneBoundaryLayer.sizeDelta = new Vector2(0, 0);
		this.ZoneBoundaryLayer.anchorMin = new Vector2(0, 0);
		this.ZoneBoundaryLayer.anchorMax = new Vector2(0, 0);
		this.ZoneBoundaryLayer.SetParent(this.mapParent, false);
		this.ShowZones = true;
	}

	public float AboveCurrentAlpha
	{
		get => this.aboveCurrentAlpha;
		set => this.aboveCurrentAlpha = value.Clamp(0, 1);
	}

	public float BelowCurrentAlpha
	{
		get => this.belowCurrentAlpha;
		set => this.belowCurrentAlpha = value.Clamp(0, 1);
	}

	public float CurrentAlpha
	{
		get => this.currentAlpha;
		set => this.currentAlpha = value.Clamp(0, 1);
	}

	public bool ShowZones
	{
		get => this.showZones;
		set
		{
			this.showZones = value;
			this.ZoneBoundaryLayer.gameObject.SetActive(value);
		}
	}

	public RectTransform ZoneBoundaryLayer { get; }

	public int MinElevation { get; private set; }

	public int MaxElevation { get; private set; } = -1;

	public int CurrentElevation { get; private set; }

	public bool CanMoveDown => this.CurrentElevation > this.MinElevation;

	public bool CanMoveUp => this.CurrentElevation < this.MaxElevation;

	public void MoveDown()
	{
		if (!this.CanMoveDown)
		{
			return;
		}

		this.CurrentElevation--;
		this.UpdateLayer(this.CurrentElevation + 1);
		this.UpdateLayer(this.CurrentElevation);
		this.updateUI();
	}

	public void MoveUp()
	{
		if (!this.CanMoveUp)
		{
			return;
		}

		this.CurrentElevation++;
		this.UpdateLayer(this.CurrentElevation - 1);
		this.UpdateLayer(this.CurrentElevation);
		this.updateUI();
	}

	public void SetSiblingIndices()
	{
		if (this.resortLayers)
		{
			this.resortLayers = false;
			int max = this.layers.Values.Count;
			for (int i = 0; i < max; i++)
			{
				this.layers.Values[i].Value.transform.SetSiblingIndex(i);
			}

			this.ZoneBoundaryLayer.SetSiblingIndex(max);
		}
	}

	public RectTransform GetLayer(int elevation) => (RectTransform)this.layers[elevation].Value.transform;

	public bool TryAddLayer(int elevation, out RectTransform layer)
	{
		if (this.layers.ContainsKey(elevation))
		{
			layer = null;
			return false;
		}

		var pooled = this.pools.Layers.Rent();
		var layerCanvas = pooled.Value;
		layerCanvas.name = "Map Elevation " + elevation.ToString();
		layerCanvas.transform.SetParent(this.mapParent, false);

		if (this.layers.Count == 0)
		{
			this.CurrentElevation = elevation;
			this.MinElevation = elevation;
			this.MaxElevation = elevation;
		}
		else
		{
			this.MinElevation = Mathf.Min(this.MinElevation, elevation);
			this.MaxElevation = Mathf.Max(this.MaxElevation, elevation);
		}

		this.layers.Add(elevation, pooled);
		this.resortLayers = true;
		this.UpdateLayer(elevation);
		layer = (RectTransform)layerCanvas.transform;
		return true;
	}

	public void Clear()
	{
		foreach (var pooled in this.layers.Values)
		{
			pooled.Dispose();
		}

		this.layers.Clear();
		this.CurrentElevation = 0;
		this.MinElevation = 0;
		this.MaxElevation = -1;
	}

	public void SetSettings(DebugMapDrawerSettings settings = null)
	{
		if (settings == null)
		{
			this.AboveCurrentAlpha = DebugMapDrawerSettings.Default.AboveCurrentLayerAlpha;
			this.BelowCurrentAlpha = DebugMapDrawerSettings.Default.BelowCurrentLayerAlpha;
			this.CurrentAlpha = DebugMapDrawerSettings.Default.CurrentLayerAlpha;
		}
		else
		{
			settings.Validate();
			this.AboveCurrentAlpha = settings.AboveCurrentLayerAlpha;
			this.BelowCurrentAlpha = settings.BelowCurrentLayerAlpha;
			this.CurrentAlpha = settings.CurrentLayerAlpha;
		}
	}

	private static void SetLayerAlpha(CanvasGroup canvas, float alpha)
	{
		Debug.Assert(canvas != null);
		Debug.Assert(alpha >= 0 && alpha <= 1);

		if (alpha == 0)
		{
			canvas.gameObject.SetActive(false);
		}
		else
		{
			canvas.alpha = alpha;
			canvas.gameObject.SetActive(true);
		}
	}

	private void UpdateLayer(int elevation)
	{
		var layer = this.layers[elevation].Value;
		if (elevation == this.CurrentElevation)
		{
			SetLayerAlpha(layer, this.CurrentAlpha);
			layer.blocksRaycasts = true;
		}
		else if (elevation > this.CurrentElevation)
		{
			SetLayerAlpha(layer, this.AboveCurrentAlpha);
			layer.blocksRaycasts = false;
		}
		else
		{
			SetLayerAlpha(layer, this.BelowCurrentAlpha);
			layer.blocksRaycasts = false;
		}
	}
}
