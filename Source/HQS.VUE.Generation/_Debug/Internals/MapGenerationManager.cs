﻿namespace HQS.VUE.Generation;

internal class MapGenerationManager
{
	private readonly Stopwatch timer = new Stopwatch();

	private IEnumerator<object> generating;

	public Map Map { get; set; }

	public MapGenModule Generator { get; set; }

	public int Seed { get; set; }

	public bool RandomizeSeed { get; set; } = true;

	public bool IsGeneratingDone { get; private set; }

	public long TotalSteps { get; private set; }

	public TimeSpan TotalTime => this.timer.Elapsed;

	public void ClearMap()
	{
		this.Map.Clear();
		this.IsGeneratingDone = false;
		this.TotalSteps = 0;
		this.timer.Reset();
		this.generating = null;
	}

	public bool GenerateAll() => this.GenerateSteps(long.MaxValue);

	public bool GenerateSteps(long steps)
	{
		Ensure.That(steps, nameof(steps)).IsGte(0);
		Ensure.That(this.Map, nameof(this.Map)).IsNotNull();
		Ensure.That(this.Generator, nameof(this.Generator)).IsNotNull();

		if (this.IsGeneratingDone)
		{
			return false;
		}

		if (this.generating == null)
		{
			this.ClearMap();
			if (this.RandomizeSeed)
			{
				this.Seed = ThreadLocalRandom.Instance.Next();
			}

			this.generating = this.Generator.Generate(this.Map, new Random(this.Seed)).GetEnumerator();
		}

		long count = 0;
		this.timer.Start();
		try
		{
			while (count < steps)
			{
				if (!this.generating.MoveNext())
				{
					this.IsGeneratingDone = true;
					this.generating = null;
					return false;
				}

				count++;
			}

			return true;
		}
		finally
		{
			this.timer.Stop();
			this.TotalSteps += count;
		}
	}
}
