﻿namespace HQS.VUE.Generation;

internal class DebugMapZoneDrawer
{
	private readonly Pixels2D32 pixels = new Pixels2D32();

	private readonly DebugMapPools pools;

	private readonly Action moveElevationUp;

	private readonly Action moveElevationDown;

	public DebugMapZoneDrawer(DebugMapPools pools, MapLayerManager layers)
	{
		Debug.Assert(pools != null);
		Debug.Assert(layers != null);

		this.pools = pools;
		this.moveElevationUp = layers.MoveUp;
		this.moveElevationDown = layers.MoveDown;
	}

	public DebugMapDrawerSettings Settings { get; set; }

	private int cellPixels;

	private int zonePixels;

	private Vector2 iconSize;

	public void SetDrawingSizes()
	{
		Debug.Assert(this.Settings != null);

		this.cellPixels = this.Settings.GetCellPixels();
		this.zonePixels = this.pools.CellsPerZoneSide * this.cellPixels;
		this.pixels.SetDimensions(this.zonePixels, this.zonePixels);
		this.iconSize = this.pools.CellSize * this.Settings.CellIconScale;
	}

	public void DrawZone(DebugMapZone zone)
	{
		Debug.Assert(this.Settings != null);
		Debug.Assert(zone != null);
		Debug.Assert(zone.Zone != null);
		Debug.Assert(zone.IsDirty);

		var mapZone = zone.Zone;
		var layers = zone.Layers;
		int max = layers.Count;

		for (int i = 0; i < max; i++)
		{
			this.DrawZone(mapZone, layers[i].Value);
		}

		zone.IsDirty = false;
	}

	private void DrawZone(MapZone zone, DebugMapZoneLayerComponent component)
	{
		Debug.Assert(zone != null);
		Debug.Assert(component != null);

		component.ClearIcons();
		this.pixels.SetAllPixelsTo(new Color32(0, 0, 0, 0));

		int zoneLength = this.pools.CellsPerZoneSide;
		int cellX = 0;
		for (int iX = 0; iX < zoneLength; iX++)
		{
			int cellY = 0;
			for (int iY = 0; iY < zoneLength; iY++)
			{
				var mapCell = zone[iX, iY];
				if (mapCell.TryGetValue(component.Elevation, out var path))
				{
					this.DrawPath(path, cellX, cellY);
					this.DrawIcons(component, path, iX, iY);
				}
				else
				{
					this.DrawCell(mapCell, cellX, cellY, component.Elevation);
				}

				cellY += this.cellPixels;
			}

			cellX += this.cellPixels;
		}

		var texture = component.Texture;
		if (texture == null)
		{
			texture = new Texture2D(this.zonePixels, this.zonePixels, TextureFormat.RGBA32, mipChain: false)
			{
				filterMode = FilterMode.Point,
			};

			component.Texture = texture;
			component.GetComponent<RawImage>().texture = texture;
		}

		this.pixels.ApplyTo(texture, updateMipmaps: false);
	}

	private void DrawCell(MapCell cell, int x, int y, int layerElevation)
	{
		Debug.Assert(cell != null);

		var fillColor = this.Settings.CellGroundColor;
		var outlineColor = this.Settings.CellGroundOutlineColor;
		if (cell.Elevation < layerElevation)
		{
			fillColor = this.Settings.CellAboveGroundColor;
			outlineColor = this.Settings.CellAboveGroundOutlineColor;
		}
		else if (cell.Elevation > layerElevation)
		{
			fillColor = this.Settings.CellBelowGroundColor;
			outlineColor = this.Settings.CellBelowGroundOutlineColor;
		}

		this.pixels.DrawRectangle(
			x, y, this.cellPixels, this.cellPixels, fillColor, outlineColor, this.Settings.CellOutlinePixelThickness);
	}

	private void DrawPath(PathCell path, int x, int y)
	{
		Debug.Assert(path != null);

		var fillColor = this.Settings.PathGroundColor;
		var outlineColor = this.Settings.PathGroundOutlineColor;
		if (path.IsAboveGround)
		{
			fillColor = this.Settings.PathAboveGroundColor;
			outlineColor = this.Settings.PathAboveGroundOutlineColor;
		}
		else if (path.IsUnderGround)
		{
			fillColor = this.Settings.PathBelowGroundColor;
			outlineColor = this.Settings.PathBelowGroundOutlineColor;
		}

		// fill in the floor
		this.pixels.DrawRectangle(
			x, y, this.cellPixels, this.cellPixels, fillColor, outlineColor, this.Settings.PathOutlinePixelThickness);

		// draw any barriers
		int outlineSize = this.Settings.CellPixelsOutline;
		if (path.South == null)
		{
			this.pixels.DrawRectangleFilled(
				x, y, this.cellPixels, outlineSize, this.Settings.PathBarrierColor);
		}

		if (path.North == null)
		{
			this.pixels.DrawRectangleFilled(
				x, y + this.cellPixels - outlineSize, this.cellPixels, outlineSize, this.Settings.PathBarrierColor);
		}

		if (path.West == null)
		{
			this.pixels.DrawRectangleFilled(
				x, y, outlineSize, this.cellPixels, this.Settings.PathBarrierColor);
		}

		if (path.East == null)
		{
			this.pixels.DrawRectangleFilled(
				x + this.cellPixels - outlineSize, y, outlineSize, this.cellPixels, this.Settings.PathBarrierColor);
		}
	}

	private void DrawIcons(DebugMapZoneLayerComponent zoneComponent, PathCell path, int x, int y)
	{
		Debug.Assert(zoneComponent != null);
		Debug.Assert(path != null);

		Sprite stairsIcon = null;
		Action leftClick = null;
		Action rightClick = null;

		if (path.Above != null)
		{
			if (path.Below != null)
			{
				// ascend and descend
				stairsIcon = this.Settings.PathAscendDescendIcon;
				leftClick = this.moveElevationUp;
				rightClick = this.moveElevationDown;
			}
			else
			{
				// ascend only
				stairsIcon = this.Settings.PathAscendIcon;
				leftClick = this.moveElevationUp;
			}
		}
		else if (path.Below != null)
		{
			// descend only
			stairsIcon = this.Settings.PathDescendIcon;
			leftClick = this.moveElevationDown;
		}

		if (stairsIcon != null)
		{
			var pooled = this.pools.Icons.Rent();
			var icon = pooled.Value;

			icon.GetComponent<Image>().sprite = stairsIcon;
			icon.OnLeftClick = leftClick;
			icon.OnRightClick = rightClick;

			icon.transform.SetParent(zoneComponent.transform, false);
			var rect = (RectTransform)icon.transform;
			rect.sizeDelta = this.iconSize;
			rect.anchoredPosition = new Vector2(this.pools.CellSize.x * x, this.pools.CellSize.y * y) + this.pools.CellHalfSize;
			icon.gameObject.SetActive(true);

			zoneComponent.Icons.Add(pooled);
		}
	}
}
