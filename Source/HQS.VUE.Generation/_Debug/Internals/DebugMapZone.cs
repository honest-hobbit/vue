﻿namespace HQS.VUE.Generation;

internal class DebugMapZone
{
	public DebugMapZone(RawImage boundary)
	{
		Debug.Assert(boundary != null);

		this.Boundary = boundary;
		this.Boundary.gameObject.hideFlags = HideFlags.HideInHierarchy;
	}

	public List<Pin<DebugMapZoneLayerComponent>> Layers { get; } = new List<Pin<DebugMapZoneLayerComponent>>();

	public RawImage Boundary { get; }

	public MapZone Zone { get; set; }

	public bool IsDirty { get; set; }

	public void SetPosition(Vector2 position)
	{
		int max = this.Layers.Count;
		for (int i = 0; i < max; i++)
		{
			((RectTransform)this.Layers[i].Value.transform).anchoredPosition = position;
		}

		if (this.Boundary != null)
		{
			((RectTransform)this.Boundary.transform).anchoredPosition = position;
		}
	}

	public void Clear()
	{
		this.Zone = null;
		this.IsDirty = false;

		this.Boundary.gameObject.hideFlags = HideFlags.HideInHierarchy;
		this.Boundary.gameObject.SetActive(false);
		this.Boundary.transform.SetParent(null, false);

		int max = this.Layers.Count;
		for (int i = 0; i < max; i++)
		{
			this.Layers[i].Dispose();
		}

		this.Layers.Clear();
	}
}
