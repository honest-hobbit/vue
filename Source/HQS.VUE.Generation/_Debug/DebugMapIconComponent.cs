﻿namespace HQS.VUE.Generation;

public class DebugMapIconComponent : EventTrigger
{
	public Action OnLeftClick { get; set; }

	public Action OnRightClick { get; set; }

	public void Clear()
	{
		this.OnLeftClick = null;
		this.OnRightClick = null;
	}

	public override void OnPointerClick(PointerEventData eventData)
	{
		switch (eventData.button)
		{
			case PointerEventData.InputButton.Left:
				this.OnLeftClick?.Invoke();
				break;

			case PointerEventData.InputButton.Right:
				this.OnRightClick?.Invoke();
				break;
		}
	}
}
