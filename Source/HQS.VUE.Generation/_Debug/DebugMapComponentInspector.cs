﻿#if UNITY_EDITOR

namespace HQS.VUE.Generation;

[CustomEditor(typeof(DebugMapComponent))]
public class DebugMapComponentInspector : Editor
{
	private SerializedProperty propertyUIMapParent;
	private SerializedProperty propertyUISeed;
	private SerializedProperty propertyUIRandomizeSeed;
	private SerializedProperty propertyUIClearMap;
	private SerializedProperty propertyUIStepAmount;
	private SerializedProperty propertyUIDelayAmount;
	private SerializedProperty propertyUISingleStep;
	private SerializedProperty propertyUIToggleAutoSteps;
	private SerializedProperty propertyUIElevationHeader;
	private SerializedProperty propertyUIElevationDown;
	private SerializedProperty propertyUIElevationUp;
	private SerializedProperty propertyUIShowZones;
	private SerializedProperty propertyUIDiagnosticsOutput;
	private SerializedProperty propertyLayerPrefab;
	private SerializedProperty propertyZoneBoundaryPrefab;
	private SerializedProperty propertyZonePrefab;
	private SerializedProperty propertyIconPrefab;
	private SerializedProperty propertyMap;
	private SerializedProperty propertyGenerator;
	private SerializedProperty propertyDrawerSettings;

	private bool areDependenciesExpanded = false;

	public override void OnInspectorGUI()
	{
		this.serializedObject.Update();
		this.areDependenciesExpanded = EditorGUILayout.Foldout(this.areDependenciesExpanded, "Dependencies");
		if (this.areDependenciesExpanded)
		{
			EditorGUILayout.PropertyField(this.propertyUIMapParent);
			EditorGUILayout.PropertyField(this.propertyUISeed);
			EditorGUILayout.PropertyField(this.propertyUIRandomizeSeed);
			EditorGUILayout.PropertyField(this.propertyUIClearMap);
			EditorGUILayout.PropertyField(this.propertyUIStepAmount);
			EditorGUILayout.PropertyField(this.propertyUIDelayAmount);
			EditorGUILayout.PropertyField(this.propertyUISingleStep);
			EditorGUILayout.PropertyField(this.propertyUIToggleAutoSteps);
			EditorGUILayout.PropertyField(this.propertyUIElevationHeader);
			EditorGUILayout.PropertyField(this.propertyUIElevationDown);
			EditorGUILayout.PropertyField(this.propertyUIElevationUp);
			EditorGUILayout.PropertyField(this.propertyUIShowZones);
			EditorGUILayout.PropertyField(this.propertyUIDiagnosticsOutput);
			EditorGUILayout.PropertyField(this.propertyLayerPrefab);
			EditorGUILayout.PropertyField(this.propertyZoneBoundaryPrefab);
			EditorGUILayout.PropertyField(this.propertyZonePrefab);
			EditorGUILayout.PropertyField(this.propertyIconPrefab);
			EditorGUILayout.PropertyField(this.propertyMap);
		}

		EditorGUILayout.PropertyField(this.propertyGenerator);
		EditorGUILayout.PropertyField(this.propertyDrawerSettings);
		this.serializedObject.ApplyModifiedProperties();
	}

	public void OnEnable()
	{
		this.propertyUIMapParent = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIMapParent));
		this.propertyUISeed = this.serializedObject.FindProperty(nameof(DebugMapComponent.UISeed));
		this.propertyUIRandomizeSeed = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIRandomizeSeed));
		this.propertyUIClearMap = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIClearMap));
		this.propertyUIStepAmount = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIStepAmount));
		this.propertyUIDelayAmount = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIDelayAmount));
		this.propertyUISingleStep = this.serializedObject.FindProperty(nameof(DebugMapComponent.UISingleStep));
		this.propertyUIToggleAutoSteps = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIToggleAutoSteps));
		this.propertyUIElevationHeader = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIElevationHeader));
		this.propertyUIElevationDown = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIElevationDown));
		this.propertyUIElevationUp = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIElevationUp));
		this.propertyUIShowZones = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIShowZones));
		this.propertyUIDiagnosticsOutput = this.serializedObject.FindProperty(nameof(DebugMapComponent.UIDiagnosticsOutput));
		this.propertyLayerPrefab = this.serializedObject.FindProperty(nameof(DebugMapComponent.LayerPrefab));
		this.propertyZoneBoundaryPrefab = this.serializedObject.FindProperty(nameof(DebugMapComponent.ZoneBoundaryPrefab));
		this.propertyZonePrefab = this.serializedObject.FindProperty(nameof(DebugMapComponent.ZonePrefab));
		this.propertyIconPrefab = this.serializedObject.FindProperty(nameof(DebugMapComponent.IconPrefab));
		this.propertyMap = this.serializedObject.FindProperty(nameof(DebugMapComponent.Map));
		this.propertyGenerator = this.serializedObject.FindProperty(nameof(DebugMapComponent.Generator));
		this.propertyDrawerSettings = this.serializedObject.FindProperty(nameof(DebugMapComponent.DrawerSettings));
	}
}

#endif
