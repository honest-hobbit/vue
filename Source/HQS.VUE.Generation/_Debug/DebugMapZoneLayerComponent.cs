﻿namespace HQS.VUE.Generation;

public class DebugMapZoneLayerComponent : MonoBehaviour
{
	public int Elevation { get; internal set; }

	internal Texture2D Texture { get; set; }

	internal List<Pin<DebugMapIconComponent>> Icons { get; } = new List<Pin<DebugMapIconComponent>>();

	internal void Clear()
	{
		this.Elevation = 0;
		this.ClearIcons();
	}

	internal void ClearIcons()
	{
		int max = this.Icons.Count;
		for (int i = 0; i < max; i++)
		{
			this.Icons[i].Dispose();
		}

		this.Icons.Clear();
	}
}
