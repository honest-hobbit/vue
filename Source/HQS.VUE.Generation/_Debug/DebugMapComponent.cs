﻿namespace HQS.VUE.Generation;

[SuppressMessage("Design", "CA1051", Justification = "Unity loves public fields.")]
[SuppressMessage("StyleCop", "SA1401", Justification = "Unity loves public fields.")]
public class DebugMapComponent : MonoBehaviour
{
	public MapComponent Map;

	[Expandable]
	public MapGenModule Generator;

	[Expandable]
	public DebugMapDrawerSettings DrawerSettings;

	public CanvasGroup LayerPrefab;

	public RawImage ZoneBoundaryPrefab;

	public DebugMapZoneLayerComponent ZonePrefab;

	public DebugMapIconComponent IconPrefab;

	public RectTransform UIMapParent;

	public InputField UISeed;

	public Toggle UIRandomizeSeed;

	public Button UIClearMap;

	public InputField UIStepAmount;

	public InputField UIDelayAmount;

	public Button UISingleStep;

	public Button UIToggleAutoSteps;

	public Text UIElevationHeader;

	public Button UIElevationDown;

	public Button UIElevationUp;

	public Toggle UIShowZones;

	public Text UIDiagnosticsOutput;

	private readonly MapGenerationManager generationManager;

	private readonly MapAutoStepManager autoGenerateManager;

	private DebugMapDrawer mapDrawer;

	private string elevationHeaderTextFormat;

	private string diagnosticsOutputTextFormat;

	public DebugMapComponent()
	{
		this.generationManager = new MapGenerationManager();
		this.autoGenerateManager = new MapAutoStepManager(this, this.generationManager);
	}

	public bool IsInitialized => this.mapDrawer != null;

	public int Seed
	{
		get => this.generationManager.Seed;
		set
		{
			this.generationManager.Seed = value;
			if (this.UISeed != null)
			{
				this.UISeed.text = this.SeedText;
			}
		}
	}

	public string SeedText
	{
		get => this.Seed.ToString();
		set
		{
			if (int.TryParse(value, out int seed))
			{
				this.Seed = seed;
			}
		}
	}

	public bool RandomizeSeed
	{
		get => this.generationManager.RandomizeSeed;
		set
		{
			this.generationManager.RandomizeSeed = value;
			if (this.UIRandomizeSeed != null)
			{
				this.UIRandomizeSeed.isOn = this.RandomizeSeed;
			}
		}
	}

	public int StepAmount
	{
		get => this.autoGenerateManager.StepAmount;
		set
		{
			this.autoGenerateManager.StepAmount = value;
			if (this.UIStepAmount != null)
			{
				this.UIStepAmount.text = this.StepAmountText;
			}
		}
	}

	public string StepAmountText
	{
		get => this.StepAmount.ToString();
		set
		{
			if (int.TryParse(value, out int amount))
			{
				this.StepAmount = amount;
			}
		}
	}

	public float DelayAmount
	{
		get => this.autoGenerateManager.DelayAmount;
		set
		{
			this.autoGenerateManager.DelayAmount = value;
			if (this.UIDelayAmount != null)
			{
				this.UIDelayAmount.text = this.DelayAmountText;
			}
		}
	}

	public string DelayAmountText
	{
		get => this.DelayAmount.ToString();
		set
		{
			if (float.TryParse(value, out float amount))
			{
				this.DelayAmount = amount;
			}
		}
	}

	public void GenerateMap()
	{
		this.ValidateIsInitialized(nameof(this.GenerateMap));

		this.autoGenerateManager.Pause();
		this.ClearMapIfDoneGenerating();
		this.generationManager.GenerateAll();
		this.DrawMapAndUI();
	}

	public void ClearMap()
	{
		this.ValidateIsInitialized(nameof(this.ClearMap));

		this.autoGenerateManager.Pause();
		this.generationManager.ClearMap();
		this.mapDrawer.ClearDrawnMap();
		this.UpdateUI();
	}

	public void GenerateSingleStep()
	{
		this.ValidateIsInitialized(nameof(this.GenerateSingleStep));

		this.autoGenerateManager.Pause();
		this.generationManager.GenerateSteps(this.StepAmount);
		this.DrawMapAndUI();
	}

	public void ToggleAutoGeneratingSteps()
	{
		this.ValidateIsInitialized(nameof(this.ToggleAutoGeneratingSteps));

		this.autoGenerateManager.ToggleRunning();
		this.UpdateUIToggleAutoSteps();
	}

	public void MoveElevationLayerDown()
	{
		this.ValidateIsInitialized(nameof(this.MoveElevationLayerDown));

		this.mapDrawer.Layers.MoveDown();
	}

	public void MoveElevationLayerUp()
	{
		this.ValidateIsInitialized(nameof(this.MoveElevationLayerUp));

		this.mapDrawer.Layers.MoveUp();
	}

	public void AdjustZoomAmount(float zoomAmout)
	{
		this.ValidateIsInitialized(nameof(this.AdjustZoomAmount));

		var scale = (float)Interpolation.ConvertRange(zoomAmout, 0, 1, 0, 2).ClampLower(.1);
		this.UIMapParent.localScale = new Vector3(scale, scale, 1);
	}

	public bool ShowZones
	{
		get => this.mapDrawer.Layers.ShowZones;
		set
		{
			this.mapDrawer.Layers.ShowZones = value;
			if (this.UIShowZones != null)
			{
				this.UIShowZones.isOn = this.ShowZones;
			}
		}
	}

	public void Awake()
	{
		Ensure.That(this.Map, nameof(this.Map)).IsNotNull();
		Ensure.That(this.Generator, nameof(this.Generator)).IsNotNull();
		Ensure.That(this.DrawerSettings, nameof(this.DrawerSettings)).IsNotNull();
		Ensure.That(this.LayerPrefab, nameof(this.LayerPrefab)).IsNotNull();
		Ensure.That(this.ZoneBoundaryPrefab, nameof(this.ZoneBoundaryPrefab)).IsNotNull();
		Ensure.That(this.ZonePrefab, nameof(this.ZonePrefab)).IsNotNull();
		Ensure.That(this.IconPrefab, nameof(this.IconPrefab)).IsNotNull();
		Ensure.That(this.UIMapParent, nameof(this.UIMapParent)).IsNotNull();
		Ensure.That(this.UISeed, nameof(this.UISeed)).IsNotNull();
		Ensure.That(this.UIRandomizeSeed, nameof(this.UIRandomizeSeed)).IsNotNull();
		Ensure.That(this.UIClearMap, nameof(this.UIClearMap)).IsNotNull();
		Ensure.That(this.UIStepAmount, nameof(this.UIStepAmount)).IsNotNull();
		Ensure.That(this.UIDelayAmount, nameof(this.UIDelayAmount)).IsNotNull();
		Ensure.That(this.UISingleStep, nameof(this.UISingleStep)).IsNotNull();
		Ensure.That(this.UIToggleAutoSteps, nameof(this.UIToggleAutoSteps)).IsNotNull();
		Ensure.That(this.UIElevationHeader, nameof(this.UIElevationHeader)).IsNotNull();
		Ensure.That(this.UIElevationDown, nameof(this.UIElevationDown)).IsNotNull();
		Ensure.That(this.UIElevationUp, nameof(this.UIElevationUp)).IsNotNull();
		Ensure.That(this.UIShowZones, nameof(this.UIShowZones)).IsNotNull();
		Ensure.That(this.UIDiagnosticsOutput, nameof(this.UIDiagnosticsOutput)).IsNotNull();

		this.DrawerSettings.Validate();

		this.generationManager.Map = this.Map.Map;
		this.generationManager.Generator = this.Generator;

		this.UIStepAmount.onValidateInput += (string text, int charIndex, char addedChar) =>
			char.IsDigit(addedChar) ? addedChar : '\0';

		this.elevationHeaderTextFormat = this.UIElevationHeader.text;
		this.diagnosticsOutputTextFormat = this.UIDiagnosticsOutput.text;

		this.mapDrawer = new DebugMapDrawer(
			new DebugMapPools(this.LayerPrefab, this.ZoneBoundaryPrefab, this.ZonePrefab, this.IconPrefab),
			this.UIMapParent,
			this.UpdateUIElevation)
		{
			Settings = this.DrawerSettings,
			Map = this.Map.Map,
		};
	}

	public void Start()
	{
		this.UIRandomizeSeed.isOn = this.RandomizeSeed;
		this.UIShowZones.isOn = this.ShowZones;
		this.UIStepAmount.text = this.StepAmountText;
		this.UIDelayAmount.text = this.DelayAmountText;
		this.UpdateUI();
	}

	internal void ClearMapIfDoneGenerating()
	{
		Debug.Assert(this.IsInitialized);

		if (this.generationManager.IsGeneratingDone)
		{
			this.generationManager.ClearMap();
			this.mapDrawer.ClearDrawnMap();
		}
	}

	internal void DrawMapAndUI()
	{
		Debug.Assert(this.IsInitialized);

		this.mapDrawer.DrawMap();
		this.UpdateUI();
	}

	private void ValidateIsInitialized(string methodName)
	{
		Debug.Assert(!methodName.IsNullOrWhiteSpace());

		if (!this.IsInitialized)
		{
			throw new InvalidOperationException($"Can't call {methodName} before this component is initialized.");
		}
	}

	private void UpdateUI()
	{
		this.UISeed.text = this.SeedText;
		this.UpdateUIToggleAutoSteps();
		this.UpdateUIElevation();

		var map = this.generationManager.Map;
		this.UIClearMap.interactable = map.Paths.Count > 0;
		this.UISingleStep.interactable = !this.generationManager.IsGeneratingDone;

		this.UIDiagnosticsOutput.text = string.Format(
			this.diagnosticsOutputTextFormat,
			map.Paths.Count,
			map.Paths.Dimensions.ToString(),
			map.Zones.Values.Where(x => x.PathsCount > 0).Count(),
			this.mapDrawer.ZonesCount,
			map.Zones.Dimensions.ToString(),
			$"({map.Paths.MinElevation:n0}, {map.Paths.MaxElevation:n0})",
			this.generationManager.TotalSteps,
			this.generationManager.TotalTime.TotalMilliseconds,
			this.mapDrawer.DrawTimeAverage.TotalMilliseconds,
			this.mapDrawer.DrawTimeMax.TotalMilliseconds);
	}

	private void UpdateUIToggleAutoSteps() => this.UIToggleAutoSteps.GetComponentInChildren<Text>().text =
		this.autoGenerateManager.IsRunning ? "Stop Auto Generating" : "Start Auto Generating";

	private void UpdateUIElevation()
	{
		this.UIElevationHeader.text = string.Format(this.elevationHeaderTextFormat, this.mapDrawer.Layers.CurrentElevation);
		this.UIElevationDown.interactable = this.mapDrawer.Layers.CanMoveDown;
		this.UIElevationUp.interactable = this.mapDrawer.Layers.CanMoveUp;
	}
}
