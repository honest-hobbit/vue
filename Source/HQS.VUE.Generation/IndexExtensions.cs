﻿namespace HQS.VUE.Generation;

internal static class IndexExtensions
{
	public static Vector2 ToUnityVector(this Index2D index) => new Vector2(index.X, index.Y);
}
