﻿namespace HQS.VUE.Generation;

[CreateAssetMenu(menuName = "VUE/Maps/Relink MapGenModule")]
public class RelinkMapGenModule : MapGenModule
{
	[SerializeField]
	[Range(0, 1)]
	private float horizontalBetweenZones;

	[SerializeField]
	[Range(0, 1)]
	private float horizontalWithinZone;

	[SerializeField]
	[Range(0, 1)]
	private float vertical;

	public float HorizontalBetweenZonesChance
	{
		get => this.horizontalBetweenZones;
		set
		{
			this.horizontalBetweenZones = value;
			this.OnValidate();
		}
	}

	public float HorizontalWithinZoneChance
	{
		get => this.horizontalWithinZone;
		set
		{
			this.horizontalWithinZone = value;
			this.OnValidate();
		}
	}

	public float VerticalChance
	{
		get => this.vertical;
		set
		{
			this.vertical = value;
			this.OnValidate();
		}
	}

	/// <inheritdoc />
	protected override IEnumerable<object> HandleGenerating(Map map, System.Random random)
	{
		bool includeHorizontal = this.horizontalBetweenZones > 0 || this.horizontalWithinZone > 0;
		bool includeVertical = this.vertical > 0;

		if (!includeHorizontal && !includeVertical)
		{
			yield break;
		}

		map.Snapshot.SetSnapshot();
		foreach (var path in map.Snapshot.Paths)
		{
			if (includeHorizontal)
			{
				if (CheckHorizontal(path, Adjacency.North, out var adjacentPath))
				{
					yield return adjacentPath;
				}

				if (CheckHorizontal(path, Adjacency.South, out adjacentPath))
				{
					yield return adjacentPath;
				}

				if (CheckHorizontal(path, Adjacency.East, out adjacentPath))
				{
					yield return adjacentPath;
				}

				if (CheckHorizontal(path, Adjacency.West, out adjacentPath))
				{
					yield return adjacentPath;
				}
			}

			if (includeVertical)
			{
				if (CheckVertical(path, Adjacency.Above, out var adjacentPath))
				{
					yield return adjacentPath;
				}

				if (CheckVertical(path, Adjacency.Below, out adjacentPath))
				{
					yield return adjacentPath;
				}
			}
		}

		bool CheckHorizontal(PathCell path, Adjacency side, out PathCell adjacentPath)
		{
			Debug.Assert(path != null);

			if (path.TryGetUnlinkedPathIfExists(side, out var adjacent))
			{
				float chance = (path.Cell.Zone == adjacent.Cell.Zone) ?
					this.horizontalWithinZone : this.horizontalBetweenZones;
				if (random.NextChanceOutcome(chance))
				{
					return path.TryLinkTo(side, out adjacentPath);
				}
			}

			adjacentPath = null;
			return false;
		}

		bool CheckVertical(PathCell path, Adjacency side, out PathCell adjacentPath)
		{
			Debug.Assert(path != null);

			if (path.DoesUnlinkedPathExist(side))
			{
				if (random.NextChanceOutcome(this.vertical))
				{
					return path.TryLinkTo(side, out adjacentPath);
				}
			}

			adjacentPath = null;
			return false;
		}
	}

	private void OnValidate()
	{
		this.horizontalBetweenZones = this.horizontalBetweenZones.Clamp(0, 1);
		this.horizontalWithinZone = this.horizontalWithinZone.Clamp(0, 1);
		this.vertical = this.vertical.Clamp(0, 1);
	}
}
