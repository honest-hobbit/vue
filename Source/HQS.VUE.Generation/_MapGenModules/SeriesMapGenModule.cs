﻿namespace HQS.VUE.Generation;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
[CreateAssetMenu(menuName = "VUE/Maps/Series MapGenModule")]
public class SeriesMapGenModule : MapGenModule
{
	[SerializeField]
	[Expandable]
	private MapGenModule[] modules = null;

	/// <inheritdoc />
	protected override IEnumerable<object> HandleGenerating(Map map, System.Random random)
	{
		int max = this.modules?.Length ?? 0;
		for (int i = 0; i < max; i++)
		{
			if (this.modules[i] != null)
			{
				foreach (var step in this.modules[i].Generate(map, random))
				{
					yield return step;
				}
			}
		}
	}
}
