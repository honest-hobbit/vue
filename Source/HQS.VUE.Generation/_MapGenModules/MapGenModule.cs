﻿namespace HQS.VUE.Generation;

public abstract class MapGenModule : ScriptableObject
{
	public IEnumerable<object> Generate(Map map, System.Random random)
	{
		Ensure.That(map, nameof(map)).IsNotNull();
		Ensure.That(random, nameof(random)).IsNotNull();

		return Enumerate();

		IEnumerable<object> Enumerate()
		{
			yield return this;
			foreach (var step in this.HandleGenerating(map, random))
			{
				yield return step;
			}
		}
	}

	protected abstract IEnumerable<object> HandleGenerating(Map map, System.Random random);
}
