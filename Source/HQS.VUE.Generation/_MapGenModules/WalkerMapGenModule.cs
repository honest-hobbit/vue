﻿namespace HQS.VUE.Generation;

[CreateAssetMenu(menuName = "VUE/Maps/Walker MapGenModule")]
public class WalkerMapGenModule : MapGenModule
{
	private readonly WalkerGenerator walkers = new WalkerGenerator();

	[SerializeField]
	private MinMaxInt totalPaths = new MinMaxInt(100, 100);

	public MinMaxInt TotalPaths
	{
		get => this.totalPaths;
		set
		{
			this.totalPaths = value;
			this.OnValidate();
		}
	}

	/// <inheritdoc />
	protected override IEnumerable<object> HandleGenerating(Map map, System.Random random)
	{
		int totalPaths = this.totalPaths.GetRandomValue(random);
		return this.walkers.Generate().TakeWhile(path => map.Paths.Count < totalPaths);
	}

	private void OnValidate()
	{
		this.totalPaths.Min = this.totalPaths.Min.ClampLower(0);
		this.totalPaths.Max = this.totalPaths.Max.ClampLower(0);
		this.totalPaths = this.totalPaths.Order();
	}
}
