﻿namespace HQS.VUE.Generation;

[CreateAssetMenu(menuName = "VUE/Maps/Elevation MapGenModule")]
public class ElevationMapGenModule : MapGenModule
{
	private readonly FastNoiseLite noise = new FastNoiseLite();

	[SerializeField]
	private float frequency = 1;

	[SerializeField]
	private int octaves = 1;

	[SerializeField]
	private MinMaxInt elevations = new MinMaxInt(0, 0);

	public float Frequency
	{
		get => this.frequency;
		set
		{
			this.frequency = value;
			this.OnValidate();
		}
	}

	public int Octaves
	{
		get => this.octaves;
		set
		{
			this.octaves = value;
			this.OnValidate();
		}
	}

	public MinMaxInt Elevations
	{
		get => this.elevations;
		set
		{
			this.elevations = value;
			this.OnValidate();
		}
	}

	/// <inheritdoc />
	protected override IEnumerable<object> HandleGenerating(Map map, System.Random random)
	{
		this.noise.SetSeed(random.Next());
		this.noise.SetFrequency(this.frequency);
		this.noise.SetFractalOctaves(this.octaves);

		int zoneLength = map.CellsPerZoneSide;
		foreach (var zone in map.Zones.Values)
		{
			for (int iX = 0; iX < zoneLength; iX++)
			{
				for (int iY = 0; iY < zoneLength; iY++)
				{
					var cell = zone[iX, iY];
					float elevation = this.noise.GetNoise(cell.Key.X, cell.Key.Y);
					elevation = this.elevations.Min == this.elevations.Max ? this.elevations.Min :
						(float)Interpolation.ConvertRange(elevation, -1, 1, this.elevations.Min, this.elevations.Max);
					cell.Elevation = Mathf.RoundToInt(elevation);
					yield return cell;
				}
			}
		}
	}

	private void OnValidate()
	{
		this.frequency = this.frequency.ClampLower(0);
		this.octaves = this.octaves.ClampLower(1);
		this.elevations = this.elevations.Order();
	}
}
