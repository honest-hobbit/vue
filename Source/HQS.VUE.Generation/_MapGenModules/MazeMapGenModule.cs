﻿namespace HQS.VUE.Generation;

[CreateAssetMenu(menuName = "VUE/Maps/Maze MapGenModule")]
public class MazeMapGenModule : MapGenModule
{
	private readonly GrowingTreeMazeGenerator generator;

	[SerializeField]
	private MinMaxInt startX = new MinMaxInt(0, 0);

	[SerializeField]
	private MinMaxInt startY = new MinMaxInt(0, 0);

	[SerializeField]
	[Range(0, 1)]
	private float newestVsRandom = 1;

	[SerializeField]
	[Range(0, 1)]
	private float horizontalVsVertical;

	[SerializeField]
	[Range(0, 1)]
	private float newLinkVsRelink;

	[SerializeField]
	private MinMaxInt totalPaths = new MinMaxInt(100, 100);

	public MazeMapGenModule()
	{
		this.generator = new GrowingTreeMazeGenerator()
		{
			PathSelector = (random, count) =>
				(random.NextDouble() < this.newestVsRandom) ? count - 1 : random.Next(count),
		};
	}

	public MinMaxInt StartX
	{
		get => this.startX;
		set
		{
			this.startX = value;
			this.OnValidate();
		}
	}

	public MinMaxInt StartY
	{
		get => this.startY;
		set
		{
			this.startY = value;
			this.OnValidate();
		}
	}

	public float NewestVsRandom
	{
		get => this.newestVsRandom;
		set
		{
			this.newestVsRandom = value;
			this.OnValidate();
		}
	}

	public float HorizontalVsVertical
	{
		get => this.horizontalVsVertical;
		set
		{
			this.horizontalVsVertical = value;
			this.OnValidate();
		}
	}

	public float NewLinkVsRelink
	{
		get => this.newLinkVsRelink;
		set
		{
			this.newLinkVsRelink = value;
			this.OnValidate();
		}
	}

	public MinMaxInt TotalPaths
	{
		get => this.totalPaths;
		set
		{
			this.totalPaths = value;
			this.OnValidate();
		}
	}

	/// <inheritdoc />
	protected override IEnumerable<object> HandleGenerating(Map map, System.Random random)
	{
		this.generator.HorizontalVsVertical = this.horizontalVsVertical;
		this.generator.NewLinkVsRelink = this.newLinkVsRelink;

		var start = map.Paths[this.startX.GetRandomValue(random), this.startY.GetRandomValue(random), 0];
		int totalPaths = this.totalPaths.GetRandomValue(random);

		return this.generator.Generate(start, random).TakeWhile(path => map.Paths.Count < totalPaths);
	}

	private void OnValidate()
	{
		this.startX = this.startX.Order();
		this.startY = this.startY.Order();

		this.newestVsRandom = this.newestVsRandom.Clamp(0, 1);
		this.horizontalVsVertical = this.horizontalVsVertical.Clamp(0, 1);
		this.newLinkVsRelink = this.newLinkVsRelink.Clamp(0, 1);

		this.totalPaths.Min = this.totalPaths.Min.ClampLower(0);
		this.totalPaths.Max = this.totalPaths.Max.ClampLower(0);
		this.totalPaths = this.totalPaths.Order();
	}
}
