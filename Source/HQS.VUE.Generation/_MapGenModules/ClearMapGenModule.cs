﻿namespace HQS.VUE.Generation;

[CreateAssetMenu(menuName = "VUE/Maps/Clear All MapGenModule")]
public class ClearMapGenModule : MapGenModule
{
	/// <inheritdoc />
	protected override IEnumerable<object> HandleGenerating(Map map, System.Random random)
	{
		map.Clear();
		yield break;
	}
}
