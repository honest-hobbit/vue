﻿namespace HQS.VUE.Generation;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
[CreateAssetMenu(menuName = "VUE/Maps/Update Links MapGenModule")]
public class UpdateLinksMapGenModule : MapGenModule
{
	[SerializeField]
	private Change action = Change.LinkAll;

	[SerializeField]
	private bool includeHorizontal = true;

	[SerializeField]
	private bool includeVertical = true;

	public bool IncludeHorizontal
	{
		get => this.includeHorizontal;
		set => this.includeHorizontal = value;
	}

	public bool IncludeVertical
	{
		get => this.includeVertical;
		set => this.includeVertical = value;
	}

	/// <inheritdoc />
	protected override IEnumerable<object> HandleGenerating(Map map, System.Random random)
	{
		map.Snapshot.SetSnapshot();
		switch (this.action)
		{
			case Change.LinkAll:
				foreach (var path in map.Snapshot.Paths)
				{
					path.LinkToAllThatExist(this.includeHorizontal, this.includeVertical);
					yield return path;
				}

				break;

			case Change.UnlinkAll:
				foreach (var path in map.Snapshot.Paths)
				{
					path.UnlinkFromAll(this.includeHorizontal, this.includeVertical);
					yield return path;
				}

				break;
		}
	}

	public enum Change
	{
		LinkAll,
		UnlinkAll,
	}
}
