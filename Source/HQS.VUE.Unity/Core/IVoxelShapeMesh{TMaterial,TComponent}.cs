﻿namespace HQS.VUE.Unity.Core;

public interface IVoxelShapeMesh<TMaterial, TComponent>
	where TComponent : VoxelShapeComponent<TMaterial>
{
	Guid VoxelShapeKey { get; set; }

	Guid SurfaceToTriangleMapKey { get; set; }

	IDictionary<Guid, TMaterial> Materials { get; }

	IReadOnlyList<TComponent> GameObjects { get; }

	TComponent CreateGameObject();

	// TODO maybe call this Unload to be consistent with VUE?
	void Destroy();
}
