﻿namespace HQS.VUE.Unity.Core;

public interface IVoxelGraphicShape : IVoxelShapeMesh<Material, VoxelGraphicComponent>
{
}
