﻿namespace HQS.VUE.Unity.Core;

public interface IUnityVoxelUniverse : IVoxelUniverse
{
	////IDiagnostics Diagnostics { get; }

	IDictionary<Guid, Material> GraphicMaterials { get; }

	IDictionary<Guid, PhysicMaterial> PhysicMaterials { get; }

	IReadOnlyList<IVoxelGraphicShape> GraphicShapes { get; }

	IReadOnlyList<IVoxelPhysicShape> PhysicShapes { get; }

	IVoxelGraphicShape CreateGraphicShape();

	IVoxelPhysicShape CreatePhysicShape();
}
