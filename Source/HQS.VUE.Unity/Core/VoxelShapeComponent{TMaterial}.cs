﻿namespace HQS.VUE.Unity.Core;

public abstract class VoxelShapeComponent<TMaterial> : MonoBehaviour
{
	public IDictionary<Guid, TMaterial> Materials { get; }

	// TODO maybe call this Unload to be consistent with VUE?
	public void Destroy()
	{
	}
}
