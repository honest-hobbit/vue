﻿namespace HQS.VUE.Unity.Core;

public interface IVoxelPhysicShape : IVoxelShapeMesh<PhysicMaterial, VoxelPhysicComponent>
{
}
