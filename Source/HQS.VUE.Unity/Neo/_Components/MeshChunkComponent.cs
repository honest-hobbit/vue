﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public class MeshChunkComponent : MonoBehaviour
{
	public bool ShareVertices = true;

	public bool ScaleOutput = false;

	public Material Material = null;

	public Texture2D[] Textures = null;

	public bool Log = false;

	// TODO not to be assigned in the Inspector or by other scripts
	public GameObject ChunkGameObject = null;

	// TODO not to be assigned in the Inspector or by other scripts
	public MeshCollider ChunkCollider = null;

	// TODO not to be assigned in the Inspector or by other scripts
	public Mesh ChunkMesh = null;

	private MeshChunkBuilder<Surface16> meshChunkBuilder;

	public void UpdateMesh(
		ChunkSize size, CoplanarChunk<Surface16> chunk, CubeChunkComponent source, Int3 voxelOffset)
	{
		this.UpdateMesh(size, chunk);
		var link = this.ChunkGameObject.GetOrAddComponent<CubeChunkSourceComponent>();
		link.Source = source;
		link.VoxelOffset = voxelOffset;
	}

	public void UpdateMesh(
		ChunkSize size, CoplanarChunk<Surface16> chunk, WedgeChunkComponent source, Int3 voxelOffset)
	{
		this.UpdateMesh(size, chunk);
		var link = this.ChunkGameObject.GetOrAddComponent<WedgeChunkSourceComponent>();
		link.Source = source;
		link.VoxelOffset = voxelOffset;
	}

	private void UpdateMesh(ChunkSize size, CoplanarChunk<Surface16> chunk)
	{
		size.Validate(nameof(size));

		ContourerGameObjectUtility.Resize(this.ChunkGameObject.transform, this.ScaleOutput, size);

		var logger = this.Log ? new StringBuilder() : null;

		this.meshChunkBuilder.BuildMesh(chunk, this.ChunkMesh, logger);

		// MeshCollider.sharedMesh has to be reassigned whenever Mesh is updated
		// MeshFilter.sharedMesh does not
		this.ChunkCollider.sharedMesh = this.ChunkMesh;

		if (logger != null)
		{
			UnityEngine.Debug.Log(logger.ToString());
		}
	}

	private void Awake()
	{
		if (this.ChunkGameObject != null) { DestroyImmediate(this.ChunkGameObject); }
		if (this.ChunkCollider != null) { DestroyImmediate(this.ChunkMesh); }
		if (this.ChunkMesh != null) { DestroyImmediate(this.ChunkMesh); }

		var material = Instantiate(this.Material);
		if (this.Textures != null)
		{
			material.SetTexture("_MainTex", Texture2DArrayUtility.CreateTextureArray(this.Textures));
		}
		else
		{
			UnityEngine.Debug.Log($"{nameof(this.Textures)} must not be null.");
		}

		this.ChunkGameObject = new GameObject("Chunk");
		this.ChunkGameObject.transform.SetParent(this.gameObject.transform, false);

		var renderer = this.ChunkGameObject.AddComponent<MeshRenderer>();
		renderer.material = material;
		renderer.shadowCastingMode = ShadowCastingMode.TwoSided;

		this.ChunkMesh = new Mesh();
		this.ChunkMesh.indexFormat = IndexFormat.UInt32;

		// MeshFilter.sharedMesh can be set a single time and then the Mesh updated
		this.ChunkGameObject.AddComponent<MeshFilter>().sharedMesh = this.ChunkMesh;

		// MeshCollider.sharedMesh has to be reassigned whenever Mesh is updated
		this.ChunkCollider = this.ChunkGameObject.AddComponent<MeshCollider>();

		// TODO what happens if this isn't ChunkSize.Max?
		this.meshChunkBuilder = new MeshChunkBuilder<Surface16>(ChunkSize.Max, a => a - 1)
		{
			ShareVertices = this.ShareVertices,
		};
	}
}
