﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public class ContourerBuilderComponent : MonoBehaviour
{
	[Range(ChunkSize.MinExponent, ChunkSize.MaxExponent)]
	public int ChunkSizeExponent = ChunkSize.MaxExponent;

	// TODO maybe this shouldn't be here?
	public bool UseSimpleTriangles = false;

	public ChunkSize Size => new ChunkSize(this.ChunkSizeExponent);

	private readonly Lazy<ContourerBuilder<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>>> builder;

	public ContourerBuilderComponent()
	{
		this.builder = new Lazy<ContourerBuilder<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>>>(
			() => ContourerBuilder.CreatePalette(new ChunkSize(this.ChunkSizeExponent), this.UseSimpleTriangles));
	}

	public ContourerBuilder<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>> Builder => this.builder.Value;

	private void OnValidate()
	{
		this.ChunkSizeExponent = this.ChunkSizeExponent.Clamp(ChunkSize.MinExponent, ChunkSize.MaxExponent);
	}
}
