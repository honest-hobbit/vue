﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public abstract class AbstractChunkContourerComponent : MonoBehaviour
{
	public ContourerBuilderComponent BuilderComponent;

	public Int3 Origin = Int3.Zero;

	public Int3 Dimensions = Int3.Zero;

	public bool ClampToBounds = true;

	public int MaxDegreeOfParallelism = DegreeOfParallelism.Unbounded;

	//public bool UseSimpleTriangles = false;

	public bool SortAndCompact = false;

	public bool CheckForMissedTriangles = false;

	public bool Log = false;

	public bool Benchmark = false;

	public int BenchmarkWarmup = 1;

	public int BenchmarkWorkload = 10;

	protected ContourerBuilder<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>> builder;

	protected void SetBounds(IContouringBounds<Int3> bounds)
	{
		Ensure.That(bounds, nameof(bounds)).IsNotNull();

		if (this.ClampToBounds)
		{
			bounds.ClampToBounds(ref this.Origin, ref this.Dimensions);
		}

		bounds.SetBounds(this.Origin, this.Dimensions);
	}

	protected virtual void OnValidate()
	{
		this.MaxDegreeOfParallelism = DegreeOfParallelism.ClampToProcessorCount(this.MaxDegreeOfParallelism);
	}

	protected virtual void Awake()
	{
		if (this.BuilderComponent != null) { this.builder = this.BuilderComponent.Builder; }
	}
}
