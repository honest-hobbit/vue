﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public class FillNoiseWedgeChunkComponent : MonoBehaviour
{
	public WedgeChunkComponent ChunkComponent;

	public NoiseWedgeChunkFiller ChunkFiller;

	public bool RandomizeSeed = false;

	private void Start()
	{
		if (this.ChunkComponent == null || this.ChunkFiller == null) { return; }

		var builderPool = this.ChunkComponent.ChunkPools.Builders;
		var chunkBuilder = builderPool.Rent();
		chunkBuilder.Initialize(this.ChunkComponent.Size, 1);

		if (this.RandomizeSeed) { this.ChunkFiller.Seed = UnityEngine.Random.Range(int.MinValue, int.MaxValue); }

		try
		{
			this.ChunkFiller.FillChunk(chunkBuilder);
			this.ChunkComponent.UpdateChunk(chunkBuilder.Build());
		}
		finally
		{
			builderPool.Return(chunkBuilder);
		}
	}
}
