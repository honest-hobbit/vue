﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public class WedgeChunkContourerComponent : AbstractChunkContourerComponent
{
	public WedgeTableAsset TableAsset;

	public WedgeChunkComponent WedgeChunkComponent;

	public MeshChunkComponent MeshChunkComponent;

	private IWedgeChunkContourer<Voxel16, Surface16, VoxelPalette<Voxel16, Surface16>> chunkContourer;

	private IDisposable chunkUpdatedSubscription;

	protected override void Awake()
	{
		base.Awake();

		if (this.builder == null || this.TableAsset == null || this.WedgeChunkComponent == null) { return; }

		this.chunkContourer = this.builder.CreateWedgeChunkContourer();
		this.chunkContourer.Wedges = this.TableAsset.Table;
		this.chunkContourer.MaxDegreeOfParallelism = this.MaxDegreeOfParallelism;
		this.chunkContourer.SortAndCompactOutput = this.SortAndCompact;
		this.chunkContourer.CheckForTrianglesMissed = this.CheckForMissedTriangles;
		this.SetBounds(this.chunkContourer.Bounds);

		this.chunkUpdatedSubscription = this.WedgeChunkComponent.ChunkUpdated.Subscribe(this.ContourChunk);
	}

	private void OnDestroy()
	{
		this.chunkUpdatedSubscription?.Dispose();
	}

	private void ContourChunk(WedgeChunk<Voxel16> wedgeChunk)
	{
		Ensure.That(wedgeChunk, nameof(wedgeChunk)).IsAssigned();

		if (this.chunkContourer == null) { return; }

		if (this.Benchmark)
		{
			for (int i = 0; i < this.BenchmarkWarmup; i++)
			{
				this.builder.CoplanarChunkPools.ReturnChunk(this.chunkContourer.ContourChunk(wedgeChunk));
			}

			this.chunkContourer.Profiler.Reset();
			for (int i = 0; i < this.BenchmarkWorkload; i++)
			{
				this.builder.CoplanarChunkPools.ReturnChunk(this.chunkContourer.ContourChunk(wedgeChunk));
			}
		}

		var coplanarChunk = this.chunkContourer.ContourChunk(wedgeChunk);

		if (this.Log || this.Benchmark)
		{
			var logger = new StringBuilder();
			this.chunkContourer.Profiler.AppendChunkAveragesTo(logger);
			UnityEngine.Debug.Log(logger.ToString());
		}

		if (this.MeshChunkComponent != null)
		{
			var chunkSize = this.chunkContourer.Bounds.Size;
			int offset = (ChunkSize.Max.SideLength - chunkSize.SideLength) >> 1;
			this.MeshChunkComponent.UpdateMesh(
				chunkSize, coplanarChunk, this.WedgeChunkComponent, new Int3(offset));
		}

		this.builder.CoplanarChunkPools.ReturnChunk(coplanarChunk);
	}
}
