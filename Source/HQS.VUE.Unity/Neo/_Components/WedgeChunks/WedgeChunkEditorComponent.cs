﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class WedgeChunkEditorComponent : MonoBehaviour
{
	public Camera Camera;

	public Wedge16 AddVoxelTypeA = new Wedge16(1, 1);

	public Wedge16 AddVoxelTypeB = new Wedge16(0, 0);

	private void Update()
	{
		if (this.Camera == null) { return; }

		if (Input.GetMouseButtonDown(MouseButton.Left))
		{
			if (this.RaycastVoxel(out var voxelHit))
			{
				if (Input.GetKey(KeyCode.LeftShift))
				{
					// printing voxel contents
					var wedges = voxelHit.Source.Chunk.Voxels[voxelHit.HitIndex];
					UnityEngine.Debug.Log(string.Join(", ", wedges.ToArray()));
					return;
				}

				// removing a voxel
				var chunk = voxelHit.Source.Chunk;
				var buildersPool = voxelHit.Source.ChunkPools.Builders;
				var builder = buildersPool.Rent();

				try
				{
					builder.Initialize(chunk);
					builder.IsUniform = false;

					var wedges = builder.Voxels[voxelHit.HitIndex];
					wedges.Fill(Wedge<Voxel16>.Empty);

					chunk = builder.Build();
					voxelHit.Source.UpdateChunk(chunk);
				}
				finally
				{
					buildersPool.Return(builder);
				}
			}
		}
		else if (Input.GetMouseButtonDown(MouseButton.Right))
		{
			if (this.RaycastVoxel(out var voxelHit))
			{
				// adding a voxel
				var chunk = voxelHit.Source.Chunk;
				if (chunk.Size.AsCube.IsInBounds(voxelHit.AdjacentIndex))
				{
					var buildersPool = voxelHit.Source.ChunkPools.Builders;
					var builder = buildersPool.Rent();

					try
					{
						builder.Initialize(chunk);
						builder.IsUniform = false;

						if (this.AddVoxelTypeB.Shape == 0)
						{
							var wedges = builder.Voxels[voxelHit.AdjacentIndex];
							wedges.Fill(Wedge<Voxel16>.Empty);
							wedges[0] = this.AddVoxelTypeA;
						}
						else
						{
							builder.SetChannels(builder.Channels.ClampLower(2));

							var wedges = builder.Voxels[voxelHit.AdjacentIndex];
							wedges.Fill(Wedge<Voxel16>.Empty);
							wedges[0] = this.AddVoxelTypeA;
							wedges[1] = this.AddVoxelTypeB;
						}

						chunk = builder.Build();
						voxelHit.Source.UpdateChunk(chunk);
					}
					finally
					{
						buildersPool.Return(builder);
					}
				}
			}
		}
	}

	private bool RaycastVoxel(out VoxelHit<WedgeChunkComponent> voxelHit)
	{
		var ray = this.Camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
		return VoxelPhysics.Raycast(ray, out voxelHit);
	}
}
