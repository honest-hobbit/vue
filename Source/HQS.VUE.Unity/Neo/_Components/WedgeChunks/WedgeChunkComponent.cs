﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[SuppressMessage("Design", "CA1001", Justification = "OnDestroy disposes.")]
[ExecuteInEditMode]
public class WedgeChunkComponent : MonoBehaviour
{
	public ContourerBuilderComponent BuilderComponent;

	public ChunkSize Size { get; private set; } = ChunkSize.Invalid;

	public WedgeChunkPools<Voxel16> ChunkPools { get; private set; }

	public WedgeChunk<Voxel16> Chunk { get; private set; }

	public IObservable<WedgeChunk<Voxel16>> ChunkUpdated => this.chunkUpdated.AsObservable();

	private readonly Subject<WedgeChunk<Voxel16>> chunkUpdated = new Subject<WedgeChunk<Voxel16>>();

	public void UpdateChunk(WedgeChunk<Voxel16> chunk)
	{
		Ensure.That(chunk, nameof(chunk)).IsAssigned();

		if (!this.Chunk.IsDefault) { this.ChunkPools.ReturnChunk(this.Chunk); }

		this.Chunk = chunk;
		this.chunkUpdated.OnNext(chunk);
	}

	private void Awake()
	{
		if (this.BuilderComponent == null) { return; }

		var builder = this.BuilderComponent.Builder;
		this.Size = builder.Size;
		this.ChunkPools = builder.WedgeChunkPools;
	}

	private void OnDestroy()
	{
		this.chunkUpdated.OnCompleted();
		this.chunkUpdated.Dispose();
	}
}
