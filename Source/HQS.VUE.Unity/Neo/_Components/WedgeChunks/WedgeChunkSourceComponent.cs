﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class WedgeChunkSourceComponent : MonoBehaviour
{
	public WedgeChunkComponent Source;

	public Int3 VoxelOffset;
}
