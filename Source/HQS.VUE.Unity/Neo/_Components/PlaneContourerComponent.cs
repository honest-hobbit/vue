﻿namespace HQS.VUE.Unity.Neo;

// TODO temporary prototyping purposes only
[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public class PlaneContourerComponent : MonoBehaviour
{
	public int VoxelSizeExponent = 3;

	public int OriginX = 0;

	public int OriginY = 0;

	public int DimensionsX = 15;

	public int DimensionsY = 15;

	public bool ClampToBounds = true;

	public bool AutoOffset = true;

	public int OffsetX = 0;

	public int OffsetY = 0;

	public PlaneNormal Normal;

	public int Depth;

	public bool UseSimpleTriangles = false;

	public bool ShareVertices = true;

	public bool ScaleOutput = false;

	public bool Log = false;

	public bool CheckForMissedTriangles = false;

	public bool CheckTrianglesMatchVoxels = false;

	public Material Material = null;

	public Texture2D[] Textures = null;

	public SurfacePattern Pattern = SurfacePattern.Empty;

	public ushort VoxelTypeA = 0;

	public ushort VoxelTypeB = 1;

	public ushort VoxelTypeC = 2;

	public ushort VoxelTypeD = 3;

	public ushort VoxelTypeE = 4;

	public bool RandomizeNoiseSeed = false;

	public int NoiseSeed = 0;

	public float NoiseFrequency = .1f;

	public float NoiseWeight = 1f;

	protected virtual void OnValidate()
	{
		this.VoxelSizeExponent = this.VoxelSizeExponent.Clamp(ChunkSize.MinExponent, ChunkSize.MaxExponent);

		if (this.AutoOffset)
		{
			int offset = (ChunkSize.Max.SideLength - new ChunkSize(this.VoxelSizeExponent).SideLength) >> 1;
			this.OffsetX = offset;
			this.OffsetY = offset;
		}
	}

	protected virtual void Start()
	{
		this.GenerateMeshes();
	}

	private void GenerateMeshes()
	{
		foreach (var child in this.GetComponentsInChildren<TagComponent>())
		{
			DestroyImmediate(child.gameObject);
		}

		if (this.Textures == null)
		{
			UnityEngine.Debug.Log($"{nameof(this.Textures)} must not be null.");
			return;
		}

		var voxelSize = new ChunkSize(this.VoxelSizeExponent);
		var origin = new Int2(this.OriginX, this.OriginY);
		var dimensions = new Int2(this.DimensionsX, this.DimensionsY);

		var contourerBuilder = ContourerBuilder.Create(voxelSize, this.UseSimpleTriangles);

		var planeContourer = contourerBuilder.CreatePlaneContourer();
		planeContourer.CheckForTrianglesMissed = this.CheckForMissedTriangles;
		planeContourer.OffsetOutput = new Int2(this.OffsetX, this.OffsetY);

		if (this.ClampToBounds)
		{
			planeContourer.Bounds.ClampToBounds(ref origin, ref dimensions);

			this.OriginX = origin.X;
			this.OriginY = origin.Y;
			this.DimensionsX = dimensions.X;
			this.DimensionsY = dimensions.Y;
		}

		planeContourer.Bounds.SetBounds(origin, dimensions);

		var chunkBuilder = contourerBuilder.CreateCoplanarChunkBuilder();
		planeContourer.Output = chunkBuilder;

		if (this.RandomizeNoiseSeed)
		{
			this.NoiseSeed = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
		}

		var planeFiller = new SurfacePlaneFiller()
		{
			Pattern = this.Pattern,
			SurfaceTypeA = this.VoxelTypeA,
			SurfaceTypeB = this.VoxelTypeB,
			SurfaceTypeC = this.VoxelTypeC,
			SurfaceTypeD = this.VoxelTypeD,
			SurfaceTypeE = this.VoxelTypeE,
			NoiseSeed = this.NoiseSeed,
			NoiseFrequency = this.NoiseFrequency,
			NoiseWeight = this.NoiseWeight,
			NoiseCountOfVoxelTypes = this.Textures.Length,
		};

		planeFiller.FillPlane(planeContourer.Surfaces);
		planeContourer.ReverseOutput = this.Normal.GetReverseOutput();
		planeContourer.ContourPlane(PlaneIndex.From(this.Normal, this.Depth));

		var chunk = chunkBuilder.Build();

		var meshChunkBuilder = new MeshChunkBuilder<Surface16>(ChunkSize.Max, a => a - 1)
		{
			ShareVertices = this.ShareVertices,
		};

		var mesh = new Mesh();
		mesh.indexFormat = IndexFormat.UInt32;
		if (!this.Log)
		{
			meshChunkBuilder.BuildMesh(chunk, mesh);
		}
		else
		{
			var logger = new StringBuilder();
			planeContourer.Profiler.AppendPlaneTotalsTo(logger);
			meshChunkBuilder.BuildMesh(chunk, mesh, logger);
			UnityEngine.Debug.Log(logger.ToString());
		}

		contourerBuilder.CoplanarChunkPools.ReturnChunk(chunk);

		var material = Instantiate(this.Material);
		material.SetTexture("_MainTex", Texture2DArrayUtility.CreateTextureArray(this.Textures));

		var meshChild = new GameObject();
		meshChild.transform.SetParent(this.gameObject.transform, false);
		ContourerGameObjectUtility.Resize(meshChild.transform, this.ScaleOutput, voxelSize);
		meshChild.AddComponent<MeshFilter>().sharedMesh = mesh;
		meshChild.AddComponent<MeshRenderer>().material = material;
		var tags = meshChild.AddComponent<TagComponent>();

		if (this.CheckTrianglesMatchVoxels)
		{
			if (this.Normal != PlaneNormal.FaceNegZ)
			{
				Console.WriteLine($"Unable to check triangles match voxels because {nameof(this.Normal)} isn't {nameof(PlaneNormal.FaceNegZ)}");
			}
			else
			{
				meshChild.AddComponent<Rigidbody>().isKinematic = true;
				var meshCollider = meshChild.AddComponent<MeshCollider>();
				meshCollider.sharedMesh = mesh;

				this.HandleCheckingTrianglesMatchVoxels(planeContourer, meshCollider);
			}
		}
	}

	// TODO this might be broken
	private void HandleCheckingTrianglesMatchVoxels(
		IPlaneContourer<Surface16, Voxel16SurfaceExtractor> contourer, MeshCollider meshCollider)
	{
		Debug.Assert(contourer != null);
		Debug.Assert(meshCollider != null);

		var planeTable = new PlaneTo3DTransformer()
		{
			Size = ChunkSize.Max,
			Normal = this.Normal,
			Depth = this.Depth,
		};

		var triangles = meshCollider.sharedMesh.triangles;
		var uvs = new List<Vector3>();
		meshCollider.sharedMesh.GetUVs(0, uvs);

		var top = new Vector3(0, .5f, 0);
		var right = new Vector3(.5f, 0, 0);

		var transform = meshCollider.transform;
		var normal = this.Normal.ToVector().ToUnity();
		var direction = transform.TransformDirection(-normal);

		var min = contourer.Bounds.Lower;
		var max = contourer.Bounds.Upper;

		int errors = 0;

		for (int iY = min.Y; iY <= max.Y; iY++)
		{
			for (int iX = min.X; iX <= max.X; iX++)
			{
				var voxels = contourer.Surfaces[iX, iY];
				var point = planeTable.Transform((iX * 2) + 1, (iY * 2) + 1).ToUnityVector() + normal;

				// TODO the top and right offsets depend on Normal being FaceNegZ
				Raycast(voxels.Top, point + top);
				Raycast(voxels.Right, point + right);
				Raycast(voxels.Bottom, point - top);
				Raycast(voxels.Left, point - right);
			}
		}

		if (errors == 0)
		{
			Console.WriteLine("All triangles and voxels match.");
		}
		else
		{
			Console.WriteLine($"{errors} triangles don't match their voxels.");
		}

		void Raycast(ushort expected, Vector3 point)
		{
			var ray = new Ray(transform.TransformPoint(point), direction);
			if (meshCollider.Raycast(ray, out var hit, 1000))
			{
				int texture = (int)uvs[triangles[hit.triangleIndex * 3]].z;

				if (texture + 1 != expected)
				{
					errors++;
				}
			}
			else
			{
				if (expected != 0)
				{
					errors++;
				}
			}
		}
	}
}
