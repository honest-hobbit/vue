﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public class FillDebugCubeChunkComponent : MonoBehaviour
{
	public CubeChunkComponent ChunkComponent;

	public DebugCubeChunkFiller ChunkFiller;

	private void Start()
	{
		if (this.ChunkComponent == null || this.ChunkFiller == null) { return; }

		var builderPool = this.ChunkComponent.ChunkPools.Builders;
		var chunkBuilder = builderPool.Rent();
		chunkBuilder.Initialize(this.ChunkComponent.Size);

		try
		{
			this.ChunkFiller.FillChunk(chunkBuilder);
			this.ChunkComponent.UpdateChunk(chunkBuilder.Build());
		}
		finally
		{
			builderPool.Return(chunkBuilder);
		}
	}
}
