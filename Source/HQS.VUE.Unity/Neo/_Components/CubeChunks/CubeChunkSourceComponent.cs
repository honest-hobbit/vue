﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class CubeChunkSourceComponent : MonoBehaviour
{
	public CubeChunkComponent Source;

	public Int3 VoxelOffset;
}
