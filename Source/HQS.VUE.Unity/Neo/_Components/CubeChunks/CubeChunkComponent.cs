﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[SuppressMessage("Design", "CA1001", Justification = "OnDestroy instead.")]
[ExecuteInEditMode]
public class CubeChunkComponent : MonoBehaviour
{
	public ContourerBuilderComponent BuilderComponent;

	public ChunkSize Size { get; private set; } = ChunkSize.Invalid;

	public CubeChunkPools<Voxel16> ChunkPools { get; private set; }

	public CubeChunk<Voxel16> Chunk { get; private set; }

	public IObservable<CubeChunk<Voxel16>> ChunkUpdated => this.chunkUpdated.AsObservable();

	private readonly Subject<CubeChunk<Voxel16>> chunkUpdated = new Subject<CubeChunk<Voxel16>>();

	public void UpdateChunk(CubeChunk<Voxel16> chunk)
	{
		Ensure.That(chunk, nameof(chunk)).IsAssigned();

		if (!this.Chunk.IsDefault) { this.ChunkPools.ReturnChunk(this.Chunk); }

		this.Chunk = chunk;
		this.chunkUpdated.OnNext(chunk);
	}

	private void Awake()
	{
		if (this.BuilderComponent == null) { return; }

		var builder = this.BuilderComponent.Builder;
		this.Size = builder.Size;
		this.ChunkPools = builder.CubeChunkPools;
	}

	private void OnDestroy()
	{
		this.chunkUpdated.OnCompleted();
		this.chunkUpdated.Dispose();
	}
}
