﻿namespace HQS.VUE.Unity.Neo;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class CubeChunkEditorComponent : MonoBehaviour
{
	public Camera Camera;

	public ushort AddVoxelType = 1;

	private void Update()
	{
		if (this.Camera == null) { return; }

		if (Input.GetMouseButtonDown(MouseButton.Left))
		{
			if (this.RaycastVoxel(out var voxelHit))
			{
				// removing a voxel
				var chunk = voxelHit.Source.Chunk;
				var buildersPool = voxelHit.Source.ChunkPools.Builders;
				var builder = buildersPool.Rent();

				try
				{
					builder.Initialize(chunk);
					builder.IsUniform = false;
					var voxels = builder.Voxels;
					voxels[voxelHit.HitIndex] = 0;

					chunk = builder.Build();
					voxelHit.Source.UpdateChunk(chunk);
				}
				finally
				{
					buildersPool.Return(builder);
				}
			}
		}
		else if (Input.GetMouseButtonDown(MouseButton.Right))
		{
			if (this.RaycastVoxel(out var voxelHit))
			{
				// adding a voxel
				var chunk = voxelHit.Source.Chunk;
				if (chunk.Size.AsCube.IsInBounds(voxelHit.AdjacentIndex))
				{
					var buildersPool = voxelHit.Source.ChunkPools.Builders;
					var builder = buildersPool.Rent();

					try
					{
						builder.Initialize(chunk);
						builder.IsUniform = false;
						var voxels = builder.Voxels;
						voxels[voxelHit.AdjacentIndex] = this.AddVoxelType;

						chunk = builder.Build();
						voxelHit.Source.UpdateChunk(chunk);
					}
					finally
					{
						buildersPool.Return(builder);
					}
				}
			}
		}
	}

	private bool RaycastVoxel(out VoxelHit<CubeChunkComponent> voxelHit)
	{
		var ray = this.Camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
		return VoxelPhysics.Raycast(ray, out voxelHit);
	}
}
