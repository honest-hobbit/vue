﻿#if UNITY_EDITOR

namespace HQS.VUE.Unity.Neo;

[CustomPropertyDrawer(typeof(Wedge16))]
public class Wedge16Drawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUIUtility.singleLineHeight * (EditorGUIUtility.wideMode ? 1 : 2);
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		// Find the SerializedProperties by name
		var shape = property.FindPropertyRelative(nameof(Wedge16.Shape));
		var data = property.FindPropertyRelative(nameof(Wedge16.Data));

		// Using BeginProperty / EndProperty on the parent property means that
		// prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Keyboard), label);
		var left = new Rect(position.x, position.y, (position.width / 2) - 5, position.height);
		var right = new Rect(position.x + position.width - left.width, position.y, left.width, position.height);

		EditorGUI.indentLevel = 0;
		EditorGUIUtility.labelWidth = 45;
		shape.intValue = EditorGUI.IntField(left, shape.displayName, shape.intValue);
		EditorGUIUtility.labelWidth = 35;
		data.intValue = EditorGUI.IntField(right, data.displayName, data.intValue);

		EditorGUI.EndProperty();
	}
}

#endif
