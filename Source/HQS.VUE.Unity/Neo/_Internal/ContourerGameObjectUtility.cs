﻿namespace HQS.VUE.Unity.Neo;

internal static class ContourerGameObjectUtility
{
	public static void Resize(Transform transform, bool scaleOutput, ChunkSize voxelSize)
	{
		Debug.Assert(transform != null);
		voxelSize.AssertValid();

		if (scaleOutput)
		{
			float length = (float)Math.Pow(2, ChunkSize.MaxExponent - voxelSize.Exponent) - 1;
			transform.localPosition = -new Vector3(length, length, length);
			transform.localScale = Vector3.one / voxelSize.SideLength;
		}
		else
		{
			transform.localScale = Vector3.one / ChunkSize.Max.SideLength;
		}
	}
}
