﻿namespace HQS.VUE.Unity.Neo;

internal class MeshChunkBuilder<TSurface>
	where TSurface : unmanaged
{
	private readonly Stopwatch timer = new Stopwatch();

	private readonly List<Vector3> vertices = new List<Vector3>();

	private readonly List<Vector3> normals = new List<Vector3>();

	private readonly List<Vector3> uvs = new List<Vector3>();

	private readonly List<int> triangles = new List<int>();

	private readonly PlaneTo3DTransformer planeTable = new PlaneTo3DTransformer();

	private readonly Func<TSurface, int> getTextureIndex;

	private readonly PointToVertexIndexMap vertexMap;

	public MeshChunkBuilder(ChunkSize size, Func<TSurface, int> getTextureIndex)
	{
		size.Validate(nameof(size));
		Ensure.That(getTextureIndex, nameof(getTextureIndex)).IsNotNull();

		this.getTextureIndex = getTextureIndex;

		// TODO maybe outputSize could be changed at runtime?
		// however, VertexMap does allocate a large array
		this.planeTable.Size = size;
		this.vertexMap = new PointToVertexIndexMap(ChunkSize.Max);
	}

	public bool ShareVertices { get; set; } = true;

	public void BuildMesh(CoplanarChunk<TSurface> surfaceChunk, Mesh mesh, StringBuilder logger = null)
	{
		Ensure.That(mesh, nameof(mesh)).IsNotNull();

		this.vertexMap.Clear();
		this.vertices.Clear();
		this.normals.Clear();
		this.uvs.Clear();
		this.triangles.Clear();

		this.timer.Restart();

		if (surfaceChunk.Planes.Count > 0)
		{
			if (this.ShareVertices)
			{
				this.UnpackVerticesWithSharing(surfaceChunk);
			}
			else
			{
				this.UnpackVerticesNoSharing(surfaceChunk);
			}
		}

		this.timer.Stop();
		var unpackData = this.timer.Elapsed;
		this.timer.Restart();

		mesh.Clear();
		mesh.SetVertices(this.vertices);
		mesh.SetNormals(this.normals);
		mesh.SetUVs(0, this.uvs);
		mesh.SetTriangles(this.triangles, 0, calculateBounds: true);

		this.timer.Stop();
		var createMesh = this.timer.Elapsed;

		if (logger != null)
		{
			logger.AppendLineHeader("Build Mesh");
			logger.AppendLineCount(this.vertices.Count, nameof(this.vertices));
			logger.AppendLineTimeSpan(unpackData, nameof(unpackData));
			logger.AppendLineTimeSpan(createMesh, nameof(createMesh));
		}
	}

	[Conditional(CompilationSymbol.Debug)]
	private static void AssertEnumeratorsFinished(
		PagedValuesEnumerator<SurfaceGroup<TSurface>> surfaces,
		PagedValuesEnumerator<CompactTriangle> triangles)
	{
		Debug.Assert(!surfaces.MoveNext(), "There were still surfaces left over after processing all planes.");
		Debug.Assert(!triangles.MoveNext(), "There were still triangles left over after processing all planes.");
	}

	private void UnpackVerticesWithSharing(CoplanarChunk<TSurface> chunk)
	{
		using var surfaces = chunk.Surfaces.GetEnumerator();
		using var triangles = chunk.Triangles.GetEnumerator();
		int vertexIndex = 0;

		foreach (var planeGroup in chunk.Planes)
		{
			this.planeTable.Plane = planeGroup.Plane;
			var normal = this.planeTable.Normal.ToVector().ToUnity();

			for (int surfaceCount = 0; surfaceCount < planeGroup.SurfaceCount; surfaceCount++)
			{
				vertexIndex += this.vertexMap.VertexCount;
				this.vertexMap.Clear();

				bool surfacesMoved = surfaces.MoveNext();
				Debug.Assert(surfacesMoved);
				var surfaceGroup = surfaces.Current;

				var textureIndex = this.getTextureIndex(surfaceGroup.SurfaceType);

				for (int triangleCount = 0; triangleCount < surfaceGroup.TriangleCount; triangleCount++)
				{
					bool trianglesMoved = triangles.MoveNext();
					Debug.Assert(trianglesMoved);
					var triangle = triangles.Current;

					AddVertex(triangle.A);
					AddVertex(triangle.B);
					AddVertex(triangle.C);

					void AddVertex(CompactPoint2 vertex)
					{
						if (this.vertexMap.TryGetOrCreateVertexIndex(vertex, out var indexOffset))
						{
							this.triangles.Add(vertexIndex + indexOffset);
						}
						else
						{
							vertex.GetPosition(out var x, out var y);
							this.vertices.Add(this.planeTable.Transform(x, y).ToUnityVector());
							this.normals.Add(normal);
							this.uvs.Add(new Vector3(x / 2f, y / 2f, textureIndex));
							this.triangles.Add(vertexIndex + indexOffset);
						}
					}
				}
			}
		}

		AssertEnumeratorsFinished(surfaces, triangles);
	}

	private void UnpackVerticesNoSharing(CoplanarChunk<TSurface> chunk)
	{
		using var surfaces = chunk.Surfaces.GetEnumerator();
		using var triangles = chunk.Triangles.GetEnumerator();
		int vertexIndex = 0;

		foreach (var planeGroup in chunk.Planes)
		{
			this.planeTable.Plane = planeGroup.Plane;
			var normal = this.planeTable.Normal.ToVector().ToUnity();

			for (int surfaceCount = 0; surfaceCount < planeGroup.SurfaceCount; surfaceCount++)
			{
				bool surfacesMoved = surfaces.MoveNext();
				Debug.Assert(surfacesMoved);
				var surfaceGroup = surfaces.Current;

				var textureIndex = this.getTextureIndex(surfaceGroup.SurfaceType);

				for (int triangleCount = 0; triangleCount < surfaceGroup.TriangleCount; triangleCount++)
				{
					bool trianglesMoved = triangles.MoveNext();
					Debug.Assert(trianglesMoved);
					var triangle = triangles.Current;

					AddVertex(triangle.A);
					AddVertex(triangle.B);
					AddVertex(triangle.C);

					void AddVertex(CompactPoint2 vertex)
					{
						vertex.GetPosition(out var x, out var y);
						this.vertices.Add(this.planeTable.Transform(x, y).ToUnityVector());
						this.normals.Add(normal);
						this.uvs.Add(new Vector3(x / 2f, y / 2f, textureIndex));
						this.triangles.Add(vertexIndex);
						vertexIndex++;
					}
				}
			}
		}

		AssertEnumeratorsFinished(surfaces, triangles);
	}
}
