﻿namespace HQS.VUE.Unity.Neo;

public record struct VoxelHit<T>
{
	public T Source;

	public Int3 HitIndex;

	public Int3 AdjacentIndex;
}
