﻿namespace HQS.VUE.Unity.Neo;

public static class VoxelPhysics
{
	public static bool Raycast(
		Ray ray, out VoxelHit<CubeChunkComponent> voxelHit, float maxDistance = Mathf.Infinity) =>
		Raycast(ray, out voxelHit, out var _, maxDistance);

	public static bool Raycast(
		Ray ray,
		out VoxelHit<CubeChunkComponent> voxelHit,
		out RaycastHit raycastHit,
		float maxDistance = Mathf.Infinity)
	{
		if (!TryRaycast(ray, out raycastHit, out CubeChunkSourceComponent link, maxDistance))
		{
			voxelHit = default;
			return false;
		}

		var hitPoint = link.transform.InverseTransformPoint(raycastHit.point);
		var hitNormal = link.transform.InverseTransformDirection(raycastHit.normal);

		hitNormal *= .5f;
		voxelHit = new VoxelHit<CubeChunkComponent>()
		{
			Source = link.Source,
			HitIndex = VectorToVoxelIndex(hitPoint - hitNormal) - link.VoxelOffset,
			AdjacentIndex = VectorToVoxelIndex(hitPoint + hitNormal) - link.VoxelOffset,
		};

		return true;
	}

	public static bool Raycast(
		Ray ray, out VoxelHit<WedgeChunkComponent> voxelHit, float maxDistance = Mathf.Infinity) =>
		Raycast(ray, out voxelHit, out var _, maxDistance);

	public static bool Raycast(
		Ray ray,
		out VoxelHit<WedgeChunkComponent> voxelHit,
		out RaycastHit raycastHit,
		float maxDistance = Mathf.Infinity)
	{
		if (!TryRaycast(ray, out raycastHit, out WedgeChunkSourceComponent link, maxDistance))
		{
			voxelHit = default;
			return false;
		}

		var hitPoint = link.transform.InverseTransformPoint(raycastHit.point);
		var hitNormal = link.transform.InverseTransformDirection(raycastHit.normal);

		// check what kind of surface was hit
		if (MathUtility.AreSimiliar((hitNormal.x + hitNormal.y + hitNormal.z).Abs(), 1))
		{
			// hit a face surface between 2 voxels
			hitNormal *= .5f;
			voxelHit = new VoxelHit<WedgeChunkComponent>()
			{
				Source = link.Source,
				HitIndex = VectorToVoxelIndex(hitPoint - hitNormal) - link.VoxelOffset,
				AdjacentIndex = VectorToVoxelIndex(hitPoint + hitNormal) - link.VoxelOffset,
			};
		}
		else
		{
			// hit an edge or corner surface inside a voxel
			voxelHit = new VoxelHit<WedgeChunkComponent>()
			{
				Source = link.Source,
				HitIndex = VectorToVoxelIndex(hitPoint) - link.VoxelOffset,
				AdjacentIndex = VectorToVoxelIndex(hitPoint + (hitNormal * 1.5f)) - link.VoxelOffset,
			};
		}

		return true;
	}

	private static bool TryRaycast<T>(
		Ray ray, out RaycastHit raycastHit, out T componentHit, float maxDistance)
		where T : class
	{
		if (!Physics.Raycast(ray, out raycastHit, maxDistance))
		{
			componentHit = null;
			return false;
		}

		componentHit = raycastHit.collider.GetComponent<T>();
		return componentHit != null;
	}

	private static Int3 VectorToVoxelIndex(Vector3 point) =>
		new Int3(Mathf.FloorToInt(point.x), Mathf.FloorToInt(point.y), Mathf.FloorToInt(point.z)) >> 1;
}
