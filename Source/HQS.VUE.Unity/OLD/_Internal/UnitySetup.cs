﻿namespace HQS.VUE.Unity.OLD;

internal static class UnitySetup
{
	public static void SetUpUnityHostApplication(this Container container, UnityConfig unityConfig)
	{
		Debug.Assert(container != null);
		Debug.Assert(unityConfig != null);

		container.RegisterInstance(unityConfig);
		container.RegisterAllCommandsFromAssembly<INamespaceOLD>(typeof(UnitySetup).Assembly);
		container.RegisterAllTypeResolversFromAssembly(typeof(UnitySetup).Assembly);

		container.RegisterAllPoolsOLD(
			PooledUnityTypesOLD.EnumerateAll(),
			typeof(ChunkCollidersComponent),
			typeof(ChunkComponent),
			typeof(ChunkComponentSubchunk),
			typeof(MeshFilter),
			typeof(VerifyCollidersComponent),
			typeof(VolumeComponent));

		container.Register<ResyncUnityFrame>();
		container.Register<QuadMeshBuilder>();
		container.Register<ResyncUnityChunk>();

		container.Register<ResyncUnityVolume>();
		container.Register<ResyncUnityVolumeInstance>();

		container.RegisterSingleton<VolumeGameObjects>();
		container.RegisterSingleton<IVolumeServices, UnityVolumeServices>();

		container.RegisterSingleton<CollisionCheckManager>();
		container.RegisterSingleton<ColliderResyncManager>();
		container.RegisterSingleton<AnimatorResyncManager>();
		container.RegisterSingleton<MeshResyncManager>();

		container.RegisterSingleton<ResyncUnityPipeline>();
		container.RegisterSingleton<IResyncHostApplicationQueue, ResyncUnityQueue>();

		container.RegisterSingleton<UnityObservables>();
		container.RegisterSingleton<UnityDiagnostics>();
	}
}
