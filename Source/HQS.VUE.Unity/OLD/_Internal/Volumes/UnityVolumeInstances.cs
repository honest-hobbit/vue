﻿namespace HQS.VUE.Unity.OLD;

internal class UnityVolumeInstances : IVolumeInstances
{
	private readonly List<VolumeComponent> instances = new List<VolumeComponent>();

	private readonly VolumeGameObjects gameObjects;

	private readonly IPool<VolumeComponent> volumePool;

	public UnityVolumeInstances(Volume volume, VolumeGameObjects gameObjects, IPool<VolumeComponent> volumePool)
	{
		Debug.Assert(volume != null);
		Debug.Assert(gameObjects != null);
		Debug.Assert(volumePool != null);

		this.Volume = volume;
		this.gameObjects = gameObjects;
		this.volumePool = volumePool;
	}

	/// <inheritdoc />
	public Volume Volume { get; }

	/// <inheritdoc />
	public int Count => this.instances.Count;

	/// <inheritdoc />
	public IVolumeInstance this[int index] => this.instances[index];

	/// <inheritdoc />
	public IEnumerator<IVolumeInstance> GetEnumerator() => this.instances.GetEnumerator();

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

	/// <inheritdoc />
	public IVolumeInstance CreateInstance()
	{
		var instance = this.volumePool.Rent();
		instance.Initialize(this);
		instance.name = $"Volume";
		instance.name = $"Volume [{this.Volume.Key}]";
		instance.gameObject.SetActive(true);
		this.instances.Add(instance);

		this.Volume.ContourEntireVolume();

		return instance;
	}

	public void TrackDeactivatingInstance(VolumeComponent volume)
	{
		Debug.Assert(volume != null);
		Debug.Assert(volume.IsDeactivating);
		Debug.Assert(this.instances.Contains(volume));

		this.instances.Remove(volume);
		this.gameObjects.TrackDeactivatingVolume(volume);
	}
}
