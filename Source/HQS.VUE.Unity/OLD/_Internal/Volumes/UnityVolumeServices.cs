﻿namespace HQS.VUE.Unity.OLD;

internal class UnityVolumeServices : VolumeServices
{
	private readonly VolumeGameObjects gameObjects;

	private readonly IPool<VolumeComponent> volumePool;

	public UnityVolumeServices(
		EngineValidator validator,
		ICommandSubmitter<ContourVolumeCommand.Args> contourSubmitter,
		ICommandSubmitter<SaveVolumeCommand.Args> saveSubmitter,
		VolumeGameObjects gameObjects,
		IPool<VolumeComponent> volumePool)
		: base(validator, contourSubmitter, saveSubmitter)
	{
		Debug.Assert(gameObjects != null);
		Debug.Assert(volumePool != null);

		this.gameObjects = gameObjects;
		this.volumePool = volumePool;
	}

	/// <inheritdoc />
	public override IVolumeInstances CreateInstances(Volume volume)
	{
		Debug.Assert(volume != null);

		return new UnityVolumeInstances(volume, this.gameObjects, this.volumePool);
	}
}
