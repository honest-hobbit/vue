﻿namespace HQS.VUE.Unity.OLD;

internal class VolumeGameObjects
{
	private readonly List<VolumeComponent> deactivatingVolumes = new List<VolumeComponent>();

	private readonly IPool<VolumeComponent> volumePool;

	private readonly IPool<ChunkComponent> chunkPool;

	private readonly IPool<MeshFilter> chunkMeshPool;

	private readonly IPool<ChunkCollidersComponent> chunkCollidersPool;

	private readonly IPool<ChunkComponentSubchunk> subchunkPool;

	private readonly Predicate<VolumeComponent> removeFullyDeactivatedVolumes;

	private readonly Predicate<ChunkCollidersComponent> removeCollidersIfDisabled;

	public VolumeGameObjects(
		IPool<VolumeComponent> volumePool,
		IPool<ChunkComponent> chunkPool,
		IPool<MeshFilter> chunkMeshPool,
		IPool<ChunkCollidersComponent> chunkCollidersPool,
		IPool<ChunkComponentSubchunk> subchunkPool)
	{
		Debug.Assert(volumePool != null);
		Debug.Assert(chunkPool != null);
		Debug.Assert(chunkMeshPool != null);
		Debug.Assert(chunkCollidersPool != null);
		Debug.Assert(subchunkPool != null);

		this.volumePool = volumePool;
		this.chunkPool = chunkPool;
		this.chunkMeshPool = chunkMeshPool;
		this.chunkCollidersPool = chunkCollidersPool;
		this.subchunkPool = subchunkPool;
		this.removeFullyDeactivatedVolumes = this.RemoveFullyDeactivatedVolumes;
		this.removeCollidersIfDisabled = this.RemoveCollidersIfDisabled;
	}

	// this must only be called while on the Unity main thread
	public ChunkComponent GetChunk(ResyncUnityChunk resyncChunk, VolumeComponent volume)
	{
		Debug.Assert(resyncChunk != null);
		Debug.Assert(volume != null);

		if (volume.Chunks.TryGetValue(resyncChunk.Key.Location, out var chunk))
		{
			return chunk;
		}

		// creating new chunk
		chunk = this.chunkPool.Rent();
		chunk.name = "Chunk " + resyncChunk.Key.Location;
		chunk.Volume = volume;
		chunk.Key = resyncChunk.Key;
		chunk.VoxelIndexOffset = resyncChunk.VoxelIndexOffset;
		chunk.transform.position = chunk.VoxelIndexOffset.ToUnityVector();
		chunk.transform.SetParent(volume.transform, false);
		chunk.gameObject.SetActive(true);
		volume.Chunks.Add(resyncChunk.Key.Location, chunk);
		return chunk;
	}

	// this must only be called while on the Unity main thread
	public bool TryGetChunk(
		ResyncUnityChunk resyncChunk, bool ensureSucceeds, VolumeComponent volume, out ChunkComponent chunk)
	{
		Debug.Assert(resyncChunk != null);
		Debug.Assert(volume != null);

		if (ensureSucceeds)
		{
			chunk = this.GetChunk(resyncChunk, volume);
			return true;
		}

		return volume.Chunks.TryGetValue(resyncChunk.Key.Location, out chunk);
	}

	// this must only be called while on the Unity main thread
	public void CleanupChunk(ChunkComponent chunk)
	{
		Debug.Assert(chunk != null);

		if (chunk.CleanupColliders)
		{
			chunk.Colliders.RemoveAll(this.removeCollidersIfDisabled);
			chunk.CleanupColliders = false;
		}

		if (chunk.Meshes.Count == 0 &&
			chunk.Colliders.Count == 0 &&
			chunk.VerifyColliders.Count == 0)
		{
			chunk.Volume.Chunks.Remove(chunk.Key.Location);
			this.ClearAndReturnToPool(chunk);
		}
	}

	// this must only be called while on the Unity main thread
	public void ClearSubchunk(ChunkComponentSubchunk subchunk)
	{
		Debug.Assert(subchunk != null);

		subchunk.Chunk.ClearSubchunk(subchunk);
		this.subchunkPool.Return(subchunk);
	}

	// this must only be called while on the Unity main thread
	public void TrackDeactivatingVolume(VolumeComponent volume)
	{
		Debug.Assert(volume != null);
		Debug.Assert(volume.IsDeactivating);
		Debug.Assert(!this.deactivatingVolumes.Contains(volume));

		this.deactivatingVolumes.Add(volume);
	}

	// this must only be called while on the Unity main thread
	public void CheckDeactivatingVolumes()
	{
		this.deactivatingVolumes.RemoveAll(this.removeFullyDeactivatedVolumes);
	}

	private bool RemoveFullyDeactivatedVolumes(VolumeComponent volume)
	{
		Debug.Assert(volume != null);

		bool isDeactivated = volume.IsFullyDeactivated();
		if (isDeactivated)
		{
			this.ClearVolume(volume);
		}

		return isDeactivated;
	}

	private void ClearVolume(VolumeComponent volume)
	{
		Debug.Assert(volume != null);

		foreach (var chunk in volume.Chunks.Values)
		{
			this.ClearAndReturnToPool(chunk);
		}

		volume.Chunks.Clear();
		this.volumePool.Return(volume);
	}

	private bool RemoveCollidersIfDisabled(ChunkCollidersComponent chunk)
	{
		Debug.Assert(chunk != null);

		if (!chunk.AllCollidersDisabled)
		{
			return false;
		}

		chunk.Clear();
		this.chunkCollidersPool.Return(chunk);
		return true;
	}

	private void ClearAndReturnToPool(ChunkComponent chunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(chunk.IsInitialized);

		chunk.Volume = null;
		chunk.Key = default;
		chunk.VoxelIndexOffset = Int3.Zero;
		chunk.AreCollidersInitialized = false;
		chunk.CleanupColliders = false;

		// clear meshes
		int max = chunk.Meshes.Count;
		for (int i = 0; i < max; i++)
		{
			var meshFilter = chunk.Meshes[i];
			meshFilter.sharedMesh.Clear();
			this.chunkMeshPool.Return(meshFilter);
		}

		// clear subchunks
		max = chunk.Subchunks.Length;
		for (int i = 0; i < max; i++)
		{
			var subchunk = chunk.Subchunks[i];
			if (subchunk != null)
			{
				subchunk.Chunk.ClearSubchunk(subchunk);
				this.subchunkPool.Return(subchunk);
				chunk.Subchunks[i] = null;
			}
		}

		// clear colliders
		max = chunk.Colliders.Count;
		for (int i = 0; i < max; i++)
		{
			var colliders = chunk.Colliders[i];
			colliders.Clear();
			this.chunkCollidersPool.Return(colliders);
		}

		// clear verify colliders
		max = chunk.VerifyColliders.Count;
		for (int i = 0; i < max; i++)
		{
			// verify colliders are deactivated and removed but not returned to their pool
			// because FrameCollisionVerifier will pool them next ClearVerification call
			var colliders = chunk.VerifyColliders[i];
			colliders.Chunk = null;
			colliders.gameObject.SetActive(false);
			colliders.transform.parent = null;
		}

		chunk.Meshes.Clear();
		chunk.Colliders.Clear();
		chunk.VerifyColliders.Clear();
		this.chunkPool.Return(chunk);
	}
}
