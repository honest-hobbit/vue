﻿namespace HQS.VUE.Unity.OLD;

internal class ColliderResyncManager
{
	private readonly VolumeGameObjects gameObjects;

	private readonly IPool<ChunkComponentSubchunk> subchunkPool;

	private readonly CollidersFinder collidersFinder;

	public ColliderResyncManager(
		VolumeGameObjects gameObjects,
		IPool<ChunkComponentSubchunk> subchunkPool,
		IPool<ChunkCollidersComponent> chunkCollidersPool)
	{
		Debug.Assert(gameObjects != null);
		Debug.Assert(subchunkPool != null);
		Debug.Assert(chunkCollidersPool != null);

		this.gameObjects = gameObjects;
		this.subchunkPool = subchunkPool;
		this.collidersFinder = new CollidersFinder(chunkCollidersPool);
	}

	// this must only be called while on the Unity main thread
	public void ResyncColliders(ResyncUnityFrame frame)
	{
		Debug.Assert(frame != null);

		int max = frame.Volumes.Count;
		for (int i = 0; i < max; i++)
		{
			this.UpdateVolumeColliders(frame.Volumes[i]);
		}
	}

	private void UpdateVolumeColliders(ResyncUnityVolume resyncvolume)
	{
		Debug.Assert(resyncvolume != null);

		var instances = resyncvolume.Volume.Instances;
		int max = instances.Count;
		for (int i = 0; i < max; i++)
		{
			var reynscInstance = resyncvolume.GetAndAddInstance((VolumeComponent)instances[i]);
			this.UpdateVolumeInstanceColliders(reynscInstance);
		}
	}

	private void UpdateVolumeInstanceColliders(ResyncUnityVolumeInstance instance)
	{
		Debug.Assert(instance != null);
		Debug.Assert(!instance.IsResyncApplied);
		Debug.Assert(!instance.IsFinished);

		// update the colliders (this must be done before the animator is updated)
		var chunks = instance.ResyncVolume.Chunks;
		int max = chunks.Count;
		for (int i = 0; i < max; i++)
		{
			var resyncChunk = chunks[i];
			if (this.gameObjects.TryGetChunk(resyncChunk, resyncChunk.Colliders.Count > 0, instance.Volume, out var chunk))
			{
				// Don't cleanup the chunks here because that would trigger removing colliders immediately.
				// Instead let MeshResyncManager handle that so old colliders are only removed when the mesh updates.
				this.UpdateChunkColliders(chunk, resyncChunk, instance);
			}
		}
	}

	private void UpdateChunkColliders(ChunkComponent chunk, ResyncUnityChunk resync, ResyncUnityVolumeInstance instance)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(resync != null);
		Debug.Assert(instance != null);

		var subchunks = chunk.Subchunks;
		var subchunkColliderCounts = resync.SubchunkColliderCounts;
		var colliders = resync.Colliders;
		int colliderIndex = 0;

		if (!chunk.AreCollidersInitialized)
		{
			// first time adding colliders to chunk so no need to check for removing subchunks
			chunk.AreCollidersInitialized = true;

			int index = 0;
			int max = resync.CountOfSubchunks_0_ChangedAndHasColliders;

			for (; index < max; index++)
			{
				SetupSubchunk(index);
			}

			index += resync.CountOfSubchunks_1_ChangedAndNoColliders;
			max = index + resync.CountOfSubchunks_2_UnchangedAndHasColliders;

			for (; index < max; index++)
			{
				SetupSubchunk(index);
			}
		}
		else
		{
			// updating chunk that already has colliders so check for removing subchunks
			int index = 0;
			int max = resync.CountOfSubchunks_0_ChangedAndHasColliders;

			for (; index < max; index++)
			{
				SetupAndReplaceSubchunk(index);
			}

			max = index + resync.CountOfSubchunks_1_ChangedAndNoColliders;
			for (; index < max; index++)
			{
				RemoveSubchunk(index);
			}
		}

		void SetupSubchunk(int index)
		{
			var subchunkColliders = subchunkColliderCounts[index];
			var subchunk = this.subchunkPool.Rent();
			this.collidersFinder
				.GetCollidersComponent(chunk, subchunkColliders.Count)
				.InitializeSubchunk(subchunk, colliders, ref colliderIndex, subchunkColliders.Count);
			subchunks[subchunkColliders.Index] = subchunk;
		}

		void SetupAndReplaceSubchunk(int index)
		{
			var subchunkColliders = subchunkColliderCounts[index];
			var subchunk = subchunks[subchunkColliders.Index];
			if (subchunk != null)
			{
				instance.AddSubchunkToRemove(subchunk);
			}

			subchunk = this.subchunkPool.Rent();
			this.collidersFinder
				.GetCollidersComponent(chunk, subchunkColliders.Count)
				.InitializeSubchunk(subchunk, colliders, ref colliderIndex, subchunkColliders.Count);
			subchunks[subchunkColliders.Index] = subchunk;
		}

		void RemoveSubchunk(int index)
		{
			var subchunkColliders = subchunkColliderCounts[index];
			var subchunk = subchunks[subchunkColliders.Index];
			subchunks[subchunkColliders.Index] = null;
			if (subchunk != null)
			{
				instance.AddSubchunkToRemove(subchunk);
			}
		}
	}

	private class CollidersFinder
	{
		private readonly IPool<ChunkCollidersComponent> chunkCollidersPool;

		private readonly Predicate<ChunkCollidersComponent> findColliders;

		private int collidersCount;

		public CollidersFinder(IPool<ChunkCollidersComponent> chunkCollidersPool)
		{
			Debug.Assert(chunkCollidersPool != null);

			this.chunkCollidersPool = chunkCollidersPool;
			this.findColliders = x => x.AvailableCollidersCount >= this.collidersCount;
		}

		public ChunkCollidersComponent GetCollidersComponent(ChunkComponent chunk, int collidersCount)
		{
			Debug.Assert(chunk != null);
			Debug.Assert(collidersCount > 0);

			this.collidersCount = collidersCount;
			var result = chunk.Colliders.Find(this.findColliders);
			if (result == null)
			{
				result = this.chunkCollidersPool.Rent();
				result.Chunk = chunk;
				chunk.Colliders.Add(result);
				result.EnsureEnoughColliders(collidersCount);
				result.transform.SetParent(chunk.transform, false);
				result.gameObject.SetActive(true);
			}

			Debug.Assert(result.AvailableCollidersCount >= collidersCount);
			return result;
		}
	}
}
