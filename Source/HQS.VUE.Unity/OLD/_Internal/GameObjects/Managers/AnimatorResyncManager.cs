﻿namespace HQS.VUE.Unity.OLD;

internal class AnimatorResyncManager
{
	// this must only be called while on the Unity main thread
	[SuppressMessage("Performance", "CA1822", Justification = "Managers are dependency injected instances.")]
	public void ResyncAnimators(ResyncUnityFrame frame)
	{
		Debug.Assert(frame != null);

		int max = frame.Volumes.Count;
		for (int i = 0; i < max; i++)
		{
			UpdateVolumeAnimators(frame.Volumes[i]);
		}
	}

	private static void UpdateVolumeAnimators(ResyncUnityVolume volume)
	{
		Debug.Assert(volume != null);

		int max = volume.Instances.Count;
		for (int i = 0; i < max; i++)
		{
			UpdateVolumeInstanceAnimator(volume.Instances[i]);
		}

		// any records still remaining need to be returned to their animators
		volume.ClearRemainingRecords();
		volume.IncrementFinished();
	}

	private static void UpdateVolumeInstanceAnimator(ResyncUnityVolumeInstance instance)
	{
		Debug.Assert(instance != null);
		Debug.Assert(!instance.IsResyncApplied);
		Debug.Assert(!instance.IsFinished);

		var animator = instance.Volume.Animator;
		if (animator.TryGetValue(out var animatorComponent))
		{
			// volume has an animator, check if it matches the record's animator
			if (instance.ResyncVolume.TryGetAndRemoveRecord(animator, out var recorder))
			{
				// volume's current animator is the same as when the record was taken so use the animator with its record
				animatorComponent.UpdateVolume(instance, recorder);
			}
			else
			{
				// volume's current animator is not the same as the record's so use the animator without a record
				animatorComponent.UpdateVolume(instance, default);
			}
		}
		else
		{
			// volume does not have an animator so apply the resync immediately
			instance.ApplyResync();
			instance.Finished();
		}
	}
}
