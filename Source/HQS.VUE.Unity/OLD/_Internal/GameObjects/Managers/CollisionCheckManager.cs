﻿namespace HQS.VUE.Unity.OLD;

internal class CollisionCheckManager
{
	private readonly List<VerifyCollidersComponent> verifyingChunks = new List<VerifyCollidersComponent>();

	private readonly float downscaleColliders;

	private readonly VolumeGameObjects gameObjects;

	private readonly IPool<VerifyCollidersComponent> verifyCollidersPool;

	public CollisionCheckManager(
		UnityConfig config,
		VolumeGameObjects gameObjects,
		IPool<VerifyCollidersComponent> verifyCollidersPool)
	{
		Debug.Assert(config != null);
		Debug.Assert(gameObjects != null);
		Debug.Assert(verifyCollidersPool != null);

		this.downscaleColliders = config.DownscaleVerifyColliders;
		this.gameObjects = gameObjects;
		this.verifyCollidersPool = verifyCollidersPool;
	}

	// this must only be called while on the Unity main thread
	public void ClearVerification()
	{
		int max = this.verifyingChunks.Count;
		if (max == 0)
		{
			return;
		}

		for (int i = 0; i < max; i++)
		{
			var verifyChunk = this.verifyingChunks[i];
			var chunk = verifyChunk.Chunk;
			verifyChunk.Clear();
			this.verifyCollidersPool.Return(verifyChunk);

			if (chunk != null)
			{
				this.gameObjects.CleanupChunk(chunk);
			}
		}

		this.verifyingChunks.Clear();
	}

	// this must only be called while on the Unity main thread
	public void SetupVerification(ResyncUnityFrame frame)
	{
		Debug.Assert(frame != null);

		int max = frame.Volumes.Count;
		for (int i = 0; i < max; i++)
		{
			this.SetupVolumeVerification(frame, frame.Volumes[i]);
		}
	}

	private void SetupVolumeVerification(ResyncUnityFrame frame, ResyncUnityVolume resyncVolume)
	{
		Debug.Assert(frame != null);
		Debug.Assert(resyncVolume != null);

		var instances = resyncVolume.Volume.Instances;
		int max = instances.Count;
		for (int i = 0; i < max; i++)
		{
			this.SetupVolumeInstanceVerification(frame, resyncVolume, (VolumeComponent)instances[i]);
		}
	}

	private void SetupVolumeInstanceVerification(ResyncUnityFrame frame, ResyncUnityVolume resyncVolume, VolumeComponent volume)
	{
		Debug.Assert(frame != null);
		Debug.Assert(resyncVolume != null);
		Debug.Assert(volume != null);

		if (!volume.CheckCollision)
		{
			return;
		}

		int max = resyncVolume.Chunks.Count;
		for (int i = 0; i < max; i++)
		{
			var resyncChunk = resyncVolume.Chunks[i];
			if (resyncChunk.CountOfChangedColliders > 0)
			{
				var chunk = this.gameObjects.GetChunk(resyncChunk, volume);
				this.SetupChunkVerification(chunk, frame, resyncChunk);
			}
		}
	}

	private void SetupChunkVerification(ChunkComponent chunk, ResyncUnityFrame resyncFrame, ResyncUnityChunk resyncChunk)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(resyncFrame != null);
		Debug.Assert(resyncChunk != null);
		Debug.Assert(resyncChunk.CountOfChangedColliders > 0);

		var verifyChunk = this.GetVerifyComponent(chunk, resyncFrame);
		int max = resyncChunk.CountOfChangedColliders;

		for (int i = 0; i < max; i++)
		{
			while (!verifyChunk.TryAddCollider(resyncChunk.Colliders[i], this.downscaleColliders))
			{
				verifyChunk = this.GetVerifyComponent(chunk, resyncFrame);
			}
		}
	}

	private VerifyCollidersComponent GetVerifyComponent(ChunkComponent chunk, ResyncUnityFrame resyncFrame)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(resyncFrame != null);

		var verifyChunk = this.verifyCollidersPool.Rent();
		verifyChunk.transform.SetParent(chunk.transform, false);
		verifyChunk.gameObject.SetActive(true);
		verifyChunk.Frame = resyncFrame;
		verifyChunk.Chunk = chunk;
		chunk.VerifyColliders.Add(verifyChunk);
		this.verifyingChunks.Add(verifyChunk);
		return verifyChunk;
	}
}
