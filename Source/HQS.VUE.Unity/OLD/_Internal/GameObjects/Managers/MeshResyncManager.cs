﻿namespace HQS.VUE.Unity.OLD;

internal class MeshResyncManager
{
	private readonly Queue<MeshFilter> temporaryMeshes = new Queue<MeshFilter>();

	private readonly VolumeGameObjects gameObjects;

	private readonly IPool<MeshFilter> chunkMeshPool;

	public MeshResyncManager(VolumeGameObjects gameObjects, IPool<MeshFilter> chunkMeshPool)
	{
		Debug.Assert(gameObjects != null);
		Debug.Assert(chunkMeshPool != null);

		this.gameObjects = gameObjects;
		this.chunkMeshPool = chunkMeshPool;
	}

	// this must only be called while on the Unity main thread
	public void ResyncMeshes(ResyncUnityVolumeInstance instance)
	{
		Debug.Assert(instance != null);

		instance.RemoveSubchunks(this.gameObjects);

		// update chunk meshes
		int max = instance.ResyncVolume.Chunks.Count;
		for (int i = 0; i < max; i++)
		{
			var resyncChunk = instance.ResyncVolume.Chunks[i];
			if (this.gameObjects.TryGetChunk(
				resyncChunk, resyncChunk.Meshes.Count > 0, instance.Volume, out var chunk))
			{
				this.UpdateChunkMeshes(chunk, resyncChunk);
				this.gameObjects.CleanupChunk(chunk);
			}
		}
	}

	private void UpdateChunkMeshes(ChunkComponent chunk, ResyncUnityChunk resync)
	{
		Debug.Assert(chunk != null);
		Debug.Assert(resync != null);
		Debug.Assert(this.temporaryMeshes.Count == 0);

		// transfer all the chunk's current meshes to temporary storage to be reused
		int max = chunk.Meshes.Count;
		for (int i = 0; i < max; i++)
		{
			this.temporaryMeshes.Enqueue(chunk.Meshes[i]);
		}

		chunk.Meshes.Clear();

		// update the chunk's meshes
		max = resync.Meshes.Count;
		for (int i = 0; i < max; i++)
		{
			if (this.temporaryMeshes.TryDequeue(out var meshFilter))
			{
				meshFilter.sharedMesh.Clear();
			}
			else
			{
				meshFilter = this.chunkMeshPool.Rent();
				meshFilter.transform.SetParent(chunk.transform, false);
				meshFilter.gameObject.SetActive(true);
			}

			resync.Meshes[i].BuildMesh(meshFilter.sharedMesh);
			chunk.Meshes.Add(meshFilter);
		}

		// cleanup any leftover meshes from the chunk
		while (this.temporaryMeshes.TryDequeue(out var meshFilter))
		{
			meshFilter.sharedMesh.Clear();
			this.chunkMeshPool.Return(meshFilter);
		}
	}
}
