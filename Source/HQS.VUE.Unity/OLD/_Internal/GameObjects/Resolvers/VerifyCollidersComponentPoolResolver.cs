﻿namespace HQS.VUE.Unity.OLD;

internal class VerifyCollidersComponentPoolResolver : ITypeResolver<IPool<VerifyCollidersComponent>>
{
	private readonly IPool<VerifyCollidersComponent> pool;

	public VerifyCollidersComponentPoolResolver(UnityConfig config)
	{
		var prefab = new GameObject("Verify Colliders (Don't click, will lag)");
		var pooledComponent = prefab.AddComponent<VerifyCollidersComponent>();

		// Rigidbody needed in order for trigger to detect static colliders
		prefab.AddComponent<Rigidbody>().isKinematic = true;

		int max = config.BoxCollidersPerVerifyCollider;
		for (int i = 0; i < max; i++)
		{
			var box = prefab.AddComponent<BoxCollider>();
			box.enabled = false;
			box.isTrigger = true;
		}

		this.pool = GameObjectPool.ThreadSafe.Create(pooledComponent, x => x.Initialize());
	}

	/// <inheritdoc />
	public IPool<VerifyCollidersComponent> GetInstance() => this.pool;
}
