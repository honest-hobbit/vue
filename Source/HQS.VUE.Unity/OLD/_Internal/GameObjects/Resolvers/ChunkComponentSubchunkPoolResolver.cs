﻿namespace HQS.VUE.Unity.OLD;

internal class ChunkComponentSubchunkPoolResolver : ITypeResolver<IPool<ChunkComponentSubchunk>>
{
	private readonly IPool<ChunkComponentSubchunk> pool;

	public ChunkComponentSubchunkPoolResolver(UniverseConfig config)
	{
		Debug.Assert(config != null);

		this.pool = Pool.ThreadSafe.Create(
			() => new ChunkComponentSubchunk(config.ContourSubchunkCollidersCapacity));
	}

	/// <inheritdoc />
	public IPool<ChunkComponentSubchunk> GetInstance() => this.pool;
}
