﻿namespace HQS.VUE.Unity.OLD;

internal class MeshComponentPoolResolver : ITypeResolver<IPool<MeshFilter>>
{
	private readonly IPool<MeshFilter> pool;

	public MeshComponentPoolResolver(UnityConfig config)
	{
		var prefab = new GameObject("Mesh");
		var renderer = prefab.AddComponent<MeshRenderer>();
		renderer.material = config.VoxelMaterial;
		renderer.shadowCastingMode = ShadowCastingMode.TwoSided;
		renderer.receiveShadows = true;
		var pooledComponent = prefab.AddComponent<MeshFilter>();

		this.pool = GameObjectPool.ThreadSafe.Create(pooledComponent, x => x.sharedMesh = new Mesh());
	}

	/// <inheritdoc />
	public IPool<MeshFilter> GetInstance() => this.pool;
}
