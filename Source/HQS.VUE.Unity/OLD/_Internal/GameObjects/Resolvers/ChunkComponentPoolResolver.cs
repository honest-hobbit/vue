﻿namespace HQS.VUE.Unity.OLD;

internal class ChunkComponentPoolResolver : ITypeResolver<IPool<ChunkComponent>>
{
	private readonly IPool<ChunkComponent> pool;

	public ChunkComponentPoolResolver(VoxelSizeConfig sizes)
	{
		Debug.Assert(sizes != null);

		// name will be assigned when taken from the pool
		this.pool = GameObjectPool.ThreadSafe.Create<ChunkComponent>(null, x => x.Initialize(sizes));
	}

	/// <inheritdoc />
	public IPool<ChunkComponent> GetInstance() => this.pool;
}
