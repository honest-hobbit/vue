﻿namespace HQS.VUE.Unity.OLD;

internal class ChunkCollidersComponentPoolResolver : ITypeResolver<IPool<ChunkCollidersComponent>>
{
	private readonly IPool<ChunkCollidersComponent> pool;

	public ChunkCollidersComponentPoolResolver(UnityConfig config)
	{
		// This same List is purposely shared by all ChunkCollidersComponents.
		// This is safe to do because the List is only accessed on the Unity main thread
		// and only holds temporary state for GetComponents during Initialize.
		// Doing this avoids garbage creation whenever a new ChunkCollidersComponent is created.
		var tempColliders = new List<BoxCollider>(config.BoxCollidersPerChunkCollider);

		var prefab = new GameObject("Colliders (Don't click, will lag)");
		var pooledComponent = prefab.AddComponent<ChunkCollidersComponent>();
		int max = config.BoxCollidersPerChunkCollider;
		for (int i = 0; i < max; i++)
		{
			prefab.AddComponent<BoxCollider>().enabled = false;
		}

		this.pool = GameObjectPool.ThreadSafe.Create(pooledComponent, x => x.Initialize(tempColliders));
	}

	/// <inheritdoc />
	public IPool<ChunkCollidersComponent> GetInstance() => this.pool;
}
