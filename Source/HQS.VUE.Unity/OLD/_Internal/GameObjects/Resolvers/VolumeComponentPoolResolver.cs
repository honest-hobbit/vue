﻿namespace HQS.VUE.Unity.OLD;

internal class VolumeComponentPoolResolver : ITypeResolver<IPool<VolumeComponent>>
{
	private readonly IPool<VolumeComponent> pool;

	public VolumeComponentPoolResolver()
	{
		// name will be assigned when taken from the pool
		// TODO rigidbody is only requirred if the volume will be moving and it's expensive
		////x.AddComponent<Rigidbody>().isKinematic = true;
		this.pool = GameObjectPool.ThreadSafe.Create<VolumeComponent>();
	}

	/// <inheritdoc />
	public IPool<VolumeComponent> GetInstance() => this.pool;
}
