﻿namespace HQS.VUE.Unity.OLD;

internal class ResyncUnityQueue : IResyncHostApplicationQueue
{
	private readonly IPinPool<ResyncUnityFrame> framePool;

	private readonly IPinPool<ResyncUnityVolume> volumePool;

	private readonly ICommandSubmitter<CopyResyncChunkCommand.Args, Pinned<ResyncUnityChunk>> copyProcessor;

	private readonly ResyncUnityPipeline pipeline;

	public ResyncUnityQueue(
		IPinPool<ResyncUnityFrame> framePool,
		IPinPool<ResyncUnityVolume> volumePool,
		ICommandSubmitter<CopyResyncChunkCommand.Args, Pinned<ResyncUnityChunk>> copyProcessor,
		ResyncUnityPipeline pipeline)
	{
		Debug.Assert(framePool != null);
		Debug.Assert(volumePool != null);
		Debug.Assert(copyProcessor != null);
		Debug.Assert(pipeline != null);

		this.framePool = framePool;
		this.volumePool = volumePool;
		this.copyProcessor = copyProcessor;
		this.pipeline = pipeline;
	}

	/// <inheritdoc />
	public bool SubmitResyncFrame(
		IReadOnlyList<IVolumeView> volumes,
		VoxelJobDiagnosticsRecorder diagnostics,
		bool checkCollision,
		Pinned<IVoxelJobInfo> jobCompleted,
		ChangesetRecorder recorder)
	{
		IResyncHostApplicationQueueContracts.SubmitResyncFrame(volumes, diagnostics, jobCompleted, recorder);

		// setup the frame
		using var framePin = this.framePool.Rent(out var frame);
		frame.SetJobCompleted(jobCompleted);
		frame.CheckCollision = checkCollision;
		diagnostics.VerifyResync = checkCollision;

		// add volumes to frame (copying is done in parallel)
		diagnostics.StartTiming();
		int max = volumes.Count;
		for (int i = 0; i < max; i++)
		{
			this.AddVolumeToFrame(framePin.AsPinned, volumes[i]);
		}

		// wait for copying to finish before sending frame to Unity main thread
		frame.WaitUntilCopyingFinished();
		diagnostics.SetCopyResyncDuration();

		// if voxel changes recording was passed in then wait for that to finish too
		// before sending frame to Unity main thread
		recorder?.WaitUntilRecordingFinished(diagnostics);

		// resync frame with Unity
		this.pipeline.EnqueueFrame(framePin.AsPinned);
		diagnostics.ResyncSuccessful = true;
		if (checkCollision)
		{
			diagnostics.StartTiming();
			frame.ResyncVerified.Wait();
			diagnostics.SetVerifyResyncDuration();
			diagnostics.ResyncSuccessful = !frame.CollisionDetected;
		}

		return diagnostics.ResyncSuccessful;
	}

	private void AddVolumeToFrame(Pinned<ResyncUnityFrame> frame, IVolumeView view)
	{
		Debug.Assert(frame.IsPinned);
		Debug.Assert(view != null);

		var pin = this.volumePool.Rent(out var resyncVolume);
		resyncVolume.Initialize(frame, view.Volume);
		frame.Value.AddVolume(pin);

		int max = view.ChangeRecords.Count;
		for (int i = 0; i < max; i++)
		{
			// add voxel change records to the frame
			// (actual recording of voxels is not handled here, this just adds the objects to the frame)
			resyncVolume.AddRecord(view.ChangeRecords[i]);
		}

		max = view.ChangedContourChunks.Count;
		if (max == 0)
		{
			return;
		}

		for (int i = 0; i < max; i++)
		{
			// copy chunks to frame in parallel
			resyncVolume.AddCopyCommand(this.copyProcessor.SubmitAndGetWaitableResult(
				new CopyResyncChunkCommand.Args() { ContourResult = view.ChangedContourChunks[i] }));
		}
	}
}
