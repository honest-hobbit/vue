﻿namespace HQS.VUE.Unity.OLD;

internal class ResyncUnityPipeline : AbstractDisposable
{
	private readonly Subject<ResyncUnityDiagnostics> diagnosticsSubject = new Subject<ResyncUnityDiagnostics>();

	private readonly ResyncUnityDiagnosticsRecorder diagnostics = new ResyncUnityDiagnosticsRecorder();

	private readonly ConcurrentQueue<Pin<ResyncUnityFrame>> pendingFrames = new ConcurrentQueue<Pin<ResyncUnityFrame>>();

	private readonly VolumeGameObjects gameObjects;

	private readonly CollisionCheckManager collisionManager;

	private readonly ColliderResyncManager colliderManager;

	private readonly AnimatorResyncManager animatorManager;

	private readonly TimeSpan maxUpdateDuration;

	private Pin<ResyncUnityFrame> currentFramePin;

	private FrameStatus frameStatus;

	public ResyncUnityPipeline(
		VolumeGameObjects gameObjects,
		CollisionCheckManager collisionManager,
		ColliderResyncManager colliderManager,
		AnimatorResyncManager animatorManager,
		UnityConfig config)
	{
		Debug.Assert(gameObjects != null);
		Debug.Assert(collisionManager != null);
		Debug.Assert(colliderManager != null);
		Debug.Assert(animatorManager != null);
		Debug.Assert(config != null);

		this.gameObjects = gameObjects;
		this.collisionManager = collisionManager;
		this.colliderManager = colliderManager;
		this.animatorManager = animatorManager;

		var maxUpdateDuration = config.MaxResyncDuration;
		this.maxUpdateDuration = maxUpdateDuration.IsFiniteDuration() ? maxUpdateDuration : TimeSpan.MaxValue;

		this.Diagnostics = this.diagnosticsSubject.AsObservableOnly();
	}

	public IObservable<ResyncUnityDiagnostics> Diagnostics { get; }

	// this is safe to call from any thread
	public void EnqueueFrame(Pinned<ResyncUnityFrame> frame)
	{
		Debug.Assert(!this.IsDisposed);
		Debug.Assert(frame.IsPinned);

		this.pendingFrames.Enqueue(frame.CreatePin());
	}

	// must only be called while on the Unity main thread
	public void UnityFixedUpdate()
	{
		Debug.Assert(!this.IsDisposed);

		// FixedUpdate is called before the physics engine update
		// so only clear the collision if this has been called multiple times in a row
		if (this.frameStatus == FrameStatus.CollisionCheckPrepared)
		{
			this.frameStatus = FrameStatus.CollisionChecked;
			this.diagnostics.StartTiming();
		}
		else
		{
			this.HandlePostCollisionCheckCleanup();
		}
	}

	private readonly System.Diagnostics.Stopwatch resyncTimer = new System.Diagnostics.Stopwatch();

	// must only be called while on the Unity main thread
	public void UnityUpdate()
	{
		Debug.Assert(!this.IsDisposed);

		this.gameObjects.CheckDeactivatingVolumes();

		this.resyncTimer.Restart();
		while (this.TryAdvanceFrame() && this.resyncTimer.Elapsed < this.maxUpdateDuration)
		{
		}

		this.resyncTimer.Stop();

		if (this.diagnostics.HasData)
		{
			this.diagnosticsSubject.OnNext(this.diagnostics.GetSnapshot());
			this.diagnostics.Clear();
		}
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.diagnosticsSubject.OnCompleted();
		this.diagnosticsSubject.Dispose();
	}

	// returns true to indicate that work might be able to be done, false to stop any further work
	private bool TryAdvanceFrame()
	{
		// must update colliders before the animator so that the resync instance will contain
		// any old subchunks to be removed by the animator when it calls ApplyResync
		switch (this.frameStatus)
		{
			case FrameStatus.None: return this.StartNextFrame();
			case FrameStatus.CollisionCheckPrepared: return false;
			case FrameStatus.CollisionChecked:
			case FrameStatus.CollisionCheckCleaned: return this.ResyncCollidersOrDiscardChanges();
			case FrameStatus.CollidersResynced: return this.ResyncAnimators();
			case FrameStatus.AnimatorsResynced: return this.CompleteResyncFrame();
			default: throw InvalidEnumArgument.CreateException(nameof(this.frameStatus), this.frameStatus);
		}
	}

	// returns true to indicate that work might be able to be done, false to stop any further work
	private bool StartNextFrame()
	{
		if (!this.pendingFrames.TryDequeue(out var frame))
		{
			return false;
		}

		this.currentFramePin = frame;
		var currentFrame = this.currentFramePin.Value;

		if (currentFrame.Volumes.Count == 0)
		{
			// nothing was changed so just check the job for completion interfaces
			return this.CompleteResyncFrame();
		}

		// 1 or more volumes changed so handle resyncing game objects
		if (!currentFrame.CheckCollision)
		{
			return this.ResyncCollidersOrDiscardChanges();
		}

		// collision must be checked before resyncing game objects
		this.diagnostics.StartTiming();
		this.collisionManager.SetupVerification(currentFrame);
		this.diagnostics.SetVerifySetupDuration();
		this.frameStatus = FrameStatus.CollisionCheckPrepared;

		// UnityFixedUpdate must be called before collision can be checked
		// so no further work can be done until then
		return false;
	}

	private void HandlePostCollisionCheckCleanup()
	{
		if (this.frameStatus != FrameStatus.CollisionChecked)
		{
			return;
		}

		this.diagnostics.SetApproxVerifyDuration();
		this.currentFramePin.Value.ResyncVerified.Set();
		this.frameStatus = FrameStatus.CollisionCheckCleaned;

		this.diagnostics.StartTiming();
		this.collisionManager.ClearVerification();
		this.diagnostics.SetVerifyCleanupDuration();
	}

	// returns true to indicate that work might be able to be done, false to stop any further work
	private bool ResyncCollidersOrDiscardChanges()
	{
		this.HandlePostCollisionCheckCleanup();

		var currentFrame = this.currentFramePin.Value;
		if (!currentFrame.CollisionDetected)
		{
			// frame was accepted
			this.diagnostics.StartTiming();
			this.colliderManager.ResyncColliders(currentFrame);
			this.diagnostics.SetResyncCollidersDuration();
			this.frameStatus = FrameStatus.CollidersResynced;
			return true;
		}
		else
		{
			// frame was rejected
			return this.CompleteResyncFrame();
		}
	}

	// returns true to indicate that work might be able to be done, false to stop any further work
	private bool ResyncAnimators()
	{
		this.diagnostics.StartTiming();
		this.animatorManager.ResyncAnimators(this.currentFramePin.Value);
		this.diagnostics.SetResyncAnimatorsDuration();
		this.frameStatus = FrameStatus.AnimatorsResynced;
		return true;
	}

	// returns true to indicate that work might be able to be done, false to stop any further work
	private bool CompleteResyncFrame()
	{
		this.currentFramePin.Dispose();
		this.currentFramePin = default;
		this.frameStatus = FrameStatus.None;
		return true;
	}

	private enum FrameStatus
	{
		None,
		CollisionCheckPrepared,
		CollisionChecked,
		CollisionCheckCleaned,
		CollidersResynced,
		AnimatorsResynced,
	}
}
