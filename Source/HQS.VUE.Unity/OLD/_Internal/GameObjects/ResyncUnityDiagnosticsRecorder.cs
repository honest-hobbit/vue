﻿namespace HQS.VUE.Unity.OLD;

internal class ResyncUnityDiagnosticsRecorder
{
	private readonly Stopwatch timer = new Stopwatch();

	public bool HasData { get; private set; }

	public TimeSpan VerifySetupDuration { get; private set; }

	public TimeSpan ApproxVerifyDuration { get; private set; }

	public TimeSpan VerifyCleanupDuration { get; private set; }

	public TimeSpan ResyncCollidersDuration { get; private set; }

	public TimeSpan ResyncAnimatorsDuration { get; private set; }

	public void StartTiming()
	{
		Debug.Assert(!this.timer.IsRunning);
		this.timer.Restart();
		this.HasData = true;
	}

	public void SetVerifySetupDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.VerifySetupDuration = this.timer.Elapsed;
	}

	public void SetApproxVerifyDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.ApproxVerifyDuration = this.timer.Elapsed;
	}

	public void SetVerifyCleanupDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.VerifyCleanupDuration = this.timer.Elapsed;
	}

	public void SetResyncCollidersDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.ResyncCollidersDuration = this.timer.Elapsed;
	}

	public void SetResyncAnimatorsDuration()
	{
		Debug.Assert(this.timer.IsRunning);
		this.timer.Stop();
		this.ResyncAnimatorsDuration = this.timer.Elapsed;
	}

	public void Clear()
	{
		this.HasData = false;
		this.timer.Reset();
		this.VerifySetupDuration = TimeSpan.Zero;
		this.ApproxVerifyDuration = TimeSpan.Zero;
		this.VerifyCleanupDuration = TimeSpan.Zero;
		this.ResyncCollidersDuration = TimeSpan.Zero;
		this.ResyncAnimatorsDuration = TimeSpan.Zero;
	}

	public ResyncUnityDiagnostics GetSnapshot() => new ResyncUnityDiagnostics(this);
}
