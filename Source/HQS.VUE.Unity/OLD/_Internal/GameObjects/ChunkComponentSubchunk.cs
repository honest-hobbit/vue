﻿namespace HQS.VUE.Unity.OLD;

internal class ChunkComponentSubchunk
{
	public ChunkComponentSubchunk(int collidersCapacity)
	{
		Debug.Assert(collidersCapacity >= 0);

		this.Colliders = new List<BoxCollider>(collidersCapacity);
	}

	public ChunkCollidersComponent Chunk { get; set; }

	public List<BoxCollider> Colliders { get; }
}
