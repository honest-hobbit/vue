﻿namespace HQS.VUE.Unity.OLD;

internal class UnityDiagnostics : IUnityDiagnostics
{
	private readonly UnityObservables observables;

	public UnityDiagnostics(
		UnityObservables observables,
		TypePoolCollection pools,
		EngineValidator validator,
		VoxelChunkCache voxelChunkCache,
		ContourChunkCache contourChunkCache)
	{
		Debug.Assert(observables != null);
		Debug.Assert(pools != null);
		Debug.Assert(validator != null);
		Debug.Assert(voxelChunkCache != null);
		Debug.Assert(contourChunkCache != null);

		this.observables = observables;
		this.Pools = pools;
		this.VoxelChunkCache = new ChunkCacheWrapper(validator, voxelChunkCache);
		this.ContourChunkCache = new ChunkCacheWrapper(validator, contourChunkCache);
	}

	/// <inheritdoc />
	public IObservable<ResyncUnityDiagnostics> ResyncCompleted => this.observables.ResyncCompleted;

	/// <inheritdoc />
	public IObservable<VoxelJobDiagnostics> JobCompleted => this.observables.JobCompleted;

	/// <inheritdoc />
	public IObservable<Exception> ErrorOccurred => this.observables.ErrorOccurred;

	/// <inheritdoc />
	public ITypePoolCollection Pools { get; }

	/// <inheritdoc />
	public IChunkCache VoxelChunkCache { get; }

	/// <inheritdoc />
	public IChunkCache ContourChunkCache { get; }

	public void UpdateObservables() => this.observables.Update();
}
