﻿namespace HQS.VUE.Unity.OLD;

internal readonly struct SubchunkMeshCollider
{
	public SubchunkMeshCollider(int index, IMeshDefinition mesh)
	{
		this.Index = (ushort)index;
		this.Mesh = mesh;
	}

	// the index of the subchunk in its parent chunk
	public ushort Index { get; }

	public IMeshDefinition Mesh { get; }
}
