﻿namespace HQS.VUE.Unity.OLD;

internal class ResyncUnityVolume : IDeinitializable
{
	private readonly List<Pin<ResyncUnityVolumeInstance>> instances = new List<Pin<ResyncUnityVolumeInstance>>();

	private readonly CommandWaiter<Pinned<ResyncUnityChunk>> copying = new CommandWaiter<Pinned<ResyncUnityChunk>>();

	private readonly List<Pin<ResyncUnityChunk>> chunks = new List<Pin<ResyncUnityChunk>>();

	private readonly Dictionary<Pin<AnimateVoxelChangesComponent>, Pin<IVoxelChangesRecord>> records =
		new Dictionary<Pin<AnimateVoxelChangesComponent>, Pin<IVoxelChangesRecord>>(
			EqualityComparer.ForStruct<Pin<AnimateVoxelChangesComponent>>());

	private readonly IPinPool<ResyncUnityVolumeInstance> instancePool;

	private Pin<ResyncUnityFrame> frame;

	private int finishedCount;

	public ResyncUnityVolume(IPinPool<ResyncUnityVolumeInstance> instancePool)
	{
		Debug.Assert(instancePool != null);

		this.instancePool = instancePool;
		this.Instances = ReadOnlyList.Convert(this.instances, x => x.Value);
		this.Chunks = ReadOnlyList.Convert(this.chunks, x => x.Value);
	}

	public bool IsInitialized => this.Volume != null;

	public bool IsFinished => this.finishedCount >= this.instances.Count + 1;

	public Volume Volume { get; private set; }

	public IReadOnlyList<ResyncUnityVolumeInstance> Instances { get; }

	public IReadOnlyList<ResyncUnityChunk> Chunks { get; }

	public void Initialize(Pinned<ResyncUnityFrame> frame, Volume volume)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(frame.IsPinned);
		Debug.Assert(volume != null);

		this.frame = frame.CreatePin();
		this.Volume = volume;
	}

	public void AddRecord(ChangeRecord pair)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsFinished);
		Debug.Assert(this.Chunks.Count == 0);
		Debug.Assert(pair.IsAssigned);
		Debug.Assert(!this.records.ContainsKey(pair.Animator.CastTo<AnimateVoxelChangesComponent>()));

		this.records.Add(pair.Animator.CastTo<AnimateVoxelChangesComponent>(), pair.Record);
	}

	public void AddCopyCommand(Pin<IWaitable<Pinned<ResyncUnityChunk>>> command)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsFinished);
		Debug.Assert(this.Chunks.Count == 0);
		Debug.Assert(command.IsPinned);

		this.copying.Add(command);
	}

	public void WaitUntilCopyingFinished()
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsFinished);
		Debug.Assert(this.Chunks.Count == 0);

		this.copying.Result.PinAllTo(this.chunks);
		this.copying.Clear();
	}

	public ResyncUnityVolumeInstance GetAndAddInstance(VolumeComponent volume)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsFinished);
		Debug.Assert(volume != null);
		Debug.Assert(!this.instances.Any(x => x.Value.Volume.EqualsByReferenceNullSafe(volume)));

		var pin = this.instancePool.Rent(out var instance);
		instance.Initialize(this.frame.Value.JobCompleted, this, volume);
		this.instances.Add(pin);
		return instance;
	}

	public bool TryGetAndRemoveRecord(
		Pin<AnimateVoxelChangesComponent> animator, out Pin<IVoxelChangesRecord> record)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsFinished);

		if (this.records.TryGetValue(animator, out record))
		{
			this.records.Remove(animator);
			return true;
		}
		else
		{
			record = default;
			return false;
		}
	}

	public void ClearRemainingRecords()
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsFinished);

		foreach (var pair in this.records)
		{
			pair.Value.Dispose();
		}

		this.records.Clear();
	}

	public void IncrementFinished()
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsFinished);

		this.finishedCount++;
		if (this.IsFinished)
		{
			this.frame.Dispose();
			this.frame = default;
		}
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.IsFinished);
		Debug.Assert(this.records.Count == 0);

		this.chunks.DisposeAllAndClear();
		this.instances.DisposeAllAndClear();
		this.Volume = null;
		this.finishedCount = 0;
	}
}
