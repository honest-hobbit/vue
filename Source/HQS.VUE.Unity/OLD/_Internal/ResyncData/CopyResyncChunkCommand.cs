﻿namespace HQS.VUE.Unity.OLD;

internal class CopyResyncChunkCommand :
	AbstractResultCommand<Pinned<ResyncUnityChunk>>, IParallelCommand, IInitializable<CopyResyncChunkCommand.Args>, INamespaceOLD
{
	private readonly IPinPool<ResyncUnityChunk> chunkPool;

	private ReadOnlyContourChunkResult contourResult;

	private Pin<ResyncUnityChunk> chunk;

	public CopyResyncChunkCommand(IPinPool<ResyncUnityChunk> chunkPool)
	{
		Debug.Assert(chunkPool != null);

		this.chunkPool = chunkPool;
	}

	/// <inheritdoc />
	public void Initialize(Args args)
	{
		Debug.Assert(!args.ContourResult.IsNull);
		this.ValidateAndSetInitialized();

		this.contourResult = args.ContourResult;
	}

	/// <inheritdoc />
	protected override Pinned<ResyncUnityChunk> RunAndGetResult()
	{
		this.chunk = this.chunkPool.Rent();
		this.chunk.Value.CopyIn(this.contourResult);
		return this.chunk.AsPinned;
	}

	/// <inheritdoc />
	protected override void OnDeinitialize()
	{
		this.contourResult = default;
		this.chunk.Dispose();
		this.chunk = default;
	}

	public struct Args
	{
		public ReadOnlyContourChunkResult ContourResult;
	}
}
