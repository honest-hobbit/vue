﻿namespace HQS.VUE.Unity.OLD;

internal readonly struct SubchunkColliderCount
{
	public SubchunkColliderCount(int index, int count)
	{
		this.Index = (ushort)index;
		this.Count = (ushort)count;
	}

	public SubchunkColliderCount(ushort index, ushort count)
	{
		this.Index = index;
		this.Count = count;
	}

	// the index of the subchunk in its parent chunk
	public ushort Index { get; }

	// the number of colliders the subchunk has
	public ushort Count { get; }

	/// <inheritdoc />
	public override string ToString() => $"{nameof(this.Index)}: {this.Index}, {nameof(this.Count)}: {this.Count}";
}
