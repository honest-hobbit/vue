﻿namespace HQS.VUE.Unity.OLD;

internal class ResyncUnityVolumeInstance : IResyncVolumeInstance, IDeinitializable
{
	private readonly List<ChunkComponentSubchunk> subchunksToRemove = new List<ChunkComponentSubchunk>();

	private readonly MeshResyncManager meshManager;

	private Pin<IVoxelJobInfo> jobCompleted;

	public ResyncUnityVolumeInstance(MeshResyncManager meshManager)
	{
		Debug.Assert(meshManager != null);

		this.meshManager = meshManager;
	}

	public bool IsInitialized => this.Volume != null;

	public ResyncUnityVolume ResyncVolume { get; private set; }

	public VolumeComponent Volume { get; private set; }

	/// <inheritdoc />
	public bool IsResyncApplied { get; private set; }

	/// <inheritdoc />
	public bool IsFinished { get; private set; }

	/// <inheritdoc />
	public Pinned<IVoxelJobInfo> JobCompleted
	{
		get
		{
			Debug.Assert(this.IsInitialized);
			this.ValidateNotFinished();

			return this.jobCompleted.AsPinned;
		}
	}

	/// <inheritdoc />
	public void ApplyResync()
	{
		Debug.Assert(this.IsInitialized);
		this.ValidateNotFinished();

		if (this.IsResyncApplied)
		{
			return;
		}

		this.IsResyncApplied = true;
		this.meshManager.ResyncMeshes(this);
	}

	/// <inheritdoc />
	public void Finished()
	{
		Debug.Assert(this.IsInitialized);

		if (this.IsFinished)
		{
			return;
		}

		this.IsFinished = true;
		this.ResyncVolume.IncrementFinished();
	}

	public void Initialize(
		Pinned<IVoxelJobInfo> jobCompleted, ResyncUnityVolume resyncVolume, VolumeComponent volumeComponent)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(jobCompleted.IsUnassigned || jobCompleted.IsPinned);
		Debug.Assert(resyncVolume != null);
		Debug.Assert(volumeComponent != null);

		if (jobCompleted.IsPinned)
		{
			this.jobCompleted = jobCompleted.CreatePin();
		}

		this.ResyncVolume = resyncVolume;
		this.Volume = volumeComponent;
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		Debug.Assert(this.IsInitialized);

		this.jobCompleted.Dispose();
		this.jobCompleted = default;
		this.ResyncVolume = null;
		this.Volume = null;
		this.subchunksToRemove.Clear();
		this.IsResyncApplied = false;
		this.IsFinished = false;
	}

	public void AddSubchunkToRemove(ChunkComponentSubchunk subchunk)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(!this.IsResyncApplied);
		Debug.Assert(!this.IsFinished);
		Debug.Assert(subchunk != null);

		this.subchunksToRemove.Add(subchunk);
	}

	public void RemoveSubchunks(VolumeGameObjects gameObjects)
	{
		Debug.Assert(gameObjects != null);

		int max = this.subchunksToRemove.Count;
		for (int i = 0; i < max; i++)
		{
			gameObjects.ClearSubchunk(this.subchunksToRemove[i]);
		}

		this.subchunksToRemove.Clear();
	}

	private void ValidateNotFinished()
	{
		if (this.IsFinished)
		{
			throw new InvalidOperationException($"{nameof(this.Finished)} was already called.");
		}
	}
}
