﻿namespace HQS.VUE.Unity.OLD;

internal class ResyncUnityChunk : IKeyed<ChunkKey>, IDeinitializable
{
	private readonly VoxelSizeConfig config;

	private readonly float sideLength;

	private readonly QuadMultiMeshBuilder meshes;

	private readonly QuadConverterList quadConverter;

	private readonly SubchunkColliderCount[] subchunkColliderCounts;

	////private readonly FastList<SubchunkMeshCollider> changedSubchunkColliders;

	////private readonly FastList<SubchunkMeshCollider> unchangedSubchunkColliders;

	private readonly FastList<BoxColliderStruct> colliders;

	public ResyncUnityChunk(
		UnityConfig unityConfig,
		VoxelSizeConfig sizeConfig,
		VoxelTypesConfig typesConfig,
		IPinPool<QuadMeshBuilder> meshBuilderPool)
	{
		Debug.Assert(unityConfig != null);
		Debug.Assert(sizeConfig != null);
		Debug.Assert(typesConfig != null);
		Debug.Assert(meshBuilderPool != null);

		this.config = sizeConfig;
		this.sideLength = sizeConfig.VoxelIndexer.SideLength;

		this.meshes = new QuadMultiMeshBuilder(meshBuilderPool);
		this.quadConverter = new QuadConverterList(typesConfig.GetColor);

		this.subchunkColliderCounts = new SubchunkColliderCount[sizeConfig.SubchunkIndexer.Length];
		this.colliders = new FastList<BoxColliderStruct>(unityConfig.ResyncChunkCollidersCapacity);
	}

	/// <inheritdoc />
	public ChunkKey Key { get; private set; }

	public Int3 VoxelIndexOffset { get; private set; }

	public IReadOnlyList<IMeshDefinition> Meshes => this.meshes;

	////public IReadOnlyList<SubchunkMeshCollider> ChangedSubchunkColliders => this.changedSubchunkColliders;

	////public IReadOnlyList<SubchunkMeshCollider> UnchangedSubchunkColliders => this.unchangedSubchunkColliders;

	#region OLD BoxCollider stuff

	public int CountOfSubchunks_0_ChangedAndHasColliders { get; private set; }

	public int CountOfSubchunks_1_ChangedAndNoColliders { get; private set; }

	public int CountOfSubchunks_2_UnchangedAndHasColliders { get; private set; }

	// changed colliders always appear at the beginning of Colliders
	public int CountOfChangedColliders { get; private set; }

	// there is always an entry for every subchunk and they appear in the order of;
	// ChangedAndHasColliders, ChangedAndNoColliders, UnchangedAndHasColliders, UnchangedAndNoColliders
	public ReadOnlyArray<SubchunkColliderCount> SubchunkColliderCounts =>
		new ReadOnlyArray<SubchunkColliderCount>(this.subchunkColliderCounts);

	// colliders appear in the same order of SubchunkColliderCounts
	public IReadOnlyList<BoxColliderStruct> Colliders => this.colliders;

	#endregion

	/// <inheritdoc />
	public void Deinitialize()
	{
		// this clearing is cheap to do and done on the main Unity thread
		// this releases objects and to their pools so that pool counts don't show objects still OnLoan
		this.meshes.QuickClear();
	}

	// TODO not done yet, I think this was started as part of the change to mesh colliders
	// this is called on one of the background threads
	public void CopyInNEW(ReadOnlyContourChunkResult chunk)
	{
		Debug.Assert(!chunk.IsNull);

		// this clearing is more expensive to perform so it's done on the background threads
		// this is okay to do because all the data being cleared is copied value types
		// no references to objects are being held onto longer than they should be delaying this clearing
		this.meshes.QuickClear();
		this.colliders.FastClear();

		////this.changedSubchunkColliders.Clear();
		////this.unchangedSubchunkColliders.Clear();

		this.Key = chunk.ContourPin.Value.Key;
		this.VoxelIndexOffset = this.config.ConvertChunkToVoxelIndex(this.Key.Location);
		this.CountOfSubchunks_0_ChangedAndHasColliders = 0;
		this.CountOfSubchunks_1_ChangedAndNoColliders = 0;
		this.CountOfSubchunks_2_UnchangedAndHasColliders = 0;
		this.CountOfChangedColliders = 0;

		if (chunk.Subchunks.IsNull)
		{
			// the chunk is completely uniform
			if (chunk.VoxelChangeset.IsNull)
			{
			}
			else
			{
			}
		}
		else
		{
			// the chunk is not uniform so handle copying in subchunks
			if (chunk.VoxelChangeset.IsNull)
			{
			}
			else
			{
			}

			this.quadConverter.Source = null;
		}
	}

	// this is called on one of the background threads
	public void CopyIn(ReadOnlyContourChunkResult chunk)
	{
		Debug.Assert(!chunk.IsNull);

		// this clearing is more expensive to perform so it's done on the background threads
		// this is okay to do because all the data being cleared is copied value types
		// no references to objects are being held onto longer than they should be delaying this clearing
		this.meshes.QuickClear();
		this.colliders.FastClear();

		this.Key = chunk.ContourPin.Value.Key;
		this.VoxelIndexOffset = this.config.ConvertChunkToVoxelIndex(this.Key.Location);
		this.CountOfSubchunks_0_ChangedAndHasColliders = 0;
		this.CountOfSubchunks_1_ChangedAndNoColliders = 0;
		this.CountOfSubchunks_2_UnchangedAndHasColliders = 0;
		this.CountOfChangedColliders = 0;

		if (chunk.Subchunks.IsNull)
		{
			// the chunk is completely uniform
			// subchunkColliderCounts array is never cleared, instead this method guarantees that the entire array
			// will always be overwritten with new data. Subchunks being null means the entire chunk is empty,
			// so set all the collider counts to 0.
			if (chunk.VoxelChangeset.IsNull)
			{
				int max = this.subchunkColliderCounts.Length;
				for (int i = 0; i < max; i++)
				{
					this.subchunkColliderCounts[i] = new SubchunkColliderCount(i, 0);
				}
			}
			else
			{
				var changeset = chunk.VoxelChangeset;
				this.CountOfSubchunks_1_ChangedAndNoColliders = changeset.ChangedSubchunksCount;
				var changedSubchunks = changeset.ChangedSubchunks;

				int max = this.subchunkColliderCounts.Length;
				for (int i = 0; i < max; i++)
				{
					this.subchunkColliderCounts[i] = new SubchunkColliderCount(changedSubchunks[i], (ushort)0);
				}
			}
		}
		else
		{
			// the chunk is not uniform so handle copying in subchunks
			if (chunk.VoxelChangeset.IsNull)
			{
				this.CopyIn(chunk.Subchunks);
			}
			else
			{
				this.CopyInWithChangeset(chunk.Subchunks, chunk.VoxelChangeset);

				// precompute the count of changed colliders
				int max = this.CountOfSubchunks_0_ChangedAndHasColliders;
				for (int i = 0; i < max; i++)
				{
					this.CountOfChangedColliders += this.subchunkColliderCounts[i].Count;
				}
			}

			this.quadConverter.Source = null;
		}
	}

	private void CopyIn(ReadOnlyContourSubchunkArray subchunks)
	{
		Debug.Assert(!subchunks.IsNull);

		int assignToIndex = 0;
		int max = subchunks.Length;

		// copy in subchunks with colliders
		for (int subchunkIndex = 0; subchunkIndex < max; subchunkIndex++)
		{
			if (this.TryCopyInSubchunkWithColliders(subchunks[subchunkIndex], subchunkIndex, ref assignToIndex))
			{
				this.CountOfSubchunks_2_UnchangedAndHasColliders++;
			}
		}

		// copy in subchunks without colliders
		for (int subchunkIndex = 0; subchunkIndex < max; subchunkIndex++)
		{
			this.TryCopyInSubchunkNoColliders(subchunks[subchunkIndex], subchunkIndex, ref assignToIndex);
		}
	}

	private void CopyInWithChangeset(ReadOnlyContourSubchunkArray subchunks, VoxelChunkChangeset changeset)
	{
		Debug.Assert(!subchunks.IsNull);
		Debug.Assert(!changeset.IsNull);
		Debug.Assert(subchunks.Length == changeset.ChangedSubchunks.Length);

		int assignToIndex = 0;
		var changedSubchunks = changeset.ChangedSubchunks;

		// changed subchunks first
		int max = changeset.ChangedSubchunksCount;

		// copy in changed subchunks with colliders
		for (int i = 0; i < max; i++)
		{
			int subchunkIndex = changedSubchunks[i];
			if (this.TryCopyInSubchunkWithColliders(subchunks[subchunkIndex], subchunkIndex, ref assignToIndex))
			{
				this.CountOfSubchunks_0_ChangedAndHasColliders++;
			}
		}

		// copy in changed subchunks without colliders
		for (int i = 0; i < max; i++)
		{
			int subchunkIndex = changedSubchunks[i];
			if (this.TryCopyInSubchunkNoColliders(subchunks[subchunkIndex], subchunkIndex, ref assignToIndex))
			{
				this.CountOfSubchunks_1_ChangedAndNoColliders++;
			}
		}

		// unchanged subchunks last
		int start = max;
		max = subchunks.Length;

		// copy in unchanged subchunks with colliders
		for (int i = start; i < max; i++)
		{
			int subchunkIndex = changedSubchunks[i];
			if (this.TryCopyInSubchunkWithColliders(subchunks[subchunkIndex], subchunkIndex, ref assignToIndex))
			{
				this.CountOfSubchunks_2_UnchangedAndHasColliders++;
			}
		}

		// copy in unchanged subchunks without colliders
		for (int i = start; i < max; i++)
		{
			int subchunkIndex = changedSubchunks[i];
			this.TryCopyInSubchunkNoColliders(subchunks[subchunkIndex], subchunkIndex, ref assignToIndex);
		}
	}

	private bool TryCopyInSubchunkWithColliders(ReadOnlyContourSubchunk subchunk, int subchunkIndex, ref int assignToIndex)
	{
		if (subchunk.IsEmpty || subchunk.Colliders.Count == 0)
		{
			return false;
		}

		var offset = this.AddMesh(subchunk.Mesh, subchunkIndex);

		// copy in the expanded box colliders
		int collidersCount = subchunk.Colliders.Count;
		for (int i = 0; i < collidersCount; i++)
		{
			this.colliders.Add(subchunk.Colliders[i].Expand(offset));
		}

		this.subchunkColliderCounts[assignToIndex] = new SubchunkColliderCount(subchunkIndex, (ushort)collidersCount);
		assignToIndex++;
		return true;
	}

	private bool TryCopyInSubchunkNoColliders(ReadOnlyContourSubchunk subchunk, int subchunkIndex, ref int assignToIndex)
	{
		if (subchunk.IsEmpty)
		{
			this.subchunkColliderCounts[assignToIndex] = new SubchunkColliderCount(subchunkIndex, 0);
			assignToIndex++;
			return true;
		}

		if (subchunk.Colliders.Count > 0)
		{
			return false;
		}

		this.AddMesh(subchunk.Mesh, subchunkIndex);

		this.subchunkColliderCounts[assignToIndex] = new SubchunkColliderCount(subchunkIndex, 0);
		assignToIndex++;
		return true;
	}

	private System.Numerics.Vector3 AddMesh(IReadOnlyList<MeshQuadSlim> mesh, int subchunkIndex)
	{
		Debug.Assert(mesh != null);

		var temp = this.config.SubchunkIndexer[subchunkIndex];
		var offset = new Vector3(temp.X * this.sideLength, temp.Y * this.sideLength, temp.Z * this.sideLength);

		this.quadConverter.Source = mesh;
		this.meshes.Add(this.quadConverter, offset);

		return offset.ToSystem();
	}
}
