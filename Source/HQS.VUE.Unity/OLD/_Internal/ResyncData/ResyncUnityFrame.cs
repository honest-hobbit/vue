﻿namespace HQS.VUE.Unity.OLD;

internal class ResyncUnityFrame : IDeinitializable
{
	private readonly List<Pin<ResyncUnityVolume>> volumes = new List<Pin<ResyncUnityVolume>>();

	private Pin<IVoxelJobInfo> jobCompleted;

	public ResyncUnityFrame()
	{
		this.Volumes = ReadOnlyList.Convert(this.volumes, x => x.Value);
	}

	public IReadOnlyList<ResyncUnityVolume> Volumes { get; }

	public Pinned<IVoxelJobInfo> JobCompleted => this.jobCompleted.AsPinned;

	public bool CheckCollision { get; set; }

	public bool CollisionDetected { get; set; }

	public Signal ResyncVerified { get; } = Signal.CreateManualReset(isSet: false);

	public void SetJobCompleted(Pinned<IVoxelJobInfo> jobCompleted)
	{
		if (jobCompleted.IsUnassigned)
		{
			return;
		}

		this.jobCompleted = jobCompleted.CreatePin();
	}

	public void AddVolume(Pin<ResyncUnityVolume> volumePin)
	{
		Debug.Assert(volumePin.IsPinned);

		this.volumes.Add(volumePin);
	}

	public void WaitUntilCopyingFinished()
	{
		int max = this.Volumes.Count;
		for (int i = 0; i < max; i++)
		{
			this.Volumes[i].WaitUntilCopyingFinished();
		}
	}

	/// <inheritdoc />
	public void Deinitialize()
	{
		int max = this.volumes.Count;
		for (int i = 0; i < max; i++)
		{
			this.volumes[i].Dispose();
		}

		this.volumes.Clear();
		this.jobCompleted.Dispose();
		this.jobCompleted = default;
		this.CheckCollision = false;
		this.CollisionDetected = false;
		this.ResyncVerified.Reset();
	}
}
