﻿namespace HQS.VUE.Unity.OLD;

internal class UnityObservables : AbstractDisposable
{
	// VoxelEngine.JobDiagnostics are observed on the voxel engine main thread,
	// so QueuedObservable is used to marshal those notifications to the Unity main thread.
	// ResyncUnityPipeline.Diagnostics doesn't need this because it's already on the Unity main thread.
	private readonly QueuedObservable<VoxelJobDiagnostics> jobDiagnosticsQueue = new QueuedObservable<VoxelJobDiagnostics>();

	private readonly QueuedObservable<Exception> errorOccurredQueue = new QueuedObservable<Exception>();

	public UnityObservables(
		ResyncUnityPipeline resyncUnity,
		MainCommandProcessorOLD jobManager,
		Reporter<Exception> errorReporter)
	{
		Debug.Assert(resyncUnity != null);
		Debug.Assert(jobManager != null);
		Debug.Assert(errorReporter != null);

		this.ResyncCompleted = resyncUnity.Diagnostics;

		jobManager.Diagnostics.Subscribe(this.jobDiagnosticsQueue.Source);
		errorReporter.Values.Subscribe(this.errorOccurredQueue.Source);
	}

	public IObservable<ResyncUnityDiagnostics> ResyncCompleted { get; }

	public IObservable<VoxelJobDiagnostics> JobCompleted => this.jobDiagnosticsQueue.Result;

	public IObservable<Exception> ErrorOccurred => this.errorOccurredQueue.Result;

	public void Update()
	{
		Debug.Assert(!this.IsDisposed);

		this.jobDiagnosticsQueue.RunQueue();
		this.errorOccurredQueue.RunQueue();
	}

	/// <inheritdoc />
	protected override void ManagedDisposal()
	{
		this.jobDiagnosticsQueue.Dispose();
		this.errorOccurredQueue.Dispose();
	}
}
