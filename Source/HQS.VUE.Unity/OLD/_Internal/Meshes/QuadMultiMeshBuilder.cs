﻿namespace HQS.VUE.Unity.OLD;

internal class QuadMultiMeshBuilder : IReadOnlyList<IMeshDefinition>
{
	private readonly List<Pin<QuadMeshBuilder>> builders = new List<Pin<QuadMeshBuilder>>();

	private readonly IPinPool<QuadMeshBuilder> pool;

	public QuadMultiMeshBuilder(IPinPool<QuadMeshBuilder> pool)
	{
		Debug.Assert(pool != null);

		this.pool = pool;
	}

	/// <inheritdoc />
	public int Count => this.builders.Count;

	/// <inheritdoc />
	public IMeshDefinition this[int index] => this.builders[index].Value;

	public QuadMultiMeshBuilder Add(IReadOnlyList<MeshQuad> mesh) => this.Add(mesh, Vector3.zero);

	public QuadMultiMeshBuilder Add(IReadOnlyList<MeshQuad> mesh, Vector3 offset)
	{
		Debug.Assert(mesh != null);

		if (mesh.Count == 0)
		{
			return this;
		}

		QuadMeshBuilder builder;
		if (this.builders.Count == 0)
		{
			var pooled = this.pool.Rent(out builder);
			builder.Clear();
			this.builders.Add(pooled);
		}
		else
		{
			builder = this.builders[this.builders.Count - 1].Value;
		}

		builder.Offset = offset;
		for (int index = 0; index < mesh.Count; index++)
		{
			while (!builder.TryAdd(mesh[index]))
			{
				var pooled = this.pool.Rent(out builder);
				builder.Clear();
				builder.Offset = offset;
				this.builders.Add(pooled);
			}
		}

		return this;
	}

	public void Clear()
	{
		int max = this.builders.Count;
		for (int i = 0; i < max; i++)
		{
			var pooledBuilder = this.builders[i];
			pooledBuilder.Value.Clear();
			pooledBuilder.Dispose();
		}

		this.builders.Clear();
	}

	public void QuickClear()
	{
		int max = this.builders.Count;
		for (int i = 0; i < max; i++)
		{
			this.builders[i].Dispose();
		}

		this.builders.Clear();
	}

	public IMeshDefinition SingleOrEmpty()
	{
		Debug.Assert(this.Count <= 1);

		return this.Count == 1 ? this[0] : MeshDefinition.Empty;
	}

	/// <inheritdoc />
	public IEnumerator<IMeshDefinition> GetEnumerator()
	{
		foreach (var pooled in this.builders)
		{
			yield return pooled.Value;
		}
	}

	/// <inheritdoc />
	IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
}
