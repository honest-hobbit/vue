﻿namespace HQS.VUE.Unity.OLD;

internal class QuadMeshBuilder : AbstractMeshBuilder
{
	public QuadMeshBuilder(UnityConfig config)
		: this(config.ResyncChunkMeshQuadsCapacity)
	{
	}

	private QuadMeshBuilder(int initialQuadsCapacity)
		: base(OptionalMeshParts.Normals | OptionalMeshParts.Colors, initialQuadsCapacity * 6, initialQuadsCapacity * 4)
	{
		Debug.Assert(initialQuadsCapacity >= 0 && initialQuadsCapacity <= MaxCapacity);
	}

	public static int MaxCapacity { get; } = MeshConstants.MaxVertices / 4;

	public static QuadMeshBuilder CreateWithMaxVertexCapacity() => new QuadMeshBuilder(MaxCapacity);

	public Vector3 Offset { get; set; } = Vector3.zero;

	public bool TryAdd(MeshQuad quad)
	{
		if (this.Builder.Vertices.Count + 4 > MeshConstants.MaxVertices)
		{
			return false;
		}

		int index = this.Builder.Vertices.Count;

		// C, A, B
		this.Builder.Triangles.Add(index + 2);
		this.Builder.Triangles.Add(index);
		this.Builder.Triangles.Add(index + 1);

		// B, D, C
		this.Builder.Triangles.Add(index + 1);
		this.Builder.Triangles.Add(index + 3);
		this.Builder.Triangles.Add(index + 2);

		// vertices
		this.Builder.Vertices.Add(quad.A.ToUnity() + this.Offset);
		this.Builder.Vertices.Add(quad.B.ToUnity() + this.Offset);
		this.Builder.Vertices.Add(quad.C.ToUnity() + this.Offset);
		this.Builder.Vertices.Add(quad.D.ToUnity() + this.Offset);

		var normal = quad.Normal.ToUnity();
		this.Builder.Normals.Add(normal);
		this.Builder.Normals.Add(normal);
		this.Builder.Normals.Add(normal);
		this.Builder.Normals.Add(normal);

		var color = quad.Color.ToUnity();
		this.Builder.Colors.Add(color);
		this.Builder.Colors.Add(color);
		this.Builder.Colors.Add(color);
		this.Builder.Colors.Add(color);

		return true;
	}
}
