﻿namespace HQS.VUE.Unity.OLD.Utility.Particles;

[SuppressMessage("Design", "CA1001", Justification = "OnDestroy handles disposal.")]
public class AnimateAllVoxelParticlesOnCpuComponent : AnimateAllVoxelParticlesComponent
{
	[SerializeField]
	private int batchSize = 100;

	[SerializeField]
	private int maxDegreeOfParallelism = 1;

	private RangeProcessor voxelAddedProcessor;

	private RangeBatcher batcher;

	/// <inheritdoc />
	public override void OnValidate()
	{
		base.OnValidate();
		this.batchSize = this.batchSize.ClampLower(1);

		this.maxDegreeOfParallelism = DegreeOfParallelism.ClampToProcessorCount(this.maxDegreeOfParallelism);
	}

	/// <inheritdoc />
	public override void Awake()
	{
		base.Awake();
		this.voxelAddedProcessor = new RangeProcessor(this.UpdateParticle);
		this.voxelAddedProcessor.MaxDegreeOfParallelism = this.maxDegreeOfParallelism;
	}

	// run the animation for the added voxels, this is done in late update so the particle system will
	// have already moved the voxels according to noise and now we lerp them towards their final destinations
	public void LateUpdate()
	{
		int particleCount = this.VoxelAddedSystem.GetParticles(this.AddedParticles);
		if (particleCount > 0)
		{
			this.VoxelAddedSystem.GetCustomParticleData(this.AddedCustomData1, ParticleSystemCustomData.Custom1);
			this.VoxelAddedSystem.GetCustomParticleData(this.AddedCustomData2, ParticleSystemCustomData.Custom2);

			this.batcher = new RangeBatcher(particleCount, this.batchSize);
			this.voxelAddedProcessor.Run(this.batcher.BatchesCount);

			this.VoxelAddedSystem.SetParticles(this.AddedParticles, particleCount);
		}
	}

	/// <inheritdoc />
	protected override void OnDestroy()
	{
		base.OnDestroy();
		this.voxelAddedProcessor?.Dispose();
	}

	private void UpdateParticle(int batchIndex)
	{
		var particles = this.AddedParticles;
		var customData1 = this.AddedCustomData1;
		var customData2 = this.AddedCustomData2;
		var (start, end) = this.batcher.GetBatchedRange(batchIndex);

		for (int i = start; i < end; i++)
		{
			float percentLifetime = (particles[i].startLifetime - particles[i].remainingLifetime) / customData1[i].w;
			float adjusted = Mathf.Lerp(0, 1, percentLifetime);
			adjusted = adjusted * adjusted * adjusted;
			particles[i].position = Vector3.Lerp(particles[i].position, customData1[i], percentLifetime * adjusted);
			particles[i].startSize3D = Vector3.Lerp(Vector3.zero, customData2[i], percentLifetime);
		}
	}
}
