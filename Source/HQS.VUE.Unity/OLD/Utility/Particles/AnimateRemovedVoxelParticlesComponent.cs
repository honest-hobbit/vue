﻿namespace HQS.VUE.Unity.OLD.Utility.Particles;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class AnimateRemovedVoxelParticlesComponent : AnimateVoxelParticlesComponent
{
	[SerializeField]
	private ParticleSystem voxelRemovedSystem = null;

	private int maxParticles;

	/// <inheritdoc />
	public override bool IsAnimating => this.voxelRemovedSystem.particleCount > 0;

	public void Awake()
	{
		Ensure.That(this.voxelRemovedSystem, nameof(this.voxelRemovedSystem)).IsNotNull();

		this.maxParticles = this.voxelRemovedSystem.main.maxParticles;
	}

	/// <inheritdoc />
	protected override void HandleUpdatingVolume(
		IResyncVolumeInstance resyncVolume, Pin<IVoxelParticlesRecord> pooledRecord)
	{
		Debug.Assert(resyncVolume != null);

		if (pooledRecord.TryGetValue(out var record))
		{
			int removeCount = record.RemoveCount.ClampUpper(this.maxParticles);
			if (removeCount > 0)
			{
				var particle = default(ParticleSystem.EmitParams);
				var removed = record.RemoveParticles;
				this.Timer.Restart();

				for (int i = 0; i < removeCount; i++)
				{
					particle.position = removed[i].Position.ToUnity();
					particle.startColor = removed[i].Color.ToUnity();
					this.voxelRemovedSystem.Emit(particle, 1);
				}

				int particleCount = this.voxelRemovedSystem.particleCount;
				this.OnNextVoxelsRemoved(removeCount, particleCount);
			}

			pooledRecord.Dispose();
		}

		resyncVolume.ApplyResync();
		resyncVolume.Finished();
	}

	/// <inheritdoc />
	protected override void HandleDeinitializing()
	{
	}
}
