﻿namespace HQS.VUE.Unity.OLD.Utility.Particles;

[SuppressMessage("Design", "CA1001", Justification = "OnDestroy handles disposal.")]
public abstract class AnimateVoxelParticlesComponent : AnimateVoxelChangesComponent<IVoxelParticlesRecord>
{
	private readonly Subject<ParticleSystemDiagnostics> voxelsAdded = new Subject<ParticleSystemDiagnostics>();

	private readonly Subject<ParticleSystemDiagnostics> voxelsRemoved = new Subject<ParticleSystemDiagnostics>();

	public AnimateVoxelParticlesComponent()
	{
		this.VoxelsAdded = this.voxelsAdded.AsObservableOnly();
		this.VoxelsRemoved = this.voxelsRemoved.AsObservableOnly();
	}

	public IObservable<ParticleSystemDiagnostics> VoxelsAdded { get; }

	public IObservable<ParticleSystemDiagnostics> VoxelsRemoved { get; }

	protected Stopwatch Timer { get; } = new Stopwatch();

	protected void OnNextVoxelsAdded(int particlesEmitted, int totalActiveParticles)
	{
		this.Timer.Stop();
		this.OnNextVoxelsAdded(this.Timer.Elapsed, particlesEmitted, totalActiveParticles);
	}

	protected void OnNextVoxelsRemoved(int particlesEmitted, int totalActiveParticles)
	{
		this.Timer.Stop();
		this.OnNextVoxelsRemoved(this.Timer.Elapsed, particlesEmitted, totalActiveParticles);
	}

	protected void OnNextVoxelsAdded(TimeSpan emitDuration, int particlesEmitted, int totalActiveParticles) =>
		this.voxelsAdded.OnNext(new ParticleSystemDiagnostics(emitDuration, particlesEmitted, totalActiveParticles));

	protected void OnNextVoxelsRemoved(TimeSpan emitDuration, int particlesEmitted, int totalActiveParticles) =>
		this.voxelsRemoved.OnNext(new ParticleSystemDiagnostics(emitDuration, particlesEmitted, totalActiveParticles));

	protected virtual void OnDestroy()
	{
		this.voxelsAdded.OnCompleted();
		this.voxelsRemoved.OnCompleted();
		this.voxelsAdded.Dispose();
		this.voxelsRemoved.Dispose();
	}
}
