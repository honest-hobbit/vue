﻿namespace HQS.VUE.Unity.OLD.Utility.Particles;

public static class AnimateVoxelParticlesPoolComponentExtensions
{
	public static Pin<AnimateVoxelParticlesComponent> GetVoxelChangeAnimatorNullSafe(
		this AnimateVoxelParticlesPoolComponent pool) => pool?.GetVoxelChangeAnimator() ?? default;
}
