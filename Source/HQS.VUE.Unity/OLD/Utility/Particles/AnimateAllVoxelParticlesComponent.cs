﻿namespace HQS.VUE.Unity.OLD.Utility.Particles;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class AnimateAllVoxelParticlesComponent : AnimateVoxelParticlesComponent
{
	private readonly Queue<(IResyncVolumeInstance resync, Pin<IVoxelParticlesRecord> record, float remainingLifeTime)>
		pendingResyncs = new Queue<(IResyncVolumeInstance, Pin<IVoxelParticlesRecord>, float)>();

	[SerializeField]
	private ParticleSystem voxelAddedSystem = null;

	[SerializeField]
	private ParticleSystem voxelRemovedSystem = null;

	[SerializeField]
	private float accelerateSimulationSpeedMultiplier = 1f;

	[SerializeField]
	private float maxSimulationSpeed = 1f;

	private int maxAddParticles;

	private int maxRemoveParticles;

	private float initialSimulationSpeed;

	private float voxelAddedLifetime;

	private float minVoxelLifetime;

	/// <inheritdoc />
	public override bool IsAnimating => this.voxelAddedSystem.particleCount > 0 || this.voxelRemovedSystem.particleCount > 0;

	protected ParticleSystem VoxelAddedSystem => this.voxelAddedSystem;

	protected ParticleSystem.Particle[] AddedParticles { get; private set; }

	protected List<Vector4> AddedCustomData1 { get; private set; }

	protected List<Vector4> AddedCustomData2 { get; private set; }

	public void Update()
	{
		// update the remaining life times of all pending resyncs and apply them if completed
		bool canApply = true;
		float deltaTime = Time.deltaTime * this.voxelAddedSystem.main.simulationSpeed;
		int count = this.pendingResyncs.Count;

		for (int i = 0; i < count; i++)
		{
			var (resync, record, remainingLifeTime) = this.pendingResyncs.Dequeue();

			float remainingLife = remainingLifeTime - deltaTime;
			if (remainingLife <= 0 && canApply)
			{
				this.ApplyResyncVolume(resync, record);
			}
			else
			{
				// canApply remains true until the first resync that isn't ready
				// all resyncs that come after that are re-enqueued even if their lifetime reaches 0
				// this ensures that resyncs are applied in the same order they are received
				canApply = false;
				this.pendingResyncs.Enqueue((resync, record, remainingLife.ClampLower(0)));
			}
		}
	}

	public virtual void OnValidate()
	{
		this.accelerateSimulationSpeedMultiplier = this.accelerateSimulationSpeedMultiplier.ClampLower(0);
		this.maxSimulationSpeed = this.maxSimulationSpeed.ClampLower(0);
	}

	public virtual void Awake()
	{
		Ensure.That(this.voxelAddedSystem, nameof(this.voxelAddedSystem)).IsNotNull();
		Ensure.That(this.voxelRemovedSystem, nameof(this.voxelRemovedSystem)).IsNotNull();

		this.maxRemoveParticles = this.voxelRemovedSystem.main.maxParticles;

		var addedMain = this.voxelAddedSystem.main;
		this.maxAddParticles = addedMain.maxParticles;
		this.AddedParticles = new ParticleSystem.Particle[this.maxAddParticles];
		this.AddedCustomData1 = new List<Vector4>(this.maxAddParticles);
		this.AddedCustomData2 = new List<Vector4>(this.maxAddParticles);

		this.initialSimulationSpeed = addedMain.simulationSpeed;
		this.voxelAddedLifetime = addedMain.startLifetime.constant;
		this.minVoxelLifetime = this.voxelAddedLifetime / 4f;
	}

	/// <inheritdoc />
	protected override void HandleDeinitializing()
	{
		int count = this.pendingResyncs.Count;
		for (int i = 0; i < count; i++)
		{
			var (resync, record, _) = this.pendingResyncs.Dequeue();
			record.Dispose();
			resync.Finished();
		}

		this.pendingResyncs.Clear();
	}

	/// <inheritdoc />
	protected override void HandleUpdatingVolume(IResyncVolumeInstance resyncVolume, Pin<IVoxelParticlesRecord> record)
	{
		Debug.Assert(resyncVolume != null);

		this.StartResyncingVolume(resyncVolume, record);

		// optionally adjust the voxel animation speed depending on how many resyncs are pending
		if (this.accelerateSimulationSpeedMultiplier != 1f)
		{
			if (this.pendingResyncs.Count > 1)
			{
				var main = this.voxelAddedSystem.main;
				main.simulationSpeed =
					(main.simulationSpeed * this.accelerateSimulationSpeedMultiplier)
					.ClampUpper(this.maxSimulationSpeed);
			}
			else
			{
				var main = this.voxelAddedSystem.main;
				main.simulationSpeed = this.initialSimulationSpeed;
			}
		}
	}

	//// This could be done much more efficiently (twice as fast) by initializing the Particles
	//// on a background thread instead of calling ToUnity() on the main thread
	//// and then spawning the particles by using;
	////
	//// this.voxelAddedSystem.Emit(addCount);
	//// int particleCount = this.voxelAddedSystem.GetParticles(particles);
	//// loop assign to particles[i] and then
	//// this.voxelAddedSystem.SetParticles(particles, addCount);
	////
	//// Instead of using this.voxelAddedSystem.Emit(particle, 1); in a loop (very slow).
	//// Also look into this;
	//// https://forum.unity.com/threads/more-efficient-way-to-emit-many-particles.538530/
	//// https://learn.unity.com/tutorial/particle-system-ordered-mesh-emission-2019-3#
	//// Particle System: Ordered Mesh Emission via the Shape Module set to Mesh
	private void StartResyncingVolume(IResyncVolumeInstance resyncVolume, Pin<IVoxelParticlesRecord> pooledRecord)
	{
		Debug.Assert(resyncVolume != null);

		if (pooledRecord.IsUnassigned)
		{
			this.pendingResyncs.Enqueue((resyncVolume, default, 0));
			return;
		}

		var record = pooledRecord.Value;
		int addCount = record.AddCount.ClampUpper(this.maxAddParticles);
		if (addCount == 0)
		{
			// no voxel particles to add so enqueue the resync to still handle updating meshes and removed voxels as usual
			float time = (record.TimingMode == VoxelChangeTiming.Immediate) ? 0f : this.voxelAddedLifetime;
			this.pendingResyncs.Enqueue((resyncVolume, pooledRecord, time));
			return;
		}

		// emit the added voxel particles
		var particle = default(ParticleSystem.EmitParams);
		var added = record.AddParticles;

		this.Timer.Restart();

		for (int i = 0; i < addCount; i++)
		{
			particle.startSize3D = added[i].Size.ToUnity();
			particle.position = added[i].Position.ToUnity();
			particle.startColor = added[i].Color.ToUnity();
			this.voxelAddedSystem.Emit(particle, 1);
		}

		// assign custom data for the voxels added
		var particles = this.AddedParticles;
		var customData1 = this.AddedCustomData1;
		var customData2 = this.AddedCustomData2;
		int particleCount = this.voxelAddedSystem.GetParticles(particles);
		this.voxelAddedSystem.GetCustomParticleData(customData1, ParticleSystemCustomData.Custom1);
		this.voxelAddedSystem.GetCustomParticleData(customData2, ParticleSystemCustomData.Custom2);

		for (int i = 0; i < particleCount; i++)
		{
			if (customData1[i].w == 0)
			{
				customData1[i] = new Vector4(
					particles[i].position.x,
					particles[i].position.y,
					particles[i].position.z,
					UnityEngine.Random.Range(this.minVoxelLifetime, this.voxelAddedLifetime));
				customData2[i] = particles[i].startSize3D;
			}
		}

		this.voxelAddedSystem.SetCustomParticleData(customData1, ParticleSystemCustomData.Custom1);
		this.voxelAddedSystem.SetCustomParticleData(customData2, ParticleSystemCustomData.Custom2);

		this.OnNextVoxelsAdded(addCount, particleCount);

		this.pendingResyncs.Enqueue((resyncVolume, pooledRecord, this.voxelAddedLifetime));
	}

	private void ApplyResyncVolume(IResyncVolumeInstance resyncVolume, Pin<IVoxelParticlesRecord> pooledRecord)
	{
		Debug.Assert(resyncVolume != null);

		if (pooledRecord.TryGetValue(out var record))
		{
			int removeCount = record.RemoveCount.ClampUpper(this.maxRemoveParticles);
			if (removeCount > 0)
			{
				// emit the removed voxel particles
				var particle = default(ParticleSystem.EmitParams);
				var removed = record.RemoveParticles;
				this.Timer.Restart();

				for (int i = 0; i < removeCount; i++)
				{
					particle.startSize3D = removed[i].Size.ToUnity();
					particle.position = removed[i].Position.ToUnity();
					particle.startColor = removed[i].Color.ToUnity();
					this.voxelRemovedSystem.Emit(particle, 1);
				}

				int particleCount = this.voxelRemovedSystem.particleCount;
				this.OnNextVoxelsRemoved(removeCount, particleCount);
			}

			pooledRecord.Dispose();
		}

		resyncVolume.ApplyResync();
		resyncVolume.Finished();
	}
}
