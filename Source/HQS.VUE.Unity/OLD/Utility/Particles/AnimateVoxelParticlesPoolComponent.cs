﻿namespace HQS.VUE.Unity.OLD.Utility.Particles;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class AnimateVoxelParticlesPoolComponent : MonoBehaviour
{
	private StringBuilder logBuilder;

	private IPinPool<AnimateVoxelParticlesComponent> animatorPool;

	[SerializeField]
	private VoxelUniverseComponent universeComponent = null;

	[SerializeField]
	private AnimateVoxelParticlesComponent volumeAnimatorPrefab = null;

	[SerializeField]
	private int maxAdded = 0;

	[SerializeField]
	private int maxRemoved = 0;

	[SerializeField]
	private int reduceAddedLimit = -1;

	[SerializeField]
	private int reduceRemovedLimit = -1;

	[SerializeField]
	private int dontAnimateAddingLimit = 0;

	[SerializeField]
	private int dontAnimateRemovingLimit = 0;

	[SerializeField]
	private bool animateReplacing = false;

	[SerializeField]
	private bool onlySurfaces = false;

	[SerializeField]
	private bool combineParticles = false;

	[SerializeField]
	private bool clearIfFull = false;

	[SerializeField]
	private bool logDiagnostics = false;

	[SerializeField]
	private bool logMaximums = false;

	public double VoxelsAddedMaxEmitDuration { get; private set; }

	public int VoxelsAddedMaxParticlesEmitted { get; private set; }

	public int VoxelsAddedMaxTotalActiveParticles { get; private set; }

	public double VoxelsRemovedMaxEmitDuration { get; private set; }

	public int VoxelsRemovedMaxParticlesEmitted { get; private set; }

	public int VoxelsRemovedMaxTotalActiveParticles { get; private set; }

	public Pin<AnimateVoxelParticlesComponent> GetVoxelChangeAnimator()
	{
		this.TryCreatePool();
		return this.animatorPool?.Rent() ?? default;
	}

	public void OnValidate()
	{
		this.maxAdded = this.maxAdded.ClampLower(0);
		this.maxRemoved = this.maxRemoved.ClampLower(0);
		this.reduceAddedLimit = this.reduceAddedLimit.ClampLower(-1);
		this.reduceRemovedLimit = this.reduceRemovedLimit.ClampLower(-1);
		this.dontAnimateAddingLimit = this.dontAnimateAddingLimit.ClampLower(0);
		this.dontAnimateRemovingLimit = this.dontAnimateRemovingLimit.ClampLower(0);
	}

	private static void LogMaximums(
		AnimateVoxelParticlesComponent component, IObservable<ParticleSystemDiagnostics> diagnostics, string diagnosticsName)
	{
		Debug.Assert(component != null);
		Debug.Assert(diagnostics != null);
		Debug.Assert(!diagnosticsName.IsNullOrWhiteSpace());

		diagnostics.Select(x => x.EmitDuration.TotalMilliseconds).MaxScan().Subscribe(
			x => UnityEngine.Debug.Log($"{diagnosticsName} Max {nameof(ParticleSystemDiagnostics.EmitDuration)}: {x:n2} ms", component));

		diagnostics.Select(x => x.ParticlesEmitted).MaxScan().Subscribe(
			x => UnityEngine.Debug.Log($"{diagnosticsName} Max {nameof(ParticleSystemDiagnostics.ParticlesEmitted)}: {x:n0}", component));

		diagnostics.Select(x => x.TotalActiveParticles).MaxScan().Subscribe(
			x => UnityEngine.Debug.Log($"{diagnosticsName} Max {nameof(ParticleSystemDiagnostics.TotalActiveParticles)}: {x:n0}", component));
	}

	private void TryCreatePool()
	{
		if (this.animatorPool != null || this.universeComponent == null || this.volumeAnimatorPrefab == null)
		{
			return;
		}

		var universe = this.universeComponent.Universe;
		var table = new VoxelParticleCombinerTable(universe.SizeConfig.VoxelIndexer);

		var recordPool = new PinPool<IVoxelParticlesRecord>(
			() => new VoxelParticlesRecord(universe.Definitions, table, this.maxAdded, this.maxRemoved)
			{
				AnimateReplacing = this.animateReplacing,
				OnlySurfaces = this.onlySurfaces,
				CombineParticles = this.combineParticles,
				ClearIfFull = this.clearIfFull,
				DontAnimateAddingMaxThreshold = this.dontAnimateAddingLimit,
				DontAnimateRemovingMaxThreshold = this.dontAnimateRemovingLimit,
				ReduceAddingThreshold = this.reduceAddedLimit,
				ReduceRemovingThreshold = this.reduceRemovedLimit,
			});

		bool isLogging = this.logDiagnostics;
		bool isLoggingMaximums = this.logMaximums;
		if (isLogging || isLoggingMaximums)
		{
			this.logBuilder = new StringBuilder(ParticleSystemDiagnostics.AverageStringLength);
		}

		this.animatorPool = GameObjectPool.ThreadSafe.Pins.Create(
			this.volumeAnimatorPrefab,
			x =>
			{
				x.RecordPool = recordPool;
				if (isLogging)
				{
					this.LogDiagnostics(x, x.VoxelsAdded, nameof(x.VoxelsAdded));
					this.LogDiagnostics(x, x.VoxelsRemoved, nameof(x.VoxelsRemoved));
				}

				if (isLoggingMaximums)
				{
					LogMaximums(x, x.VoxelsAdded, nameof(x.VoxelsAdded));
					LogMaximums(x, x.VoxelsRemoved, nameof(x.VoxelsRemoved));
				}

				x.VoxelsAdded.Select(y => y.EmitDuration.TotalMilliseconds).MaxScan().Subscribe(y => this.VoxelsAddedMaxEmitDuration = y);
				x.VoxelsAdded.Select(y => y.ParticlesEmitted).MaxScan().Subscribe(y => this.VoxelsAddedMaxParticlesEmitted = y);
				x.VoxelsAdded.Select(y => y.TotalActiveParticles).MaxScan().Subscribe(y => this.VoxelsAddedMaxTotalActiveParticles = y);

				x.VoxelsRemoved.Select(y => y.EmitDuration.TotalMilliseconds).MaxScan().Subscribe(y => this.VoxelsRemovedMaxEmitDuration = y);
				x.VoxelsRemoved.Select(y => y.ParticlesEmitted).MaxScan().Subscribe(y => this.VoxelsRemovedMaxParticlesEmitted = y);
				x.VoxelsRemoved.Select(y => y.TotalActiveParticles).MaxScan().Subscribe(y => this.VoxelsRemovedMaxTotalActiveParticles = y);
			});
	}

	private void LogDiagnostics(
		AnimateVoxelParticlesComponent component, IObservable<ParticleSystemDiagnostics> diagnostics, string diagnosticsName)
	{
		Debug.Assert(component != null);
		Debug.Assert(diagnostics != null);
		Debug.Assert(!diagnosticsName.IsNullOrWhiteSpace());

		diagnostics.Subscribe(x =>
		{
			this.logBuilder.AppendLine(diagnosticsName);
			x.AddToBuilder(this.logBuilder);
			UnityEngine.Debug.Log(this.logBuilder.ToString(), component);
			this.logBuilder.Clear();
		});
	}
}
