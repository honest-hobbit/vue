﻿#if UNITY_EDITOR

namespace HQS.VUE.Unity.OLD.Utility.Particles;

[CustomEditor(typeof(AnimateVoxelParticlesPoolComponent))]
public class AnimateVoxelParticlesPoolComponentInspector : Editor
{
	private const float LabelScale = 1.1f;

	public override void OnInspectorGUI()
	{
		this.DrawDefaultInspector();
		var component = (AnimateVoxelParticlesPoolComponent)this.target;

		InspectorUtility.LabelHeader("Voxels Added Diagnostics");

		InspectorUtility.SelectableTextFieldWithLabel(
			$"Max {nameof(ParticleSystemDiagnostics.EmitDuration)}",
			$"{component.VoxelsAddedMaxEmitDuration:n2} ms",
			LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			$"Max {nameof(ParticleSystemDiagnostics.ParticlesEmitted)}",
			$"{component.VoxelsAddedMaxParticlesEmitted:n0}",
			LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			$"Max {nameof(ParticleSystemDiagnostics.TotalActiveParticles)}",
			$"{component.VoxelsAddedMaxTotalActiveParticles:n0}",
			LabelScale);

		InspectorUtility.LabelHeader("Voxels Removed Diagnostics");

		InspectorUtility.SelectableTextFieldWithLabel(
			$"Max {nameof(ParticleSystemDiagnostics.EmitDuration)}",
			$"{component.VoxelsRemovedMaxEmitDuration:n2} ms",
			LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			$"Max {nameof(ParticleSystemDiagnostics.ParticlesEmitted)}",
			$"{component.VoxelsRemovedMaxParticlesEmitted:n0}",
			LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			$"Max {nameof(ParticleSystemDiagnostics.TotalActiveParticles)}",
			$"{component.VoxelsRemovedMaxTotalActiveParticles:n0}",
			LabelScale);
	}
}

#endif
