﻿namespace HQS.VUE.Unity.OLD;

public static class VoxelPhysics
{
	public static bool Raycast(
		Vector3 origin, Vector3 direction, out VoxelHit voxelHitInfo, float maxDistance = Mathf.Infinity) =>
		Raycast(new Ray(origin, direction), out voxelHitInfo, out _, maxDistance);

	public static bool Raycast(
		Vector3 origin, Vector3 direction, out VoxelHit voxelHitInfo, out RaycastHit hitInfo, float maxDistance = Mathf.Infinity) =>
		Raycast(new Ray(origin, direction), out voxelHitInfo, out hitInfo, maxDistance);

	public static bool Raycast(
		Ray ray, out VoxelHit voxelHitInfo, float maxDistance = Mathf.Infinity) =>
		Raycast(ray, out voxelHitInfo, out _, maxDistance);

	public static bool Raycast(
		Ray ray, out VoxelHit voxelHitInfo, out RaycastHit hitInfo, float maxDistance = Mathf.Infinity)
	{
		if (Physics.Raycast(ray, out hitInfo, maxDistance))
		{
			var chunk = hitInfo.collider.GetComponent<ChunkCollidersComponent>()?.Chunk;
			if (chunk != null)
			{
				var hitPoint = chunk.transform.InverseTransformPoint(hitInfo.point);
				var hitNormal = chunk.transform.InverseTransformDirection(hitInfo.normal);
				hitNormal *= .5f;

				var hitIndex = VectorToVoxelIndex(hitPoint - hitNormal) + chunk.VoxelIndexOffset;
				var hitAdjacent = VectorToVoxelIndex(hitPoint + hitNormal) + chunk.VoxelIndexOffset;

				Direction direction;
				if (hitAdjacent.X != hitIndex.X)
				{
					direction = (hitAdjacent.X < hitIndex.X) ? Direction.NegativeX : Direction.PositiveX;
				}
				else if (hitAdjacent.Y != hitIndex.Y)
				{
					direction = (hitAdjacent.Y < hitIndex.Y) ? Direction.NegativeY : Direction.PositiveY;
				}
				else
				{
					direction = (hitAdjacent.Z < hitIndex.Z) ? Direction.NegativeZ : Direction.PositiveZ;
				}

				voxelHitInfo = new VoxelHit(chunk.Volume, hitIndex, hitAdjacent, direction);
				return true;
			}
			else
			{
				voxelHitInfo = default;
				return false;
			}
		}
		else
		{
			voxelHitInfo = default;
			return false;
		}
	}

	private static Int3 VectorToVoxelIndex(Vector3 point) =>
		new Int3(Mathf.FloorToInt(point.x), Mathf.FloorToInt(point.y), Mathf.FloorToInt(point.z));
}
