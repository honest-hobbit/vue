﻿namespace HQS.VUE.Unity.OLD;

public interface IUnityDiagnostics : VUE.OLD.IDiagnostics
{
	IObservable<ResyncUnityDiagnostics> ResyncCompleted { get; }
}
