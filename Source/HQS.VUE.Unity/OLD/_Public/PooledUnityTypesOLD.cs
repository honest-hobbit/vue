﻿namespace HQS.VUE.Unity.OLD;

public static class PooledUnityTypesOLD
{
	public static IEnumerable<PooledType> EnumerateAll() =>
		Resync.Enumerate().Concat(GameObjects.Enumerate());

	public static class Resync
	{
		public static PooledType ResyncFrame { get; } = new PooledType(
			new Guid("0d20d906-cda0-484d-8f5d-0b0d40864b6e"),
			CreateName(nameof(ResyncFrame)),
			typeof(ResyncUnityFrame));

		public static PooledType ResyncVolume { get; } = new PooledType(
			new Guid("57cac2df-b784-462c-8e50-22f8d854f1bb"),
			CreateName(nameof(ResyncVolume)),
			typeof(ResyncUnityVolume));

		public static PooledType ResyncVolumeInstance { get; } = new PooledType(
			new Guid("791c320f-7e8c-4cf3-a896-be2cbc755eec"),
			CreateName(nameof(ResyncVolumeInstance)),
			typeof(ResyncUnityVolumeInstance));

		public static PooledType ResyncChunk { get; } = new PooledType(
			new Guid("a93a70ef-a0b1-4fce-ae88-99e453d7d72f"),
			CreateName(nameof(ResyncChunk)),
			typeof(ResyncUnityChunk));

		public static PooledType MeshBuilder { get; } = new PooledType(
			new Guid("57c3ca15-4ade-439f-81e9-5b73d1648d14"),
			CreateName(nameof(MeshBuilder)),
			typeof(QuadMeshBuilder));

		public static PooledType CopyResyncChunkCommand { get; } = new PooledType(
			new Guid("4ccf0a70-2ba3-4d38-9d60-dfba60438acf"),
			CreateName(nameof(CopyResyncChunkCommand)),
			typeof(CopyResyncChunkCommand));

		public static IEnumerable<PooledType> Enumerate()
		{
			yield return ResyncFrame;
			yield return ResyncVolume;
			yield return ResyncVolumeInstance;
			yield return ResyncChunk;

			yield return MeshBuilder;
			yield return CopyResyncChunkCommand;
		}

		private static string CreateName(string name) => $"{nameof(PooledUnityTypesOLD)}.{nameof(Resync)}.{name}";
	}

	public static class GameObjects
	{
		public static PooledType VolumeComponent { get; } = new PooledType(
			new Guid("14485699-ac59-4db1-8415-dfac05cb4dc8"),
			CreateName(nameof(VolumeComponent)),
			typeof(VolumeComponent));

		public static PooledType ChunkComponent { get; } = new PooledType(
			new Guid("b4ab27c9-ba97-4dcb-82ce-29519ff27182"),
			CreateName(nameof(ChunkComponent)),
			typeof(ChunkComponent));

		public static PooledType MeshComponent { get; } = new PooledType(
			new Guid("b4636b8c-5867-46aa-b69f-3e39fc47e3c6"),
			CreateName(nameof(MeshComponent)),
			typeof(MeshFilter));

		public static PooledType ChunkCollidersComponent { get; } = new PooledType(
			new Guid("153d5019-3476-4803-87e3-a1ddf4d745e7"),
			CreateName(nameof(ChunkCollidersComponent)),
			typeof(ChunkCollidersComponent));

		public static PooledType VerifyCollidersComponent { get; } = new PooledType(
			new Guid("ea2f1f8a-7ac1-4eac-9611-d0c069cd942a"),
			CreateName(nameof(VerifyCollidersComponent)),
			typeof(VerifyCollidersComponent));

		public static PooledType ChunkComponentSubchunk { get; } = new PooledType(
			new Guid("f55e55ef-feea-40eb-a5a8-3cb259290d68"),
			CreateName(nameof(ChunkComponentSubchunk)),
			typeof(ChunkComponentSubchunk));

		public static IEnumerable<PooledType> Enumerate()
		{
			yield return VolumeComponent;
			yield return ChunkComponent;
			yield return MeshComponent;
			yield return ChunkCollidersComponent;
			yield return VerifyCollidersComponent;
			yield return ChunkComponentSubchunk;
		}

		private static string CreateName(string name) => $"{nameof(PooledUnityTypesOLD)}.{nameof(GameObjects)}.{name}";
	}
}
