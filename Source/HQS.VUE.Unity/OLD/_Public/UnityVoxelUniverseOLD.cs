﻿namespace HQS.VUE.Unity.OLD;

[SuppressMessage("Design", "CA1001", Justification = "AbstractCompletable handles disposal.")]
public class UnityVoxelUniverseOLD : AbstractCompletable, IVoxelUniverseOLD
{
	private readonly Container container;

	private readonly VoxelEngineOLD engine;

	private readonly ResyncUnityPipeline resyncUnity;

	private readonly UnityDiagnostics diagnostics;

	public UnityVoxelUniverseOLD(UnityConfig unityConfig, UniverseConfig universeConfig, VoxelTypesConfig voxelTypes)
	{
		Ensure.That(unityConfig, nameof(unityConfig)).IsNotNull();
		Ensure.That(universeConfig, nameof(universeConfig)).IsNotNull();
		Ensure.That(voxelTypes, nameof(voxelTypes)).IsNotNull();

		this.container = new Container();
		this.container.SetUpVoxelEngineOLD(universeConfig, voxelTypes);
		this.container.SetUpUnityHostApplication(unityConfig);

		this.container.VerifyInDebugOnly();

		this.engine = this.container.GetInstance<VoxelEngineOLD>();
		this.resyncUnity = this.container.GetInstance<ResyncUnityPipeline>();
		this.diagnostics = this.container.GetInstance<UnityDiagnostics>();
	}

	public IUnityDiagnostics Diagnostics => this.diagnostics;

	/// <inheritdoc />
	HQS.VUE.OLD.IDiagnostics IVoxelUniverseOLD.Diagnostics => this.diagnostics;

	/// <inheritdoc />
	public IVoxelSizeConfig SizeConfig => this.engine.SizeConfig;

	/// <inheritdoc />
	public IVoxelDefinitions Definitions => this.engine.Definitions;

	/// <inheritdoc />
	public IVolumes Volumes => this.engine.Volumes;

	/// <inheritdoc />
	public IVoxelJobProcessorOLD Jobs => this.engine.Jobs;

	internal void UnityFixedUpdate()
	{
		Debug.Assert(!this.IsCompleting);

		this.resyncUnity.UnityFixedUpdate();
	}

	internal void UnityUpdate()
	{
		Debug.Assert(!this.IsCompleting);

		this.engine.Update();
		this.resyncUnity.UnityUpdate();
		this.diagnostics.UpdateObservables();
	}

	/// <inheritdoc />
	protected override async Task CompleteAsync()
	{
		try
		{
			this.engine.Complete();
			await this.engine.Completion.DontMarshallContext();
		}
		finally
		{
			this.container.Dispose();
		}
	}
}
