﻿namespace HQS.VUE.Unity.OLD;

public readonly struct VoxelHit : IEquatable<VoxelHit>
{
	public VoxelHit(VolumeComponent volume, Int3 hitIndex, Int3 adjacentIndex, Direction adjacentDirection)
	{
		this.Volume = volume;
		this.HitIndex = hitIndex;
		this.AdjacentIndex = adjacentIndex;
		this.AdjacentDirection = adjacentDirection;
	}

	public VolumeComponent Volume { get; }

	public Int3 HitIndex { get; }

	public Int3 AdjacentIndex { get; }

	public Direction AdjacentDirection { get; }

	/// <inheritdoc />
	public override bool Equals(object obj) => Struct.Equals(this, obj);

	/// <inheritdoc />
	public bool Equals(VoxelHit other) =>
		this.Volume == other.Volume &&
		this.HitIndex == other.HitIndex &&
		this.AdjacentIndex == other.AdjacentIndex &&
		this.AdjacentDirection == other.AdjacentDirection;

	/// <inheritdoc />
	public override int GetHashCode() =>
		HashCode.Combine(this.Volume, this.HitIndex, this.AdjacentIndex, this.AdjacentDirection);

	public static bool operator ==(VoxelHit left, VoxelHit right) => left.Equals(right);

	public static bool operator !=(VoxelHit left, VoxelHit right) => !(left == right);
}
