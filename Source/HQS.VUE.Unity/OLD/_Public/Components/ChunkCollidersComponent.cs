﻿namespace HQS.VUE.Unity.OLD;

public class ChunkCollidersComponent : MonoBehaviour
{
	private Stack<BoxCollider> availableColliders;

	private int maxColliders;

	internal ChunkComponent Chunk { get; set; }

	internal bool IsInitialized => this.availableColliders != null;

	internal void Initialize(List<BoxCollider> temporaryColliders)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(this.Chunk == null);
		Debug.Assert(temporaryColliders != null);
		Debug.Assert(temporaryColliders.Count == 0);

		this.GetComponents(temporaryColliders);
		this.maxColliders = temporaryColliders.Count;
		this.availableColliders = new Stack<BoxCollider>(this.maxColliders);

		for (int i = 0; i < this.maxColliders; i++)
		{
			this.availableColliders.Push(temporaryColliders[i]);
		}

		temporaryColliders.Clear();
	}

	internal int AvailableCollidersCount => this.availableColliders.Count;

	internal bool AllCollidersDisabled => this.AvailableCollidersCount == this.maxColliders;

	internal void Clear()
	{
		this.Chunk = null;
	}

	internal void EnsureEnoughColliders(int collidersCount)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.Chunk != null);
		Debug.Assert(collidersCount > 0);

		if (this.AvailableCollidersCount >= collidersCount)
		{
			return;
		}

		var gameObject = this.gameObject;
		int max = collidersCount - this.AvailableCollidersCount;
		for (int count = 0; count < max; count++)
		{
			var box = gameObject.AddComponent<BoxCollider>();
			box.enabled = false;
			this.availableColliders.Push(box);
		}

		UnityEngine.Debug.LogWarning($"Subchunk had more colliders ({collidersCount}) than what could fit in a single empty {nameof(ChunkCollidersComponent)}.");
	}

	internal void InitializeSubchunk(
		ChunkComponentSubchunk subchunk, IReadOnlyList<BoxColliderStruct> colliders, ref int colliderIndex, int colliderCount)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.Chunk != null);
		Debug.Assert(subchunk != null);
		Debug.Assert(subchunk.Chunk == null);
		Debug.Assert(subchunk.Colliders.Count == 0);
		Debug.Assert(colliders != null);
		Debug.Assert(colliderCount > 0);
		Debug.Assert(this.AvailableCollidersCount >= colliderCount);
		Debug.Assert(colliders.IsIndexInBoundsReadOnly(colliderIndex + colliderCount - 1));

		colliderCount = colliderIndex + colliderCount;
		for (; colliderIndex < colliderCount; colliderIndex++)
		{
			var resyncBox = colliders[colliderIndex];
			var box = this.availableColliders.Pop();
			box.center = resyncBox.Center.ToUnity();
			box.size = resyncBox.Size.ToUnity();
			box.enabled = true;
			subchunk.Colliders.Add(box);
		}

		subchunk.Chunk = this;
	}

	internal void ClearSubchunk(ChunkComponentSubchunk subchunk)
	{
		Debug.Assert(this.IsInitialized);
		Debug.Assert(this.Chunk != null);
		Debug.Assert(subchunk != null);
		Debug.Assert(subchunk.Chunk.EqualsByReferenceNullSafe(this));
		Debug.Assert(subchunk.Colliders.Count > 0);

		var colliders = subchunk.Colliders;
		int max = colliders.Count;
		for (int i = 0; i < max; i++)
		{
			var box = colliders[i];
			box.enabled = false;
			this.availableColliders.Push(box);
		}

		subchunk.Colliders.Clear();
		subchunk.Chunk = null;

		if (this.AllCollidersDisabled)
		{
			this.Chunk.CleanupColliders = true;
		}
	}
}
