﻿namespace HQS.VUE.Unity.OLD;

public class VolumeComponent : MonoBehaviour, IVolumeInstance
{
	private Pin<AnimateVoxelChangesComponent> animator;

	private UnityVolumeInstances parent;

	private bool checkCollision = true;

	private bool finishAnimation = false;

	public bool IsInitialized => this.parent != null;

	public Volume Volume
	{
		get
		{
			this.ValidateInitialized();
			return this.parent.Volume;
		}
	}

	public Pin<AnimateVoxelChangesComponent> Animator
	{
		get
		{
			this.ValidateInitialized();
			return this.animator;
		}

		set
		{
			// TODO what to do if this already has an animator and you're assinging over it?
			// Should it be deinitialized? It could be in the middle of an animation.
			this.ValidateInitialized();
			if (value.IsPinned && value != this.animator)
			{
				this.animator = value;
				var animatorComponent = value.Value;
				animatorComponent.transform.SetParent(this.transform, false);
				animatorComponent.gameObject.SetActive(true);
			}
		}
	}

	public bool CheckCollision
	{
		get
		{
			this.ValidateInitialized();
			return this.checkCollision;
		}

		set
		{
			this.ValidateInitialized();
			this.checkCollision = value;
		}
	}

	internal bool IsDeactivating { get; private set; }

	internal Dictionary<Int3, ChunkComponent> Chunks { get; } =
		new Dictionary<Int3, ChunkComponent>(MortonCurve.EqualityComparer.ForInt3);

	/// <inheritdoc />
	public bool TryGetChangeRecord(out ChangeRecord record)
	{
		var pooledAnimator = this.Animator;
		if (pooledAnimator.TryGetValue(out var animator))
		{
			var pooledRecorder = animator.GetRecord();
			if (pooledRecorder.IsPinned)
			{
				record = new ChangeRecord(
					pooledAnimator.CastTo<object>(), pooledRecorder);
				return true;
			}
		}

		record = default;
		return false;
	}

	public void SetAnimator(AnimateVoxelChangesComponent animator) => this.Animator = Pin.WithoutPool(animator);

	public void SetAnimator<TAnimator>(Pin<TAnimator> animator)
		where TAnimator : AnimateVoxelChangesComponent =>
		this.Animator = animator.CastToOrNone<AnimateVoxelChangesComponent>();

	public void RemoveUnityInstance(bool finishAnimation = false)
	{
		this.ValidateInitialized();

		this.finishAnimation = finishAnimation;
		if (!this.IsDeactivating)
		{
			this.IsDeactivating = true;
			this.parent.TrackDeactivatingInstance(this);
		}
	}

	internal void Initialize(UnityVolumeInstances parent)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(parent != null);

		this.parent = parent;
		this.IsDeactivating = false;
		this.finishAnimation = false;
	}

	internal bool IsFullyDeactivated()
	{
		Debug.Assert(this.IsDeactivating);

		if (this.animator.TryGetValue(out var animator))
		{
			if (this.finishAnimation && animator.IsAnimating)
			{
				return false;
			}

			animator.transform.parent = null;
			animator.gameObject.SetActive(false);
			animator.Deinitialize();
			this.animator.Dispose();
			this.animator = default;
		}

		this.checkCollision = true;
		this.IsDeactivating = false;
		this.finishAnimation = false;
		this.parent = null;
		return true;
	}

	private void ValidateInitialized()
	{
		if (!this.IsInitialized)
		{
			throw new InvalidOperationException(
				$"{nameof(VolumeComponent)} was not properly initialized. Don't use {nameof(this.gameObject.AddComponent)} to add a {nameof(VolumeComponent)} manually.");
		}
	}
}
