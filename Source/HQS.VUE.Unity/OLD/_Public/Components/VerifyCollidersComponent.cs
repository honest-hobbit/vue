﻿namespace HQS.VUE.Unity.OLD;

public class VerifyCollidersComponent : MonoBehaviour
{
	private BoxCollider[] colliders;

	private int enabledCount;

	internal ResyncUnityFrame Frame { get; set; }

	internal ChunkComponent Chunk { get; set; }

	public void OnTriggerEnter(Collider other)
	{
		Debug.Assert(this.Frame != null);
		Debug.Assert(this.Chunk != null);

		var chunk = other.GetComponent<ChunkCollidersComponent>()?.Chunk;
		if (!chunk.EqualsByReferenceNullSafe(this.Chunk))
		{
			this.Frame.CollisionDetected = true;
		}
	}

	internal bool TryAddCollider(BoxColliderStruct box, float downscale)
	{
		if (this.enabledCount == this.colliders.Length)
		{
			return false;
		}

		var collider = this.colliders[this.enabledCount];
		collider.center = box.Center.ToUnity();
		collider.size = new Vector3(box.Size.X - downscale, box.Size.Y - downscale, box.Size.Z - downscale);
		collider.enabled = true;
		this.enabledCount++;
		return true;
	}

	internal void Clear()
	{
		Debug.Assert(this.Frame != null);
		Debug.Assert(this.Chunk != null);
		Debug.Assert(this.enabledCount > 0);
		Debug.Assert(this.enabledCount <= this.colliders.Length);
		Debug.Assert(this.colliders[this.enabledCount - 1].enabled);
		Debug.Assert(this.enabledCount == this.colliders.Length || !this.colliders[this.enabledCount].enabled);

		this.Chunk?.VerifyColliders.Remove(this);
		this.Chunk = null;
		this.Frame = null;

		int max = this.enabledCount;
		for (int i = 0; i < max; i++)
		{
			this.colliders[i].enabled = false;
		}

		this.enabledCount = 0;
	}

	internal void Initialize()
	{
		this.colliders = this.GetComponents<BoxCollider>();
	}
}
