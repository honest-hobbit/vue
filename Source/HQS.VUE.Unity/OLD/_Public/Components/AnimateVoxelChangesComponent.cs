﻿namespace HQS.VUE.Unity.OLD;

public abstract class AnimateVoxelChangesComponent : MonoBehaviour
{
	public abstract bool IsAnimating { get; }

	internal Pin<IVoxelChangesRecord> GetRecord() => this.HandleGettingRecord();

	internal void UpdateVolume(IResyncVolumeInstance resyncVolume, Pin<IVoxelChangesRecord> record)
	{
		Debug.Assert(resyncVolume != null);

		this.HandleUpdatingVolume(resyncVolume, record);
	}

	internal void Deinitialize() => this.HandleDeinitializing();

	protected abstract Pin<IVoxelChangesRecord> HandleGettingRecord();

	protected abstract void HandleUpdatingVolume(IResyncVolumeInstance resyncVolume, Pin<IVoxelChangesRecord> record);

	protected abstract void HandleDeinitializing();
}
