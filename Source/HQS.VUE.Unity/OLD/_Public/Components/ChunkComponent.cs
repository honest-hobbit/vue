﻿namespace HQS.VUE.Unity.OLD;

public class ChunkComponent : MonoBehaviour
{
	public VolumeComponent Volume { get; internal set; }

	internal ChunkKey Key { get; set; }

	internal Int3 VoxelIndexOffset { get; set; }

	internal ChunkComponentSubchunk[] Subchunks { get; private set; }

	internal List<MeshFilter> Meshes { get; } = new List<MeshFilter>();

	internal List<ChunkCollidersComponent> Colliders { get; } = new List<ChunkCollidersComponent>();

	internal List<VerifyCollidersComponent> VerifyColliders { get; } = new List<VerifyCollidersComponent>();

	internal bool AreCollidersInitialized { get; set; }

	internal bool CleanupColliders { get; set; }

	internal bool IsInitialized => this.Subchunks != null;

	internal void Initialize(VoxelSizeConfig config)
	{
		Debug.Assert(!this.IsInitialized);
		Debug.Assert(config != null);

		this.Subchunks = new ChunkComponentSubchunk[config.SubchunkIndexer.Length];
	}
}
