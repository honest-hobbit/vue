﻿namespace HQS.VUE.Unity.OLD;

public class VoxelUniverseComponent : MonoBehaviour
{
	private UnityVoxelUniverseOLD universe;

	public bool IsInitialized => this.universe != null;

	public UnityVoxelUniverseOLD Universe
	{
		get
		{
			if (!this.IsInitialized)
			{
				throw new InvalidOperationException();
			}

			return this.universe;
		}
	}

	public void Initialize(UnityConfig unityConfig, UniverseConfig universeConfig, VoxelTypesConfig voxelTypes)
	{
		if (this.IsInitialized)
		{
			throw new InvalidOperationException();
		}

		Ensure.That(unityConfig, nameof(unityConfig)).IsNotNull();
		Ensure.That(universeConfig, nameof(universeConfig)).IsNotNull();
		Ensure.That(voxelTypes, nameof(voxelTypes)).IsNotNull();

		this.universe = new UnityVoxelUniverseOLD(unityConfig, universeConfig, voxelTypes);
		////this.universe.Diagnostics.ErrorOccurred.Subscribe(x => Debug.LogError(x));
	}

	public void FixedUpdate()
	{
		this.universe?.UnityFixedUpdate();
	}

	public void Update()
	{
		this.universe?.UnityUpdate();
	}

	public void OnDestroy()
	{
		this.universe?.Complete();
		this.universe = null;
	}
}
