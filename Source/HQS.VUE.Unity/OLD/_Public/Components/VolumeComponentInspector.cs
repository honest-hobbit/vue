﻿#if UNITY_EDITOR

namespace HQS.VUE.Unity.OLD;

[CustomEditor(typeof(VolumeComponent))]
public class VolumeComponentInspector : Editor
{
	private const float LabelScale = .75f;

	private bool finishAnimation = false;

	public override void OnInspectorGUI()
	{
		var volumeComponent = (VolumeComponent)this.target;
		if (!volumeComponent.IsInitialized)
		{
			return;
		}

		var volume = volumeComponent.Volume;

		InspectorUtility.SelectableTextFieldWithLabel(
			nameof(volume.Key), volume.Key.ToString(), LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			nameof(volume.Created), volume.Created.ToLocalTime().ToString(), LabelScale);

		InspectorUtility.LabelHeader("Config");
		volume.Name = InspectorUtility.TextFieldWithLabel(
			nameof(volume.Name), volume.Name, LabelScale);
		volume.Autosave = InspectorUtility.ToggleWithLabel(
			nameof(volume.Autosave), volume.Autosave, LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			nameof(volume.HasVoxelGridStore), volume.HasVoxelGridStore.ToString(), LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			nameof(volume.HasVoxelShapeStore), volume.HasVoxelShapeStore.ToString(), LabelScale);

		InspectorUtility.LabelHeader("Stats");
		InspectorUtility.SelectableTextFieldWithLabel(
			nameof(volume.LastModified), volume.LastModified.ToLocalTime().ToString(), LabelScale);
		InspectorUtility.SelectableTextFieldWithLabel(
			nameof(volume.Version), volume.Version.ToString(), LabelScale);

		InspectorUtility.SelectableTextFieldWithLabel(
			nameof(volume.HasUnsavedChanges), volume.HasUnsavedChanges.ToString(), LabelScale);

		if (volume.HasSavePending)
		{
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Save Changes"))
			{
				volume.SaveChanges();
			}

			EditorGUILayout.EndHorizontal();
		}

		InspectorUtility.LabelHeader("Misc");
		volumeComponent.CheckCollision = InspectorUtility.ToggleWithLabel(
			nameof(volumeComponent.CheckCollision), volumeComponent.CheckCollision, LabelScale);

		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Remove Unity Instance"))
		{
			volumeComponent.RemoveUnityInstance(this.finishAnimation);
		}

		this.finishAnimation = InspectorUtility.ToggleWithLabel("Finish Animation", this.finishAnimation, .8f);
		EditorGUILayout.EndHorizontal();
	}
}

#endif
