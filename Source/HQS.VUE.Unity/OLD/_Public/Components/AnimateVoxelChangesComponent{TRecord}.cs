﻿namespace HQS.VUE.Unity.OLD;

public abstract class AnimateVoxelChangesComponent<TRecord> : AnimateVoxelChangesComponent
	where TRecord : class, IVoxelChangesRecord
{
	public IPinPool<TRecord> RecordPool { get; set; }

	/// <inheritdoc />
	protected sealed override Pin<IVoxelChangesRecord> HandleGettingRecord() =>
		this.RecordPool?.Rent().CastTo<IVoxelChangesRecord>() ?? default;

	/// <inheritdoc />
	protected sealed override void HandleUpdatingVolume(
		IResyncVolumeInstance resyncVolume, Pin<IVoxelChangesRecord> record) =>
		this.HandleUpdatingVolume(resyncVolume, record.CastToOrNone<TRecord>());

	protected abstract void HandleUpdatingVolume(IResyncVolumeInstance resyncVolume, Pin<TRecord> record);
}
