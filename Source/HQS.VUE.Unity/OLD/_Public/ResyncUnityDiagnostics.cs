﻿namespace HQS.VUE.Unity.OLD;

public readonly struct ResyncUnityDiagnostics
{
	internal ResyncUnityDiagnostics(ResyncUnityDiagnosticsRecorder diagnostics)
	{
		Debug.Assert(diagnostics != null);

		this.VerifySetupDuration = diagnostics.VerifySetupDuration;
		this.ApproxVerifyDuration = diagnostics.ApproxVerifyDuration;
		this.VerifyCleanupDuration = diagnostics.VerifyCleanupDuration;
		this.ResyncCollidersDuration = diagnostics.ResyncCollidersDuration;
		this.ResyncAnimatorsDuration = diagnostics.ResyncAnimatorsDuration;
	}

	public static int AverageStringLength => 200;

	public TimeSpan VerifySetupDuration { get; }

	public TimeSpan ApproxVerifyDuration { get; }

	public TimeSpan VerifyCleanupDuration { get; }

	public TimeSpan ResyncCollidersDuration { get; }

	public TimeSpan ResyncAnimatorsDuration { get; }

	public override string ToString()
	{
		var builder = new StringBuilder(AverageStringLength);
		this.AddToBuilder(builder);
		return builder.ToString();
	}

	public void AddToBuilder(StringBuilder builder)
	{
		Ensure.That(builder, nameof(builder)).IsNotNull();

		builder.AppendLine($"{nameof(this.VerifySetupDuration)}: {this.VerifySetupDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.ApproxVerifyDuration)}: {this.ApproxVerifyDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.VerifyCleanupDuration)}: {this.VerifyCleanupDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.ResyncCollidersDuration)}: {this.ResyncCollidersDuration.TotalMilliseconds:n2} ms");
		builder.AppendLine($"{nameof(this.ResyncAnimatorsDuration)}: {this.ResyncAnimatorsDuration.TotalMilliseconds:n2} ms");
	}
}
