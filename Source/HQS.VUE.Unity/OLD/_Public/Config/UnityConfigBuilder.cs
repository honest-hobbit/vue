﻿namespace HQS.VUE.Unity.OLD;

public class UnityConfigBuilder : AbstractConfigBuilder
{
	// TODO this should likely be handled differently. Not sure how yet.
	public Material VoxelMaterial { get; set; }

	public int BoxCollidersPerChunkCollider { get; set; } = 500;

	public int BoxCollidersPerVerifyCollider { get; set; } = 100;

	public float DownscaleVerifyColliders { get; set; } = .1f;

	public int ResyncChunkMeshQuadsCapacity { get; set; } = 16383;

	public int ResyncChunkCollidersCapacity { get; set; } = 8192;

	public TimeSpan MaxResyncDuration { get; set; } = TimeSpan.FromMilliseconds(4);

	public UnityConfig BuildConfig() => new UnityConfig(this);

	/// <inheritdoc />
	protected override void PerformValidation()
	{
		this.Validate(
			this.VoxelMaterial,
			nameof(this.VoxelMaterial),
			(value, name) => Ensure.That(value, name).IsNotNull());

		this.Validate(
			this.BoxCollidersPerChunkCollider,
			nameof(this.BoxCollidersPerChunkCollider),
			(value, name) => Ensure.That(value, name).IsGte(1));

		this.Validate(
			this.BoxCollidersPerVerifyCollider,
			nameof(this.BoxCollidersPerVerifyCollider),
			(value, name) => Ensure.That(value, name).IsGte(1));

		this.Validate(
			this.DownscaleVerifyColliders,
			nameof(this.DownscaleVerifyColliders),
			(value, name) => Ensure.That(value, name).IsInRange(0, 1));

		this.Validate(
			this.ResyncChunkMeshQuadsCapacity,
			nameof(this.ResyncChunkMeshQuadsCapacity),
			(value, name) => Ensure.That(value, name).IsInRange(0, QuadMeshBuilder.MaxCapacity));

		this.Validate(
			this.ResyncChunkCollidersCapacity,
			nameof(this.ResyncChunkCollidersCapacity),
			(value, name) => Ensure.That(value, name).IsGte(0));

		this.Validate(
			this.MaxResyncDuration,
			nameof(this.MaxResyncDuration),
			(value, name) => Duration.ValidateIsDuration(value, name));
	}
}
