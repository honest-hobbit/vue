﻿namespace HQS.VUE.Unity.OLD;

public class UnityConfig
{
	internal UnityConfig(UnityConfigBuilder config)
	{
		Debug.Assert(config != null);
		config.Validate();

		this.VoxelMaterial = config.VoxelMaterial;
		this.BoxCollidersPerChunkCollider = config.BoxCollidersPerChunkCollider;
		this.BoxCollidersPerVerifyCollider = config.BoxCollidersPerVerifyCollider;
		this.DownscaleVerifyColliders = config.DownscaleVerifyColliders;
		this.ResyncChunkMeshQuadsCapacity = config.ResyncChunkMeshQuadsCapacity;
		this.ResyncChunkCollidersCapacity = config.ResyncChunkCollidersCapacity;
		this.MaxResyncDuration = config.MaxResyncDuration;
	}

	public Material VoxelMaterial { get; }

	public int BoxCollidersPerChunkCollider { get; }

	public int BoxCollidersPerVerifyCollider { get; }

	public float DownscaleVerifyColliders { get; }

	public int ResyncChunkMeshQuadsCapacity { get; }

	public int ResyncChunkCollidersCapacity { get; }

	public TimeSpan MaxResyncDuration { get; }
}
