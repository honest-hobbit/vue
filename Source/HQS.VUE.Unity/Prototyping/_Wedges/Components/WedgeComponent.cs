﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class WedgeComponent : MonoBehaviour
{
	public byte CurrentWedgeIndex;

	////[ReadOnlyField]
	public string HexString;

	public bool SetNameToHexString;

	public bool ShowCorners;

	public float ResizeMeshPercentage;

	public float ResizeColliderPercentage;

	private static readonly WedgeBuilder WedgeBuilder = new();

	public void SetWedge(WedgeTable table, WedgeIndex index)
	{
		Ensure.That(table, nameof(table)).IsNotNull();

		this.SetWedge(table.GetDefinition(index));
		this.SetColliders(
			table.GetPart1Definition(index),
			table.GetPart2Definition(index));
	}

	public void SetWedge(WedgeDefinition definition)
	{
		this.HexString = WedgeHexEncoding.ToString(definition);
		if (this.SetNameToHexString)
		{
			this.name = this.HexString;
		}

		var meshFilter = this.GetComponent<MeshFilter>();
		if (meshFilter != null)
		{
			meshFilter.sharedMesh = WedgeBuilder.BuildMesh(
				definition.Surfaces, this.ResizeMeshPercentage);
		}

		if (this.ShowCorners)
		{
			var corners = this.GetComponent<WedgeCornersComponent>();
			if (corners != null)
			{
				corners.CreateCorners(definition.FilledCorners);
			}
		}
	}

	public void SetColliders(WedgeDefinition collider1, WedgeDefinition collider2)
	{
		var colliders = this.GetComponents<MeshCollider>();
		if (colliders?.Length != 2)
		{
			return;
		}

		SetCollider(collider1, colliders[0]);
		SetCollider(collider2, colliders[1]);

		void SetCollider(WedgeDefinition parts, MeshCollider collider)
		{
			if (parts.FilledCorners != WedgeCorners.None)
			{
				collider.sharedMesh = WedgeBuilder.BuildMesh(
					parts.Surfaces, this.ResizeColliderPercentage);
				collider.enabled = true;
			}
			else
			{
				collider.sharedMesh = null;
				collider.enabled = false;
			}
		}
	}
}
