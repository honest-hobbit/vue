﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[RequireComponent(typeof(WedgeBuilderComponent))]
public class WedgeOverlapsComponent : MonoBehaviour
{
	public WedgeTableAsset TableAsset;

	public WedgeSelectorComponent WedgeA;

	public WedgeSelectorComponent WedgeB;

	private WedgeBuilderComponent wedgeBuilder;

	private WedgeTable table;

	private WedgeSurfaces surfacesA;

	private WedgeSurfaces surfacesB;

	private void Start()
	{
		this.wedgeBuilder = this.GetComponent<WedgeBuilderComponent>();
		this.table = this.TableAsset.Table;
	}

	private void Update()
	{
		var surfacesA = this.table.GetDefinition(this.WedgeA.CurrentWedge).Surfaces;
		var surfacesB = this.table.GetDefinition(this.WedgeB.CurrentWedge).Surfaces;

		if (this.surfacesA != surfacesA || this.surfacesB != surfacesB)
		{
			this.surfacesA = surfacesA;
			this.surfacesB = surfacesB;

			this.wedgeBuilder.FaceNegX = surfacesA.FaceNegX & SurfaceUtility.FlipLeftRight(surfacesB.FacePosX);
			this.wedgeBuilder.FacePosX = surfacesA.FacePosX & SurfaceUtility.FlipLeftRight(surfacesB.FaceNegX);

			this.wedgeBuilder.FaceNegY = surfacesA.FaceNegY & SurfaceUtility.FlipTopBottom(surfacesB.FacePosY);
			this.wedgeBuilder.FacePosY = surfacesA.FacePosY & SurfaceUtility.FlipTopBottom(surfacesB.FaceNegY);

			this.wedgeBuilder.FaceNegZ = surfacesA.FaceNegZ & SurfaceUtility.FlipLeftRight(surfacesB.FacePosZ);
			this.wedgeBuilder.FacePosZ = surfacesA.FacePosZ & SurfaceUtility.FlipLeftRight(surfacesB.FaceNegZ);

			this.wedgeBuilder.EdgeXPosYPosZ =
				(SurfaceUtility.FlipLeftRight(surfacesA.EdgeXNegYNegZ) & surfacesB.EdgeXPosYPosZ) |
				(SurfaceUtility.FlipLeftRight(surfacesB.EdgeXNegYNegZ) & surfacesA.EdgeXPosYPosZ);
			this.wedgeBuilder.EdgeXNegYNegZ = SurfaceUtility.FlipLeftRight(this.wedgeBuilder.EdgeXPosYPosZ);

			this.wedgeBuilder.EdgeXPosYNegZ =
				(SurfaceUtility.FlipLeftRight(surfacesA.EdgeXNegYPosZ) & surfacesB.EdgeXPosYNegZ) |
				(SurfaceUtility.FlipLeftRight(surfacesB.EdgeXNegYPosZ) & surfacesA.EdgeXPosYNegZ);
			this.wedgeBuilder.EdgeXNegYPosZ = SurfaceUtility.FlipLeftRight(this.wedgeBuilder.EdgeXPosYNegZ);

			this.wedgeBuilder.EdgeYPosXPosZ =
				(SurfaceUtility.FlipLeftRight(surfacesA.EdgeYNegXNegZ) & surfacesB.EdgeYPosXPosZ) |
				(SurfaceUtility.FlipLeftRight(surfacesB.EdgeYNegXNegZ) & surfacesA.EdgeYPosXPosZ);
			this.wedgeBuilder.EdgeYNegXNegZ = SurfaceUtility.FlipLeftRight(this.wedgeBuilder.EdgeYPosXPosZ);

			this.wedgeBuilder.EdgeYPosXNegZ =
				(SurfaceUtility.FlipLeftRight(surfacesA.EdgeYNegXPosZ) & surfacesB.EdgeYPosXNegZ) |
				(SurfaceUtility.FlipLeftRight(surfacesB.EdgeYNegXPosZ) & surfacesA.EdgeYPosXNegZ);
			this.wedgeBuilder.EdgeYNegXPosZ = SurfaceUtility.FlipLeftRight(this.wedgeBuilder.EdgeYPosXNegZ);

			this.wedgeBuilder.EdgeZPosXPosY =
				(SurfaceUtility.FlipLeftRight(surfacesA.EdgeZNegXNegY) & surfacesB.EdgeZPosXPosY) |
				(SurfaceUtility.FlipLeftRight(surfacesB.EdgeZNegXNegY) & surfacesA.EdgeZPosXPosY);
			this.wedgeBuilder.EdgeZNegXNegY = SurfaceUtility.FlipLeftRight(this.wedgeBuilder.EdgeZPosXPosY);

			this.wedgeBuilder.EdgeZPosXNegY =
				(SurfaceUtility.FlipLeftRight(surfacesA.EdgeZNegXPosY) & surfacesB.EdgeZPosXNegY) |
				(SurfaceUtility.FlipLeftRight(surfacesB.EdgeZNegXPosY) & surfacesA.EdgeZPosXNegY);
			this.wedgeBuilder.EdgeZNegXPosY = SurfaceUtility.FlipLeftRight(this.wedgeBuilder.EdgeZPosXNegY);

			this.wedgeBuilder.CornerWideTop = surfacesA.CornerWideTop & SurfaceUtility.Negate(surfacesB.CornerWideTop);
			this.wedgeBuilder.CornerWideTop |= SurfaceUtility.Negate(this.wedgeBuilder.CornerWideTop);

			this.wedgeBuilder.CornerWideBot = surfacesA.CornerWideBot & SurfaceUtility.Negate(surfacesB.CornerWideBot);
			this.wedgeBuilder.CornerWideBot |= SurfaceUtility.Negate(this.wedgeBuilder.CornerWideBot);

			this.wedgeBuilder.RebuildWedge();
		}
	}
}
