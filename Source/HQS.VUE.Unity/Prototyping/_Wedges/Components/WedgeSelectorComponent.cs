namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[RequireComponent(typeof(WedgeComponent))]
public class WedgeSelectorComponent : MonoBehaviour
{
	public WedgeTableAsset TableAsset;

	public byte CurrentWedgeIndex;

	public KeyCode ResetKey = KeyCode.Backspace;

	public byte ResetToWedgeIndex;

	public bool LogClicks;

	public Material FilledCornerMaterial;

	public Material RemovableCornerMaterial;

	public Material AddableCornerMaterial;

	public GameObject CornerNNN;

	public GameObject CornerNNP;

	public GameObject CornerNPN;

	public GameObject CornerNPP;

	public GameObject CornerPNN;

	public GameObject CornerPNP;

	public GameObject CornerPPN;

	public GameObject CornerPPP;

	public GameObject[] MirrorX;

	public GameObject[] MirrorY;

	public GameObject[] MirrorZ;

	public GameObject[] RotateX90;

	public GameObject[] RotateX270;

	public GameObject[] RotateY90;

	public GameObject[] RotateY270;

	public GameObject[] RotateZ90;

	public GameObject[] RotateZ270;

	private WedgeComponent wedgeComponent;

	private WedgeIndex currentWedge;

	public WedgeIndex CurrentWedge => new WedgeIndex(this.CurrentWedgeIndex);

	private void Start()
	{
		this.wedgeComponent = this.GetComponent<WedgeComponent>();
		this.ResetCurrentWedge();
		this.UpdateGameObjects();
	}

	private void Update()
	{
		if (Input.GetKeyDown(this.ResetKey))
		{
			this.ResetCurrentWedge();
			this.UpdateGameObjects();
		}

		if (Input.GetMouseButtonDown(0))
		{
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out var hit, 100.0f))
			{
				var newWedge = this.HandleClickedOn(hit.transform.gameObject);
				if (newWedge != this.currentWedge && newWedge != WedgeIndex.Empty)
				{
					this.SetCurrentWedge(newWedge);
					this.UpdateGameObjects();
				}
			}
		}
	}

	private void ResetCurrentWedge()
	{
		this.ResetToWedgeIndex = this.ResetToWedgeIndex.Clamp((byte)0, (byte)(this.TableAsset.Table.Length - 1));
		this.SetCurrentWedge(new WedgeIndex(this.ResetToWedgeIndex));
	}

	private void SetCurrentWedge(WedgeIndex wedge)
	{
		this.currentWedge = wedge;
		this.CurrentWedgeIndex = wedge.Index;
	}

	private WedgeIndex HandleClickedOn(GameObject clickedOn)
	{
		var table = this.TableAsset.Table;

		if (clickedOn == this.CornerNNN)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerNNN)); }
			return ClickedOnCorner(
				table.AddCornerNNN(this.currentWedge),
				table.RemoveCornerNNN(this.currentWedge));
		}

		if (clickedOn == this.CornerNNP)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerNNP)); }
			return ClickedOnCorner(
				table.AddCornerNNP(this.currentWedge),
				table.RemoveCornerNNP(this.currentWedge));
		}

		if (clickedOn == this.CornerNPN)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerNPN)); }
			return ClickedOnCorner(
				table.AddCornerNPN(this.currentWedge),
				table.RemoveCornerNPN(this.currentWedge));
		}

		if (clickedOn == this.CornerNPP)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerNPP)); }
			return ClickedOnCorner(
				table.AddCornerNPP(this.currentWedge),
				table.RemoveCornerNPP(this.currentWedge));
		}

		if (clickedOn == this.CornerPNN)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerPNN)); }
			return ClickedOnCorner(
				table.AddCornerPNN(this.currentWedge),
				table.RemoveCornerPNN(this.currentWedge));
		}

		if (clickedOn == this.CornerPNP)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerPNP)); }
			return ClickedOnCorner(
				table.AddCornerPNP(this.currentWedge),
				table.RemoveCornerPNP(this.currentWedge));
		}

		if (clickedOn == this.CornerPPN)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerPPN)); }
			return ClickedOnCorner(
				table.AddCornerPPN(this.currentWedge),
				table.RemoveCornerPPN(this.currentWedge));
		}

		if (clickedOn == this.CornerPPP)
		{
			if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.CornerPPP)); }
			return ClickedOnCorner(
				table.AddCornerPPP(this.currentWedge),
				table.RemoveCornerPPP(this.currentWedge));
		}

		for (int i = 0; i < this.MirrorX.Length; i++)
		{
			if (clickedOn == this.MirrorX[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.MirrorX)); }
				return table.MirrorX(this.currentWedge);
			}
		}

		for (int i = 0; i < this.MirrorY.Length; i++)
		{
			if (clickedOn == this.MirrorY[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.MirrorY)); }
				return table.MirrorY(this.currentWedge);
			}
		}

		for (int i = 0; i < this.MirrorZ.Length; i++)
		{
			if (clickedOn == this.MirrorZ[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.MirrorZ)); }
				return table.MirrorZ(this.currentWedge);
			}
		}

		for (int i = 0; i < this.RotateX90.Length; i++)
		{
			if (clickedOn == this.RotateX90[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.RotateX90)); }
				return table.RotateX90(this.currentWedge);
			}
		}

		for (int i = 0; i < this.RotateX270.Length; i++)
		{
			if (clickedOn == this.RotateX270[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.RotateX270)); }
				return table.RotateX270(this.currentWedge);
			}
		}

		for (int i = 0; i < this.RotateY90.Length; i++)
		{
			if (clickedOn == this.RotateY90[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.RotateY90)); }
				return table.RotateY90(this.currentWedge);
			}
		}

		for (int i = 0; i < this.RotateY270.Length; i++)
		{
			if (clickedOn == this.RotateY270[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.RotateY270)); }
				return table.RotateY270(this.currentWedge);
			}
		}

		for (int i = 0; i < this.RotateZ90.Length; i++)
		{
			if (clickedOn == this.RotateZ90[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.RotateZ90)); }
				return table.RotateZ90(this.currentWedge);
			}
		}

		for (int i = 0; i < this.RotateZ270.Length; i++)
		{
			if (clickedOn == this.RotateZ270[i])
			{
				if (this.LogClicks) { UnityEngine.Debug.Log(nameof(this.RotateZ270)); }
				return table.RotateZ270(this.currentWedge);
			}
		}

		return this.currentWedge;

		WedgeIndex ClickedOnCorner(WedgeIndex addCorner, WedgeIndex removeCorner)
		{
			return addCorner != this.currentWedge ? addCorner : removeCorner;
		}
	}

	private void UpdateGameObjects()
	{
		var table = this.TableAsset.Table;

		this.wedgeComponent.SetWedge(table, this.currentWedge);

		var invalidWedge = this.currentWedge;

		this.UpdateMirror(table.MirrorX(this.currentWedge) != invalidWedge, this.MirrorX);
		this.UpdateMirror(table.MirrorY(this.currentWedge) != invalidWedge, this.MirrorY);
		this.UpdateMirror(table.MirrorZ(this.currentWedge) != invalidWedge, this.MirrorZ);

		this.UpdateRotate(table.RotateX90(this.currentWedge) != invalidWedge, this.RotateX90, this.RotateX270);
		this.UpdateRotate(table.RotateY90(this.currentWedge) != invalidWedge, this.RotateY90, this.RotateY270);
		this.UpdateRotate(table.RotateZ90(this.currentWedge) != invalidWedge, this.RotateZ90, this.RotateZ270);

		invalidWedge = WedgeIndex.Empty;
		var parts = table.GetDefinition(this.currentWedge);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.NNN),
			table.RemoveCornerNNN(this.currentWedge) != invalidWedge,
			table.AddCornerNNN(this.currentWedge) != invalidWedge,
			this.CornerNNN);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.NNP),
			table.RemoveCornerNNP(this.currentWedge) != invalidWedge,
			table.AddCornerNNP(this.currentWedge) != invalidWedge,
			this.CornerNNP);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.NPN),
			table.RemoveCornerNPN(this.currentWedge) != invalidWedge,
			table.AddCornerNPN(this.currentWedge) != invalidWedge,
			this.CornerNPN);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.NPP),
			table.RemoveCornerNPP(this.currentWedge) != invalidWedge,
			table.AddCornerNPP(this.currentWedge) != invalidWedge,
			this.CornerNPP);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.PNN),
			table.RemoveCornerPNN(this.currentWedge) != invalidWedge,
			table.AddCornerPNN(this.currentWedge) != invalidWedge,
			this.CornerPNN);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.PNP),
			table.RemoveCornerPNP(this.currentWedge) != invalidWedge,
			table.AddCornerPNP(this.currentWedge) != invalidWedge,
			this.CornerPNP);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.PPN),
			table.RemoveCornerPPN(this.currentWedge) != invalidWedge,
			table.AddCornerPPN(this.currentWedge) != invalidWedge,
			this.CornerPPN);

		this.UpdateCorner(
			parts.FilledCorners.Has(WedgeCorners.PPP),
			table.RemoveCornerPPP(this.currentWedge) != invalidWedge,
			table.AddCornerPPP(this.currentWedge) != invalidWedge,
			this.CornerPPP);
	}

	private void UpdateMirror(bool canMirror, GameObject[] mirror)
	{
		for (int i = 0; i < mirror.Length; i++)
		{
			mirror[i].SetActive(canMirror);
		}
	}

	private void UpdateRotate(bool canRotate, GameObject[] rotate90, GameObject[] rotate270)
	{
		for (int i = 0; i < rotate90.Length; i++)
		{
			rotate90[i].SetActive(canRotate);
			rotate270[i].SetActive(canRotate);
		}
	}

	private void UpdateCorner(bool isFilled, bool canRemove, bool canAdd, GameObject corner)
	{
		if (isFilled)
		{
			corner.SetActive(true);
			if (canRemove)
			{
				corner.GetComponent<MeshRenderer>().sharedMaterial = this.RemovableCornerMaterial;
			}
			else
			{
				corner.GetComponent<MeshRenderer>().sharedMaterial = this.FilledCornerMaterial;
			}
		}
		else if (canAdd)
		{
			corner.SetActive(true);
			corner.GetComponent<MeshRenderer>().sharedMaterial = this.AddableCornerMaterial;
		}
		else
		{
			corner.SetActive(false);
		}
	}
}
