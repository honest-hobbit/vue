﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[CreateAssetMenu(menuName = $"{nameof(VUE)}/{nameof(WedgeTableAsset)}")]
public class WedgeTableAsset : ScriptableObject
{
	public Mode CreateMode;

	public bool CheckCollisions;

	public bool SaveTable;

	public string TableFilePath;

	public bool ValidateOnGenerate;

	public bool ValidateOnLoad;

	public bool LogData;

	private readonly Lazy<WedgeTable> table;

	public WedgeTableAsset()
	{
		this.table = new Lazy<WedgeTable>(() => this.CreateMode switch
		{
			Mode.AutoGenerateOrLoad => File.Exists(this.GetTableFilePath()) ? this.LoadTable() : this.CreateTable(),
			Mode.Generate => this.CreateTable(),
			Mode.Load => this.LoadTable(),
			_ => throw InvalidEnumArgument.CreateException(nameof(this.CreateMode), this.CreateMode),
		});
	}

	public WedgeTable Table => this.table.Value;

	private string GetTableFilePath() =>
		Path.GetFullPath(Path.Combine(Application.streamingAssetsPath, this.TableFilePath));

	private T TimeMethod<T>(string methodName, Func<T> method)
	{
		Debug.Assert(!methodName.IsNullOrWhiteSpace());
		Debug.Assert(method != null);

		var timer = Stopwatch.StartNew();
		try
		{
			return method();
		}
		finally
		{
			timer.Stop();
			if (this.LogData)
			{
				UnityEngine.Debug.Log($"{methodName} took {timer.ElapsedMilliseconds} ms");
			}
		}
	}

	private WedgeTable LoadTable() => this.TimeMethod(nameof(this.LoadTable), () =>
	{
		var builder = WedgeTableBuilderSerDes.Instance.Deserialize(File.ReadAllBytes(this.GetTableFilePath()));

		builder.GenerateMissingTables();
		if (this.ValidateOnLoad)
		{
			builder.ValidateTablesFull();
		}

		this.Log(builder);

		return builder.BuildWedgeTable();
	});

	private WedgeTable CreateTable() => this.TimeMethod(nameof(this.CreateTable), () =>
	{
		var builder = new WedgeTableBuilder()
		{
			Definition = WedgePrototypes.EnumerateDistinctRotations().ToArray(),
		};

		builder.GenerateMissingTables();

		if (this.CheckCollisions)
		{
			var (collisions, misses) = this.TimeMethod(
				nameof(this.CheckCollisions), () => WedgeCollisionChecker.CheckCollisions(builder));

			if (this.LogData)
			{
				UnityEngine.Debug.Log($"{nameof(this.CheckCollisions)}   Collisions: {collisions}   Misses: {misses}");
			}
		}

		if (this.ValidateOnGenerate)
		{
			builder.ValidateTablesFull();
		}

		this.Log(builder);

		if (this.SaveTable)
		{
			this.TimeMethod(nameof(this.SaveTable), () =>
			{
				var filePath = this.GetTableFilePath();
				Directory.CreateDirectory(Path.GetDirectoryName(filePath));
				File.WriteAllBytes(filePath, WedgeTableBuilderSerDes.Instance.Serialize(builder));
				return filePath;
			});
		}

		return builder.BuildWedgeTable();
	});

	private void Log(WedgeTableBuilder builder)
	{
		Debug.Assert(builder != null);

		if (!this.LogData) { return; }

		var message = new StringBuilder();
		message.AppendLine($"{nameof(builder.WedgeByFilledCornersDuration)}: {builder.WedgeByFilledCornersDuration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.PartsDuration)}: {builder.PartsDuration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.MirrorDuration)}: {builder.MirrorDuration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.Rotate90Duration)}: {builder.Rotate90Duration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.Rotate180Duration)}: {builder.Rotate180Duration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.Rotate270Duration)}: {builder.Rotate270Duration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.CombineWedgesDuration)}: {builder.CombineWedgesDuration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.ValidateDefinitionDuration)}: {builder.ValidateDefinitionDuration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.ValidateTablesQuickDuration)}: {builder.ValidateTablesQuickDuration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.ValidateTablesIntermediateDuration)}: {builder.ValidateTablesIntermediateDuration.TotalMilliseconds}");
		message.AppendLine($"{nameof(builder.ValidateTablesFullDuration)}: {builder.ValidateTablesFullDuration.TotalMilliseconds}");
		UnityEngine.Debug.Log(message.ToString());
	}

	public enum Mode
	{
		AutoGenerateOrLoad,

		Generate,

		Load,
	}
}
