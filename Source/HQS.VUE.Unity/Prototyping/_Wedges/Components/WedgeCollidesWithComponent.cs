﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class WedgeCollidesWithComponent : MonoBehaviour
{
	public WedgeTableAsset TableAsset;

	public Material PassMaterial;

	public Material CollidesMaterial;

	public WedgeSelectorComponent WedgeA;

	public WedgeSelectorComponent WedgeB;

	[ReadOnlyField]
	public bool Collides = false;

	private MeshRenderer meshRenderer;

	private WedgeTable table;

	private void Start()
	{
		this.meshRenderer = this.GetComponent<MeshRenderer>();
		this.table = this.TableAsset.Table;
	}

	private void Update()
	{
		var collides = this.table.GetCollidesWith(this.WedgeA.CurrentWedge, this.WedgeB.CurrentWedge);
		if (this.Collides != collides)
		{
			this.Collides = collides;
			if (collides)
			{
				this.meshRenderer.sharedMaterial = this.CollidesMaterial;
			}
			else
			{
				this.meshRenderer.sharedMaterial = this.PassMaterial;
			}
		}
	}
}
