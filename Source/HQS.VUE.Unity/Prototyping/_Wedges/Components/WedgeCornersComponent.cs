﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class WedgeCornersComponent : MonoBehaviour
{
	public GameObject CornerPrefab;

	public Material FilledCornerMaterial;

	public Material EmptyCornerMaterial;

	public float Spacing = .5f;

	public Renderer CornerNNN;

	public Renderer CornerNNP;

	public Renderer CornerNPN;

	public Renderer CornerNPP;

	public Renderer CornerPNN;

	public Renderer CornerPNP;

	public Renderer CornerPPN;

	public Renderer CornerPPP;

	public void CreateCorners(WedgeCorners corners)
	{
		Ensure.That(this.CornerPrefab, nameof(this.CornerPrefab)).IsNotNull();
		Ensure.That(this.FilledCornerMaterial, nameof(this.FilledCornerMaterial)).IsNotNull();
		Ensure.That(this.EmptyCornerMaterial, nameof(this.EmptyCornerMaterial)).IsNotNull();

		HandleCorner(ref this.CornerNNN, WedgeCorners.NNN, new Vector3(-1, -1, -1));
		HandleCorner(ref this.CornerNNP, WedgeCorners.NNP, new Vector3(-1, -1, 1));
		HandleCorner(ref this.CornerNPN, WedgeCorners.NPN, new Vector3(-1, 1, -1));
		HandleCorner(ref this.CornerNPP, WedgeCorners.NPP, new Vector3(-1, 1, 1));
		HandleCorner(ref this.CornerPNN, WedgeCorners.PNN, new Vector3(1, -1, -1));
		HandleCorner(ref this.CornerPNP, WedgeCorners.PNP, new Vector3(1, -1, 1));
		HandleCorner(ref this.CornerPPN, WedgeCorners.PPN, new Vector3(1, 1, -1));
		HandleCorner(ref this.CornerPPP, WedgeCorners.PPP, new Vector3(1, 1, 1));

		void HandleCorner(ref Renderer gameObject, WedgeCorners corner, Vector3 offset)
		{
			if (gameObject == null)
			{
				gameObject = Instantiate(this.CornerPrefab).GetComponent<Renderer>();
				gameObject.name = corner.ToString();
				gameObject.transform.position = offset * this.Spacing;
				gameObject.transform.SetParent(this.transform, false);
				gameObject.gameObject.SetActive(true);
			}

			gameObject.sharedMaterial = corners.Has(corner) ?
				this.FilledCornerMaterial : this.EmptyCornerMaterial;
		}
	}
}
