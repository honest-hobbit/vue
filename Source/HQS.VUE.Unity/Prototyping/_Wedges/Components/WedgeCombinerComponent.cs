﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[RequireComponent(typeof(WedgeComponent))]
public class WedgeCombinerComponent : MonoBehaviour
{
	public WedgeTableAsset TableAsset;

	public WedgeSelectorComponent WedgeA;

	public WedgeSelectorComponent WedgeB;

	private WedgeComponent wedgeComponent;

	private WedgeTable table;

	private WedgeIndex combinedWedge;

	private void Start()
	{
		this.wedgeComponent = this.GetComponent<WedgeComponent>();
		this.table = this.TableAsset.Table;
	}

	private void Update()
	{
		var combinedWedge = this.table.Combine(this.WedgeA.CurrentWedge, this.WedgeB.CurrentWedge);
		if (combinedWedge != this.combinedWedge)
		{
			this.combinedWedge = combinedWedge;
			this.wedgeComponent.SetWedge(this.table, combinedWedge);
		}
	}
}
