﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[RequireComponent(typeof(WedgeComponent))]
[ExecuteInEditMode]
public class WedgeBuilderComponent : MonoBehaviour
{
	public bool RebuildOnValidate;

	public string FromHexString;

	public QuadParts FaceNegX;

	public QuadParts FacePosX;

	public QuadParts FaceNegY;

	public QuadParts FacePosY;

	public QuadParts FaceNegZ;

	public QuadParts FacePosZ;

	public QuadParts EdgeXNegYNegZ;

	public QuadParts EdgeXNegYPosZ;

	public QuadParts EdgeXPosYNegZ;

	public QuadParts EdgeXPosYPosZ;

	public QuadParts EdgeYNegXNegZ;

	public QuadParts EdgeYNegXPosZ;

	public QuadParts EdgeYPosXNegZ;

	public QuadParts EdgeYPosXPosZ;

	public QuadParts EdgeZNegXNegY;

	public QuadParts EdgeZNegXPosY;

	public QuadParts EdgeZPosXNegY;

	public QuadParts EdgeZPosXPosY;

	public WedgeCorners CornerWideBot;

	public WedgeCorners CornerWideTop;

	public WedgeCorners FilledCorners;

	public void RebuildWedge()
	{
		var parts = !this.FromHexString.IsNullOrWhiteSpace() ?
			WedgeHexEncoding.ToWedgeParts(this.FromHexString) :
			new WedgeDefinition()
			{
				Surfaces = new WedgeSurfaces
				{
					FaceNegX = this.FaceNegX.SelectAll(),
					FacePosX = this.FacePosX.SelectAll(),
					FaceNegY = this.FaceNegY.SelectAll(),
					FacePosY = this.FacePosY.SelectAll(),
					FaceNegZ = this.FaceNegZ.SelectAll(),
					FacePosZ = this.FacePosZ.SelectAll(),

					EdgeXNegYNegZ = this.EdgeXNegYNegZ.SelectAll(),
					EdgeXNegYPosZ = this.EdgeXNegYPosZ.SelectAll(),
					EdgeXPosYNegZ = this.EdgeXPosYNegZ.SelectAll(),
					EdgeXPosYPosZ = this.EdgeXPosYPosZ.SelectAll(),

					EdgeYNegXNegZ = this.EdgeYNegXNegZ.SelectAll(),
					EdgeYNegXPosZ = this.EdgeYNegXPosZ.SelectAll(),
					EdgeYPosXNegZ = this.EdgeYPosXNegZ.SelectAll(),
					EdgeYPosXPosZ = this.EdgeYPosXPosZ.SelectAll(),

					EdgeZNegXNegY = this.EdgeZNegXNegY.SelectAll(),
					EdgeZNegXPosY = this.EdgeZNegXPosY.SelectAll(),
					EdgeZPosXNegY = this.EdgeZPosXNegY.SelectAll(),
					EdgeZPosXPosY = this.EdgeZPosXPosY.SelectAll(),

					CornerWideBot = this.CornerWideBot.SelectAll(),
					CornerWideTop = this.CornerWideTop.SelectAll(),
				},

				FilledCorners = this.FilledCorners.SelectAll(),
			};

		this.FromHexString = null;

		this.FaceNegX = parts.Surfaces.FaceNegX;
		this.FacePosX = parts.Surfaces.FacePosX;
		this.FaceNegY = parts.Surfaces.FaceNegY;
		this.FacePosY = parts.Surfaces.FacePosY;
		this.FaceNegZ = parts.Surfaces.FaceNegZ;
		this.FacePosZ = parts.Surfaces.FacePosZ;

		this.EdgeXNegYNegZ = parts.Surfaces.EdgeXNegYNegZ;
		this.EdgeXNegYPosZ = parts.Surfaces.EdgeXNegYPosZ;
		this.EdgeXPosYNegZ = parts.Surfaces.EdgeXPosYNegZ;
		this.EdgeXPosYPosZ = parts.Surfaces.EdgeXPosYPosZ;

		this.EdgeYNegXNegZ = parts.Surfaces.EdgeYNegXNegZ;
		this.EdgeYNegXPosZ = parts.Surfaces.EdgeYNegXPosZ;
		this.EdgeYPosXNegZ = parts.Surfaces.EdgeYPosXNegZ;
		this.EdgeYPosXPosZ = parts.Surfaces.EdgeYPosXPosZ;

		this.EdgeZNegXNegY = parts.Surfaces.EdgeZNegXNegY;
		this.EdgeZNegXPosY = parts.Surfaces.EdgeZNegXPosY;
		this.EdgeZPosXNegY = parts.Surfaces.EdgeZPosXNegY;
		this.EdgeZPosXPosY = parts.Surfaces.EdgeZPosXPosY;

		this.CornerWideBot = parts.Surfaces.CornerWideBot;
		this.CornerWideTop = parts.Surfaces.CornerWideTop;

		this.FilledCorners = parts.FilledCorners;

		this.gameObject.GetComponent<WedgeComponent>().SetWedge(parts);
	}

	private void OnValidate()
	{
		if (this.RebuildOnValidate)
		{
			this.RebuildWedge();
		}
	}

	private void Start()
	{
		this.RebuildWedge();
	}
}
