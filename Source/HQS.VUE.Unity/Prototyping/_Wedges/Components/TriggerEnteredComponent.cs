﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class TriggerEnteredComponent : MonoBehaviour
{
	public bool CausesTriggerEntered;

	public bool IsTriggerEntered { get; private set; } = false;

	public void ResetTrigger()
	{
		this.IsTriggerEntered = false;
	}

	private void OnTriggerEnter(Collider other)
	{
		var trigger = other.gameObject.GetComponent<TriggerEnteredComponent>();
		if (trigger == null)
		{
			return;
		}

		if (trigger.CausesTriggerEntered)
		{
			this.IsTriggerEntered = true;
		}
	}
}
