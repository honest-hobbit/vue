﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
public class WedgeRotationsComponent : MonoBehaviour
{
	public WedgeTableAsset TableAsset;

	public GameObject WedgePrefab;

	public float Padding = 1f;

	public bool ShowCorners;

	public bool LogData;

	private readonly List<CaseType> caseTypes = new();

	private readonly List<WedgeDefinition> allCases = new();

	private readonly List<WedgeCorners> filledCornerCases = new();

	private void Start()
	{
		this.CreateGameObjects();

		if (this.LogData)
		{
			this.PrintResults();
		}
	}

	private void CreateGameObjects()
	{
		var wedgeType = this.TableAsset.Table.First().Value.Type;
		var category = new GameObject(wedgeType.ToString());
		category.transform.SetParent(this.transform, false);
		var position = Int3.Zero;
		int count = 0;

		foreach (var pair in this.TableAsset.Table)
		{
			var wedge = pair.Value;

			if (wedge.Type != wedgeType)
			{
				this.caseTypes.Add(new CaseType(category.name, count));
				count = 0;

				wedgeType = wedge.Type;
				category = new GameObject(wedgeType.ToString());
				category.transform.SetParent(this.transform, false);

				position.Y = 0;
				position.Z = 0;
				position.X++;
			}

			this.allCases.Add(wedge);
			if (wedge.FilledCorners != WedgeCorners.None)
			{
				this.filledCornerCases.Add(wedge.FilledCorners);
			}

			this.CreateGameObject(pair.Key, position, category.transform);

			count++;
			if (position.Y <= 2)
			{
				position.Y++;
			}
			else
			{
				position.Y = 0;
				position.Z--;
			}
		}
	}

	private void CreateGameObject(WedgeIndex wedge, Int3 position, Transform parent)
	{
		Debug.Assert(parent != null);

		var gameObject = Instantiate(this.WedgePrefab);
		gameObject.name = wedge.ToString();

		var wedgeComponent = gameObject.GetComponent<WedgeComponent>();
		wedgeComponent.ShowCorners = this.ShowCorners;
		wedgeComponent.SetWedge(this.TableAsset.Table, wedge);

		gameObject.transform.position = gameObject.transform.position + (position.ToUnityVector() * this.Padding);
		gameObject.transform.SetParent(parent, false);

		gameObject.SetActive(true);
	}

	private void PrintResults()
	{
		var message = new StringBuilder();

		foreach (var result in this.caseTypes)
		{
			message.AppendLine($"{result.Label}: {result.Count}");
		}

		message.AppendLine($"Case types: {this.caseTypes.Count}");
		message.AppendLine($"Total cases: {this.allCases.Count}");

		int totalDuplicateCases = this.allCases.Count - this.allCases.Distinct().Count();
		if (totalDuplicateCases == 0)
		{
			message.AppendLine("No duplicate cases.");
		}
		else
		{
			message.AppendLine($"WARNING! {totalDuplicateCases} duplicate cases found!");
		}

		int totalDuplicateFilledCornerCases = this.filledCornerCases.Count - this.filledCornerCases.Distinct().Count();
		if (totalDuplicateFilledCornerCases == 0)
		{
			message.AppendLine("No duplicate filled corner cases.");
		}
		else
		{
			message.AppendLine($"WARNING! {totalDuplicateFilledCornerCases} duplicate filled corner cases found!");
		}

		UnityEngine.Debug.Log(message);
	}

	[SuppressMessage("StyleCop", "SA1313", Justification = "Record type.")]
	private readonly record struct CaseType(string Label, int Count);
}
