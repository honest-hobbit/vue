﻿namespace HQS.VUE.Unity.Prototyping;

internal static class QuadPartsConversionExtensions
{
	// this only works because the bit flag layout of QuadParts and QuadEdges is exactly the same
	public static QuadParts ToParts(this QuadEdges value) => (QuadParts)value;

	public static QuadEdges ToEdges(this QuadParts value) => (QuadEdges)value;
}
