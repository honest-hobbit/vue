﻿namespace HQS.VUE.Unity.Prototyping;

internal static class WedgeCollisionChecker
{
	public static (int collisions, int misses) CheckCollisions(WedgeTableBuilder tableBuilder)
	{
		Ensure.That(tableBuilder, nameof(tableBuilder)).IsNotNull();

		var previousScene = SceneManager.GetActiveScene();
		try
		{
			var tempScene = SceneManager.CreateScene(
				"Wedge Collision Scene", new CreateSceneParameters(LocalPhysicsMode.Physics3D));
			SceneManager.SetActiveScene(tempScene);
			var physicsScene = tempScene.GetPhysicsScene();

			var (subjectWedge, subjectTrigger) = CreateWedgeTrigger("Subject Wedge");
			subjectTrigger.CausesTriggerEntered = true;

			var tempTable = tableBuilder.BuildWedgeTable();
			int length = tempTable.Length;
			var testTriggers = new TriggerEnteredComponent[length];

			for (int i = 0; i < length; i++)
			{
				var (testWedge, testTrigger) = CreateWedgeTrigger($"Test Wedge {i}");
				testWedge.SetWedge(tempTable, new WedgeIndex((byte)i));
				testTriggers[i] = testTrigger;
			}

			int collisions = 0;
			int misses = 0;

			for (int a = 0; a < length; a++)
			{
				subjectWedge.SetWedge(tempTable, new WedgeIndex((byte)a));

				physicsScene.Simulate(Time.fixedDeltaTime);
				var collidesWith = Bit256.AllFalse;

				for (int b = 0; b < length; b++)
				{
					if (testTriggers[b].IsTriggerEntered)
					{
						collisions++;
						collidesWith[b] = true;
					}
					else
					{
						misses++;
					}

					testTriggers[b].ResetTrigger();
				}

				tableBuilder.CollidesWith[a] = collidesWith;
			}

			UnityEngine.Object.Destroy(subjectTrigger.gameObject);
			for (int i = 0; i < length; i++)
			{
				UnityEngine.Object.Destroy(testTriggers[i].gameObject);
			}

			return (collisions, misses);
		}
		finally
		{
			SceneManager.SetActiveScene(previousScene);
		}
	}

	private static (WedgeComponent, TriggerEnteredComponent) CreateWedgeTrigger(string name)
	{
		var gameObject = new GameObject(name);

		var rigidbody = gameObject.AddComponent<Rigidbody>();
		rigidbody.useGravity = false;
		rigidbody.isKinematic = true;

		var collider = gameObject.AddComponent<MeshCollider>();
		collider.convex = true;
		collider.isTrigger = true;

		collider = gameObject.AddComponent<MeshCollider>();
		collider.convex = true;
		collider.isTrigger = true;

		var wedge = gameObject.AddComponent<WedgeComponent>();
		wedge.ShowCorners = false;
		wedge.ResizeColliderPercentage = -.1f;

		var trigger = gameObject.AddComponent<TriggerEnteredComponent>();

		gameObject.SetActive(true);
		return (wedge, trigger);
	}
}
