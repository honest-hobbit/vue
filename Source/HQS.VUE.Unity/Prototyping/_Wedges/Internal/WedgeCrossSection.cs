﻿namespace HQS.VUE.Unity.Prototyping;

internal static class WedgeCrossSection
{
	public static void SetUVs(SplitQuadBuilder quad)
	{
		quad.A.UV = new Vector2(0, 1);
		quad.B.UV = new Vector2(1, 1);
		quad.C.UV = new Vector2(1, 0);
		quad.D.UV = new Vector2(0, 0);
		quad.E.UV = new Vector2(.5f, .5f);
	}

	// 6 cases
	public static class Face
	{
		public static void SetNegX(SplitQuadBuilder quad, float length)
		{
			quad.Edges = QuadEdges.All;
			quad.A.Position = new Vector3(-1, 1, 1) * length;
			quad.B.Position = new Vector3(-1, 1, -1) * length;
			quad.C.Position = new Vector3(-1, -1, -1) * length;
			quad.D.Position = new Vector3(-1, -1, 1) * length;
			quad.E.Position = new Vector3(-1, 0, 0) * length;
			quad.SetNormals(new Vector3(-1, 0, 0));
		}

		public static void SetPosX(SplitQuadBuilder quad, float length)
		{
			quad.Edges = QuadEdges.All;
			quad.A.Position = new Vector3(1, 1, -1) * length;
			quad.B.Position = new Vector3(1, 1, 1) * length;
			quad.C.Position = new Vector3(1, -1, 1) * length;
			quad.D.Position = new Vector3(1, -1, -1) * length;
			quad.E.Position = new Vector3(1, 0, 0) * length;
			quad.SetNormals(new Vector3(1, 0, 0));
		}

		public static void SetNegY(SplitQuadBuilder quad, float length)
		{
			quad.Edges = QuadEdges.All;
			quad.A.Position = new Vector3(1, -1, 1) * length;
			quad.B.Position = new Vector3(-1, -1, 1) * length;
			quad.C.Position = new Vector3(-1, -1, -1) * length;
			quad.D.Position = new Vector3(1, -1, -1) * length;
			quad.E.Position = new Vector3(0, -1, 0) * length;
			quad.SetNormals(new Vector3(0, -1, 0));
		}

		public static void SetPosY(SplitQuadBuilder quad, float length)
		{
			quad.Edges = QuadEdges.All;
			quad.A.Position = new Vector3(1, 1, -1) * length;
			quad.B.Position = new Vector3(-1, 1, -1) * length;
			quad.C.Position = new Vector3(-1, 1, 1) * length;
			quad.D.Position = new Vector3(1, 1, 1) * length;
			quad.E.Position = new Vector3(0, 1, 0) * length;
			quad.SetNormals(new Vector3(0, 1, 0));
		}

		public static void SetNegZ(SplitQuadBuilder quad, float length)
		{
			quad.Edges = QuadEdges.All;
			quad.A.Position = new Vector3(-1, 1, -1) * length;
			quad.B.Position = new Vector3(1, 1, -1) * length;
			quad.C.Position = new Vector3(1, -1, -1) * length;
			quad.D.Position = new Vector3(-1, -1, -1) * length;
			quad.E.Position = new Vector3(0, 0, -1) * length;
			quad.SetNormals(new Vector3(0, 0, -1));
		}

		public static void SetPosZ(SplitQuadBuilder quad, float length)
		{
			quad.Edges = QuadEdges.All;
			quad.A.Position = new Vector3(1, 1, 1) * length;
			quad.B.Position = new Vector3(-1, 1, 1) * length;
			quad.C.Position = new Vector3(-1, -1, 1) * length;
			quad.D.Position = new Vector3(1, -1, 1) * length;
			quad.E.Position = new Vector3(0, 0, 1) * length;
			quad.SetNormals(new Vector3(0, 0, 1));
		}
	}

	// 4 cases
	public static class EdgeX
	{
		public static void SetNegYNegZ(SplitQuadBuilder quad, float length)
		{
			Face.SetNegZ(quad, length);
			quad.C.Position.z *= -1;
			quad.D.Position.z *= -1;
			quad.E.Position.z = 0;
			quad.SetNormals(new Vector3(0, -1, -1));
		}

		public static void SetNegYPosZ(SplitQuadBuilder quad, float length)
		{
			Face.SetPosZ(quad, length);
			quad.C.Position.z *= -1;
			quad.D.Position.z *= -1;
			quad.E.Position.z = 0;
			quad.SetNormals(new Vector3(0, -1, 1));
		}

		public static void SetPosYNegZ(SplitQuadBuilder quad, float length)
		{
			Face.SetNegZ(quad, length);
			quad.A.Position.z *= -1;
			quad.B.Position.z *= -1;
			quad.E.Position.z = 0;
			quad.SetNormals(new Vector3(0, 1, -1));
		}

		public static void SetPosYPosZ(SplitQuadBuilder quad, float length)
		{
			Face.SetPosZ(quad, length);
			quad.A.Position.z *= -1;
			quad.B.Position.z *= -1;
			quad.E.Position.z = 0;
			quad.SetNormals(new Vector3(0, 1, 1));
		}
	}

	// 4 cases
	public static class EdgeY
	{
		public static void SetNegXNegZ(SplitQuadBuilder quad, float length)
		{
			Face.SetNegX(quad, length);
			quad.B.Position.x *= -1;
			quad.C.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(-1, 0, -1));
		}

		public static void SetNegXPosZ(SplitQuadBuilder quad, float length)
		{
			Face.SetNegX(quad, length);
			quad.A.Position.x *= -1;
			quad.D.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(-1, 0, 1));
		}

		public static void SetPosXNegZ(SplitQuadBuilder quad, float length)
		{
			Face.SetPosX(quad, length);
			quad.A.Position.x *= -1;
			quad.D.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(1, 0, -1));
		}

		public static void SetPosXPosZ(SplitQuadBuilder quad, float length)
		{
			Face.SetPosX(quad, length);
			quad.B.Position.x *= -1;
			quad.C.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(1, 0, 1));
		}
	}

	// 4 cases
	public static class EdgeZ
	{
		public static void SetNegXNegY(SplitQuadBuilder quad, float length)
		{
			Face.SetNegX(quad, length);
			quad.C.Position.x *= -1;
			quad.D.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(-1, -1, 0));
		}

		public static void SetNegXPosY(SplitQuadBuilder quad, float length)
		{
			Face.SetNegX(quad, length);
			quad.A.Position.x *= -1;
			quad.B.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(-1, 1, 0));
		}

		public static void SetPosXNegY(SplitQuadBuilder quad, float length)
		{
			Face.SetPosX(quad, length);
			quad.C.Position.x *= -1;
			quad.D.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(1, -1, 0));
		}

		public static void SetPosXPosY(SplitQuadBuilder quad, float length)
		{
			Face.SetPosX(quad, length);
			quad.A.Position.x *= -1;
			quad.B.Position.x *= -1;
			quad.E.Position.x = 0;
			quad.SetNormals(new Vector3(1, 1, 0));
		}
	}

	public static class Corner
	{
		public static class WideBot
		{
			// 4 cases
			public static class NegY
			{
				public static void SetNegXNegZ(TriangleBuilder triangle, float length)
				{
					triangle.A.Position = new Vector3(-1, 1, -1) * length;
					triangle.B.Position = new Vector3(1, -1, -1) * length;
					triangle.C.Position = new Vector3(-1, -1, 1) * length;
					triangle.A.UV = new Vector2(.5f, 1);
					triangle.B.UV = new Vector2(1, 0);
					triangle.C.UV = new Vector2(0, 0);
					triangle.SetNormals(new Vector3(-1, -1, -1).normalized);
				}

				public static void SetNegXPosZ(TriangleBuilder triangle, float length)
				{
					triangle.A.Position = new Vector3(-1, 1, 1) * length;
					triangle.B.Position = new Vector3(-1, -1, -1) * length;
					triangle.C.Position = new Vector3(1, -1, 1) * length;
					triangle.A.UV = new Vector2(.5f, 1);
					triangle.B.UV = new Vector2(1, 0);
					triangle.C.UV = new Vector2(0, 0);
					triangle.SetNormals(new Vector3(-1, -1, 1).normalized);
				}

				public static void SetPosXNegZ(TriangleBuilder triangle, float length)
				{
					triangle.A.Position = new Vector3(1, 1, -1) * length;
					triangle.B.Position = new Vector3(1, -1, 1) * length;
					triangle.C.Position = new Vector3(-1, -1, -1) * length;
					triangle.A.UV = new Vector2(.5f, 1);
					triangle.B.UV = new Vector2(1, 0);
					triangle.C.UV = new Vector2(0, 0);
					triangle.SetNormals(new Vector3(1, -1, -1).normalized);
				}

				public static void SetPosXPosZ(TriangleBuilder triangle, float length)
				{
					triangle.A.Position = new Vector3(1, 1, 1) * length;
					triangle.B.Position = new Vector3(-1, -1, 1) * length;
					triangle.C.Position = new Vector3(1, -1, -1) * length;
					triangle.A.UV = new Vector2(.5f, 1);
					triangle.B.UV = new Vector2(1, 0);
					triangle.C.UV = new Vector2(0, 0);
					triangle.SetNormals(new Vector3(1, -1, 1).normalized);
				}
			}

			// 4 cases
			public static class PosY
			{
				public static void SetNegXNegZ(TriangleBuilder triangle, float length)
				{
					NegY.SetNegXNegZ(triangle, length);
					FlipA(triangle);
				}

				public static void SetNegXPosZ(TriangleBuilder triangle, float length)
				{
					NegY.SetNegXPosZ(triangle, length);
					FlipA(triangle);
				}

				public static void SetPosXNegZ(TriangleBuilder triangle, float length)
				{
					NegY.SetPosXNegZ(triangle, length);
					FlipA(triangle);
				}

				public static void SetPosXPosZ(TriangleBuilder triangle, float length)
				{
					NegY.SetPosXPosZ(triangle, length);
					FlipA(triangle);
				}
			}
		}

		public static class WideTop
		{
			// 4 cases
			public static class NegY
			{
				public static void SetNegXNegZ(TriangleBuilder triangle, float length)
				{
					WideBot.PosY.SetNegXNegZ(triangle, length);
					FlipY(triangle);
				}

				public static void SetNegXPosZ(TriangleBuilder triangle, float length)
				{
					WideBot.PosY.SetNegXPosZ(triangle, length);
					FlipY(triangle);
				}

				public static void SetPosXNegZ(TriangleBuilder triangle, float length)
				{
					WideBot.PosY.SetPosXNegZ(triangle, length);
					FlipY(triangle);
				}

				public static void SetPosXPosZ(TriangleBuilder triangle, float length)
				{
					WideBot.PosY.SetPosXPosZ(triangle, length);
					FlipY(triangle);
				}
			}

			// 4 cases
			public static class PosY
			{
				public static void SetNegXNegZ(TriangleBuilder triangle, float length)
				{
					NegY.SetNegXNegZ(triangle, length);
					FlipA(triangle);
				}

				public static void SetNegXPosZ(TriangleBuilder triangle, float length)
				{
					NegY.SetNegXPosZ(triangle, length);
					FlipA(triangle);
				}

				public static void SetPosXNegZ(TriangleBuilder triangle, float length)
				{
					NegY.SetPosXNegZ(triangle, length);
					FlipA(triangle);
				}

				public static void SetPosXPosZ(TriangleBuilder triangle, float length)
				{
					NegY.SetPosXPosZ(triangle, length);
					FlipA(triangle);
				}
			}
		}

		private static void FlipA(TriangleBuilder triangle)
		{
			triangle.A.Position.x *= -1;
			triangle.A.Position.z *= -1;
			triangle.A.Normal.y *= -1;
			triangle.B.Normal.y *= -1;
			triangle.C.Normal.y *= -1;
		}

		private static void FlipY(TriangleBuilder triangle)
		{
			triangle.A.Position.y *= -1;
			triangle.B.Position.y *= -1;
			triangle.C.Position.y *= -1;
			triangle.A.UV.y = -triangle.A.UV.y + 1;
			triangle.B.UV.y = -triangle.B.UV.y + 1;
			triangle.C.UV.y = -triangle.C.UV.y + 1;
			triangle.A.Normal.y *= -1;
			triangle.B.Normal.y *= -1;
			triangle.C.Normal.y *= -1;
			var temp = triangle.B;
			triangle.B = triangle.C;
			triangle.C = temp;
		}
	}
}
