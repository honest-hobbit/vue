﻿namespace HQS.VUE.Unity.Prototyping;

internal class WedgeBuilder
{
	private readonly MeshBuilder builder;

	private readonly SplitQuadBuilder quad;

	private readonly TriangleBuilder triangle;

	public WedgeBuilder()
	{
		var parts = OptionalMeshParts.Normals.Add(OptionalMeshParts.UV);
		this.builder = new MeshBuilder(parts);
		this.quad = new SplitQuadBuilder() { Parts = parts };
		this.triangle = new TriangleBuilder() { Parts = parts };
		WedgeCrossSection.SetUVs(this.quad);
	}

	public float HalfLength { get; set; } = .5f;

	public Mesh BuildMesh(WedgeSurfaces surfaces, float resizePercentage = 0)
	{
		this.builder.Clear();

		this.HandleFace(surfaces.FaceNegX, WedgeCrossSection.Face.SetNegX);
		this.HandleFace(surfaces.FacePosX, WedgeCrossSection.Face.SetPosX);
		this.HandleFace(surfaces.FaceNegY, WedgeCrossSection.Face.SetNegY);
		this.HandleFace(surfaces.FacePosY, WedgeCrossSection.Face.SetPosY);
		this.HandleFace(surfaces.FaceNegZ, WedgeCrossSection.Face.SetNegZ);
		this.HandleFace(surfaces.FacePosZ, WedgeCrossSection.Face.SetPosZ);

		this.HandleFace(surfaces.EdgeXNegYNegZ, WedgeCrossSection.EdgeX.SetNegYNegZ);
		this.HandleFace(surfaces.EdgeXNegYPosZ, WedgeCrossSection.EdgeX.SetNegYPosZ);
		this.HandleFace(surfaces.EdgeXPosYNegZ, WedgeCrossSection.EdgeX.SetPosYNegZ);
		this.HandleFace(surfaces.EdgeXPosYPosZ, WedgeCrossSection.EdgeX.SetPosYPosZ);

		this.HandleFace(surfaces.EdgeYNegXNegZ, WedgeCrossSection.EdgeY.SetNegXNegZ);
		this.HandleFace(surfaces.EdgeYNegXPosZ, WedgeCrossSection.EdgeY.SetNegXPosZ);
		this.HandleFace(surfaces.EdgeYPosXNegZ, WedgeCrossSection.EdgeY.SetPosXNegZ);
		this.HandleFace(surfaces.EdgeYPosXPosZ, WedgeCrossSection.EdgeY.SetPosXPosZ);

		this.HandleFace(surfaces.EdgeZNegXNegY, WedgeCrossSection.EdgeZ.SetNegXNegY);
		this.HandleFace(surfaces.EdgeZNegXPosY, WedgeCrossSection.EdgeZ.SetNegXPosY);
		this.HandleFace(surfaces.EdgeZPosXNegY, WedgeCrossSection.EdgeZ.SetPosXNegY);
		this.HandleFace(surfaces.EdgeZPosXPosY, WedgeCrossSection.EdgeZ.SetPosXPosY);

		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.NNN), WedgeCrossSection.Corner.WideBot.NegY.SetNegXNegZ);
		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.NNP), WedgeCrossSection.Corner.WideBot.NegY.SetNegXPosZ);
		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.PNN), WedgeCrossSection.Corner.WideBot.NegY.SetPosXNegZ);
		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.PNP), WedgeCrossSection.Corner.WideBot.NegY.SetPosXPosZ);

		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.NPN), WedgeCrossSection.Corner.WideBot.PosY.SetNegXNegZ);
		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.NPP), WedgeCrossSection.Corner.WideBot.PosY.SetNegXPosZ);
		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.PPN), WedgeCrossSection.Corner.WideBot.PosY.SetPosXNegZ);
		this.HandleCorner(surfaces.CornerWideBot.Has(WedgeCorners.PPP), WedgeCrossSection.Corner.WideBot.PosY.SetPosXPosZ);

		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.NNN), WedgeCrossSection.Corner.WideTop.NegY.SetNegXNegZ);
		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.NNP), WedgeCrossSection.Corner.WideTop.NegY.SetNegXPosZ);
		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.PNN), WedgeCrossSection.Corner.WideTop.NegY.SetPosXNegZ);
		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.PNP), WedgeCrossSection.Corner.WideTop.NegY.SetPosXPosZ);

		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.NPN), WedgeCrossSection.Corner.WideTop.PosY.SetNegXNegZ);
		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.NPP), WedgeCrossSection.Corner.WideTop.PosY.SetNegXPosZ);
		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.PPN), WedgeCrossSection.Corner.WideTop.PosY.SetPosXNegZ);
		this.HandleCorner(surfaces.CornerWideTop.Has(WedgeCorners.PPP), WedgeCrossSection.Corner.WideTop.PosY.SetPosXPosZ);

		this.builder.Builder.Resize(resizePercentage);

		return this.builder.BuildMesh();
	}

	private void HandleFace(QuadParts face, Action<SplitQuadBuilder, float> setBuilder)
	{
		Debug.Assert(setBuilder != null);

		if (face == QuadParts.None) { return; }

		setBuilder(this.quad, this.HalfLength);
		this.quad.Edges = face.ToEdges();
		this.builder.Add(this.quad);
	}

	private void HandleCorner(bool hasCorner, Action<TriangleBuilder, float> setBuilder)
	{
		Debug.Assert(setBuilder != null);

		if (!hasCorner) { return; }

		setBuilder(this.triangle, this.HalfLength);
		this.builder.Add(this.triangle);
	}
}
