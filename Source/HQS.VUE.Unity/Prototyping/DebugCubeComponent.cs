﻿namespace HQS.VUE.Unity.Prototyping;

[SuppressMessage("StyleCop", "SA1401", Justification = "Unity requires it.")]
[ExecuteInEditMode]
public class DebugCubeComponent : MonoBehaviour
{
	public Texture2D[] Textures;

	public float Length = 1f;

	public float Height = 1f;

	public float Width = 1f;

	public bool AssignUvs = true;

	protected virtual void Start()
	{
		////Vector3 p0 = new Vector3(-length * .5f, -width * .5f, height * .5f);
		////Vector3 p1 = new Vector3(length * .5f, -width * .5f, height * .5f);
		////Vector3 p2 = new Vector3(length * .5f, -width * .5f, -height * .5f);
		////Vector3 p3 = new Vector3(-length * .5f, -width * .5f, -height * .5f);

		////Vector3 p4 = new Vector3(-length * .5f, width * .5f, height * .5f);
		////Vector3 p5 = new Vector3(length * .5f, width * .5f, height * .5f);
		////Vector3 p6 = new Vector3(length * .5f, width * .5f, -height * .5f);
		////Vector3 p7 = new Vector3(-length * .5f, width * .5f, -height * .5f);

		Vector3 p0 = new Vector3(0, 0, this.Width);
		Vector3 p1 = new Vector3(this.Length, 0, this.Width);
		Vector3 p2 = new Vector3(this.Length, 0, 0);
		Vector3 p3 = new Vector3(0, 0, 0);

		Vector3 p4 = new Vector3(0, this.Height, this.Width);
		Vector3 p5 = new Vector3(this.Length, this.Height, this.Width);
		Vector3 p6 = new Vector3(this.Length, this.Height, 0);
		Vector3 p7 = new Vector3(0, this.Height, 0);

		Vector3[] vertices = new Vector3[]
		{
			// Bottom
			p0, p1, p2, p3,

			// Left
			p7, p4, p0, p3,

			// Front
			p4, p5, p1, p0,

			// Back
			p6, p7, p3, p2,

			// Right
			p5, p6, p2, p1,

			// Top
			p7, p6, p5, p4,
		};

		Vector3 up = Vector3.up;
		Vector3 down = Vector3.down;
		Vector3 front = Vector3.forward;
		Vector3 back = Vector3.back;
		Vector3 left = Vector3.left;
		Vector3 right = Vector3.right;

		Vector3[] normales = new Vector3[]
		{
			// Bottom
			down, down, down, down,

			// Left
			left, left, left, left,

			// Front
			front, front, front, front,

			// Back
			back, back, back, back,

			// Right
			right, right, right, right,

			// Top
			up, up, up, up,
		};

		Vector2 uv00 = new Vector2(0f, 0f);
		Vector2 uv10 = new Vector2(1f, 0f);
		Vector2 uv01 = new Vector2(0f, 1f);
		Vector2 uv11 = new Vector2(1f, 1f);

		Vector2[] uvs = new Vector2[]
		{
			// Bottom
			uv00, uv10, uv11, uv01,

			// Left
			uv11, uv01, uv00, uv10,

			// Front
			uv11, uv01, uv00, uv10,

			// Back
			uv11, uv01, uv00, uv10,

			// Right
			uv11, uv01, uv00, uv10,

			// Top
			uv00, uv10, uv11, uv01,
		};

		int[] triangles = new int[]
		{
			// Bottom
			3, 1, 0,
			3, 2, 1,

			// Left
			3 + (4 * 1), 1 + (4 * 1), 0 + (4 * 1),
			3 + (4 * 1), 2 + (4 * 1), 1 + (4 * 1),

			// Front
			3 + (4 * 2), 1 + (4 * 2), 0 + (4 * 2),
			3 + (4 * 2), 2 + (4 * 2), 1 + (4 * 2),

			// Back
			3 + (4 * 3), 1 + (4 * 3), 0 + (4 * 3),
			3 + (4 * 3), 2 + (4 * 3), 1 + (4 * 3),

			// Right
			3 + (4 * 4), 1 + (4 * 4), 0 + (4 * 4),
			3 + (4 * 4), 2 + (4 * 4), 1 + (4 * 4),

			// Top
			3 + (4 * 5), 1 + (4 * 5), 0 + (4 * 5),
			3 + (4 * 5), 2 + (4 * 5), 1 + (4 * 5),
		};

		var mesh = this.gameObject.GetOrAddComponent<MeshFilter>().GetOrAddSharedMesh();

		mesh.vertices = vertices;
		mesh.normals = normales;
		mesh.uv = this.AssignUvs ? uvs : Factory.CreateArray(vertices.Length, new Vector2(0, 0));
		mesh.uv2 = Factory.CreateArray(vertices.Length, new Vector2(0, 0));
		mesh.triangles = triangles;

		mesh.RecalculateBounds();

		this.GetComponent<Renderer>().sharedMaterial.SetTexture(
			"_MainTex", Texture2DArrayUtility.CreateTextureArray(this.Textures));
	}
}
