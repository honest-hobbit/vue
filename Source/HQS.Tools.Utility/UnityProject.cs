﻿namespace HQS.Tools.Utility;

public static class UnityProject
{
	public static class UnityProvidedAssemblies
	{
		public static string UnityEngineDirectory => @"Editor\Data\Managed\UnityEngine\";

		public static string NetStandardDirectory => @"Editor\Data\NetStandard\";
	}

	public static string ProjectDirectory => @"Unity\VUE Unity Development\";

	public static string AssetsDirectory { get; } = Path.Combine(ProjectDirectory, @"Assets\");

	public static string BuiltPluginsDirectory { get; } = Path.Combine(AssetsDirectory, @"Plugins\Build\");

	public static string LibraryDirectory { get; } = Path.Combine(ProjectDirectory, @"Library\");

	public static string ScriptAssembliesDirectory { get; } = Path.Combine(LibraryDirectory, @"ScriptAssemblies\");

	public static string ProjectFileName => @"Assembly-CSharp.csproj";

	public static string ProjectFilePath { get; } = Path.Combine(ProjectDirectory, ProjectFileName);

	public static IEnumerable<string> GetAssemblyReferences(string repositoryDirectory)
	{
		EnsureArg.IsNotNullOrWhiteSpace(repositoryDirectory, nameof(repositoryDirectory));

		var projectRootElement = ProjectRootElement.Open(Path.Combine(repositoryDirectory, ProjectFilePath));
		var unityProjectDirectory = Path.Combine(repositoryDirectory, ProjectDirectory);

		foreach (var item in projectRootElement.Items)
		{
			if (item.ElementName.Equals("Reference") &&
				item.FirstChild is ProjectMetadataElement metadata)
			{
				var hintPath = metadata.Value;

				if (Path.IsPathFullyQualified(hintPath))
				{
					yield return hintPath;
				}
				else
				{
					yield return Path.Combine(unityProjectDirectory, hintPath);
				}
			}
		}
	}
}
