﻿using System;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;

namespace HQS.CodeAnalyzers
{
	[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(NamespaceCodeFixProvider)), Shared]
	public class NamespaceCodeFixProvider : CodeFixProvider
	{
		private const string title = "Fix namespace";

		public sealed override ImmutableArray<string> FixableDiagnosticIds
		{
			get { return ImmutableArray.Create(NamespaceAnalyzer.Rule1000.Id); }
		}

		public sealed override FixAllProvider GetFixAllProvider()
		{
			// See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/FixAllProvider.md for more information on Fix All Providers
			return WellKnownFixAllProviders.BatchFixer;
		}

		public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
		{
			var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

			// TODO: Replace the following code with your own analysis, generating a CodeAction for each fix to suggest
			var diagnostic = context.Diagnostics.First();
			var diagnosticSpan = diagnostic.Location.SourceSpan;

			// Find the type declaration identified by the diagnostic.
			var declaration = root.FindToken(diagnosticSpan.Start).Parent.AncestorsAndSelf().OfType<NamespaceDeclarationSyntax>().First();

			// Register a code action that will invoke the fix.
			context.RegisterCodeFix(
				CodeAction.Create(
					title: title,
					createChangedDocument: c => FixNamespaceAsync(context.Document, declaration, c),
					equivalenceKey: title),
				diagnostic);
		}

		private async Task<Document> FixNamespaceAsync(
			Document document, NamespaceDeclarationSyntax namespaceDeclaration, CancellationToken cancellationToken)
		{
			var filePath = document.FilePath;
			if (filePath == null)
			{
				return document;
			}

			bool expectedNamespaceFound = NamespaceFinder.TryGetNamespaceFromPath(filePath, out var expectedNamespace);
			if (!expectedNamespaceFound || namespaceDeclaration.Name.ToString().Equals(expectedNamespace, StringComparison.Ordinal))
			{
				return document;
			}

			var newNamespace = namespaceDeclaration.ReplaceNode(namespaceDeclaration.Name, SyntaxFactory.IdentifierName(expectedNamespace));
			var formattedNamespace = newNamespace.WithAdditionalAnnotations(Formatter.Annotation);

			var oldRoot = await document.GetSyntaxRootAsync(cancellationToken);
			var newRoot = oldRoot.ReplaceNode(namespaceDeclaration, formattedNamespace);

			return document.WithSyntaxRoot(newRoot);
		}
	}
}
