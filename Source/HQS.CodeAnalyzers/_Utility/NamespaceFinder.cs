﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace HQS.CodeAnalyzers
{
	/// <summary>
	/// Provides utilities for following our namespace conventions.
	/// </summary>
	public static class NamespaceFinder
	{
		/// <summary>
		/// Get the expected namespace for a file given the path to that file.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="foundNamespace">The namespace.</param>
		/// <returns>True if the namespace could be determined; otherwise false.</returns>
		/// <exception cref="System.ArgumentException">Can't be null or white space.</exception>
		public static bool TryGetNamespaceFromPath(string path, out string foundNamespace)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				throw new ArgumentException("Can't be null or white space.", nameof(path));
			}

			try
			{
				path = Path.GetDirectoryName(path);

				// find the repository root directory
				int sourceIndex = path.IndexOf(RepositoryConstants.SourceFolder, StringComparison.OrdinalIgnoreCase);
				if (sourceIndex == -1)
				{
					foundNamespace = null;
					return false;
				}

				// remove everything up to and including the 'Source' folder
				path = path.Substring(sourceIndex + RepositoryConstants.SourceFolder.Length);

				// check that the same repository root directory is not found again
				// (nesting the same folder structure makes it difficult to determine the real repository root)
				if (path.IndexOf(RepositoryConstants.SourceFolder, StringComparison.OrdinalIgnoreCase) != -1)
				{
					foundNamespace = null;
					return false;
				}

				// split into separate strings for each folder
				// (Path.DirectorySeparatorChar not available in portable library)
				var folders = path.Split('\\', '/');
				foundNamespace = string.Empty;
				foreach (var folder in folders)
				{
					var nextNamespace = folder;
					if (nextNamespace.StartsWith("_"))
					{
						return true;
					}
					else
					{
						int openIndex = nextNamespace.IndexOf('(');
						if (openIndex != -1)
						{
							int closeIndex = nextNamespace.IndexOf(')');
							if (closeIndex <= openIndex)
							{
								foundNamespace = null;
								return false;
							}

							nextNamespace = nextNamespace.Remove(openIndex, closeIndex - openIndex + 1);
						}

						if (foundNamespace == string.Empty)
						{
							foundNamespace = nextNamespace;
						}
						else
						{
							foundNamespace = foundNamespace + "." + nextNamespace;
						}
					}
				}

				return true;
			}
			catch (Exception)
			{
				foundNamespace = null;
				return false;
			}
		}

		/// <summary>
		/// Get the expected namespace for a file by automatically parsing the path to the calling source file.
		/// </summary>
		/// <param name="foundNamespace">The namespace.</param>
		/// <param name="path">
		/// The path of the calling source file. Do not provide this parameter. Let the default be used.
		/// </param>
		/// <returns>True if the namespace could be determined; otherwise false.</returns>
		/// <exception cref="System.ArgumentException">Can't be null or white space.</exception>
		public static bool TryGetNamespaceFromCaller(
			out string foundNamespace, [CallerFilePath] string path = null) =>
			TryGetNamespaceFromPath(path, out foundNamespace);
	}
}
