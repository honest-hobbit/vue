﻿namespace HQS.CodeAnalyzers
{
	/// <summary>
	/// Provides constants for working with our repository setup.
	/// </summary>
	/// <remarks>
	/// This will require updating if the repository/project structure changes.
	/// </remarks>
	public static class RepositoryConstants
	{
		/// <summary>
		/// The partial path of the repository's source folder ("\Source\").
		/// </summary>
		/// <remarks>
		/// This will require updating if the repository/project structure changes.
		/// </remarks>
		public const string SourceFolder = @"\Source\";
	}
}
