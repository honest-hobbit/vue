using System.Linq;
using EnsureThat;
using HQS.Utility.Numerics;
using HQS.Utility.Unity;
using HQS.VUE.Neo;
using UnityEngine;

[ExecuteInEditMode]
public class VectorTransformerComponent : MonoBehaviour
{
	public int VoxelChunkSizeExponent = 1;

	public PlaneNormal Normal;

	public int Depth;

	public GameObject VertexPrefab;

	public GameObject Vertices;

	private void OnValidate()
	{
		this.VoxelChunkSizeExponent = this.VoxelChunkSizeExponent.Clamp(
			ChunkSize.MinExponent, ChunkSize.MaxExponent);

		SetVerticesPositions();
	}

	private void Start()
	{
		Ensure.That(this.VertexPrefab, nameof(this.VertexPrefab)).IsNotNull();
		Ensure.That(this.Vertices, nameof(this.Vertices)).IsNotNull();

		if (!this.AreVerticesInstantiated(out var vertices))
		{
			foreach (var child in vertices)
			{
				DestroyImmediate(child.gameObject);
			}
		}

		int expectedVertices = this.GetExpectedNumberOfVertices();
		for (int count = 0; count < expectedVertices; count++)
		{
			var vector = Instantiate(this.VertexPrefab, this.Vertices.transform);
			vector.transform.localScale /= this.VoxelChunkSizeExponent;
		}

		SetVerticesPositions();
	}

	private int GetExpectedNumberOfVertices()
	{
		int expectedLength = new ChunkSize(this.VoxelChunkSizeExponent).AsSquare.SideLength;
		return MathUtility.IntegerPower(expectedLength + 1, 2)
			+ MathUtility.IntegerPower(expectedLength, 2);
	}

	private bool AreVerticesInstantiated(out Transform[] vertices)
	{
		vertices = this.Vertices.GetComponentsInChildren<Transform>(true)
			.Where(x => x.gameObject != this.Vertices).ToArray();
		return vertices.Length == this.GetExpectedNumberOfVertices();
	}

	private void SetVerticesPositions()
	{
		if (!this.AreVerticesInstantiated(out var vertices))
		{
			return;
		}

		var size = new ChunkSize(this.VoxelChunkSizeExponent);
		var table = new PlaneTo3DTransformer()
		{
			Size = size,
			Depth = this.Depth,
			Normal = this.Normal,
		};

		int index = 0;
		int sideLength = size.AsSquare.SideLength * 2;

		// position vertices at the corners between voxels
		for (int iB = 0; iB <= sideLength; iB += 2)
		{
			for (int iA = 0; iA <= sideLength; iA += 2)
			{
				var vector = vertices[index];
				vector.localPosition = table.Transform(iA, iB).ToUnityVector() / sideLength;
				vector.name = $"{iA}, {iB}";
				SetActive(vector);
				index++;
			}
		}

		// if a corner plane, disable all remaining vertices and exit
		if (table.Normal.IsCorner())
		{
			for (; index < vertices.Length; index++)
			{
				vertices[index].gameObject.SetActive(false);
			}

			return;
		}

		// position vertices at the centers of voxels
		for (int iB = 1; iB < sideLength; iB += 2)
		{
			for (int iA = 1; iA < sideLength; iA += 2)
			{
				var vector = vertices[index];
				vector.localPosition = table.Transform(iA, iB).ToUnityVector() / sideLength;
				vector.name = $"{iA}, {iB}";
				SetActive(vector);
				index++;
			}
		}

		void SetActive(Transform vector) => vector.gameObject.SetActive(
			vector.localPosition.x.IsIn(-.001f, 1.001f) &&
			vector.localPosition.y.IsIn(-.001f, 1.001f) &&
			vector.localPosition.z.IsIn(-.001f, 1.001f));
	}
}
