﻿using UnityEngine;

public class RaycastComponent : MonoBehaviour
{
	public new Camera camera = null;

	public KeyCode keyPress;

	private void Update()
	{
		if (Input.GetKeyDown(this.keyPress))
		{
			var ray = this.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
			if (Physics.Raycast(ray, out var hitInfo, 1000))
			{
				Debug.Log(hitInfo.transform.gameObject.name);
			}
		}
	}
}
