using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class CopyObjectsComponent : MonoBehaviour
{
	private List<GameObject> objects;

	public GameObject prefab;

	public int repeat;

	public float spacing;

	public KeyCode toggle;

	// Start is called before the first frame update
	private void Start()
	{
		this.objects = new List<GameObject>(this.repeat * this.repeat);

		var timer = new Stopwatch();
		timer.Restart();

		for (int iX = 0; iX < this.repeat; iX++)
		{
			for (int iZ = 0; iZ < this.repeat; iZ++)
			{
				var gameObject = GameObject.Instantiate(this.prefab);
				gameObject.SetActive(false);
				gameObject.transform.position = new Vector3(iX * this.spacing, 0, iZ * this.spacing);
				gameObject.transform.SetParent(this.transform, false);
				this.objects.Add(gameObject);
			}
		}

		timer.Stop();
		Debug.Log($"Create {this.prefab.name}: {timer.ElapsedMilliseconds} ms");
	}

	private void Update()
	{
		if (Input.GetKeyDown(this.toggle))
		{
			var timer = new Stopwatch();
			timer.Restart();

			foreach (var gameObject in this.objects)
			{
				gameObject.SetActive(!gameObject.activeSelf);
			}

			Debug.Log($"Toggle {this.prefab.name} {timer.ElapsedMilliseconds} ms");
		}
	}
}
