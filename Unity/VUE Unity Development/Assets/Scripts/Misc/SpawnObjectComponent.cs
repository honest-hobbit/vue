﻿using UnityEngine;

public class SpawnObjectComponent : MonoBehaviour
{
	public new Camera camera = null;

	public GameObject spawnPrefab = null;

	public KeyCode spawnKey = KeyCode.Return;

	public float spawnOffset = 0;

	public void Update()
	{
		if (this.spawnPrefab != null && Input.GetKeyDown(this.spawnKey))
		{
			var ray = this.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
			Instantiate(this.spawnPrefab, ray.GetPoint(this.spawnOffset), Quaternion.identity);
		}
	}
}
