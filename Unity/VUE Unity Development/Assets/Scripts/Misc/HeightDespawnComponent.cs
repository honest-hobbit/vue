﻿using UnityEngine;

public class HeightDespawnComponent : MonoBehaviour
{
	public float height = 0;

	public void Update()
	{
		if (this.transform.position.y < this.height)
		{
			Destroy(this.gameObject);
		}
	}
}
