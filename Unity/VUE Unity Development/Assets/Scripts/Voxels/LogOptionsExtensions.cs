﻿public static class LogOptionsExtensions
{
	public static bool Has(this LogOptions current, LogOptions flag) => (current & flag) == flag;
}
