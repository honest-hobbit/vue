﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using HQS.Utility.Numerics;
using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility;
using HQS.VUE.OLD.Utility.Jobs;
using HQS.VUE.Unity.OLD;
using UnityEngine;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class RegeneratingHillsComponent : MonoBehaviour
{
	[SerializeField]
	private InitializeVoxelUniverseComponent universeComponent = null;

	[SerializeField]
	private SingleVolumeComponent volumeComponent = null;

	[SerializeField]
	[Range(0, 15)]
	private int sizeExponent = 0;

	[SerializeField]
	private int startingSeed = 0;

	[SerializeField]
	private int bandsCount = 0;

	[SerializeField]
	private float delayBetweenJobs = 1;

	[SerializeField]
	private float noiseFrequency = 0;

	[SerializeField]
	private bool checkCollision = false;

	[SerializeField]
	private ushort voxelGroundFirst = 1;

	[SerializeField]
	private ushort voxelGroundSecond = 1;

	[SerializeField]
	private ushort voxelGroundDots = 1;

	public UnityVoxelUniverseOLD Universe { get; private set; }

	public IVoxelJobProcessorOLD JobQueue { get; private set; }

	public Volume Volume { get; private set; }

	public void OnValidate()
	{
		this.bandsCount.ClampLower(0);
		if (this.delayBetweenJobs <= 0)
		{
			this.delayBetweenJobs = 1;
		}
	}

	public void Start()
	{
		if (this.universeComponent == null)
		{
			throw new ArgumentNullException(nameof(this.universeComponent));
		}

		this.Universe = this.universeComponent.GetComponent<VoxelUniverseComponent>().Universe;
		this.JobQueue = this.universeComponent.Universe.Jobs;
		this.Volume = this.volumeComponent.Volume;

		var map = new ChangeMap(this.sizeExponent, this.startingSeed, this.bandsCount, this.noiseFrequency);
		this.StartCoroutine(this.RegenerateHills(map));
	}

	private IEnumerator RegenerateHills(ChangeMap map)
	{
		Debug.Assert(map != null);

		var delay = new WaitForSeconds(this.delayBetweenJobs);
		var job = new GenerateHillsJob()
		{
			Volume = this.Volume,
			Shape = map.EnumerateBounds(),
			ClearAboveGround = true,
			TimingMode = VoxelChangeTiming.Consistent,
			CheckCollision = this.checkCollision,
			GroundFirst = new Voxel(this.voxelGroundFirst),
			GroundSecond = new Voxel(this.voxelGroundSecond),
			GroundDots = new Voxel(this.voxelGroundDots),
		};

		var task = this.JobQueue.SubmitAsync(job);
		yield return delay;
		while (!task.IsCompleted)
		{
			yield return null;
		}

		job.Shape = map;
		while (true)
		{
			map.Next();
			job.Seed = map.Seed;

			task = this.JobQueue.SubmitAsync(job);
			yield return delay;
			while (!task.IsCompleted)
			{
				yield return null;
			}
		}
	}

	private class ChangeMap : IEnumerable<Int2>
	{
		private readonly FastNoiseLite noise = new FastNoiseLite();

		private readonly int bandsCount;

		private SquareArray<int> map;

		private int band = int.MaxValue;

		public ChangeMap(int exponent, int startingSeed, int bandsCount, float frequency)
		{
			Debug.Assert(exponent.IsIn(0, 15), $"Max of 15 is dictated by {nameof(int.MaxValue)}");
			Debug.Assert(bandsCount >= 0);

			this.map = new SquareArray<int>(new SquareArrayIndexer(exponent));
			this.Seed = startingSeed;
			this.bandsCount = bandsCount;
			this.noise.SetFrequency(frequency);
			this.noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
		}

		public SquareArrayIndexer Indexer => this.map.Indexer;

		public int Seed { get; private set; }

		public void Next()
		{
			if (this.band < this.bandsCount)
			{
				this.band++;
				return;
			}

			this.band = -this.bandsCount;
			this.Seed++;
			this.noise.SetSeed(this.Seed);

			int sideLength = this.map.Indexer.SideLength;
			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iY = 0; iY < sideLength; iY++)
				{
					this.map[iX, iY] = (int)Math.Round(this.noise.GetNoise(iX, iY) * this.bandsCount);
				}
			}
		}

		public IEnumerable<Int2> EnumerateBounds()
		{
			int sideLength = this.map.Indexer.SideLength;
			for (int iX = 0; iX < sideLength; iX++)
			{
				for (int iY = 0; iY < sideLength; iY++)
				{
					yield return new Int2(iX, iY);
				}
			}
		}

		/// <inheritdoc />
		public IEnumerator<Int2> GetEnumerator()
		{
			var array = this.map.Array;
			int length = array.Length;
			for (int i = 0; i < length; i++)
			{
				if (array[i] == this.band)
				{
					yield return this.map.Indexer[i];
				}
			}
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
