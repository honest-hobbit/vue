﻿using System;

[Flags]
public enum LogOptions
{
	None = 0,

	CountOfPools = 1,

	ChunkPools = 1 << 1,

	VolumePools = 1 << 2,

	VoxelJobPools = 1 << 3,

	ContouringPools = 1 << 4,

	PersistencePools = 1 << 5,

	ResyncPools = 1 << 6,

	GameObjectPools = 1 << 7,

	VoxelChunkCache = 1 << 8,

	ContourChunkCache = 1 << 9,
}
