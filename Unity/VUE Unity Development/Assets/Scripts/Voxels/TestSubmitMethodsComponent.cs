﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using HQS.Utility.Concurrency;
using HQS.Utility.Resources;
using HQS.VUE.OLD;
using UnityEngine;

[SuppressMessage("Style", "IDE0063", Justification = "Can't use new using statement in Unity yet.")]
[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class TestSubmitMethodsComponent : MonoBehaviour
{
	[SerializeField]
	private SingleVolumeComponent volumeComponent = null;

	private Volume volume;

	private IVoxelJobProcessorOLD processor;

	private IPinPool<TestVoxelJob> normalJobs;

	private IPinPool<TestVoxelJobComplete> completableJobs;

	protected async void Start()
	{
		this.volume = this.volumeComponent.Volume;
		this.processor = this.volumeComponent.Universe.Jobs;
		this.normalJobs = Pool.ThreadSafe.Pins.Create<TestVoxelJob>();
		this.completableJobs = Pool.ThreadSafe.Pins.Create<TestVoxelJobComplete>();
		await this.TestSubmitMethodsAsync();
	}

	private async Task TestSubmitMethodsAsync()
	{
		var delay = TimeSpan.FromSeconds(.5);

		this.Sync1();
		await Task.Delay(delay).MarshallContext();
		this.SyncPooled2();
		await Task.Delay(delay).MarshallContext();
		this.SyncPooled3();
		await Task.Delay(delay).MarshallContext();

		await this.Async4().MarshallContext();
		await this.AsyncPooled5().MarshallContext();
		await this.AsyncPooled6().MarshallContext();

		this.CompletableSync7();
		await Task.Delay(delay).MarshallContext();
		this.CompletableSyncPooled8();
		await Task.Delay(delay).MarshallContext();
		this.CompletableSyncPooled9();
		await Task.Delay(delay).MarshallContext();

		await this.CompletableAsync10().MarshallContext();
		await this.CompletableAsyncPooled11().MarshallContext();
		await this.CompletableAsyncPooled12().MarshallContext();
	}

	private void Sync1()
	{
		var job = new TestVoxelJob
		{
			Volume = this.volume,
			ID = "1 (Sync)"
		};

		this.processor.Submit(job);
	}

	private void SyncPooled2()
	{
		using (var job = this.normalJobs.Rent())
		{
			job.Value.Volume = this.volume;
			job.Value.ID = "2 (Sync, PooledView)";

			this.processor.Submit(job.AsPinned);
		}
	}

	private void SyncPooled3()
	{
		var job = this.normalJobs.Rent();
		job.Value.Volume = this.volume;
		job.Value.ID = "3 (Sync, Pooled)";

		this.processor.Submit(job);
	}

	private async Task Async4()
	{
		var job = new TestVoxelJob
		{
			Volume = this.volume,
			ID = "4 (Async)"
		};

		var result = await this.processor.SubmitAsync(job).MarshallContext();
		this.CheckEquals(job, result.Job, job.ID);
	}

	private async Task AsyncPooled5()
	{
		using (var job = this.normalJobs.Rent())
		{
			job.Value.Volume = this.volume;
			job.Value.ID = "5 (Async, PooledView)";

			var result = await this.processor.SubmitAsync(job.AsPinned).MarshallContext();
			using (result.Job)
			{
				this.CheckEquals(job, result.Job, job.Value.ID);
			}
		}
	}

	private async Task AsyncPooled6()
	{
		using (var job = this.normalJobs.Rent())
		{
			job.Value.Volume = this.volume;
			job.Value.ID = "6 (Async, Pooled)";

			var result = await this.processor.SubmitAsync(job.CreatePin()).MarshallContext();
			using (result.Job)
			{
				this.CheckEquals(job, result.Job, job.Value.ID);
			}
		}
	}

	private void CompletableSync7()
	{
		var job = new TestVoxelJobComplete
		{
			Volume = this.volume,
			ID = "7 (Sync)"
		};

		this.processor.Submit(job);
	}

	private void CompletableSyncPooled8()
	{
		using (var job = this.completableJobs.Rent())
		{
			job.Value.Volume = this.volume;
			job.Value.ID = "8 (Sync, PooledView)";

			this.processor.Submit(job.AsPinned);
		}
	}

	private void CompletableSyncPooled9()
	{
		var job = this.completableJobs.Rent();
		job.Value.Volume = this.volume;
		job.Value.ID = "9 (Sync, Pooled)";

		this.processor.Submit(job);
	}

	private async Task CompletableAsync10()
	{
		var job = new TestVoxelJobComplete
		{
			Volume = this.volume,
			ID = "10 (Async)"
		};

		var result = await this.processor.SubmitAsync(job).MarshallContext();
		this.CheckEquals(job, result.Job, job.ID);
	}

	private async Task CompletableAsyncPooled11()
	{
		using (var job = this.completableJobs.Rent())
		{
			job.Value.Volume = this.volume;
			job.Value.ID = "11 (Async, PooledView)";

			var result = await this.processor.SubmitAsync(job.AsPinned).MarshallContext();
			using (result.Job)
			{
				this.CheckEquals(job, result.Job, job.Value.ID);
			}
		}
	}

	private async Task CompletableAsyncPooled12()
	{
		using (var job = this.completableJobs.Rent())
		{
			job.Value.Volume = this.volume;
			job.Value.ID = "12 (Async, Pooled)";

			var result = await this.processor.SubmitAsync(job.CreatePin()).MarshallContext();
			using (result.Job)
			{
				this.CheckEquals(job, result.Job, job.Value.ID);
			}
		}
	}

	private void CheckEquals(IVoxelJob jobA, IVoxelJob jobB, string id)
	{
		if (!jobA.Equals(jobB))
		{
			throw new ArgumentException($"Test {id} equality failed.");
		}
	}

	private void CheckEquals<T>(Pin<T> jobA, Pin<T> jobB, string id)
		where T : class, IVoxelJob
	{
		if (!jobA.Value.Equals(jobB.Value))
		{
			throw new ArgumentException($"Test {id} equality failed.");
		}
	}

	private class TestVoxelJob : ReadSingleVolumeJob, IDeinitializable
	{
		public string ID { get; set; } = "UNINITIALIZED";

		/// <inheritdoc />
		public void Deinitialize()
		{
			Debug.Log($"Job {this.ID} deinitialized on thread {Thread.CurrentThread.ManagedThreadId}");
			this.ID = "UNINITIALIZED";
		}

		/// <inheritdoc />
		protected override void Run(IVoxelReadView volume)
		{
			Debug.Log($"Job {this.ID} ran on thread {Thread.CurrentThread.ManagedThreadId}");
		}
	}

	private class TestVoxelJobComplete : TestVoxelJob, IVoxelJobCompletion
	{
		/// <inheritdoc />
		public void Complete(VoxelJobDiagnostics results)
		{
			Debug.Log($"Job {this.ID} completed on thread {Thread.CurrentThread.ManagedThreadId}");
		}
	}
}
