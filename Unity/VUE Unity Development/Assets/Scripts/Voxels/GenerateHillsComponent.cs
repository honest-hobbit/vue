﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HQS.Utility.Indexing.Indices;
using HQS.Utility.Indexing.Shapes;
using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility.Jobs;
using UnityEngine;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class GenerateHillsComponent : MonoBehaviour
{
	[SerializeField]
	private SingleVolumeComponent universe = null;

	[SerializeField]
	private KeyCode triggerOnKeyPressed = KeyCode.Return;

	[SerializeField]
	private int seed = 0;

	[SerializeField]
	private int length = 0;

	[SerializeField]
	private ushort voxelGroundFirst = 1;

	[SerializeField]
	private ushort voxelGroundSecond = 1;

	[SerializeField]
	private ushort voxelGroundDots = 1;

	[SerializeField]
	private bool checkCollision = false;

	public void Update()
	{
		if (Input.GetKeyDown(this.triggerOnKeyPressed))
		{
			this.universe.Universe.Jobs.Submit(new GenerateHillsJob()
			{
				Volume = this.universe.Volume,
				Seed = this.seed,
				Shape = IndexShape.CreateSquare(this.length).Select(i => i.ToInt()),
				GroundFirst = new Voxel(this.voxelGroundFirst),
				GroundSecond = new Voxel(this.voxelGroundSecond),
				GroundDots = new Voxel(this.voxelGroundDots),
				CheckCollision = this.checkCollision,
			});
		}
	}
}
