﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reactive.Linq;
using System.Text;
using HQS.Utility;
using HQS.Utility.Numerics;
using HQS.Utility.Unity.EditorAddons;
using HQS.VUE.Core;
using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility.Stores;
using HQS.VUE.Unity;
using HQS.VUE.Unity.OLD;
using UnityEngine;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
[RequireComponent(typeof(VoxelUniverseComponent))]
public class InitializeVoxelUniverseComponent : MonoBehaviour
{
	private StringBuilder logBuilder;

	[SerializeField]
	private Material material = null;

	[SerializeField]
	private int populateChunks = 0;

	[Range(0, 1)]
	[SerializeField]
	private float populatePercentUniform = .5f;

	[SerializeField]
	private bool logJobDiagnostics = false;

	[SerializeField]
	private bool logUnityDiagnostics = false;

	[SerializeField]
	private KeyCode logResourceDiagnostics = KeyCode.None;

	[SerializeField]
	private KeyCode clearChunkCaches = KeyCode.None;

	[SerializeField]
	private LogOptions logOptions = LogOptions.None;

	private bool isInitialized = false;

	private UnityVoxelUniverseOLD universe;

	private FileStores stores;

	public UnityVoxelUniverseOLD Universe
	{
		get
		{
			this.Initialize();
			return this.universe;
		}
	}

	public FileStores Stores
	{
		get
		{
			this.Initialize();
			return this.stores;
		}
	}

	protected void OnValidate()
	{
		this.populateChunks = this.populateChunks.ClampLower(0);
		this.populatePercentUniform = this.populatePercentUniform.Clamp(0, 1);
	}

	protected void Awake() => this.Initialize();

	protected void Update()
	{
		if (Input.GetKeyDown(this.logResourceDiagnostics))
		{
			if (this.logOptions.Has(LogOptions.CountOfPools))
			{
				this.logBuilder.AppendLine("Count of Pools: " + this.universe.Diagnostics.Pools.Count);
				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.ChunkPools))
			{
				this.logBuilder.AppendLine("Chunk Pools");
				foreach (var pool in PooledTypesOLD.Chunks.Enumerate())
				{
					this.LogPool(pool);
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.VolumePools))
			{
				this.logBuilder.AppendLine("Volume Pools");
				foreach (var pool in PooledTypesOLD.Volumes.Enumerate())
				{
					this.LogPool(pool);
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.VoxelJobPools))
			{
				this.logBuilder.AppendLine("Voxel Job Pools");
				foreach (var pool in PooledTypesOLD.VoxelJobs.Enumerate())
				{
					this.LogPool(pool);
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.ContouringPools))
			{
				this.logBuilder.AppendLine("Contouring Pools");
				foreach (var pool in PooledTypesOLD.Contouring.Enumerate())
				{
					this.LogPool(pool);
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.PersistencePools))
			{
				this.logBuilder.AppendLine("Persistence Pools");
				foreach (var pool in PooledTypesOLD.Persistence.Enumerate())
				{
					this.LogPool(pool);
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.ResyncPools))
			{
				this.logBuilder.AppendLine("Resync Pools");
				foreach (var pool in PooledUnityTypesOLD.Resync.Enumerate())
				{
					this.LogPool(pool);
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.GameObjectPools))
			{
				this.logBuilder.AppendLine("GameObject Pools");
				foreach (var pool in PooledUnityTypesOLD.GameObjects.Enumerate())
				{
					this.LogPool(pool);
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.VoxelChunkCache))
			{
				var cache = this.universe.Diagnostics.VoxelChunkCache;
				this.logBuilder.AppendLine($"Voxel Chunk Cache - Count: {cache.Count}");
				foreach (var entry in cache.GetChunksSnapshot())
				{
					this.logBuilder.AppendLine(entry.ToString());
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}

			if (this.logOptions.Has(LogOptions.ContourChunkCache))
			{
				var cache = this.universe.Diagnostics.ContourChunkCache;
				this.logBuilder.AppendLine($"Contour Chunk Cache - Count: {cache.Count}");
				foreach (var entry in cache.GetChunksSnapshot())
				{
					this.logBuilder.AppendLine(entry.ToString());
				}

				Debug.Log(this.logBuilder.ToString());
				this.logBuilder.Clear();
			}
		}

		if (Input.GetKeyDown(this.clearChunkCaches))
		{
			int voxelChunksCached = this.universe.Diagnostics.VoxelChunkCache.Count;
			int voxelChunksCleared = this.universe.Diagnostics.VoxelChunkCache.TryClear();
			int contourChunksCached = this.universe.Diagnostics.ContourChunkCache.Count;
			int contourChunksCleared = this.universe.Diagnostics.ContourChunkCache.TryClear();

			this.logBuilder.AppendLine($"Voxel Chunks Cached: {voxelChunksCached}");
			this.logBuilder.AppendLine($"Voxel Chunks Cleared: {voxelChunksCleared}");
			this.logBuilder.AppendLine($"Contour Chunks Cached: {contourChunksCached}");
			this.logBuilder.AppendLine($"Contour Chunks Cleared: {contourChunksCleared}");

			Debug.Log(this.logBuilder.ToString());
			this.logBuilder.Clear();
		}
	}

	private void LogPool(PooledType type, bool showWarning = true)
	{
		Debug.Assert(type != null);

		var diagnostics = this.universe.Diagnostics.Pools[type].Diagnostics;
		if (showWarning && (diagnostics.Value.Active > 0 || diagnostics.Value.Released > 0))
		{
			this.logBuilder.AppendLine(diagnostics.ToString() + "   WARNING!!!   WARNING!!!   WARNING!!!   WARNING!!!   WARNING!!!");
		}
		else
		{
			this.logBuilder.AppendLine(diagnostics.ToString());
		}
	}

	private void Initialize()
	{
		if (this.isInitialized)
		{
			return;
		}

		this.isInitialized = true;
		if (this.material == null)
		{
			throw new ArgumentNullException(nameof(this.material));
		}

		var voxelTypes = new VoxelTypesConfigBuilder()
			.StartSurfaceTypes().AddMany(
				SurfaceVisibility.Opaque, SurfaceVisibility.Transparent)
			.StartPolyTypes()
				.Add(new SurfaceTypeIndex(1), Colors.White)
				.Add(new SurfaceTypeIndex(1), Colors.Gray)
				.Add(new SurfaceTypeIndex(1), Colors.Black)
				.Add(new SurfaceTypeIndex(1), Colors.Red)
				.Add(new SurfaceTypeIndex(1), Colors.Green)
				.Add(new SurfaceTypeIndex(1), Colors.Blue)
				.Add(new SurfaceTypeIndex(1), Colors.Violet)
				.Add(new SurfaceTypeIndex(1), Colors.Orange)
				.Add(new SurfaceTypeIndex(1), Colors.Yellow)
				.Add(new SurfaceTypeIndex(1), Colors.Brown)
			.StartColliderTypes().AddMany(
				ColliderType.Empty) // this is a placeholder until real collider types added
			.StartVoxelTypes()
				.Add(new PolyTypeIndex(1), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(2), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(3), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(4), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(5), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(6), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(7), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(8), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(9), new ColliderTypeIndex(1))
				.Add(new PolyTypeIndex(10), new ColliderTypeIndex(1))
			.BuildConfig();

		var universeConfig = new UniverseConfigBuilder()
		{
		}.BuildConfig();

		var unityConfig = new UnityConfigBuilder()
		{
			VoxelMaterial = this.material,
		}.BuildConfig();

		var universeComponent = this.GetComponent<VoxelUniverseComponent>();
		universeComponent.Initialize(unityConfig, universeConfig, voxelTypes);
		this.universe = universeComponent.Universe;

		var storeConfig = new FileStoreConfigBuilder()
		{
			VoxelSizeConfig = this.universe.SizeConfig,
		}.BuildConfig();

		this.stores = new FileStores(storeConfig);

		if (this.populateChunks > 0)
		{
			this.Universe.PopulateVoxelChunks(this.populateChunks, this.populatePercentUniform);
			this.Universe.PopulateContourChunks(this.populateChunks, this.populatePercentUniform);
		}

		this.universe.Diagnostics.ErrorOccurred.Subscribe(x => Debug.LogError(x));

		int builderCapacity = Math.Max(VoxelJobDiagnostics.AverageStringLength, ResyncUnityDiagnostics.AverageStringLength);
		this.logBuilder = new StringBuilder(builderCapacity);

		if (this.logJobDiagnostics)
		{
			this.LogDiagnostics(this.universe.Diagnostics.JobCompleted, x => x.AddToBuilder(this.logBuilder));
		}

		if (this.logUnityDiagnostics)
		{
			this.LogDiagnostics(this.universe.Diagnostics.ResyncCompleted, x => x.AddToBuilder(this.logBuilder));
		}
	}

	private void LogDiagnostics<T>(IObservable<T> diagnostics, Action<T> addToBuilder)
	{
		Debug.Assert(this.logBuilder != null);
		Debug.Assert(this.logBuilder.Length == 0);
		Debug.Assert(diagnostics != null);
		Debug.Assert(addToBuilder != null);

		diagnostics.Subscribe(results =>
		{
			addToBuilder(results);
			Debug.Log(this.logBuilder.ToString());
			this.logBuilder.Clear();
		});
	}
}
