﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using HQS.Utility;
using HQS.Utility.Unity;
using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility.Stores;
using HQS.VUE.Unity.OLD;
using HQS.VUE.Unity.OLD.Utility.Particles;
using UnityEngine;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class SingleVolumeComponent : MonoBehaviour
{
	[SerializeField]
	private InitializeVoxelUniverseComponent universeComponent = null;

	[SerializeField]
	private AnimateVoxelParticlesPoolComponent animatorPoolComponent = null;

	[SerializeField]
	private string voxelStoreFileName = null;

	[SerializeField]
	private string contourStoreFileName = null;

	[SerializeField]
	private bool multipleVolumes = false;

	[SerializeField]
	private KeyCode createVolumeInstance = KeyCode.None;

	private UnityVoxelUniverseOLD universe;

	private FileStores stores;

	private Volume volume;

	private VolumeComponent volumeInstance;

	public UnityVoxelUniverseOLD Universe
	{
		get
		{
			this.Initialize();
			return this.universe;
		}
	}

	public FileStores Stores
	{
		get
		{
			this.Initialize();
			return this.stores;
		}
	}

	public Volume Volume
	{
		get
		{
			this.Initialize();
			return this.volume;
		}
	}

	public VolumeComponent VolumeInstance
	{
		get
		{
			this.Initialize();
			return this.volumeInstance;
		}
	}

	public void Start() => this.Initialize();

	public void Update()
	{
		if (Input.GetKeyDown(this.createVolumeInstance))
		{
			var instance = (VolumeComponent)this.Volume.Instances.CreateInstance();
			instance.transform.localScale = .25f.ToUnityVector3();
			instance.SetAnimator(this.animatorPoolComponent.GetVoxelChangeAnimatorNullSafe());
		}
	}

	private void Initialize()
	{
		if (this.universe != null)
		{
			return;
		}

		if (this.universeComponent == null)
		{
			throw new ArgumentNullException(nameof(this.universeComponent));
		}

		this.universe = this.universeComponent.Universe;
		this.stores = this.universeComponent.Stores;

		// create stores
		VoxelGridFile voxelStore = null;
		VoxelShapeFile contourStore = null;
		var folder = Path.Combine(Application.streamingAssetsPath, "Generated");
		this.stores.FindAllStoresInDirectory(folder, SearchOption.AllDirectories);

		if (!this.voxelStoreFileName.IsNullOrWhiteSpace())
		{
			try
			{
				voxelStore = this.stores.GetOrCreateVoxelGridStore(Path.Combine(folder, this.voxelStoreFileName));
			}
			catch (ArgumentException)
			{
				Debug.LogWarning("Voxel store file path contains invalid characters.");
			}
		}

		if (!this.contourStoreFileName.IsNullOrWhiteSpace())
		{
			try
			{
				contourStore = this.stores.GetOrCreateVoxelShapeStore(Path.Combine(folder, this.contourStoreFileName));
			}
			catch (ArgumentException)
			{
				Debug.LogWarning("Contour store file path contains invalid characters.");
			}
		}

		this.stores.LoadAllVolumesFromCurrentStores(this.universe.Volumes);

		if (this.universe.Volumes.Collection.Count == 0)
		{
			// create new volume if none were loaded
			this.volume = this.Universe.Volumes.CreateVolume();
			voxelStore?.AttachTo(this.volume);
			contourStore?.AttachTo(this.volume);
			this.volume.Autosave = true;
		}
		else
		{
			this.volume = this.Universe.Volumes.Collection.Values.First();
			if (!this.volume.HasVoxelGridStore)
			{
				voxelStore?.AttachTo(this.volume);
			}

			if (!this.volume.HasVoxelShapeStore)
			{
				contourStore?.AttachTo(this.volume);
			}
		}

		// create game object instance for volume
		var instance = (VolumeComponent)this.Volume.Instances.CreateInstance();
		instance.transform.localScale = .25f.ToUnityVector3();
		instance.SetAnimator(this.animatorPoolComponent.GetVoxelChangeAnimatorNullSafe());
		this.volumeInstance = instance;

		if (this.multipleVolumes)
		{
			instance = (VolumeComponent)this.Volume.Instances.CreateInstance();
			instance.transform.localScale = new Vector3(.1f, .2f, .3f);
			instance.transform.position = new Vector3(-10, 10, 0);
			instance.transform.Rotate(55, 0, -20);
			instance.SetAnimator(this.animatorPoolComponent.GetVoxelChangeAnimatorNullSafe());

			instance = (VolumeComponent)this.Volume.Instances.CreateInstance();
			instance.transform.localScale = .1f.ToUnityVector3();
			instance.transform.position = new Vector3(5, 5, 5);
			instance.transform.Rotate(0, 30, 0);
		}
	}
}
