﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HQS.Utility.Indexing.Indices;
using HQS.Utility.Indexing.Shapes;
using HQS.Utility.Numerics;
using HQS.Utility.Resources;
using HQS.Utility.Unity;
using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility.Jobs;
using HQS.VUE.Unity.OLD;
using UnityEngine;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class VoxelEditComponent : MonoBehaviour
{
	private IPinPool<GenerateHillsJob> hillsJobs;

	private IPinPool<FillShapeJob> shapeJobs;

	private IPinPool<SetVoxelJob> setJobs;

	private IPinPool<ReadVoxelJob> printJobs;

	private IEnumerable<Int2> hillsShape;

	private IEnumerable<Int3> fillShape;

	private Voxel voxel = new Voxel(1);

	[SerializeField]
	private new Camera camera = null;

	[SerializeField]
	private InitializeVoxelUniverseComponent universe = null;

	public void Start()
	{
		this.hillsJobs = Pool.ThreadSafe.Pins.Create<GenerateHillsJob>();
		this.shapeJobs = Pool.ThreadSafe.Pins.Create<FillShapeJob>();
		this.setJobs = Pool.ThreadSafe.Pins.Create<SetVoxelJob>();
		this.printJobs = Pool.ThreadSafe.Pins.Create<ReadVoxelJob>();

		// not safe to instantiate IndexShapes outside of a method because of Unity's serialization system
		this.hillsShape = IndexShape.CreateCircle(60).Select(i => i.ToInt());
		this.fillShape = IndexShape.CreateSphere(20).Select(i => i.ToInt());
	}

	public void Update()
	{
		if (this.universe.Universe == null)
		{
			return;
		}

		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			this.voxel = new Voxel(1);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			this.voxel = new Voxel(2);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			this.voxel = new Voxel(3);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha4))
		{
			this.voxel = new Voxel(4);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha5))
		{
			this.voxel = new Voxel(5);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha6))
		{
			this.voxel = new Voxel(6);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha7))
		{
			this.voxel = new Voxel(7);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha8))
		{
			this.voxel = new Voxel(8);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha9))
		{
			this.voxel = new Voxel(9);
		}
		else if (Input.GetKeyDown(KeyCode.Alpha0))
		{
			this.voxel = new Voxel(10);
		}

		var maxVoxelType = this.universe.Universe.Definitions.Voxels.Count - 1;
		if (this.voxel.TypeIndex > maxVoxelType)
		{
			this.voxel = new Voxel((ushort)maxVoxelType);
		}

		if (Input.GetMouseButtonDown(MouseButton.Left))
		{
			if (this.RaycastVoxel(out var hit))
			{
				if (Input.GetKey(KeyCode.LeftControl))
				{
					var pooled = this.hillsJobs.Rent();
					var job = pooled.Value;
					job.Volume = hit.Volume.Volume;
					job.Seed = 3;
					job.Origin = hit.HitIndex;
					job.Shape = this.hillsShape;
					job.GroundFirst = new Voxel(5);
					job.GroundSecond = new Voxel(2);
					job.GroundDots = new Voxel(9);
					this.universe.Universe.Jobs.Submit(pooled);
				}
				else if (Input.GetKey(KeyCode.LeftShift))
				{
					var pooled = this.shapeJobs.Rent();
					var job = pooled.Value;
					job.Volume = hit.Volume.Volume;
					job.Origin = hit.HitIndex;
					job.Shape = this.fillShape;
					job.Voxel = this.voxel;
					this.universe.Universe.Jobs.Submit(pooled);
				}
				else
				{
					var pooled = this.setJobs.Rent();
					var job = pooled.Value;
					job.Volume = hit.Volume.Volume;
					job.Voxel = this.voxel;
					job.Index = hit.AdjacentIndex;
					this.universe.Universe.Jobs.Submit(pooled);
				}
			}
		}
		else if (Input.GetMouseButtonDown(MouseButton.Right))
		{
			if (this.RaycastVoxel(out var hit))
			{
				if (Input.GetKey(KeyCode.LeftControl))
				{
					var pooled = this.printJobs.Rent();
					var job = pooled.Value;
					job.Volume = hit.Volume.Volume;
					job.Index = hit.HitIndex;
					job.LogMessage = Debug.Log;
					this.universe.Universe.Jobs.Submit(pooled);
				}
				else if (Input.GetKey(KeyCode.LeftShift))
				{
					var pooled = this.shapeJobs.Rent();
					var job = pooled.Value;
					job.Volume = hit.Volume.Volume;
					job.Origin = hit.HitIndex;
					job.Shape = this.fillShape;
					job.Voxel = Voxel.Empty;
					this.universe.Universe.Jobs.Submit(pooled);
				}
				else
				{
					var pooled = this.setJobs.Rent();
					var job = pooled.Value;
					job.Volume = hit.Volume.Volume;
					job.Voxel = Voxel.Empty;
					job.Index = hit.HitIndex;
					this.universe.Universe.Jobs.Submit(pooled);
				}
			}
		}
	}

	private bool RaycastVoxel(out VoxelHit hit)
	{
		var ray = this.camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
		return VoxelPhysics.Raycast(ray, out hit);
	}
}
