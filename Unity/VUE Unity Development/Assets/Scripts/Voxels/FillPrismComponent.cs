﻿using System.Diagnostics.CodeAnalysis;
using HQS.Utility.Numerics;
using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility.Jobs;
using UnityEngine;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class FillPrismComponent : MonoBehaviour
{
	[SerializeField]
	private SingleVolumeComponent universe = null;

	[SerializeField]
	private KeyCode triggerOnKeyPressed = KeyCode.Return;

	[SerializeField]
	private ushort voxelA = 0;

	[SerializeField]
	private ushort voxelB = 0;

	[SerializeField]
	private bool isCheckered = false;

	[SerializeField]
	private int minX = 0;

	[SerializeField]
	private int minY = 0;

	[SerializeField]
	private int minZ = 0;

	[SerializeField]
	private int dimX = 0;

	[SerializeField]
	private int dimY = 0;

	[SerializeField]
	private int dimZ = 0;

	[SerializeField]
	private bool checkCollision = false;

	public Voxel VoxelA
	{
		get => new Voxel(this.voxelA);
		set => this.voxelA = value.TypeIndex;
	}

	public Voxel VoxelB
	{
		get => new Voxel(this.voxelB);
		set => this.voxelB = value.TypeIndex;
	}

	public bool IsCheckered
	{
		get => this.isCheckered;
		set => this.isCheckered = value;
	}

	public Int3 Min
	{
		get => new Int3(this.minX, this.minY, this.minZ);

		set
		{
			this.minX = value.X;
			this.minY = value.Y;
			this.minZ = value.Z;
		}
	}

	public Int3 Dimensions
	{
		get => new Int3(this.dimX, this.dimY, this.dimZ);

		set
		{
			this.dimX = value.X;
			this.dimY = value.Y;
			this.dimZ = value.Z;
		}
	}

	public bool CheckCollision
	{
		get => this.checkCollision;
		set => this.checkCollision = value;
	}

	public void Update()
	{
		if (Input.GetKeyDown(this.triggerOnKeyPressed))
		{
			if (this.isCheckered)
			{
				this.universe.Universe.Jobs.Submit(new CheckerJob()
				{
					Volume = this.universe.Volume,
					VoxelA = this.VoxelA,
					VoxelB = this.VoxelB,
					Min = this.Min,
					Dimensions = this.Dimensions,
					CheckCollision = this.CheckCollision,
				});
			}
			else
			{
				this.universe.Universe.Jobs.Submit(new FillPrismJob()
				{
					Volume = this.universe.Volume,
					Voxel = this.VoxelA,
					Min = this.Min,
					Dimensions = this.Dimensions,
					CheckCollision = this.CheckCollision,
				});
			}
		}
	}
}
