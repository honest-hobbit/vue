﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using HQS.Utility;
using HQS.Utility.Numerics;
using UnityEngine;

namespace HQS.VUE
{
	public class RectangleArray<T> : IEnumerable<T>
	{
		public RectangleArray(RectangleArrayIndexer indexer)
		{
			this.Indexer = indexer;
			this.Array = new T[this.Indexer.Length];
		}

		public RectangleArray(RectangleArrayIndexer indexer, T[] array)
		{
			Debug.Assert(array != null);
			Debug.Assert(array.Length == indexer.Length);

			this.Indexer = indexer;
			this.Array = array;
		}

		public RectangleArrayIndexer Indexer { get; }

		public T[] Array { get; }

		public T this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => this.Array[index];

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set => this.Array[index] = value;
		}

		public T this[int x, int y]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => this.Array[this.Indexer[x, y]];

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set => this.Array[this.Indexer[x, y]] = value;
		}

		public T this[Int2 index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => this.Array[this.Indexer[index]];

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set => this.Array[this.Indexer[index]] = value;
		}

		public ArraySegment<T>.Enumerator GetEnumerator() => this.Array.GetEnumeratorTypeSafe();

		/// <inheritdoc />
		IEnumerator<T> IEnumerable<T>.GetEnumerator() => this.GetEnumerator();

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
	}
}
