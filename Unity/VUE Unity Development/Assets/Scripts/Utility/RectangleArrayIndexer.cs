﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using HQS.Utility;
using HQS.Utility.Numerics;
using UnityEngine;

namespace HQS.VUE
{
	public readonly struct RectangleArrayIndexer : IEquatable<RectangleArrayIndexer>
	{
		public RectangleArrayIndexer(Int2 dimensions)
		{
			Debug.Assert(dimensions.X >= 1);
			Debug.Assert(dimensions.Y >= 1);

			this.Dimensions = dimensions;
		}

		public Int2 Dimensions { get; }

		public Int2 LowerBounds => Int2.Zero;

		public Int2 UpperBounds => new Int2(this.Dimensions.X - 1, this.Dimensions.Y - 1);

		public int Length
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => this.Dimensions.X * this.Dimensions.Y;
		}

		public int this[int x, int y]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => x + (y * this.Dimensions.X);
		}

		public int this[Int2 index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => index.X + (index.Y * this.Dimensions.X);
		}

		public Int2 this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				int y = index / this.Dimensions.X;
				int x = index - (y * this.Dimensions.X);
				return new Int2(x, y);
			}
		}

		/// <inheritdoc />
		public override bool Equals(object obj) => Struct.Equals(this, obj);

		/// <inheritdoc />
		public bool Equals(RectangleArrayIndexer other) => this.Dimensions.Equals(other.Dimensions);

		/// <inheritdoc />
		public override int GetHashCode() => this.Dimensions.GetHashCode();

		public static bool operator ==(RectangleArrayIndexer left, RectangleArrayIndexer right) => left.Equals(right);

		public static bool operator !=(RectangleArrayIndexer left, RectangleArrayIndexer right) => !(left == right);
	}
}
