﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenePicker : MonoBehaviour
{
	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			SceneManager.LoadScene("Simple");
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			SceneManager.LoadScene("Large");
		}
		else if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			SceneManager.LoadScene("Animated");
		}
		else if (Input.GetKeyDown(KeyCode.Alpha4))
		{
			SceneManager.LoadScene("Waves");
		}
	}
}
