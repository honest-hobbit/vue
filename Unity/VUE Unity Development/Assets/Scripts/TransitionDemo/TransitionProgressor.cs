﻿using HQS.Utility.Indexing.Indices;
using HQS.Utility.Numerics;
using HQS.Utility.Resources;
using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility;
using UnityEngine;

public class TransitionProgressor : IVoxelChangeAnimatorTiming
{
	private readonly IPinPool<CopyBandJob> jobPool = Pool.ThreadSafe.Pins.Create<CopyBandJob>();

	private readonly IVoxelJobProcessorOLD jobQueue;

	private readonly DistanceMap2D map;

	private int maxBandNumber;

	private int bandNumber = 0;

	public TransitionProgressor(IVoxelJobProcessorOLD jobQueue, Int2 dimensions)
	{
		Debug.Assert(jobQueue != null);
		Debug.Assert(dimensions.X >= 1);
		Debug.Assert(dimensions.Y >= 1);

		this.jobQueue = jobQueue;
		this.map = new DistanceMap2D(dimensions);

		var noise = new FastNoiseLite();
		noise.SetNoiseType(FastNoiseLite.NoiseType.OpenSimplex2);
		noise.SetFrequency(.1f);
		this.map.Noise = noise;
	}

	public Volume ReadVolume { get; set; }

	public Volume WriteVolume { get; set; }

	public int MinY { get; set; }

	public int MaxY { get; set; }

	public int BandWidth
	{
		get => this.map.BandWidth;
		set => this.map.BandWidth = value;
	}

	public bool CheckCollision { get; set; } = false;

	/// <inheritdoc />
	public VoxelChangeTiming TimingMode { get; set; } = VoxelChangeTiming.Consistent;

	public void SetOrigin(Index2D origin)
	{
		Debug.Assert(this.BandWidth >= 1);

		this.maxBandNumber = this.map.SetOrigin(origin);
		this.bandNumber = 0;
	}

	// returns true if there are more bands left to copy, otherwise false
	public bool NextBand()
	{
		Debug.Assert(this.ReadVolume != null);
		Debug.Assert(this.WriteVolume != null);
		Debug.Assert(this.MinY <= this.MaxY);

		if (this.bandNumber > this.maxBandNumber)
		{
			return false;
		}

		var pooledJob = this.jobPool.Rent();
		var job = pooledJob.Value;
		job.ReadVolume = this.ReadVolume;
		job.WriteVolume = this.WriteVolume;
		job.Map = this.map;
		job.BandNumber = this.bandNumber;
		job.MinY = this.MinY;
		job.MaxY = this.MaxY;
		job.CheckCollision = this.CheckCollision;
		job.TimingMode = this.TimingMode;
		this.jobQueue.Submit(pooledJob);

		this.bandNumber++;
		return this.bandNumber <= this.maxBandNumber;
	}
}
