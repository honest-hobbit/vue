﻿using System;
using HQS.Utility.Indexing.Indices;
using HQS.Utility.Numerics;
using HQS.VUE;
using UnityEngine;

public class DistanceMap2D
{
	private readonly RectangleArray<ushort> distances;

	public DistanceMap2D(Int2 dimensions)
	{
		Debug.Assert(dimensions.X >= 1);
		Debug.Assert(dimensions.Y >= 1);

		this.distances = new RectangleArray<ushort>(new RectangleArrayIndexer(dimensions));
	}

	public FastNoiseLite Noise { get; set; }

	public int BandWidth { get; set; } = 1;

	public RectangleArrayIndexer Indexer => this.distances.Indexer;

	public int Length => this.distances.Array.Length;

	public int this[int index] => this.distances.Array[index];

	public int this[int x, int y] => this.distances[x, y];

	public int this[Int2 index] => this.distances[index];

	// returns the max distance stored in map
	public int SetOrigin(Index2D origin)
	{
		Debug.Assert(this.BandWidth >= 1);

		int xMax = this.distances.Indexer.Dimensions.X;
		int yMax = this.distances.Indexer.Dimensions.Y;
		int maxDistance = 0;

		if (this.Noise == null)
		{
			for (int iX = 0; iX < xMax; iX++)
			{
				int xDistance = origin.X - iX;
				xDistance *= xDistance;

				for (int iY = 0; iY < yMax; iY++)
				{
					int yDistance = origin.Y - iY;
					yDistance *= yDistance;

					ushort distance = (Math.Sqrt(xDistance + yDistance) / this.BandWidth).ClampToUShort();

					this.distances[iX, iY] = distance;
					if (distance > maxDistance)
					{
						maxDistance = distance;
					}
				}
			}
		}
		else
		{
			for (int iX = 0; iX < xMax; iX++)
			{
				int xDistance = origin.X - iX;
				xDistance *= xDistance;

				for (int iY = 0; iY < yMax; iY++)
				{
					int yDistance = origin.Y - iY;
					yDistance *= yDistance;

					ushort distance = ((Math.Sqrt(xDistance + yDistance) / this.BandWidth) + this.Noise.GetNoise(iX, iY)).ClampToUShort();

					this.distances[iX, iY] = distance;
					if (distance > maxDistance)
					{
						maxDistance = distance;
					}
				}
			}
		}

		return maxDistance;
	}
}
