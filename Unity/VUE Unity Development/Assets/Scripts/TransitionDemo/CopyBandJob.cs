﻿using HQS.VUE.OLD;
using HQS.VUE.OLD.Utility;
using UnityEngine;

public class CopyBandJob : ReadWriteVolumesJob, IVoxelChangeAnimatorTiming
{
	public DistanceMap2D Map { get; set; }

	public int BandNumber { get; set; }

	public int MinY { get; set; }

	public int MaxY { get; set; }

	/// <inheritdoc />
	public VoxelChangeTiming TimingMode { get; set; } = VoxelChangeTiming.Immediate;

	/// <inheritdoc />
	protected override void Run(IVoxelReadView readVolume, IVoxelWriteView writeVolume)
	{
		Debug.Assert(this.Map != null);
		Debug.Assert(this.BandNumber >= 0);
		Debug.Assert(this.MinY <= this.MaxY);

		int max = this.Map.Length;
		for (int i = 0; i < max; i++)
		{
			if (this.Map[i] == this.BandNumber)
			{
				var index = this.Map.Indexer[i];
				for (int iY = this.MinY; iY <= this.MaxY; iY++)
				{
					writeVolume[index.X, iY, index.Y] = readVolume[index.X, iY, index.Y];
				}
			}
		}
	}

	/// <inheritdoc />
	protected override void OnCleared()
	{
		this.Map = null;
		this.BandNumber = 0;
		this.MinY = 0;
		this.MaxY = 0;
	}
}
