﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HQS.Utility.Indexing.Indices;
using HQS.Utility.Numerics;
using HQS.Utility.Resources;
using HQS.Utility.Unity;
using HQS.VUE.OLD.Utility;
using HQS.VUE.OLD.Utility.Jobs;
using HQS.VUE.Unity.OLD;
using HQS.VUE.Unity.OLD.Utility.Particles;
using UnityEngine;
using Debug = UnityEngine.Debug;

[SuppressMessage("Style", "IDE0044", Justification = "SerializeField must be mutable for Unity.")]
public class VolumeTransitionComponent : MonoBehaviour
{
	private VolumeComponent volumeInstance;

	private TransitionProgressor progressor;

	private IPinPool<GenerateHillsJob> generateJobs;

	private IEnumerable<Int2> generateShape;

	private int seed = 0;

	private Vector3 originPosition;

	private Coroutine transition;

	[SerializeField]
	private InitializeVoxelUniverseComponent universeComponent = null;

	[SerializeField]
	private AnimateVoxelParticlesPoolComponent animatorPoolComponent = null;

	[SerializeField]
	private int dimensionsX = 1;

	[SerializeField]
	private int dimensionsZ = 1;

	[SerializeField]
	private int minY = 0;

	[SerializeField]
	private int maxY = 0;

	[SerializeField]
	private int bandWidth = 1;

	[SerializeField]
	private float delayBetweenBands = 1;

	[SerializeField]
	private bool checkCollision = false;

	[SerializeField]
	private VoxelChangeTiming timingMode = VoxelChangeTiming.Consistent;

	[SerializeField]
	private KeyCode startTransition = KeyCode.None;

	[SerializeField]
	private KeyCode generateNewVolume = KeyCode.None;

	[SerializeField]
	private float originCutoffThreshold = 1;

	[SerializeField]
	private GameObject origin = null;

	public void OnValidate()
	{
		this.dimensionsX = this.dimensionsX.ClampLower(1);
		this.dimensionsZ = this.dimensionsZ.ClampLower(1);
		this.maxY = this.maxY.ClampLower(this.minY);
		this.bandWidth = this.bandWidth.ClampLower(1);
		if (this.delayBetweenBands <= 0)
		{
			this.delayBetweenBands = 1;
		}

		if (this.originCutoffThreshold <= 0)
		{
			this.originCutoffThreshold = 1;
		}
	}

	public void Start()
	{
		Debug.Assert(this.universeComponent != null);
		Debug.Assert(this.origin != null);

		var dimensions = new Int2(this.dimensionsX, this.dimensionsZ);
		this.progressor = new TransitionProgressor(this.universeComponent.Universe.Jobs, dimensions)
		{
			MinY = this.minY,
			MaxY = this.maxY,
			BandWidth = this.bandWidth,
			CheckCollision = this.checkCollision,
			TimingMode = this.timingMode,
			WriteVolume = this.universeComponent.Universe.Volumes.CreateVolume(),
			ReadVolume = this.universeComponent.Universe.Volumes.CreateVolume(),
		};

		this.volumeInstance = (VolumeComponent)this.progressor.WriteVolume.Instances.CreateInstance();
		this.volumeInstance.transform.localScale = .25f.ToUnityVector3();
		this.volumeInstance.SetAnimator(this.animatorPoolComponent.GetVoxelChangeAnimatorNullSafe());

		this.generateJobs = Pool.ThreadSafe.Pins.Create<GenerateHillsJob>();
		this.generateShape = Index2D.Zero.Range(new Index2D(this.dimensionsX, this.dimensionsZ)).Select(i => i.ToInt());
		this.RegenerateVolume();
	}

	public void Update()
	{
		if (Input.GetKeyDown(this.generateNewVolume))
		{
			this.RegenerateVolume();
		}

		if (Input.GetKeyDown(this.startTransition))
		{
			if (this.transition != null)
			{
				this.StopCoroutine(this.transition);
			}

			this.RegenerateVolume();
			this.transition = this.StartCoroutine(this.TransitionVolume());
		}
	}

	private void RegenerateVolume()
	{
		this.seed++;

		var pooled = this.generateJobs.Rent();
		var job = pooled.Value;
		job.Volume = this.progressor.ReadVolume;
		job.Seed = this.seed;
		job.Shape = this.generateShape;
		job.ClearAboveGround = true;
		job.TimingMode = VoxelChangeTiming.Consistent;
		job.CheckCollision = false;
		this.universeComponent.Universe.Jobs.Submit(pooled);
	}

	private readonly Stopwatch timer = new Stopwatch();

	private IEnumerator TransitionVolume()
	{
		var delay = new WaitForSeconds(this.delayBetweenBands);

		this.originPosition = this.origin.transform.position;
		var position = this.volumeInstance.transform.InverseTransformPoint(this.originPosition);

		this.timer.Restart();
		this.progressor.SetOrigin(new Index2D((int)position.x, (int)position.z));
		this.timer.Stop();
		UnityEngine.Debug.Log($"SetOrigin: {this.timer.Elapsed.TotalMilliseconds:n2}");

		while (Vector3.Distance(this.origin.transform.position, this.originPosition) < this.originCutoffThreshold && this.progressor.NextBand())
		{
			yield return delay;
			////if (Vector3.Distance(this.origin.transform.position, this.originPosition) > this.originCutoffThreshold)
			////{
			////	yield break;
			////}
		}

		this.RegenerateVolume();
	}
}
