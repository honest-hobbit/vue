﻿using HQS.Utility;
using UnityEngine;

public class HelloWorldScript : MonoBehaviour
{
	public void Start()
	{
		var gameObject = new GameObject();

		Debug.Log("Testing");

		Debug.Log("Default name");
		PrintName(gameObject);

		Debug.Log("Null name");
		gameObject.name = null;
		PrintName(gameObject);

		Debug.Log("Empty name");
		gameObject.name = "";
		PrintName(gameObject);

		Debug.Log("Some name");
		gameObject.name = "Hello world";
		PrintName(gameObject);
	}

	private void PrintName(GameObject gameObject)
	{
		if (gameObject.name == null)
		{
			Debug.Log("Null");
		}
		else if (gameObject.name.IsNullOrWhiteSpace())
		{
			Debug.Log("Is Empty");
		}
		else
		{
			Debug.Log(gameObject.name);
		}
	}
}
