Voxel Universe Engine for Unity3D
=================================
An open source voxel engine using C# and the Unity3D game engine.
Still in the early stages of development and no longer being actively worked on.
Released under the MIT License.

The voxels can be cubes or any combination of 45 degree sloped wedge shapes.
This maintains a similar aesthetic to a blocky cube only voxel engine while
significantly expanding on what's possible with sloped surfaces. For example,
houses can have V shaped roofs instead of flat and towers can be octagons instead of square.
However, because all surfaces are still constrained to a limited set of possible slopes,
many of the same optimizations used in a cube only voxel engine can still be applied
to the wedge shapes resulting in significant performance improvements over a completely
organic and smooth voxel system.

[Click here](https://www.youtube.com/watch?v=ftmWbf1sKj8&t=22s) for a video demonstration of the voxel engine.