@ECHO OFF
ECHO Deleting everything except meta files from Unity built plugins
MKDIR temporary_pit
ROBOCOPY "Unity\VUE Unity Development\Assets\Plugins\Build" temporary_pit /XF *.meta /MOV >NUL
RMDIR /S /Q temporary_pit
PAUSE